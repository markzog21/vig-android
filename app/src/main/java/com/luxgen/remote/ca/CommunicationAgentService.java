package com.luxgen.remote.ca;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.luxgen.remote.R;
import com.luxgen.remote.activity.MainActivity;
import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.model.Beacon;
import com.luxgen.remote.model.KeyInfo;
import com.luxgen.remote.model.PushData;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.db.VigDatabase;
import com.luxgen.remote.model.option.BeaconOptions;
import com.luxgen.remote.model.option.CarOptions;
import com.luxgen.remote.model.option.VigOptions;
import com.luxgen.remote.network.CarWebApi;
import com.luxgen.remote.network.IkeyWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.VigRequestData;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.car.DeleteMasterKeyByAPPResponseData;
import com.luxgen.remote.network.model.car.GetKeyChainListRequestData;
import com.luxgen.remote.network.model.car.GetKeyChainListResponseData;
import com.luxgen.remote.network.model.car.GetProcedureRecordbyAPPRequestData;
import com.luxgen.remote.network.model.car.GetProcedureRecordbyAPPResponseData;
import com.luxgen.remote.network.model.car.GetWakeupVigResultRequestData;
import com.luxgen.remote.network.model.car.GetWakeupVigResultResponseData;
import com.luxgen.remote.network.model.car.SetCarControlLogRequestData;
import com.luxgen.remote.network.model.car.WakeupVigRequestData;
import com.luxgen.remote.network.model.ikey.CarProfileData;
import com.luxgen.remote.network.model.ikey.IkeyFuncToServerRequestData;
import com.luxgen.remote.network.model.ikey.IkeyFuncToServerResponseData;
import com.luxgen.remote.network.model.ikey.IkeyLoginResponseData;
import com.luxgen.remote.network.model.ikey.IkeyLoginSettingData;
import com.luxgen.remote.network.model.ikey.IkeyRemoteControlRequestData;
import com.luxgen.remote.network.model.ikey.IkeyRemoteControlResponseData;
import com.luxgen.remote.network.model.ikey.KeyOperationProcedure;
import com.luxgen.remote.ui.viewmodel.KeyRepository;
import com.luxgen.remote.util.BleManager;
import com.luxgen.remote.util.NetUtils;
import com.luxgen.remote.util.ObuManager;
import com.luxgen.remote.util.ObuMessageReceiver;
import com.luxgen.remote.util.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;

import luxgen.service.LuxgenLinkService;
import luxgen.service.StatusListener;
import tw.com.haitec.ae.project.smartphoneikey.ChannelsStatusListener;
import tw.com.haitec.ae.project.smartphoneikey.CommandCallback;
import tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent;
import tw.com.haitec.ae.project.smartphoneikey.DataChannel;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.IKeyDataBean;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.MasterKeyDataBean;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.PrepareKeyOperationBean;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.SnapKeyDataBean;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.SnapTicket;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.SubKeyDataBean;

import static tw.com.haitec.ae.project.smartphoneikey.snappydb.IKeyDataBean.MASTER_KEY;
import static tw.com.haitec.ae.project.smartphoneikey.snappydb.IKeyDataBean.SNAP_KEY;
import static tw.com.haitec.ae.project.smartphoneikey.snappydb.IKeyDataBean.STATUS_NORMAL;
import static tw.com.haitec.ae.project.smartphoneikey.snappydb.IKeyDataBean.SUB_KEY;

public class CommunicationAgentService extends Service {

    private static final String TAG = CommunicationAgentService.class.getSimpleName();
    private static final int CA_BLE_PACKAGE_LENGTH = 16;

    public static final String CA_STATUS_REMOTE_CONTROL_INTERNAL_ERROR =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.remoteControlInternalError";

    public static final String CA_STATUS_REMOTE_CONTROL_DONE =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.remoteControlControlDone";

    public static final String CA_STATUS_REMOTE_CONTROL_FAILED =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.remoteControlFailed";

    public static final String CA_STATUS_REMOTE_CONTROL_BLOCKED =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.remoteControlBlocked";

    public static final String CA_STATUS_IKEY_FUNCTOSERVER_TIMEOUT =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.ikeyFuncToServerTimeout";

    public static final String CA_STATUS_IKEY_LOGIN =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.ikeylogin";

    public static final String CA_STATUS_IKEY_UPDATE =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.ikeyUpdates";

    public static final String CA_STATUS_VIG_CONNECTION =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.vigConnected";

    public static final String CA_STATUS_ADD_MASTERKEY =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.addMasterKey";

    public static final String CA_STATUS_DEL_MASTERKEY =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.delMasterKey";

    public static final String CA_STATUS_ADD_SUBKEY =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.addSubKey";

    public static final String CA_STATUS_DEL_SUBKEY =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.delSubKey";

    public static final String CA_STATUS_RENEW_SNAPKEY =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.renewSnapKey";

    public static final String CA_STATUS_ALLOCATE_SNAPKEY =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.allocateSnapKey";

    public static final String CA_STATUS_TERMINATE_SNAPKEY =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.terminateSnapKey";

    public static final String CA_STATUS_VIG_WAKEUP_SUCCESS =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.vigWakeSuccess";

    public static final String CA_STATUS_VIG_WAKEUP_FAILED =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.vigWakeFailed";

    public static final String CA_STATUS_VIG_ONLINE =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.vigOnline";

    public static final String CA_STATUS_VIG_OFFLINE =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.vigOffline";

    public static final String CA_STATUS_MASTERKEY_DELETED =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.masterKeyDeleted";

    public static final String CA_STATUS_SUBKEY_ADDED =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.subKeyAdded";

    public static final String CA_STATUS_SUBKEY_DELETED =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.subKeyDeleted";

    public static final String CA_STATUS_SNAPKEY_ADDED =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.snapKeyAdded";

    public static final String CA_STATUS_SNAPKEY_DELETED =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.snapKeyDeleted";

    // 通知APP DEBUG訊息，正式版會移除
    static final String CA_STATUS_GET_DEBUG_MESSAGE =
            "tw.com.haitec.ae.project.smartphoneikey.CommunicationAgent.debugMessage";

    public static final String CA_STATUS_EXTRA_COMMAND = "command";
    public static final String CA_STATUS_EXTRA_RETURN_CODE = "returnCode";
    public static final String CA_EXTRA_IKEY_LOGIN = "doIkeyLogin";
    public static final String CA_EXTRA_REMOTE_CONTROL = "remoteControl";
    public static final String CA_EXTRA_COMMAND_VALUE = "commandValue";
    public static final String CA_EXTRA_STOP = "stopService";
    public static final String CA_EXTRA_START = "startService";
    public static final String CA_EXTRA_PING = "pingService";

    public static final int CA_EXTRA_LOGIN_SUCCESS_RETURN_CODE = 200;
    public static final int CA_EXTRA_MAX_LOGIN_RETRY_RETURN_CODE = 987;
    public static final int CA_EXTRA_DEVICE_TIMESTAMP_NOT_MATCH_RETURN_CODE = 600;
    public static final int CA_EXTRA_DEVICE_ID_NOT_MATCH_RETURN_CODE = 801;

    // debug flags
    private final static boolean DO_IKEYLOGIN_BEFORE_REMOTE_CONTROL = true;
    private final static boolean DO_IKEYLOGIN_BEFORE_KEY_OPERATION = true;
    private final static boolean USE_DEBUG_VERSION_CA = false;

    private CommunicationAgent ca = null;
    private boolean mBtReceiverRegistered = false;
    private boolean mInitialized = false;

    private boolean mNetConnReceiverRegistered = false;
    private boolean mVerboseLog = false;

    private ChannelsStatusListener mChannelsStatusListener = new ChannelsStatusListener() {

        @Override
        public void securityServerDataChannelHasData() {
            byPassToServer();
        }

        @Override
        public void bleDataChannelHasData() {
            processBleData();
        }

        @Override
        public void mainDataChannelHasData() {
            processMainData();
        }

        @Override
        public void badBleReception() {
            resetBleAndVig();
        }

        // 通知APP DEBUG訊息，正式版會移除
        public void getDebugMessage(String message) {
            broadcastUpdate(CA_STATUS_GET_DEBUG_MESSAGE, message);
        }
    };

    private void updateCAInternalData(IkeyLoginResponseData responseData) {
        if (responseData.getLoginSetting() != null) {
            LogCat.d("儲存 loginSetting");
            saveLoginSettings(getApplicationContext(), responseData.getLoginSetting());
        }
        if (responseData.getAppSecret() != null) {
            LogCat.d("回傳 appsecret 給 CA");
            sendAppSecretToCAlib(responseData.getAppSecret());
        }
    }

    private IKeyLoginRequest.Callback mIkeyLoginRequestNormalCallback = new IKeyLoginRequest.Callback() {

        @Override
        public void onSuccess(IkeyLoginResponseData responseData) {
            updateCAInternalData(responseData);
            broadcastUpdate(CA_STATUS_IKEY_LOGIN, CA_EXTRA_LOGIN_SUCCESS_RETURN_CODE);
        }

        @Override
        public void onFailure(int code) {
            broadcastUpdate(CA_STATUS_IKEY_LOGIN, code);
        }

        @Override
        public void onNetError(String message) {
            broadcastUpdate(CA_STATUS_IKEY_LOGIN, message);
        }
    };

    private IKeyLoginRequest.Callback mIkeyLoginRequestRemoteControlCallback = new IKeyLoginRequest.Callback() {

        @Override
        public void onSuccess(IkeyLoginResponseData responseData) {
            updateCAInternalData(responseData);
            LogCat.d("睡一秒之後再繼續");
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    doRemoteControl(mCommand, mCommandValue);
                }
            }, 1000);
        }

        @Override
        public void onFailure(int code) {
            onRemoteControlFailure();
        }

        @Override
        public void onNetError(String message) {
            onRemoteControlFailure();
        }
    };

    private void continueKeyOperationsAfterIkeyLogin() {
        if (mCAOperationType == OP_ACTIVATE_MASTERKEY) {
            if (!checkVigWakeUp(mOnCheckVigKeyOperationApiListener)) {
                onCheckVigKeyOperationFailure(null);
            }
        } else if (mCAOperationType == OP_DELETE_MASTERKEY) {
            if (!checkVigWakeUp(mOnCheckVigKeyOperationApiListener)) {
                onCheckVigKeyOperationFailure(null);
            }
        } else if (mCAOperationType == OP_ADD_SUBKEY) {
            String carId = getCurrentConnectedVigCarId(getApplicationContext());
            PrepareKeyOperationBean bean = getPrepareKeyOperationBean(mTargetSubKeyUid, mTargetSubKeyDeviceId,
                    carId, PrepareKeyOperationBean.OPERATION_ADD_SUB_KEY);
            if (bean == null) {
                ca.prepareAddSubKeyData(mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime,
                        carId, mPrepareAddSubKeyCommandCallback);
            } else {
                mCAOperationType = OP_ADD_SUBKEY_PREPARED;
                afterPrepareAddSubKeySuccess(bean);
            }
        } else if (mCAOperationType == OP_DEL_SUBKEY) {
            String carId = getCurrentConnectedVigCarId(getApplicationContext());
            PrepareKeyOperationBean bean = getPrepareKeyOperationBean(mTargetSubKeyUid, mTargetSubKeyDeviceId,
                    carId, PrepareKeyOperationBean.OPERATION_DELETE_SUB_KEY);
            if (bean == null) {
                ca.prepareDeleteSubKeyData(mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime,
                        carId, mPrepareDelSubKeyCommandCallback);
            } else {
                mCAOperationType = OP_DEL_SUBKEY_PREPARED;
                afterPrepareDelSubKeySuccess(bean);
            }
//            } else if (mCAOperationType == OP_RENEW_SNAPKEY) {
//                // no-op
        } else if (mCAOperationType == OP_DEL_ADD_PREPARE) {
            String carId = getCurrentConnectedVigCarId(getApplicationContext());
            PrepareKeyOperationBean bean = getPrepareKeyOperationBean(mTargetSubKeyUid, mTargetSubKeyDeviceId,
                    carId, PrepareKeyOperationBean.OPERATION_ADD_SUB_KEY);
            if (bean != null) {
                ca.deletePrepareData(bean, mDelSubKeyPrepareCommandCallback);
            } else {
                broadcastUpdate(CA_STATUS_DEL_SUBKEY, CA_EXTRA_DEL_SUBKEY_FAILED);
            }
        } else if (mCAOperationType == OP_DEL_DEL_PREPARE) {
            String carId = getCurrentConnectedVigCarId(getApplicationContext());
            PrepareKeyOperationBean bean = getPrepareKeyOperationBean(mTargetSubKeyUid, mTargetSubKeyDeviceId,
                    carId, PrepareKeyOperationBean.OPERATION_DELETE_SUB_KEY);
            if (bean != null) {
                ca.deletePrepareData(bean, mDelSubKeyPrepareCommandCallback);
            } else {
                broadcastUpdate(CA_STATUS_DEL_SUBKEY, CA_EXTRA_DEL_SUBKEY_FAILED);
            }
        } else if (mCAOperationType == OP_ALLOCATE_SNAPKEY) {
            if (!checkVigWakeUp(mOnCheckVigKeyOperationApiListener)) {
                onCheckVigKeyOperationFailure(null);
            }
        } else if (mCAOperationType == OP_TERMINATE_SNAPKEY) {
            if (!checkVigWakeUp(mOnCheckVigKeyOperationApiListener)) {
                onCheckVigKeyOperationFailure(null);
            }
        } else {
            broadcastUpdate(CA_STATUS_VIG_ONLINE);
            mCAOperationType = OP_NONE;
        }
    }

    private IKeyLoginRequest.Callback mIkeyLoginRequestKeyOperationCallback = new IKeyLoginRequest.Callback() {

        @Override
        public void onSuccess(IkeyLoginResponseData responseData) {
            updateCAInternalData(responseData);
            LogCat.d("睡一秒之後再繼續");
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    continueKeyOperationsAfterIkeyLogin();
                }
            }, 1000);
        }

        @Override
        public void onFailure(int code) {
            if (mCAOperationType == OP_ACTIVATE_MASTERKEY) {
                broadcastUpdate(CA_STATUS_ADD_MASTERKEY, CA_EXTRA_ADD_MASTERKEY_FAILED);
            } else if (mCAOperationType == OP_DELETE_MASTERKEY) {
                broadcastUpdate(CA_STATUS_DEL_MASTERKEY, CA_EXTRA_DEL_MASTERKEY_FAILED);
            } else if (mCAOperationType == OP_ADD_SUBKEY) {
                clearCarKeyInfo(mTargetSubKeyPosition, CarOptions.KEY_S1, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime());
                broadcastUpdate(CA_STATUS_ADD_SUBKEY, CA_EXTRA_ADD_SUBKEY_FAILED);
            } else if (mCAOperationType == OP_DEL_SUBKEY || mCAOperationType == OP_DEL_ADD_PREPARE || mCAOperationType == OP_DEL_DEL_PREPARE) {
                broadcastUpdate(CA_STATUS_DEL_SUBKEY, CA_EXTRA_DEL_SUBKEY_FAILED);
//            } else if (mCAOperationType == OP_RENEW_SNAPKEY) {
//                broadcastUpdate(CA_STATUS_RENEW_SNAPKEY, CA_EXTRA_RENEW_SNAPKEY_FAILED);
            } else if (mCAOperationType == OP_ALLOCATE_SNAPKEY) {
                broadcastUpdate(CA_STATUS_ALLOCATE_SNAPKEY, CA_EXTRA_ALLOCATE_SNAPKEY_FAILED);
            } else if (mCAOperationType == OP_TERMINATE_SNAPKEY) {
                broadcastUpdate(CA_STATUS_TERMINATE_SNAPKEY, CA_EXTRA_TERMINATE_SNAPKEY_FAILED);
            }
            mCAOperationType = OP_NONE;
        }

        @Override
        public void onNetError(String message) {
            if (mCAOperationType == OP_ACTIVATE_MASTERKEY) {
                broadcastUpdate(CA_STATUS_ADD_MASTERKEY, CA_EXTRA_ADD_MASTERKEY_FAILED);
            } else if (mCAOperationType == OP_DELETE_MASTERKEY) {
                broadcastUpdate(CA_STATUS_DEL_MASTERKEY, CA_EXTRA_DEL_MASTERKEY_FAILED);
            } else if (mCAOperationType == OP_ADD_SUBKEY) {
                clearCarKeyInfo(mTargetSubKeyPosition, CarOptions.KEY_S1, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime());
                broadcastUpdate(CA_STATUS_ADD_SUBKEY, CA_EXTRA_ADD_SUBKEY_FAILED);
            } else if (mCAOperationType == OP_DEL_SUBKEY || mCAOperationType == OP_DEL_ADD_PREPARE || mCAOperationType == OP_DEL_DEL_PREPARE) {
                broadcastUpdate(CA_STATUS_DEL_SUBKEY, CA_EXTRA_DEL_SUBKEY_FAILED);
//            } else if (mCAOperationType == OP_RENEW_SNAPKEY) {
//                broadcastUpdate(CA_STATUS_RENEW_SNAPKEY, CA_EXTRA_RENEW_SNAPKEY_FAILED);
            } else if (mCAOperationType == OP_ALLOCATE_SNAPKEY) {
                broadcastUpdate(CA_STATUS_ALLOCATE_SNAPKEY, CA_EXTRA_ALLOCATE_SNAPKEY_FAILED);
            } else if (mCAOperationType == OP_TERMINATE_SNAPKEY) {
                broadcastUpdate(CA_STATUS_TERMINATE_SNAPKEY, CA_EXTRA_TERMINATE_SNAPKEY_FAILED);
            }
            mCAOperationType = OP_NONE;
        }
    };

    private OnApiListener<IkeyFuncToServerResponseData> mOnIkeyFuncToServerApiListener = new OnApiListener<IkeyFuncToServerResponseData>() {
        @Override
        public void onApiTaskSuccessful(IkeyFuncToServerResponseData responseData) {
            if (responseData != null) {
                if (responseData.getAction() != null && responseData.getAction().getReturnCode() == 404) {
                    doIkeyLogin(mIkeyLoginRequestNormalCallback);
                } else if (!TextUtils.isEmpty(responseData.getAppSecret())) {
                    sendAppSecretToCAlib(responseData.getAppSecret());
                } else if (responseData.getKeyOperationProcedure() != null) {
                    KeyOperationProcedure keyOperationProcedure = responseData.getKeyOperationProcedure();
                    // 要確定對象是自己且完成
                    if (getIkeyUid(getApplicationContext()).equalsIgnoreCase(keyOperationProcedure.getToUserId()) &&
                            keyOperationProcedure.isCompleted()) {
                        if (keyOperationProcedure.isAddSubKey()) {
                            LogCat.d("副 key 新增完成回應");
                        } else if (keyOperationProcedure.isDelSubKey()) {
                            LogCat.d("副 key 刪除完成回應");
                        } else if (keyOperationProcedure.isAddSnapKey()) {
                            LogCat.d("臨時 key 新增完成回應");
                        } else if (keyOperationProcedure.isTerminateSnapKey()) {
                            LogCat.d("臨時 key 終結完成回應");
                        } else if (keyOperationProcedure.isDeleteMasterKey()) {
                            LogCat.d("主 key 刪除完成回應");
                        } // uploadAddedSubKey
                        mIsKeyChainUpdated = true;
                        broadcastUpdate(CA_STATUS_IKEY_UPDATE);
                    }
                } else if (responseData.isTimeout()) {
                    broadcastUpdate(CA_STATUS_IKEY_FUNCTOSERVER_TIMEOUT);
                }
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            // TODO: for now, not sure if we need to resend command
            if (!NetUtils.isConnectToInternet(getApplicationContext())) {
                return;
            }
            broadcastUpdate(CA_STATUS_IKEY_FUNCTOSERVER_TIMEOUT);
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {

        }
    };

    private String getCommandResult(String command, String retVal) {
        try {
            JSONObject jo = new JSONObject(retVal);
            return jo.getString(command);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private OnApiListener<IkeyRemoteControlResponseData> mOnIkeyRemoteControlApiListener = new OnApiListener<IkeyRemoteControlResponseData>() {
        @Override
        public void onApiTaskSuccessful(IkeyRemoteControlResponseData responseData) {
            mIsDoingRemoteControl = false;
            LogCat.d("returnValue:" + responseData.getReturnValue());
            if (responseData.isSuccess()) {
                String result = getCommandResult(mCommand, responseData.getReturnValue());
                if ("success".equals(result)) {
                    logControls(mCommand, VigOptions.REMOTE_CONTROL_FAR, VigOptions.REMOTE_CONTROL_OK);
                    showRemoteSuccessNotification(mCommand);
                    broadcastUpdate(CA_STATUS_REMOTE_CONTROL_DONE, mCommand);
                } else {
                    logControls(mCommand, VigOptions.REMOTE_CONTROL_FAR, VigOptions.REMOTE_CONTROL_FAIL);
                    if (REMOTE_COMMAND_ONE_CLICK_OPTIMIZE.equals(mCommand)) {
                        if ("rejected".equals(result)) {
                            showRemoteFailureNotification(mCommand, R.string.remote_control_op_error_rejected_optimize);
                            broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, R.string.remote_control_op_error_rejected_optimize);
                        } else {
                            showRemoteFailureNotification(mCommand, R.string.remote_control_op_error_special);
                            broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, mCommand, R.string.remote_control_op_error_special);
                        }
                    } else {
                        if ("unsupported".equals(result)) {
                            showRemoteFailureNotification(mCommand, R.string.remote_control_op_error_unsupported);
                            broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, mCommand, R.string.remote_control_op_error_unsupported);
                        } else if ("rejected".equals(result)) {
                            if (REMOTE_COMMAND_DOOR_UNLOCK.equals(mCommand)) {
                                showRemoteFailureNotification(mCommand, R.string.remote_control_op_error_rejected_unlock);
                                broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, mCommand, R.string.remote_control_op_error_rejected_unlock);
                            } else if (REMOTE_COMMAND_ENGINE_OFF.equals(mCommand)) {
                                showRemoteFailureNotification(mCommand, R.string.remote_control_op_error_rejected_engine_off);
                                broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, R.string.remote_control_op_error_rejected_engine_off);
                            } else {
                                showRemoteFailureNotification(mCommand, R.string.remote_control_op_error_rejected);
                                broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, mCommand, R.string.remote_control_op_error_rejected);
                            }
                        } else {
                            showRemoteFailureNotification(mCommand, -1);
                            broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, mCommand, -1);
                        }
                    }
                }
            } else {
                if (responseData.getReturnCode() == 404) {
                    mIsDoingRemoteControl = true;
                    doIkeyLogin(mIkeyLoginRequestRemoteControlCallback);
                } else {
                    logControls(mCommand, VigOptions.REMOTE_CONTROL_FAR, VigOptions.REMOTE_CONTROL_FAIL);
                    if (responseData.getReturnCode() == 650) {
                        showRemoteFailureNotification(mCommand, R.string.remote_control_op_error_offline);
                        broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, mCommand, R.string.remote_control_op_error_offline);
                    } else if (responseData.getReturnCode() == 500) {
                        showRemoteFailureNotification(mCommand, R.string.remote_control_op_error_server);
                        broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, mCommand, R.string.remote_control_op_error_server);
                    } else {
                        showRemoteFailureNotification(mCommand, 0);
                        broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, mCommand);
                    }
                }
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            mIsDoingRemoteControl = false;
            logControls(mCommand, VigOptions.REMOTE_CONTROL_FAR, VigOptions.REMOTE_CONTROL_FAIL);
            showRemoteFailureNotification(mCommand, 0);
            broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, mCommand);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {

        }
    };

    private OnApiListener<ResponseData> mOnSetCarControlLogApiListener = new OnApiListener<ResponseData>() {
        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {

        }

        @Override
        public void onApiTaskFailure(String failMessage) {

        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {

        }
    };

    private void saveLoginSettings(Context context, IkeyLoginSettingData ikeyLoginSettingData) {
        Prefs.setLoginSettings(context, ikeyLoginSettingData);
    }

    private void sendAppSecretToCAlib(String appSecret) {
        if (ca != null && appSecret != null) {
            ca.getSecurityServerDataChannel().write(appSecret.getBytes());
        }
    }

    private String getCurrentConnectedVigCarId(Context context) {
        CarKeyData carKeyData = Prefs.getCarKeyData(context);
        if (carKeyData == null || !carKeyData.isVig()) {
            return null;
        }
        return carKeyData.getVigCarId();
    }

    private String getCurrentCarId(Context context) {
        CarKeyData carKeyData = Prefs.getCarKeyData(context);
        if (carKeyData == null) {
            return null;
        }
        return carKeyData.getCarId();
    }

    private String getIkeyUid(Context context) {
        if (USE_DEBUG_VERSION_CA) {
            return "27932239";
        } else {
            return Prefs.getKeyUid(context);
        }
    }

    private String getDeviceId(Context context) {
        if (USE_DEBUG_VERSION_CA) {
            return "111122223333";
        } else {
            return Prefs.getDeviceId(context);
        }
    }

    private long getDeviceIdCreateTime(Context context) {
        return Prefs.getDeviceIdCreateTime(context);
    }

    private String getCurrentTimeString() {
        SimpleDateFormat mDateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.TAIWAN);
        Calendar calendar = Calendar.getInstance();
        mDateFormatter.setTimeZone(calendar.getTimeZone());
        return mDateFormatter.format(calendar.getTime());
    }

    private String getCurrentTime() {
        return String.valueOf(System.currentTimeMillis());
    }

    private void sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void doIkeyLogin(IKeyLoginRequest.Callback callback) {
        if (ca == null) {
            return;
        }

        Context context = this.getApplicationContext();
        String ikeyUid = getIkeyUid(context);
        String deviceId = getDeviceId(context);
        String deviceIdCreateTime = String.valueOf(getDeviceIdCreateTime(context));
        String appSecret = ca.genLoginData((i, hashMap) -> {
        });

        IKeyLoginRequest loginRequest = new IKeyLoginRequest(context,
                Prefs.getAvailableActionId(context, IKeyLoginRequest.MAX_RETRY),
                ikeyUid, appSecret, deviceId, deviceIdCreateTime);

        loginRequest.setCallback(callback);
        loginRequest.start();
    }

    private void iKeyFuncToServer(String appSecret) {
        Context context = this.getApplicationContext();
        int actionId = Prefs.getAvailableActionId(context);
        IkeyLoginSettingData loginSettingData = Prefs.getLoginSettings(context);
        IkeyFuncToServerRequestData requestData = new IkeyFuncToServerRequestData(actionId, appSecret, loginSettingData);

        IkeyWebApi ikeyWebApi = IkeyWebApi.getInstance(context);
        ikeyWebApi.funcToServer(requestData, mOnIkeyFuncToServerApiListener);
    }

    private boolean mIsWakingUpVig = false;
    private int mWakingUpVigCount = 0;
    private final static int MAX_WAKING_UP_VIG_COUNT = 6;

    private boolean mIsDoingRemoteControl = false;
    private String mCommand, mCommandValue;

    private MasterKeyDataBean getMasterKey(Context context, String carId) {
        String deviceId = getDeviceId(context);
        String uid = getIkeyUid(context);

        if (ca == null) {
            return null;
        }

        List<MasterKeyDataBean> masterKeyDataList = ca.getMasterKeyDataList();
        if (masterKeyDataList.size() > 0) {
            for (MasterKeyDataBean bean : masterKeyDataList) {
                if (bean.getUserId().equals(uid) && bean.getDeviceId().equals(deviceId) &&
                        bean.getCarId().equals(carId) && bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                    return bean;
                }
            }
        }
        return null;
    }

    private SubKeyDataBean getSubKey(Context context, String carId) {
        String deviceId = getDeviceId(context);
        String uid = getIkeyUid(context);

        if (ca == null) {
            return null;
        }

        List<SubKeyDataBean> subKeyDataList = ca.getSubKeyDataList();
        if (subKeyDataList.size() > 0) {
            for (SubKeyDataBean bean : subKeyDataList) {
                if (bean.getUserId().equals(uid) && bean.getDeviceId().equals(deviceId) &&
                        bean.getCarId().equals(carId) && bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                    return bean;
                }
            }
        }
        return null;
    }

    private SnapKeyDataBean getSnapKey(Context context, String carId) {
        String deviceId = getDeviceId(context);
        String uid = getIkeyUid(context);

        if (ca == null) {
            return null;
        }

        List<SnapKeyDataBean> snapKeyDataList = ca.getSnapKeyDataListByCarId(carId);
        if (snapKeyDataList.size() > 0) {
            for (SnapKeyDataBean bean : snapKeyDataList) {
                if (bean.getUserId().equals(uid) && bean.getDeviceId().equals(deviceId) &&
                        bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                    return bean;
                }
            }
        }
        return null;
    }

    private void doUnfinishedJobs(Context context) {
        if (ca == null) {
            return;
        }

        ArrayList<PrepareKeyOperationBean> prepareKeyOperationList = ca.getPrepareKeyOperationList();
        if (prepareKeyOperationList.size() > 0) {
            for (PrepareKeyOperationBean bean : prepareKeyOperationList) {
                if (bean.getCarId().equalsIgnoreCase(mCurrentConnectedVigCarId)) {
                    if (bean.getOperation().equalsIgnoreCase(PrepareKeyOperationBean.OPERATION_ADD_SUB_KEY)) {
                        mCAOperationType = OP_ADD_SUBKEY;
                        ca.addSubKeyThroughBle(bean, mAddSubKeyBLECommandCallback);
                    } else if (bean.getOperation().equalsIgnoreCase(PrepareKeyOperationBean.OPERATION_DELETE_SUB_KEY)) {
                        mCAOperationType = OP_DEL_SUBKEY;
                        ca.deleteSubKeyThroughBle(bean, mDelSubKeyBLECommandCallback);
                    }
                }
            }
        }
    }

    public boolean hasAnyKey() {
        Context context = this.getApplicationContext();
        CarKeyData carKeyData = Prefs.getCarKeyData(context);
        if (carKeyData == null) {
            return false;
        }

        String carId = carKeyData.getVigCarId();
        IKeyDataBean keyDataBean = null;
        if (CarOptions.KEY_M.equals(carKeyData.getKeyType()) || CarOptions.KEY_M2.equals(carKeyData.getKeyType())) {
            keyDataBean = getMasterKey(context, carId);
        } else if (CarOptions.KEY_S2.equals(carKeyData.getKeyType())) {
            keyDataBean = getSubKey(context, carId);
        } else if (CarOptions.KEY_T2.equals(carKeyData.getKeyType())) {
            keyDataBean = getSnapKey(context, carId);
        }

        return keyDataBean != null;
    }

    public String getCurrentRemoteControl() {
        if (mIsDoingRemoteControl || getVigState() == VIG_ISSUE_COMMAND) {
            return mCommand;
        }
        return null;
    }

    private void startRemoteControl(String command, String commandValue) {
        if (!NetUtils.isConnectToInternet(this)) {
            broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, R.string.dialog_message_no_internet);
            return;
        }
        if (mIsDoingRemoteControl) {
            broadcastUpdate(CA_STATUS_REMOTE_CONTROL_BLOCKED, command);
            return;
        }
        mIsDoingRemoteControl = true;
        LogCat.d("預備遠端控制:" + command + "," + commandValue);

        mCommand = command;
        mCommandValue = commandValue;

        if (!checkVigWakeUp(mOnCheckVigRemoteControlApiListener)) {
            onRemoteControlInternalFailure();
        }
    }

    private void doRemoteControl(String command, String commandValue) {
        Context context = this.getApplicationContext();
        CarKeyData carKeyData = Prefs.getCarKeyData(context);
        if (carKeyData == null) {
            mIsDoingRemoteControl = false;
            logControls(command, VigOptions.REMOTE_CONTROL_FAR, VigOptions.REMOTE_CONTROL_FAIL);
            showRemoteFailureNotification(command, -2);
            broadcastUpdate(CA_STATUS_REMOTE_CONTROL_INTERNAL_ERROR, command);
            return;
        }

        String carId = carKeyData.getVigCarId();
        IKeyDataBean keyDataBean = null;
        if (CarOptions.KEY_M.equals(carKeyData.getKeyType()) || CarOptions.KEY_M2.equals(carKeyData.getKeyType())) {
            keyDataBean = getMasterKey(context, carId);
        } else if (CarOptions.KEY_S2.equals(carKeyData.getKeyType())) {
            keyDataBean = getSubKey(context, carId);
        } else if (CarOptions.KEY_T2.equals(carKeyData.getKeyType())) {
            keyDataBean = getSnapKey(context, carId);
        }
        if (keyDataBean == null) {
            mIsDoingRemoteControl = false;
            logControls(command, VigOptions.REMOTE_CONTROL_FAR, VigOptions.REMOTE_CONTROL_FAIL);
            showRemoteFailureNotification(command, -2);
            broadcastUpdate(CA_STATUS_REMOTE_CONTROL_INTERNAL_ERROR, command);
            return;
        }

        if (ca == null) {
            mIsDoingRemoteControl = false;
            logControls(command, VigOptions.REMOTE_CONTROL_FAR, VigOptions.REMOTE_CONTROL_FAIL);
            showRemoteFailureNotification(command, -2);
            broadcastUpdate(CA_STATUS_REMOTE_CONTROL_INTERNAL_ERROR, command);
            return;
        }

        String inputCommandStr = String.format("{\"%s\":\"%s\"}", command, commandValue);
        String appSecret = ca.sendCommandThroughServer(keyDataBean, inputCommandStr, (i, hashMap) -> {
        });
        String timestamp = getCurrentTime();
        int actionId = Prefs.getAvailableActionId(context);
        CarProfileData carProfileData = new CarProfileData(carId);
        carProfileData.setCreateTimeStamp(timestamp);
        IkeyLoginSettingData loginSettingData = Prefs.getLoginSettings(context);
        IkeyRemoteControlRequestData data = new IkeyRemoteControlRequestData(actionId,
                command, commandValue, timestamp, appSecret, carProfileData, loginSettingData);

        IkeyWebApi ikeyWebApi = IkeyWebApi.getInstance(context);
        ikeyWebApi.remoteControl(data, mOnIkeyRemoteControlApiListener);
    }

    private void onRemoteControlFailure() {
        onRemoteControlFailure(false);
    }

    private void onRemoteControlFailure(boolean vigFailed) {
        logControls(mCommand, VigOptions.REMOTE_CONTROL_FAR, VigOptions.REMOTE_CONTROL_FAIL);
        if (vigFailed) {
            showRemoteFailureNotification(mCommand, -3);
            broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, mCommand, R.string.remote_control_op_vig_failed_message);
        } else {
            showRemoteFailureNotification(mCommand, 0);
            broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, mCommand);
        }
        mIsDoingRemoteControl = false;
        mIsWakingUpVig = false;
        mWakingUpVigCount = 0;
    }

    private void onRemoteControlInternalFailure() {
        logControls(mCommand, VigOptions.REMOTE_CONTROL_FAR, VigOptions.REMOTE_CONTROL_FAIL);
        showRemoteFailureNotification(mCommand, -2);
        broadcastUpdate(CA_STATUS_REMOTE_CONTROL_INTERNAL_ERROR, mCommand);
        mIsDoingRemoteControl = false;
        mIsWakingUpVig = false;
        mWakingUpVigCount = 0;
    }

    private OnApiListener<ResponseData> mOnWakeUpVigRemoteControlApiListener = new OnApiListener<ResponseData>() {
        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            if (responseData.isSuccessful()) {
                LogCat.d("喚醒成功，等待 10 秒");
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!checkVigWakeUp(mOnCheckVigRemoteControlApiListener)) {
                            onRemoteControlInternalFailure();
                        }
                    }
                }, 10 * 1000);
            } else {
                LogCat.d("喚醒失敗");
                onRemoteControlFailure(true);
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            onRemoteControlFailure(true);
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
        }
    };

    private void onWakeUpVigKeyOperationFailure(String message) {
        LogCat.d("喚醒 VIG 失敗：" + message);
        broadcastUpdate(CA_STATUS_VIG_WAKEUP_FAILED, message);
        mCAOperationType = OP_NONE;
        mIsWakingUpVig = false;
        mWakingUpVigCount = 0;
    }

    private void onCheckVigKeyOperationFailure(String message) {
        LogCat.d("檢查 VIG 狀態失敗：" + message);
        broadcastUpdate(CA_STATUS_VIG_WAKEUP_FAILED, message);
        mCAOperationType = OP_NONE;
        mIsWakingUpVig = false;
        mWakingUpVigCount = 0;
    }

    private OnApiListener<ResponseData> mOnWakeUpVigKeyOperationApiListener = new OnApiListener<ResponseData>() {
        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            if (responseData.isSuccessful()) {
                mWakingUpVigCount++;
                LogCat.d("喚醒成功，等待 10 秒");
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!checkVigWakeUp(mOnCheckVigKeyOperationApiListener)) {
                            onCheckVigKeyOperationFailure(null);
                        }
                    }
                }, 10 * 1000);
            } else {
                LogCat.d("喚醒失敗");
                onWakeUpVigKeyOperationFailure(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            onWakeUpVigKeyOperationFailure(failMessage);
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
        }
    };

    private boolean wakeUpVig(OnApiListener<ResponseData> listener) {
        LogCat.d("喚醒 VIG");
        Context context = this.getApplicationContext();
        UserInfo userInfo = Prefs.getUserInfo(context);
        CarKeyData carKeyData = Prefs.getCarKeyData(context);
        if (carKeyData == null || userInfo == null) {
            return false;
        }

        WakeupVigRequestData data = new WakeupVigRequestData(userInfo.getKeyUid(), userInfo.getToken(), carKeyData.getCarId());
        data.setPhoneId(userInfo.getDeviceId());

        CarWebApi carWebApi = CarWebApi.getInstance(context);
        carWebApi.wakeUpVig(data, listener);

        return true;
    }

    private OnApiListener<GetWakeupVigResultResponseData> mOnCheckVigRemoteControlApiListener = new OnApiListener<GetWakeupVigResultResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetWakeupVigResultResponseData responseData) {
            if (responseData.isSuccessful()) {
                LogCat.d("VIG 狀態檢查成功");
                if (VigOptions.WAKE_UP_SUCCESS.equals(responseData.getResult())) {
                    LogCat.d("VIG 已喚醒");
                    mIsWakingUpVig = false;
                    mWakingUpVigCount = 0;

                    if (DO_IKEYLOGIN_BEFORE_REMOTE_CONTROL) {
                        doIkeyLogin(mIkeyLoginRequestRemoteControlCallback);
                    } else {
                        doRemoteControl(mCommand, mCommandValue);
                    }
                } else {
                    LogCat.d("VIG 未喚醒");
                    if (!mIsWakingUpVig) {
                        mIsWakingUpVig = true;
                        if (!wakeUpVig(mOnWakeUpVigRemoteControlApiListener)) {
                            onRemoteControlInternalFailure();
                        }
                    } else {
                        if (mWakingUpVigCount++ > MAX_WAKING_UP_VIG_COUNT) {
                            LogCat.d("超過喚醒上限");
                            onRemoteControlFailure(true);
                            return;
                        }

                        LogCat.d("等待 10 秒");
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (!checkVigWakeUp(mOnCheckVigRemoteControlApiListener)) {
                                    onRemoteControlInternalFailure();
                                }
                            }
                        }, 10 * 1000);
                    }
                }
            } else {
                LogCat.d("VIG 狀態檢查失敗");
                onRemoteControlFailure(true);
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            onRemoteControlFailure(true);
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
        }
    };

    private OnApiListener<GetWakeupVigResultResponseData> mOnCheckVigKeyOperationApiListener = new OnApiListener<GetWakeupVigResultResponseData>() {

        private void continueKeyOperations() {
            if (mCAOperationType == OP_ACTIVATE_MASTERKEY) {
                String carId = getCurrentConnectedVigCarId(getApplicationContext());
                ca.addMasterKey(carId, mAddMasterKeyCommandCallback);
            } else if (mCAOperationType == OP_DELETE_MASTERKEY) {
                if (!doDeleteMasterKey()) {
                    broadcastUpdate(CA_STATUS_DEL_MASTERKEY, CA_EXTRA_DEL_MASTERKEY_FAILED);
                    mCAOperationType = OP_NONE;
                }
            } else if (mCAOperationType == OP_ADD_SUBKEY) {
                String carId = getCurrentConnectedVigCarId(getApplicationContext());
                PrepareKeyOperationBean bean = getPrepareKeyOperationBean(mTargetSubKeyUid, mTargetSubKeyDeviceId,
                        carId, PrepareKeyOperationBean.OPERATION_ADD_SUB_KEY);
                ca.addSubKeyThroughServer(bean, mAddSubKeyServerCommandCallback);
            } else if (mCAOperationType == OP_DEL_SUBKEY) {
                String carId = getCurrentConnectedVigCarId(getApplicationContext());
                PrepareKeyOperationBean bean = getPrepareKeyOperationBean(mTargetSubKeyUid, mTargetSubKeyDeviceId,
                        carId, PrepareKeyOperationBean.OPERATION_DELETE_SUB_KEY);
                ca.deleteSubKeyThroughServer(bean, mDelSubKeyServerCommandCallback);
            } else if (mCAOperationType == OP_RENEW_SNAPKEY) {
                if (checkIfCanIssueSnapkeyCommand()) {
                    SnapTicket snapTicket = getEmptySnapTicket();
                    if (snapTicket != null) {
                        ca.renewSnapTicket(snapTicket, mRenewSnapKeyCommandCallback);
                        return;
                    }
                }
                broadcastUpdate(CA_STATUS_RENEW_SNAPKEY, CA_EXTRA_RENEW_SNAPKEY_FAILED);
                mCAOperationType = OP_NONE;
            } else if (mCAOperationType == OP_ALLOCATE_SNAPKEY) {
                String carId = getCurrentConnectedVigCarId(getApplicationContext());
                IKeyDataBean keyDataBean = getMasterKey(getApplicationContext(), carId);
                SnapTicket snapTicket = getAvailableSnapTicket(keyDataBean);
                if (snapTicket == null) {
                    broadcastUpdate(CA_STATUS_ALLOCATE_SNAPKEY, CA_EXTRA_ALLOCATE_SNAPKEY_FAILED);
                    mCAOperationType = OP_NONE;
                } else {
                    ca.allocateSnapKey(mTargetSnapKeyUid, mTargetSnapKeyDeviceId, mTargetSnapKeyCreateTime,
                            keyDataBean, snapTicket, mTargetSnapKeyStartTime, mTargetSnapKeyEndTime, mAllocateSnapKeyCommandCallback);
                }
            } else if (mCAOperationType == OP_TERMINATE_SNAPKEY) {
                String carId = getCurrentConnectedVigCarId(getApplicationContext());
                IKeyDataBean keyDataBean = getMasterKey(getApplicationContext(), carId);
                SnapTicket snapTicket = getSnapTicket(keyDataBean, mTargetSnapKeyUid, mTargetSnapKeyDeviceId, mTargetSnapKeyCreateTime.getTime());
                if (snapTicket == null) {
                    broadcastUpdate(CA_STATUS_TERMINATE_SNAPKEY, CA_EXTRA_TERMINATE_SNAPKEY_FAILED);
                    mCAOperationType = OP_NONE;
                } else {
                    ca.terminateSnapKey(mTargetSnapKeyUid, mTargetSnapKeyDeviceId, mTargetSnapKeyCreateTime,
                            keyDataBean, snapTicket, mTerminateSnapKeyCommandCallback);
                }
            } else {
                broadcastUpdate(CA_STATUS_VIG_ONLINE);
                mCAOperationType = OP_NONE;
            }
        }

        @Override
        public void onApiTaskSuccessful(GetWakeupVigResultResponseData responseData) {
            if (responseData.isSuccessful()) {
                LogCat.d("VIG 狀態檢查成功");
                if (VigOptions.WAKE_UP_SUCCESS.equals(responseData.getResult())) {
                    LogCat.d("VIG 已喚醒");
                    mIsWakingUpVig = false;
                    mWakingUpVigCount = 0;

                    broadcastUpdate(CA_STATUS_VIG_WAKEUP_SUCCESS);
                    continueKeyOperations();
                } else {
                    LogCat.d("VIG 未喚醒");
                    if (!mIsWakingUpVig) {
                        mIsWakingUpVig = true;
                        if (!wakeUpVig(mOnWakeUpVigKeyOperationApiListener)) {
                            onWakeUpVigKeyOperationFailure(responseData.getErrorMessage());
                        }
                    } else {
                        if (mWakingUpVigCount++ > MAX_WAKING_UP_VIG_COUNT) {
                            LogCat.d("超過喚醒上限");
                            onCheckVigKeyOperationFailure(responseData.getErrorMessage());
                            return;
                        }

                        LogCat.d("等待 10 秒");
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (!checkVigWakeUp(mOnCheckVigKeyOperationApiListener)) {
                                    onCheckVigKeyOperationFailure(responseData.getErrorMessage());
                                }
                            }
                        }, 10 * 1000);
                    }
                }
            } else {
                LogCat.d("VIG 狀態檢查失敗");
                onCheckVigKeyOperationFailure(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            onCheckVigKeyOperationFailure(failMessage);
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
        }
    };

    private boolean checkVigWakeUp(OnApiListener<GetWakeupVigResultResponseData> listener) {
        LogCat.d("檢查 VIG 是否喚醒");
        Context context = this.getApplicationContext();
        UserInfo userInfo = Prefs.getUserInfo(context);
        CarKeyData carKeyData = Prefs.getCarKeyData(context);
        if (carKeyData == null || userInfo == null) {
            return false;
        }

        GetWakeupVigResultRequestData data = new GetWakeupVigResultRequestData(userInfo.getKeyUid(), userInfo.getToken(), carKeyData.getCarId());

        CarWebApi carWebApi = CarWebApi.getInstance(context);
        carWebApi.getWakeUpVigResult(data, listener);

        return true;
    }

    private boolean doNotLog(String command) {
        if ("BeaconRSSI".equalsIgnoreCase(command)) {
            return true;
        }

        if ("partno".equalsIgnoreCase(command)) {
            return true;
        }

        return "version".equalsIgnoreCase(command);

    }

    private void logControls(String command, String type, String result) {
        Context context = this.getApplicationContext();
        UserInfo userInfo = Prefs.getUserInfo(context);
        CarKeyData carKeyData = Prefs.getCarKeyData(context);
        if (carKeyData == null || userInfo == null) {
            return;
        }

        SetCarControlLogRequestData data = new SetCarControlLogRequestData(
                userInfo.getKeyUid(), userInfo.getToken(),
                carKeyData.getCarId(), carKeyData.getCarNo(), carKeyData.getVigCarId(),
                command, getCurrentTimeString()
        );
        data.setResult(result);
        data.setType(type);

        CarWebApi carWebApi = CarWebApi.getInstance(context);
        carWebApi.setCarControlLog(data, mOnSetCarControlLogApiListener);
    }

    private void byPassToServer() {
        if (ca == null) {
            return;
        }

        DataChannel channel = ca.getSecurityServerDataChannel();
        while (channel.hasData()) {
            byte[] data = channel.take();
            if (data != null) {
                iKeyFuncToServer(new String(data));
            }
        }
    }

    private Intent mLastResult = null;

    public Intent getLastResult() {
        Intent ret = mLastResult;

        mLastResult = null;

        return ret;
    }

    private void broadcastUpdate(final String action) {
        broadcastUpdate(action, null);
    }

    private void broadcastUpdate(final String action, String command) {
        final Intent intent = new Intent(action);
        if (command != null) {
            intent.putExtra(CA_STATUS_EXTRA_COMMAND, command);
        }
        mLastResult = intent;
        LocalBroadcastManager.getInstance(this.getApplicationContext()).sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action, int returnCode) {
        final Intent intent = new Intent(action);
        intent.putExtra(CA_STATUS_EXTRA_RETURN_CODE, returnCode);
        mLastResult = intent;
        LocalBroadcastManager.getInstance(this.getApplicationContext()).sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action, String command, int returnCode) {
        final Intent intent = new Intent(action);
        intent.putExtra(CA_STATUS_EXTRA_COMMAND, command);
        intent.putExtra(CA_STATUS_EXTRA_RETURN_CODE, returnCode);
        mLastResult = intent;
        LocalBroadcastManager.getInstance(this.getApplicationContext()).sendBroadcast(intent);
    }

    public CommunicationAgent getCA() {
        return this.ca;
    }

    public class LocalBinder extends Binder {
        public CommunicationAgentService getService() {
            return CommunicationAgentService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new LocalBinder();
    }

    private final static int ASK_UPDATE_FOREGROUND_POLLING_TIME = 15 * 1000; // 15 seconds
    private boolean bIsForeground = false;
    final Runnable changeToDefaultPollingRunnable = new Runnable() {
        @Override
        public void run() {
            if (ca != null) {
                ca.setServerPollingPeriodTime(0);
                bIsForeground = false;
                LogCat.d("休眠中");
                LogCat.d("預設");
            }
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            if (intent.hasExtra(CA_EXTRA_IKEY_LOGIN)) {
                startService();
                doIkeyLogin(mIkeyLoginRequestNormalCallback);
            } else if (intent.hasExtra(CA_EXTRA_REMOTE_CONTROL)) {
                String command = intent.getStringExtra(CA_EXTRA_REMOTE_CONTROL);
                String commandValue = intent.getStringExtra(CA_EXTRA_COMMAND_VALUE);
                startRemoteControl(command, commandValue);
            } else if (intent.hasExtra(VIG_BLE_COMMAND)) {
                String command = intent.getStringExtra(VIG_BLE_COMMAND);
                String commandValue = intent.getStringExtra(VIG_BLE_COMMAND_VALUE);
                startBleCommand(command, commandValue);
            } else if (intent.hasExtra(VIG_SCAN_BEACONS)) {
                boolean start = intent.getBooleanExtra(VIG_SCAN_BEACONS, true);
                // scanForBeacons(start);
            } else if (intent.hasExtra(VIG_SCAN_VIG)) {
                if (mCurrentConnectedVigCarId == null ||
                        !mCurrentConnectedVigCarId.equals(getCurrentConnectedVigCarId(getApplicationContext()))) {
                    closeBleAndCaConnections();
                    scanLeDevice(false);
                    scanLeDevice(true);
                }
            } else if (intent.hasExtra(CA_EXTRA_STOP)) {
                stopService();
            } else if (intent.hasExtra(CA_EXTRA_PING)) {
                boolean foreground = intent.getBooleanExtra(CA_EXTRA_PING, false);
                doHeavyPolling(foreground);
            }
        }
        return START_STICKY;
    }

    public void doHeavyPolling(boolean enable) {
        if (enable) {
            mHandler.removeCallbacks(changeToDefaultPollingRunnable);
            if (ca != null) {
                LogCat.d("使用中");
                if (!bIsForeground) {
                    bIsForeground = ca.setServerPollingPeriodTime(ASK_UPDATE_FOREGROUND_POLLING_TIME);
                    LogCat.d("加強");
                }
            }
        } else {
            mHandler.postDelayed(changeToDefaultPollingRunnable, 5000);
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent intent = new Intent(getApplicationContext(), CommunicationAgentService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, 1, intent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 100, pendingIntent);
        super.onTaskRemoved(rootIntent);
    }

    private final static int WATCHDOG_INTERVAL = 30 * 1000; // 30 seconds
    private Runnable mWatchDogRunnable = new Runnable() {
        @Override
        public void run() {
            String msg = "CA service 還活著。";
            if (ca != null) {
                msg = msg + "CA vc status:" + ca.getVehicleCommunicationStatus();
            }
            msg = msg + ", VIG status: " + getVigStateStr() + ", BLE state: " + mBleConnectionState;
            LogCat.d(msg);
            if (mHandler != null) {
                mHandler.postDelayed(mWatchDogRunnable, WATCHDOG_INTERVAL);
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        if (mHandlerThread == null) {
            mHandlerThread = new HandlerThread("CommunicationAgent");
            mHandlerThread.start();
        }
        mHandler = new Handler(mHandlerThread.getLooper());

        if (stbQueueHandler == null) {
            stbQueueHandler = new SendToBleQueueHandler();
            stbQueueHandler.start();
        }
        if (stcaQueueHandler == null) {
            stcaQueueHandler = new SendToCAQueueHandler();
            stcaQueueHandler.start();
        }
        startService();

        mHandler.postDelayed(mWatchDogRunnable, WATCHDOG_INTERVAL);
        queryMasterKeyDeletionStatus();
        LogCat.d("CA service 正常運轉");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopService();

        if (stcaQueueHandler != null) {
            stcaQueueHandler.terminate();
            stcaQueueHandler = null;
        }

        if (stbQueueHandler != null) {
            stbQueueHandler.terminate();
            stbQueueHandler = null;
        }

        mHandler.removeCallbacks(mWatchDogRunnable);
        if (mHandlerThread != null) {
            mHandlerThread.quit();
            mHandlerThread = null;
        }
        LogCat.d("CA service 已經死了。");

    }

    private void startService() {
        if (mInitialized) {
            return;
        }

        init();

        if (mBleManager == null) {
            mBleManager = new BleManager(this);
            mBleManager.scanForVig(true);
            mBleManager.setScanPeriod(-1);
            mBleManager.setFilterManufacturer(89, Prefs.getBeaconUUID(this));
            mBleManager.setScanResultCallback(mScanResultCallback);
        }

        if (mBleManager.isOpenBluetoothForAll()) {
            connectToObu();
        }

        if (!mBtReceiverRegistered) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
            intentFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
            intentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
            registerReceiver(mBluetoothStateReceiver, intentFilter);
            mBtReceiverRegistered = true;
        }

        if (!mNetConnReceiverRegistered) {
            mIsStartedWithNetwork = NetUtils.isConnectToInternet(this);
            mFirstTimeConnectingToNetwork = true;
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                NetworkRequest request = new NetworkRequest.Builder()
                        .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                        .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                        .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                        .build();
                cm.registerNetworkCallback(request, mNetworkCallback);
            }

            mNetConnReceiverRegistered = true;
        }

        mInitialized = true;
    }

    private void stopService() {
        if (!mInitialized) {
            return;
        }

        closeBleAndCaConnections();
        reset();

        if (mObuService != null) {
            mObuService.removeStatusListener(mObuListener);
        }

        if (mBtReceiverRegistered) {
            unregisterReceiver(mBluetoothStateReceiver);
            mBtReceiverRegistered = false;
        }

        if (mNetConnReceiverRegistered) {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                cm.unregisterNetworkCallback(mNetworkCallback);
            }
            mNetConnReceiverRegistered = false;
            mIsStartedWithNetwork = NetUtils.isConnectToInternet(this);
            mFirstTimeConnectingToNetwork = true;
        }

        scanLeDevice(false);
        bIsForeground = false;

        mInitialized = false;
    }

    private boolean mIsKeyChainUpdated = false;

    public boolean isKeyChainUpdated() {
        if (mIsKeyChainUpdated) {
            mIsKeyChainUpdated = false;
            return true;
        }

        return false;
    }

    private boolean isKey(IKeyDataBean keyDataBean, String keyRole, String status) {
        return keyDataBean != null && keyDataBean.getUserId().equals(getIkeyUid(getApplicationContext())) &&
                keyDataBean.getDeviceId().equalsIgnoreCase(getDeviceId(getApplicationContext())) &&
                keyDataBean.getKeyRole().equals(keyRole) &&
                keyDataBean.getStatus().equals(status);
    }

    private boolean isAddSubKey(IKeyDataBean keyDataBean) {
        return isKey(keyDataBean, SUB_KEY, STATUS_NORMAL);
    }

    private boolean isDelSubKey(IKeyDataBean keyDataBean) {
        return isKey(keyDataBean, SUB_KEY, "deleted");
    }

    private boolean isDelMasterKey(IKeyDataBean keyDataBean) {
        return isKey(keyDataBean, MASTER_KEY, "deleted");
    }

    private boolean isAddSnapKey(IKeyDataBean keyDataBean) {
        return isKey(keyDataBean, SNAP_KEY, STATUS_NORMAL);
    }

    private boolean isDelSnapKey(IKeyDataBean keyDataBean) {
        return isKey(keyDataBean, SNAP_KEY, "deleted");
    }

    private final CommandCallback mIkeyUpdateNotificationCommandCallback = new CommandCallback() {
        @Override
        public void commandComplete(int i, HashMap hashMap) {
            LogCat.d("收到 ikey update notification");
            if (hashMap.containsKey("iKeyDataBean")) {
                IKeyDataBean keyDataBean = (IKeyDataBean) hashMap.get("iKeyDataBean");
                if (isAddSubKey(keyDataBean)) {
                    LogCat.d("收到 subkey 通知");
                    broadcastUpdate(CA_STATUS_SUBKEY_ADDED, keyDataBean.getCarId());
                } else if (isDelSubKey(keyDataBean)) {
                    LogCat.d("刪除 subkey 通知");
                    broadcastUpdate(CA_STATUS_SUBKEY_DELETED, keyDataBean.getCarId());
                } else if (isDelMasterKey(keyDataBean)) {
                    LogCat.d("刪除 masterkey 通知");
                    broadcastUpdate(CA_STATUS_MASTERKEY_DELETED, keyDataBean.getCarId());
                } else if (isAddSnapKey(keyDataBean)) {
                    LogCat.d("新增 snapkey 通知");
                    broadcastUpdate(CA_STATUS_SNAPKEY_ADDED, keyDataBean.getCarId());
                } else if (isDelSnapKey(keyDataBean)) {
                    LogCat.d("刪除 snapkey 通知");
                    broadcastUpdate(CA_STATUS_SNAPKEY_DELETED, keyDataBean.getCarId());
                }
                // TODO: Snapkey
            }
        }
    };

    private void init() {
        if (ca == null) {
            LogCat.d("========== init ==========");
            Context context = this.getApplicationContext();
            String ikeyUid = getIkeyUid(context);
            String deviceId = getDeviceId(context);
            Timestamp timestamp = new Timestamp(getDeviceIdCreateTime(context));
            ca = new CommunicationAgent(context, CA_BLE_PACKAGE_LENGTH, ikeyUid, deviceId, timestamp, mChannelsStatusListener);
            mVerboseLog = true; // Prefs.shouldShowCaLog(context);
            if (mVerboseLog) {
                ca.setShowLog();
            }
            ca.setIKeyUpdateNotification(mIkeyUpdateNotificationCommandCallback);
        }
    }

    private void reset() {
        LogCat.d("========== reset ==========");
        if (ca != null) {
            ca.reset();
            ca.close();
            ca = null;
        }
    }

    public final static String REMOTE_COMMAND_OPEN_BACKDOOR = "bdooron";
    public final static String REMOTE_COMMAND_DOOR_UNLOCK = "doorunlock";
    public final static String REMOTE_COMMAND_DOOR_LOCK = "doorlock";
    public final static String REMOTE_COMMAND_FIND_CAR = "honk";
    public final static String REMOTE_COMMAND_ONE_CLICK_OPTIMIZE = "AcTemp";
    public final static String REMOTE_COMMAND_ENGINE_OFF = "engineoff";

    private Handler mHandler;
    private HandlerThread mHandlerThread;
    private final static int BLE_QUEUE_QUERY_TIME_MS = 200; // time to check queue
    private final static int BLE_QUEUE_PROCESS_PAUSE_TIME_MS = 15; // pause between each packet
    private final static int BLE_QUEUE_MAX_PACKETS_PAUSE_TIME_MS = 50; // pause between 40 packets
    private final static int BLE_QUEUE_RESET_COUNT_TIME_MS = 1000; // when to reset
    private final static int BLE_QUEUE_PEEK_TIME_MS = 50; // pause when got no data
    private SendToBleQueueHandler stbQueueHandler;
    private SendToCAQueueHandler stcaQueueHandler;

    private BleManager mBleManager;
    private BluetoothGatt mBluetoothGatt;
    private String mBluetoothDeviceAddress;

    private final static int STATE_DISCONNECTED = 0;
    private final static int STATE_CONNECTING = 1;
    private final static int STATE_CONNECTED = 2;
    private int mBleConnectionState = STATE_DISCONNECTED;

    public static final String RECEIVER = "com.luxgen.remote.RECEIVER.";
    public final static String VIG_BLE_COMMAND = "VIG_BLE_COMMAND";
    public final static String VIG_BLE_COMMAND_VALUE = "VIG_BLE_COMMAND_VALUE";
    public final static String VIG_SCAN_BEACONS = "VIG_SCAN_BEACONS";
    public final static String VIG_SCAN_VIG = "VIG_SCAN_VIG";
    public final static String VIG_STATUS_COMMAND_TIMEOUT = RECEIVER + "VIG_STATUS_COMMAND_TIMEOUT";

    public final static int VIG_INTERNAL_ERROR = -1;
    public final static int VIG_COMMAND_TIMEOUT = -2;
    public final static int VIG_DISCONNECTED = 0;
    public final static int VIG_CONNECTING = 1;
    public final static int VIG_CONNECTED = 2;
    public final static int VIG_BY_PASSING = 3;
    public final static int VIG_REQUEST_CONTROL = 4;
    public final static int VIG_READY_TO_GO = 5;
    public final static int VIG_ISSUE_COMMAND = 6;
    private int mVigConnectionState = VIG_DISCONNECTED;
    private String mCurrentConnectedVigCarId;

    private static int retryConnectToVehicle = 0;
    private final static int MAX_RETRY_COUNT = 5;
    private final static int VIG_COMMAND_TIMEOUT_SEC = 5 * 1000; // 5seconds timeout
    private final static int VIG_COMMAND_TIMEOUT_LONG_SEC = 30 * 1000; // 30seconds timeout

    private final static UUID CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    private final static UUID RX_SERVICE_UUID = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
    private final static UUID RX_CHAR_UUID = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
    private final static UUID TX_CHAR_UUID = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");

    private Beacon mLeftBeacon, mRightBeacon, mTrunkBeacon;

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                LogCat.d("mGattCallback: STATE_CONNECTED");
                mBleConnectionState = STATE_CONNECTED;
                setVigState(VIG_CONNECTED);
                gatt.discoverServices();
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                LogCat.d("mGattCallback: STATE_DISCONNECTED");
                closeBleAndCaConnections();
                if (mBleManager != null && mBleManager.isOpenBluetooth()) {
                    scanLeDevice(false);
                    scanLeDevice(true);
                }
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                doVigByPass(gatt);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            // non-op
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            byte[] rxData = characteristic.getValue();
            if (rxData != null) {
                sendToCABleDataChannel(rxData);
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            // non-op
        }
    };

    private BleManager.ScanResultCallback mScanResultCallback = new BleManager.ScanResultCallback() {
        @Override
        public void onReceiveBeacon(Beacon beacon) {
            if (!isVigReady()) {
                return;
            }

            if (beacon.equals(mRightBeacon)) {
                mRightBeacon = beacon;
            } else if (beacon.equals(mLeftBeacon)) {
                mLeftBeacon = beacon;
            } else if (beacon.equals(mTrunkBeacon)) {
                mTrunkBeacon = beacon;
            }
        }

        @Override
        public void onReceiveVig(BluetoothDevice device) {
            if (getVigState() != VIG_DISCONNECTED) {
                return;
            }

            LogCat.d("ADDRESS:" + device.getAddress());
            String[] address = device.getAddress().split(":");
            if (!device.getName().endsWith(address[4] + address[5])) {
                return;
            }

            setVigState(VIG_CONNECTING);
            if (!connect(device)) {
                setVigState(VIG_DISCONNECTED);
            }
        }

        @Override
        public void onTimeout() {

        }
    };

    private LuxgenLinkService mObuService = null;
    private ObuManager mObuManager = null;
    private ObuMessageReceiver mObuRceiver = null;
    private StatusListener mObuListener = new StatusListener() {

        @Override
        public void onRemoteError(int error, String msg) {
            super.onRemoteError(error, msg);
            LogCat.d("onRemoteError:" + error + "," + msg);
        }

        @Override
        public void onRemoteDisconnected() {
            super.onRemoteDisconnected();
            LogCat.d("onRemoteDisconnected");
        }

        @Override
        public void onRemoteConnected() {
            super.onRemoteConnected();
            LogCat.d("onRemoteConnected");
        }
    };

    private final BroadcastReceiver mBluetoothStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        LogCat.d("BT: STATE_TURNING_OFF");
                        scanLeDevice(false);
                        closeBleAndCaConnections();
                        break;

                    case BluetoothAdapter.STATE_ON:
                        LogCat.d("BT: STATE_ON");
                        scanLeDevice(false);
                        scanLeDevice(true);
                        break;

                    default:
                        break;
                }
            } else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                LogCat.d("ACTION_ACL_CONNECTED");
                connectToObu();
            } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                LogCat.d("ACTION_ACL_DISCONNECTED");
            }
        }
    };

    private void connectToObu() {
        if (LuxgenLinkService.isServiceAvailable(getApplicationContext())) {
            LogCat.d("connectToObu");
            mObuManager = ObuManager.getInstance(getApplicationContext());
            mObuRceiver = new ObuMessageReceiver(mObuManager, getApplicationContext());

            mObuService = LuxgenLinkService.getInstance(getApplicationContext());
            mObuService.addStatusListener(mObuListener);
            mObuService.setMessageReceiver(mObuRceiver);
        }
    }

    public void syncPushMessage(ArrayList<PushData> list) {
        if (mObuManager != null && mObuService != null && mObuService.isRemoteConnected()) {
            LogCat.d("syncPushMessage");
            mObuManager.syncPushList(list);
        }
    }

    private boolean mFirstTimeConnectingToNetwork = true;
    private boolean mIsStartedWithNetwork = false;

    private final ConnectivityManager.NetworkCallback mNetworkCallback = new ConnectivityManager.NetworkCallback() {

        private boolean mConnected = false;

        private boolean getNetworkStatus(Context context) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                for (Network availableNetwork : cm.getAllNetworks()) {
                    NetworkInfo activeNetwork = cm.getNetworkInfo(availableNetwork);
                    if (activeNetwork != null && (activeNetwork.isConnected() || activeNetwork.isConnectedOrConnecting())) {
                        return true;
                    }
                }
            }

            return false;
        }

        private boolean getNetworkStatus(Context context, Network network) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                NetworkInfo activeNetwork = cm.getNetworkInfo(network);
                if (null != activeNetwork) {
                    return activeNetwork.isConnected() || activeNetwork.isConnectedOrConnecting();
                }
            }

            return false;
        }

        @Override
        public void onLost(Network network) {
            super.onLost(network);
            LogCat.d("mNetworkCallback: onLost");
            LogCat.d("getNetworkStatus:" + getNetworkStatus(getApplicationContext()));
            mConnected = getNetworkStatus(getApplicationContext());
        }

        @Override
        public void onAvailable(Network network) {
            super.onAvailable(network);
            LogCat.d("mNetworkCallback: onAvailable");
            LogCat.d("getNetworkStatus:" + getNetworkStatus(getApplicationContext(), network));
            if (!mIsStartedWithNetwork) {
                mFirstTimeConnectingToNetwork = false;
            }
            if (mFirstTimeConnectingToNetwork) {
                mFirstTimeConnectingToNetwork = false;
            } else {
                if (!mConnected) {
                    mConnected = getNetworkStatus(getApplicationContext(), network);
                    if (mConnected) {
                        doIkeyLogin(mIkeyLoginRequestNormalCallback);
                    }
                }
            }
        }
    };

    private boolean connect(final BluetoothDevice device) {
        String address = device.getAddress();

        if (address.equals(mBluetoothDeviceAddress) && mBluetoothGatt != null) {
            LogCat.d("Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mBleConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }

        Context context = this.getApplicationContext();
        CarKeyData carKeyData = Prefs.getCarKeyData(context);
        if (carKeyData == null) {
            return false;
        }

        String currentVigName = carKeyData.getVigName();
        if (currentVigName.equals(device.getName())) {
            mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
            mBluetoothDeviceAddress = address;
            mBleConnectionState = STATE_CONNECTING;

            return true;
        }
        return false;
    }

    private void bleDisconnect() {
        if (mBluetoothGatt != null) {
            mBluetoothGatt.disconnect();
        }
    }

    private void bleClose() {
        if (mBluetoothGatt != null) {
            mBluetoothGatt.close();
            mBluetoothGatt = null;
            mBluetoothDeviceAddress = null;
        }
    }

    private void caDisconnect() {
        if (ca != null) {
            ca.resetVehicleConnection();
            mCurrentConnectedVigCarId = null;
        }
    }

    private void closeBleAndCaConnections() {
        mBleManager.scanForVig(false);
        mHandler.removeCallbacks(mUploadBeaconsRunnable);
        mHandler.removeCallbacks(mCheckVIGCommandTimeout);
        resetBleAndVig();
    }

    private void resetBleAndVig() {
        bleDisconnect();
        bleClose();
        mBleConnectionState = STATE_DISCONNECTED;

        caDisconnect();
        notifyVIGOffline();
        setVigState(VIG_DISCONNECTED);
    }

    private BluetoothGattCharacteristic getCharacteristic(BluetoothGatt gatt, UUID uuid) {
        if (gatt == null) {
            return null;
        }

        BluetoothGattService service = gatt.getService(RX_SERVICE_UUID);
        if (service == null) {
            return null;
        }

        BluetoothGattCharacteristic characteristic = service.getCharacteristic(uuid);
        if (characteristic == null) {
            return null;
        }

        return characteristic;
    }

    private void enableTXNotification(BluetoothGatt gatt) {
        BluetoothGattCharacteristic TxChar = getCharacteristic(gatt, TX_CHAR_UUID);
        if (TxChar == null) {
            bleDisconnect();
            return;
        }

        gatt.setCharacteristicNotification(TxChar, true);

        BluetoothGattDescriptor descriptor = TxChar.getDescriptor(CCCD);
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        gatt.writeDescriptor(descriptor);
    }

    private int writeRXCharacteristic(byte[] value) {
        BluetoothGattCharacteristic RxChar = getCharacteristic(mBluetoothGatt, RX_CHAR_UUID);
        if (RxChar == null) {
//            bleDisconnect();
            return 0;
        }

        if (mBleConnectionState == STATE_CONNECTED) {
            RxChar.setValue(value);
        }

        return mBluetoothGatt.writeCharacteristic(RxChar) ? 1 : 2;
    }

    private void readVigInfo() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isVigReady()) {
                    LogCat.d("讀取 partno");
                    startBleCommand("partno", "");
                }
            }
        }, 5 * 1000);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isVigReady()) {
                    LogCat.d("讀取 version");
                    startBleCommand("version", "");
                }
            }
        }, 7 * 1000);
    }

    // TODO: it depends on readVigInfo()
    private void notifyVIGOnline() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                broadcastUpdate(CA_STATUS_VIG_ONLINE);
            }
        }, 5 * 1000);
    }

    private void notifyVIGOffline() {
        if (getVigState() == VIG_ISSUE_COMMAND) {
            broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, mCommand, R.string.remote_control_op_bt_disconnect_during_rc_message);
        }
        broadcastUpdate(CA_STATUS_VIG_OFFLINE);
    }

    private Runnable mUploadBeaconsRunnable = new Runnable() {
        @Override
        public void run() {
            uploadBeacons();
        }
    };

    private void uploadBeacons() {
        if (ca == null) {
            LogCat.e("ca 尚未準備好");
            return;
        }

        mHandler.removeCallbacks(mUploadBeaconsRunnable);
        if (mLeftBeacon != null && mRightBeacon != null && mTrunkBeacon != null) {
            String lrssi = mLeftBeacon.getEffectiveRssi() == 0 ? "" : String.valueOf(mLeftBeacon.getEffectiveRssi());
            String trssi = mTrunkBeacon.getEffectiveRssi() == 0 ? "" : String.valueOf(mTrunkBeacon.getEffectiveRssi());
            String rrssi = mRightBeacon.getEffectiveRssi() == 0 ? "" : String.valueOf(mRightBeacon.getEffectiveRssi());

            String commandValue = String.format(Locale.TAIWAN, "%s,%s,%s", lrssi, trssi, rrssi);
            if (",,".equals(commandValue)) {
                if (mVerboseLog) LogCat.d("no beacons detected");
            } else {
                if (ca.getVehicleCommunicationStatus() == CommunicationAgent.CA_STATUS_CONNECTED) {
                    if (mVerboseLog) LogCat.d("uploadBeacons: " + commandValue);
                    ca.getMainDataChannel().write(wrapBleCommand("BeaconRSSI", commandValue));
                }
            }
        }
        mHandler.postDelayed(mUploadBeaconsRunnable, 200);
    }

    private void scanForBeacons(boolean start) {
        Context context = this.getApplicationContext();
        CarKeyData carKeyData = Prefs.getCarKeyData(context);
        if (carKeyData == null || !carKeyData.isVig()) {
            return;
        }

        String carId = carKeyData.getVigCarId();
        mLeftBeacon = Prefs.getBeacon(this, BeaconOptions.LEFT_FRONT, carId);
        mTrunkBeacon = Prefs.getBeacon(this, BeaconOptions.TAILGATE, carId);
        mRightBeacon = Prefs.getBeacon(this, BeaconOptions.RIGHT_FRONT, carId);

        if (mBleManager != null) {
            if (mLeftBeacon != null && mTrunkBeacon != null && mRightBeacon != null && start) {
                mBleManager.scanForBeacon(true);
                mBleManager.enableBeaconThreshold(false);
                uploadBeacons();
            } else {
                mHandler.removeCallbacks(mUploadBeaconsRunnable);
                mBleManager.scanForBeacon(false);
            }
        }
    }

    private void scanLeDevice(final boolean enable) {
        if (mBleManager != null) {
            if (enable) {
                mBleManager.scanForVig(true);
                mBleManager.start();
            } else {
                mBleManager.stop();
            }
        }
    }

    public boolean isVigReady() {
        return (getVigState() == VIG_READY_TO_GO || getVigState() == VIG_COMMAND_TIMEOUT) &&
                (ca.getVehicleCommunicationStatus() == CommunicationAgent.CA_STATUS_CONNECTED);
    }

    public boolean isVigConnected() {
        return (ca.getVehicleCommunicationStatus() == CommunicationAgent.CA_STATUS_CONNECTED);
    }

    public int getVigState() {
        return mVigConnectionState;
    }

    private String getVigStateStr() {
        switch (mVigConnectionState) {
            case VIG_INTERNAL_ERROR:
                return "VIG_INTERNAL_ERROR";
            case VIG_COMMAND_TIMEOUT:
                return "VIG_COMMAND_TIMEOUT";
            case VIG_DISCONNECTED:
                return "VIG_DISCONNECTED";
            case VIG_CONNECTING:
                return "VIG_CONNECTING";
            case VIG_CONNECTED:
                return "VIG_CONNECTED";
            case VIG_BY_PASSING:
                return "VIG_BY_PASSING";
            case VIG_REQUEST_CONTROL:
                return "VIG_REQUEST_CONTROL";
            case VIG_READY_TO_GO:
                return "VIG_READY_TO_GO";
            case VIG_ISSUE_COMMAND:
                return "VIG_ISSUE_COMMAND";
        }
        return "UNDEF";
    }

    private void setVigState(int state) {
        mVigConnectionState = state;
    }

    private JSONObject mResultJSONObject;

    private Runnable mCheckVIGCommandTimeout = new Runnable() {
        @Override
        public void run() {
            if (getVigState() == VIG_ISSUE_COMMAND) {
                setVigState(VIG_COMMAND_TIMEOUT);
            }

            // scanForBeacons(true);

            if (doNotLog(mCommand)) {
                return;
            }

            if (mResultJSONObject == null) {
                logControls(mCommand, VigOptions.REMOTE_CONTROL_NEAR, VigOptions.REMOTE_CONTROL_FAIL);
                showRemoteFailureNotification(mCommand, -2);
                broadcastUpdate(CA_STATUS_REMOTE_CONTROL_INTERNAL_ERROR, mCommand);
            }
        }
    };

    private byte[] wrapBleCommand(String command, String commandValue) {
        String input = String.format(Locale.TAIWAN, "{\"%s\":\"%s\"}", command, commandValue);
        return input.getBytes(StandardCharsets.UTF_8);
    }

    private void startBleCommand(String command, String commandValue) {
        byte[] value = wrapBleCommand(command, commandValue);
        if (value != null) {
            if (getVigState() == VIG_ISSUE_COMMAND) {
                LogCat.d("VIG is processing last command");
                broadcastUpdate(CA_STATUS_REMOTE_CONTROL_BLOCKED, command);
                return;
            }

            dismissNotification(command);
            if (isVigReady()) {
                // scanForBeacons(false);
                setVigState(VIG_ISSUE_COMMAND);
                mResultJSONObject = null;
                mCommand = command;
                if (command.equalsIgnoreCase("AcTemp")) {
                    LogCat.d("AcTemp 拉長 timeout");
                    mHandler.postDelayed(mCheckVIGCommandTimeout, VIG_COMMAND_TIMEOUT_LONG_SEC);
                } else {
                    mHandler.postDelayed(mCheckVIGCommandTimeout, VIG_COMMAND_TIMEOUT_SEC);
                }
                if (ca != null) {
                    ca.getMainDataChannel().write(value);
                }
            } else {
                LogCat.d("VIG is not ready, current state: " + getVigState());
                startRemoteControl(command, commandValue);
            }
        }
    }

    private void doVigByPass(BluetoothGatt gatt) {
        if (getVigState() == VIG_CONNECTED) {
            setVigState(VIG_BY_PASSING);
            enableTXNotification(gatt);
            putIntoSentOutDataQueue("VIG_BYPASS".getBytes());
            // then wait for 'MAC CHK OK' ACTION_DATA_READ broadcast
        }
    }

    private void stopRetryConnectVehicle() {
        retryConnectToVehicle = 0;
    }

    private void retryConnectVehicle() {
        if (retryConnectToVehicle++ < MAX_RETRY_COUNT) {
            connectToVehicle();
        } else {
            stopRetryConnectVehicle();
            closeBleAndCaConnections();
            // set to internal error for stop connecting again
            setVigState(VIG_INTERNAL_ERROR);
        }
    }

    private void connectToVehicle() {
        Context context = this.getApplicationContext();
        CarKeyData carKeyData = Prefs.getCarKeyData(context);
        if (carKeyData == null || !carKeyData.isVig()) {
            setVigState(VIG_INTERNAL_ERROR);
            return;
        }

        String carId = carKeyData.getVigCarId();
        IKeyDataBean keyDataBean = null;
        if (CarOptions.KEY_M.equals(carKeyData.getKeyType()) || CarOptions.KEY_M2.equals(carKeyData.getKeyType())) {
            keyDataBean = getMasterKey(context, carId);
        } else if (CarOptions.KEY_S2.equals(carKeyData.getKeyType())) {
            keyDataBean = getSubKey(context, carId);
        } else if (CarOptions.KEY_T2.equals(carKeyData.getKeyType())) {
            keyDataBean = getSnapKey(context, carId);
        }
        if (USE_DEBUG_VERSION_CA) {
            keyDataBean = ca.getProductTestIKey(carId);
        }
        if (keyDataBean == null) {
            setVigState(VIG_INTERNAL_ERROR);
            return;
        }

        if (ca == null) {
            setVigState(VIG_INTERNAL_ERROR);
            return;
        }

        if (ca.connectToVehicle(keyDataBean, new CommandCallback() {
            @Override
            public void commandComplete(int i, HashMap hashMap) {
                if (i == CommandCallback.SUCCESS) {
                    LogCat.d("VIG is ready to go");
                    setVigState(VIG_READY_TO_GO);
                    stopRetryConnectVehicle();
                    mCurrentConnectedVigCarId = getCurrentConnectedVigCarId(getApplicationContext());
                    // doUnfinishedJobs(getApplicationContext());
                    // readVigInfo();
                    notifyVIGOnline();
                    // scanForBeacons(true);
                } else if (i == CommandCallback.TIMEOUT) {
                    LogCat.d("Failed to connect vehicle (timeout)");
                    stopRetryConnectVehicle();
                    resetBleAndVig();
                    scanLeDevice(true);
                } else if (i != CommandCallback.TERMINATED) {
                    LogCat.d("Failed to connect vehicle : " + retryConnectToVehicle);
                    caDisconnect();
                    retryConnectVehicle();
                }
            }
        })) {
            setVigState(VIG_REQUEST_CONTROL);
        } else {
            setVigState(VIG_INTERNAL_ERROR);
        }
    }

    private void logByteData(String prefix, byte[] data) {
        try {
            String text = new String(data, StandardCharsets.UTF_8).trim();
            LogCat.d(prefix + ":" + text);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void logByteDataHex(String prefix, byte[] data) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("(binary2):");
            byte[] receivedData = new byte[data.length];
            System.arraycopy(data, 0, receivedData, 0, data.length);
            for (int i = 0; i < receivedData.length; i++) {
                int txValueToInteger = (int) receivedData[i];
                txValueToInteger = txValueToInteger & 255;
                sb.append(Integer.toHexString(txValueToInteger));
                if (i < receivedData.length - 1) {
                    sb.append(",");
                }
            }
            LogCat.d(prefix + ":" + sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processBleData() {
        if (ca != null) {
            DataChannel channel = ca.getBLEDataChannel();
            while (channel.hasData()) {
                byte[] data = channel.take();
                if (data != null) {
                    if (mVerboseLog) logByteDataHex("ble data channel", data);
                    putIntoSentOutDataQueue(data);
                }
            }
        }
    }

    private void processMainData() {
        if (ca != null) {
            DataChannel channel = ca.getMainDataChannel();
            while (channel.hasData()) {
                byte[] data = channel.take();
                if (data != null) {
                    if (mVerboseLog) logByteDataHex("main data channel", data);
                    processVigCommand(data);
                }
            }
        }
    }

    private void showNotification(int id, String title, String content) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        String channelId = getString(R.string.notification_channel_id_ca);

        PendingIntent pi = PendingIntent.getService(this, 1, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(content)
                        .setAutoCancel(true)
                        .setContentIntent(pi);

        Notification notify = notificationBuilder.build();
//        notify.flags = Notification.FLAG_NO_CLEAR;//|Notification.FLAG_ONGOING_EVENT;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    getString(R.string.notification_channel_name),
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setShowBadge(true);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(id, notify);
    }

    private void dismissNotification(String command) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(getNotificationId(command));
    }

    private void showRemoteSuccessNotification(String command) {
        String message;
        if (REMOTE_COMMAND_OPEN_BACKDOOR.equals(command)) {
            message = getString(R.string.remote_control_op_open_backdoor_success_message);
        } else if (REMOTE_COMMAND_DOOR_UNLOCK.equals(command)) {
            message = getString(R.string.remote_control_op_unlock_car_success_message);
        } else if (REMOTE_COMMAND_DOOR_LOCK.equals(command)) {
            message = getString(R.string.remote_control_op_lock_car_success_message);
        } else if (REMOTE_COMMAND_FIND_CAR.equals(command)) {
            message = getString(R.string.remote_control_op_find_car_success_message);
        } else if (REMOTE_COMMAND_ONE_CLICK_OPTIMIZE.equals(command)) {
            message = getString(R.string.remote_control_op_optimize_success_message);
        } else if (REMOTE_COMMAND_ENGINE_OFF.equals(command)) {
            message = getString(R.string.remote_control_op_engineoff_success_message);
        } else {
            LogCat.d("No command value found");
            message = getString(R.string.remote_control_op_success_message);
        }
        showNotification(getNotificationId(command), getString(R.string.remote_control_op_success_title), message);
    }

    private int getNotificationId(String command) {
        if (REMOTE_COMMAND_ONE_CLICK_OPTIMIZE.equals(command)) {
            return 9001;
        } else if (REMOTE_COMMAND_OPEN_BACKDOOR.equals(command)) {
            return 9002;
        } else if (REMOTE_COMMAND_DOOR_LOCK.equals(command)) {
            return 9003;
        } else if (REMOTE_COMMAND_DOOR_UNLOCK.equals(command)) {
            return 9004;
        } else if (REMOTE_COMMAND_FIND_CAR.equals(command)) {
            return 9005;
        } else if (REMOTE_COMMAND_ENGINE_OFF.equals(command)) {
            return 9006;
        } else {
            return 9000;
        }
    }

    private String getErrorString(String command) {
        if (REMOTE_COMMAND_ONE_CLICK_OPTIMIZE.equals(command)) {
            return getString(R.string.remote_control_op_optimize_failed_message);
        } else if (REMOTE_COMMAND_OPEN_BACKDOOR.equals(command)) {
            return getString(R.string.remote_control_op_open_backdoor_failed_message);
        } else if (REMOTE_COMMAND_DOOR_LOCK.equals(command)) {
            return getString(R.string.remote_control_op_lock_car_failed_message);
        } else if (REMOTE_COMMAND_DOOR_UNLOCK.equals(command)) {
            return getString(R.string.remote_control_op_unlock_car_failed_message);
        } else if (REMOTE_COMMAND_FIND_CAR.equals(command)) {
            return getString(R.string.remote_control_op_find_car_failed_message);
        } else if (REMOTE_COMMAND_ENGINE_OFF.equals(command)) {
            return getString(R.string.remote_control_op_engineoff_failed_message);
        } else {
            return getString(R.string.remote_control_op_error_failed);
        }
    }

    private void showRemoteFailureNotification(String command, int reason) {
        if (reason == 0) {
            showNotification(getNotificationId(command),
                    getString(R.string.remote_control_network_error_title), getString(R.string.remote_control_network_error_message));
        } else if (reason == -1) {
            showNotification(getNotificationId(command),
                    getString(R.string.remote_control_op_error_title), getErrorString(command));
        } else if (reason == -2) {
            showNotification(getNotificationId(command),
                    getString(R.string.remote_control_op_error_title), getString(R.string.remote_control_internal_error));
        } else if (reason == -3) {
            showNotification(getNotificationId(command),
                    getString(R.string.remote_control_op_vig_failed_title), getString(R.string.remote_control_op_vig_failed_message));
        } else {
            showNotification(getNotificationId(command),
                    getString(R.string.remote_control_op_error_title), getString(reason));
        }
    }

    private void processVigCommand(byte[] rawCommand) {
        try {
            String text = new String(rawCommand, StandardCharsets.UTF_8).trim();
            mResultJSONObject = new JSONObject(text);
            mHandler.removeCallbacks(mCheckVIGCommandTimeout);
            setVigState(VIG_READY_TO_GO);

            // scanForBeacons(true);

            if (doNotLog(mCommand)) {
                return;
            }

            if ("success".equals(mResultJSONObject.getString(mCommand))) {
                logControls(mCommand, VigOptions.REMOTE_CONTROL_NEAR, VigOptions.REMOTE_CONTROL_OK);
                showRemoteSuccessNotification(mCommand);
                broadcastUpdate(CA_STATUS_REMOTE_CONTROL_DONE, mCommand);
            } else {
                logControls(mCommand, VigOptions.REMOTE_CONTROL_NEAR, VigOptions.REMOTE_CONTROL_FAIL);
                if (REMOTE_COMMAND_ONE_CLICK_OPTIMIZE.equals(mCommand)) {
                    if ("rejected".equals(mResultJSONObject.getString(mCommand))) {
                        showRemoteFailureNotification(mCommand, R.string.remote_control_op_error_rejected_optimize);
                        broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, R.string.remote_control_op_error_rejected_optimize);
                    } else {
                        showRemoteFailureNotification(mCommand, R.string.remote_control_op_error_special);
                        broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, mCommand, R.string.remote_control_op_error_special);
                    }
                } else {
                    if ("unsupported".equals(mResultJSONObject.getString(mCommand))) {
                        showRemoteFailureNotification(mCommand, R.string.remote_control_op_error_unsupported);
                        broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, mCommand, R.string.remote_control_op_error_unsupported);
                    } else if ("rejected".equals(mResultJSONObject.getString(mCommand))) {
                        if (REMOTE_COMMAND_ENGINE_OFF.equals(mCommand)) {
                            showRemoteFailureNotification(mCommand, R.string.remote_control_op_error_rejected_engine_off);
                            broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, R.string.remote_control_op_error_rejected_engine_off);
                        } else {
                            showRemoteFailureNotification(mCommand, R.string.remote_control_op_error_rejected);
                            broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, mCommand, R.string.remote_control_op_error_rejected);
                        }
                    } else {
                        showRemoteFailureNotification(mCommand, -1);
                        broadcastUpdate(CA_STATUS_REMOTE_CONTROL_FAILED, mCommand, -1);
                    }
                }
            }
        } catch (JSONException e) {
            mResultJSONObject = null;
            e.printStackTrace();
        }
    }

    private ArrayBlockingQueue<byte[]> rxQueue = new ArrayBlockingQueue<>(1000);
    private ArrayBlockingQueue<byte[]> dataQueue = new ArrayBlockingQueue<>(100);

    private class SendToBleQueueHandler extends Thread {
        private boolean loopSwitch = true;
        private int mPacketCount = 0;
        private long mLastPacketTime = 0;

        void terminate() {
            loopSwitch = false;
        }

        @Override
        public void run() {
            while (loopSwitch) {
                if (dataQueue.size() > 0) {
                    try {
                        byte[] data = dataQueue.peek();
                        if (data == null) {
                            sleep(BLE_QUEUE_PEEK_TIME_MS);
                            continue;
                        }
                        int result = writeRXCharacteristic(data);
                        if (result == 1) {
                            dataQueue.remove(data);
                        }
                        long currentTime = System.currentTimeMillis();
                        if (currentTime - mLastPacketTime > BLE_QUEUE_RESET_COUNT_TIME_MS) {
                            mPacketCount = 0;
                            if (mVerboseLog) LogCat.d("Reset packet count");
                        }
                        mLastPacketTime = currentTime;
                        mPacketCount++;
                        if (mPacketCount >= 40) {
                            mPacketCount = 0;
                            if (mVerboseLog) LogCat.d("Reset packet count");
                            sleep(BLE_QUEUE_MAX_PACKETS_PAUSE_TIME_MS);
                        } else {
                            sleep(BLE_QUEUE_PROCESS_PAUSE_TIME_MS);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        sleep(BLE_QUEUE_QUERY_TIME_MS);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void putIntoSentOutDataQueue(byte[] data) {
        try {
            dataQueue.offer(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class SendToCAQueueHandler extends Thread {
        private boolean loopSwitch = true;

        void terminate() {
            loopSwitch = false;
        }

        /*
         * 1. data is 16bytes
         * 2. data is not human readable (non alpha)
         */
        private boolean readyToTransmit(byte[] data) {
            if (data == null || data.length == 0) {
                return false;
            }

            if (data.length == 16) {
                return true;
            }

            for (byte txByte : data) {
                int txValueInt = txByte;
                txValueInt = txValueInt & 255;
                if (txValueInt < 32 || txValueInt > 126) {
                    return true;
                }
            }
            return false;
        }

        private boolean readyToConnectVehicle(byte[] data) {
            if (data != null && data.length == 10) {
                if (data[0] == 77 && data[1] == 65 && data[2] == 67 && data[3] == 32 && // 'MAC '
                        data[4] == 67 && data[5] == 72 && data[6] == 75 && data[7] == 32  // 'CHK '
                        && data[8] == 79 && data[9] == 75) { // 'OK'
                    return true;
                }
            }
            return false;
        }

        @Override
        public void run() {
            while (loopSwitch) {
                if (rxQueue.size() > 0) {
                    final byte[] txValue = rxQueue.poll();
                    if (txValue != null) {
                        if (readyToTransmit(txValue)) {
                            if (mVerboseLog) logByteDataHex("SendToCAQueueHandler", txValue);
                            if (ca != null) {
                                ca.getBLEDataChannel().write(txValue);
                            }
                        } else {
                            logByteData("SendToCAQueueHandler", txValue);
                        }

                        if ((getVigState() == VIG_BY_PASSING) && readyToConnectVehicle(txValue)) {
                            LogCat.d("BUILD SECURE CHANNEL");
                            connectToVehicle();
                        }
                    }
                }
            }
        }
    }

    private void sendToCABleDataChannel(byte[] data) {
        try {
            rxQueue.offer(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void continueKeyOperationsAfterOpSuccess() {
        if (mCAOperationType == OP_ACTIVATE_MASTERKEY) {
            broadcastUpdate(CA_STATUS_ADD_MASTERKEY, CA_EXTRA_ADD_MASTERKEY_SUCCESS);
        } else if (mCAOperationType == OP_ADD_SUBKEY) {
            updateCarKeyInfo(mTargetSubKeyPosition, CarOptions.KEY_S1, CarOptions.KEY_STATUS_S1, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime(), null);
            broadcastUpdate(CA_STATUS_ADD_SUBKEY, CA_EXTRA_ADD_SUBKEY_SUCCESS);
        } else if (mCAOperationType == OP_ADD_SUBKEY_PREPARED) {
            updateCarKeyInfo(mTargetSubKeyPosition, CarOptions.KEY_S1, CarOptions.KEY_STATUS_AP2S1, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime(), null);
            broadcastUpdate(CA_STATUS_ADD_SUBKEY, CA_EXTRA_ADD_SUBKEY_PREPARED_SUCCESS);
        } else if (mCAOperationType == OP_DEL_SUBKEY) {
            updateCarKeyInfo(mTargetSubKeyType, CarOptions.KEY_STATUS_D1, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime());
            broadcastUpdate(CA_STATUS_DEL_SUBKEY, CA_EXTRA_DEL_SUBKEY_SUCCESS_START);
        } else if (mCAOperationType == OP_DEL_SUBKEY_PREPARED) {
            updateCarKeyInfo(mTargetSubKeyType, CarOptions.KEY_STATUS_DP2D1, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime());
            broadcastUpdate(CA_STATUS_DEL_SUBKEY, CA_EXTRA_DEL_SUBKEY_PREPARED_SUCCESS);
        } else if (mCAOperationType == OP_RENEW_SNAPKEY) {
            renewSnapTicketsInDB();
            broadcastUpdate(CA_STATUS_RENEW_SNAPKEY, CA_EXTRA_RENEW_SNAPKEY_SUCCESS);
        } else if (mCAOperationType == OP_ALLOCATE_SNAPKEY) {
            addSnapTicketT1inDB(mTargetSnapKeyPosition, mTargetSnapKeyUid, mTargetSnapKeyDeviceId, mTargetSnapKeyCreateTime.getTime(),
                    mTargetSnapKeyStartTime.getTime(), mTargetSnapKeyEndTime.getTime(), mTargetSnapKeyName);
            broadcastUpdate(CA_STATUS_ALLOCATE_SNAPKEY, CA_EXTRA_ALLOCATE_SNAPKEY_SUCCESS);
        } else if (mCAOperationType == OP_TERMINATE_SNAPKEY) {
            tagSnapTicketD1inDB(mTargetSnapKeyPosition);
            broadcastUpdate(CA_STATUS_TERMINATE_SNAPKEY, CA_EXTRA_TERMINATE_SNAPKEY_SUCCESS);
        }
        mCAOperationType = OP_NONE;
    }

    private void continueKeyOperationsAfterOpSuccessForKeyChainList() {
        if (mCAOperationType == OP_ACTIVATE_MASTERKEY) {
            broadcastUpdate(CA_STATUS_ADD_MASTERKEY, CA_EXTRA_ADD_MASTERKEY_SUCCESS);
            mCAOperationType = OP_NONE;
        } else if (mCAOperationType == OP_DEL_SUBKEY) {
            broadcastUpdate(CA_STATUS_DEL_SUBKEY, CA_EXTRA_DEL_SUBKEY_SUCCESS_FINISH);
            mCAOperationType = OP_NONE;
        }
    }

    private OnApiListener<GetKeyChainListResponseData> mGetKeyChainListApiListener = new OnApiListener<GetKeyChainListResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetKeyChainListResponseData responseData) {
            continueKeyOperationsAfterOpSuccessForKeyChainList();
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            continueKeyOperationsAfterOpSuccessForKeyChainList();
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            continueKeyOperationsAfterOpSuccessForKeyChainList();
        }
    };

    private void updateKeyChainList() {
        Context context = this.getApplicationContext();
        UserInfo userInfo = Prefs.getUserInfo(context);
        CarKeyData carKeyData = Prefs.getCarKeyData(context);
        if (carKeyData == null || userInfo == null) {
            continueKeyOperationsAfterOpSuccessForKeyChainList();
            return;
        }

        GetKeyChainListRequestData data = new GetKeyChainListRequestData(userInfo.getKeyUid(), userInfo.getToken(), carKeyData.getVigCarId());

        CarWebApi carWebApi = CarWebApi.getInstance(context);
        carWebApi.getKeyChainList(data, mGetKeyChainListApiListener);
    }

    private final static int OP_NONE = 0;
    private final static int OP_ACTIVATE_MASTERKEY = 1;
    private final static int OP_DELETE_MASTERKEY = 2;
    private final static int OP_ADD_SUBKEY = 3;
    private final static int OP_ADD_SUBKEY_PREPARED = 13;
    private final static int OP_DEL_SUBKEY = 4;
    private final static int OP_DEL_SUBKEY_PREPARED = 14;
    private final static int OP_RENEW_SNAPKEY = 5;
    private final static int OP_ALLOCATE_SNAPKEY = 6;
    private final static int OP_TERMINATE_SNAPKEY = 7;
    private final static int OP_DEL_ADD_PREPARE = 8;
    private final static int OP_DEL_DEL_PREPARE = 9;

    private int mCAOperationType = OP_NONE;

    public boolean isAddingSubKey() {
        return (mCAOperationType == OP_ADD_SUBKEY || mCAOperationType == OP_ADD_SUBKEY_PREPARED);
    }

    public boolean isDelingSubKey() {
        return (mCAOperationType == OP_DEL_SUBKEY || mCAOperationType == OP_DEL_ADD_PREPARE || mCAOperationType == OP_DEL_DEL_PREPARE || mCAOperationType == OP_DEL_SUBKEY_PREPARED);
    }

    public final static int CA_EXTRA_RENEW_SNAPKEY_FAILED = 0;
    public final static int CA_EXTRA_RENEW_SNAPKEY_SUCCESS = 1;

    private CommandCallback mRenewSnapKeyCommandCallback = new CommandCallback() {

        @Override
        public void commandComplete(int commandResultCode, HashMap hashMap) {
            if (commandResultCode == CommandCallback.SUCCESS) {
                continueKeyOperationsAfterOpSuccess();
            } else if (commandResultCode != CommandCallback.START) {
                LogCat.d("mRenewSnapKeyCommandCallback:" + commandResultCode);
                broadcastUpdate(CA_STATUS_RENEW_SNAPKEY, CA_EXTRA_RENEW_SNAPKEY_FAILED);
                mCAOperationType = OP_NONE;
            }
        }
    };

    private final static int MAX_VIG_CONNECTION_CHECK = 3;

    private boolean checkIfCanIssueSnapkeyCommand() {
        int loopCount = 0;

        do {
            if (ca.getVehicleCommunicationStatus() == CommunicationAgent.CA_STATUS_CONNECTED) {
                return true;
            } else if (ca.getVehicleCommunicationStatus() == CommunicationAgent.CA_STATUS_CONNECTING) {
                LogCat.d("VIG 連線中，等待 10 秒");
                sleep(10);
            }
        } while (loopCount++ < MAX_VIG_CONNECTION_CHECK);

        return ca.getVehicleCommunicationStatus() == CommunicationAgent.CA_STATUS_CONNECTED;
    }

    private SnapTicket getEmptySnapTicket() {
        String carId = getCurrentConnectedVigCarId(getApplicationContext());
        if (carId == null) {
            return null;
        }

        IKeyDataBean keyDataBean = getMasterKey(getApplicationContext(), carId);
        if (keyDataBean == null) {
            return null;
        }

        List<SnapTicket> snapTicketList = ca.getSnapTicketByIKeyDataBean(keyDataBean);
        if (snapTicketList.size() == 0 || snapTicketList.get(0) == null) {
            return null;
        }

        return snapTicketList.get(0);
    }

    public void renewSnapTicket() {
        if (ca == null) {
            broadcastUpdate(CA_STATUS_RENEW_SNAPKEY, CA_EXTRA_RENEW_SNAPKEY_FAILED);
            return;
        }

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                SnapTicket snapTicket = getEmptySnapTicket();
                if (snapTicket == null) {
                    broadcastUpdate(CA_STATUS_RENEW_SNAPKEY, CA_EXTRA_RENEW_SNAPKEY_FAILED);
                    return;
                }

                mCAOperationType = OP_RENEW_SNAPKEY;
                if (checkIfCanIssueSnapkeyCommand()) {
                    ca.renewSnapTicket(snapTicket, mRenewSnapKeyCommandCallback);
                } else {
                    if (!checkVigWakeUp(mOnCheckVigKeyOperationApiListener)) {
                        onCheckVigKeyOperationFailure(null);
                    }
                }
            }
        });
    }

    private void renewSnapTicketsInDB() {
        Context context = getApplicationContext();
        VigDatabase vigDatabase = VigDatabase.getInstance(context);
        KeyRepository keyRepository = new KeyRepository(context, vigDatabase);

        KeyInfo keyInfo = getCarKeyInfo(4);
        if (keyInfo != null) {
            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_SNAP_NORMAL);
            keyRepository.updateToDb(keyInfo);
        }
        keyInfo = getCarKeyInfo(5);
        if (keyInfo != null) {
            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_SNAP_NORMAL);
            keyRepository.updateToDb(keyInfo);
        }
    }

    private String mTargetSnapKeyUid;
    private String mTargetSnapKeyDeviceId;
    private Timestamp mTargetSnapKeyCreateTime;
    private Timestamp mTargetSnapKeyStartTime;
    private Timestamp mTargetSnapKeyEndTime;
    private int mTargetSnapKeyPosition;
    private String mTargetSnapKeyName;

    public final static int CA_EXTRA_ALLOCATE_SNAPKEY_FAILED = 0;
    public final static int CA_EXTRA_ALLOCATE_SNAPKEY_SUCCESS = 1;

    private CommandCallback mAllocateSnapKeyCommandCallback = new CommandCallback() {

        @Override
        public void commandComplete(int commandResultCode, HashMap hashMap) {
            LogCat.d("mAllocateSnapKeyCommandCallback:" + commandResultCode);
            if (commandResultCode == CommandCallback.START) {
                continueKeyOperationsAfterOpSuccess();
            } else if (commandResultCode == CommandCallback.FINISH) {
                updateKeyChainList();
            } else {
                LogCat.d("mAllocateSnapKeyCommandCallback:" + commandResultCode);
                broadcastUpdate(CA_STATUS_ALLOCATE_SNAPKEY, CA_EXTRA_ALLOCATE_SNAPKEY_FAILED);
                mCAOperationType = OP_NONE;
            }
        }
    };

    private SnapTicket getAvailableSnapTicket(IKeyDataBean keyDataBean) {
        if (keyDataBean == null) {
            return null;
        }

        SnapTicket[] snapTickets = keyDataBean.getSnapTicketArray();
        if (snapTickets == null) {
            return null;
        }

        if (snapTickets[0].getStatus() != SnapTicket.NORMAL) {
            return null;
        }

        return snapTickets[0];
    }

    public void allocateSnapTicket(int positionInUI, String iKeyUid, String deviceId, long deviceCreateTime, long startTime, long endTime, String name) {
        if (TextUtils.isEmpty(iKeyUid) || TextUtils.isEmpty(deviceId) || deviceCreateTime <= 0 || startTime <= 0 || endTime <= 0) {
            broadcastUpdate(CA_STATUS_ALLOCATE_SNAPKEY, CA_EXTRA_ALLOCATE_SNAPKEY_FAILED);
            return;
        }

        String carId = getCurrentConnectedVigCarId(getApplicationContext());
        if (carId == null) {
            broadcastUpdate(CA_STATUS_ALLOCATE_SNAPKEY, CA_EXTRA_ALLOCATE_SNAPKEY_FAILED);
            return;
        }

        IKeyDataBean keyDataBean = getMasterKey(getApplicationContext(), carId);
        if (keyDataBean == null) {
            broadcastUpdate(CA_STATUS_ALLOCATE_SNAPKEY, CA_EXTRA_ALLOCATE_SNAPKEY_FAILED);
            return;
        }

        if (getAvailableSnapTicket(keyDataBean) == null) {
            broadcastUpdate(CA_STATUS_ALLOCATE_SNAPKEY, CA_EXTRA_ALLOCATE_SNAPKEY_FAILED);
            return;
        }

        mTargetSnapKeyUid = iKeyUid;
        mTargetSnapKeyDeviceId = deviceId;
        mTargetSnapKeyCreateTime = new Timestamp(deviceCreateTime);
        mTargetSnapKeyStartTime = new Timestamp(startTime);
        mTargetSnapKeyEndTime = new Timestamp(endTime);
        mTargetSnapKeyPosition = positionInUI;
        mTargetSnapKeyName = name;
        mCAOperationType = OP_ALLOCATE_SNAPKEY;

        if (DO_IKEYLOGIN_BEFORE_KEY_OPERATION) {
            doIkeyLogin(mIkeyLoginRequestKeyOperationCallback);
        } else {
            continueKeyOperationsAfterIkeyLogin();
        }
    }

    private void addSnapTicketT1inDB(int positionInUI, String iKeyUid, String deviceId, long deviceCreateTime, long startTime, long endTime, String name) {
        KeyInfo keyInfo = getCarKeyInfo(positionInUI);
        if (keyInfo != null) {
            keyInfo.setUserId(iKeyUid);
            keyInfo.setDeviceId(deviceId);
            keyInfo.setDeviceCreateTime(deviceCreateTime);
            keyInfo.setKeyType(CarOptions.KEY_T1);
            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_T1);
            keyInfo.setSnapKeyStartTime(startTime);
            keyInfo.setSnapKeyEndTime(endTime);
            keyInfo.setDisplayName(name);

            Context context = getApplicationContext();
            VigDatabase vigDatabase = VigDatabase.getInstance(context);
            KeyRepository keyRepository = new KeyRepository(context, vigDatabase);
            keyRepository.updateToDb(keyInfo);
        }
    }

    public final static int CA_EXTRA_TERMINATE_SNAPKEY_FAILED = 0;
    public final static int CA_EXTRA_TERMINATE_SNAPKEY_SUCCESS = 1;

    private CommandCallback mTerminateSnapKeyCommandCallback = new CommandCallback() {

        private boolean success = false;

        @Override
        public void commandComplete(int commandResultCode, HashMap hashMap) {
            if (commandResultCode == CommandCallback.START) {
                success = true;
                continueKeyOperationsAfterOpSuccess();
            } else if (commandResultCode == CommandCallback.FINISH) {
                delSnapTicketinDB(mTargetSnapKeyUid, mTargetSnapKeyDeviceId, mTargetSnapKeyCreateTime.getTime());
                updateKeyChainList();
            } else {
                LogCat.d("mTerminateSnapKeyCommandCallback:" + commandResultCode);
                if (!success) {
                    broadcastUpdate(CA_STATUS_TERMINATE_SNAPKEY, CA_EXTRA_TERMINATE_SNAPKEY_FAILED);
                    mCAOperationType = OP_NONE;
                }
            }
        }
    };

    private SnapTicket getSnapTicket(IKeyDataBean keyDataBean, String iKeyUid, String deviceId, long deviceCreateTime) {
        if (keyDataBean == null) {
            return null;
        }

        SnapTicket[] snapTickets = keyDataBean.getSnapTicketArray();
        if (snapTickets == null) {
            return null;
        }

        if (snapTickets[0].getStatus() != SnapTicket.ACTIVATE) {
            return null;
        }

        if (snapTickets[0].getSnapUserId().equalsIgnoreCase(iKeyUid) && snapTickets[0].getSnapDeviceId().equalsIgnoreCase(deviceId) &&
            snapTickets[0].getSnapDeviceCreateTime().getTime() == deviceCreateTime) {
            return snapTickets[0];
        }

        return null;
    }

    public void terminateSnapTicket(int positionInDB, String iKeyUid, String deviceId, long deviceCreateTime) {
        if (TextUtils.isEmpty(iKeyUid) || TextUtils.isEmpty(deviceId) || deviceCreateTime <= 0) {
            broadcastUpdate(CA_STATUS_TERMINATE_SNAPKEY, CA_EXTRA_TERMINATE_SNAPKEY_FAILED);
            return;
        }

        String carId = getCurrentConnectedVigCarId(getApplicationContext());
        if (carId == null) {
            broadcastUpdate(CA_STATUS_TERMINATE_SNAPKEY, CA_EXTRA_TERMINATE_SNAPKEY_FAILED);
            return;
        }

        IKeyDataBean keyDataBean = getMasterKey(getApplicationContext(), carId);
        if (keyDataBean == null) {
            broadcastUpdate(CA_STATUS_TERMINATE_SNAPKEY, CA_EXTRA_TERMINATE_SNAPKEY_FAILED);
            return;
        }

        if (getSnapTicket(keyDataBean, iKeyUid, deviceId, deviceCreateTime) == null) {
            broadcastUpdate(CA_STATUS_TERMINATE_SNAPKEY, CA_EXTRA_TERMINATE_SNAPKEY_FAILED);
            return;
        }

        mTargetSnapKeyUid = iKeyUid;
        mTargetSnapKeyDeviceId = deviceId;
        mTargetSnapKeyCreateTime = new Timestamp(deviceCreateTime);
        mTargetSnapKeyPosition = positionInDB;
        mCAOperationType = OP_TERMINATE_SNAPKEY;

        if (DO_IKEYLOGIN_BEFORE_KEY_OPERATION) {
            doIkeyLogin(mIkeyLoginRequestKeyOperationCallback);
        } else {
            continueKeyOperationsAfterIkeyLogin();
        }
    }

    private void tagSnapTicketD1inDB(int positionInDB) {
        Context context = getApplicationContext();
        VigDatabase vigDatabase = VigDatabase.getInstance(context);
        KeyRepository keyRepository = new KeyRepository(context, vigDatabase);
        String carId = getCurrentCarId(context);

        ArrayList<KeyInfo> keyInfos = keyRepository.fetchListFromDb(carId);
        for (KeyInfo keyInfo : keyInfos) {
            if (keyInfo.getPosition() == positionInDB) {
                keyInfo.setKeyStatus(CarOptions.KEY_STATUS_D1);
                keyRepository.updateToDb(keyInfo);
                break;
            }
        }
    }

    private void delSnapTicketinDB(String iKeyUid, String deviceId, long deviceCreateTime) {
        Context context = getApplicationContext();
        VigDatabase vigDatabase = VigDatabase.getInstance(context);
        KeyRepository keyRepository = new KeyRepository(context, vigDatabase);
        String carId = getCurrentCarId(context);

        ArrayList<KeyInfo> keyInfos = keyRepository.fetchListFromDb(carId);
        for (KeyInfo keyInfo : keyInfos) {
            if (keyInfo.getUserId().equals(iKeyUid) &&
                    keyInfo.getDeviceId().equals(deviceId) &&
                    keyInfo.getDeviceCreateTime() == deviceCreateTime &&
                    (keyInfo.getKeyType().equals(CarOptions.KEY_T1) || keyInfo.getKeyType().equals(CarOptions.KEY_T2))) {
                keyInfo.setKeyStatus(CarOptions.KEY_STATUS_SNAP_TERMINATE);
                keyRepository.updateToDb(keyInfo);
                break;
            }
        }
    }

    public final static int CA_EXTRA_ADD_MASTERKEY_FAILED = 0;
    public final static int CA_EXTRA_ADD_MASTERKEY_SUCCESS = 1;

    private CommandCallback mAddMasterKeyCommandCallback = new CommandCallback() {

        @Override
        public void commandComplete(int commandResultCode, HashMap hashMap) {
            if (commandResultCode == CommandCallback.SUCCESS) {
                LogCat.d("睡一秒之後再繼續 updateKeyChainList");
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        updateKeyChainList();
                    }
                }, 1000);
            } else if (commandResultCode != CommandCallback.START) {
                LogCat.d("mAddMasterKeyCommandCallback:" + commandResultCode);
                broadcastUpdate(CA_STATUS_ADD_MASTERKEY, CA_EXTRA_ADD_MASTERKEY_FAILED);
                mCAOperationType = OP_NONE;
            }
        }
    };

    public void addMasterKey() {
        mCAOperationType = OP_ACTIVATE_MASTERKEY;
        if (DO_IKEYLOGIN_BEFORE_KEY_OPERATION) {
            doIkeyLogin(mIkeyLoginRequestKeyOperationCallback);
        } else {
            continueKeyOperationsAfterIkeyLogin();
        }
    }

    public boolean isAddingMasterKey() {
        return (mCAOperationType == OP_ACTIVATE_MASTERKEY);
    }

    public boolean isDeletingMasterKey() {
        return (mCAOperationType == OP_DELETE_MASTERKEY);
    }

    public final static int CA_EXTRA_DEL_MASTERKEY_FAILED = 0;
    public final static int CA_EXTRA_DEL_MASTERKEY_NO_DATA = 1;
    public final static int CA_EXTRA_DEL_MASTERKEY_START = 2;
    public final static int CA_EXTRA_DEL_MASTERKEY_PROCESSING = 3;
    public final static int CA_EXTRA_DEL_MASTERKEY_COMPLETE = 4;

    public void delMasterKey() {
        mCAOperationType = OP_DELETE_MASTERKEY;
        continueKeyOperationsAfterIkeyLogin();
    }

    private OnApiListener<DeleteMasterKeyByAPPResponseData> mDeleteMasterKeyApiListener = new OnApiListener<DeleteMasterKeyByAPPResponseData>() {

        @Override
        public void onApiTaskSuccessful(DeleteMasterKeyByAPPResponseData responseData) {
            if (responseData.isSuccessful()) {
                if (CarOptions.DELETE_START.equals(responseData.getDeleteStatus())) {
                    broadcastUpdate(CA_STATUS_DEL_MASTERKEY, CA_EXTRA_DEL_MASTERKEY_START);
                    Prefs.setMasterKeyDeleteTime(getApplicationContext(), System.currentTimeMillis());
                    Prefs.setMasterKeyDeleteId(getApplicationContext(), responseData.getProcedureID());
                    queryMasterKeyDeletionStatus();
                } else if (CarOptions.DELETE_COMPLETE.equals(responseData.getDeleteStatus())) {
                    broadcastUpdate(CA_STATUS_DEL_MASTERKEY, CA_EXTRA_DEL_MASTERKEY_COMPLETE);
                    mCAOperationType = OP_NONE;
                } else {
                    broadcastUpdate(CA_STATUS_DEL_MASTERKEY, CA_EXTRA_DEL_MASTERKEY_NO_DATA);
                    mCAOperationType = OP_NONE;
                }
            } else {
                broadcastUpdate(CA_STATUS_DEL_MASTERKEY, CA_EXTRA_DEL_MASTERKEY_FAILED);
                mCAOperationType = OP_NONE;
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            broadcastUpdate(CA_STATUS_DEL_MASTERKEY, CA_EXTRA_DEL_MASTERKEY_FAILED);
            mCAOperationType = OP_NONE;
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            broadcastUpdate(CA_STATUS_DEL_MASTERKEY, CA_EXTRA_DEL_MASTERKEY_FAILED);
            mCAOperationType = OP_NONE;
        }
    };

    private boolean doDeleteMasterKey() {
        LogCat.d("刪除 master key");
        Context context = this.getApplicationContext();
        UserInfo userInfo = Prefs.getUserInfo(context);
        CarKeyData carKeyData = Prefs.getCarKeyData(context);
        if (carKeyData == null || userInfo == null) {
            return false;
        }

        VigRequestData data = new VigRequestData(userInfo.getKeyUid(), userInfo.getToken(), carKeyData.getCarId());

        CarWebApi carWebApi = CarWebApi.getInstance(context);
        carWebApi.deleteMasterKeyByAPP(data, mDeleteMasterKeyApiListener);

        return true;
    }

    private Runnable mQueryMasterKeyDeletionStatusRunnable = new Runnable() {
        @Override
        public void run() {
            Prefs.setMasterKeyDeleteTime(getApplicationContext(), System.currentTimeMillis());
            doQueryMasterKeyDeletionStatus();
        }
    };

    private final static int QUERY_MASTER_KEY_DELETION_STATUS_TIME_MS = 10 * 1000; // 10 秒查詢一次

    private boolean queryMasterKeyDeletionStatus() {
        long lastTime = Prefs.getMasterKeyDeleteTime(getApplicationContext());
        long currTime = System.currentTimeMillis();
        String procedureId = Prefs.getMasterKeyDeleteId(getApplicationContext());

        if (lastTime == 0 || TextUtils.isEmpty(procedureId)) { // no such process
            return false;
        }

        mCAOperationType = OP_DELETE_MASTERKEY;
        long runTime = QUERY_MASTER_KEY_DELETION_STATUS_TIME_MS - (currTime - lastTime);
        mHandler.removeCallbacks(mQueryMasterKeyDeletionStatusRunnable);
        mHandler.postDelayed(mQueryMasterKeyDeletionStatusRunnable, runTime);

        return true;
    }

    private OnApiListener<GetProcedureRecordbyAPPResponseData> mQueryMasterKeyDeletionApiListener = new OnApiListener<GetProcedureRecordbyAPPResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetProcedureRecordbyAPPResponseData responseData) {
            if (responseData.isSuccessful()) {
                if (CarOptions.DELETE_PROCESSING.equals(responseData.getDeleteMasterResult())) {
                    broadcastUpdate(CA_STATUS_DEL_MASTERKEY, CA_EXTRA_DEL_MASTERKEY_PROCESSING);
                    queryMasterKeyDeletionStatus();
                } else if (CarOptions.DELETE_COMPLETE.equals(responseData.getDeleteMasterResult())) {
                    broadcastUpdate(CA_STATUS_DEL_MASTERKEY, CA_EXTRA_DEL_MASTERKEY_COMPLETE);
                    mCAOperationType = OP_NONE;
                    Prefs.clearMasterKeyDeleteProcess(getApplicationContext());
                } else {
                    broadcastUpdate(CA_STATUS_DEL_MASTERKEY, CA_EXTRA_DEL_MASTERKEY_NO_DATA);
                    mCAOperationType = OP_NONE;
                    Prefs.clearMasterKeyDeleteProcess(getApplicationContext());
                }
            } else {
                broadcastUpdate(CA_STATUS_DEL_MASTERKEY, CA_EXTRA_DEL_MASTERKEY_FAILED);
                mCAOperationType = OP_NONE;
                Prefs.clearMasterKeyDeleteProcess(getApplicationContext());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            broadcastUpdate(CA_STATUS_DEL_MASTERKEY, CA_EXTRA_DEL_MASTERKEY_FAILED);
            mCAOperationType = OP_NONE;
            Prefs.clearMasterKeyDeleteProcess(getApplicationContext());
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            broadcastUpdate(CA_STATUS_DEL_MASTERKEY, CA_EXTRA_DEL_MASTERKEY_FAILED);
            mCAOperationType = OP_NONE;
            Prefs.clearMasterKeyDeleteProcess(getApplicationContext());
        }
    };

    public void doQueryMasterKeyDeletionStatus() {
        LogCat.d("查詢刪除 master key 進度");
        Context context = this.getApplicationContext();
        UserInfo userInfo = Prefs.getUserInfo(context);
        String procedureId = Prefs.getMasterKeyDeleteId(context);
        if (TextUtils.isEmpty(procedureId) || userInfo == null) {
            LogCat.d("目前無此操作");
            return;
        }

        GetProcedureRecordbyAPPRequestData data = new GetProcedureRecordbyAPPRequestData(userInfo.getKeyUid(), userInfo.getToken(), procedureId);

        CarWebApi carWebApi = CarWebApi.getInstance(context);
        carWebApi.getProcedureRecordbyAPP(data, mQueryMasterKeyDeletionApiListener);
    }

    public final static int CA_EXTRA_ADD_SUBKEY_FAILED = 0;
    public final static int CA_EXTRA_ADD_SUBKEY_SUCCESS = 1;
    public final static int CA_EXTRA_ADD_SUBKEY_PREPARED_SUCCESS = 2;

    private CommandCallback mAddSubKeyServerCommandCallback = new CommandCallback() {

        private boolean started = false;

        @Override
        public void commandComplete(int commandResultCode, HashMap hashMap) {
            LogCat.d("mAddSubKeyServerCommandCallback:" + commandResultCode);
            if (commandResultCode == CommandCallback.START) {
                started = true;
                continueKeyOperationsAfterOpSuccess();
            } else if (commandResultCode == CommandCallback.FINISH) {
                updateCarKeyInfo(mTargetSubKeyPosition, CarOptions.KEY_S2, CarOptions.KEY_STATUS_S2, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime(), null);
                updateKeyChainList();
            } else {
                if (!started) {
                    broadcastUpdate(CA_STATUS_ADD_SUBKEY, CA_EXTRA_ADD_SUBKEY_FAILED);
                    mCAOperationType = OP_NONE;
                }
            }
        }
    };

    private CommandCallback mAddSubKeyBLECommandCallback = new CommandCallback() {

        private boolean started = false;

        @Override
        public void commandComplete(int commandResultCode, HashMap hashMap) {
            LogCat.d("mAddSubKeyBLECommandCallback:" + commandResultCode);
            if (commandResultCode == CommandCallback.START) {
                started = true;
                continueKeyOperationsAfterOpSuccess();
            } else if (commandResultCode == CommandCallback.UPLOAD) {
                updateCarKeyInfo(mTargetSubKeyPosition, CarOptions.KEY_S1, CarOptions.KEY_STATUS_S1, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime(), null);
            } else if (commandResultCode == CommandCallback.FINISH) {
                updateCarKeyInfo(mTargetSubKeyPosition, CarOptions.KEY_S2, CarOptions.KEY_STATUS_S2, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime(), null);
                updateKeyChainList();
            } else {
                if (!started) {
                    broadcastUpdate(CA_STATUS_ADD_SUBKEY, CA_EXTRA_ADD_SUBKEY_FAILED);
                    mCAOperationType = OP_NONE;
                }
            }
        }
    };

    private void afterPrepareAddSubKeySuccess(PrepareKeyOperationBean bean) {
        updateCarKeyInfo(mTargetSubKeyPosition, CarOptions.KEY_S1, CarOptions.KEY_STATUS_AP2, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime(), null);
        if (ca.getVehicleCommunicationStatus() == CommunicationAgent.CA_STATUS_CONNECTED) {
            ca.addSubKeyThroughBle(bean, mAddSubKeyBLECommandCallback);
        } else {
            mCAOperationType = OP_ADD_SUBKEY;
            if (!checkVigWakeUp(mOnCheckVigKeyOperationApiListener)) {
                onCheckVigKeyOperationFailure(null);
            }
        }
    }

    private CommandCallback mPrepareAddSubKeyCommandCallback = new CommandCallback() {

        @Override
        public void commandComplete(int commandResultCode, HashMap hashMap) {
            if (commandResultCode == CommandCallback.SUCCESS) {
                String carId = getCurrentConnectedVigCarId(getApplicationContext());
                PrepareKeyOperationBean bean = getPrepareKeyOperationBean(mTargetSubKeyUid, mTargetSubKeyDeviceId,
                        carId, PrepareKeyOperationBean.OPERATION_ADD_SUB_KEY);
                if (bean != null && ca != null) {
                    afterPrepareAddSubKeySuccess(bean);
                    return;
                }
            }
            LogCat.d("mPrepareAddSubKeyCommandCallback failed:" + commandResultCode);
            clearCarKeyInfo(mTargetSubKeyPosition, CarOptions.KEY_S1, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime());
            broadcastUpdate(CA_STATUS_ADD_SUBKEY, CA_EXTRA_ADD_SUBKEY_FAILED);
        }
    };

    private String mTargetSubKeyUid;
    private String mTargetSubKeyDeviceId;
    private Timestamp mTargetSubKeyCreateTime;
    private String mTargetSubKeyType;
    private int mTargetSubKeyPosition;

    public void addSubKey(int positionInUi, String iKeyUid, String deviceId, long deviceCreateTime, String displayName) {
        if (TextUtils.isEmpty(iKeyUid) || TextUtils.isEmpty(deviceId) || deviceCreateTime <= 0) {
            broadcastUpdate(CA_STATUS_ADD_SUBKEY, CA_EXTRA_ADD_SUBKEY_FAILED);
            return;
        }

        String carId = getCurrentConnectedVigCarId(getApplicationContext());
        if (carId == null) {
            broadcastUpdate(CA_STATUS_ADD_SUBKEY, CA_EXTRA_ADD_SUBKEY_FAILED);
            return;
        }

        mTargetSubKeyUid = iKeyUid;
        mTargetSubKeyDeviceId = deviceId;
        mTargetSubKeyCreateTime = new Timestamp(deviceCreateTime);
        mCAOperationType = OP_ADD_SUBKEY;
        mTargetSubKeyPosition = positionInUi;

        if (NetUtils.isConnectToInternet(getApplicationContext())) {
            updateCarKeyInfo(mTargetSubKeyPosition, CarOptions.KEY_S1, CarOptions.KEY_STATUS_AP1, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime(), displayName);
            if (DO_IKEYLOGIN_BEFORE_KEY_OPERATION) {
                doIkeyLogin(mIkeyLoginRequestKeyOperationCallback);
            } else {
                continueKeyOperationsAfterIkeyLogin();
            }
        } else {
            continueKeyOperationsAfterIkeyLogin();
        }
    }

    private PrepareKeyOperationBean getPrepareKeyOperationBean(String userId, String deviceId, String carId, String operation) {
        ArrayList<PrepareKeyOperationBean> prepareKeyOperationList = ca.getPrepareKeyOperationList();
        if (prepareKeyOperationList.size() == 0) {
            prepareKeyOperationList = ca.getPrepareKeyOperationList();
        }

        PrepareKeyOperationBean prepareKeyOperationBean = null;
        if (prepareKeyOperationList.size() > 0) {
            for (PrepareKeyOperationBean bean : prepareKeyOperationList) {
                if (bean.getUserId().equalsIgnoreCase(userId) &&
                        bean.getDeviceId().equalsIgnoreCase(deviceId) &&
                        bean.getCarId().equalsIgnoreCase(carId) &&
                        bean.getOperation().equalsIgnoreCase(operation)
                ) {
                    prepareKeyOperationBean = bean;
                    break;
                }
            }
        }

        return prepareKeyOperationBean;
    }

    public final static int CA_EXTRA_DEL_SUBKEY_FAILED = 0;
    public final static int CA_EXTRA_DEL_SUBKEY_SUCCESS_START = 1;
    public final static int CA_EXTRA_DEL_SUBKEY_SUCCESS_FINISH = 2;
    public final static int CA_EXTRA_DEL_SUBKEY_PREPARED_SUCCESS = 3;

    private CommandCallback mDelSubKeyServerCommandCallback = new CommandCallback() {

        @Override
        public void commandComplete(int commandResultCode, HashMap hashMap) {
            LogCat.d("mDelSubKeyServerCommandCallback:" + commandResultCode);
            if (commandResultCode == CommandCallback.START) {
                continueKeyOperationsAfterOpSuccess();
            } else if (commandResultCode == CommandCallback.FINISH) {
                clearCarKeyInfo(mTargetSubKeyType, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime());
                updateKeyChainList();
            } else {
                broadcastUpdate(CA_STATUS_DEL_SUBKEY, CA_EXTRA_DEL_SUBKEY_FAILED);
                mCAOperationType = OP_NONE;
            }
        }
    };

    private CommandCallback mDelSubKeyBLECommandCallback = new CommandCallback() {

        private boolean started = false;

        @Override
        public void commandComplete(int commandResultCode, HashMap hashMap) {
            LogCat.d("mDelSubKeyBLECommandCallback:" + commandResultCode);
            if (commandResultCode == CommandCallback.START) {
                started = true;
                continueKeyOperationsAfterOpSuccess();
            } else if (commandResultCode == CommandCallback.UPLOAD) {
                updateCarKeyInfo(mTargetSubKeyType, CarOptions.KEY_STATUS_D1, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime());
            } else if (commandResultCode == CommandCallback.FINISH) {
                clearCarKeyInfo(mTargetSubKeyType, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime());
                updateKeyChainList();
            } else {
                if (!started) {
                    broadcastUpdate(CA_STATUS_DEL_SUBKEY, CA_EXTRA_DEL_SUBKEY_FAILED);
                    mCAOperationType = OP_NONE;
                }
            }
        }
    };

    private void afterPrepareDelSubKeySuccess(PrepareKeyOperationBean bean) {
        updateCarKeyInfo(mTargetSubKeyType, CarOptions.KEY_STATUS_DP2, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime());
        if (ca.getVehicleCommunicationStatus() == CommunicationAgent.CA_STATUS_CONNECTED) {
            ca.deleteSubKeyThroughBle(bean, mDelSubKeyBLECommandCallback);
        } else {
            mCAOperationType = OP_DEL_SUBKEY;
            if (!checkVigWakeUp(mOnCheckVigKeyOperationApiListener)) {
                onCheckVigKeyOperationFailure(null);
            }
        }
    }

    private CommandCallback mPrepareDelSubKeyCommandCallback = new CommandCallback() {

        @Override
        public void commandComplete(int commandResultCode, HashMap hashMap) {
            if (commandResultCode == CommandCallback.SUCCESS) {
                String carId = getCurrentConnectedVigCarId(getApplicationContext());
                PrepareKeyOperationBean bean = getPrepareKeyOperationBean(mTargetSubKeyUid, mTargetSubKeyDeviceId,
                        carId, PrepareKeyOperationBean.OPERATION_DELETE_SUB_KEY);
                if (bean != null && ca != null) {
                    afterPrepareDelSubKeySuccess(bean);
                    return;
                }
            }
            LogCat.d("mPrepareDelSubKeyCommandCallback failed:" + commandResultCode);
            broadcastUpdate(CA_STATUS_DEL_SUBKEY, CA_EXTRA_DEL_SUBKEY_FAILED);
        }
    };

    public void delSubKey(@CarOptions.KeyType String type, String iKeyUid, String deviceId, long deviceCreateTime) {
        if (TextUtils.isEmpty(iKeyUid) || TextUtils.isEmpty(deviceId) || deviceCreateTime <= 0) {
            broadcastUpdate(CA_STATUS_DEL_SUBKEY, CA_EXTRA_DEL_SUBKEY_FAILED);
            return;
        }

        String carId = getCurrentConnectedVigCarId(getApplicationContext());
        if (carId == null) {
            broadcastUpdate(CA_STATUS_DEL_SUBKEY, CA_EXTRA_DEL_SUBKEY_FAILED);
            return;
        }

        mTargetSubKeyUid = iKeyUid;
        mTargetSubKeyDeviceId = deviceId;
        mTargetSubKeyCreateTime = new Timestamp(deviceCreateTime);
        mTargetSubKeyType = type;
        mCAOperationType = OP_DEL_SUBKEY;

        if (NetUtils.isConnectToInternet(getApplicationContext())) {
            if (DO_IKEYLOGIN_BEFORE_KEY_OPERATION) {
                doIkeyLogin(mIkeyLoginRequestKeyOperationCallback);
            } else {
                continueKeyOperationsAfterIkeyLogin();
            }
        } else {
            continueKeyOperationsAfterIkeyLogin();
        }
    }

    private CommandCallback mDelSubKeyPrepareCommandCallback = new CommandCallback() {

        @Override
        public void commandComplete(int commandResultCode, HashMap hashMap) {
            if (commandResultCode == CommandCallback.SUCCESS) {
                int keyStatus = CarOptions.KEY_STATUS_DEF;
                if (CarOptions.KEY_S1.equals(mTargetSubKeyType)) {
                    keyStatus = CarOptions.KEY_STATUS_S1;
                } else if (CarOptions.KEY_S2.equals(mTargetSubKeyType)) {
                    keyStatus = CarOptions.KEY_STATUS_S2;
                }
                updateCarKeyInfo(mTargetSubKeyType, keyStatus, mTargetSubKeyUid, mTargetSubKeyDeviceId, mTargetSubKeyCreateTime.getTime());
                broadcastUpdate(CA_STATUS_DEL_SUBKEY, CA_EXTRA_DEL_SUBKEY_SUCCESS_START);
                return;
            }
            LogCat.d("mDelSubKeyPrepareCommandCallback failed:" + commandResultCode);
            broadcastUpdate(CA_STATUS_DEL_SUBKEY, CA_EXTRA_DEL_SUBKEY_FAILED);
        }
    };

    public void delSubKeyPrepare(boolean isAdd, @CarOptions.KeyType String type, String iKeyUid, String deviceId, long deviceCreateTime) {
        if (TextUtils.isEmpty(iKeyUid) || TextUtils.isEmpty(deviceId) || deviceCreateTime <= 0) {
            broadcastUpdate(CA_STATUS_DEL_SUBKEY, CA_EXTRA_DEL_SUBKEY_FAILED);
            return;
        }

        String carId = getCurrentConnectedVigCarId(getApplicationContext());
        if (carId == null) {
            broadcastUpdate(CA_STATUS_DEL_SUBKEY, CA_EXTRA_DEL_SUBKEY_FAILED);
            return;
        }

        mTargetSubKeyUid = iKeyUid;
        mTargetSubKeyDeviceId = deviceId;
        mTargetSubKeyCreateTime = new Timestamp(deviceCreateTime);
        mTargetSubKeyType = type;
        mCAOperationType = isAdd ? OP_DEL_ADD_PREPARE : OP_DEL_DEL_PREPARE;

        if (DO_IKEYLOGIN_BEFORE_KEY_OPERATION) {
            doIkeyLogin(mIkeyLoginRequestKeyOperationCallback);
        } else {
            continueKeyOperationsAfterIkeyLogin();
        }
    }

    private int getKeyPosition(int positionInUI) {
        return KeyRepository.getKeyPosition(positionInUI);
    }

    private KeyInfo getCarKeyInfo(int positionInUI) {
        Context context = getApplicationContext();
        VigDatabase vigDatabase = VigDatabase.getInstance(context);
        KeyRepository keyRepository = new KeyRepository(context, vigDatabase);
        String carId = getCurrentCarId(context);

        ArrayList<KeyInfo> keyInfos = keyRepository.fetchListFromDb(carId);
        for (KeyInfo keyInfo : keyInfos) {
            if (keyInfo.getPosition() == getKeyPosition(positionInUI)) {
                return keyInfo;
            }
        }
        return null;
    }

    private KeyInfo getCarKeyInfo(@CarOptions.KeyType String type, String iKeyUid, String deviceId, long deviceCreateTime) {
        Context context = getApplicationContext();
        VigDatabase vigDatabase = VigDatabase.getInstance(context);
        KeyRepository keyRepository = new KeyRepository(context, vigDatabase);
        String carId = getCurrentCarId(context);

        ArrayList<KeyInfo> keyInfos = keyRepository.fetchListFromDb(carId);
        for (KeyInfo keyInfo : keyInfos) {
            if (keyInfo.getKeyType().equals(type) &&
                    keyInfo.getUserId().equals(iKeyUid) &&
                    keyInfo.getDeviceId().equals(deviceId) &&
                    keyInfo.getDeviceCreateTime() == deviceCreateTime) {
                return keyInfo;
            }
        }
        return null;
    }

    private void updateCarKeyInfo(@CarOptions.KeyType String type, @CarOptions.KeyStatus int status,
                                  String iKeyUid, String deviceId, long deviceCreateTime) {
        updateCarKeyInfo(-1, type, status, iKeyUid, deviceId, deviceCreateTime, null);
    }

    private void updateCarKeyInfo(int positionInUi, @CarOptions.KeyType String type, @CarOptions.KeyStatus int status,
                                  String iKeyUid, String deviceId, long deviceCreateTime, String name) {
        KeyInfo keyInfo = null;
        if (positionInUi == -1) {
            keyInfo = getCarKeyInfo(type, iKeyUid, deviceId, deviceCreateTime);
        } else {
            keyInfo = getCarKeyInfo(positionInUi);
        }
        if (keyInfo != null) {
            keyInfo.setUserId(iKeyUid);
            keyInfo.setDeviceId(deviceId);
            keyInfo.setDeviceCreateTime(deviceCreateTime);
            keyInfo.setKeyType(type);
            keyInfo.setKeyStatus(status);
            if (!TextUtils.isEmpty(name)) {
                keyInfo.setDisplayName(name);
            }

            Context context = getApplicationContext();
            VigDatabase vigDatabase = VigDatabase.getInstance(context);
            KeyRepository keyRepository = new KeyRepository(context, vigDatabase);
            keyRepository.updateToDb(keyInfo);
        }
    }

    private void clearCarKeyInfo(int positionInUi, @CarOptions.KeyType String type, String iKeyUid, String deviceId, long deviceCreateTime) {
        KeyInfo keyInfo = getCarKeyInfo(positionInUi);
        if (keyInfo != null) {
            keyInfo.clear();

            Context context = getApplicationContext();
            VigDatabase vigDatabase = VigDatabase.getInstance(context);
            KeyRepository keyRepository = new KeyRepository(context, vigDatabase);
            keyRepository.updateToDb(keyInfo);
        }
    }

    private void clearCarKeyInfo(@CarOptions.KeyType String type, String iKeyUid, String deviceId, long deviceCreateTime) {
        KeyInfo keyInfo = getCarKeyInfo(type, iKeyUid, deviceId, deviceCreateTime);
        if (keyInfo != null) {
            keyInfo.clear();

            Context context = getApplicationContext();
            VigDatabase vigDatabase = VigDatabase.getInstance(context);
            KeyRepository keyRepository = new KeyRepository(context, vigDatabase);
            keyRepository.updateToDb(keyInfo);
        }
    }
}
