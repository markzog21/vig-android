package com.luxgen.remote.ca;

import android.content.Context;

import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.network.IkeyWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.ikey.ActionData;
import com.luxgen.remote.network.model.ikey.DeviceProfileData;
import com.luxgen.remote.network.model.ikey.IkeyLoginRequestData;
import com.luxgen.remote.network.model.ikey.IkeyLoginResponseData;
import com.luxgen.remote.network.model.ikey.UserProfileData;

import static com.luxgen.remote.ca.CommunicationAgentService.CA_EXTRA_DEVICE_ID_NOT_MATCH_RETURN_CODE;
import static com.luxgen.remote.ca.CommunicationAgentService.CA_EXTRA_DEVICE_TIMESTAMP_NOT_MATCH_RETURN_CODE;

public class IKeyLoginRequest {

    public final static int MAX_RETRY = 3;
    private final static int MAX_LOGIN_RETRY_RETURN_CODE = 987;

    private int retryCount = 0;
    private IkeyWebApi ikeyWebApi;

    private long mActionId;
    private String mAppSecret;
    private UserProfileData mUserProfileData;
    private DeviceProfileData mDeviceProfileData;
    private ActionData mActionData;

    public interface Callback {
        void onSuccess(IkeyLoginResponseData responseData);

        void onFailure(int code);

        void onNetError(String message);
    }

    private Callback mCallback;

    public IKeyLoginRequest(Context context, long actionId, String iKeyUid, String appSecret, String deviceId, String deviceIdCreateTime) {
        ikeyWebApi = IkeyWebApi.getInstance(context);

        mActionId = actionId;
        mAppSecret = appSecret;

        mUserProfileData = new UserProfileData(iKeyUid, "");
        mDeviceProfileData = new DeviceProfileData(deviceId);
        mDeviceProfileData.setCreateTimeStamp(deviceIdCreateTime);
        mActionData = new ActionData("login");
    }

    private OnApiListener<IkeyLoginResponseData> mOnIkeyApiListener = new OnApiListener<IkeyLoginResponseData>() {

        private boolean canRetry(int returnCode) {
            if (returnCode == CA_EXTRA_DEVICE_TIMESTAMP_NOT_MATCH_RETURN_CODE ||
                    returnCode == CA_EXTRA_DEVICE_ID_NOT_MATCH_RETURN_CODE) {
                return false;
            }

            if (retryCount < MAX_RETRY) {
                retryCount++;
                return true;
            }
            return false;
        }

        @Override
        public void onApiTaskSuccessful(IkeyLoginResponseData responseData) {
            if (responseData != null && responseData.isLoginSuccess()) {
                LogCat.d("ikey login 成功");
                if (mCallback != null) {
                    mCallback.onSuccess(responseData);
                }
            } else {
                int returnCode = MAX_LOGIN_RETRY_RETURN_CODE;
                if (responseData != null && responseData.getAction() != null) {
                    returnCode = responseData.getAction().getReturnCode();
                }
                if (canRetry(returnCode)) {
                    LogCat.d("ikey login 失敗，重新來過");
                    start();
                } else {
                    LogCat.d("ikey login 失敗太多:" + returnCode);
                    if (mCallback != null) {
                        mCallback.onFailure(returnCode);
                    }
                }
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            LogCat.d("ikey login 失敗:" + failMessage);
            if (mCallback != null) {
                mCallback.onNetError(failMessage);
            }
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {

        }
    };

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public void start() {
        LogCat.d("進行 ikey login");

        mActionData.setId(String.valueOf(mActionId++));

        IkeyLoginRequestData requestData = new IkeyLoginRequestData(
                mUserProfileData, mDeviceProfileData, mActionData, mAppSecret,
                String.valueOf(System.currentTimeMillis()));

        ikeyWebApi.login(requestData, mOnIkeyApiListener);
    }
}
