package com.luxgen.remote.custom;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.luxgen.remote.ca.CommunicationAgentService;

public abstract class CustomBaseFragmentWithCaService extends CustomBaseFragment {

    protected CommunicationAgentService mCommunicationAgentService = null;

    private ServiceConnection caServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder serviceBinder) {
            mCommunicationAgentService = ((CommunicationAgentService.LocalBinder) serviceBinder).getService();
            onCaServiceConnected();
        }

        public void onServiceDisconnected(ComponentName name) {
            mCommunicationAgentService = null;
            onCaServiceDisconnected();
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindCaService();
    }

    @Override
    public void onDestroy() {
        unbindCaService();
        super.onDestroy();
    }

    public void bindCaService() {
        Intent intent = new Intent(getContext(), CommunicationAgentService.class);
        getActivity().bindService(intent, caServiceConnection, Context.BIND_AUTO_CREATE);
    }

    public void unbindCaService() {
        getContext().unbindService(caServiceConnection);
    }

    protected void onCaServiceConnected() {

    }

    protected void onCaServiceDisconnected() {

    }
}
