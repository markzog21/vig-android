package com.luxgen.remote.custom;

import luxgen.service.AsyncResultSendMessage;
import luxgen.service.IResultListener;

/**
 * Created by evahsu on 2018/2/6.
 */

public class SendObuMsgResultListener implements IResultListener<AsyncResultSendMessage> {
    String tag = "";

    public void setTag(String tag) {
        this.tag = tag;
    }

    public SendObuMsgResultListener() {
    }

    public SendObuMsgResultListener(String tag) {
        this.tag = tag;
    }

    @Override
    public void onRemoteResult(AsyncResultSendMessage result) throws Exception {
        if (result.error == 0) {
            LogCat.d("onRemoteResult:" + new String(result.replyMsg));
//            LogHelper.logToCloud(new String(result.replyMsg),String.format("%s onRemoteResult",tag));
        } else {
            LogCat.d("onRemoteResult err:" + result.toString());
//            LogHelper.logToCloud(result.toString(),String.format("%s onRemoteResult err",tag));
        }
    }
}
