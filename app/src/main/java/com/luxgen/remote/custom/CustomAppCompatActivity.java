package com.luxgen.remote.custom;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.luxgen.remote.R;
import com.luxgen.remote.activity.MainActivity;
import com.luxgen.remote.ca.CommunicationAgentService;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.network.CarWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.assistant.SetIkeyFCMTokenRequestData;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.car.GetCarKeyRequestData;
import com.luxgen.remote.network.model.car.GetCarKeyResponseData;
import com.luxgen.remote.network.model.maintenance.GetCarProgressInWorkshopRequestData;
import com.luxgen.remote.network.model.maintenance.GetCarProgressInWorkshopResponseData;
import com.luxgen.remote.ui.dialog.CustomAlertDialog;
import com.luxgen.remote.ui.dialog.CustomProgressDialog;
import com.luxgen.remote.ui.wheel.WheelActivity;
import com.luxgen.remote.util.BitmapUtils;
import com.luxgen.remote.util.NetUtils;
import com.luxgen.remote.util.Prefs;
import com.luxgen.remote.util.ShakeDetector;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import tw.com.haitec.ae.project.smartphoneikey.snappydb.IKeyDataBean;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.SnapKeyDataBean;

public class CustomAppCompatActivity extends AppCompatActivity {

    protected static class InnerHandler extends Handler {
        WeakReference<CustomAppCompatActivity> parentClass;

        InnerHandler(CustomAppCompatActivity activity) {
            parentClass = new WeakReference<CustomAppCompatActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            CustomAppCompatActivity activity = parentClass.get();
            if (activity != null) {
                activity.handleMessage(msg);
            }
        }
    }

    protected InnerHandler mMessageHandler = null;
    protected CommunicationAgentService mCommunicationAgentService = null;
    private IntentFilter mIntentFilter = new IntentFilter();

    private UserInfo mUserInfo = null;
    private String mReceivedIntentAction = null;
    private int mReceivedReturnCode = 0;

    private CustomProgressDialog mProgressDialog = null;
    private boolean mReShowProgressDialog = false;
    protected CustomAlertDialog mAlertDialog1 = null;
    private boolean mReShowAlertDialog1 = false;
    protected CustomAlertDialog mAlertDialog2 = null;
    private boolean mReShowAlertDialog2 = false;
    private View.OnClickListener mOnConfirmDialogPositiveButtonClickListener = null;

    private String mFcmToken = "";
    private boolean mBoundToCAService = false;

    private LinearLayout mScreenLayout;
    private ImageView mImageViewWheelRightHandle;
    private ImageView mImageViewWheelLeftHandle;
    private int mWheelHandleCurrentPosition = CommonDefs.WHEEL_HANDLE_POSITION_RIGHT;
    private String mIKeyUid = null;
    private SharedPreferences mWheelSharedPreferences = null;
    private GestureDetectorCompat mWheelGestureDetector = null;
    private SensorManager mWheelSensorManager = null;
    private ShakeDetector mWheelShakeDetector = null;
    private View mWheelTheLastTouchedView = null;

    protected View.OnTouchListener mOnGlobalTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            mWheelTheLastTouchedView = v;
            return mWheelGestureDetector.onTouchEvent(event);
        }
    };

    private ShakeDetector.Listener mShakeDetectorListener = new ShakeDetector.Listener() {
        @Override
        public void hearShake() {
            startWheelMenu();
        }
    };

    private OnSwipeListener mOnSwipeListener = new OnSwipeListener() {
        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            if (mWheelTheLastTouchedView != null) {
                mWheelTheLastTouchedView.performClick();
                mWheelTheLastTouchedView = null;
            } else {
                startWheelMenu();
            }
            return super.onSingleTapConfirmed(e);
        }

        @Override
        public boolean onSwipe(OnSwipeListener.Direction direction) {
            if ((direction == Direction.right) &&
                    (mWheelHandleCurrentPosition == CommonDefs.WHEEL_HANDLE_POSITION_LEFT) ||
                    (direction == Direction.left) &&
                            (mWheelHandleCurrentPosition == CommonDefs.WHEEL_HANDLE_POSITION_RIGHT)) {
                startWheelMenu();
            }
            return super.onSwipe(direction);
        }
    };

    private View.OnClickListener mOnHandleImageViewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startWheelMenu();
        }
    };

    private View.OnTouchListener mOnHandleImageViewTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return mWheelGestureDetector.onTouchEvent(event);
        }
    };

    private BroadcastReceiver mCaStatusBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            onReceiveBroadcastFromCaService(context, intent);
        }
    };

    private ServiceConnection caServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder serviceBinder) {
            dismissProgressDialog();
            mCommunicationAgentService = ((CommunicationAgentService.LocalBinder) serviceBinder).getService();
            if (mCommunicationAgentService != null) {
                mCommunicationAgentService.doHeavyPolling(true);
            }
            onCaServiceConnected();
        }

        public void onServiceDisconnected(ComponentName name) {
            dismissProgressDialog();
            mCommunicationAgentService = null;
            onCaServiceDisconnected();
        }
    };

    private OnApiListener<GetCarKeyResponseData> mGetCarKeyListApiListener = new OnApiListener<GetCarKeyResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetCarKeyResponseData responseData) {
            dismissProgressDialog();

            if (responseData.isSuccessful()) {
                CarKeyData selectedCarKeyData = Prefs.getCarKeyData(CustomAppCompatActivity.this);
                boolean isMatched = false;
                ArrayList<CarKeyData> carKeyDataList = responseData.getCarKeyDataList();
                for (CarKeyData carKeyData : carKeyDataList) {
                    if (selectedCarKeyData.getCarId().equals(carKeyData.getCarId()) &&
                            selectedCarKeyData.getCarNo().equals(carKeyData.getCarNo())) {
                        selectedCarKeyData = carKeyData;
                        Prefs.setCarKeyData(CustomAppCompatActivity.this, selectedCarKeyData);
                        Prefs.setCarKeyDataWithIKeyUid(CustomAppCompatActivity.this, mUserInfo.getKeyUid(), selectedCarKeyData);
                        onReloadSelectedCarKeyData(selectedCarKeyData);
                        isMatched = true;
                        break;
                    }
                }

                if (isMatched) {
                    if (CommunicationAgentService.CA_STATUS_ADD_MASTERKEY.equals(mReceivedIntentAction)) {
                        switch (mReceivedReturnCode) {
                            case CommunicationAgentService.CA_EXTRA_ADD_MASTERKEY_SUCCESS: {
                                showConfirmDialog(getString(R.string.dialog_title_remind),
                                        getString(R.string.dialog_message_master_key_added));
                                break;
                            }

                            default: {
                                showConfirmDialog(getString(R.string.dialog_title_remind),
                                        getString(R.string.digital_key_master_key_dialog_activate_master_key_fail_message));
                            }
                        }
                    } else if (CommunicationAgentService.CA_STATUS_DEL_MASTERKEY.equals(mReceivedIntentAction)) {
                        showConfirmDialog(getString(R.string.dialog_title_remind),
                                getString(R.string.digital_key_master_key_dialog_master_key_deleted_message));
                    }
                } else {
                    if (CommunicationAgentService.CA_STATUS_ADD_MASTERKEY.equals(mReceivedIntentAction)) {
                        showConfirmDialog(getString(R.string.dialog_title_remind),
                                getString(R.string.digital_key_master_key_dialog_activate_master_key_fail_message));
                    } else if (CommunicationAgentService.CA_STATUS_DEL_MASTERKEY.equals(mReceivedIntentAction)) {
                        showConfirmDialog(getString(R.string.dialog_title_remind),
                                getString(R.string.digital_key_master_key_dialog_delete_master_key_fail_message));
                    }
                }

            } else {
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }

            mUserInfo = null;
            mReceivedIntentAction = null;
            mReceivedReturnCode = 0;
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            mUserInfo = null;
            mReceivedIntentAction = null;
            mReceivedReturnCode = 0;
            LogCat.d("mGetCarKeyListApiListener: " + failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            dismissProgressDialog();
            mUserInfo = null;
            mReceivedIntentAction = null;
            mReceivedReturnCode = 0;
            showMultiLoginErrorDialog();
        }
    };

    private void initWheelMenu() {
        mWheelSharedPreferences = getSharedPreferences("wheelmenu", Context.MODE_PRIVATE);
        mIKeyUid = Prefs.getKeyUid(this);

        mWheelSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mWheelShakeDetector = new ShakeDetector(mShakeDetectorListener);
        mWheelGestureDetector = new GestureDetectorCompat(this, mOnSwipeListener);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mMessageHandler == null) {
            mMessageHandler = new InnerHandler(this);
        }

        if (mProgressDialog == null) {
            mProgressDialog = new CustomProgressDialog(this, R.style.CustomProgressDialog);
            mProgressDialog.setCancelable(false);
        }

        if (mAlertDialog1 == null) {
            mAlertDialog1 = new CustomAlertDialog(this, R.style.CustomDialog);
            mAlertDialog1.setCancelable(false);
        }

        if (mAlertDialog2 == null) {
            mAlertDialog2 = new CustomAlertDialog(this, R.style.CustomDialog);
            mAlertDialog2.setCancelable(false);
        }

        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_ADD_MASTERKEY);
        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_DEL_MASTERKEY);
        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_ADD_SUBKEY);
        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_DEL_SUBKEY);
        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_ALLOCATE_SNAPKEY);
        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_TERMINATE_SNAPKEY);
        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_MASTERKEY_DELETED);
        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_SUBKEY_ADDED);
        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_SUBKEY_DELETED);
        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_SNAPKEY_ADDED);
        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_SNAPKEY_DELETED);
        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_VIG_WAKEUP_FAILED);
        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_IKEY_FUNCTOSERVER_TIMEOUT);
        addActionToReceiveBroadcastFromCaService(mIntentFilter);

        initWheelMenu();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mReShowProgressDialog) {
            mReShowProgressDialog = false;
            mMessageHandler.sendEmptyMessage(CommonDefs.INNER_MSG_WHAT_RESHOW_PROGRESS_DIALOG);
        }

        if (mReShowAlertDialog1) {
            mReShowAlertDialog1 = false;
            mMessageHandler.sendEmptyMessage(CommonDefs.INNER_MSG_WHAT_RESHOW_ALERT_DIALOG_1);
        }

        if (mReShowAlertDialog2) {
            mReShowAlertDialog2 = false;
            mMessageHandler.sendEmptyMessage(CommonDefs.INNER_MSG_WHAT_RESHOW_ALERT_DIALOG_2);
        }

        checkWheelHandlePositionAndDisplay();
        if (isShakeGestureEnabled()) {
            mWheelShakeDetector.start(mWheelSensorManager);
        } else {
            mWheelShakeDetector.stop();
        }

        registerCaStatusBroadcastReceiver();

        if (mIKeyUid != null) {
            if (mBoundToCAService && mCommunicationAgentService != null) {
                mCommunicationAgentService.doHeavyPolling(true);
            }
        }

        callAPIToGetCarProgressInWorkshop(null);
    }

    @Override
    protected void onPause() {
        if (mProgressDialog.isShowing()) {
            mReShowProgressDialog = true;
            mProgressDialog.dismiss();
        }

        if (mAlertDialog1.isShowing()) {
            mReShowAlertDialog1 = true;
            mAlertDialog1.dismiss();
        }

        if (mAlertDialog2.isShowing()) {
            mReShowAlertDialog2 = true;
            mAlertDialog2.dismiss();
        }

        mWheelShakeDetector.stop();

        unregisterCaStatusBroadcastReceiver();

        if (mIKeyUid != null) {
            if (mBoundToCAService && mCommunicationAgentService != null) {
                mCommunicationAgentService.doHeavyPolling(false);
            }
        }
        super.onPause();
    }

    private boolean isOnRight() {
        return mWheelSharedPreferences.getBoolean(mIKeyUid + "_onRight", true);
    }

    private boolean isShakeGestureEnabled() {
        return mWheelSharedPreferences.getBoolean(mIKeyUid + "_shakeGesture", false);
    }

    private void checkWheelHandlePositionAndDisplay() {
        if (mImageViewWheelRightHandle == null || mImageViewWheelLeftHandle == null) {
            return;
        }

        if (!Prefs.isWheelEnabled(this)) {
            mImageViewWheelRightHandle.setVisibility(View.GONE);
            mImageViewWheelLeftHandle.setVisibility(View.GONE);
            return;
        }

        if (isOnRight()) {
            mWheelHandleCurrentPosition = CommonDefs.WHEEL_HANDLE_POSITION_RIGHT;
        } else {
            mWheelHandleCurrentPosition = CommonDefs.WHEEL_HANDLE_POSITION_LEFT;
        }

        switch (mWheelHandleCurrentPosition) {
            case CommonDefs.WHEEL_HANDLE_POSITION_RIGHT: {
                mImageViewWheelRightHandle.setVisibility(View.VISIBLE);
                mImageViewWheelLeftHandle.setVisibility(View.GONE);
            }
            break;

            case CommonDefs.WHEEL_HANDLE_POSITION_LEFT: {
                mImageViewWheelRightHandle.setVisibility(View.GONE);
                mImageViewWheelLeftHandle.setVisibility(View.VISIBLE);
            }
            break;

            default:
                mImageViewWheelRightHandle.setVisibility(View.VISIBLE);
                mImageViewWheelLeftHandle.setVisibility(View.GONE);
        }
    }

    protected void setWheelHandles(ImageView rightHandle, ImageView leftHandle) {
        mImageViewWheelRightHandle = rightHandle;
        if (mImageViewWheelRightHandle != null) {
            mImageViewWheelRightHandle.setOnClickListener(mOnHandleImageViewClickListener);
            mImageViewWheelRightHandle.setOnTouchListener(mOnHandleImageViewTouchListener);
        }

        mImageViewWheelLeftHandle = leftHandle;
        if (mImageViewWheelLeftHandle != null) {
            mImageViewWheelLeftHandle.setOnClickListener(mOnHandleImageViewClickListener);
            mImageViewWheelLeftHandle.setOnTouchListener(mOnHandleImageViewTouchListener);
        }
        checkWheelHandlePositionAndDisplay();
    }

    protected void setWheelScreenLayout(LinearLayout layout) {
        mScreenLayout = layout;
        if (mScreenLayout != null) {
            mScreenLayout.setOnTouchListener(mOnGlobalTouchListener);
        }
    }

    private void startWheelMenu() {
        if (!Prefs.isWheelEnabled(this)) {
            return;
        }

        if (mScreenLayout == null) {
            return;
        }
        Bitmap background = BitmapUtils.getBlurScreenShot(this, mScreenLayout);
        BitmapUtils.saveImage(this, background, "background");

        startActivity(WheelActivity.newIntent(this));
        overridePendingTransition(0, 0);
    }

    protected void onReloadSelectedCarKeyData(CarKeyData selectedCarKeyData) {

    }

    protected void onCaServiceConnected() {

    }

    protected void onCaServiceDisconnected() {

    }

    public void bindCaService() {
        showProgressDialog();
        Intent intent = new Intent(this, CommunicationAgentService.class);
        bindService(intent, caServiceConnection, BIND_AUTO_CREATE);
        mBoundToCAService = true;
    }

    public void unbindCaService() {
        unbindService(caServiceConnection);
        mBoundToCAService = false;
    }

    private void registerCaStatusBroadcastReceiver() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mCaStatusBroadcastReceiver, mIntentFilter);
    }

    private void unregisterCaStatusBroadcastReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mCaStatusBroadcastReceiver);
    }

    protected void addActionToReceiveBroadcastFromCaService(IntentFilter intentFilter) {

    }

    protected void onReceiveBroadcastFromCaService(Context context, Intent intent) {
        mReceivedIntentAction = intent.getAction();
        if (CommunicationAgentService.CA_STATUS_ADD_MASTERKEY.equals(mReceivedIntentAction)) {
            mReceivedReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            reloadSelectedCarKeyData();

        } else if (CommunicationAgentService.CA_STATUS_DEL_MASTERKEY.equals(mReceivedIntentAction)) {
            mReceivedReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            if (mReceivedReturnCode == CommunicationAgentService.CA_EXTRA_DEL_MASTERKEY_COMPLETE) {
                reloadSelectedCarKeyData();
            } else if (mReceivedReturnCode == CommunicationAgentService.CA_EXTRA_DEL_MASTERKEY_FAILED ||
                    mReceivedReturnCode == CommunicationAgentService.CA_EXTRA_DEL_MASTERKEY_NO_DATA) {
                showConfirmDialog(getString(R.string.dialog_title_remind),
                        getString(R.string.digital_key_master_key_dialog_delete_master_key_fail_message));
            }

        } else if (CommunicationAgentService.CA_STATUS_VIG_WAKEUP_FAILED.equals(mReceivedIntentAction)) {
            showConfirmDialog(getString(R.string.dialog_title_vig_connect_fail),
                    getString(R.string.dialog_message_vig_connect_fail));

        } else if (CommunicationAgentService.CA_STATUS_ADD_SUBKEY.equals(mReceivedIntentAction)) {
            int extraReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            if (extraReturnCode == CommunicationAgentService.CA_EXTRA_ADD_SUBKEY_SUCCESS) {
                showConfirmDialog(getString(R.string.add_new_sub_key_dialog_sub_key_sent_title),
                        getString(R.string.add_new_sub_key_dialog_sub_key_sent_message));
            } else {
                showConfirmDialog(getString(R.string.add_new_sub_key_dialog_add_sub_key_fail_title),
                        getString(R.string.add_new_sub_key_dialog_add_sub_key_fail_message));
            }

        } else if (CommunicationAgentService.CA_STATUS_DEL_SUBKEY.equals(mReceivedIntentAction)) {
            int extraReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            if (extraReturnCode == CommunicationAgentService.CA_EXTRA_DEL_SUBKEY_SUCCESS_START) {
                showConfirmDialog(getString(R.string.edit_digital_key_dialog_sub_key_deleted_title),
                        getString(R.string.edit_digital_key_dialog_sub_key_deleted_message));
            } else {
                showConfirmDialog(getString(R.string.edit_digital_key_dialog_delete_sub_key_fail_title),
                        getString(R.string.edit_digital_key_dialog_delete_sub_key_fail_message));
            }

        } else if (CommunicationAgentService.CA_STATUS_ALLOCATE_SNAPKEY.equals(mReceivedIntentAction)) {
            int extraReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            if (extraReturnCode == CommunicationAgentService.CA_EXTRA_ALLOCATE_SNAPKEY_SUCCESS) {
                showConfirmDialog(getString(R.string.set_temp_key_dialog_temp_key_sent_title),
                        getString(R.string.set_temp_key_dialog_temp_key_sent_message));
            } else {
                showConfirmDialog(getString(R.string.set_temp_key_dialog_set_temp_key_fail_title),
                        getString(R.string.set_temp_key_dialog_set_temp_key_fail_message));
            }

        } else if (CommunicationAgentService.CA_STATUS_TERMINATE_SNAPKEY.equals(mReceivedIntentAction)) {
            int extraReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            if (extraReturnCode == CommunicationAgentService.CA_EXTRA_TERMINATE_SNAPKEY_SUCCESS) {
                showConfirmDialog(getString(R.string.edit_digital_key_dialog_temp_key_deleted_title),
                        getString(R.string.edit_digital_key_dialog_temp_key_deleted_message));
            } else {
                showConfirmDialog(getString(R.string.edit_digital_key_dialog_delete_temp_key_fail_title),
                        getString(R.string.edit_digital_key_dialog_delete_temp_key_fail_message));
            }

        } else if (CommunicationAgentService.CA_STATUS_MASTERKEY_DELETED.equals(mReceivedIntentAction)) {
            CarKeyData selectedCarKeyData = Prefs.getCarKeyData(this);
            String deletedVigCarId = intent.getStringExtra(CommunicationAgentService.CA_STATUS_EXTRA_COMMAND);
            if (selectedCarKeyData != null && selectedCarKeyData.isMasterKey() && selectedCarKeyData.getVigCarId().equals(deletedVigCarId)) {
                showSelectedCarKeyDataDeletedDialog(selectedCarKeyData);
            } else {
                showConfirmDialog(getString(R.string.dialog_title_remind),
                        getString(R.string.dialog_message_master_key_deleted));

                // 不用做 Prefs.setRefreshCarKeyListFlag()，在選車的時候就會更新。
            }

        } else if (CommunicationAgentService.CA_STATUS_SUBKEY_ADDED.equals(mReceivedIntentAction)) {
            showConfirmDialog(getString(R.string.dialog_title_sub_key_added),
                    getString(R.string.dialog_message_sub_key_added));
            boolean isDMS = Prefs.getUserInfo(this).isDMS();
            int currentNumOfCarKeyData = Prefs.getCurrentNumOfCarKeyData(this);
            CarKeyData selectedCarKeyData = Prefs.getCarKeyData(this);
            if (selectedCarKeyData == null || (selectedCarKeyData != null && currentNumOfCarKeyData == 1 && isDMS)) {
                Prefs.setRefreshCarKeyListFlag(this, Prefs.FLAG_REFRESH_CAR_KEY_LIST);
            }

        } else if (CommunicationAgentService.CA_STATUS_SUBKEY_DELETED.equals(mReceivedIntentAction)) {
            CarKeyData selectedCarKeyData = Prefs.getCarKeyData(this);
            String deletedVigCarId = intent.getStringExtra(CommunicationAgentService.CA_STATUS_EXTRA_COMMAND);
            if (selectedCarKeyData != null && selectedCarKeyData.isSubKey() && selectedCarKeyData.getVigCarId().equals(deletedVigCarId)) {
                showSelectedCarKeyDataDeletedDialog(selectedCarKeyData);
            } else {
                showConfirmDialog(getString(R.string.dialog_title_remind),
                        getString(R.string.dialog_message_sub_key_deleted));
                boolean isDMS = Prefs.getUserInfo(this).isDMS();
                int currentNumOfCarKeyData = Prefs.getCurrentNumOfCarKeyData(this);
                if ((selectedCarKeyData != null && currentNumOfCarKeyData == 2 && isDMS) || (currentNumOfCarKeyData < 2)) {
                    Prefs.setRefreshCarKeyListFlag(this, Prefs.FLAG_REFRESH_CAR_KEY_LIST);
                }
            }

        } else if (CommunicationAgentService.CA_STATUS_SNAPKEY_ADDED.equals(mReceivedIntentAction)) {
            showConfirmDialog(getString(R.string.dialog_title_remind),
                    getString(R.string.dialog_message_snap_key_added));
            boolean isDMS = Prefs.getUserInfo(this).isDMS();
            int currentNumOfCarKeyData = Prefs.getCurrentNumOfCarKeyData(this);
            CarKeyData selectedCarKeyData = Prefs.getCarKeyData(this);
            if (selectedCarKeyData == null || (selectedCarKeyData != null && currentNumOfCarKeyData == 1 && isDMS)) {
                Prefs.setRefreshCarKeyListFlag(this, Prefs.FLAG_REFRESH_CAR_KEY_LIST);
            }
            // TODO: 流程與 subkey 相同
        } else if (CommunicationAgentService.CA_STATUS_SNAPKEY_DELETED.equals(mReceivedIntentAction)) {
            UserInfo userInfo = Prefs.getUserInfo(this);
            CarKeyData selectedCarKeyData = Prefs.getCarKeyData(this);
            // TODO: 流程與 subkey 相同
            if (mCommunicationAgentService != null && userInfo != null && selectedCarKeyData != null && selectedCarKeyData.isSnapKey()) {
                boolean isKeyActive = false;
                ArrayList<SnapKeyDataBean> snapKeyDataList = mCommunicationAgentService.getCA().getSnapKeyDataListByCarId(selectedCarKeyData.getVigCarId());
                for (SnapKeyDataBean bean : snapKeyDataList) {
                    if (bean.getUserId().equals(userInfo.getKeyUid()) &&
                            bean.getDeviceId().equals(userInfo.getDeviceId()) &&
                            bean.getCarId().equals(selectedCarKeyData.getVigCarId()) &&
                            bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                        isKeyActive = true;
                        break;
                    }
                }

                if (isKeyActive) {
                    showConfirmDialog(getString(R.string.dialog_title_remind),
                            getString(R.string.dialog_message_snap_key_deleted));
                    boolean isDMS = Prefs.getUserInfo(this).isDMS();
                    int currentNumOfCarKeyData = Prefs.getCurrentNumOfCarKeyData(this);
                    if ((selectedCarKeyData != null && currentNumOfCarKeyData == 2 && isDMS) || (currentNumOfCarKeyData < 2)) {
                        Prefs.setRefreshCarKeyListFlag(this, Prefs.FLAG_REFRESH_CAR_KEY_LIST);
                    }
                } else {
                    showSelectedCarKeyDataDeletedDialog(selectedCarKeyData);
                }

            } else {
                showConfirmDialog(getString(R.string.dialog_title_remind),
                        getString(R.string.dialog_message_snap_key_deleted));
                boolean isDMS = Prefs.getUserInfo(this).isDMS();
                int currentNumOfCarKeyData = Prefs.getCurrentNumOfCarKeyData(this);
                if ((selectedCarKeyData != null && currentNumOfCarKeyData == 2 && isDMS) || (currentNumOfCarKeyData < 2)) {
                    Prefs.setRefreshCarKeyListFlag(this, Prefs.FLAG_REFRESH_CAR_KEY_LIST);
                }
            }
        }
    }

    protected void onServerErrorDialogShown() {

    }

    protected void showProgressDialog() {
        showProgressDialog(null);
    }

    protected void showProgressDialog(String message) {
        if (!mProgressDialog.isShowing() && !mReShowProgressDialog) {
            mProgressDialog.setMessage(message);
            mProgressDialog.show();
        }
    }

    protected void dismissProgressDialog() {
        mReShowProgressDialog = false;
        mProgressDialog.dismiss();
    }

    private void showAlertDialog1() {
        if (!mAlertDialog1.isShowing()) {
            mAlertDialog1.show();
        }
    }

    private void showAlertDialog2() {
        if (!mAlertDialog2.isShowing()) {
            mAlertDialog2.show();
        }
    }

    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case CommonDefs.INNER_MSG_WHAT_RESHOW_PROGRESS_DIALOG: {
                showProgressDialog();
                break;
            }

            case CommonDefs.INNER_MSG_WHAT_RESHOW_ALERT_DIALOG_1: {
                showAlertDialog1();
                break;
            }

            case CommonDefs.INNER_MSG_WHAT_RESHOW_ALERT_DIALOG_2: {
                showAlertDialog2();
                break;
            }

            default:
                break;
        }
    }

    protected void showNoLongerRemindDialog(String message, String prefKey) {
        if (mAlertDialog1.isShowing()) {
            mAlertDialog1.dismiss();
        }
        mAlertDialog1.restoreDefault();
        mAlertDialog1.setTitle(getString(R.string.dialog_title_remind));
        mAlertDialog1.setMessage(message);
        mAlertDialog1.setCheckBoxData(getString(R.string.dialog_checkbox_no_longer_remind), prefKey);
        mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                getResources().getString(R.string.btn_confirm),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mAlertDialog1.dismiss();
                    }
                });
        mAlertDialog1.show();
    }

    protected void showConfirmDialog(String title, String message, View.OnClickListener customOnClickListener) {
        mOnConfirmDialogPositiveButtonClickListener = customOnClickListener;

        if (mAlertDialog1.isShowing()) {
            mAlertDialog1.dismiss();
        }
        mAlertDialog1.restoreDefault();
        mAlertDialog1.setTitle(title);
        mAlertDialog1.setMessage(message);
        mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                getResources().getString(R.string.btn_confirm),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (mOnConfirmDialogPositiveButtonClickListener != null) {
                            mOnConfirmDialogPositiveButtonClickListener.onClick(v);
                        }

                        mAlertDialog1.dismiss();
                    }
                });
        mAlertDialog1.show();
    }

    protected void showConfirmDialog(String title, String message) {
        showConfirmDialog(title, message, null);
    }

    protected void showServerErrorDialog() {
        showServerErrorDialog("", "", null);
    }

    protected void showServerErrorDialog(String title, String errorMessage) {
        showServerErrorDialog(title, errorMessage, null);
    }

    protected void showServerErrorDialog(String title, String errorMessage, View.OnClickListener customOnClickListener) {
        if (NetUtils.isConnectToInternet(this)) {
            showConfirmDialog(title, errorMessage, customOnClickListener);
        } else {
            showConfirmDialog(getString(R.string.dialog_title_error), getString(R.string.dialog_message_no_internet), customOnClickListener);
            onServerErrorDialogShown();
        }
    }

    protected void showMultiLoginErrorDialog() {
        showConfirmDialog(getString(R.string.dialog_title_remind), getString(R.string.dialog_message_multi_login), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishApplication();
            }
        });
    }

    protected void showSelectedCarKeyDataDeletedDialog(CarKeyData selectedCarKeyData) {
        if (mAlertDialog2.isShowing()) {
            mAlertDialog2.dismiss();
        }
        mAlertDialog2.restoreDefault();
        if (selectedCarKeyData.isMasterKey()) {
            mAlertDialog2.setTitle(getString(R.string.digital_key_master_key_dialog_receive_master_key_deleted_title));
            mAlertDialog2.setMessage(getString(R.string.digital_key_master_key_dialog_receive_master_key_deleted_message));
            mAlertDialog2.setButton(DialogInterface.BUTTON_POSITIVE,
                    getString(R.string.btn_confirm),
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            refreshCarKeyData();
                            mAlertDialog2.dismiss();
                        }
                    });
        } else {
            mAlertDialog2.setTitle(getString(R.string.dialog_title_remind));
            mAlertDialog2.setMessage(getString(R.string.dialog_message_selected_key_deleted));
            mAlertDialog2.setButton(DialogInterface.BUTTON_POSITIVE,
                    getString(R.string.btn_confirm),
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            reselectCarKeyData();
                            mAlertDialog2.dismiss();
                        }
                    });
        }
        mAlertDialog2.show();
    }

    private void finishApplication() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(MainActivity.INTENT_EXTRA_NAME_FORCE_EXIT_APP, true);
        startActivity(intent);
    }

    private void reselectCarKeyData() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(MainActivity.INTENT_EXTRA_NAME_RESELECT_CAR_KEY_DATA, true);
        startActivity(intent);
    }

    private void refreshCarKeyData() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(MainActivity.INTENT_EXTRA_NAME_REFRESH_CAR_KEY_LIST, true);
        startActivity(intent);
    }

    private void reloadSelectedCarKeyData() {
        showProgressDialog();
        callAPIToGetCarKeyList();
    }

    private void callAPIToGetCarKeyList() {
        mUserInfo = Prefs.getUserInfo(this);
        if (mUserInfo != null) {
            GetCarKeyRequestData data = new GetCarKeyRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken());

            CarWebApi carWebApi = CarWebApi.getInstance(this);
            carWebApi.getCarKeyList(data, mGetCarKeyListApiListener);
        } else {
            dismissProgressDialog();
        }
    }

    /* Start: About FCM */
    protected void setFcmToken() {
        try {
            if (!NetUtils.isConnectToInternet(this)) {
                return;
            }

            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                @Override
                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        mFcmToken = task.getResult().getToken();
                        UserInfo userInfo = Prefs.getUserInfo(CustomAppCompatActivity.this);
                        Log.i("Firebase Id", task.getResult().getToken());

                        if (userInfo != null && userInfo.getKeyUid() != null) {

                            SetIkeyFCMTokenRequestData data = new SetIkeyFCMTokenRequestData(
                                    userInfo.getKeyUid(), userInfo.getToken(), mFcmToken);
                            AssistantWebApi.getInstance(CustomAppCompatActivity.this).setFcmToken(data, mSetFcmTokenListener);
                        }
                    }

                    initFcm();
                }
            });
        } catch (Exception e) {
            LogCat.e(e.toString());
        }
    }

    protected void initFcm() {

        if (TextUtils.isEmpty(mFcmToken)) {
            LogCat.v("FCM Token: " + Prefs.getFcmToken(this));
        } else {
            LogCat.v("FCM Token: " + mFcmToken);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = getString(R.string.notification_channel_id);
            String channelName = getString(R.string.notification_channel_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            NotificationChannel channel = new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW);
            channel.setShowBadge(true);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private OnApiListener<ResponseData> mSetFcmTokenListener = new OnApiListener<ResponseData>() {
        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            if (!TextUtils.isEmpty(mFcmToken)) {
                Prefs.setFcmToken(getApplicationContext(), mFcmToken);
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {

        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {

        }
    };
    /* End: About FCM */

    public void callAPIToGetCarProgressInWorkshop(final OnApiListener<GetCarProgressInWorkshopResponseData> listener) {
        mUserInfo = Prefs.getUserInfo(this);
        if (mUserInfo == null) {
            return;
        }
        CarKeyData selectedCarKeyData = Prefs.getCarKeyData(this);
        if(selectedCarKeyData == null) {
            return;
        }
        GetCarProgressInWorkshopRequestData data = new GetCarProgressInWorkshopRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken(), selectedCarKeyData.getCarNo());
        AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(this);
        if(listener == null) {
            assistantWebApi.getCarProgressInWorkshop(data, mGetCarProgressInWorkshopApiListener);
        } else {
            assistantWebApi.getCarProgressInWorkshop(data, listener);
        }
    }

    private OnApiListener<GetCarProgressInWorkshopResponseData> mGetCarProgressInWorkshopApiListener = new OnApiListener<GetCarProgressInWorkshopResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetCarProgressInWorkshopResponseData responseData) {
            if (responseData.isSuccessful()) {
                if(responseData.getWsResult() != null) {
                    LogCat.d("Workshop-Progress:" + responseData.getWsResult().getmProgress());
                    Prefs.setRepairProgress(getApplicationContext(), responseData.getWsResult().getmProgress());
                }
            } else {
                if(responseData.getErrorCode().equalsIgnoreCase("-1017") || responseData.getErrorCode().equalsIgnoreCase("-889")) {
                    LogCat.d("Workshop-getErrorMessage:"+responseData.getErrorMessage());
                    Prefs.setRepairProgress(getApplicationContext(), "0");
                }
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            LogCat.e("Workshop-failMessage:"+failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {

        }
    };
}
