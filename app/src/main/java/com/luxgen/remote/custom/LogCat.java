package com.luxgen.remote.custom;

import android.util.Log;

import com.luxgen.remote.BuildConfig;
import com.luxgen.remote.model.UserInfo;

public class LogCat {
    private static final String TAG = "LUXGEN";
    private static boolean mDebug = BuildConfig.DEBUG;

    public static void e(String msg) {
        Log.e(TAG, msg);
    }

    public static void w(String msg) {
        Log.w(TAG, msg);
    }

    public static void d(String msg) {
        if (mDebug) {
            Log.d(TAG, msg);
        }
    }

    public static void i(String msg) {
        if (mDebug) {
            Log.i(TAG, msg);
        }
    }

    public static void v(String msg) {
        if (mDebug) {
            Log.v(TAG, msg);
        }
    }

    public static void printUser(String tag, UserInfo user) {
        if (user == null) {
            return;
        }
        final String temp = "\n" +
                "~~ UserInfo 有資料 ~~ \n" +
                "email: {email} \n" +
                "key uid: {uid} \n" +
                "token: {token} \n";
        Log.i(TAG + "." + tag,
                temp.replace("{email}", user.getEmail())
                        .replace("{uid}", user.getKeyUid())
                        .replace("{token}", user.getToken()));
    }
}

