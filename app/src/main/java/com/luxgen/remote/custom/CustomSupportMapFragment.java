package com.luxgen.remote.custom;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.maps.SupportMapFragment;
import com.luxgen.remote.ca.CommunicationAgentService;

public class CustomSupportMapFragment extends SupportMapFragment {

    private IntentFilter mIntentFilter = new IntentFilter();

    private BroadcastReceiver mCaStatusBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            onReceiveBroadcastFromCaService(context, intent);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_ADD_SUBKEY);
        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_DEL_SUBKEY);
        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_ALLOCATE_SNAPKEY);
        mIntentFilter.addAction(CommunicationAgentService.CA_STATUS_TERMINATE_SNAPKEY);
        addActionToReceiveBroadcastFromCaService(mIntentFilter);
    }

    @Override
    public void onResume() {
        super.onResume();
        registerCaStatusBroadcastReceiver();
    }

    @Override
    public void onPause() {
        unregisterCaStatusBroadcastReceiver();
        super.onPause();
    }

    private void registerCaStatusBroadcastReceiver() {
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mCaStatusBroadcastReceiver, mIntentFilter);
    }

    private void unregisterCaStatusBroadcastReceiver() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mCaStatusBroadcastReceiver);
    }

    protected void addActionToReceiveBroadcastFromCaService(IntentFilter intentFilter) {

    }

    protected void onReceiveBroadcastFromCaService(Context context, Intent intent) {

    }
}
