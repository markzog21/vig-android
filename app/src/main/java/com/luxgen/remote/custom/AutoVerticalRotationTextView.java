package com.luxgen.remote.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.github.mikephil.charting.utils.Utils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class AutoVerticalRotationTextView extends TextSwitcher implements ViewSwitcher.ViewFactory {

    public static class DisplayData {
        public ArrayList<Drawable> mDrawableList = new ArrayList<>();
        public ArrayList<String> mTextList = new ArrayList<>();
    }

    private static class InnerHandler extends Handler {
        WeakReference<AutoVerticalRotationTextView> parentClass;

        InnerHandler(AutoVerticalRotationTextView view) {
            parentClass = new WeakReference<>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            AutoVerticalRotationTextView view = parentClass.get();
            if (view != null) {
                view.handleMessage(msg);
            }
        }
    }

    private static final int DEFAULT_TEXT_SIZE = 14; // px
    private static final int DEFAULT_DRAWABLE_PADDING = 14; // px

    private static final int FLAG_START_AUTO_SCROLL = 0;
    private static final int FLAG_STOP_AUTO_SCROLL = 1;

    private float mTextSize = DEFAULT_TEXT_SIZE;
    private int mTextColor = Color.WHITE;
    private Drawable mDrawableStart = null;
    private Drawable mDrawableTop = null;
    private Drawable mDrawableEnd = null;
    private Drawable mDrawableBottom = null;
    private int mDrawablePadding = 0;
    private long mStillTime = 0;
    private Context mContext;
    private int mIndex = -1;

    private DisplayData mDisplayData = null;
    private InnerHandler mHandler;

    private String mCurrentString = "";

    public AutoVerticalRotationTextView(Context context) {
        this(context, null);
    }

    public AutoVerticalRotationTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        int[] attrsArray = new int[]{ // 如果中間有屬性沒有在xml設定，則之後的屬性都會抓不到
                android.R.attr.textSize, // 0
                android.R.attr.textColor, // 1
                android.R.attr.drawablePadding, // 2
                android.R.attr.drawableStart, // 3
                android.R.attr.drawableTop, // 4
                android.R.attr.drawableEnd, // 5
                android.R.attr.drawableBottom // 6

        };

        TypedArray typedArray = context.obtainStyledAttributes(attrs, attrsArray);
        mTextSize = typedArray.getDimensionPixelSize(0, DEFAULT_TEXT_SIZE);
        mTextColor = typedArray.getColor(1, Color.WHITE);
        mDrawablePadding = typedArray.getDimensionPixelSize(2, DEFAULT_DRAWABLE_PADDING);
        mDrawableStart = typedArray.getDrawable(3);
        mDrawableTop = typedArray.getDrawable(4);
        mDrawableEnd = typedArray.getDrawable(5);
        mDrawableBottom = typedArray.getDrawable(6);

        mContext = context;
        mHandler = new InnerHandler(this);
        setFactory(this);
    }

    @Override
    public View makeView() {
        TextView textView = new TextView(mContext);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setVerticalScrollBarEnabled(true);
        textView.setSingleLine(false);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
        textView.setTextColor(mTextColor);
        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(mDrawableStart, mDrawableTop, mDrawableEnd, mDrawableBottom);
        textView.setCompoundDrawablePadding(mDrawablePadding);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER_VERTICAL;
        textView.setLayoutParams(layoutParams);

        return textView;
    }

    /**
     * 設定動畫間隔時間
     *
     * @param stillTime 間隔時間
     */
    public void setTextStillTime(final long stillTime) {
        mStillTime = stillTime;
    }

    public void setAnimTime(long animDuration) {
        Animation in = new TranslateAnimation(0, 0, animDuration, 0);
        in.setDuration(animDuration);
        in.setInterpolator(new AccelerateInterpolator());
        Animation out = new TranslateAnimation(0, 0, 0, -animDuration);
        out.setDuration(animDuration);
        out.setInterpolator(new AccelerateInterpolator());
        setInAnimation(in);
        setOutAnimation(out);
    }

    public void setDisplayData(DisplayData displayData) {
        mDisplayData = displayData;
        mIndex = -1;
    }

    public void setCompoundDrawablesRelativeWithIntrinsicBounds(Drawable start, Drawable top, Drawable end, Drawable bottom) {
        ((TextView) this.getNextView()).setCompoundDrawablesRelativeWithIntrinsicBounds(start, top, end, bottom);
    }


    public void startAutoScroll() {
        mHandler.removeMessages(FLAG_STOP_AUTO_SCROLL);
        mHandler.removeMessages(FLAG_START_AUTO_SCROLL);
        mHandler.sendEmptyMessage(FLAG_START_AUTO_SCROLL);
    }

    public void stopAutoScroll() {
        mHandler.sendEmptyMessage(FLAG_STOP_AUTO_SCROLL);
    }

    private void handleMessage(Message msg) {
        switch (msg.what) {
            case FLAG_START_AUTO_SCROLL: {
                if (mDisplayData != null && mDisplayData.mTextList.size() > 0) {
                    mIndex++;
                    mIndex = mIndex % mDisplayData.mTextList.size();
                    String nextString = mDisplayData.mTextList.get(mIndex);
                    if (!nextString.equals(mCurrentString)) {
                        if(mDisplayData.mDrawableList.size() > mIndex) {
                            setCompoundDrawablesRelativeWithIntrinsicBounds(mDisplayData.mDrawableList.get(mIndex),null,null,null);
                        }
                        setText(nextString);
                    }
                    mCurrentString = nextString;
                    mHandler.sendEmptyMessageDelayed(FLAG_START_AUTO_SCROLL, mStillTime);
                }
                break;
            }

            case FLAG_STOP_AUTO_SCROLL: {
                mHandler.removeMessages(FLAG_START_AUTO_SCROLL);
                break;
            }
        }
    }
}
