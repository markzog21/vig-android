package com.luxgen.remote.custom;

public class CommonDefs {

    // Inner Message
    public static final int INNER_MSG_WHAT_RESHOW_PROGRESS_DIALOG = 0;
    public static final int INNER_MSG_WHAT_RESHOW_ALERT_DIALOG_1 = 1;
    public static final int INNER_MSG_WHAT_RESHOW_ALERT_DIALOG_2 = 2;
    public static final int INNER_MSG_WHAT_REFRESH_IN_BACKGROUND = 3;

    // Wheel Handle Position
    public static final int WHEEL_HANDLE_POSITION_RIGHT = 0;
    public static final int WHEEL_HANDLE_POSITION_LEFT = 1;

    // Door Status
    public static final int DOOR_STATUS_NONE = -1;
    public static final int DOOR_STATUS_LOCK = 0;
    public static final int DOOR_STATUS_UNLOCK = 1;
    public static final int DOOR_STATUS_LOCK_AND_TRUNK_OPEN = 2;
    public static final int DOOR_STATUS_UNLOCK_AND_TRUNK_OPEN = 3;

    // Request Code
    public static final int REQUEST_CODE_REQUEST_PERMISSIONS = 0;

    // Others
    public static final int DIGITAL_KEY_SUBKEY_OWNER_MAX_COUNT = 4;
    public static final int DIGITAL_KEY_TEMPKEY_OWNER_MAX_COUNT = 2;
    public static final int MAIN_ACTIVITY_NEWS_MAX_COUNT = 20;
    public static final long MAIN_REFRESH_IN_BACKGROUND_DELAY_TIME = 30000;
    public static final int DIGITAL_KEY_DISPLAYNAME_MAX_LENGTH = 12;

}
