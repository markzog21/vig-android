package com.luxgen.remote.custom;

public class DataKeys {

    // Digital Key Name
    public static final String KEY_SUBKEY_PHONE = "phone";
    public static final String KEY_SUBKEY_OWNER_NAME = "name";
    public static final String KEY_TEMPKEY_PHONE = "phone";
    public static final String KEY_TEMPKEY_OWNER_NAME = "name";
    public static final String KEY_TEMPKEY_START_TIME = "start";
    public static final String KEY_TEMPKEY_END_TIME = "end";
    public static final String KEY_TEMPKEY_ENABLE = "enable";

    // Edit Digital Key Name
    public static final String KEY_LABEL = "label";

}
