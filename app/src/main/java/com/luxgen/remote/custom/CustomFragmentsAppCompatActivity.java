package com.luxgen.remote.custom;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.luxgen.remote.R;

public abstract class CustomFragmentsAppCompatActivity extends CustomAppCompatActivity {

    protected abstract int getLayoutId();

    protected abstract void setupTitle(String tag);

    private void setupToolBar() {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        if (mToolbar != null) {
            mToolbar.setTitle("");
            setSupportActionBar(mToolbar);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void setupTitleChangeListener() {
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                String tag = "";
                if (fragment != null) {
                    fragment.setUserVisibleHint(true);
                    tag = fragment.getTag();
                }
                setupTitle(tag);
            }
        });
    }

    private void setupWheelMenu() {
        setWheelHandles(findViewById(R.id.wheel_right_handle_image_view), findViewById(R.id.wheel_left_handle_image_view));
        setWheelScreenLayout(findViewById(R.id.activity_container));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());

        setupToolBar();
        setupTitleChangeListener();
        setupWheelMenu();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment frag : getSupportFragmentManager().getFragments()) {
            if (frag != null) {
                frag.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack();
                } else {
                    finish();
                }
        }
        return super.onOptionsItemSelected(item);
    }

    public void navigate(@Nullable Fragment from, @NonNull Fragment to, @IdRes int containerViewId, @Nullable String tag, boolean addToBackStack) {
        if (from != null) {
            from.setUserVisibleHint(false);
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(containerViewId, to, tag);
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();

        setupTitle(tag);
    }

    public void navigate(Fragment from, Fragment to, String tag, boolean addToBackStack) {
        navigate(from, to, R.id.fragment_container, tag, addToBackStack);
    }

    protected void show(Fragment fragment, String tag) {
        navigate(null, fragment, tag, false);
    }

    protected void show(Fragment fragment) {
        show(fragment, null);
    }

    protected void popFragment() {
        if (getSupportFragmentManager() != null) {
            getSupportFragmentManager().popBackStack();
        }
    }
}
