package com.luxgen.remote.custom;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public abstract class CustomRecyclerViewBaseAdapter<ViewHolder extends RecyclerView.ViewHolder> extends
        RecyclerView.Adapter<ViewHolder> {

    public class RecyclerViewItemData {
        private int mItemType;
        private Object mDataObject = null;
        private Object mExtraDataObject = null;
        private boolean mIsEnabled = false;

        public int getItemType() {
            return mItemType;
        }

        void setItemType(int itemType) {
            this.mItemType = itemType;
        }

        public Object getDataObject() {
            return mDataObject;
        }

        void setDataObject(Object dataObject) {
            this.mDataObject = dataObject;
        }

        public Object getExtraDataObject() {
            return mExtraDataObject;
        }

        void setExtraDataObject(Object extraDataObject) {
            this.mExtraDataObject = extraDataObject;
        }

        public boolean isEnabled() {
            return mIsEnabled;
        }

        void setEnable(boolean isEnabled) {
            this.mIsEnabled = isEnabled;
        }

    }

    protected Context mContext = null;
    protected ArrayList<RecyclerViewItemData> mDisplayArrayList = new ArrayList<RecyclerViewItemData>();
    protected ArrayList<RecyclerViewItemData> mTempArrayList = new ArrayList<RecyclerViewItemData>();

    public CustomRecyclerViewBaseAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getItemCount() {
        return mDisplayArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return mDisplayArrayList.get(position).getItemType();
    }

    public RecyclerViewItemData getItem(int position) {
        if ((mDisplayArrayList.size() > 0) && (position < mDisplayArrayList.size())) {
            return mDisplayArrayList.get(position);
        } else {
            return null;
        }
    }

    public void clear() {
        mDisplayArrayList.clear();
    }

    public void removeItemData(int position) {
        mDisplayArrayList.remove(position);
        notifyItemRemoved(position);
    }

    public boolean isEnabled(int position) {
        return mDisplayArrayList.get(position).isEnabled();
    }

    public void buildItemData(Object dataObject, Object extraDataObject, int itemType, boolean isEnabled) {
        RecyclerViewItemData itemData = new RecyclerViewItemData();
        itemData.setItemType(itemType);
        itemData.setDataObject(dataObject);
        itemData.setExtraDataObject(extraDataObject);
        itemData.setEnable(isEnabled);

        mTempArrayList.add(itemData);
    }

    public void commitBuildItemData() {
        ArrayList<RecyclerViewItemData> temp = mDisplayArrayList;
        mDisplayArrayList = mTempArrayList;
        mTempArrayList = temp;
        mTempArrayList.clear();
    }

    public void addItemData(Object dataObject, Object extraDataObject, int itemType, boolean isEnabled) {
        RecyclerViewItemData itemData = new RecyclerViewItemData();
        itemData.setItemType(itemType);
        itemData.setDataObject(dataObject);
        itemData.setExtraDataObject(extraDataObject);
        itemData.setEnable(isEnabled);

        mDisplayArrayList.add(itemData);
    }

}
