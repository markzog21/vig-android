package com.luxgen.remote.custom;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.luxgen.remote.R;
import com.luxgen.remote.activity.MainActivity;
import com.luxgen.remote.ui.dialog.AlertDialogFragment;
import com.luxgen.remote.util.NetUtils;

import static com.luxgen.remote.activity.MainActivity.INTENT_EXTRA_NAME_FORCE_EXIT_APP;

public abstract class CustomBaseFragment extends Fragment {

    public final static String FRAGMENT_TAG_SERVER_ERROR_DIALOG = "showServerErrorDialog";
    public final static String FRAGMENT_TAG_GENERAL_ERROR_DIALOG = "showGeneralErrorDialog";
    public final static String FRAGMENT_TAG_CONFIRM_DIALOG = "showConfirmDialog";
    public final static String FRAGMENT_TAG_MULTI_LOGIN_ERROR_DIALOG = "showMultiLoginErrorDialog";

    private IntentFilter mIntentFilter = new IntentFilter();

    private BroadcastReceiver mCaStatusBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            onReceiveBroadcastFromCaService(context, intent);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addActionToReceiveBroadcastFromCaService(mIntentFilter);
    }

    @Override
    public void onResume() {
        super.onResume();
        registerCaStatusBroadcastReceiver();
    }

    @Override
    public void onPause() {
        unregisterCaStatusBroadcastReceiver();
        super.onPause();
    }

    private void registerCaStatusBroadcastReceiver() {
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mCaStatusBroadcastReceiver, mIntentFilter);
    }

    private void unregisterCaStatusBroadcastReceiver() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mCaStatusBroadcastReceiver);
    }

    protected void addActionToReceiveBroadcastFromCaService(IntentFilter intentFilter) {

    }

    protected void onReceiveBroadcastFromCaService(Context context, Intent intent) {

    }

    protected void dismissConfirmDialog() {
        if (isAdded()) {
            Fragment dialog = getFragmentManager().findFragmentByTag(FRAGMENT_TAG_CONFIRM_DIALOG);
            if (dialog != null) {
                getFragmentManager().beginTransaction().remove(dialog).commit();
            }
        }
    }

    protected void dismissServerErrorDialog() {
        if (isAdded()) {
            Fragment dialog = getFragmentManager().findFragmentByTag(FRAGMENT_TAG_SERVER_ERROR_DIALOG);
            if (dialog != null) {
                getFragmentManager().beginTransaction().remove(dialog).commit();
            }
        }
    }

    protected void showServerErrorDialog() {
        showServerErrorDialog(null, null, null);
    }

    protected void showServerErrorDialog(String message) {
        showServerErrorDialog(null, message, null);
    }

    protected void showServerErrorDialog(String title, String message) {
        showServerErrorDialog(title, message, null);
    }

    protected void showServerErrorDialog(String message, AlertDialogFragment.Callbacks callbacks) {
        showServerErrorDialog(null, message, callbacks);
    }

    protected void showServerErrorDialog(String title, String message, AlertDialogFragment.Callbacks callbacks) {
        showServerErrorDialog(title, message, false, callbacks);
    }

    protected void showServerErrorDialog(String title, String message, boolean showPositiveButton, AlertDialogFragment.Callbacks callbacks) {
        if (isAdded()) {
            if ((getContext() != null && !NetUtils.isConnectToInternet(getContext()))
                || (!message.isEmpty() && message.contains("UnknownHostException"))
            ) {
                message = getString(R.string.dialog_message_no_internet);
            }
            if (title == null) {
                title = getString(R.string.dialog_title_error);
            }
            AlertDialogFragment dialog = AlertDialogFragment.newInstance(title, message);
            dialog.setButtons(getString(R.string.btn_confirm), showPositiveButton ? getString(R.string.btn_cancel) : null, callbacks);
            dialog.show(getFragmentManager(), FRAGMENT_TAG_SERVER_ERROR_DIALOG);
        }
    }

    protected void showConfirmDialog(String title, String message) {
        showConfirmDialog(title, message, null);
    }

    protected void showConfirmDialog(String title, String message, AlertDialogFragment.Callbacks callbacks) {
        showConfirmDialog(title, message, false, callbacks);
    }

    protected void showConfirmDialog(String title, String message, boolean showPositiveButton, AlertDialogFragment.Callbacks callbacks) {
        if (isAdded()) {
            AlertDialogFragment dialog = AlertDialogFragment.newInstance(title, message);
            dialog.setButtons(getString(R.string.btn_confirm), showPositiveButton ? getString(R.string.btn_cancel) : null, callbacks);
            dialog.show(getFragmentManager(), FRAGMENT_TAG_CONFIRM_DIALOG);
        }
    }

    protected void showNoLongerRemindDialog(String message, String prefKey, String positiveButtonText, String negativeButtonText, AlertDialogFragment.Callbacks callbacks) {
        if (isAdded()) {
            AlertDialogFragment dialog = AlertDialogFragment.newInstance(getString(R.string.dialog_title_remind),
                    message, getString(R.string.dialog_checkbox_no_longer_remind), prefKey);
            dialog.setButtons(positiveButtonText, negativeButtonText, callbacks);
            dialog.show(getFragmentManager(), FRAGMENT_TAG_CONFIRM_DIALOG);
        }
    }

    protected void showMultiLoginErrorDialog() {
        if (isAdded()) {
            AlertDialogFragment dialog = AlertDialogFragment.newInstance(getString(R.string.dialog_title_remind),
                    getString(R.string.dialog_message_multi_login));
            dialog.setButtons(getString(R.string.btn_confirm), null, new AlertDialogFragment.Callbacks() {
                @Override
                public void onPositiveButtonClicked() {
                    finishApplication();
                }

                @Override
                public void onNegativeButtonClicked() {

                }

                @Override
                public void onBackKeyPressed() {
                    finishApplication();
                }
            });
            dialog.show(getFragmentManager(), FRAGMENT_TAG_MULTI_LOGIN_ERROR_DIALOG);
        }
    }

    public void navigate(Fragment from, Fragment to, @IdRes int containerViewId, String tag, boolean addToBackStack) {
        if (from != null) {
            from.setUserVisibleHint(false);
        }

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(containerViewId, to, tag);
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public void navigate(Fragment from, Fragment to, String tag, boolean addToBackStack) {
        navigate(from, to, R.id.fragment_container, tag, addToBackStack);
    }

    protected void show(Fragment fragment, String tag, boolean addToBackStack) {
        navigate(null, fragment, tag, addToBackStack);
    }

    protected void show(Fragment fragment) {
        navigate(null, fragment, null, true);
    }

    protected void popFragment() {
        if (getFragmentManager() != null) {
            getFragmentManager().popBackStack();
        }
    }

    protected void hideSoftKeyboard(View view) {
        if (getContext() != null && view != null) {
            InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            if (getActivity() != null) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
            view.clearFocus();
        }
    }

    private void finishApplication() {
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(INTENT_EXTRA_NAME_FORCE_EXIT_APP, true);
        startActivity(intent);
    }

    protected long getCurrentTime() {
        return System.currentTimeMillis();
    }
}
