package com.luxgen.remote.custom;

import com.luxgen.remote.model.PushData;

import java.util.Comparator;

public class PushComparator implements Comparator<PushData> {
    @Override
    public int compare(PushData o1, PushData o2) {
        return o2.getDate().compareTo(o1.getDate());
    }
}
