package com.luxgen.remote.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.luxgen.remote.R;
import com.luxgen.remote.ca.CommunicationAgentService;
import com.luxgen.remote.custom.CustomAppCompatActivity;
import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.UserWebApi;
import com.luxgen.remote.network.model.user.GetIkeyUidRequestData;
import com.luxgen.remote.network.model.user.GetIkeyUidResponseData;
import com.luxgen.remote.network.model.user.LoginRequestData;
import com.luxgen.remote.network.model.user.LoginResponseData;
import com.luxgen.remote.ui.login.LoginActivity;
import com.luxgen.remote.ui.viewmodel.DigitalKeyViewModel;
import com.luxgen.remote.ui.viewmodel.SplashViewModel;
import com.luxgen.remote.ui.viewmodel.ViewModelFactory;
import com.luxgen.remote.util.NetUtils;
import com.luxgen.remote.util.Prefs;

import static com.luxgen.remote.ca.CommunicationAgentService.CA_EXTRA_DEVICE_ID_NOT_MATCH_RETURN_CODE;
import static com.luxgen.remote.ca.CommunicationAgentService.CA_EXTRA_DEVICE_TIMESTAMP_NOT_MATCH_RETURN_CODE;
import static com.luxgen.remote.ca.CommunicationAgentService.CA_EXTRA_LOGIN_SUCCESS_RETURN_CODE;
import static com.luxgen.remote.ca.CommunicationAgentService.CA_EXTRA_MAX_LOGIN_RETRY_RETURN_CODE;

public class SplashActivity extends CustomAppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 1000;
    private UserInfo mUserInfo;
    private FirebaseAnalytics mFirebaseAnalytics;
    private SplashViewModel mViewModel = null;

    private OnApiListener<GetIkeyUidResponseData> mGetIkeyDataRequestListener = new OnApiListener<GetIkeyUidResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetIkeyUidResponseData responseData) {
            doAutoLogin();
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            doAutoLogin();
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<LoginResponseData> mLoginRequestListener = new OnApiListener<LoginResponseData>() {
        @Override
        public void onApiTaskSuccessful(LoginResponseData responseData) {
            if (responseData.isSuccessful() && !responseData.isRedirect()) {
                String mIkeyUid = responseData.getKeyUid();
                String mIkeyToken = responseData.getKeyToken();
                String mUID = responseData.getUid();
                String mPhone = responseData.getPhone();
                updateUserData(mUID, mIkeyToken, mIkeyUid, mPhone);
                doIkeyLogin();
            } else {
                showLoginScreen();
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            // If there is any failure, still show Main screen.
            // showLoginScreen();
            showMainScreen();
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {

        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mViewModel = ViewModelProviders.of(this).get(SplashViewModel.class);

        getLoginData();
        resetActionId();
        if (hasLoginData()) {
            if (NetUtils.isConnectToInternet(this)) {
                doFuckingMultiLoginCheckBeforeLogin();
            } else {
                showMainScreen();
            }
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showLoginScreen();
                }
            }, SPLASH_DISPLAY_LENGTH);
        }
    }

    @Override
    protected void addActionToReceiveBroadcastFromCaService(IntentFilter intentFilter) {
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_IKEY_LOGIN);
    }

    @Override
    protected void onReceiveBroadcastFromCaService(Context context, Intent intent) {
        if (CommunicationAgentService.CA_STATUS_IKEY_LOGIN.equals(intent.getAction())) {
            if (intent.hasExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE)) {
                int returnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
                LogCat.d("ikey login ack:" + returnCode);
                if (returnCode == CA_EXTRA_LOGIN_SUCCESS_RETURN_CODE) {
                    showMainScreen();
                } else {
                    if (returnCode == CA_EXTRA_MAX_LOGIN_RETRY_RETURN_CODE) {
                        LogCat.d("ikey login error: 失敗重試已達上限");
                        showMainScreen();
                    } else if (returnCode == CA_EXTRA_DEVICE_TIMESTAMP_NOT_MATCH_RETURN_CODE ||
                            returnCode == CA_EXTRA_DEVICE_ID_NOT_MATCH_RETURN_CODE) {
                        Prefs.clear(SplashActivity.this, Prefs.PREFS_DEVICE_ID);
                        getLoginData();
                        resetActionId();
                        doAutoLogin();
                    } else {
                        LogCat.d("ikey login error: " + returnCode);
                        showConfirmDialog(getString(R.string.dialog_title_error), getString(R.string.fake_999), mFinishActivityCallbacks);
                    }
                }
            } else if (intent.hasExtra(CommunicationAgentService.CA_STATUS_EXTRA_COMMAND)) {
                String message = intent.getStringExtra(CommunicationAgentService.CA_STATUS_EXTRA_COMMAND);
                LogCat.d("ikey login failed:" + message);
                showConfirmDialog(getString(R.string.dialog_title_error), getString(R.string.fake_999), mFinishActivityCallbacks);
            }

            return;
        }

        super.onReceiveBroadcastFromCaService(context, intent);
    }

    private void getLoginData() {
        mUserInfo = Prefs.getUserInfo(this);
    }

    private void resetActionId() {
        Prefs.resetActionId(this);
    }

    private void updateUserData(String UID, String iKeyToken, String iKeyUID, String phone) {
        mUserInfo.setUid(UID);
        mUserInfo.setToken(iKeyToken);
        mUserInfo.setKeyUid(iKeyUID);
        mUserInfo.setPhone(phone);

        Prefs.setUserInfo(this, mUserInfo);
    }

    private void doFuckingMultiLoginCheckBeforeLogin() {
        GetIkeyUidRequestData data = new GetIkeyUidRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken());

        UserWebApi userWebApi = UserWebApi.getInstance(this);
        userWebApi.getIkeyUid(data, mGetIkeyDataRequestListener);
    }

    private boolean hasLoginData() {
        return mUserInfo != null && mUserInfo.hasLoginData();
    }

    private LoginRequestData buildLoginRequest(String email, String password, String deviceId) {
        LoginRequestData data = new LoginRequestData(email);
        data.setPassword(password);
        data.setPhoneId(deviceId);
        data.setOsVersion(Build.VERSION.RELEASE);
        data.setPhoneBrand(Build.BRAND);

        return data;
    }

    private void doAutoLogin() {
        LoginRequestData data = buildLoginRequest(mUserInfo.getAccount(), mUserInfo.getPassword(), mUserInfo.getDeviceId(this));
        data.setDeviceCreateTime(mUserInfo.getDeviceCreateTime(this));

        UserWebApi userWebApi = UserWebApi.getInstance(this);
        userWebApi.login(data, mLoginRequestListener);
    }

    private void showLoginScreen() {
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void doIkeyLogin() {
        Intent ikeyLoginIntent = new Intent(SplashActivity.this, CommunicationAgentService.class);
        if (mUserInfo != null) {
            ikeyLoginIntent.putExtra(CommunicationAgentService.CA_EXTRA_IKEY_LOGIN, mUserInfo.getKeyUid());
        }
        startService(ikeyLoginIntent);
    }

    private void showMainScreen() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private final View.OnClickListener mFinishActivityCallbacks = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    protected void showMultiLoginErrorDialog() {
        showConfirmDialog(getString(R.string.dialog_title_remind), getString(R.string.dialog_message_multi_login), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.clear();
                Prefs.clear(SplashActivity.this, Prefs.PREFS_ALL);
                showLoginScreen();
            }
        });
    }
}
