package com.luxgen.remote.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomAppCompatActivity;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.CarWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.car.AddNewCarRequestData;
import com.luxgen.remote.network.model.car.DmsUpgradeRequestData;
import com.luxgen.remote.network.model.car.DmsUpgradeResponseData;
import com.luxgen.remote.network.model.car.UpgradeCheckMessageRequestData;
import com.luxgen.remote.network.model.car.UpgradeCheckMessageResponseData;
import com.luxgen.remote.util.Prefs;

public class AddNewCarActivity extends CustomAppCompatActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, AddNewCarActivity.class);
    }

    private final int STEP_1 = 0;
    private final int STEP_2 = 1;
    private final int STEP_3 = 2;

    private GetUserInfoAsyncTask mGetUserInfoAsyncTask = null;

    private UserInfo mUserInfo = null;

    // Step 1 Objects
    private ViewGroup mStep1Layout = null;
    private EditText mIDNumberEditText = null;
    private EditText mPlateNumberEditText = null;
    private Button mStep1NextButton = null;
    private TextView mStep1SkipTextView = null;

    // Step 2 Objects
    private ViewGroup mStep2Layout = null;
    private EditText mPhoneNumberEditText = null;
    private Button mStep2NextButton = null;

    // Step 3 Objects
    private ViewGroup mStep3Layout = null;
    private EditText mVerificationCodeEditText = null;
    private Button mStep3NextButton = null;
    private TextView mStep3ResendCodeTextView = null;

    private String mRegisterNewCarTitleString = null;
    private String mPhoneVerificationTitleString = null;

    private int mCurrentStrp = STEP_1;

    private class GetUserInfoAsyncTask extends AsyncTask<Void, Void, UserInfo> {

        @Override
        protected UserInfo doInBackground(Void... voids) {
            return Prefs.getUserInfo(AddNewCarActivity.this);
        }

        @Override
        protected void onPostExecute(UserInfo userInfo) {
            mGetUserInfoAsyncTask = null;

            mUserInfo = userInfo;
            dismissProgressDialog();
        }
    }

    private View.OnClickListener mStep1NextButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String idNumber = mIDNumberEditText.getText().toString().trim().toUpperCase();
            String plateNumber = mPlateNumberEditText.getText().toString().trim().toUpperCase();
            if (TextUtils.isEmpty(idNumber)) {
                mIDNumberEditText.setError(getString(R.string.add_new_car_input_error));
            } else if (TextUtils.isEmpty(plateNumber)) {
                mPlateNumberEditText.setError(getString(R.string.add_new_car_input_error));
            } else {
                callAPIToAddNewCar();
            }
        }
    };

    private View.OnClickListener mStep1SkipButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mCurrentStrp = STEP_2;
            switchStepDisplay();
        }
    };

    private View.OnClickListener mStep2NextButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String phoneNumber = mPhoneNumberEditText.getText().toString().trim();
            if (TextUtils.isEmpty(phoneNumber)) {
                mPhoneNumberEditText.setError(getString(R.string.add_new_car_input_error));
            } else {
                callAPIToCheckPhoneNumber();
            }
        }
    };

    private View.OnClickListener mStep3NextButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            callAPIToCheckMessage();
        }
    };

    private View.OnClickListener mStep3ResendCodeTextViewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            callAPIToCheckPhoneNumber();
        }
    };

    private OnApiListener<ResponseData> mOnAddNewCarApiListener = new OnApiListener<ResponseData>() {

        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            if (responseData.isSuccessful()) {
                mCurrentStrp = STEP_2;
                switchStepDisplay();
            } else {
                if (responseData.getErrorCode().contains("911")) {
                    showServerErrorDialog(getString(R.string.add_new_car_dialog_data_error_title),
                            responseData.getErrorMessage());
                } else {
                    showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                            responseData.getErrorMessage());
                }
            }
            dismissProgressDialog();
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(getString(R.string.dialog_title_error), failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<DmsUpgradeResponseData> mOnCheckPhoneNumberApiListener = new OnApiListener<DmsUpgradeResponseData>() {

        @Override
        public void onApiTaskSuccessful(DmsUpgradeResponseData responseData) {
            if (responseData.isSuccessful()) {
                mCurrentStrp = STEP_3;
                switchStepDisplay();
            } else {
                if (responseData.getErrorCode().contains("992")) {
                    showServerErrorDialog(getString(R.string.add_new_car_dialog_phone_number_error_title),
                            responseData.getErrorMessage());
                } else {
                    showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                            responseData.getErrorMessage());
                }
            }
            dismissProgressDialog();
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(getString(R.string.dialog_title_error), failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<UpgradeCheckMessageResponseData> mOnCheckMessageApiListener = new OnApiListener<UpgradeCheckMessageResponseData>() {

        @Override
        public void onApiTaskSuccessful(UpgradeCheckMessageResponseData responseData) {
            if (responseData.isSuccessful()) {
                String UIDString = responseData.getUid();
                if (UIDString != null && !TextUtils.isEmpty(UIDString)) {
                    mUserInfo.setUid(UIDString);
                    Prefs.setUserInfo(AddNewCarActivity.this, mUserInfo);
                    Prefs.setRefreshCarKeyListFlag(AddNewCarActivity.this, Prefs.FLAG_REFRESH_CAR_KEY_LIST_AND_OPEN_IT);
                }
                finish();
            } else {
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }
            dismissProgressDialog();
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(getString(R.string.dialog_title_error), failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_car);

        LinearLayout mainContainer = findViewById(R.id.activity_container);
        setWheelScreenLayout(mainContainer);

        ImageView wheelRightHandle = findViewById(R.id.wheel_right_handle_image_view);
        ImageView wheelLeftHandle = findViewById(R.id.wheel_left_handle_image_view);
        setWheelHandles(wheelRightHandle, wheelLeftHandle);

        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            toolBar.setTitle(R.string.activity_add_new_car_title_register);
            setSupportActionBar(toolBar);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Step 1
        mStep1Layout = findViewById(R.id.activity_add_new_car_step_1_layout);
        mIDNumberEditText = findViewById(R.id.activity_add_new_car_id_number_edit_text);
        mPlateNumberEditText = findViewById(R.id.activity_add_new_car_plate_number_edit_text);
        mStep1NextButton = findViewById(R.id.activity_add_new_car_step_1_next_buttton);
        mStep1NextButton.setOnClickListener(mStep1NextButtonClickListener);
        mStep1SkipTextView = findViewById(R.id.activity_add_new_car_step_1_skip_text_view);
        mStep1SkipTextView.setOnClickListener(mStep1SkipButtonClickListener);

        // Step 2
        mStep2Layout = findViewById(R.id.activity_add_new_car_step_2_layout);
        mPhoneNumberEditText = findViewById(R.id.activity_add_new_car_phone_number_edit_text);
        mStep2NextButton = findViewById(R.id.activity_add_new_car_step_2_next_buttton);
        mStep2NextButton.setOnClickListener(mStep2NextButtonClickListener);

        // Step 3
        mStep3Layout = findViewById(R.id.activity_add_new_car_step_3_layout);
        mVerificationCodeEditText = findViewById(R.id.activity_add_new_car_verification_code_edit_text);
        mStep3NextButton = findViewById(R.id.activity_add_new_car_step_3_next_buttton);
        mStep3NextButton.setOnClickListener(mStep3NextButtonClickListener);
        mStep3ResendCodeTextView = findViewById(R.id.activity_add_new_car_step_3_resend_code_text_view);
        mStep3ResendCodeTextView.setOnClickListener(mStep3ResendCodeTextViewClickListener);

        mRegisterNewCarTitleString = getString(R.string.activity_add_new_car_title_register);
        mPhoneVerificationTitleString = getString(R.string.activity_add_new_car_title_phone_verification);

        bindCaService();
    }

    @Override
    protected void onCaServiceConnected() {
        init();
        super.onCaServiceConnected();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                switch (mCurrentStrp) {
                    case STEP_1: {
                        finish();
                    }
                    break;

                    case STEP_2: {
                        mCurrentStrp = STEP_1;
                        switchStepDisplay();
                    }
                    break;

                    case STEP_3: {
                        mCurrentStrp = STEP_2;
                        switchStepDisplay();
                    }
                    break;

                    default:
                        finish();
                }

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        switch (mCurrentStrp) {
            case STEP_1: {
                super.onBackPressed();
            }
            break;

            case STEP_2: {
                mCurrentStrp = STEP_1;
                switchStepDisplay();
            }
            break;

            case STEP_3: {
                mCurrentStrp = STEP_2;
                switchStepDisplay();
            }
            break;

            default:
                super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        if (mGetUserInfoAsyncTask != null) {
            mGetUserInfoAsyncTask.cancel(true);
        }

        unbindCaService();
        super.onDestroy();
    }

    private void init() {
        showProgressDialog();
        if (mGetUserInfoAsyncTask == null) {
            mGetUserInfoAsyncTask = new GetUserInfoAsyncTask();
            mGetUserInfoAsyncTask.execute();
        }
    }

    private void callAPIToAddNewCar() {
        showProgressDialog();
        CarWebApi carWebApi = CarWebApi.getInstance(this);

        AddNewCarRequestData addNewCarRequestData = new AddNewCarRequestData(mUserInfo.getKeyUid(),
                mUserInfo.getToken(),
                mPlateNumberEditText.getText().toString().trim().toUpperCase(),
                mIDNumberEditText.getText().toString().trim().toUpperCase());
        carWebApi.addNewCar(addNewCarRequestData, mOnAddNewCarApiListener);
    }

    private void callAPIToCheckPhoneNumber() {
        showProgressDialog();
        CarWebApi carWebApi = CarWebApi.getInstance(this);

        DmsUpgradeRequestData dmsUpgradeRequestData = new DmsUpgradeRequestData(mUserInfo.getAccount(),
                mIDNumberEditText.getText().toString().trim().toUpperCase(),
                mPlateNumberEditText.getText().toString().trim().toUpperCase(),
                mPhoneNumberEditText.getText().toString().trim());
        carWebApi.upgradePhoneNo(dmsUpgradeRequestData, mOnCheckPhoneNumberApiListener);
    }

    private void callAPIToCheckMessage() {
        showProgressDialog();
        CarWebApi carWebApi = CarWebApi.getInstance(this);

        UpgradeCheckMessageRequestData upgradeCheckMessageRequestData = new UpgradeCheckMessageRequestData(mUserInfo.getAccount(),
                mIDNumberEditText.getText().toString().trim().toUpperCase(),
                mVerificationCodeEditText.getText().toString().trim());
        carWebApi.upgradeCheckPhone(upgradeCheckMessageRequestData, mOnCheckMessageApiListener);
    }

    private void switchStepDisplay() {
        switch (mCurrentStrp) {
            case STEP_1: {
                mStep1Layout.setVisibility(View.VISIBLE);
                mStep2Layout.setVisibility(View.INVISIBLE);
                mStep3Layout.setVisibility(View.INVISIBLE);
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setTitle(mRegisterNewCarTitleString);
                }
            }
            break;

            case STEP_2: {
                mStep1Layout.setVisibility(View.INVISIBLE);
                mStep2Layout.setVisibility(View.VISIBLE);
                mStep3Layout.setVisibility(View.INVISIBLE);
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setTitle(mPhoneVerificationTitleString);
                }
            }
            break;

            case STEP_3: {
                mStep1Layout.setVisibility(View.INVISIBLE);
                mStep2Layout.setVisibility(View.INVISIBLE);
                mStep3Layout.setVisibility(View.VISIBLE);
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setTitle(mPhoneVerificationTitleString);
                }
            }
            break;

            default:
        }
    }
}
