package com.luxgen.remote.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomAppCompatActivity;
import com.luxgen.remote.util.DialUtils;


public class ServiceCenterActivity extends CustomAppCompatActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, ServiceCenterActivity.class);
    }

    private TextView mSoundPlusLinkTextView = null;
    private TextView mSmartEatsLinkTextView = null;

    private OnClickListener mOnSoundPlusTextViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = DialUtils.soundPlus(ServiceCenterActivity.this);
            startActivity(intent);
        }
    };

    private OnClickListener mOnSmartEatsTextViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = DialUtils.smartEats(ServiceCenterActivity.this);
            startActivity(intent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_center);

        LinearLayout mainContainer = findViewById(R.id.activity_container);
        setWheelScreenLayout(mainContainer);

        ImageView wheelRightHandle = findViewById(R.id.wheel_right_handle_image_view);
        ImageView wheelLeftHandle = findViewById(R.id.wheel_left_handle_image_view);
        setWheelHandles(wheelRightHandle, wheelLeftHandle);

        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            toolBar.setTitle(R.string.activity_service_center_title);
            setSupportActionBar(toolBar);
            toolBar.setNavigationIcon(R.drawable.ic_back);
        }

        mSoundPlusLinkTextView = findViewById(R.id.activity_service_center_sound_plus_link_text_view);
        mSoundPlusLinkTextView.setOnClickListener(mOnSoundPlusTextViewClickListener);
        mSmartEatsLinkTextView = findViewById(R.id.activity_service_center_smart_eats_link_text_view);
        mSmartEatsLinkTextView.setOnClickListener(mOnSmartEatsTextViewClickListener);

        bindCaService();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        unbindCaService();
        super.onDestroy();
    }
}

