package com.luxgen.remote.activity;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.ca.CommunicationAgentService;
import com.luxgen.remote.custom.CustomAppCompatActivity;
import com.luxgen.remote.model.Beacon;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.option.BeaconOptions;
import com.luxgen.remote.model.option.CarOptions;
import com.luxgen.remote.network.CarWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.car.BeaconData;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.car.SetBeaconLearingStatusRequestData;
import com.luxgen.remote.network.model.car.SetBeaconRequestData;
import com.luxgen.remote.util.BleManager;
import com.luxgen.remote.util.Prefs;

import java.util.ArrayList;

import static com.luxgen.remote.ca.CommunicationAgentService.VIG_SCAN_BEACONS;
import static com.luxgen.remote.util.Prefs.PREFS_BEACONS;

public class KeylessSettingsActivity extends CustomAppCompatActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, KeylessSettingsActivity.class);
    }

    private TextView mDetectionHintTextView = null;
    private TextView mCompleteHintTextView = null;
    private ImageView mCompleteImageView = null;
    private Button mRestoreDefaultButton = null;

    // Left Door
    private ImageView mLeftDoorDetectionImageView = null;
    private ImageView mLeftDoorDetectionDoneImageView = null;
    private ImageView mLeftDoorAllDoneImageView = null;

    // Right Door
    private ImageView mRightDoorDetectionImageView = null;
    private ImageView mRightDoorDetectionDoneImageView = null;
    private ImageView mRightDoorAllDoneImageView = null;

    // Trunk
    private ImageView mTrunkDetectionImageView = null;
    private ImageView mTrunkDetectionDoneImageView = null;
    private ImageView mTrunkAllDoneImageView = null;

    private Boolean mIsLeftDoorSetDone = false;
    private Boolean mIsRightDoorSetDone = false;
    private Boolean mIsTrunkSetDone = false;

    private Boolean mIsScanningLeftDoor = false;
    private Boolean mIsScanningRightDoor = false;
    private Boolean mIsScanningTrunk = false;

    private BleManager mBleManager;
    private long mFirstReceiveTime = 0;
    private final static int LEARNING_TIME = 1000; // 1second
    private final static int SCAN_PERIOD = 3000; // 3seconds
    private Beacon mLastBeacon, mLeftBeacon, mRightBeacon, mTrunkBeacon;
    private String mVigCarId;
    private String mBeaconLearningStatus;

    private View.OnClickListener mOnDialogNegativeButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            mAlertDialog1.dismiss();
        }
    };

    private View.OnClickListener mOnRestoreButtonClickListner = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            mAlertDialog1.restoreDefault();
            mAlertDialog1.setTitle(R.string.keyless_settings_dialog_restore_default_title);
            mAlertDialog1.setMessage(getResources().getString(R.string.keyless_settings_dialog_restore_default_message));
            mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                    getResources().getString(R.string.btn_confirm),
                    mOnRestoreDefaultDialogPositiveButtonClickListener);
            mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                    getResources().getString(R.string.btn_cancel),
                    mOnDialogNegativeButtonClickListener);
            mAlertDialog1.show();
        }
    };

    private View.OnClickListener mOnRestoreDefaultDialogPositiveButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            setBeaconLearningStatus();
            updateBeaconLearningStatus(BeaconOptions.READ);
            sendRssiToVig(false);

            mIsLeftDoorSetDone = false;
            mIsRightDoorSetDone = false;
            mIsTrunkSetDone = false;

            mDetectionHintTextView.setVisibility(View.VISIBLE);
            mCompleteHintTextView.setVisibility(View.INVISIBLE);
            mCompleteImageView.setVisibility(View.INVISIBLE);

            mLeftDoorDetectionImageView.setVisibility(View.VISIBLE);
            mLeftDoorDetectionDoneImageView.setVisibility(View.INVISIBLE);
            mLeftDoorAllDoneImageView.setVisibility(View.INVISIBLE);

            mRightDoorDetectionImageView.setVisibility(View.VISIBLE);
            mRightDoorDetectionDoneImageView.setVisibility(View.INVISIBLE);
            mRightDoorAllDoneImageView.setVisibility(View.INVISIBLE);

            mTrunkDetectionImageView.setVisibility(View.VISIBLE);
            mTrunkDetectionDoneImageView.setVisibility(View.INVISIBLE);
            mTrunkAllDoneImageView.setVisibility(View.INVISIBLE);

            mLeftBeacon = null;
            mRightBeacon = null;
            mTrunkBeacon = null;

            Prefs.clear(KeylessSettingsActivity.this, PREFS_BEACONS);
            mAlertDialog1.dismiss();
        }
    };

    private View.OnClickListener mOnLeftDoorDetectionImageViewClickListner = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            mAlertDialog1.restoreDefault();
            mAlertDialog1.setTitle(R.string.keyless_settings_dialog_left_door_set_title);
            mAlertDialog1.setMessage(getResources().getString(R.string.keyless_settings_dialog_left_door_set_message));
            mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                    getResources().getString(R.string.btn_connect),
                    mOnLeftDoorDetectionDialogPositiveButtonClickListener);
            mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                    getResources().getString(R.string.btn_cancel),
                    mOnDialogNegativeButtonClickListener);
            mAlertDialog1.show();
        }
    };

    private View.OnClickListener mOnLeftDoorDetectionDialogPositiveButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            mIsScanningLeftDoor = true;
            startScan();
            mAlertDialog1.dismiss();
        }
    };

    private View.OnClickListener mOnRightDoorDetectionImageViewClickListner = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            mAlertDialog1.restoreDefault();
            mAlertDialog1.setTitle(R.string.keyless_settings_dialog_right_door_set_title);
            mAlertDialog1.setMessage(getResources().getString(R.string.keyless_settings_dialog_right_door_set_message));
            mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                    getResources().getString(R.string.btn_connect),
                    mOnRightDoorDetectionDialogPositiveButtonClickListener);
            mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                    getResources().getString(R.string.btn_cancel),
                    mOnDialogNegativeButtonClickListener);
            mAlertDialog1.show();
        }
    };

    private View.OnClickListener mOnRightDoorDetectionDialogPositiveButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            mIsScanningRightDoor = true;
            startScan();
            mAlertDialog1.dismiss();
        }
    };

    private View.OnClickListener mOnTrunkDetectionImageViewClickListner = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            mAlertDialog1.restoreDefault();
            mAlertDialog1.setTitle(R.string.keyless_settings_dialog_trunk_set_title);
            mAlertDialog1.setMessage(getResources().getString(R.string.keyless_settings_dialog_trunk_set_message));
            mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                    getResources().getString(R.string.btn_connect),
                    mOnTrunkDetectionDialogPositiveButtonClickListener);
            mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                    getResources().getString(R.string.btn_cancel),
                    mOnDialogNegativeButtonClickListener);
            mAlertDialog1.show();
        }
    };

    private View.OnClickListener mOnTrunkDetectionDialogPositiveButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            mIsScanningTrunk = true;
            startScan();
            mAlertDialog1.dismiss();
        }
    };

    private OnApiListener<ResponseData> mOnSetBeaconsApiListener = new OnApiListener<ResponseData>() {
        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {

        }

        @Override
        public void onApiTaskFailure(String failMessage) {

        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<ResponseData> mOnSetBeaconLearningStatusApiListener = new OnApiListener<ResponseData>() {

        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {

        }

        @Override
        public void onApiTaskFailure(String failMessage) {

        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keyless_settings);

        LinearLayout mainContainer = findViewById(R.id.activity_container);
        setWheelScreenLayout(mainContainer);

        ImageView wheelRightHandle = findViewById(R.id.wheel_right_handle_image_view);
        ImageView wheelLeftHandle = findViewById(R.id.wheel_left_handle_image_view);
        setWheelHandles(wheelRightHandle, wheelLeftHandle);

        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            toolBar.setTitle(R.string.activity_keyless_settings_title);
            setSupportActionBar(toolBar);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mDetectionHintTextView = findViewById(R.id.activity_keyless_settings_detection_hint_text_view);
        mCompleteHintTextView = findViewById(R.id.activity_keyless_settings_complete_hint_text_view);
        mCompleteImageView = findViewById(R.id.activity_keyless_settings_complete_image_view);
        mRestoreDefaultButton = findViewById(R.id.activity_keyless_settings_restore_default_button);
        mRestoreDefaultButton.setOnClickListener(mOnRestoreButtonClickListner);

        // Left Door
        mLeftDoorDetectionImageView = findViewById(R.id.activity_keyless_settings_left_door_detection_image_view);
        mLeftDoorDetectionImageView.setOnClickListener(mOnLeftDoorDetectionImageViewClickListner);
        mLeftDoorDetectionDoneImageView = findViewById(R.id.activity_keyless_settings_left_door_detection_done_image_view);
        mLeftDoorAllDoneImageView = findViewById(R.id.activity_keyless_settings_left_door_all_done_image_view);

        // Right Door
        mRightDoorDetectionImageView = findViewById(R.id.activity_keyless_settings_right_door_detection_image_view);
        mRightDoorDetectionImageView.setOnClickListener(mOnRightDoorDetectionImageViewClickListner);
        mRightDoorDetectionDoneImageView = findViewById(R.id.activity_keyless_settings_right_door_detection_done_image_view);
        mRightDoorAllDoneImageView = findViewById(R.id.activity_keyless_settings_right_door_all_done_image_view);

        // Trunk
        mTrunkDetectionImageView = findViewById(R.id.activity_keyless_settings_trunk_detection_image_view);
        mTrunkDetectionImageView.setOnClickListener(mOnTrunkDetectionImageViewClickListner);
        mTrunkDetectionDoneImageView = findViewById(R.id.activity_keyless_settings_trunk_detection_done_image_view);
        mTrunkAllDoneImageView = findViewById(R.id.activity_keyless_settings_trunk_all_done_image_view);

        bindCaService();
    }

    @Override
    protected void onCaServiceConnected() {
        initBleManager();

        CarKeyData carKeyData = Prefs.getCarKeyData(this);
        mVigCarId = carKeyData.getVigCarId();
        mBeaconLearningStatus = carKeyData.getBeaconLearningStatus();

        updateBeaconInformation(carKeyData);

        super.onCaServiceConnected();
    }

    @Override
    protected void onDestroy() {
        unbindCaService();
        super.onDestroy();
    }

    private void initBleManager() {
        mBleManager = new BleManager(this);
        mBleManager.setFilterManufacturer(89, Prefs.getBeaconUUID(this));
        mBleManager.scanForBeacon(true);
        mBleManager.enableBeaconThreshold(true);
        mBleManager.setScanPeriod(SCAN_PERIOD);
        mBleManager.setScanResultCallback(new BleManager.ScanResultCallback() {
            @Override
            public void onReceiveBeacon(Beacon beacon) {
                long now = System.currentTimeMillis();
                if (mFirstReceiveTime == 0) {
                    mFirstReceiveTime = now;
                }

                if (mLastBeacon == null || mLastBeacon.getRssi() < beacon.getRssi()) {
                    mLastBeacon = beacon;
                }

                if (mFirstReceiveTime + LEARNING_TIME < now) {
                    stopScan();

                    if (isBeaconAlreadyScanned(mLastBeacon)) {
                        String msg = null;
                        if (mIsScanningLeftDoor) {
                            msg = getString(R.string.keyless_settings_dialog_left_door_set_message);
                        } else if (mIsScanningRightDoor) {
                            msg = getString(R.string.keyless_settings_dialog_right_door_set_message);
                        } else if (mIsScanningTrunk) {
                            msg = getString(R.string.keyless_settings_dialog_trunk_set_message);
                        }

                        mAlertDialog1.restoreDefault();
                        mAlertDialog1.setTitle(R.string.keyless_settings_dialog_beacon_duplicated_title);
                        mAlertDialog1.setMessage(msg);
                        mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                                getResources().getString(R.string.btn_confirm),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mLastBeacon = null;
                                        mFirstReceiveTime = 0;
                                        startScan();
                                        mAlertDialog1.dismiss();
                                    }
                                });
                        mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                                getResources().getString(R.string.btn_cancel),
                                mOnDialogNegativeButtonClickListener);
                        mAlertDialog1.show();
                    } else {
                        if (mIsScanningLeftDoor) {
                            setLeftDoorDone(mLastBeacon, mVigCarId);
                        } else if (mIsScanningRightDoor) {
                            setRightDoorDone(mLastBeacon, mVigCarId);
                        } else if (mIsScanningTrunk) {
                            setTrunkDone(mLastBeacon, mVigCarId);
                        }

                        mLastBeacon = null;
                        mFirstReceiveTime = 0;
                        checkAllDetectionDone(true);
                        mAlertDialog1.dismiss();
                    }
                }
            }

            @Override
            public void onReceiveVig(BluetoothDevice vig) {

            }

            @Override
            public void onTimeout() {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.keyless_settings_dialog_connect_failed_title);
                mAlertDialog1.setMessage(getResources().getString(R.string.keyless_settings_dialog_connect_failed_message));
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getResources().getString(R.string.btn_confirm),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mAlertDialog1.dismiss();
                            }
                        });
                mAlertDialog1.show();
            }
        });
    }

    public void startScan() {
        mBleManager.start();
    }

    public void stopScan() {
        mBleManager.stop();
    }

    private Beacon getBeaconFromStr(String uuid, String input) {
        if (TextUtils.isEmpty(input)) {
            return null;
        }

        String[] token = input.split(",");
        int major = Integer.parseInt(token[0]);
        int minor = Integer.parseInt(token[1]);
        return new Beacon(uuid, major, minor);
    }

    private void updateBeaconInformation(CarKeyData carKeyData) {
        Beacon LBeacon = null;
        Beacon BBeacon = null;
        Beacon RBeacon = null;
        String UUID = Prefs.getBeaconUUID(this);

        if (BeaconOptions.LEARNT.equals(mBeaconLearningStatus)) {
            LBeacon = Prefs.getBeacon(this, BeaconOptions.LEFT_FRONT, mVigCarId);
            if (LBeacon == null) {
                LBeacon = getBeaconFromStr(UUID, carKeyData.getBeacon1No());
            }
            BBeacon = Prefs.getBeacon(this, BeaconOptions.TAILGATE, mVigCarId);
            if (BBeacon == null) {
                BBeacon = getBeaconFromStr(UUID, carKeyData.getBeacon2No());
            }
            RBeacon = Prefs.getBeacon(this, BeaconOptions.RIGHT_FRONT, mVigCarId);
            if (RBeacon == null) {
                RBeacon = getBeaconFromStr(UUID, carKeyData.getBeacon3No());
            }
        }

        if (LBeacon != null) {
            setLeftDoorDone(LBeacon, mVigCarId);
        }
        if (RBeacon != null) {
            setRightDoorDone(RBeacon, mVigCarId);
        }
        if (BBeacon != null) {
            setTrunkDone(BBeacon, mVigCarId);
        }

        checkAllDetectionDone(false);
    }

    private void setLeftDoorDone(Beacon beacon, String vigCarId) {
        mLeftDoorDetectionImageView.setVisibility(View.INVISIBLE);
        mLeftDoorDetectionDoneImageView.setVisibility(View.VISIBLE);
        mIsLeftDoorSetDone = true;
        mIsScanningLeftDoor = false;
        beacon.setRssi(0);
        mLeftBeacon = beacon;
        Prefs.setBeacon(KeylessSettingsActivity.this, BeaconOptions.LEFT_FRONT, beacon, vigCarId);
    }

    private void setRightDoorDone(Beacon beacon, String vigCarId) {
        mRightDoorDetectionImageView.setVisibility(View.INVISIBLE);
        mRightDoorDetectionDoneImageView.setVisibility(View.VISIBLE);
        mIsRightDoorSetDone = true;
        mIsScanningRightDoor = false;
        beacon.setRssi(0);
        mRightBeacon = beacon;
        Prefs.setBeacon(KeylessSettingsActivity.this, BeaconOptions.RIGHT_FRONT, beacon, vigCarId);
    }

    private void setTrunkDone(Beacon beacon, String vigCarId) {
        mTrunkDetectionImageView.setVisibility(View.INVISIBLE);
        mTrunkDetectionDoneImageView.setVisibility(View.VISIBLE);
        mIsTrunkSetDone = true;
        mIsScanningTrunk = false;
        beacon.setRssi(0);
        mTrunkBeacon = beacon;
        Prefs.setBeacon(KeylessSettingsActivity.this, BeaconOptions.TAILGATE, beacon, vigCarId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkAllDetectionDone(boolean upload) {
        if (mIsLeftDoorSetDone && mIsRightDoorSetDone && mIsTrunkSetDone) {
            mDetectionHintTextView.setVisibility(View.INVISIBLE);
            mCompleteHintTextView.setVisibility(View.VISIBLE);
            mCompleteImageView.setVisibility(View.VISIBLE);

            mLeftDoorDetectionDoneImageView.setVisibility(View.INVISIBLE);
            mLeftDoorAllDoneImageView.setVisibility(View.VISIBLE);

            mRightDoorDetectionDoneImageView.setVisibility(View.INVISIBLE);
            mRightDoorAllDoneImageView.setVisibility(View.VISIBLE);

            mTrunkDetectionDoneImageView.setVisibility(View.INVISIBLE);
            mTrunkAllDoneImageView.setVisibility(View.VISIBLE);

            if (upload) {
                setBeacons();
                updateBeaconLearningStatus(BeaconOptions.LEARNT);
                sendRssiToVig(true);
            }
        }
    }

    private boolean isBeaconAlreadyScanned(Beacon beacon) {
        if (mIsScanningLeftDoor) {
            return beacon.equals(mRightBeacon) || beacon.equals(mTrunkBeacon);
        } else if (mIsScanningRightDoor) {
            return beacon.equals(mLeftBeacon) || beacon.equals(mTrunkBeacon);
        } else if (mIsScanningTrunk) {
            return beacon.equals(mRightBeacon) || beacon.equals(mLeftBeacon);
        } else {
            return false;
        }
    }

    private void setBeacons() {
        UserInfo userInfo = Prefs.getUserInfo(this);
        ArrayList<BeaconData> beaconData = new ArrayList<>();
        beaconData.add(new BeaconData(mLeftBeacon.getMajor(), mLeftBeacon.getMinor(), BeaconOptions.LEFT_FRONT));
        beaconData.add(new BeaconData(mRightBeacon.getMajor(), mRightBeacon.getMinor(), BeaconOptions.RIGHT_FRONT));
        beaconData.add(new BeaconData(mTrunkBeacon.getMajor(), mTrunkBeacon.getMinor(), BeaconOptions.TAILGATE));

        SetBeaconRequestData data = new SetBeaconRequestData(userInfo.getKeyUid(), userInfo.getToken(), mVigCarId, beaconData);

        CarWebApi carWebApi = CarWebApi.getInstance(this);
        carWebApi.setBeacons(data, mOnSetBeaconsApiListener);
    }

    private void setBeaconLearningStatus() {
        UserInfo userInfo = Prefs.getUserInfo(this);
        CarKeyData carKeyData = Prefs.getCarKeyData(this);

        SetBeaconLearingStatusRequestData data = new SetBeaconLearingStatusRequestData(userInfo.getKeyUid(), userInfo.getToken(), carKeyData.getCarId());

        CarWebApi carWebApi = CarWebApi.getInstance(this);
        carWebApi.setBeaconLearingStatus(data, mOnSetBeaconLearningStatusApiListener);
    }

    private void sendRssiToVig(boolean start) {
        Intent intent = new Intent(this, CommunicationAgentService.class);
        intent.putExtra(VIG_SCAN_BEACONS, start);

        startService(intent);
    }

    private void updateBeaconLearningStatus(String status) {
        CarKeyData carKeyData = Prefs.getCarKeyData(KeylessSettingsActivity.this);
        carKeyData.setBeaconLearningStatus(status);
        mBeaconLearningStatus = status;

        Prefs.setCarKeyData(KeylessSettingsActivity.this, carKeyData);
    }
}
