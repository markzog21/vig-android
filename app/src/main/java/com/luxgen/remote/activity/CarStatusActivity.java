package com.luxgen.remote.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomAppCompatActivity;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.option.CarOptions;
import com.luxgen.remote.network.CarWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.VigRequestData;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.car.CarWarnLevelA;
import com.luxgen.remote.network.model.car.CarWarnLevelB;
import com.luxgen.remote.network.model.car.GetCarStatusResponseData;
import com.luxgen.remote.ui.maintenance.MaintenanceActivity;
import com.luxgen.remote.ui.maintenance.MaintenanceQueryCarNoActivity;
import com.luxgen.remote.util.DialUtils;
import com.luxgen.remote.util.Prefs;

import java.util.ArrayList;

public class CarStatusActivity extends CustomAppCompatActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, CarStatusActivity.class);
    }

    private UserInfo mUserInfo = null;
    private CarKeyData mSelectedCarKeyData = null;

    private GetUserInfoAsyncTask mGetUserInfoAsyncTask = null;
    private GetSelectedCarKeyDataAsyncTask mGetSelectedCarKeyDataAsyncTask = null;

    private ArrayList<TextView> mUrgentAbnormalTextViewArrayList = new ArrayList<>();
    private ArrayList<TextView> mAbnormalTextViewArrayList = new ArrayList<>();
    private ArrayList<TextView> mNormalTextViewArrayList = new ArrayList<>();

    private ViewGroup mListLinearLayout = null;
    private TextView mEmergencyExceptionTextView = null;
    private TextView mCarModelTextView = null;
    private TextView mPlateNumberTextView = null;
    private TextView mLastUpdateTimeTextView = null;
    private TextView mAvailableTripTextView = null;
    private TextView mVoltageTextView = null;
    private TextView mLDWSTextView = null;
    private TextView mWheelPressureTextView = null;
    private TextView mEPASTextView = null;
    private TextView mAirBagTextView = null;
    private TextView mBrakeTextView = null;
    private TextView mEPBTextView = null;
    private TextView mABSTextView = null;
    private TextView mFuelTextView = null;
    private TextView mOilPressureTextView = null;
    private TextView mEngineAndGearboxTextView = null;
    private TextView mEngineWaterTemperatureTextView = null;
    private TextView mSOCTextView = null;
    private TextView mESCLTextView = null;
    private TextView mWheelPressureSensorAndBatteryTextView = null;
    private TextView mESCTextView = null;
    private TextView mTCSTextView = null;
    private TextView mAPATextView = null;
    private TextView mSNRLFTextView = null;
    private TextView mSNRRFTextView = null;
    private TextView mSNRLRTextView = null;
    private TextView mSNRRRTextView = null;
    private TextView mSNRLSTextView = null;
    private TextView mSNRRSTextView = null;
    private TextView mEVSTextView = null;
    private TextView mVigObuWifiTextView = null;
    private TextView mVigAppBleTextView = null;

    private Drawable mGoodDrawable = null;
    private Drawable mUrgentWarningDrawable = null;
    private Drawable mWarningDrawable = null;

    private boolean mIsVoltageBatteryAbnormal = false;
    private boolean mIsVoltageChargeAbnormal = false;
    private boolean mIsFuelAbnormal = false;
    private boolean mIsOilPressureAbnormal = false;
    private boolean mIsBrakeAbnormal = false;
    private boolean mIsEngineAndGearboxAbnormal = false;
    private boolean mIsABSAbnormal = false;
    private boolean mIsAirBagAbnormal = false;
    private boolean mIsEPBAbnormal = false;
    private boolean mIsWheelPressureAbnormal = false;
    private boolean mIsWheelPressureAbnormalUrgent = false;
    private boolean mIsEngineWaterTemperatureAbnormal = false;
    private boolean mIsSOCLowPower = false;
    private boolean mIsSOCVeryLowPower = false;
    private boolean mIsEPASAbnormal = false;
    private boolean mIsESCLAbnormal = false;
    private boolean mIsESCLAbnormalMiddle = false;
    private boolean mIsESCLAbnormalUrgent = false;
    private boolean mIsLDWSAbnormal = false;
    private boolean mIsWheelPressureSensorAndBatteryAbnormal = false;
    private boolean mIsEVSAbnormal = false;
    private boolean mIsTCSAbnormal = false;
    private boolean mIsESCAbnormal = false;
    private boolean mIsVigObuWifiAbnormal = false;
    private boolean mIsVigAppBleAbnormal = false;
    private boolean mIsAPAAbnormal = false;
    private boolean mIsSNRLFAbnormal = false;
    private boolean mIsSNRRFAbnormal = false;
    private boolean mIsSNRLRAbnormal = false;
    private boolean mIsSNRRRAbnormal = false;
    private boolean mIsSNRLSAbnormal = false;
    private boolean mIsSNRRSAbnormal = false;

    private class GetUserInfoAsyncTask extends AsyncTask<Void, Void, UserInfo> {

        @Override
        protected UserInfo doInBackground(Void... voids) {
            return Prefs.getUserInfo(CarStatusActivity.this);
        }

        @Override
        protected void onPostExecute(UserInfo userInfo) {
            mGetUserInfoAsyncTask = null;

            mUserInfo = userInfo;
            getSelectedCarKeyData();
        }
    }

    private class GetSelectedCarKeyDataAsyncTask extends AsyncTask<Void, Void, CarKeyData> {

        @Override
        protected CarKeyData doInBackground(Void... voids) {
            return Prefs.getCarKeyData(CarStatusActivity.this);
        }

        @Override
        protected void onPostExecute(CarKeyData carKeyData) {
            mGetSelectedCarKeyDataAsyncTask = null;

            if (carKeyData != null) {
                mSelectedCarKeyData = carKeyData;
                callAPIToGetCarStatus();
            } else {
                mSelectedCarKeyData = null;
                dismissProgressDialog();
            }
        }
    }

    private View.OnClickListener mOnEmergencyExceptionTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            startActivity(DialUtils.callService(getApplicationContext()));
        }
    };

    private View.OnClickListener mOnWarrantyReservationImageViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            startActivity(MaintenanceQueryCarNoActivity.newIntent(CarStatusActivity.this, 0));
        }
    };

    private View.OnClickListener mOnCallSalesImageViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            startActivity(DialUtils.callSales(getApplicationContext()));
        }
    };

    private View.OnClickListener mOnVoltageTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsVoltageBatteryAbnormal || mIsVoltageChargeAbnormal) {
                String dialogTitle = "";

                mAlertDialog1.restoreDefault();
                if (mIsVoltageBatteryAbnormal) {
                    dialogTitle = getString(R.string.car_status_voltage_battery_abnormal_dialog_title);
                }
                if (mIsVoltageChargeAbnormal) {
                    if (!TextUtils.isEmpty(dialogTitle)) {
                        dialogTitle = dialogTitle.concat("\n");
                    }
                    dialogTitle = dialogTitle.concat(getString(R.string.car_status_voltage_charge_abnormal_dialog_title));
                }
                mAlertDialog1.setTitle(dialogTitle);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnFuelTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsFuelAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_fuel_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnOilPressureTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsOilPressureAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_oil_pressure_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnBrakeTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsBrakeAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_brake_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnEngineAndGearboxTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsEngineAndGearboxAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_engine_and_gearbox_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnABSTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsABSAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_abs_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnAirBagTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsAirBagAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_air_bag_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnEPBTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsEPBAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_epb_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnWheelPressureTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsWheelPressureAbnormalUrgent) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_wheel_pressure_abnormal_urgent_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            } else if (mIsWheelPressureAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_wheel_pressure_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnEngineWaterTemperatureTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsEngineWaterTemperatureAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_engine_water_temperature_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnSOCTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsSOCLowPower) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_soc_low_power_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            } else if (mIsSOCVeryLowPower) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_soc_very_low_power_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnEPASTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsEPASAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_epas_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnESCLTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsESCLAbnormalUrgent || mIsESCLAbnormalMiddle || mIsESCLAbnormal) {
                String dialogTitle = "";

                mAlertDialog1.restoreDefault();
                if (mIsESCLAbnormalUrgent) {
                    dialogTitle = dialogTitle.concat(getString(R.string.car_status_escl_abnormal_urgent_dialog_title));
                }
                if (mIsESCLAbnormalMiddle) {
                    if (!TextUtils.isEmpty(dialogTitle)) {
                        dialogTitle = dialogTitle.concat("\n");
                    }
                    dialogTitle = dialogTitle.concat(getString(R.string.car_status_escl_abnormal_middle_dialog_title));
                }
                if (mIsESCLAbnormal) {
                    if (!TextUtils.isEmpty(dialogTitle)) {
                        dialogTitle = dialogTitle.concat("\n");
                    }
                    dialogTitle = dialogTitle.concat(getString(R.string.car_status_escl_abnormal_dialog_title));
                }
                mAlertDialog1.setTitle(dialogTitle);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnLDWSTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsLDWSAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_ldws_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnWheelPressureSensorAndBatteryTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsWheelPressureSensorAndBatteryAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_wheel_pressure_sensor_and_battery_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnEVSTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsEVSAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_evs_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnTCSTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsTCSAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_tcs_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnESCTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsESCAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_esc_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnVigObuWifiTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsVigObuWifiAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_vig_obu_wifi_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnVigAppBleTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsVigAppBleAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_vig_app_ble_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnAPATextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsAPAAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_apa_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnSNRLFTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsSNRLFAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_snr_lf_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnSNRRFTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsSNRRFAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_snr_rf_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnSNRLRTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsSNRLRAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_snr_lr_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnSNRRRTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsSNRRRAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_snr_rr_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnSNRLSTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsSNRLSAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_snr_ls_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private View.OnClickListener mOnSNRRSTextViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mIsSNRRSAbnormal) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.car_status_snr_rs_abnormal_dialog_title);
                mAlertDialog1.setMessage((String) v.getTag());
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        v1 -> mAlertDialog1.dismiss());
                mAlertDialog1.show();
            }
        }
    };

    private OnApiListener<GetCarStatusResponseData> mGetCarStatusApiListener = new OnApiListener<GetCarStatusResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetCarStatusResponseData responseData) {
            if (responseData.isSuccessful()) {
                updateCarStatusDisplay(responseData);
            } else {
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }
            dismissProgressDialog();
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(getString(R.string.dialog_title_error), failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_status);

        LinearLayout mainContainer = findViewById(R.id.activity_container);
        setWheelScreenLayout(mainContainer);

        ImageView wheelRightHandle = findViewById(R.id.wheel_right_handle_image_view);
        ImageView wheelLeftHandle = findViewById(R.id.wheel_left_handle_image_view);
        setWheelHandles(wheelRightHandle, wheelLeftHandle);

        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            toolBar.setTitle(R.string.activity_car_status_title);
            setSupportActionBar(toolBar);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        LayoutInflater layoutInflater = LayoutInflater.from(this);

        mEmergencyExceptionTextView = findViewById(R.id.activity_car_status_emergency_exception_text_view);
        mEmergencyExceptionTextView.setOnClickListener(mOnEmergencyExceptionTextViewClickListener);
        ImageView warrantyReservationImageView = findViewById(R.id.activity_car_status_warranty_reservation_image_view);
        warrantyReservationImageView.setOnClickListener(mOnWarrantyReservationImageViewClickListener);
        ImageView callSalesImageView = findViewById(R.id.activity_car_status_call_sales_image_view);
        callSalesImageView.setOnClickListener(mOnCallSalesImageViewClickListener);
        mCarModelTextView = findViewById(R.id.activity_car_status_car_model_text_view);
        mPlateNumberTextView = findViewById(R.id.activity_car_status_plate_number_text_view);
        mLastUpdateTimeTextView = findViewById(R.id.activity_car_status_last_update_time_text_view);
        mAvailableTripTextView = findViewById(R.id.activity_car_status_available_trip_text_view);
        mVoltageTextView = findViewById(R.id.activity_car_status_voltage_text_view);
        mVoltageTextView.setOnClickListener(mOnVoltageTextViewClickListener);

        mListLinearLayout = findViewById(R.id.activity_car_status_list_linear_layout);
        mFuelTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mFuelTextView.setText(R.string.car_status_fuel);
        mFuelTextView.setOnClickListener(mOnFuelTextViewClickListener);
        mOilPressureTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mOilPressureTextView.setText(R.string.car_status_oil_pressure);
        mOilPressureTextView.setOnClickListener(mOnOilPressureTextViewClickListener);
        mBrakeTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mBrakeTextView.setText(R.string.car_status_brake);
        mBrakeTextView.setOnClickListener(mOnBrakeTextViewClickListener);
        mEngineAndGearboxTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mEngineAndGearboxTextView.setText(R.string.car_status_engine_and_gearbox);
        mEngineAndGearboxTextView.setOnClickListener(mOnEngineAndGearboxTextViewClickListener);
        mABSTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mABSTextView.setText(R.string.car_status_abs);
        mABSTextView.setOnClickListener(mOnABSTextViewClickListener);
        mAirBagTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mAirBagTextView.setText(R.string.car_status_air_bag);
        mAirBagTextView.setOnClickListener(mOnAirBagTextViewClickListener);
        mEPBTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mEPBTextView.setText(R.string.car_status_epb);
        mEPBTextView.setOnClickListener(mOnEPBTextViewClickListener);
        mWheelPressureTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mWheelPressureTextView.setText(R.string.car_status_wheel_pressure);
        mWheelPressureTextView.setOnClickListener(mOnWheelPressureTextViewClickListener);
        mEngineWaterTemperatureTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mEngineWaterTemperatureTextView.setText(R.string.car_status_engine_water_temperature);
        mEngineWaterTemperatureTextView.setOnClickListener(mOnEngineWaterTemperatureTextViewClickListener);
        mSOCTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mSOCTextView.setText(R.string.car_status_soc);
        mSOCTextView.setOnClickListener(mOnSOCTextViewClickListener);
        mEPASTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mEPASTextView.setText(R.string.car_status_epas);
        mEPASTextView.setOnClickListener(mOnEPASTextViewClickListener);
        mESCLTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mESCLTextView.setText(R.string.car_status_escl);
        mESCLTextView.setOnClickListener(mOnESCLTextViewClickListener);
        mLDWSTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mLDWSTextView.setText(R.string.car_status_ldws);
        mLDWSTextView.setOnClickListener(mOnLDWSTextViewClickListener);
        mWheelPressureSensorAndBatteryTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mWheelPressureSensorAndBatteryTextView.setText(R.string.car_status_wheel_pressure_sensor_and_battery);
        mWheelPressureSensorAndBatteryTextView.setOnClickListener(mOnWheelPressureSensorAndBatteryTextViewClickListener);
        mEVSTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mEVSTextView.setText(R.string.car_status_evs);
        mEVSTextView.setOnClickListener(mOnEVSTextViewClickListener);
        mTCSTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mTCSTextView.setText(R.string.car_status_tcs);
        mTCSTextView.setOnClickListener(mOnTCSTextViewClickListener);
        mESCTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mESCTextView.setText(R.string.car_status_esc);
        mESCTextView.setOnClickListener(mOnESCTextViewClickListener);
        mVigObuWifiTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mVigObuWifiTextView.setText(R.string.car_status_vig_obu_wifi);
        mVigObuWifiTextView.setOnClickListener(mOnVigObuWifiTextViewClickListener);
        mVigAppBleTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mVigAppBleTextView.setText(R.string.car_status_vig_app_ble);
        mVigAppBleTextView.setOnClickListener(mOnVigAppBleTextViewClickListener);
        mAPATextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mAPATextView.setText(R.string.car_status_apa);
        mAPATextView.setOnClickListener(mOnAPATextViewClickListener);
        mSNRLFTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mSNRLFTextView.setText(R.string.car_status_snr_lf);
        mSNRLFTextView.setOnClickListener(mOnSNRLFTextViewClickListener);
        mSNRRFTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mSNRRFTextView.setText(R.string.car_status_snr_rf);
        mSNRRFTextView.setOnClickListener(mOnSNRRFTextViewClickListener);
        mSNRLRTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mSNRLRTextView.setText(R.string.car_status_snr_lr);
        mSNRLRTextView.setOnClickListener(mOnSNRLRTextViewClickListener);
        mSNRRRTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mSNRRRTextView.setText(R.string.car_status_snr_rr);
        mSNRRRTextView.setOnClickListener(mOnSNRRRTextViewClickListener);
        mSNRLSTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mSNRLSTextView.setText(R.string.car_status_snr_ls);
        mSNRLSTextView.setOnClickListener(mOnSNRLSTextViewClickListener);
        mSNRRSTextView = (TextView) layoutInflater.inflate(R.layout.item_car_status, mListLinearLayout, false);
        mSNRRSTextView.setText(R.string.car_status_snr_rs);
        mSNRRSTextView.setOnClickListener(mOnSNRRSTextViewClickListener);

        mGoodDrawable = getDrawable(R.drawable.ic_status_good);
        mUrgentWarningDrawable = getDrawable(R.drawable.ic_status_urgent_warning);
        mWarningDrawable = getDrawable(R.drawable.ic_status_warning);

        bindCaService();
    }

    @Override
    protected void onCaServiceConnected() {
        init();
        super.onCaServiceConnected();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_car_status_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;

            case R.id.action_refresh:
                refreshCarStatusData();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        if (mGetUserInfoAsyncTask != null) {
            mGetUserInfoAsyncTask.cancel(true);
        }

        if (mGetSelectedCarKeyDataAsyncTask != null) {
            mGetSelectedCarKeyDataAsyncTask.cancel(true);
        }

        unbindCaService();
        super.onDestroy();
    }

    @Override
    protected void onReloadSelectedCarKeyData(CarKeyData selectedCarKeyData) {
        mSelectedCarKeyData = selectedCarKeyData;
    }

    private void init() {
        showProgressDialog();
        if (mGetUserInfoAsyncTask == null) {
            mGetUserInfoAsyncTask = new GetUserInfoAsyncTask();
            mGetUserInfoAsyncTask.execute();
        }
    }

    private void getSelectedCarKeyData() {
        showProgressDialog();
        if (mGetSelectedCarKeyDataAsyncTask == null) {
            mGetSelectedCarKeyDataAsyncTask = new GetSelectedCarKeyDataAsyncTask();
            mGetSelectedCarKeyDataAsyncTask.execute();
        }
    }

    private void callAPIToGetCarStatus() {
        if (mSelectedCarKeyData != null) {
            showProgressDialog();
            CarWebApi carWebApi = CarWebApi.getInstance(this);

            VigRequestData vigRequestData = new VigRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken(), mSelectedCarKeyData.getCarId());
            carWebApi.getCarStatus(vigRequestData, mGetCarStatusApiListener);
        }
    }

    private void refreshCarStatusData() {
        callAPIToGetCarStatus();
        Prefs.setRefreshCarStatusFlag(CarStatusActivity.this, true);
    }

    private void updateCarStatusDisplay(GetCarStatusResponseData carStatusData) {
        if (carStatusData != null) {
            boolean showEmergencyExceptionView = false;

            CarWarnLevelA carWarnLevelA = carStatusData.getWarnLevelA();
            CarWarnLevelB carWarnLevelB = carStatusData.getWarnLevelB();

            mCarModelTextView.setText(carStatusData.getModel());

            mPlateNumberTextView.setText(carStatusData.getCarNo());

            String eventDateString = carStatusData.getEventDate();
            if (eventDateString != null && eventDateString.length() > 16) {
                eventDateString = eventDateString.substring(0, 16);
                String lastUpdateTimeString = getString(R.string.car_status_last_update_time);
                lastUpdateTimeString = String.format(lastUpdateTimeString, eventDateString);
                mLastUpdateTimeTextView.setText(lastUpdateTimeString);
            } else {
                mLastUpdateTimeTextView.setText(" ");
            }

            int availableTrip = Double.valueOf(carStatusData.getAvailableTrip()).intValue();
            if (availableTrip > -1) {
                String availableTripString = getString(R.string.car_status_available_trip);
                availableTripString = String.format(availableTripString, availableTrip);
                mAvailableTripTextView.setText(availableTripString);
            } else {
                mAvailableTripTextView.setText(R.string.car_status_no_data_upload);
            }

            mListLinearLayout.removeAllViews();
            if (carWarnLevelA != null && carWarnLevelB != null) {

                if (carWarnLevelA.getBatLowStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelA.getBatChargeStatus() == CarOptions.ABNORMAL_1) {
                    showEmergencyExceptionView = true;
                    mVoltageTextView.setText(R.string.car_status_voltage_abnormal);
                    mVoltageTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mUrgentWarningDrawable, null);
                    String abnormalReminder = "";
                    if (carWarnLevelA.getBatLowStatus() == CarOptions.ABNORMAL_1) {
                        mIsVoltageBatteryAbnormal = true;
                        abnormalReminder = abnormalReminder.concat(getString(R.string.car_status_voltage_abnormal_battery_reminder));
                    } else {
                        mIsVoltageBatteryAbnormal = false;
                    }
                    if (carWarnLevelA.getBatChargeStatus() == CarOptions.ABNORMAL_1) {
                        mIsVoltageChargeAbnormal = true;
                        if (abnormalReminder.length() != 0) {
                            abnormalReminder = abnormalReminder.concat("\n");
                        }
                        abnormalReminder = abnormalReminder.concat(getString(R.string.car_status_voltage_abnormal_charge_reminder));
                    } else {
                        mIsVoltageChargeAbnormal = false;
                    }
                    mVoltageTextView.setTag(abnormalReminder);
                } else if (carWarnLevelA.getBatLowStatus() == CarOptions.NORMAL ||
                        carWarnLevelA.getBatChargeStatus() == CarOptions.NORMAL) {
                    mIsVoltageBatteryAbnormal = false;
                    mIsVoltageChargeAbnormal = false;
                    mVoltageTextView.setText(R.string.car_status_voltage_normal);
                    mVoltageTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mVoltageTextView.setTag(null);
                } else {
                    mIsVoltageBatteryAbnormal = false;
                    mIsVoltageChargeAbnormal = false;
                    mVoltageTextView.setText(R.string.car_status_voltage_unknown);
                    mVoltageTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mVoltageTextView.setTag(null);
                }

                if (carWarnLevelA.getLowFuelStatus() == CarOptions.ABNORMAL_1) {
                    showEmergencyExceptionView = true;
                    mIsFuelAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_fuel_abnormal_reminder);
                    mFuelTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mUrgentWarningDrawable, null);
                    mFuelTextView.setVisibility(View.VISIBLE);
                    mFuelTextView.setTag(abnormalReminder);
                } else if (carWarnLevelA.getLowFuelStatus() == CarOptions.NORMAL) {
                    mIsFuelAbnormal = false;
                    mFuelTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mFuelTextView.setVisibility(View.VISIBLE);
                    mFuelTextView.setTag(null);
                } else {
                    mIsFuelAbnormal = false;
                    mFuelTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mFuelTextView.setVisibility(View.GONE);
                    mFuelTextView.setTag(null);
                }

                if (mIsFuelAbnormal) {
                    mUrgentAbnormalTextViewArrayList.add(mFuelTextView);
                } else {
                    mNormalTextViewArrayList.add(mFuelTextView);
                }

                if (carWarnLevelA.getOilPressureLedStatus() == CarOptions.ABNORMAL_1) {
                    showEmergencyExceptionView = true;
                    mIsOilPressureAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_oil_pressure_abnormal_reminder);
                    mOilPressureTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mUrgentWarningDrawable, null);
                    mOilPressureTextView.setVisibility(View.VISIBLE);
                    mOilPressureTextView.setTag(abnormalReminder);
                } else if (carWarnLevelA.getOilPressureLedStatus() == CarOptions.NORMAL) {
                    mIsOilPressureAbnormal = false;
                    mOilPressureTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mOilPressureTextView.setVisibility(View.VISIBLE);
                    mOilPressureTextView.setTag(null);
                } else {
                    mIsOilPressureAbnormal = false;
                    mOilPressureTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mOilPressureTextView.setVisibility(View.GONE);
                    mOilPressureTextView.setTag(null);
                }

                if (mIsOilPressureAbnormal) {
                    mUrgentAbnormalTextViewArrayList.add(mOilPressureTextView);
                } else {
                    mNormalTextViewArrayList.add(mOilPressureTextView);
                }

                if (carWarnLevelA.getLowBrakeFluidStaStatus() == CarOptions.ABNORMAL_1) {
                    showEmergencyExceptionView = true;
                    mIsBrakeAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_brake_abnormal_reminder);
                    mBrakeTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mUrgentWarningDrawable, null);
                    mBrakeTextView.setVisibility(View.VISIBLE);
                    mBrakeTextView.setTag(abnormalReminder);
                } else if (carWarnLevelA.getLowBrakeFluidStaStatus() == CarOptions.NORMAL) {
                    mIsBrakeAbnormal = false;
                    mBrakeTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mBrakeTextView.setVisibility(View.VISIBLE);
                    mBrakeTextView.setTag(null);
                } else {
                    mIsBrakeAbnormal = false;
                    mBrakeTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mBrakeTextView.setVisibility(View.GONE);
                    mBrakeTextView.setTag(null);
                }

                if (mIsBrakeAbnormal) {
                    mUrgentAbnormalTextViewArrayList.add(mBrakeTextView);
                } else {
                    mNormalTextViewArrayList.add(mBrakeTextView);
                }

                if (carWarnLevelA.getLvCannStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelA.getCanecuSvlStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelA.getCanecuStateStatus() == CarOptions.ABNORMAL_1) {
                    showEmergencyExceptionView = true;
                    mIsEngineAndGearboxAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_engine_and_gearbox_abnormal_reminder);
                    mEngineAndGearboxTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mUrgentWarningDrawable, null);
                    mEngineAndGearboxTextView.setVisibility(View.VISIBLE);
                    mEngineAndGearboxTextView.setTag(abnormalReminder);
                } else if (carWarnLevelA.getLvCannStatus() == CarOptions.NORMAL ||
                        carWarnLevelA.getCanecuSvlStatus() == CarOptions.NORMAL ||
                        carWarnLevelA.getCanecuStateStatus() == CarOptions.NORMAL) {
                    mIsEngineAndGearboxAbnormal = false;
                    mEngineAndGearboxTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mEngineAndGearboxTextView.setVisibility(View.VISIBLE);
                    mEngineAndGearboxTextView.setTag(null);
                } else {
                    mIsEngineAndGearboxAbnormal = false;
                    mEngineAndGearboxTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mEngineAndGearboxTextView.setVisibility(View.GONE);
                    mEngineAndGearboxTextView.setTag(null);
                }

                if (mIsEngineAndGearboxAbnormal) {
                    mUrgentAbnormalTextViewArrayList.add(mEngineAndGearboxTextView);
                } else {
                    mNormalTextViewArrayList.add(mEngineAndGearboxTextView);
                }

                if (carWarnLevelA.getAbsStatus() == CarOptions.ABNORMAL_1) {
                    showEmergencyExceptionView = true;
                    mIsABSAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_abs_abnormal_reminder);
                    mABSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mUrgentWarningDrawable, null);
                    mABSTextView.setVisibility(View.VISIBLE);
                    mABSTextView.setTag(abnormalReminder);
                } else if (carWarnLevelA.getAbsStatus() == CarOptions.NORMAL) {
                    mIsABSAbnormal = false;
                    mABSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mABSTextView.setVisibility(View.VISIBLE);
                    mABSTextView.setTag(null);
                } else {
                    mIsABSAbnormal = false;
                    mABSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mABSTextView.setVisibility(View.GONE);
                    mABSTextView.setTag(null);
                }

                if (mIsABSAbnormal) {
                    mUrgentAbnormalTextViewArrayList.add(mABSTextView);
                } else {
                    mNormalTextViewArrayList.add(mABSTextView);
                }

                if (carWarnLevelA.getAbmAirBagStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelA.getSrsLedStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelA.getAirBagFailCmd() == CarOptions.ABNORMAL_1) {
                    showEmergencyExceptionView = true;
                    mIsAirBagAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_air_bag_abnormal_reminder);
                    mAirBagTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mUrgentWarningDrawable, null);
                    mAirBagTextView.setVisibility(View.VISIBLE);
                    mAirBagTextView.setTag(abnormalReminder);
                } else if (carWarnLevelA.getAbmAirBagStatus() == CarOptions.NORMAL ||
                        carWarnLevelA.getSrsLedStatus() == CarOptions.NORMAL ||
                        carWarnLevelA.getAirBagFailCmd() == CarOptions.NORMAL) {
                    mIsAirBagAbnormal = false;
                    mAirBagTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mAirBagTextView.setVisibility(View.VISIBLE);
                    mAirBagTextView.setTag(null);
                } else {
                    mIsAirBagAbnormal = false;
                    mAirBagTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mAirBagTextView.setVisibility(View.GONE);
                    mAirBagTextView.setTag(null);
                }

                if (mIsAirBagAbnormal) {
                    mUrgentAbnormalTextViewArrayList.add(mAirBagTextView);
                } else {
                    mNormalTextViewArrayList.add(mAirBagTextView);
                }

                if (carWarnLevelA.getEpbStatus() == CarOptions.ABNORMAL_1) {
                    showEmergencyExceptionView = true;
                    mIsEPBAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_epb_abnormal_reminder);
                    mEPBTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mUrgentWarningDrawable, null);
                    mEPBTextView.setVisibility(View.VISIBLE);
                    mEPBTextView.setTag(abnormalReminder);
                } else if (carWarnLevelA.getEpbStatus() == CarOptions.NORMAL) {
                    mIsEPBAbnormal = false;
                    mEPBTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mEPBTextView.setVisibility(View.VISIBLE);
                    mEPBTextView.setTag(null);
                } else {
                    mIsEPBAbnormal = false;
                    mEPBTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mEPBTextView.setVisibility(View.GONE);
                    mEPBTextView.setTag(null);
                }

                if (mIsEPBAbnormal) {
                    mUrgentAbnormalTextViewArrayList.add(mEPBTextView);
                } else {
                    mNormalTextViewArrayList.add(mEPBTextView);
                }

                if (carWarnLevelB.getExWheelPressureRfStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelB.getExWheelPressureLfStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelB.getExWheelPressureRrStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelB.getExWheelPressureLrStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelB.getWheelPressureLfStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelB.getWheelPressureRrStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelB.getWheelPressureLrStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelB.getWheelPressureLrStatus() == CarOptions.ABNORMAL_1) {
                    mIsWheelPressureAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_wheel_pressure_abnormal_reminder);
                    mWheelPressureTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mWarningDrawable, null);
                    mWheelPressureTextView.setVisibility(View.VISIBLE);
                    mWheelPressureTextView.setTag(abnormalReminder);
                } else if (carWarnLevelB.getExWheelPressureRfStatus() == CarOptions.NORMAL ||
                        carWarnLevelB.getExWheelPressureLfStatus() == CarOptions.NORMAL ||
                        carWarnLevelB.getExWheelPressureRrStatus() == CarOptions.NORMAL ||
                        carWarnLevelB.getExWheelPressureLrStatus() == CarOptions.NORMAL ||
                        carWarnLevelB.getWheelPressureLfStatus() == CarOptions.NORMAL ||
                        carWarnLevelB.getWheelPressureRrStatus() == CarOptions.NORMAL ||
                        carWarnLevelB.getWheelPressureLrStatus() == CarOptions.NORMAL ||
                        carWarnLevelB.getWheelPressureLrStatus() == CarOptions.NORMAL) {
                    mIsWheelPressureAbnormal = false;
                    mWheelPressureTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mWheelPressureTextView.setVisibility(View.VISIBLE);
                    mWheelPressureTextView.setTag(null);
                } else {
                    mIsWheelPressureAbnormal = false;
                    mWheelPressureTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mWheelPressureTextView.setVisibility(View.GONE);
                    mWheelPressureTextView.setTag(null);
                }

                if (carWarnLevelA.getExWheelPressureRfStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelA.getExWheelPressureLfStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelA.getExWheelPressureRrStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelA.getExWheelPressureLrStatus() == CarOptions.ABNORMAL_1) {
                    showEmergencyExceptionView = true;
                    mIsWheelPressureAbnormalUrgent = true;
                    String abnormalReminder = getString(R.string.car_status_wheel_pressure_abnormal_urgent_reminder);
                    mWheelPressureTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mUrgentWarningDrawable, null);
                    mWheelPressureTextView.setVisibility(View.VISIBLE);
                    mWheelPressureTextView.setTag(abnormalReminder);
                } else if (carWarnLevelA.getExWheelPressureRfStatus() == CarOptions.NORMAL ||
                        carWarnLevelA.getExWheelPressureLfStatus() == CarOptions.NORMAL ||
                        carWarnLevelA.getExWheelPressureRrStatus() == CarOptions.NORMAL ||
                        carWarnLevelA.getExWheelPressureLrStatus() == CarOptions.NORMAL) {
                    mIsWheelPressureAbnormalUrgent = false;
                    if (!mIsWheelPressureAbnormal) {
                        mWheelPressureTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                        mWheelPressureTextView.setVisibility(View.VISIBLE);
                        mWheelPressureTextView.setTag(null);
                    }
                } else {
                    mIsWheelPressureAbnormalUrgent = false;
                    if (!mIsWheelPressureAbnormal) {
                        mWheelPressureTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                        mWheelPressureTextView.setVisibility(View.GONE);
                        mWheelPressureTextView.setTag(null);
                    }
                }

                if (mIsWheelPressureAbnormalUrgent) {
                    mUrgentAbnormalTextViewArrayList.add(mWheelPressureTextView);
                } else if (mIsWheelPressureAbnormal) {
                    mAbnormalTextViewArrayList.add(mWheelPressureTextView);
                } else {
                    mNormalTextViewArrayList.add(mWheelPressureTextView);
                }

                if (carWarnLevelA.getAlrtTempEauStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelA.getCantcosta() == CarOptions.ABNORMAL_1) {
                    showEmergencyExceptionView = true;
                    mIsEngineWaterTemperatureAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_engine_water_temperature_abnormal_reminder);
                    mEngineWaterTemperatureTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mUrgentWarningDrawable, null);
                    mEngineWaterTemperatureTextView.setVisibility(View.VISIBLE);
                    mEngineWaterTemperatureTextView.setTag(abnormalReminder);
                } else if (carWarnLevelA.getAlrtTempEauStatus() == CarOptions.NORMAL ||
                        carWarnLevelA.getCantcosta() == CarOptions.NORMAL) {
                    mIsEngineWaterTemperatureAbnormal = false;
                    mEngineWaterTemperatureTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mEngineWaterTemperatureTextView.setVisibility(View.VISIBLE);
                    mEngineWaterTemperatureTextView.setTag(null);
                } else {
                    mIsEngineWaterTemperatureAbnormal = false;
                    mEngineWaterTemperatureTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mEngineWaterTemperatureTextView.setVisibility(View.GONE);
                    mEngineWaterTemperatureTextView.setTag(null);
                }

                if (mIsEngineWaterTemperatureAbnormal) {
                    mUrgentAbnormalTextViewArrayList.add(mEngineWaterTemperatureTextView);
                } else {
                    mNormalTextViewArrayList.add(mEngineWaterTemperatureTextView);
                }

                if (carWarnLevelA.getLowSocStatus() == CarOptions.ABNORMAL_1) {
                    showEmergencyExceptionView = true;
                    mIsSOCLowPower = true;
                    mIsSOCVeryLowPower = false;
                    String abnormalReminder = getString(R.string.car_status_soc_low_power_reminder);
                    mSOCTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mUrgentWarningDrawable, null);
                    mSOCTextView.setVisibility(View.VISIBLE);
                    mSOCTextView.setTag(abnormalReminder);
                } else if (carWarnLevelA.getLowSocStatus() == CarOptions.ABNORMAL_2) {
                    showEmergencyExceptionView = true;
                    mIsSOCLowPower = false;
                    mIsSOCVeryLowPower = true;
                    String abnormalReminder = getString(R.string.car_status_soc_very_low_power_reminder);
                    mSOCTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mUrgentWarningDrawable, null);
                    mSOCTextView.setVisibility(View.VISIBLE);
                    mSOCTextView.setTag(abnormalReminder);
                } else if (carWarnLevelA.getLowSocStatus() == CarOptions.NORMAL) {
                    mIsSOCLowPower = false;
                    mIsSOCVeryLowPower = false;
                    mSOCTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mSOCTextView.setVisibility(View.VISIBLE);
                    mSOCTextView.setTag(null);
                } else {
                    mIsSOCLowPower = false;
                    mIsSOCVeryLowPower = false;
                    mSOCTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mSOCTextView.setVisibility(View.GONE);
                    mSOCTextView.setTag(null);
                }

                if (mIsSOCLowPower || mIsSOCVeryLowPower) {
                    mUrgentAbnormalTextViewArrayList.add(mSOCTextView);
                } else {
                    mNormalTextViewArrayList.add(mSOCTextView);
                }

                if (carWarnLevelA.getEpasStatus() == CarOptions.ABNORMAL_1) {
                    showEmergencyExceptionView = true;
                    mIsEPASAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_epas_abnormal_reminder);
                    mEPASTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mUrgentWarningDrawable, null);
                    mEPASTextView.setVisibility(View.VISIBLE);
                    mEPASTextView.setTag(abnormalReminder);
                } else if (carWarnLevelA.getEpasStatus() == CarOptions.NORMAL) {
                    mIsEPASAbnormal = false;
                    mEPASTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mEPASTextView.setVisibility(View.VISIBLE);
                    mEPASTextView.setTag(null);
                } else {
                    mIsEPASAbnormal = false;
                    mEPASTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mEPASTextView.setVisibility(View.GONE);
                    mEPASTextView.setTag(null);
                }

                if (mIsEPASAbnormal) {
                    mUrgentAbnormalTextViewArrayList.add(mEPASTextView);
                } else {
                    mNormalTextViewArrayList.add(mEPASTextView);
                }

                if (carWarnLevelA.getEsclCriticalStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelA.getEsclFunctionStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelA.getRotateSteerWheelStatus() == CarOptions.ABNORMAL_1) {
                    showEmergencyExceptionView = true;
                    mIsESCLAbnormalUrgent = carWarnLevelA.getEsclCriticalStatus() == CarOptions.ABNORMAL_1;
                    mIsESCLAbnormalMiddle = carWarnLevelA.getEsclFunctionStatus() == CarOptions.ABNORMAL_1;
                    mIsESCLAbnormal = carWarnLevelA.getRotateSteerWheelStatus() == CarOptions.ABNORMAL_1;
                    mESCLTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mUrgentWarningDrawable, null);
                    mESCLTextView.setVisibility(View.VISIBLE);
                    String abnormalReminder = "";
                    if (carWarnLevelA.getEsclCriticalStatus() == CarOptions.ABNORMAL_1) {
                        abnormalReminder = abnormalReminder.concat(getString(R.string.car_status_escl_abnormal_urgent_reminder));
                    }
                    if (carWarnLevelA.getEsclFunctionStatus() == CarOptions.ABNORMAL_1) {
                        if (abnormalReminder.length() != 0) {
                            abnormalReminder = abnormalReminder.concat("\n");
                        }
                        abnormalReminder = abnormalReminder.concat(getString(R.string.car_status_escl_abnormal_middle_reminder));
                    }
                    if (carWarnLevelA.getRotateSteerWheelStatus() == CarOptions.ABNORMAL_1) {
                        if (abnormalReminder.length() != 0) {
                            abnormalReminder = abnormalReminder.concat("\n");
                        }
                        abnormalReminder = abnormalReminder.concat(getString(R.string.car_status_escl_abnormal_reminder));
                    }

                    mESCLTextView.setTag(abnormalReminder);
                } else if (carWarnLevelA.getEsclCriticalStatus() == CarOptions.NORMAL ||
                        carWarnLevelA.getEsclFunctionStatus() == CarOptions.NORMAL ||
                        carWarnLevelA.getRotateSteerWheelStatus() == CarOptions.NORMAL) {
                    mIsESCLAbnormalUrgent = false;
                    mIsESCLAbnormalMiddle = false;
                    mIsESCLAbnormal = false;
                    mESCLTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mESCLTextView.setVisibility(View.VISIBLE);
                    mESCLTextView.setTag(null);
                } else {
                    mIsESCLAbnormalUrgent = false;
                    mIsESCLAbnormalMiddle = false;
                    mIsESCLAbnormal = false;
                    mESCLTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mESCLTextView.setVisibility(View.GONE);
                    mESCLTextView.setTag(null);
                }

                if (mIsESCLAbnormalUrgent || mIsESCLAbnormalMiddle || mIsESCLAbnormal) {
                    mUrgentAbnormalTextViewArrayList.add(mESCLTextView);
                } else {
                    mNormalTextViewArrayList.add(mESCLTextView);
                }

                if (carWarnLevelB.getLdwsStatus() == CarOptions.ABNORMAL_1) {
                    mIsLDWSAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_ldws_abnormal_reminder);
                    mLDWSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mWarningDrawable, null);
                    mLDWSTextView.setVisibility(View.VISIBLE);
                    mLDWSTextView.setTag(abnormalReminder);
                } else if (carWarnLevelB.getLdwsStatus() == CarOptions.NORMAL) {
                    mIsLDWSAbnormal = false;
                    mLDWSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mLDWSTextView.setVisibility(View.VISIBLE);
                    mLDWSTextView.setTag(null);
                } else {
                    mIsLDWSAbnormal = false;
                    mLDWSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mLDWSTextView.setVisibility(View.GONE);
                    mLDWSTextView.setTag(null);
                }

                if (mIsLDWSAbnormal) {
                    mAbnormalTextViewArrayList.add(mLDWSTextView);
                } else {
                    mNormalTextViewArrayList.add(mLDWSTextView);
                }

                if (carWarnLevelB.getWhellPressureSensorLfStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelB.getWhellPressureSensorRfStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelB.getWhellPressureSensorLrStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelB.getWhellPressureSensorRrStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelB.getWhellPressureBatLfStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelB.getWhellPressureBatRfStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelB.getWhellPressureBatLrStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelB.getWhellPressureBatRrStatus() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelB.getTpmsExist() == CarOptions.ABNORMAL_1 ||
                        carWarnLevelB.getTpmsWarnInd() == CarOptions.ABNORMAL_1) {
                    mIsWheelPressureSensorAndBatteryAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_wheel_pressure_sensor_and_battery_abnormal_reminder);
                    mWheelPressureSensorAndBatteryTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mWarningDrawable, null);
                    mWheelPressureSensorAndBatteryTextView.setVisibility(View.VISIBLE);
                    mWheelPressureSensorAndBatteryTextView.setTag(abnormalReminder);
                } else if (carWarnLevelB.getWhellPressureSensorLfStatus() == CarOptions.NORMAL ||
                        carWarnLevelB.getWhellPressureSensorRfStatus() == CarOptions.NORMAL ||
                        carWarnLevelB.getWhellPressureSensorLrStatus() == CarOptions.NORMAL ||
                        carWarnLevelB.getWhellPressureSensorRrStatus() == CarOptions.NORMAL ||
                        carWarnLevelB.getWhellPressureBatLfStatus() == CarOptions.NORMAL ||
                        carWarnLevelB.getWhellPressureBatRfStatus() == CarOptions.NORMAL ||
                        carWarnLevelB.getWhellPressureBatLrStatus() == CarOptions.NORMAL ||
                        carWarnLevelB.getWhellPressureBatRrStatus() == CarOptions.NORMAL ||
                        carWarnLevelB.getTpmsExist() == CarOptions.NORMAL ||
                        carWarnLevelB.getTpmsWarnInd() == CarOptions.NORMAL) {
                    mIsWheelPressureSensorAndBatteryAbnormal = false;
                    mWheelPressureSensorAndBatteryTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mWheelPressureSensorAndBatteryTextView.setVisibility(View.VISIBLE);
                    mWheelPressureSensorAndBatteryTextView.setTag(null);
                } else {
                    mIsWheelPressureSensorAndBatteryAbnormal = false;
                    mWheelPressureSensorAndBatteryTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mWheelPressureSensorAndBatteryTextView.setVisibility(View.GONE);
                    mWheelPressureSensorAndBatteryTextView.setTag(null);
                }

                if (mIsWheelPressureSensorAndBatteryAbnormal) {
                    mAbnormalTextViewArrayList.add(mWheelPressureSensorAndBatteryTextView);
                } else {
                    mNormalTextViewArrayList.add(mWheelPressureSensorAndBatteryTextView);
                }

                if (carWarnLevelB.getEvsStatus() == CarOptions.ABNORMAL_1) {
                    mIsEVSAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_evs_abnormal_reminder);
                    mEVSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mWarningDrawable, null);
                    mEVSTextView.setVisibility(View.VISIBLE);
                    mEVSTextView.setTag(abnormalReminder);
                } else if (carWarnLevelB.getEvsStatus() == CarOptions.NORMAL) {
                    mIsEVSAbnormal = false;
                    mEVSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mEVSTextView.setVisibility(View.VISIBLE);
                    mEVSTextView.setTag(null);
                } else {
                    mIsEVSAbnormal = false;
                    mEVSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mEVSTextView.setVisibility(View.GONE);
                    mEVSTextView.setTag(null);
                }

                if (mIsEVSAbnormal) {
                    mAbnormalTextViewArrayList.add(mEVSTextView);
                } else {
                    mNormalTextViewArrayList.add(mEVSTextView);
                }

                if (carWarnLevelB.getTcsStatus() == CarOptions.ABNORMAL_1) {
                    mIsTCSAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_tcs_abnormal_reminder);
                    mTCSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mWarningDrawable, null);
                    mTCSTextView.setVisibility(View.VISIBLE);
                    mTCSTextView.setTag(abnormalReminder);
                } else if (carWarnLevelB.getTcsStatus() == CarOptions.NORMAL) {
                    mIsTCSAbnormal = false;
                    mTCSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mTCSTextView.setVisibility(View.VISIBLE);
                    mTCSTextView.setTag(null);
                } else {
                    mIsTCSAbnormal = false;
                    mTCSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mTCSTextView.setVisibility(View.GONE);
                    mTCSTextView.setTag(null);
                }

                if (mIsTCSAbnormal) {
                    mAbnormalTextViewArrayList.add(mTCSTextView);
                } else {
                    mNormalTextViewArrayList.add(mTCSTextView);
                }

                if (carWarnLevelB.getEscStatus() == CarOptions.ABNORMAL_1) {
                    mIsESCAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_esc_abnormal_reminder);
                    mESCTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mWarningDrawable, null);
                    mESCTextView.setVisibility(View.VISIBLE);
                    mESCTextView.setTag(abnormalReminder);
                } else if (carWarnLevelB.getEscStatus() == CarOptions.NORMAL) {
                    mIsESCAbnormal = false;
                    mESCTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mESCTextView.setVisibility(View.VISIBLE);
                    mESCTextView.setTag(null);
                } else {
                    mIsESCAbnormal = false;
                    mESCTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mESCTextView.setVisibility(View.GONE);
                    mESCTextView.setTag(null);
                }

                if (mIsESCAbnormal) {
                    mAbnormalTextViewArrayList.add(mESCTextView);
                } else {
                    mNormalTextViewArrayList.add(mESCTextView);
                }

                if (carWarnLevelB.getObuWifiStatus() == CarOptions.ABNORMAL_1) {
                    mIsVigObuWifiAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_vig_obu_wifi_abnormal_reminder);
                    mVigObuWifiTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mWarningDrawable, null);
                    mVigObuWifiTextView.setVisibility(View.VISIBLE);
                    mVigObuWifiTextView.setTag(abnormalReminder);
                } else if (carWarnLevelB.getObuWifiStatus() == CarOptions.NORMAL) {
                    mIsVigObuWifiAbnormal = false;
                    mVigObuWifiTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mVigObuWifiTextView.setVisibility(View.VISIBLE);
                    mVigObuWifiTextView.setTag(null);
                } else {
                    mIsVigObuWifiAbnormal = false;
                    mVigObuWifiTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mVigObuWifiTextView.setVisibility(View.GONE);
                    mVigObuWifiTextView.setTag(null);
                }

                if (mIsVigObuWifiAbnormal) {
                    mAbnormalTextViewArrayList.add(mVigObuWifiTextView);
                } else {
                    mNormalTextViewArrayList.add(mVigObuWifiTextView);
                }

                if (carWarnLevelB.getAppBleStatus() == CarOptions.ABNORMAL_1) {
                    mIsVigAppBleAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_vig_app_ble_abnormal_reminder);
                    mVigAppBleTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mWarningDrawable, null);
                    mVigAppBleTextView.setVisibility(View.VISIBLE);
                    mVigAppBleTextView.setTag(abnormalReminder);
                } else if (carWarnLevelB.getAppBleStatus() == CarOptions.NORMAL) {
                    mIsVigAppBleAbnormal = false;
                    mVigAppBleTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mVigAppBleTextView.setVisibility(View.VISIBLE);
                    mVigAppBleTextView.setTag(null);
                } else {
                    mIsVigAppBleAbnormal = false;
                    mVigAppBleTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mVigAppBleTextView.setVisibility(View.GONE);
                    mVigAppBleTextView.setTag(null);
                }

                if (mIsVigAppBleAbnormal) {
                    mAbnormalTextViewArrayList.add(mVigAppBleTextView);
                } else {
                    mNormalTextViewArrayList.add(mVigAppBleTextView);
                }

                if (carWarnLevelB.getApaStatus() == CarOptions.ABNORMAL_1) {
                    mIsAPAAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_apa_abnormal_reminder);
                    mAPATextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mWarningDrawable, null);
                    mAPATextView.setVisibility(View.VISIBLE);
                    mAPATextView.setTag(abnormalReminder);
                } else if (carWarnLevelB.getApaStatus() == CarOptions.NORMAL) {
                    mIsAPAAbnormal = false;
                    mAPATextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mAPATextView.setVisibility(View.VISIBLE);
                    mAPATextView.setTag(null);
                } else {
                    mIsAPAAbnormal = false;
                    mAPATextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mAPATextView.setVisibility(View.GONE);
                    mAPATextView.setTag(null);
                }

                if (mIsAPAAbnormal) {
                    mAbnormalTextViewArrayList.add(mAPATextView);
                } else {
                    mNormalTextViewArrayList.add(mAPATextView);
                }

                if (carWarnLevelB.getSnrLfStatus() == CarOptions.ABNORMAL_1) {
                    mIsSNRLFAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_snr_lf_abnormal_reminder);
                    mSNRLFTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mWarningDrawable, null);
                    mSNRLFTextView.setVisibility(View.VISIBLE);
                    mSNRLFTextView.setTag(abnormalReminder);
                } else if (carWarnLevelB.getSnrLfStatus() == CarOptions.NORMAL) {
                    mIsSNRLFAbnormal = false;
                    mSNRLFTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mSNRLFTextView.setVisibility(View.VISIBLE);
                    mSNRLFTextView.setTag(null);
                } else {
                    mIsSNRLFAbnormal = false;
                    mSNRLFTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mSNRLFTextView.setVisibility(View.GONE);
                    mSNRLFTextView.setTag(null);
                }

                if (mIsSNRLFAbnormal) {
                    mAbnormalTextViewArrayList.add(mSNRLFTextView);
                } else {
                    mNormalTextViewArrayList.add(mSNRLFTextView);
                }

                if (carWarnLevelB.getSnrRfStatus() == CarOptions.ABNORMAL_1) {
                    mIsSNRRFAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_snr_rf_abnormal_reminder);
                    mSNRRFTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mWarningDrawable, null);
                    mSNRRFTextView.setVisibility(View.VISIBLE);
                    mSNRRFTextView.setTag(abnormalReminder);
                } else if (carWarnLevelB.getSnrRfStatus() == CarOptions.NORMAL) {
                    mIsSNRRFAbnormal = false;
                    mSNRRFTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mSNRRFTextView.setVisibility(View.VISIBLE);
                    mSNRRFTextView.setTag(null);
                } else {
                    mIsSNRRFAbnormal = false;
                    mSNRRFTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mSNRRFTextView.setVisibility(View.GONE);
                    mSNRRFTextView.setTag(null);
                }

                if (mIsSNRRFAbnormal) {
                    mAbnormalTextViewArrayList.add(mSNRRFTextView);
                } else {
                    mNormalTextViewArrayList.add(mSNRRFTextView);
                }

                if (carWarnLevelB.getSnrLrStatus() == CarOptions.ABNORMAL_1) {
                    mIsSNRLRAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_snr_lr_abnormal_reminder);
                    mSNRLRTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mWarningDrawable, null);
                    mSNRLRTextView.setVisibility(View.VISIBLE);
                    mSNRLRTextView.setTag(abnormalReminder);
                } else if (carWarnLevelB.getSnrLrStatus() == CarOptions.NORMAL) {
                    mIsSNRLRAbnormal = false;
                    mSNRLRTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mSNRLRTextView.setVisibility(View.VISIBLE);
                    mSNRLRTextView.setTag(null);
                } else {
                    mIsSNRLRAbnormal = false;
                    mSNRLRTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mSNRLRTextView.setVisibility(View.GONE);
                    mSNRLRTextView.setTag(null);
                }

                if (mIsSNRLRAbnormal) {
                    mAbnormalTextViewArrayList.add(mSNRLRTextView);
                } else {
                    mNormalTextViewArrayList.add(mSNRLRTextView);
                }

                if (carWarnLevelB.getSnrRrStatus() == CarOptions.ABNORMAL_1) {
                    mIsSNRRRAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_snr_rr_abnormal_reminder);
                    mSNRRRTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mWarningDrawable, null);
                    mSNRRRTextView.setVisibility(View.VISIBLE);
                    mSNRRRTextView.setTag(abnormalReminder);
                } else if (carWarnLevelB.getSnrRrStatus() == CarOptions.NORMAL) {
                    mIsSNRRRAbnormal = false;
                    mSNRRRTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mSNRRRTextView.setVisibility(View.VISIBLE);
                    mSNRRRTextView.setTag(null);
                } else {
                    mIsSNRRRAbnormal = false;
                    mSNRRRTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mSNRRRTextView.setVisibility(View.GONE);
                    mSNRRRTextView.setTag(null);
                }

                if (mIsSNRRRAbnormal) {
                    mAbnormalTextViewArrayList.add(mSNRRRTextView);
                } else {
                    mNormalTextViewArrayList.add(mSNRRRTextView);
                }

                if (carWarnLevelB.getSnrLsStatus() == CarOptions.ABNORMAL_1) {
                    mIsSNRLSAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_snr_ls_abnormal_reminder);
                    mSNRLSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mWarningDrawable, null);
                    mSNRLSTextView.setVisibility(View.VISIBLE);
                    mSNRLSTextView.setTag(abnormalReminder);
                } else if (carWarnLevelB.getSnrLsStatus() == CarOptions.NORMAL) {
                    mIsSNRLSAbnormal = false;
                    mSNRLSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mSNRLSTextView.setVisibility(View.VISIBLE);
                    mSNRLSTextView.setTag(null);
                } else {
                    mIsSNRLSAbnormal = false;
                    mSNRLSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mSNRLSTextView.setVisibility(View.GONE);
                    mSNRLSTextView.setTag(null);
                }

                if (mIsSNRLSAbnormal) {
                    mAbnormalTextViewArrayList.add(mSNRLSTextView);
                } else {
                    mNormalTextViewArrayList.add(mSNRLSTextView);
                }

                if (carWarnLevelB.getSnrRsStatus() == CarOptions.ABNORMAL_1) {
                    mIsSNRRSAbnormal = true;
                    String abnormalReminder = getString(R.string.car_status_snr_rs_abnormal_reminder);
                    mSNRRSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mWarningDrawable, null);
                    mSNRRSTextView.setVisibility(View.VISIBLE);
                    mSNRRSTextView.setTag(abnormalReminder);
                } else if (carWarnLevelB.getSnrRsStatus() == CarOptions.NORMAL) {
                    mIsSNRRSAbnormal = false;
                    mSNRRSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mGoodDrawable, null);
                    mSNRRSTextView.setVisibility(View.VISIBLE);
                    mSNRRSTextView.setTag(null);
                } else {
                    mIsSNRRSAbnormal = false;
                    mSNRRSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mSNRRSTextView.setVisibility(View.GONE);
                    mSNRRSTextView.setTag(null);
                }

                if (mIsSNRRSAbnormal) {
                    mAbnormalTextViewArrayList.add(mSNRRSTextView);
                } else {
                    mNormalTextViewArrayList.add(mSNRRSTextView);
                }

                for (int index = 0; index < mUrgentAbnormalTextViewArrayList.size(); index++) {
                    mListLinearLayout.addView(mUrgentAbnormalTextViewArrayList.get(index));
                }
                for (int index = 0; index < mAbnormalTextViewArrayList.size(); index++) {
                    mListLinearLayout.addView(mAbnormalTextViewArrayList.get(index));
                }
                for (int index = 0; index < mNormalTextViewArrayList.size(); index++) {
                    mListLinearLayout.addView(mNormalTextViewArrayList.get(index));
                }

                mUrgentAbnormalTextViewArrayList.clear();
                mAbnormalTextViewArrayList.clear();
                mNormalTextViewArrayList.clear();

                // mEmergencyExceptionTextView.setVisibility(showEmergencyExceptionView ? View.VISIBLE : View.GONE);
            } else {
                mVoltageTextView.setText(R.string.car_status_no_data_upload);
                mVoltageTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);

                mIsFuelAbnormal = false;
                mFuelTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mFuelTextView.setVisibility(View.VISIBLE);
                mFuelTextView.setTag(null);

                mListLinearLayout.addView(mFuelTextView);

                mIsOilPressureAbnormal = false;
                mOilPressureTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mOilPressureTextView.setVisibility(View.VISIBLE);
                mOilPressureTextView.setTag(null);

                mListLinearLayout.addView(mOilPressureTextView);

                mIsBrakeAbnormal = false;
                mBrakeTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mBrakeTextView.setVisibility(View.VISIBLE);
                mBrakeTextView.setTag(null);

                mListLinearLayout.addView(mBrakeTextView);

                mIsEngineAndGearboxAbnormal = false;
                mEngineAndGearboxTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mEngineAndGearboxTextView.setVisibility(View.VISIBLE);
                mEngineAndGearboxTextView.setTag(null);

                mListLinearLayout.addView(mEngineAndGearboxTextView);

                mIsABSAbnormal = false;
                mABSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mABSTextView.setVisibility(View.VISIBLE);
                mABSTextView.setTag(null);

                mListLinearLayout.addView(mABSTextView);

                mIsAirBagAbnormal = false;
                mAirBagTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mAirBagTextView.setVisibility(View.VISIBLE);
                mAirBagTextView.setTag(null);

                mListLinearLayout.addView(mAirBagTextView);

                mIsEPBAbnormal = false;
                mEPBTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mEPBTextView.setVisibility(View.VISIBLE);
                mEPBTextView.setTag(null);

                mListLinearLayout.addView(mEPBTextView);

                mIsWheelPressureAbnormal = false;
                mIsWheelPressureAbnormalUrgent = false;
                mWheelPressureTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mWheelPressureTextView.setVisibility(View.VISIBLE);
                mWheelPressureTextView.setTag(null);

                mListLinearLayout.addView(mWheelPressureTextView);

                mIsEngineWaterTemperatureAbnormal = false;
                mEngineWaterTemperatureTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mEngineWaterTemperatureTextView.setVisibility(View.VISIBLE);
                mEngineWaterTemperatureTextView.setTag(null);

                mListLinearLayout.addView(mEngineWaterTemperatureTextView);

                mIsSOCLowPower = false;
                mIsSOCVeryLowPower = false;
                mSOCTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mSOCTextView.setVisibility(View.VISIBLE);
                mSOCTextView.setTag(null);

                mListLinearLayout.addView(mSOCTextView);

                mIsEPASAbnormal = false;
                mEPASTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mEPASTextView.setVisibility(View.VISIBLE);
                mEPASTextView.setTag(null);

                mListLinearLayout.addView(mEPASTextView);

                mIsESCLAbnormalUrgent = false;
                mIsESCLAbnormalMiddle = false;
                mIsESCLAbnormal = false;
                mESCLTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mESCLTextView.setVisibility(View.VISIBLE);
                mESCLTextView.setTag(null);

                mListLinearLayout.addView(mESCLTextView);

                mIsLDWSAbnormal = false;
                mLDWSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mLDWSTextView.setVisibility(View.VISIBLE);
                mLDWSTextView.setTag(null);

                mListLinearLayout.addView(mLDWSTextView);

                mIsWheelPressureSensorAndBatteryAbnormal = false;
                mWheelPressureSensorAndBatteryTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mWheelPressureSensorAndBatteryTextView.setVisibility(View.VISIBLE);
                mWheelPressureSensorAndBatteryTextView.setTag(null);

                mListLinearLayout.addView(mWheelPressureSensorAndBatteryTextView);

                mIsEVSAbnormal = false;
                mEVSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mEVSTextView.setVisibility(View.VISIBLE);
                mEVSTextView.setTag(null);

                mListLinearLayout.addView(mEVSTextView);

                mIsTCSAbnormal = false;
                mTCSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mTCSTextView.setVisibility(View.VISIBLE);
                mTCSTextView.setTag(null);

                mListLinearLayout.addView(mTCSTextView);

                mIsESCAbnormal = false;
                mESCTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mESCTextView.setVisibility(View.VISIBLE);
                mESCTextView.setTag(null);

                mListLinearLayout.addView(mESCTextView);

                mIsVigObuWifiAbnormal = false;
                mVigObuWifiTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mVigObuWifiTextView.setVisibility(View.VISIBLE);
                mVigObuWifiTextView.setTag(null);

                mListLinearLayout.addView(mVigObuWifiTextView);

                mIsVigAppBleAbnormal = false;
                mVigAppBleTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mVigAppBleTextView.setVisibility(View.VISIBLE);
                mVigAppBleTextView.setTag(null);

                mListLinearLayout.addView(mVigAppBleTextView);

                mIsAPAAbnormal = false;
                mAPATextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mAPATextView.setVisibility(View.VISIBLE);
                mAPATextView.setTag(null);

                mListLinearLayout.addView(mAPATextView);

                mIsSNRLFAbnormal = false;
                mSNRLFTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mSNRLFTextView.setVisibility(View.VISIBLE);
                mSNRLFTextView.setTag(null);

                mListLinearLayout.addView(mSNRLFTextView);

                mIsSNRRFAbnormal = false;
                mSNRRFTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mSNRRFTextView.setVisibility(View.VISIBLE);
                mSNRRFTextView.setTag(null);

                mListLinearLayout.addView(mSNRRFTextView);

                mIsSNRLRAbnormal = false;
                mSNRLRTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mSNRLRTextView.setVisibility(View.VISIBLE);
                mSNRLRTextView.setTag(null);

                mListLinearLayout.addView(mSNRLRTextView);

                mIsSNRRRAbnormal = false;
                mSNRRRTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mSNRRRTextView.setVisibility(View.VISIBLE);
                mSNRRRTextView.setTag(null);

                mListLinearLayout.addView(mSNRRRTextView);

                mIsSNRLSAbnormal = false;
                mSNRLSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mSNRLSTextView.setVisibility(View.VISIBLE);
                mSNRLSTextView.setTag(null);

                mListLinearLayout.addView(mSNRLSTextView);

                mIsSNRRSAbnormal = false;
                mSNRRSTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                mSNRRSTextView.setVisibility(View.VISIBLE);
                mSNRRSTextView.setTag(null);

                mListLinearLayout.addView(mSNRRSTextView);
            }
        }
    }

}
