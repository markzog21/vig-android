package com.luxgen.remote.activity;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ResultReceiver;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.location.LocationListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.luxgen.remote.R;
import com.luxgen.remote.adapter.ActivityNewsPagerAdapter;
import com.luxgen.remote.adapter.MainCarKeyListAdapter;
import com.luxgen.remote.ca.CommunicationAgentService;
import com.luxgen.remote.custom.AutoVerticalRotationTextView;
import com.luxgen.remote.custom.CommonDefs;
import com.luxgen.remote.custom.CustomAppCompatActivity;
import com.luxgen.remote.custom.CustomRecyclerViewBaseAdapter;
import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.model.PushData;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.option.CarOptions;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.network.CarWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.UserWebApi;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.VigRequestData;
import com.luxgen.remote.network.model.assistant.CampaignData;
import com.luxgen.remote.network.model.assistant.GetCampaignResponseData;
import com.luxgen.remote.network.model.assistant.GetInitialDataResponseData;
import com.luxgen.remote.network.model.assistant.GetPm25ResponseData;
import com.luxgen.remote.network.model.assistant.GetPushRequestData;
import com.luxgen.remote.network.model.assistant.GetPushResponseData;
import com.luxgen.remote.network.model.assistant.GetWeatherRequestData;
import com.luxgen.remote.network.model.assistant.GetWeatherResponseData;
import com.luxgen.remote.network.model.assistant.Pm25Data;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.car.CarWarnLevelA;
import com.luxgen.remote.network.model.car.CarWarnLevelB;
import com.luxgen.remote.network.model.car.GetCarKeyRequestData;
import com.luxgen.remote.network.model.car.GetCarKeyResponseData;
import com.luxgen.remote.network.model.car.GetCarStatusResponseData;
import com.luxgen.remote.network.model.car.GetKeyChainListRequestData;
import com.luxgen.remote.network.model.car.GetKeyChainListResponseData;
import com.luxgen.remote.network.model.car.GetSpeedLimitStatusRequestData;
import com.luxgen.remote.network.model.car.GetSpeedLimitStatusResponseData;
import com.luxgen.remote.network.model.car.SpeedLimitStatusData;
import com.luxgen.remote.network.model.maintenance.GetCarRepairPlanRequestData;
import com.luxgen.remote.network.model.maintenance.GetCarRepairPlanResponseData;
import com.luxgen.remote.network.model.maintenance.GetCarRepairProgressRequestData;
import com.luxgen.remote.network.model.maintenance.GetCarRepairProgressResponseData;
import com.luxgen.remote.network.model.user.EmployeeData;
import com.luxgen.remote.network.model.user.GetIkeyEmployeeDataRequestData;
import com.luxgen.remote.network.model.user.GetIkeyEmployeeDataResponseData;
import com.luxgen.remote.network.model.user.GetIkeyUidRequestData;
import com.luxgen.remote.network.model.user.GetIkeyUidResponseData;
import com.luxgen.remote.network.model.user.LogoutRequestData;
import com.luxgen.remote.ui.arremote.RemoteControlActivity;
import com.luxgen.remote.ui.bonus.BonusActivity;
import com.luxgen.remote.ui.dashboard.DashboardActivity;
import com.luxgen.remote.ui.maintenance.MaintenanceQueryCarNoActivity;
import com.luxgen.remote.ui.oks.CarStolenTrackingActivity;
import com.luxgen.remote.ui.oks.OneKeyServiceActivity;
import com.luxgen.remote.ui.push.PushListActivity;
import com.luxgen.remote.ui.settings.SettingsActivity;
import com.luxgen.remote.ui.viewmodel.MainViewModel;
import com.luxgen.remote.ui.viewmodel.ViewModelFactory;
import com.luxgen.remote.util.GeocodeCountryIntentService;
import com.luxgen.remote.util.GpsUtils;
import com.luxgen.remote.util.LocationRequestModule;
import com.luxgen.remote.util.NetUtils;
import com.luxgen.remote.util.ObjectSerializer;
import com.luxgen.remote.util.PlayStoreChecker;
import com.luxgen.remote.util.Prefs;
import com.luxgen.remote.util.SyncContactIntentService;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedDeque;

import tw.com.haitec.ae.project.smartphoneikey.snappydb.IKeyDataBean;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.MasterKeyDataBean;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.SnapKeyDataBean;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.SubKeyDataBean;

import static com.luxgen.remote.ca.CommunicationAgentService.CA_EXTRA_LOGIN_SUCCESS_RETURN_CODE;
import static com.luxgen.remote.ca.CommunicationAgentService.CA_EXTRA_MAX_LOGIN_RETRY_RETURN_CODE;
import static com.luxgen.remote.ca.CommunicationAgentService.CA_EXTRA_STOP;
import static com.luxgen.remote.ca.CommunicationAgentService.CA_STATUS_EXTRA_COMMAND;
import static com.luxgen.remote.ca.CommunicationAgentService.VIG_SCAN_VIG;

public class MainActivity extends CustomAppCompatActivity {

    public final static String INTENT_EXTRA_NAME_SHOW_CAR_KEY_LIST = "show_car_key_list";
    public final static String INTENT_EXTRA_NAME_FORCE_EXIT_APP = "force_exit_app";
    public final static String INTENT_EXTRA_NAME_RESELECT_CAR_KEY_DATA = "reselect_car_key_data";
    public final static String INTENT_EXTRA_NAME_REFRESH_CAR_KEY_LIST = "refresh_car_key_data";

    public final int USER_ACCOUNT_TYPE_NORMAL = 0;
    public final int USER_ACCOUNT_TYPE_UIO = 1;
    public final int USER_ACCOUNT_TYPE_VIG = 2;

    private final int FUNC_ID_SHOW_PROGRESS_DIALOG = 0;
    private final int FUNC_ID_DISMISS_PROGRESS_DIALOG = 1;
    private final int FUNC_ID_SEND_BACKGROUND_REFRESH_MESSAGE = 2;
    private final int FUNC_ID_REMOVE_BACKGROUND_REFRESH_MESSAGE = 3;
    private final int FUNC_ID_CHECK_APP_UPDATE = 4;
    private final int FUNC_ID_GET_USER_INFO = 5;
    private final int FUNC_ID_CALL_API_TO_GET_IKEY_UID = 6;
    private final int FUNC_ID_CALL_API_TO_GET_INITIAL_DATA = 7;
    private final int FUNC_ID_CALL_API_TO_GET_PUSH_LIST = 8;
    private final int FUNC_ID_CALL_API_TO_GET_ACTIVITY_NEWS = 9;
    private final int FUNC_ID_CALL_API_TO_GET_CAR_KEY_LIST = 10;
    private final int FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA = 11;
    private final int FUNC_ID_CALL_API_TO_DO_IKEY_LOGIN = 12;
    private final int FUNC_ID_CALL_API_TO_GET_KEY_CHAIN_LIST = 13;
    private final int FUNC_ID_CALL_API_TO_GET_EMPLOYEE_DATA = 14;
    private final int FUNC_ID_CALL_API_TO_GET_CAR_STATUS = 15;
    private final int FUNC_ID_CALL_API_TO_GET_PM25 = 16;
    private final int FUNC_ID_CALL_API_TO_GET_CAR_REPAIR_PROGRESS = 17;
    private final int FUNC_ID_CALL_API_TO_GET_CAR_REPAIR_PLAN = 18;
    private final int FUNC_ID_CALL_API_TO_GET_SPEED_LIMIT_STATUS = 19;
    private final int FUNC_ID_SET_INIT_DONE = 20;
    private final int FUNC_ID_GET_GOOGLE_CALENDAR_EVENT_INDEPENDENTLY = 21;
    private final int FUNC_ID_CALL_API_TO_GET_WEATHER_INDEPENDENTLY = 22;
    private final int FUNC_ID_CALL_API_TO_GET_PUSH_LIST_INDEPENDENTLY = 23;
    private final int FUNC_ID_CALL_API_TO_LOGOUT = 24;
    private final int FUNC_ID_MULTILOGIN_RELOGIN = 25;

    private GetGoogleCalendarEventAsyncTask mGetGoogleCalendarEventAsyncTask = null;
    private GetUserInfoAsyncTask mGetUserInfoAsyncTask = null;
    private CheckAllKeysActivationStatusAndSelectCarKeyDataAsyncTask mCheckAllKeysActivationStatusAndSelectCarKeyDataAsyncTask = null;
    private AnalyzeEmployeeDataAsyncTask mAnalyzeEmployeeDataAsyncTask = null;
    private CheckCarStatusAsyncTask mCheckCarStatusAsyncTask = null;

    private ConcurrentLinkedDeque<Integer> mFunctionDeque = new ConcurrentLinkedDeque<>();
    private CountryResultReceiver mCountryResultReceiver = null;
    private LocationRequestModule mLocationRequestModule = null;
    private SimpleDateFormat mSimpleDateFormatForGoogleCalendarEvent = new SimpleDateFormat("a hh:mm", Locale.TAIWAN);
    private SimpleDateFormat mSimpleDateFormatForSpeedLimitStartDateTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.TAIWAN);
    private Uri mCalendarUri = null;
    private UserInfo mUserInfo = null;
    private CarKeyData mSelectedCarKeyData = null;
    private GetCarStatusResponseData mSelectedCarStatusData = null;
    private MainCarKeyListAdapter mMainCarKeyListAdapter = null;

    private LinearLayout mMainScreenLayout = null;
    private DrawerLayout mDrawerLayout = null;

    // Sliding Menu Objects
    private ViewGroup mSlidingmenuMenuScrollView = null;
    private RecyclerView mSlidingmenuCarKeyListRecyclerView = null;

    private TextView mSMenuUserNameTextView = null;
    private TextView mSMenuCarModelTextView = null;
    private TextView mSMenuCarCodeTextView = null;
    private TextView mSMenuPlateNumberTextView = null;
    private TextView mSMenuAddNewCarTextView = null;
    private TextView mSMenuStolenCarSpeedLimitTextView = null;
    private TextView mSMenuARRemoteControlTextView = null;
    private TextView mSMenuCarStatusReviewTextView = null;
    private TextView mSMenuReservationTextView = null;
    private TextView mSMenuServiceCenterTextView = null;
    private TextView mSMenuOneKeyWaterMarkTextView = null;
    private TextView mSMenuDashBoardSymbolsTextView = null;
    private TextView mSMenuCarBonusTextView = null;
    private TextView mSMenuETCCheckTextView = null;
    private TextView mSMenuSettingsTextView = null;
    private TextView mSMenuLogoutTextView = null;
    private TextView mSMenuDigitalKeyTextView = null;

    // ActionBar Objects
    private ImageView mActionBarNotificationCenterImageView = null;

    //Dynamic Brick Objects
    private ViewGroup mARRemoteControlBlock = null;
    private ViewGroup mCarStatusBlock = null;
    private TextView mStolenCarSpeedLimitTextView = null;
    private ImageView mWeatherImageView = null;
    private TextView mTemperatureTextView = null;
    private TextView mEventTextView = null;
    private TextView mEventTimeTextView = null;
    private ViewPager mActivityNewsViewPager = null;
    private ViewGroup mActivityNewsViewPagerIndicatorLayout = null;
    private ViewGroup mOneKeyFreshBrick = null;
    private TextView mPM2Point5ValueTextView = null;
    private ViewGroup mARRemoteControlBrick = null;
    private View mARRemoteControlBrickLightBarView = null;
    private AutoVerticalRotationTextView mARRemoteControlStatusTextView = null;
    private ViewGroup mCarStatusBrick = null;
    private View mCarStatusLightBarView = null;
    private TextView mCarStatusTextView = null;
    private TextView mLastUpdateTimeTextView = null;
    private ViewGroup mAvailableTripBrick = null;
    private TextView mAvailableTripStatusTextView = null;
    private ViewGroup mMaintenanceBrick = null;
    private TextView mMaintenanceStatusTextView = null;
    private ViewGroup mCanMessageBrick = null;
    private TextView mAdvisorTextView = null;
    private AutoVerticalRotationTextView mCanMessageTextView = null;

    private View mTheLastTouchedView = null;

    private Drawable mArrowDownDrawable = null;
    private Drawable mArrowUpDrawable = null;
    private Drawable mNotificationCenterDrawable = null;
    private Drawable mNotificationCenterAlertDrawable = null;

    private ArrayList<CarKeyData> mS1CarKeyList = new ArrayList<>();
    private ArrayList<CarKeyData> mT1CarKeyList = new ArrayList<>();
    private ArrayList<String> mArRemoteControlStatusTextList = new ArrayList<>();

    private int mCurrentUserAccountType = USER_ACCOUNT_TYPE_NORMAL;

    private boolean mIsInitDone = false;
    private boolean mIsLogoutInProgress = false;
    private boolean mIsGpsStatusReceiverRegistered = false;
    private boolean mIsCarKeyListOpened = false;
    private boolean mHaveGottonCarRepairProgress = false;
    private boolean mHasUnreadPushMessageNotificationBeenShownInSpecialistIntroduction = false;
    private boolean mIsCarKeyListSwitchEnabled = false;
    private boolean mNeedToShowCarKeyList = false;
    private boolean mIsActivationStatusChanged = false;
    private boolean mNeedToCallAPIToDoIkeyLogin = false;
    private boolean mNeedToCallAPIToGetKeyChainList = false;
    private boolean mStopGlide = false;
    private boolean mNeedToSyncPushDataToServer = false;

    private int mNumOfCarKeyData = 0;
    private int mCurrentViewPagerPosition = 0;

    private String mUserName = null;
    private String mEmployeeName = "";
    private String[] mCannedMessages = null;
    private String mSpaceString = null;
    private String mCountry = null;
    private String mVigCarIdOfAbnormalCarKeyData = null;

    private MainViewModel mViewModel = null;

    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            translateCountry(location);
        }
    };

    private class CarAndDoorStatus {
        private int mCarStatus = CarOptions.NORMAL;
        private int mDoorStatus = CommonDefs.DOOR_STATUS_NONE;

        public void setCarStatus(int carStatus) {
            mCarStatus = carStatus;
        }

        public int getCarStatus() {
            return mCarStatus;
        }

        public void setDoorStatus(int doorStatus) {
            mDoorStatus = doorStatus;
        }

        public int getDoorStatus() {
            return mDoorStatus;
        }
    }

    private class CountryResultReceiver extends ResultReceiver {
        public CountryResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (resultData == null) {
                return;
            }

            mCountry = resultData.getString(GeocodeCountryIntentService.RESULT_DATA_KEY);
            if (!TextUtils.isEmpty(mCountry)) {
                mCountry = mCountry.replace("台", "臺");
                executeFunction(FUNC_ID_CALL_API_TO_GET_WEATHER_INDEPENDENTLY);
            }
        }
    }

    private BroadcastReceiver mNetworkStatusBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (NetUtils.isConnectToInternet(MainActivity.this)) {
                if (mNeedToSyncPushDataToServer) {
                    mNeedToSyncPushDataToServer = false;
                    mViewModel.syncPushDataToServer();
                }
            }
        }
    };

    private final BroadcastReceiver mGpsStatusBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (GpsUtils.isGpsOpened(MainActivity.this)) {
                unregisterReceiver(mGpsStatusBroadcastReceiver);
                mIsGpsStatusReceiverRegistered = false;
                if (mLocationRequestModule != null) {
                    mLocationRequestModule.startLocationUpdates();
                }
            }
        }
    };

    private class GetGoogleCalendarEventAsyncTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_CALENDAR},
                        CommonDefs.REQUEST_CODE_REQUEST_PERMISSIONS);
                return null;
            }

            Calendar calendar = Calendar.getInstance();
            String startTime = String.valueOf(calendar.getTimeInMillis());
            calendar.set(calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH),
                    23,
                    59,
                    59);
            String endTime = String.valueOf(calendar.getTimeInMillis());
            String selection = "(" + CalendarContract.Events.DTSTART + ">= ?) AND ("
                    + CalendarContract.Events.DTSTART + "<=?) AND ("
                    + CalendarContract.Events.DELETED + "!=?)";
            String[] selectionArgs = new String[]{startTime, endTime, "1"};
            String sortOrder = CalendarContract.Events.DTSTART + " ASC";

            Cursor cursor = getContentResolver().query(CalendarContract.Events.CONTENT_URI, null,
                    selection, selectionArgs, sortOrder);
            return cursor;
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            mGetGoogleCalendarEventAsyncTask = null;

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();

                String title = cursor.getString(cursor.getColumnIndex(CalendarContract.Events.TITLE));

                Calendar calendar = Calendar.getInstance();
                long dtStart = cursor.getLong(cursor.getColumnIndex(CalendarContract.Events.DTSTART));
                long dtEnd = cursor.getLong(cursor.getColumnIndex(CalendarContract.Events.DTEND));
                calendar.setTimeInMillis(dtStart);
                mSimpleDateFormatForGoogleCalendarEvent.setTimeZone(calendar.getTimeZone());
                String dtStartString = mSimpleDateFormatForGoogleCalendarEvent.format(calendar.getTime());
                calendar.setTimeInMillis(dtEnd);
                mSimpleDateFormatForGoogleCalendarEvent.setTimeZone(calendar.getTimeZone());
                String dtEndString = mSimpleDateFormatForGoogleCalendarEvent.format(calendar.getTime());

                String eventTimeString = getString(R.string.main_calendar_event_time);
                eventTimeString = String.format(eventTimeString, dtStartString, dtEndString);

                mCalendarUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, cursor.getLong(cursor.getColumnIndex(CalendarContract.Events._ID)));

                if (title != null) {
                    mEventTextView.setText(title);
                    mEventTextView.setOnClickListener(mOnEventAndEventTimeTextViewClickListener);
                }
                mEventTimeTextView.setText(eventTimeString);
                mEventTimeTextView.setOnClickListener(mOnEventAndEventTimeTextViewClickListener);


            } else {
                mCalendarUri = null;
                mEventTextView.setText(R.string.main_calendar_event_no_event);
                mEventTextView.setOnClickListener(null);
                mEventTimeTextView.setText(R.string.common_space);
                mEventTimeTextView.setOnClickListener(null);
            }
        }
    }

    private class GetUserInfoAsyncTask extends AsyncTask<Void, Void, UserInfo> {

        @Override
        protected UserInfo doInBackground(Void... voids) {
            return Prefs.getUserInfo(MainActivity.this);
        }

        @Override
        protected void onPostExecute(UserInfo userInfo) {
            mGetUserInfoAsyncTask = null;

            mUserInfo = userInfo;
            updateDisplayRelatedToUserInfo();

            pollAndRunFunctionInDeque();
        }
    }

    private class CheckAllKeysActivationStatusAndSelectCarKeyDataAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            if (mUserInfo.isDMS()) {
                mCurrentUserAccountType = USER_ACCOUNT_TYPE_UIO;
            } else {
                mCurrentUserAccountType = USER_ACCOUNT_TYPE_NORMAL;
            }

            mS1CarKeyList.clear();
            mT1CarKeyList.clear();
            mVigCarIdOfAbnormalCarKeyData = null;
            mIsActivationStatusChanged = false;
            mNeedToCallAPIToDoIkeyLogin = false;
            mNeedToCallAPIToGetKeyChainList = false;
            for (int position = 0; position < mMainCarKeyListAdapter.getItemCount(); position++) {
                CustomRecyclerViewBaseAdapter.RecyclerViewItemData itemData = mMainCarKeyListAdapter.getItem(position);
                if (itemData.getItemType() == MainCarKeyListAdapter.MAIN_CAR_KEY_LIST_ITEM_TYPE_CAR_KEY) {
                    CarKeyData carKeyData = (CarKeyData) itemData.getDataObject();
                    MainCarKeyListAdapter.ExtraData extraData = (MainCarKeyListAdapter.ExtraData) itemData.getExtraDataObject();
                    extraData.setCarKeyActivationStatus(false);

                    if (carKeyData.isMasterKey()) {
                        ArrayList<MasterKeyDataBean> masterKeyDataList = mCommunicationAgentService.getCA().getMasterKeyDataList();

                        for (MasterKeyDataBean bean : masterKeyDataList) {
                            if (bean.getUserId().equals(mUserInfo.getKeyUid()) &&
                                    bean.getDeviceId().equals(mUserInfo.getDeviceId()) &&
                                    bean.getCarId().equals(carKeyData.getVigCarId()) &&
                                    bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                                extraData.setCarKeyActivationStatus(true);
                                break;
                            }
                        }

                    } else if (carKeyData.isSubKey()) {
                        ArrayList<SubKeyDataBean> subKeyDataList = mCommunicationAgentService.getCA().getSubKeyDataList();

                        if (!carKeyData.isKeyReady() && mUserInfo.getDeviceId().equals(carKeyData.getPhoneId())) {
                            mS1CarKeyList.add(carKeyData);
                            mNeedToCallAPIToDoIkeyLogin = true;
                        }

                        for (SubKeyDataBean bean : subKeyDataList) {
                            if (bean.getUserId().equals(mUserInfo.getKeyUid()) &&
                                    bean.getDeviceId().equals(mUserInfo.getDeviceId()) &&
                                    mUserInfo.getDeviceId().equals(carKeyData.getPhoneId()) &&
                                    bean.getCarId().equals(carKeyData.getVigCarId()) &&
                                    bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                                if (!carKeyData.isKeyReady()) {
                                    mNeedToCallAPIToDoIkeyLogin = false;
                                    mNeedToCallAPIToGetKeyChainList = true;
                                    mVigCarIdOfAbnormalCarKeyData = carKeyData.getVigCarId();
                                } else {
                                    extraData.setCarKeyActivationStatus(true);
                                }
                                break;
                            }
                        }

                        if (mSelectedCarKeyData != null && mSelectedCarKeyData.getCarId().equals(carKeyData.getCarId()) &&
                                mSelectedCarKeyData.getPhoneId().equals(carKeyData.getPhoneId())) {
                            if (!extraData.isCarKeyActive()) {
                                mIsActivationStatusChanged = true;
                            }
                        }
                    } else if (carKeyData.isSnapKey()) {
                        ArrayList<SnapKeyDataBean> snapKeyDataList = mCommunicationAgentService.getCA().getSnapKeyDataListByCarId(carKeyData.getVigCarId());

                        if (!carKeyData.isKeyReady() && mUserInfo.getDeviceId().equals(carKeyData.getPhoneId())) {
                            mT1CarKeyList.add(carKeyData);
                            mNeedToCallAPIToDoIkeyLogin = true;
                        }

                        for (SnapKeyDataBean bean : snapKeyDataList) {
                            if (bean.getUserId().equals(mUserInfo.getKeyUid()) &&
                                    bean.getDeviceId().equals(mUserInfo.getDeviceId()) &&
                                    mUserInfo.getDeviceId().equals(carKeyData.getPhoneId()) &&
                                    bean.getCarId().equals(carKeyData.getVigCarId()) &&
                                    bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                                if (!carKeyData.isKeyReady()) {
                                    mNeedToCallAPIToDoIkeyLogin = false;
                                    mNeedToCallAPIToGetKeyChainList = true;
                                    mVigCarIdOfAbnormalCarKeyData = carKeyData.getVigCarId();
                                } else {
                                    extraData.setCarKeyActivationStatus(true);
                                }
                                break;
                            }
                        }

                        if (mSelectedCarKeyData != null && mSelectedCarKeyData.getCarId().equals(carKeyData.getCarId()) &&
                                mSelectedCarKeyData.getPhoneId().equals(carKeyData.getPhoneId())) {
                            if (!extraData.isCarKeyActive()) {
                                mIsActivationStatusChanged = true;
                            }
                        }
                    }
                }
            }

            // 沒有上次選的車
            if (mSelectedCarKeyData == null) {
                for (int position = 0; position < mMainCarKeyListAdapter.getItemCount(); position++) {
                    CustomRecyclerViewBaseAdapter.RecyclerViewItemData itemData = mMainCarKeyListAdapter.getItem(position);
                    if (itemData.getItemType() == MainCarKeyListAdapter.MAIN_CAR_KEY_LIST_ITEM_TYPE_CAR_KEY) {
                        CarKeyData carKeyData = (CarKeyData) itemData.getDataObject();
                        MainCarKeyListAdapter.ExtraData extraData = (MainCarKeyListAdapter.ExtraData) itemData.getExtraDataObject();

                        if (carKeyData.isMasterKey() ||
                                (carKeyData.isSubKey() && extraData.isCarKeyActive()) ||
                                (carKeyData.isSnapKey() && extraData.isCarKeyActive())) {
                            mSelectedCarKeyData = carKeyData;
                            Prefs.setCarKeyData(MainActivity.this, mSelectedCarKeyData);
                            Prefs.setCarKeyDataWithIKeyUid(MainActivity.this, mUserInfo.getKeyUid(), mSelectedCarKeyData);
                            mMainCarKeyListAdapter.setSelectedItemPosition(position);
                            mCurrentUserAccountType = USER_ACCOUNT_TYPE_VIG;
                            break;

                        } else if (!carKeyData.isVig() && mUserInfo.isDMS()) {
                            mSelectedCarKeyData = carKeyData;
                            Prefs.setCarKeyData(MainActivity.this, mSelectedCarKeyData);
                            Prefs.setCarKeyDataWithIKeyUid(MainActivity.this, mUserInfo.getKeyUid(), mSelectedCarKeyData);
                            mMainCarKeyListAdapter.setSelectedItemPosition(position);
                            break;
                        }
                    }
                }
            } else if (mSelectedCarKeyData.isVig()) {
                mCurrentUserAccountType = USER_ACCOUNT_TYPE_VIG;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void voids) {
            mCheckAllKeysActivationStatusAndSelectCarKeyDataAsyncTask = null;

            mMainCarKeyListAdapter.notifyDataSetChanged();

            if (mCurrentUserAccountType == USER_ACCOUNT_TYPE_NORMAL || mCurrentUserAccountType == USER_ACCOUNT_TYPE_UIO) {
                mSelectedCarStatusData = null;
                updateDisplayRelatedToSelectedCarStatusData();
            }

            if ((mSelectedCarKeyData != null && mNumOfCarKeyData == 1 && mUserInfo.isDMS()) || (mNumOfCarKeyData < 1)) {
                mIsCarKeyListSwitchEnabled = false;
                mSMenuUserNameTextView.setCompoundDrawables(null, null, null, null);
            } else {
                mIsCarKeyListSwitchEnabled = true;
                if (mIsCarKeyListOpened) {
                    mSMenuUserNameTextView.setCompoundDrawables(null, null, mArrowUpDrawable, null);
                } else {
                    mSMenuUserNameTextView.setCompoundDrawables(null, null, mArrowDownDrawable, null);
                }
            }

            if (mNeedToShowCarKeyList) {
                mNeedToShowCarKeyList = false;
                if (mIsCarKeyListSwitchEnabled && !mIsCarKeyListOpened) {
                    switchCarKeyListDisplay();
                }
            }

            setUIDisplayByUserAccountType();
            updateDisplayRelatedToSelectedCarKeyData();

            if (mIsActivationStatusChanged) {
                clearFunctionQueue();
                if (mAlertDialog1.isShowing()) {
                    mAlertDialog1.dismiss();
                }
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.dialog_title_remind);
                mAlertDialog1.setMessage(getString(R.string.main_dialog_activation_status_changed_message));
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.btn_confirm),
                        new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                mSelectedCarKeyData = null;
                                Prefs.clearCarKeyData(MainActivity.this);
                                Prefs.clearCarKeyDataWithIKeyUid(MainActivity.this, mUserInfo.getKeyUid());
                                resetDisplayRelatedToSelectedCarKeyDataAndCarStatusData();
                                clearFunctionQueue();
                                if (mFunctionDeque.isEmpty()) {
                                    mFunctionDeque.offer(FUNC_ID_SHOW_PROGRESS_DIALOG);
                                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_KEY_LIST);
                                    mFunctionDeque.offer(FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA);
                                    // mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_EMPLOYEE_DATA);
                                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_STATUS);
                                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_PM25);
                                    mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
                                    pollAndRunFunctionInDeque();
                                } else {
                                    mFunctionDeque.offer(FUNC_ID_SHOW_PROGRESS_DIALOG);
                                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_KEY_LIST);
                                    mFunctionDeque.offer(FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA);
                                    // mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_EMPLOYEE_DATA);
                                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_STATUS);
                                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_PM25);
                                    mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
                                }
                                mAlertDialog1.dismiss();
                            }
                        });
                mAlertDialog1.show();

            } else {
                // Start to scan VIG here
                Intent intent = new Intent(MainActivity.this, CommunicationAgentService.class);
                intent.putExtra(VIG_SCAN_VIG, true);
                startService(intent);

                if (mNeedToCallAPIToGetKeyChainList) {
                    mFunctionDeque.offerFirst(FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA);
                    mFunctionDeque.offerFirst(FUNC_ID_CALL_API_TO_GET_CAR_KEY_LIST);
                    mFunctionDeque.offerFirst(FUNC_ID_CALL_API_TO_GET_KEY_CHAIN_LIST);
                } else if (mNeedToCallAPIToDoIkeyLogin) {
                    mFunctionDeque.offerFirst(FUNC_ID_CALL_API_TO_DO_IKEY_LOGIN);
                }

                pollAndRunFunctionInDeque();
            }
        }
    }

    private class AnalyzeEmployeeDataAsyncTask extends AsyncTask<GetIkeyEmployeeDataResponseData, Void, Void> {

        @Override
        protected Void doInBackground(GetIkeyEmployeeDataResponseData... params) {
            GetIkeyEmployeeDataResponseData responseData = params[0];

            Intent intent = new Intent(MainActivity.this, SyncContactIntentService.class);
            intent.putExtra(SyncContactIntentService.EXTRA_EMPLOYEE_DATA, responseData.getEmployeeDataList());
            startService(intent);

            ArrayList<EmployeeData> employeeDataArrayList = responseData.getEmployeeDataList();
            mEmployeeName = "";
            for (int index = 0; index < employeeDataArrayList.size(); index++) {
                EmployeeData data = employeeDataArrayList.get(index);
                if (mSelectedCarKeyData.getCarNo().equals(data.getCarNo()) && data.isAdvisor()) {
                    mEmployeeName = data.getName();
                    break;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void nothing) {
            mAnalyzeEmployeeDataAsyncTask = null;

            updateDisplayRelatedToSpecialist(false);

            pollAndRunFunctionInDeque();
        }
    }

    private class CheckCarStatusAsyncTask extends AsyncTask<Void, Void, CarAndDoorStatus> {

        @Override
        protected CarAndDoorStatus doInBackground(Void... voids) {
            CarAndDoorStatus carAndDoorStatus = new CarAndDoorStatus();

            CarWarnLevelA carWarnLevelA = mSelectedCarStatusData.getWarnLevelA();
            CarWarnLevelB carWarnLevelB = mSelectedCarStatusData.getWarnLevelB();

            if (carWarnLevelA != null && carWarnLevelB != null) {

                do {
                    // CarWarnLevelA
                    if (carWarnLevelA.getAbsStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getAlrtTempEauStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getAbmAirBagStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getBatChargeStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getBatLowStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getCanecuStateStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getCanecuSvlStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getEpasStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getEpbStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getEsclCriticalStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getEsclFunctionStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getExWheelPressureLfStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getExWheelPressureLrStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getExWheelPressureRfStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getExWheelPressureRrStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getLowBrakeFluidStaStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getLowFuelStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getLowSocStatus() == CarOptions.ABNORMAL_1 ||
                            carWarnLevelA.getLowSocStatus() == CarOptions.ABNORMAL_2) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getLvCannStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getOilPressureLedStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getRotateSteerWheelStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getSrsLedStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getAirBagFailCmd() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelA.getCantcosta() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }

                    // CarWarnLevelB
                    if (carWarnLevelB.getApaStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getAppBleStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getEscStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getEvsStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getLdwsStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getObuWifiStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getSnrLfStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getSnrLrStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getSnrRfStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getSnrRrStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getSnrRsStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getTcsStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getWhellPressureBatLfStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getWhellPressureBatLrStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getWhellPressureBatRfStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getWhellPressureBatRrStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getExWheelPressureLfStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getExWheelPressureLrStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getExWheelPressureRfStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getExWheelPressureRrStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getWhellPressureSensorLfStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getWhellPressureSensorLrStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getWhellPressureSensorRfStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getWhellPressureSensorRrStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getTpmsExist() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getTpmsWarnInd() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getWheelPressureLfStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getWheelPressureLrStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getWheelPressureRfStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }
                    if (carWarnLevelB.getWheelPressureRrStatus() == CarOptions.ABNORMAL_1) {
                        carAndDoorStatus.setCarStatus(CarOptions.ABNORMAL_1);
                        break;
                    }

                } while (false);
            } else {
                carAndDoorStatus.setCarStatus(CarOptions.NON);
            }

            boolean isGateOpened = false;
            if (mSelectedCarStatusData.getGateCloseStatus() == CarOptions.ABNORMAL_1) {
                isGateOpened = true;
            }

            if (isGateOpened) {
                if (mSelectedCarStatusData.isDoorLockStatus() == CarOptions.NORMAL) {
                    carAndDoorStatus.setDoorStatus(CommonDefs.DOOR_STATUS_LOCK_AND_TRUNK_OPEN);
                } else if (mSelectedCarStatusData.isDoorLockStatus() == CarOptions.ABNORMAL_1) {
                    carAndDoorStatus.setDoorStatus(CommonDefs.DOOR_STATUS_UNLOCK_AND_TRUNK_OPEN);
                }
            } else {
                if (mSelectedCarStatusData.isDoorLockStatus() == CarOptions.NORMAL) {
                    carAndDoorStatus.setDoorStatus(CommonDefs.DOOR_STATUS_LOCK);
                } else if (mSelectedCarStatusData.isDoorLockStatus() == CarOptions.ABNORMAL_1) {
                    carAndDoorStatus.setDoorStatus(CommonDefs.DOOR_STATUS_UNLOCK);
                }
            }

            return carAndDoorStatus;
        }

        @Override
        protected void onPostExecute(CarAndDoorStatus carAndDoorStatus) {
            mCheckCarStatusAsyncTask = null;

            String eventDateString = mSelectedCarStatusData.getEventDate();
            if (eventDateString != null) {
                eventDateString = eventDateString.substring(0, 16);
                Prefs.setCarStatusLastUpdateTime(MainActivity.this, eventDateString);
                String lastUpdateTimeString = getString(R.string.main_car_status_last_update_time);
                lastUpdateTimeString = String.format(lastUpdateTimeString, eventDateString);
                mLastUpdateTimeTextView.setText(lastUpdateTimeString);
            } else {
                Prefs.setCarStatusLastUpdateTime(MainActivity.this, "");
                mLastUpdateTimeTextView.setText(R.string.main_car_status_none);
            }

            int availableTrip = Double.valueOf(mSelectedCarStatusData.getAvailableTrip()).intValue();
            if (availableTrip > -1) {
                String availableTripString = getString(R.string.main_car_status_available_trip);
                availableTripString = String.format(availableTripString, availableTrip);
                mAvailableTripStatusTextView.setText(availableTripString);
            } else {
                mAvailableTripStatusTextView.setText(R.string.main_car_status_none);
            }

            switch (carAndDoorStatus.getCarStatus()) {
                case CarOptions.NORMAL: {
                    mCarStatusLightBarView.setBackgroundColor(getResources().getColor(R.color.mainActivityLightBarColorTransparent));
                    mCarStatusTextView.setText(R.string.main_car_status_normal);
                    break;
                }

                case CarOptions.ABNORMAL_1: {
                    mCarStatusLightBarView.setBackgroundColor(getResources().getColor(R.color.mainActivityLightBarColorRed));
                    mCarStatusTextView.setText(R.string.main_car_status_abnormal);
                    break;
                }

                default: {
                    mCarStatusLightBarView.setBackgroundColor(getResources().getColor(R.color.mainActivityLightBarColorTransparent));
                    mCarStatusTextView.setText(R.string.main_car_status_none);
                }
            }

            mArRemoteControlStatusTextList.clear();
            AutoVerticalRotationTextView.DisplayData displayData = new AutoVerticalRotationTextView.DisplayData();
            switch (carAndDoorStatus.getDoorStatus()) {
                case CommonDefs.DOOR_STATUS_LOCK: {
                    mARRemoteControlBrickLightBarView.setBackgroundColor(getResources().getColor(R.color.mainActivityLightBarColorTransparent));
                    mArRemoteControlStatusTextList.add(getString(R.string.main_ar_remote_control_status_door_lock));
                    displayData.mTextList.addAll(mArRemoteControlStatusTextList);
                    displayData.mDrawableList.add(getDrawable(R.drawable.ic_lock));
                    mARRemoteControlStatusTextView.setDisplayData(displayData);
                    break;
                }

                case CommonDefs.DOOR_STATUS_UNLOCK: {
                    mARRemoteControlBrickLightBarView.setBackgroundColor(getResources().getColor(R.color.mainActivityLightBarColorRed));
                    mArRemoteControlStatusTextList.add(getString(R.string.main_ar_remote_control_status_door_unlock));
                    displayData.mTextList.addAll(mArRemoteControlStatusTextList);
                    displayData.mDrawableList.add(getDrawable(R.drawable.ic_unlock));
                    mARRemoteControlStatusTextView.setDisplayData(displayData);
                    break;
                }

                case CommonDefs.DOOR_STATUS_LOCK_AND_TRUNK_OPEN: {
                    mARRemoteControlBrickLightBarView.setBackgroundColor(getResources().getColor(R.color.mainActivityLightBarColorRed));
                    mArRemoteControlStatusTextList.add(getString(R.string.main_ar_remote_control_status_door_lock_and_trunk_open));
                    displayData.mTextList.addAll(mArRemoteControlStatusTextList);
                    displayData.mDrawableList.add(getDrawable(R.drawable.ic_tailgate_open));
                    mARRemoteControlStatusTextView.setDisplayData(displayData);
                    break;
                }

                case CommonDefs.DOOR_STATUS_UNLOCK_AND_TRUNK_OPEN: {
                    mARRemoteControlBrickLightBarView.setBackgroundColor(getResources().getColor(R.color.mainActivityLightBarColorRed));
                    mArRemoteControlStatusTextList.add(getString(R.string.main_ar_remote_control_status_door_unlock));
                    mArRemoteControlStatusTextList.add(getString(R.string.main_ar_remote_control_status_door_unlock_and_trunk_open));
                    displayData.mTextList.addAll(mArRemoteControlStatusTextList);
                    displayData.mDrawableList.add(getDrawable(R.drawable.ic_unlock));
                    displayData.mDrawableList.add(getDrawable(R.drawable.ic_tailgate_open));
                    mARRemoteControlStatusTextView.setDisplayData(displayData);
                    break;
                }

                default: {
                    mARRemoteControlBrickLightBarView.setBackgroundColor(getResources().getColor(R.color.mainActivityLightBarColorTransparent));
                    mArRemoteControlStatusTextList.add(getString(R.string.main_ar_remote_control_status_door_unknown));
                    displayData.mTextList.addAll(mArRemoteControlStatusTextList);
                    displayData.mDrawableList.add(getDrawable(R.drawable.ic_lock));
                    mARRemoteControlStatusTextView.setDisplayData(displayData);
                }
            }

            mARRemoteControlStatusTextView.startAutoScroll();

            pollAndRunFunctionInDeque();
        }
    }

    private DrawerLayout.DrawerListener mDrawerListener = new DrawerLayout.DrawerListener() {

        @Override
        public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

        }

        @Override
        public void onDrawerOpened(@NonNull View drawerView) {
        }

        @Override
        public void onDrawerClosed(@NonNull View drawerView) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            if (mIsCarKeyListOpened) {
                switchCarKeyListDisplay();
            }
        }

        @Override
        public void onDrawerStateChanged(int newState) {

        }
    };

    private MainCarKeyListAdapter.OnInnerViewsClickListener mOnSlidingmenuCarKeyListRecyclerViewInnerViewsClickListener = new MainCarKeyListAdapter.OnInnerViewsClickListener() {

        @Override
        public void onCarKeyListItemViewClick(MainCarKeyListAdapter adapter, int position) {
            // 自選車
            mSelectedCarKeyData = (CarKeyData) adapter.getItem(position).getDataObject();
            Prefs.setCarKeyData(MainActivity.this, mSelectedCarKeyData);
            Prefs.setCarKeyDataWithIKeyUid(MainActivity.this, mUserInfo.getKeyUid(), mSelectedCarKeyData);
            mMainCarKeyListAdapter.setSelectedItemPosition(position);
            resetDisplayRelatedToSelectedCarKeyDataAndCarStatusData();
            clearFunctionQueue();
            if (mFunctionDeque.isEmpty()) {
                mFunctionDeque.offer(FUNC_ID_SHOW_PROGRESS_DIALOG);
                mFunctionDeque.offer(FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA);
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_EMPLOYEE_DATA);
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_STATUS);
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_PM25);
                mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
                pollAndRunFunctionInDeque();
            } else {
                mFunctionDeque.offer(FUNC_ID_SHOW_PROGRESS_DIALOG);
                mFunctionDeque.offer(FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA);
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_EMPLOYEE_DATA);
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_STATUS);
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_PM25);
                mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
            }
            switchCarKeyListDisplay();
        }

        @Override
        public void onCarKeyListAddNewCarTextViewClick() {
            startActivity(AddNewCarActivity.newIntent(MainActivity.this));
        }
    };

    private OnClickListener mOnActionBarNotificationCenterImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            if (NetUtils.isConnectToInternet(MainActivity.this)) {
                startActivity(PushListActivity.newIntent(MainActivity.this));
            } else {
                showServerErrorDialog();
            }
        }
    };

    private OnClickListener mOnSMenuUserNameTextViewClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mIsCarKeyListSwitchEnabled) {
                switchCarKeyListDisplay();
                if (mIsCarKeyListOpened) {
                    if (mFunctionDeque.isEmpty()) {
                        mFunctionDeque.offerFirst(FUNC_ID_SHOW_PROGRESS_DIALOG);
                        mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_KEY_LIST);
                        mFunctionDeque.offer(FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA);
                        mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
                        pollAndRunFunctionInDeque();
                    } else {
                        mFunctionDeque.offerFirst(FUNC_ID_SHOW_PROGRESS_DIALOG);
                        mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_KEY_LIST);
                        mFunctionDeque.offer(FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA);
                        mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
                    }
                }
            }
        }
    };

    private OnClickListener mOnSMenuPlateNumberTextViewClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            // TODO: Just for test, need to remove.
            // mCurrentUserAccountType = (mCurrentUserAccountType + 1) % 3;
            // setUIDisplayByUserAccountType();
        }
    };

    private OnClickListener mOnSMenuAddNewCarTextViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            if (NetUtils.isConnectToInternet(MainActivity.this)) {
                startActivity(AddNewCarActivity.newIntent(MainActivity.this));
            } else {
                showServerErrorDialog();
            }
        }
    };

    private OnClickListener mOnSMenuStolenCarSpeedLimitTextViewClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(CarStolenTrackingActivity.newIntent(MainActivity.this, 25.0805873, 121.5640737));
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    };

    private OnClickListener mOnSMenuARRemoteControlTextViewClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mSelectedCarKeyData.isMasterKey()) {
                if (isSelectedCarKeyDataActive()) {
                    startActivity(RemoteControlActivity.newIntent(MainActivity.this, RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_MENU));
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    showInactiveMasterKeyAlertDialog();
                }

            } else {
                startActivity(RemoteControlActivity.newIntent(MainActivity.this, RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_MENU));
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        }
    };

    private OnClickListener mOnSMenuCarStatusReviewTextViewClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (NetUtils.isConnectToInternet(MainActivity.this)) {
                startActivity(CarStatusActivity.newIntent(MainActivity.this));
            } else {
                showServerErrorDialog();
            }
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    };

    private OnClickListener mOnSMenuReservationTextViewClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (NetUtils.isConnectToInternet(MainActivity.this)) {
                startActivity(MaintenanceQueryCarNoActivity.newIntent(MainActivity.this, 0));
            } else {
                showServerErrorDialog();
            }
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    };

    private OnClickListener mOnSMenuServiceCenterTextViewClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(ServiceCenterActivity.newIntent(MainActivity.this));
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    };

    private OnClickListener mOnSMenuOneKeyWaterMarkTextViewClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(OneKeyServiceActivity.newIntent(MainActivity.this));
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    };

    private OnClickListener mOnSMenuDashBoardSymbolsTextViewClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(DashboardActivity.newIntent(MainActivity.this));
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    };

    private OnClickListener mOnSMenuCarBonusTextViewClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (NetUtils.isConnectToInternet(MainActivity.this)) {
                startActivity(BonusActivity.newIntent(MainActivity.this));
                overridePendingTransition(0, 0);
            } else {
                showServerErrorDialog();
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        }
    };

    private OnClickListener mOnSMenuETCCheckTextViewClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (NetUtils.isConnectToInternet(MainActivity.this)) {
                startActivity(ETCBalanceActivity.newIntent(MainActivity.this));
                overridePendingTransition(0, 0);
            } else {
                showServerErrorDialog();
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        }
    };

    private OnClickListener mOnSMenuSettingsTextViewClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(SettingsActivity.newIntent(MainActivity.this, SettingsActivity.SETTINGS_MENU));
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    };

    private OnClickListener mOnSMenuLogoutTextViewClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            mIsLogoutInProgress = true;
            if (mFunctionDeque.isEmpty()) {
                mFunctionDeque.offer(FUNC_ID_SHOW_PROGRESS_DIALOG);
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_LOGOUT);
                mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
                pollAndRunFunctionInDeque();
            } else {
                mFunctionDeque.offer(FUNC_ID_SHOW_PROGRESS_DIALOG);
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_LOGOUT);
                mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
            }
        }
    };

    private OnClickListener mOnSMenuDigitalKeyTextViewClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(DigitalKeyActivity.newIntent(MainActivity.this));
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    };

    private OnClickListener mOnEventAndEventTimeTextViewClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            final Intent calendarIntent = new Intent(Intent.ACTION_VIEW).setData(mCalendarUri);
            startActivity(calendarIntent);
        }
    };

    private OnClickListener mOnOneKeyFreshBrickClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (NetUtils.isConnectToInternet(MainActivity.this)) {
                if (mSelectedCarKeyData.isMasterKey()) {
                    if (isSelectedCarKeyDataActive()) {
                        startActivity(RemoteControlActivity.newIntent(MainActivity.this, RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_AIR_DATA));
                    } else {
                        showInactiveMasterKeyAlertDialog();
                    }

                } else {
                    startActivity(RemoteControlActivity.newIntent(MainActivity.this, RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_AIR_DATA));
                }
            } else {
                showServerErrorDialog();
            }
        }
    };

    private OnClickListener mOnARRemoteControlBrickClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mSelectedCarKeyData.isMasterKey()) {
                if (isSelectedCarKeyDataActive()) {
                    startActivity(RemoteControlActivity.newIntent(MainActivity.this, RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_MENU));
                } else {
                    showInactiveMasterKeyAlertDialog();
                }

            } else {
                startActivity(RemoteControlActivity.newIntent(MainActivity.this, RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_MENU));
            }
        }
    };

    private OnClickListener mOnCarStatusBrickClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (NetUtils.isConnectToInternet(MainActivity.this)) {
                startActivity(CarStatusActivity.newIntent(MainActivity.this));
            } else {
                showServerErrorDialog();
            }
        }
    };

    private OnClickListener mOnAvailableTripBrickClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (NetUtils.isConnectToInternet(MainActivity.this)) {
                startActivity(CarStatusActivity.newIntent(MainActivity.this));
            } else {
                showServerErrorDialog();
            }
        }
    };

    private OnClickListener mOnMaintenanceBrickClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(MaintenanceQueryCarNoActivity.newIntent(MainActivity.this, (int) view.getTag()));
        }
    };

    private OnClickListener mOnCanMessageBrickClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mHasUnreadPushMessageNotificationBeenShownInSpecialistIntroduction) {
                startActivity(PushListActivity.newIntent(MainActivity.this));
            } else {
                startActivity(OneKeyServiceActivity.newIntent(MainActivity.this));
            }
        }
    };

    private OnClickListener mOnBannerImageViewClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {

            if (NetUtils.isConnectToInternet(MainActivity.this)) {
                String uriString = (String) view.getTag();
                if (!uriString.contains("https://") && !uriString.contains("http://")) {
                    uriString = "https://" + uriString;
                }
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uriString));
                startActivity(intent);
            } else {
                startActivity(OneKeyServiceActivity.newIntent(MainActivity.this));
            }

        }
    };

    private ViewPager.OnPageChangeListener mOnActivityNewsViewPagerChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

            View indicatorView = mActivityNewsViewPagerIndicatorLayout.getChildAt(mCurrentViewPagerPosition);
            indicatorView.setEnabled(false);
            indicatorView = mActivityNewsViewPagerIndicatorLayout.getChildAt(position);
            indicatorView.setEnabled(true);

            mCurrentViewPagerPosition = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private OnApiListener<GetIkeyUidResponseData> mGetIkeyUidApiListener = new OnApiListener<GetIkeyUidResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetIkeyUidResponseData responseData) {
            if (responseData.isSuccessful()) {
                mUserName = responseData.getName();
                Prefs.setUserName(MainActivity.this, responseData.getUserName());
                updateDisplayRelatedToUserInfo();

                pollAndRunFunctionInDeque();
            } else {
                clearFunctionQueue();
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            // clearFunctionQueue();
            pollAndRunFunctionInDeque();
            LogCat.d("mGetIkeyUidApiListener: " + failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetInitialDataResponseData> mGetInitialDataApiListener = new OnApiListener<GetInitialDataResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetInitialDataResponseData responseData) {
            if (responseData.isSuccessful()) {
                Prefs.setBeaconUUID(MainActivity.this, responseData.getBeaconId());
                Prefs.setRepairCarQuestionnaireURL(MainActivity.this, responseData.getUrl());
                mCannedMessages = responseData.getConversations();

                pollAndRunFunctionInDeque();
            } else {
                clearFunctionQueue();
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            // clearFunctionQueue();
            pollAndRunFunctionInDeque();
            LogCat.d("mGetInitialDataApiListener: " + failMessage);
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetWeatherResponseData> mGetWeatherApiListener = new OnApiListener<GetWeatherResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetWeatherResponseData responseData) {
            if (responseData.isSuccessful()) {
                mTemperatureTextView.setVisibility(View.VISIBLE);
                mWeatherImageView.setVisibility(View.VISIBLE);

                String temperature = responseData.getTemperature().replace(".0", "");
                mTemperatureTextView.setText(String.format(Locale.TAIWAN, "%s°", temperature));
                int factor = Integer.parseInt(responseData.getWeatherFactor());
                if (factor == 1 || factor == 43) {
                    mWeatherImageView.setImageResource(R.drawable.ic_sunny);
                } else if (factor == 2 || factor == 3 || factor == 5 || factor == 6 || factor == 44) {
                    mWeatherImageView.setImageResource(R.drawable.ic_cloudy);
                } else if (factor == 7 || factor == 8 || factor == 45 || factor == 46) {
                    mWeatherImageView.setImageResource(R.drawable.ic_cloudy_sunny);
                } else if (factor == 24 || factor == 34) {
                    mWeatherImageView.setImageResource(R.drawable.ic_rainy_sunny);
                } else {
                    mWeatherImageView.setImageResource(R.drawable.ic_rainy);
                }
            } else {
                mTemperatureTextView.setVisibility(View.INVISIBLE);
                mWeatherImageView.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            mTemperatureTextView.setVisibility(View.INVISIBLE);
            mWeatherImageView.setVisibility(View.INVISIBLE);
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetPushResponseData> mGetPushListApiListener = new OnApiListener<GetPushResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetPushResponseData responseData) {
            if (responseData.isSuccessful()) {
                ArrayList<PushData> pushDataArrayList = responseData.getPushList();
                updateDisplayRelatedToPushList(pushDataArrayList);

                pollAndRunFunctionInDeque();
            } else {
                clearFunctionQueue();
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            // clearFunctionQueue();
            pollAndRunFunctionInDeque();
            LogCat.d("mGetPushListApiListener: " + failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetPushResponseData> mGetPushListApiIndependentlyListener = new OnApiListener<GetPushResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetPushResponseData responseData) {
            if (responseData.isSuccessful()) {
                ArrayList<PushData> pushDataArrayList = responseData.getPushList();
                updateDisplayRelatedToPushList(pushDataArrayList);
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            LogCat.d("mGetPushListApiIndependentlyListener: " + failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetCampaignResponseData> mGetActivityNewsApiListener = new OnApiListener<GetCampaignResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetCampaignResponseData responseData) {
            if (responseData.isSuccessful()) {
                updateDisplayRelatedToActivityNews(responseData.getCampaignList());

                pollAndRunFunctionInDeque();
            } else {
                clearFunctionQueue();
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            // clearFunctionQueue();
            pollAndRunFunctionInDeque();
            LogCat.d("mGetActivityNewsApiListener: " + failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetCarKeyResponseData> mGetCarKeyListApiListener = new OnApiListener<GetCarKeyResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetCarKeyResponseData responseData) {
            if (responseData.isSuccessful()) {
                mViewModel.syncCarKeyDataListToDb(responseData.getCarKeyDataList());
                buildCarKeyDataList(responseData.getCarKeyDataList());
            } else {
                if (NetUtils.isConnectToInternet(MainActivity.this)) {
                    clearFunctionQueue();
                    showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                            responseData.getErrorMessage());
                } else { // If no network, pass to show main screen.
                    buildCarKeyDataList(mViewModel.getCarKeyDataListFromDb());
                }
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            // If there any failure, get data from db
            buildCarKeyDataList(mViewModel.getCarKeyDataListFromDb());
            LogCat.d("mGetCarKeyListApiListener: " + failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetKeyChainListResponseData> mOnGetKeyChainListApiListener = new OnApiListener<GetKeyChainListResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetKeyChainListResponseData responseData) {
            if (responseData.isSuccessful()) {
                pollAndRunFunctionInDeque();
            } else {
                clearFunctionQueue();
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            clearFunctionQueue();
            LogCat.d("mOnGetKeyChainListApiListener: " + failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetIkeyEmployeeDataResponseData> mOnGetEmployeeDataApiListener = new OnApiListener<GetIkeyEmployeeDataResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetIkeyEmployeeDataResponseData responseData) {
            if (responseData.isSuccessful()) {
                if (mAnalyzeEmployeeDataAsyncTask == null) {
                    if (responseData.getEmployeeDataList() != null) {
                        mAnalyzeEmployeeDataAsyncTask = new AnalyzeEmployeeDataAsyncTask();
                        mAnalyzeEmployeeDataAsyncTask.execute(responseData);
                    } else {
                        updateDisplayRelatedToSpecialist(false);
                        pollAndRunFunctionInDeque();
                    }
                }
            } else {
                updateDisplayRelatedToSpecialist(false);
                pollAndRunFunctionInDeque();
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            updateDisplayRelatedToSpecialist(false);
            pollAndRunFunctionInDeque();
            LogCat.d("mOnGetEmployeeDataApiListener: " + failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetCarStatusResponseData> mGetCarStatusApiListener = new OnApiListener<GetCarStatusResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetCarStatusResponseData responseData) {
            if (responseData.isSuccessful()) {
                mSelectedCarStatusData = responseData;
                updateDisplayRelatedToSelectedCarStatusData();
                checkCarStatus();
            } else {
                mSelectedCarStatusData = null;
                Prefs.clearCarModel(MainActivity.this);
                updateDisplayRelatedToSelectedCarStatusData();
                clearFunctionQueue();
                String errorCode = responseData.getErrorCode();
                if (errorCode.contains("865")) {
                    showSelectedCarKeyDataDeletedDialog(mSelectedCarKeyData);
                } else {
                    showServerErrorDialog(getString(R.string.dialog_title_error) + errorCode,
                            responseData.getErrorMessage());
                }
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            mSelectedCarStatusData = null;
            Prefs.clearCarModel(MainActivity.this);
            updateDisplayRelatedToSelectedCarStatusData();
            clearFunctionQueue();
            LogCat.d("mGetCarStatusApiListener: " + failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetPm25ResponseData> mGetPM25ApiListener = new OnApiListener<GetPm25ResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetPm25ResponseData responseData) {
            if (responseData.isSuccessful()) {
                if (responseData.getSize() > 0) {
                    Pm25Data pm25Data = responseData.getList().get(0);
                    mPM2Point5ValueTextView.setText(String.valueOf((int) pm25Data.getRange()));
                } else {
                    mPM2Point5ValueTextView.setText(R.string.main_pm2point5_none);
                }

                pollAndRunFunctionInDeque();
            } else {
                clearFunctionQueue();
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }

        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            clearFunctionQueue();
            LogCat.d("mGetPM25ApiListener: " + failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetCarRepairProgressResponseData> mOnGetCarRepairProgressApiListener = new OnApiListener<GetCarRepairProgressResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetCarRepairProgressResponseData responseData) {
            if (responseData.isSuccessful() && responseData.hasProgress()) {
                mHaveGottonCarRepairProgress = true;
                mMaintenanceStatusTextView.setText(R.string.main_maintainence_progress_label);
                mMaintenanceStatusTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        getDrawable(R.drawable.ic_reserve_progress), null, null, null);
                mAvailableTripBrick.setTag(2);
                mMaintenanceBrick.setTag(2);
            } else {
                mHaveGottonCarRepairProgress = false;
            }

            pollAndRunFunctionInDeque();
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            clearFunctionQueue();
            LogCat.d("mOnGetCarRepairProgressApiListener: " + failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetCarRepairPlanResponseData> mOnGetCarRepairPlanApiListener = new OnApiListener<GetCarRepairPlanResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetCarRepairPlanResponseData responseData) {
            if (responseData.isSuccessful()) {
                if (responseData.getSize() != 0) {
                    mMaintenanceStatusTextView.setText(R.string.main_maintainence_user_reservations_label);
                    mMaintenanceStatusTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            getDrawable(R.drawable.ic_car_reservation), null, null, null);
                    mMaintenanceBrick.setTag(1);

                    pollAndRunFunctionInDeque();
                } else {
                    mMaintenanceStatusTextView.setText(R.string.main_maintainence_reservation_label);
                    mMaintenanceStatusTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            getDrawable(R.drawable.ic_warranty_reserve_white), null, null, null);
                    mMaintenanceBrick.setTag(0);

                    pollAndRunFunctionInDeque();
                }
            } else {
                // 無預約保修
                if (responseData.getErrorCode().equals("-940") || responseData.getErrorCode().equals("-804")) {
                    mMaintenanceStatusTextView.setText(R.string.main_maintainence_reservation_label);
                    mMaintenanceStatusTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            getDrawable(R.drawable.ic_warranty_reserve_white), null, null, null);
                    mMaintenanceBrick.setTag(0);

                    pollAndRunFunctionInDeque();
                } else {
                    clearFunctionQueue();
                    showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                            responseData.getErrorMessage());
                }
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            clearFunctionQueue();
            LogCat.d("mOnGetCarRepairPlanApiListener: " + failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetSpeedLimitStatusResponseData> mOnGetSpeedLimitStatusApiListener = new OnApiListener<GetSpeedLimitStatusResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetSpeedLimitStatusResponseData responseData) {
            if (responseData.isSuccessful()) {
                ArrayList<SpeedLimitStatusData> speedLimitStatusArrayList = responseData.getList();
                boolean isSpeedLimitOn = false;

                for (int index = 0; index < speedLimitStatusArrayList.size(); index++) {
                    SpeedLimitStatusData data = speedLimitStatusArrayList.get(index);
                    if (data.getCarId().equals(mSelectedCarKeyData.getCarId()) &&
                            data.getVigCarId().equals(mSelectedCarKeyData.getVigCarId())) {
                        try {
                            long speedLimitMillisecond = mSimpleDateFormatForSpeedLimitStartDateTime.parse(data.getStartDateTime()).getTime();
                            if (System.currentTimeMillis() > speedLimitMillisecond) {
                                isSpeedLimitOn = true;
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }

                if (isSpeedLimitOn) {
                    mSMenuStolenCarSpeedLimitTextView.setVisibility(View.VISIBLE);
                    mStolenCarSpeedLimitTextView.setVisibility(View.VISIBLE);
                } else {
                    mSMenuStolenCarSpeedLimitTextView.setVisibility(View.GONE);
                    mStolenCarSpeedLimitTextView.setVisibility(View.GONE);
                }

                pollAndRunFunctionInDeque();
            } else {
                clearFunctionQueue();
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            clearFunctionQueue();
            LogCat.d("mOnGetSpeedLimitStatusApiListener: " + failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    OnApiListener<ResponseData> mLogoutApiIndependentlyListener = new OnApiListener<ResponseData>() {

        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            if (responseData.isSuccessful()) {
                mIsLogoutInProgress = false;
                clearLocalData();

                startActivity(new Intent(MainActivity.this, SplashActivity.class));
                pollAndRunFunctionInDeque();
                finish();
            } else {
                clearFunctionQueue();
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            clearFunctionQueue();
            LogCat.d("mLogoutApiIndependentlyListener: " + failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent != null) {
            if (intent.hasExtra(INTENT_EXTRA_NAME_SHOW_CAR_KEY_LIST)) {
                if (mDrawerLayout != null) {
                    if (mIsCarKeyListSwitchEnabled) {
                        mIsCarKeyListOpened = false;
                        switchCarKeyListDisplay();
                    }
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            }

            if (intent.hasExtra(INTENT_EXTRA_NAME_FORCE_EXIT_APP)) {
                mIsLogoutInProgress = true;
                clearFunctionQueue();
                mFunctionDeque.offer(FUNC_ID_SHOW_PROGRESS_DIALOG);
                mFunctionDeque.offer(FUNC_ID_MULTILOGIN_RELOGIN);
                mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
                pollAndRunFunctionInDeque();
            }

            if (intent.hasExtra(INTENT_EXTRA_NAME_RESELECT_CAR_KEY_DATA)) {
                if (mUserInfo == null) {
                    mUserInfo = Prefs.getUserInfo(MainActivity.this);
                }
                mSelectedCarKeyData = null;
                Prefs.clearCarKeyData(MainActivity.this);
                Prefs.clearCarKeyDataWithIKeyUid(MainActivity.this, mUserInfo.getKeyUid());
                mMainCarKeyListAdapter.setSelectedItemPosition(-1);
                resetDisplayRelatedToSelectedCarKeyDataAndCarStatusData();
                clearFunctionQueue();
                if (mFunctionDeque.isEmpty()) {
                    mFunctionDeque.offer(FUNC_ID_SHOW_PROGRESS_DIALOG);
                    mFunctionDeque.offer(FUNC_ID_REMOVE_BACKGROUND_REFRESH_MESSAGE);
                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_KEY_LIST);
                    mFunctionDeque.offer(FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA);
                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_EMPLOYEE_DATA);
                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_STATUS);
                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_PM25);
                    mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
                    mFunctionDeque.offer(FUNC_ID_SEND_BACKGROUND_REFRESH_MESSAGE);
                    pollAndRunFunctionInDeque();
                } else {
                    mFunctionDeque.offer(FUNC_ID_SHOW_PROGRESS_DIALOG);
                    mFunctionDeque.offer(FUNC_ID_REMOVE_BACKGROUND_REFRESH_MESSAGE);
                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_KEY_LIST);
                    mFunctionDeque.offer(FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA);
                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_EMPLOYEE_DATA);
                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_STATUS);
                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_PM25);
                    mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
                    mFunctionDeque.offer(FUNC_ID_SEND_BACKGROUND_REFRESH_MESSAGE);
                }
            }

            if (intent.hasExtra(INTENT_EXTRA_NAME_REFRESH_CAR_KEY_LIST)) {
                clearFunctionQueue();
                refreshCarKeyList();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ActionBar
        Toolbar toolbar = findViewById(R.id.activity_main_toolbar);
        toolbar.setTitle("");

        ViewModelFactory factory = new ViewModelFactory(getApplication());
        mViewModel = ViewModelProviders.of(this, factory).get(MainViewModel.class);
        mViewModel.setUseLog();

        View view = getLayoutInflater().inflate(R.layout.activity_main_actionbar_notification_center_view, null);
        mActionBarNotificationCenterImageView = view.findViewById(R.id.activity_main_actionbar_notification_center_image_view);
        mActionBarNotificationCenterImageView.setOnClickListener(mOnActionBarNotificationCenterImageViewClickListener);
        toolbar.addView(view, new Toolbar.LayoutParams(Gravity.END));

        View logoImageView = getLayoutInflater().inflate(R.layout.activity_main_actionbar_logo_image_view, null);
        logoImageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Prefs.toggleCaLog(MainActivity.this);
                return false;
            }
        });
        toolbar.addView(logoImageView, new Toolbar.LayoutParams(Gravity.CENTER));

        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
        }

        mMainScreenLayout = findViewById(R.id.activity_main_linear_layout);
        mMainScreenLayout.setOnTouchListener(mOnGlobalTouchListener);

        mDrawerLayout = findViewById(R.id.activity_main_drawer_layout);
        mDrawerLayout.addDrawerListener(mDrawerListener);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        // Copy the ripple effect of Home Button to Notification Center Button
        View homeView = toolbar.getChildAt(toolbar.getChildCount() - 1);
        Drawable drawableNewCopy = Objects.requireNonNull(homeView.getBackground().getConstantState()).newDrawable().mutate();
        mActionBarNotificationCenterImageView.setBackground(drawableNewCopy);

        // Sliding Menu
        mSlidingmenuMenuScrollView = findViewById(R.id.activity_main_slidingmenu_menu_scroll_view);
        mSlidingmenuCarKeyListRecyclerView = findViewById(R.id.activity_main_slidingmenu_car_key_list_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        if (mMainCarKeyListAdapter == null) {
            mMainCarKeyListAdapter = new MainCarKeyListAdapter(this);
            mMainCarKeyListAdapter.setOnInnerViewsClickListener(mOnSlidingmenuCarKeyListRecyclerViewInnerViewsClickListener);
        }
        mSlidingmenuCarKeyListRecyclerView.setAdapter(mMainCarKeyListAdapter);
        mSlidingmenuCarKeyListRecyclerView.setLayoutManager(linearLayoutManager);

        mSMenuUserNameTextView = findViewById(R.id.activity_main_slidingmenu_user_name_text_view);
        mSMenuUserNameTextView.setOnClickListener(mOnSMenuUserNameTextViewClickListener);
        mSMenuCarModelTextView = findViewById(R.id.activity_main_slidingmenu_car_model_text_view);
        mSMenuCarCodeTextView = findViewById(R.id.activity_main_slidingmenu_car_code_text_view);
        mSMenuPlateNumberTextView = findViewById(R.id.activity_main_slidingmenu_plate_number_text_view);
        mSMenuPlateNumberTextView.setOnClickListener(mOnSMenuPlateNumberTextViewClickListener);
        mSMenuAddNewCarTextView = findViewById(R.id.activity_main_slidingmenu_add_new_car_text_view);
        mSMenuAddNewCarTextView.setOnClickListener(mOnSMenuAddNewCarTextViewClickListener);
        mSMenuStolenCarSpeedLimitTextView = findViewById(R.id.activity_main_slidingmenu_stolen_car_speed_limit_text_view);
        mSMenuStolenCarSpeedLimitTextView.setOnClickListener(mOnSMenuStolenCarSpeedLimitTextViewClickListener);
        mSMenuARRemoteControlTextView = findViewById(R.id.activity_main_slidingmenu_ar_remote_control_text_view);
        mSMenuARRemoteControlTextView.setOnClickListener(mOnSMenuARRemoteControlTextViewClickListener);
        mSMenuCarStatusReviewTextView = findViewById(R.id.activity_main_slidingmenu_car_status_review_text_view);
        mSMenuCarStatusReviewTextView.setOnClickListener(mOnSMenuCarStatusReviewTextViewClickListener);
        mSMenuReservationTextView = findViewById(R.id.activity_main_slidingmenu_reservation_text_view);
        mSMenuReservationTextView.setOnClickListener(mOnSMenuReservationTextViewClickListener);
        mSMenuServiceCenterTextView = findViewById(R.id.activity_main_slidingmenu_service_center_text_view);
        mSMenuServiceCenterTextView.setOnClickListener(mOnSMenuServiceCenterTextViewClickListener);
        mSMenuOneKeyWaterMarkTextView = findViewById(R.id.activity_main_slidingmenu_one_key_watermark_text_view);
        mSMenuOneKeyWaterMarkTextView.setOnClickListener(mOnSMenuOneKeyWaterMarkTextViewClickListener);
        mSMenuDashBoardSymbolsTextView = findViewById(R.id.activity_main_slidingmenu_dashboard_symbols_text_view);
        mSMenuDashBoardSymbolsTextView.setOnClickListener(mOnSMenuDashBoardSymbolsTextViewClickListener);
        mSMenuCarBonusTextView = findViewById(R.id.activity_main_slidingmenu_car_bonus_text_view);
        mSMenuCarBonusTextView.setOnClickListener(mOnSMenuCarBonusTextViewClickListener);
        mSMenuETCCheckTextView = findViewById(R.id.activity_main_slidingmenu_etc_check_text_view);
        mSMenuETCCheckTextView.setOnClickListener(mOnSMenuETCCheckTextViewClickListener);
        mSMenuSettingsTextView = findViewById(R.id.activity_main_slidingmenu_settings_text_view);
        mSMenuSettingsTextView.setOnClickListener(mOnSMenuSettingsTextViewClickListener);
        mSMenuLogoutTextView = findViewById(R.id.activity_main_slidingmenu_logout_text_view);
        mSMenuLogoutTextView.setOnClickListener(mOnSMenuLogoutTextViewClickListener);
        mSMenuDigitalKeyTextView = findViewById(R.id.activity_main_slidingmenu_digital_key_text_view);
        mSMenuDigitalKeyTextView.setOnClickListener(mOnSMenuDigitalKeyTextViewClickListener);

        // Dynamic Bricks
        mARRemoteControlBlock = findViewById(R.id.activity_main_ar_remote_control_block_layout);
        mCarStatusBlock = findViewById(R.id.activity_main_car_status_block_layout);

        mStolenCarSpeedLimitTextView = findViewById(R.id.activity_main_stolen_car_speed_limit_alert_text_view);
        mWeatherImageView = findViewById(R.id.activity_main_weather_image_view);
        mTemperatureTextView = findViewById(R.id.activity_main_temperature_text_view);
        mEventTextView = findViewById(R.id.activity_main_event_text_view);
        mEventTextView.setOnTouchListener(mOnGlobalTouchListener);
        mEventTimeTextView = findViewById(R.id.activity_main_event_time_text_view);
        mEventTimeTextView.setOnTouchListener(mOnGlobalTouchListener);
        mActivityNewsViewPager = findViewById(R.id.activity_main_activity_news_view_pager);
        mActivityNewsViewPager.addOnPageChangeListener(mOnActivityNewsViewPagerChangeListener);
        mActivityNewsViewPagerIndicatorLayout = findViewById(R.id.activity_main_activity_news_view_pager_indicator_layout);
        mOneKeyFreshBrick = findViewById(R.id.activity_main_one_key_fresh_brick_layout);
        mOneKeyFreshBrick.setOnClickListener(mOnOneKeyFreshBrickClickListener);
        mOneKeyFreshBrick.setOnTouchListener(mOnGlobalTouchListener);
        mPM2Point5ValueTextView = findViewById(R.id.activity_main_pm2point5_value_text_view);
        mARRemoteControlBrick = findViewById(R.id.activity_main_ar_remote_control_brick_layout);
        mARRemoteControlBrick.setOnClickListener(mOnARRemoteControlBrickClickListener);
        mARRemoteControlBrick.setOnTouchListener(mOnGlobalTouchListener);
        mARRemoteControlBrickLightBarView = findViewById(R.id.activity_main_ar_remote_control_brick_light_bar_view);
        mARRemoteControlStatusTextView = findViewById(R.id.activity_main_ar_remote_control_status_text_view);
        mARRemoteControlStatusTextView.setTextStillTime(5000);
        mARRemoteControlStatusTextView.setAnimTime(300);
        mCarStatusBrick = findViewById(R.id.activity_main_car_status_brick_layout);
        mCarStatusBrick.setOnClickListener(mOnCarStatusBrickClickListener);
        mCarStatusBrick.setOnTouchListener(mOnGlobalTouchListener);
        mCarStatusLightBarView = findViewById(R.id.activity_main_car_status_brick_light_bar_view);
        mCarStatusTextView = findViewById(R.id.activity_main_car_status_text_view);
        mAvailableTripBrick = findViewById(R.id.activity_main_available_trip_brick_layout);
        mAvailableTripBrick.setOnClickListener(mOnAvailableTripBrickClickListener);
        mAvailableTripBrick.setOnTouchListener(mOnGlobalTouchListener);
        mAvailableTripStatusTextView = findViewById(R.id.activity_main_available_trip_status_text_view);
        mMaintenanceBrick = findViewById(R.id.activity_main_maintenance_brick_layout);
        mMaintenanceBrick.setTag(0);
        mMaintenanceBrick.setOnClickListener(mOnMaintenanceBrickClickListener);
        mMaintenanceBrick.setOnTouchListener(mOnGlobalTouchListener);
        mMaintenanceStatusTextView = findViewById(R.id.activity_main_maintenance_status_text_view);
        mLastUpdateTimeTextView = findViewById(R.id.activity_main_last_update_time_text_view);
        mCanMessageBrick = findViewById(R.id.activity_main_can_message_brick_layout);
        mCanMessageBrick.setOnClickListener(mOnCanMessageBrickClickListener);
        mCanMessageBrick.setOnTouchListener(mOnGlobalTouchListener);
        mAdvisorTextView = findViewById(R.id.activity_main_advisor_text_view);
        mCanMessageTextView = findViewById(R.id.activity_main_can_message_text_view);
        mCanMessageTextView.setTextStillTime(10000);
        mCanMessageTextView.setAnimTime(300);

        setWheelScreenLayout(mMainScreenLayout);

        ImageView wheelRightHandle = findViewById(R.id.activity_main_right_handle_image_view);
        ImageView wheelLeftHandle = findViewById(R.id.activity_main_left_handle_image_view);
        setWheelHandles(wheelRightHandle, wheelLeftHandle);

        mArrowDownDrawable = getDrawable(R.drawable.ic_drop_down_white);
        if (mArrowDownDrawable != null) {
            mArrowDownDrawable.setBounds(0, 0, mArrowDownDrawable.getMinimumWidth(), mArrowDownDrawable.getMinimumHeight());
        }
        mArrowUpDrawable = getDrawable(R.drawable.ic_drop_up_white);
        if (mArrowUpDrawable != null) {
            mArrowUpDrawable.setBounds(0, 0, mArrowUpDrawable.getMinimumWidth(), mArrowUpDrawable.getMinimumHeight());
        }
        mNotificationCenterDrawable = getDrawable(R.drawable.ic_alert);
        if (mNotificationCenterDrawable != null) {
            mNotificationCenterDrawable.setBounds(0, 0, mNotificationCenterDrawable.getMinimumWidth(), mNotificationCenterDrawable.getMinimumHeight());
        }
        mNotificationCenterAlertDrawable = getDrawable(R.drawable.ic_alert_new);
        if (mNotificationCenterAlertDrawable != null) {
            mNotificationCenterAlertDrawable.setBounds(0, 0, mNotificationCenterAlertDrawable.getMinimumWidth(), mNotificationCenterAlertDrawable.getMinimumHeight());
        }

        mCountryResultReceiver = new CountryResultReceiver(new Handler());
        mLocationRequestModule = new LocationRequestModule(this);
        mLocationRequestModule.init();
        mLocationRequestModule.setLocationChangedListener(mLocationListener);

        mSpaceString = getString(R.string.common_space);

        Utils.init(this);

        // Reset Flags
        Prefs.setRefreshDigitalKeyListFlag(this, false);
        Prefs.setRefreshCarStatusFlag(this, false);
        Prefs.setRefreshCarKeyListFlag(this, Prefs.FLAG_REFRESH_CAR_KEY_LIST_DONE);

        bindCaService();
    }

    @Override
    protected void onCaServiceConnected() {
        init();
        super.onCaServiceConnected();
    }

    @Override
    public void onStart() {
        if (mIsInitDone) {
            mViewModel.syncUseLog();
        }

        if (NetUtils.isConnectToInternet(this)) {
            mNeedToSyncPushDataToServer = false;
            mViewModel.syncPushDataToServer();
        } else {
            mNeedToSyncPushDataToServer = true;
        }

        ArrayList<String> permissionArrayList = new ArrayList<>();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            permissionArrayList.add(Manifest.permission.READ_CALENDAR);
            mCalendarUri = null;
            mEventTextView.setText(R.string.main_calendar_event_no_permission);
            mEventTextView.setOnClickListener(null);
            mEventTimeTextView.setText(R.string.common_space);
            mEventTimeTextView.setOnClickListener(null);
        } else {
            executeFunction(FUNC_ID_GET_GOOGLE_CALENDAR_EVENT_INDEPENDENTLY);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionArrayList.add(Manifest.permission.ACCESS_FINE_LOCATION);
            permissionArrayList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        } else {
            if (GpsUtils.isGpsOpened(this)) {
                if (mLocationRequestModule != null) {
                    mLocationRequestModule.startLocationUpdates();
                }
            } else {
                if (!mIsGpsStatusReceiverRegistered) {
                    registerReceiver(mGpsStatusBroadcastReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
                    mIsGpsStatusReceiverRegistered = true;
                }
            }
        }

        if (permissionArrayList.size() > 0) {
            String[] permisssionStringArray = permissionArrayList.toArray(new String[permissionArrayList.size()]);
            ActivityCompat.requestPermissions(MainActivity.this,
                    permisssionStringArray,
                    CommonDefs.REQUEST_CODE_REQUEST_PERMISSIONS);
        }

        setupFcm();

        if (Prefs.getRefreshCarStatusFlag(this)) {
            Prefs.setRefreshCarStatusFlag(this, false);
            if (mFunctionDeque.isEmpty()) {
                mFunctionDeque.offer(FUNC_ID_SHOW_PROGRESS_DIALOG);
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_STATUS);
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_PM25);
                mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
                pollAndRunFunctionInDeque();
            } else {
                mFunctionDeque.offer(FUNC_ID_SHOW_PROGRESS_DIALOG);
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_STATUS);
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_PM25);
                mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
            }
        }

        int refreshCarKeyListFlag = Prefs.getRefreshCarKeyListFlag(this);
        if (refreshCarKeyListFlag != Prefs.FLAG_REFRESH_CAR_KEY_LIST_DONE) {
            Prefs.setRefreshCarKeyListFlag(this, Prefs.FLAG_REFRESH_CAR_KEY_LIST_DONE);
            refreshCarKeyList();

            mNeedToShowCarKeyList = (refreshCarKeyListFlag == Prefs.FLAG_REFRESH_CAR_KEY_LIST_AND_OPEN_IT);
        }

        if (Prefs.getCheckAllKeysActivationStatusFlag(this)) {
            Prefs.setCheckAllKeysActivationStatusFlag(this, false);
            if (mFunctionDeque.isEmpty()) {
                mFunctionDeque.offer(FUNC_ID_SHOW_PROGRESS_DIALOG);
                mFunctionDeque.offer(FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA);
                // mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_EMPLOYEE_DATA);
                mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
                pollAndRunFunctionInDeque();
            } else {
                mFunctionDeque.offer(FUNC_ID_SHOW_PROGRESS_DIALOG);
                mFunctionDeque.offer(FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA);
                // mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_EMPLOYEE_DATA);
                mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
            }
        }

        if (mIsInitDone && !mIsLogoutInProgress) {
            mARRemoteControlStatusTextView.startAutoScroll();

            if (mHasUnreadPushMessageNotificationBeenShownInSpecialistIntroduction) {
                updateDisplayRelatedToSpecialist(true);
                mHasUnreadPushMessageNotificationBeenShownInSpecialistIntroduction = false;
            } else {
                mCanMessageTextView.startAutoScroll();
            }

            // Do background refresh.
            if (mFunctionDeque.isEmpty()) {
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_PUSH_LIST);
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_ACTIVITY_NEWS);
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_EMPLOYEE_DATA);
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_STATUS);
                mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_PM25);
                mFunctionDeque.offer(FUNC_ID_SEND_BACKGROUND_REFRESH_MESSAGE);
                // Toast.makeText(MainActivity.this,"Refresh in background", Toast.LENGTH_SHORT).show();
                pollAndRunFunctionInDeque();
            } else {
                mFunctionDeque.offer(FUNC_ID_SEND_BACKGROUND_REFRESH_MESSAGE);
            }
        }

        super.onStart();
    }

    @Override
    protected void onResume() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        //intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        //intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        registerReceiver(mNetworkStatusBroadcastReceiver, intentFilter);
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(mNetworkStatusBroadcastReceiver);
        if (mLocationRequestModule != null) {
            mLocationRequestModule.stopLocationUpdates();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mIsInitDone) {
            if (mFunctionDeque.isEmpty()) {
                mFunctionDeque.offer(FUNC_ID_REMOVE_BACKGROUND_REFRESH_MESSAGE);
                pollAndRunFunctionInDeque();
            } else {
                mFunctionDeque.offer(FUNC_ID_REMOVE_BACKGROUND_REFRESH_MESSAGE);
            }
        }

        mARRemoteControlStatusTextView.stopAutoScroll();
        mCanMessageTextView.stopAutoScroll();
    }

    @Override
    protected void onDestroy() {
        clearFunctionQueue();
        executeFunction(FUNC_ID_REMOVE_BACKGROUND_REFRESH_MESSAGE);

        if (mGetGoogleCalendarEventAsyncTask != null) {
            mGetGoogleCalendarEventAsyncTask.cancel(true);
        }

        if (mGetUserInfoAsyncTask != null) {
            mGetUserInfoAsyncTask.cancel(true);
        }

        if (mCheckAllKeysActivationStatusAndSelectCarKeyDataAsyncTask != null) {
            mCheckAllKeysActivationStatusAndSelectCarKeyDataAsyncTask.cancel(true);
        }

        if (mAnalyzeEmployeeDataAsyncTask != null) {
            mAnalyzeEmployeeDataAsyncTask.cancel(true);
        }

        if (mCheckCarStatusAsyncTask != null) {
            mCheckCarStatusAsyncTask.cancel(true);
        }

        if (mIsGpsStatusReceiverRegistered) {
            unregisterReceiver(mGpsStatusBroadcastReceiver);
            mIsGpsStatusReceiverRegistered = false;
        }

        unbindCaService();

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            mStopGlide = true;
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mLocationRequestModule != null) {
            mLocationRequestModule.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CommonDefs.REQUEST_CODE_REQUEST_PERMISSIONS && grantResults.length > 0) {
            boolean getPermissionReadCalendar = false;
            boolean getPermissionFineLocation = false;
            boolean getPermissionCoarseLocation = false;
            boolean getPermissionNotification = false;
            int permissionCount = permissions.length;

            for (int index = 0; index < permissionCount; index++) {
                if (permissions[index].equals(Manifest.permission.READ_CALENDAR) && grantResults[index] == PackageManager.PERMISSION_GRANTED) {
                    getPermissionReadCalendar = true;
                    continue;
                }

                if (permissions[index].equals(Manifest.permission.ACCESS_FINE_LOCATION) && grantResults[index] == PackageManager.PERMISSION_GRANTED) {
                    getPermissionFineLocation = true;
                    continue;
                }

                if (permissions[index].equals(Manifest.permission.ACCESS_COARSE_LOCATION) && grantResults[index] == PackageManager.PERMISSION_GRANTED) {
                    getPermissionCoarseLocation = true;
                    continue;
                }

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    // 6.0 以後
                    if (permissions[index].equals(Manifest.permission.ACCESS_NOTIFICATION_POLICY) && grantResults[index] == PackageManager.PERMISSION_GRANTED) {
                        getPermissionNotification = true;
                    }
                } else {
                    // 6.0 之前
                }

            }

            if (getPermissionReadCalendar) {
                executeFunction(FUNC_ID_GET_GOOGLE_CALENDAR_EVENT_INDEPENDENTLY);
            }

            if (getPermissionFineLocation && getPermissionCoarseLocation) {
                if (mLocationRequestModule != null) {
                    mLocationRequestModule.startLocationUpdates();
                }
            }
        }
    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case CommonDefs.INNER_MSG_WHAT_REFRESH_IN_BACKGROUND: {
                if (mFunctionDeque.isEmpty()) {
                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_PUSH_LIST);
                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_ACTIVITY_NEWS);
                    // mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_EMPLOYEE_DATA);
                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_STATUS);
                    mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_PM25);
                    mFunctionDeque.offer(FUNC_ID_SEND_BACKGROUND_REFRESH_MESSAGE);
                    // Toast.makeText(MainActivity.this,"Refresh in background", Toast.LENGTH_SHORT).show();
                    pollAndRunFunctionInDeque();
                } else {
                    mFunctionDeque.offer(FUNC_ID_SEND_BACKGROUND_REFRESH_MESSAGE);
                }

                break;
            }

            default:
                super.handleMessage(msg);
        }
    }

    @Override
    protected void onReloadSelectedCarKeyData(CarKeyData selectedCarKeyData) {
        mSelectedCarKeyData = selectedCarKeyData;
    }

    @Override
    protected void addActionToReceiveBroadcastFromCaService(IntentFilter intentFilter) {
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_IKEY_LOGIN);
    }

    @Override
    protected void onReceiveBroadcastFromCaService(Context context, Intent intent) {
        if (CommunicationAgentService.CA_STATUS_IKEY_LOGIN.equals(intent.getAction())) {
            if (intent.hasExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE)) {
                int returnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);

                if (returnCode == CA_EXTRA_LOGIN_SUCCESS_RETURN_CODE) {

                } else if (returnCode == CA_EXTRA_MAX_LOGIN_RETRY_RETURN_CODE) {
                    LogCat.d("ikey login error: 失敗重試已達上限");
                    showConfirmDialog(getString(R.string.dialog_title_error), getString(R.string.fake_999));
                } else {
                    LogCat.d("ikey login error: " + returnCode);
                    showConfirmDialog(getString(R.string.dialog_title_error), getString(R.string.fake_999));
                }

            } else if (intent.hasExtra(CA_STATUS_EXTRA_COMMAND)) {
                String message = intent.getStringExtra(CA_STATUS_EXTRA_COMMAND);
                LogCat.d("ikey login failed: " + message);
                showConfirmDialog(getString(R.string.dialog_title_error), getString(R.string.fake_999));
            }

            boolean caHasBeenChanged = false;
            if (mS1CarKeyList != null && mS1CarKeyList.size() > 0) {
                ArrayList<SubKeyDataBean> subKeyDataList = mCommunicationAgentService.getCA().getSubKeyDataList();
                for (int index = 0; index < mS1CarKeyList.size(); index++) {
                    CarKeyData data = mS1CarKeyList.get(index);
                    for (SubKeyDataBean bean : subKeyDataList) {
                        if (bean.getUserId().equals(mUserInfo.getKeyUid()) &&
                                bean.getDeviceId().equals(mUserInfo.getDeviceId()) &&
                                data.getPhoneId().equals(mUserInfo.getDeviceId()) &&
                                bean.getCarId().equals(data.getVigCarId()) &&
                                bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                            caHasBeenChanged = true;
                            break;
                        }
                    }
                }
            }

            if (mT1CarKeyList != null && mT1CarKeyList.size() > 0) {
                for (int index = 0; index < mT1CarKeyList.size(); index++) {
                    CarKeyData data = mT1CarKeyList.get(index);
                    ArrayList<SnapKeyDataBean> snapKeyDataList = mCommunicationAgentService.getCA().getSnapKeyDataListByCarId(data.getVigCarId());
                    for (SnapKeyDataBean bean : snapKeyDataList) {
                        if (bean.getUserId().equals(mUserInfo.getKeyUid()) &&
                                bean.getDeviceId().equals(mUserInfo.getDeviceId()) &&
                                data.getPhoneId().equals(mUserInfo.getDeviceId()) &&
                                bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                            caHasBeenChanged = true;
                            break;
                        }
                    }
                }
            }

            if (caHasBeenChanged) {
                mFunctionDeque.offerFirst(FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA);
                mFunctionDeque.offerFirst(FUNC_ID_CALL_API_TO_GET_CAR_KEY_LIST);
            }

            pollAndRunFunctionInDeque();

            return;
        } else if (CommunicationAgentService.CA_STATUS_MASTERKEY_DELETED.equals(intent.getAction())) {
            String deletedVigCarId = intent.getStringExtra(CommunicationAgentService.CA_STATUS_EXTRA_COMMAND);
            if (mSelectedCarKeyData != null && mSelectedCarKeyData.isMasterKey() && mSelectedCarKeyData.getVigCarId().equals(deletedVigCarId)) {
                showConfirmDialog(getString(R.string.digital_key_master_key_dialog_receive_master_key_deleted_title),
                        getString(R.string.digital_key_master_key_dialog_receive_master_key_deleted_message),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                refreshCarKeyList();
                            }
                        });
            } else {
                showConfirmDialog(getString(R.string.dialog_title_remind),
                        getString(R.string.dialog_message_master_key_deleted));
            }

            return;

        } else if (CommunicationAgentService.CA_STATUS_SUBKEY_ADDED.equals(intent.getAction())) {
            if (mSelectedCarKeyData == null || (mSelectedCarKeyData != null && mNumOfCarKeyData == 1 && mUserInfo.isDMS())) {
                showConfirmDialog(getString(R.string.dialog_title_sub_key_added),
                        getString(R.string.dialog_message_sub_key_added),
                        new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                refreshCarKeyList();
                            }
                        });
            } else {
                showConfirmDialog(getString(R.string.dialog_title_sub_key_added),
                        getString(R.string.dialog_message_sub_key_added));
            }

            return;
        } else if (CommunicationAgentService.CA_STATUS_SUBKEY_DELETED.equals(intent.getAction())) {
            String deletedVigCarId = intent.getStringExtra(CommunicationAgentService.CA_STATUS_EXTRA_COMMAND);
            if (mSelectedCarKeyData != null && mSelectedCarKeyData.isSubKey() && mSelectedCarKeyData.getVigCarId().equals(deletedVigCarId)) {
                showSelectedCarKeyDataDeletedDialog(mSelectedCarKeyData);
            } else {
                if ((mSelectedCarKeyData != null && mNumOfCarKeyData == 2 && mUserInfo.isDMS()) || (mNumOfCarKeyData < 2)) {
                    showConfirmDialog(getString(R.string.dialog_title_remind),
                            getString(R.string.dialog_message_sub_key_deleted),
                            new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    refreshCarKeyList();
                                }
                            });
                } else {
                    showConfirmDialog(getString(R.string.dialog_title_remind),
                            getString(R.string.dialog_message_sub_key_deleted));
                }
            }

            return;
        } else if (CommunicationAgentService.CA_STATUS_SNAPKEY_ADDED.equals(intent.getAction())) {
            if (mSelectedCarKeyData == null || (mSelectedCarKeyData != null && mNumOfCarKeyData == 1 && mUserInfo.isDMS())) {
                showConfirmDialog(getString(R.string.dialog_title_remind),
                        getString(R.string.dialog_message_snap_key_added),
                        new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                refreshCarKeyList();
                            }
                        });
            } else {
                showConfirmDialog(getString(R.string.dialog_title_remind),
                        getString(R.string.dialog_message_snap_key_added));
            }

            return;
        } else if (CommunicationAgentService.CA_STATUS_SNAPKEY_DELETED.equals(intent.getAction())) {
            if (mCommunicationAgentService != null && mUserInfo != null && mSelectedCarKeyData != null && mSelectedCarKeyData.isSnapKey()) {
                boolean isKeyActive = false;
                ArrayList<SnapKeyDataBean> snapKeyDataList = mCommunicationAgentService.getCA().getSnapKeyDataListByCarId(mSelectedCarKeyData.getVigCarId());
                for (SnapKeyDataBean bean : snapKeyDataList) {
                    if (bean.getUserId().equals(mUserInfo.getKeyUid()) &&
                            bean.getDeviceId().equals(mUserInfo.getDeviceId()) &&
                            bean.getCarId().equals(mSelectedCarKeyData.getVigCarId()) &&
                            bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                        isKeyActive = true;
                        break;
                    }
                }

                if (isKeyActive) {
                    if ((mSelectedCarKeyData != null && mNumOfCarKeyData == 2 && mUserInfo.isDMS()) || (mNumOfCarKeyData < 2)) {
                        showConfirmDialog(getString(R.string.dialog_title_remind),
                                getString(R.string.dialog_message_snap_key_deleted),
                                new View.OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        refreshCarKeyList();
                                    }
                                });
                    } else {
                        showConfirmDialog(getString(R.string.dialog_title_remind),
                                getString(R.string.dialog_message_snap_key_deleted));
                    }
                } else {
                    showSelectedCarKeyDataDeletedDialog(mSelectedCarKeyData);
                }

            } else {
                if ((mSelectedCarKeyData != null && mNumOfCarKeyData == 2 && mUserInfo.isDMS()) || (mNumOfCarKeyData < 2)) {
                    showConfirmDialog(getString(R.string.dialog_title_remind),
                            getString(R.string.dialog_message_snap_key_deleted),
                            new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    refreshCarKeyList();
                                }
                            });
                } else {
                    showConfirmDialog(getString(R.string.dialog_title_remind),
                            getString(R.string.dialog_message_snap_key_deleted));
                }
            }

            return;
        }

        super.onReceiveBroadcastFromCaService(context, intent);
    }

    @Override
    protected void onServerErrorDialogShown() {
        mPM2Point5ValueTextView.setText(R.string.main_pm2point5_none);

        mArRemoteControlStatusTextList.clear();
        AutoVerticalRotationTextView.DisplayData displayData = new AutoVerticalRotationTextView.DisplayData();
        mARRemoteControlBrickLightBarView.setBackgroundColor(getResources().getColor(R.color.mainActivityLightBarColorTransparent));
        mArRemoteControlStatusTextList.add(getString(R.string.main_ar_remote_control_status_door_unknown));
        displayData.mTextList.addAll(mArRemoteControlStatusTextList);
        displayData.mDrawableList.add(getDrawable(R.drawable.ic_lock));
        mARRemoteControlStatusTextView.setDisplayData(displayData);

        mCarStatusLightBarView.setBackgroundColor(getResources().getColor(R.color.mainActivityLightBarColorTransparent));
        mCarStatusTextView.setText(R.string.main_car_status_none_2);

        mAvailableTripStatusTextView.setText(R.string.main_car_status_available_trip_none);

        Prefs.setCarStatusLastUpdateTime(MainActivity.this, "");
        mLastUpdateTimeTextView.setText(R.string.main_car_status_last_update_time_none);

        updateDisplayRelatedToSpecialist(true);
        mHasUnreadPushMessageNotificationBeenShownInSpecialistIntroduction = false;
    }

    private void init() {
        mFunctionDeque.offer(FUNC_ID_SHOW_PROGRESS_DIALOG);
        mFunctionDeque.offer(FUNC_ID_CHECK_APP_UPDATE);
        mFunctionDeque.offer(FUNC_ID_GET_USER_INFO);
        if (NetUtils.isConnectToInternet(this)) {
            mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_IKEY_UID);
            mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_INITIAL_DATA);
            mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_PUSH_LIST);
            mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_ACTIVITY_NEWS);
        }
        mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_KEY_LIST);
        mFunctionDeque.offer(FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA);
        mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_EMPLOYEE_DATA);
        mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_STATUS);
        mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_PM25);
        mFunctionDeque.offer(FUNC_ID_SET_INIT_DONE);
        mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
        mFunctionDeque.offer(FUNC_ID_SEND_BACKGROUND_REFRESH_MESSAGE);
        pollAndRunFunctionInDeque();
    }

    private void buildCarKeyDataList(ArrayList<CarKeyData> carKeyDataList) {
        mNumOfCarKeyData = carKeyDataList.size();

        if (carKeyDataList != null && carKeyDataList.size() > 0) {
            ArrayList<CarKeyData> tempCarKeyDataArrayList = new ArrayList<>();
            MainCarKeyListAdapter.LabelData labelData;
            int theLastItemPosition = 0;

            mViewModel.checkKeyInfoListFromDb(carKeyDataList);

            // Find master keys and build master keys data into mMainCarKeyListAdapter
            for (int index = 0; index < carKeyDataList.size(); index++) {
                CarKeyData carKeyData = carKeyDataList.get(index);
                if (carKeyData.isMasterKey()) {
                    carKeyData.setForceToBeMasterKey(false);
                    tempCarKeyDataArrayList.add(carKeyData);
                }
            }

            // Copy sub/snap key to be a fake master key
            boolean isRepeated = false;
            for (int index = 0; index < carKeyDataList.size(); index++) {
                CarKeyData carKeyData = carKeyDataList.get(index);
                if ((carKeyData.isDriver() || carKeyData.isOwner()) &&
                        (carKeyData.isSubKey() || carKeyData.isSnapKey())) {
                    isRepeated = false;
                    for (int index2 = 0; index2 < tempCarKeyDataArrayList.size(); index2++) {
                        CarKeyData tempCarKeyData = tempCarKeyDataArrayList.get(index2);
                        if (tempCarKeyData.getCarNo().equals(carKeyData.getCarNo())) {
                            isRepeated = true;
                            break;
                        }
                    }

                    if (!isRepeated) {
                        try {
                            CarKeyData copiedCarKeyData = (CarKeyData) ObjectSerializer.deserialize(ObjectSerializer.serialize(carKeyData));
                            copiedCarKeyData.setForceToBeMasterKey(true);
                            tempCarKeyDataArrayList.add(copiedCarKeyData);
                            mNumOfCarKeyData++;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            labelData = new MainCarKeyListAdapter.LabelData();
            labelData.setLabel(getString(R.string.main_master_key_label));
            theLastItemPosition = theLastItemPosition + tempCarKeyDataArrayList.size();
            labelData.setTheLastChildItemPosition(theLastItemPosition);
            mMainCarKeyListAdapter.buildItemData(labelData, null,
                    MainCarKeyListAdapter.MAIN_CAR_KEY_LIST_ITEM_TYPE_LABEL, false);

            for (int index = 0; index < tempCarKeyDataArrayList.size(); index++) {
                CarKeyData carKeyData = tempCarKeyDataArrayList.get(index);
                MainCarKeyListAdapter.ExtraData extraData = new MainCarKeyListAdapter.ExtraData();
                mMainCarKeyListAdapter.buildItemData(carKeyData, extraData,
                        MainCarKeyListAdapter.MAIN_CAR_KEY_LIST_ITEM_TYPE_CAR_KEY, true);
            }

            tempCarKeyDataArrayList.clear();

            // Find sub keys and build sub keys data into mMainCarKeyListAdapter
            for (int index = 0; index < carKeyDataList.size(); index++) {
                CarKeyData carKeyData = carKeyDataList.get(index);
                if (carKeyData.isSubKey()) {
                    carKeyData.setForceToBeMasterKey(false);
                    tempCarKeyDataArrayList.add(carKeyData);
                }
            }

            theLastItemPosition++;
            labelData = new MainCarKeyListAdapter.LabelData();
            labelData.setLabel(getString(R.string.main_sub_key_label));
            theLastItemPosition = theLastItemPosition + tempCarKeyDataArrayList.size();
            labelData.setTheLastChildItemPosition(theLastItemPosition);
            mMainCarKeyListAdapter.buildItemData(labelData, null,
                    MainCarKeyListAdapter.MAIN_CAR_KEY_LIST_ITEM_TYPE_LABEL, false);

            for (int index = 0; index < tempCarKeyDataArrayList.size(); index++) {
                CarKeyData carKeyData = tempCarKeyDataArrayList.get(index);
                MainCarKeyListAdapter.ExtraData extraData = new MainCarKeyListAdapter.ExtraData();
                mMainCarKeyListAdapter.buildItemData(carKeyData, extraData,
                        MainCarKeyListAdapter.MAIN_CAR_KEY_LIST_ITEM_TYPE_CAR_KEY, true);
            }

            tempCarKeyDataArrayList.clear();

            // Find temp keys and build temp keys data into mMainCarKeyListAdapter
            for (int index = 0; index < carKeyDataList.size(); index++) {
                CarKeyData carKeyData = carKeyDataList.get(index);
                if (carKeyData.isSnapKey()) {
                    carKeyData.setForceToBeMasterKey(false);
                    tempCarKeyDataArrayList.add(carKeyData);
                }
            }

            theLastItemPosition++;
            labelData = new MainCarKeyListAdapter.LabelData();
            labelData.setLabel(getString(R.string.main_temporary_key_label));
            theLastItemPosition = theLastItemPosition + tempCarKeyDataArrayList.size();
            labelData.setTheLastChildItemPosition(theLastItemPosition);
            mMainCarKeyListAdapter.buildItemData(labelData, null,
                    MainCarKeyListAdapter.MAIN_CAR_KEY_LIST_ITEM_TYPE_LABEL, false);

            for (int index = 0; index < tempCarKeyDataArrayList.size(); index++) {
                CarKeyData carKeyData = tempCarKeyDataArrayList.get(index);
                MainCarKeyListAdapter.ExtraData extraData = new MainCarKeyListAdapter.ExtraData();
                mMainCarKeyListAdapter.buildItemData(carKeyData, extraData,
                        MainCarKeyListAdapter.MAIN_CAR_KEY_LIST_ITEM_TYPE_CAR_KEY, true);
            }

            tempCarKeyDataArrayList.clear();

            // Find other keys and build other keys data into mMainCarKeyListAdapter
            for (int index = 0; index < carKeyDataList.size(); index++) {
                CarKeyData carKeyData = carKeyDataList.get(index);
                if (!carKeyData.isVig()) {
                    carKeyData.setForceToBeMasterKey(false);
                    tempCarKeyDataArrayList.add(carKeyData);
                }
            }

            theLastItemPosition++;
            labelData = new MainCarKeyListAdapter.LabelData();
            labelData.setLabel(getString(R.string.main_other_key_label));
            theLastItemPosition = theLastItemPosition + tempCarKeyDataArrayList.size();
            labelData.setTheLastChildItemPosition(theLastItemPosition);
            mMainCarKeyListAdapter.buildItemData(labelData, null,
                    MainCarKeyListAdapter.MAIN_CAR_KEY_LIST_ITEM_TYPE_LABEL, false);

            for (int index = 0; index < tempCarKeyDataArrayList.size(); index++) {
                CarKeyData carKeyData = tempCarKeyDataArrayList.get(index);
                MainCarKeyListAdapter.ExtraData extraData = new MainCarKeyListAdapter.ExtraData();
                mMainCarKeyListAdapter.buildItemData(carKeyData, extraData,
                        MainCarKeyListAdapter.MAIN_CAR_KEY_LIST_ITEM_TYPE_CAR_KEY, true);
            }

            tempCarKeyDataArrayList.clear();

            theLastItemPosition++;
            mMainCarKeyListAdapter.buildItemData(null, null,
                    MainCarKeyListAdapter.MAIN_CAR_KEY_LIST_ITEM_TYPE_ADD_NEW_CAR, true);

            mMainCarKeyListAdapter.commitBuildItemData();

            // 選定車輛
            mSelectedCarKeyData = Prefs.getCarKeyData(MainActivity.this);
            if (mSelectedCarKeyData == null) {
                mSelectedCarKeyData = Prefs.getCarKeyDataByIKeyUid(MainActivity.this, mUserInfo.getKeyUid());
            }
            if (mSelectedCarKeyData != null) {
                boolean isSelectedCarKeyDataInList = false;
                for (int position = 0; position < mMainCarKeyListAdapter.getItemCount(); position++) {
                    CustomRecyclerViewBaseAdapter.RecyclerViewItemData itemData = mMainCarKeyListAdapter.getItem(position);
                    if (itemData.getItemType() == MainCarKeyListAdapter.MAIN_CAR_KEY_LIST_ITEM_TYPE_CAR_KEY) {
                        CarKeyData carKeyData = (CarKeyData) itemData.getDataObject();
                        if (carKeyData.isMasterKey()) {
                            if (mSelectedCarKeyData.getCarId().equals(carKeyData.getCarId()) &&
                                    mSelectedCarKeyData.getForceToBeMasterKey() == carKeyData.getForceToBeMasterKey()) {
                                mSelectedCarKeyData = carKeyData;
                                Prefs.setCarKeyData(this, mSelectedCarKeyData);
                                Prefs.setCarKeyDataWithIKeyUid(MainActivity.this, mUserInfo.getKeyUid(), mSelectedCarKeyData);
                                mMainCarKeyListAdapter.setSelectedItemPosition(position);
                                isSelectedCarKeyDataInList = true;
                                break;
                            }
                        } else if (!carKeyData.isVig()) { // UIO doesn't need to check device id.
                            if (mSelectedCarKeyData.getCarId().equals(carKeyData.getCarId()) &&
                                    mSelectedCarKeyData.getForceToBeMasterKey() == carKeyData.getForceToBeMasterKey()) {
                                mSelectedCarKeyData = carKeyData;
                                Prefs.setCarKeyData(this, mSelectedCarKeyData);
                                Prefs.setCarKeyDataWithIKeyUid(MainActivity.this, mUserInfo.getKeyUid(), mSelectedCarKeyData);
                                mMainCarKeyListAdapter.setSelectedItemPosition(position);
                                isSelectedCarKeyDataInList = true;
                                break;
                            }
                        } else {
                            if (mSelectedCarKeyData.getCarId().equals(carKeyData.getCarId()) &&
                                    mSelectedCarKeyData.getPhoneId().equals(mUserInfo.getDeviceId()) &&
                                    mSelectedCarKeyData.getForceToBeMasterKey() == carKeyData.getForceToBeMasterKey()) {
                                mSelectedCarKeyData = carKeyData;
                                Prefs.setCarKeyData(this, mSelectedCarKeyData);
                                Prefs.setCarKeyDataWithIKeyUid(MainActivity.this, mUserInfo.getKeyUid(), mSelectedCarKeyData);
                                mMainCarKeyListAdapter.setSelectedItemPosition(position);
                                isSelectedCarKeyDataInList = true;
                                break;
                            }
                        }
                    }
                }

                if (isSelectedCarKeyDataInList) {
/*
                    if (BeaconOptions.LEARNT.equals(mSelectedCarKeyData.getBeaconLearningStatus())) {
                        String uuid = Prefs.getBeaconUUID(this);
                        Beacon LBeacon = mSelectedCarKeyData.getLeftBeacon(uuid);
                        if (LBeacon != null) {
                            Prefs.setBeacon(this, BeaconOptions.LEFT_FRONT, LBeacon, mSelectedCarKeyData.getVigCarId());
                        }
                        Beacon BBeacon = mSelectedCarKeyData.getBackBeacon(uuid);
                        if (BBeacon != null) {
                            Prefs.setBeacon(this, BeaconOptions.TAILGATE, BBeacon, mSelectedCarKeyData.getVigCarId());
                        }
                        Beacon RBeacon = mSelectedCarKeyData.getRightBeacon(uuid);
                        if (RBeacon != null) {
                            Prefs.setBeacon(this, BeaconOptions.RIGHT_FRONT, RBeacon, mSelectedCarKeyData.getVigCarId());
                        }
                        Intent intent = new Intent(this, CommunicationAgentService.class);
                        intent.putExtra(CommunicationAgentService.VIG_SCAN_BEACONS, true);

                        startService(intent);
                    }
*/
                } else {
                    mSelectedCarKeyData = null;
                    mSelectedCarStatusData = null;
                    Prefs.clearCarKeyData(this);
                    Prefs.clearCarKeyDataWithIKeyUid(this, mUserInfo.getKeyUid());
                }
            }

            mMainCarKeyListAdapter.notifyDataSetChanged();

            pollAndRunFunctionInDeque();
        } else {
            mSelectedCarKeyData = null;
            mSelectedCarStatusData = null;
            Prefs.clearCarKeyData(MainActivity.this);
            Prefs.clearCarKeyDataWithIKeyUid(MainActivity.this, mUserInfo.getKeyUid());
            pollAndRunFunctionInDeque();
        }

        Prefs.setCurrentNumOfCarKeyData(this, mNumOfCarKeyData);
    }

    private void checkCarStatus() {
        if (mCheckCarStatusAsyncTask == null) {
            mCheckCarStatusAsyncTask = new CheckCarStatusAsyncTask();
            mCheckCarStatusAsyncTask.execute();
        }
    }

    private void clearLocalData() {
        mViewModel.logout();
        Prefs.clear(MainActivity.this, Prefs.PREFS_ALL);

        Intent intent = new Intent(MainActivity.this, CommunicationAgentService.class);
        intent.putExtra(CA_EXTRA_STOP, true);
        startService(intent);
    }

    private void updateDisplayRelatedToUserInfo() {
        if (mUserInfo != null) {
            if (TextUtils.isEmpty(mUserName)) {
                mUserName = mUserInfo.getEmail();
                mUserName = mUserName.substring(0, mUserName.indexOf("@"));
            }
            mSMenuUserNameTextView.setText(mUserName);
        }
    }

    private void updateDisplayRelatedToPushList(ArrayList<PushData> pushDataArrayList) {
        boolean hasUnread = false;
        PushData data = mViewModel.hasUnReadPushMessage(pushDataArrayList);

        if (data != null) {
            hasUnread = true;
            String unreadPushMessagesNotifcation = getString(R.string.main_specialist_indroduction_has_unread_push_message);
            unreadPushMessagesNotifcation = String.format(unreadPushMessagesNotifcation, data.getProvider());
            AutoVerticalRotationTextView.DisplayData displayData = new AutoVerticalRotationTextView.DisplayData();
            displayData.mTextList.add(unreadPushMessagesNotifcation);
            mCanMessageTextView.setDisplayData(displayData);
            mCanMessageTextView.startAutoScroll();
            mHasUnreadPushMessageNotificationBeenShownInSpecialistIntroduction = true;
        }

        if (hasUnread) {
            mActionBarNotificationCenterImageView.setImageDrawable(mNotificationCenterAlertDrawable);
        } else {
            mActionBarNotificationCenterImageView.setImageDrawable(mNotificationCenterDrawable);
        }

        if (mCommunicationAgentService != null) {
            mCommunicationAgentService.syncPushMessage(pushDataArrayList);
        }
    }

    private void updateDisplayRelatedToSelectedCarKeyData() {
        if (mSelectedCarKeyData != null) {
            String carCodeString = getString(R.string.main_car_code);
            carCodeString = String.format(carCodeString, mSelectedCarKeyData.getVehicleNumber());
            mSMenuCarCodeTextView.setText(carCodeString);
            mSMenuPlateNumberTextView.setText(mSelectedCarKeyData.getCarNo());
            mSMenuCarModelTextView.setText(mSelectedCarKeyData.getCarModel());
        } else {
            mSMenuCarCodeTextView.setText(R.string.main_car_car_code_none);
            mSMenuPlateNumberTextView.setText(R.string.common_car_plate_number_none);
            mSMenuCarModelTextView.setText(R.string.common_car_model_none);
        }
    }

    private void updateDisplayRelatedToSelectedCarStatusData() {

    }

    private void resetDisplayRelatedToSelectedCarKeyDataAndCarStatusData() {
        mSMenuCarModelTextView.setText(R.string.common_car_model_none);
        mSMenuCarCodeTextView.setText(R.string.main_car_car_code_none);
        mSMenuPlateNumberTextView.setText(R.string.common_car_plate_number_none);

        mCarStatusTextView.setText(mSpaceString);
        mLastUpdateTimeTextView.setText(mSpaceString);
        AutoVerticalRotationTextView.DisplayData displayData = new AutoVerticalRotationTextView.DisplayData();
        displayData.mTextList.add("");
        mARRemoteControlStatusTextView.setDisplayData(displayData);
        mPM2Point5ValueTextView.setText(mSpaceString);
    }

    private void updateDisplayRelatedToActivityNews(ArrayList<CampaignData> activityNewsArrayList) {
        ArrayList<View> viewArrayList = new ArrayList<>();
        mActivityNewsViewPagerIndicatorLayout.removeAllViews();
        if (mCurrentViewPagerPosition >= activityNewsArrayList.size() || mCurrentViewPagerPosition < 0) {
            mCurrentViewPagerPosition = 0;
        }

        if (activityNewsArrayList.size() > 0) {
            String subtitleStringFormat = getString(R.string.main_activity_news_subtitle_format);
            int maxSize = activityNewsArrayList.size() > CommonDefs.MAIN_ACTIVITY_NEWS_MAX_COUNT ? CommonDefs.MAIN_ACTIVITY_NEWS_MAX_COUNT : activityNewsArrayList.size();
            for (int index = 0; index < maxSize; index++) {
                CampaignData campaignData = activityNewsArrayList.get(index);
                View view = getLayoutInflater().inflate(R.layout.viewpager_item_activity_news, null);

                ImageView bannerImageView = view.findViewById(R.id.viewpager_item_activity_news_banner_image_view);
                ImageView bannerMaskImageView = view.findViewById(R.id.viewpager_item_activity_news_banner_mask_image_view);
                bannerMaskImageView.setOnClickListener(mOnBannerImageViewClickListener);
                TextView titleTextView = view.findViewById(R.id.viewpager_item_activity_news_title_text_view);
                TextView subtitleTextView = view.findViewById(R.id.viewpager_item_activity_news_subtitle_text_view);

                String bannerUrlString = campaignData.getBannerUrl();
                if (bannerUrlString != null) {
                    if (!mStopGlide) {
                        Glide.with(this).load(bannerUrlString).into(bannerImageView);
                    }
                }
                bannerMaskImageView.setTag(campaignData.getUrl());
                titleTextView.setText(campaignData.getTitle());
                String beginDateString = campaignData.getBeginDate().substring(0, 10);
                String endDateString = campaignData.getEndDate().substring(0, 10);
                String subtitleString = String.format(subtitleStringFormat, beginDateString, endDateString);
                subtitleTextView.setText(subtitleString);
                subtitleTextView.setVisibility(View.INVISIBLE);

                viewArrayList.add(view);

                if (index == mCurrentViewPagerPosition) {
                    addViewPagerIndicator(true);
                } else {
                    addViewPagerIndicator(false);
                }
            }

            ActivityNewsPagerAdapter activityNewsPagerAdapter = new ActivityNewsPagerAdapter(viewArrayList);
            mActivityNewsViewPager.setAdapter(activityNewsPagerAdapter);
            mActivityNewsViewPager.setCurrentItem(mCurrentViewPagerPosition, false);

        } else {
            View view = getLayoutInflater().inflate(R.layout.viewpager_item_activity_news, null);
            TextView titleTextView = view.findViewById(R.id.viewpager_item_activity_news_title_text_view);
            titleTextView.setText(R.string.main_activity_news_unready);

            viewArrayList.add(view);

            addViewPagerIndicator(true);

            ActivityNewsPagerAdapter activityNewsPagerAdapter = new ActivityNewsPagerAdapter(viewArrayList);
            mActivityNewsViewPager.setAdapter(activityNewsPagerAdapter);
        }

        if (activityNewsArrayList.size() < 2) {
            mActivityNewsViewPagerIndicatorLayout.setVisibility(View.INVISIBLE);
        } else {
            mActivityNewsViewPagerIndicatorLayout.setVisibility(View.VISIBLE);
        }
    }

    private void updateDisplayRelatedToSpecialist(boolean forceUpdate) {
        boolean isEmployeeNameChanged = false;
        String lastEmployeeName = mAdvisorTextView.getText().toString().trim();
        isEmployeeNameChanged = !lastEmployeeName.equals(mEmployeeName);
        mAdvisorTextView.setText(mEmployeeName);

        if (mCannedMessages != null && mCannedMessages.length > 0) {
            if (!mIsInitDone || forceUpdate) {
                AutoVerticalRotationTextView.DisplayData displayData = new AutoVerticalRotationTextView.DisplayData();
                displayData.mTextList.addAll(Arrays.asList(mCannedMessages));
                mCanMessageTextView.setDisplayData(displayData);

                mCanMessageTextView.startAutoScroll();
            }

        } else {
            if (!mIsInitDone || forceUpdate || isEmployeeNameChanged) {
                String specialistIntroduction = getString(R.string.main_specialist_indroduction_default);
                specialistIntroduction = String.format(specialistIntroduction, mEmployeeName);
                AutoVerticalRotationTextView.DisplayData displayData = new AutoVerticalRotationTextView.DisplayData();
                displayData.mTextList.add(specialistIntroduction);
                mCanMessageTextView.setDisplayData(displayData);

                mCanMessageTextView.startAutoScroll();
            }
        }
    }

    private void addViewPagerIndicator(boolean isEnabled) {
        View indicatorView = new View(this);
        indicatorView.setBackgroundResource(R.drawable.selector_viewpager_indicator);
        indicatorView.setEnabled(isEnabled);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) Utils.convertDpToPixel(5),
                (int) Utils.convertDpToPixel(5));

        layoutParams.setMarginStart((int) Utils.convertDpToPixel(3));
        layoutParams.setMarginEnd((int) Utils.convertDpToPixel(3));
        mActivityNewsViewPagerIndicatorLayout.addView(indicatorView, layoutParams);
    }

    private void switchCarKeyListDisplay() {
        if (mIsCarKeyListOpened) {
            mSMenuUserNameTextView.setCompoundDrawables(null, null, mArrowDownDrawable, null);
            mSlidingmenuCarKeyListRecyclerView.setVisibility(View.GONE);
            mSlidingmenuMenuScrollView.setVisibility(View.VISIBLE);
            mIsCarKeyListOpened = false;
        } else {
            mSMenuUserNameTextView.setCompoundDrawables(null, null, mArrowUpDrawable, null);
            mSlidingmenuCarKeyListRecyclerView.setVisibility(View.VISIBLE);
            mSlidingmenuMenuScrollView.setVisibility(View.GONE);
            mIsCarKeyListOpened = true;
        }
    }

    private void setUIDisplayByUserAccountType() {
        switch (mCurrentUserAccountType) {
            case USER_ACCOUNT_TYPE_NORMAL: {
                mSMenuCarModelTextView.setVisibility(View.GONE);
                mSMenuPlateNumberTextView.setVisibility(View.GONE);
                mSMenuAddNewCarTextView.setVisibility(View.VISIBLE);
                mSMenuStolenCarSpeedLimitTextView.setVisibility(View.GONE);
                mSMenuARRemoteControlTextView.setVisibility(View.GONE);
                mSMenuCarStatusReviewTextView.setVisibility(View.GONE);
                mSMenuOneKeyWaterMarkTextView.setVisibility(View.GONE);
                mSMenuCarBonusTextView.setVisibility(View.GONE);
                mSMenuDigitalKeyTextView.setVisibility(View.GONE);

                mARRemoteControlBlock.setVisibility(View.GONE);
                mCarStatusBlock.setVisibility(View.GONE);
                mMaintenanceBrick.setVisibility(View.VISIBLE);
                mLastUpdateTimeTextView.setVisibility(View.GONE);
                mCanMessageBrick.setVisibility(View.GONE);

                mMainCarKeyListAdapter.enableAddNewCarFunction(false);

                break;
            }

            case USER_ACCOUNT_TYPE_UIO: {
                mSMenuCarModelTextView.setVisibility(View.VISIBLE);
                mSMenuPlateNumberTextView.setVisibility(View.VISIBLE);
                mSMenuAddNewCarTextView.setVisibility(View.GONE);
                mSMenuStolenCarSpeedLimitTextView.setVisibility(View.GONE);
                mSMenuARRemoteControlTextView.setVisibility(View.GONE);
                mSMenuCarStatusReviewTextView.setVisibility(View.GONE);
                mSMenuOneKeyWaterMarkTextView.setVisibility(View.VISIBLE);
                if (mSelectedCarKeyData != null && (mSelectedCarKeyData.isDriver() || mSelectedCarKeyData.isOwner())) {
                    mSMenuCarBonusTextView.setVisibility(View.VISIBLE);
                } else {
                    mSMenuCarBonusTextView.setVisibility(View.GONE);
                }
                mSMenuDigitalKeyTextView.setVisibility(View.GONE);

                mARRemoteControlBlock.setVisibility(View.GONE);
                mCarStatusBlock.setVisibility(View.GONE);
                mMaintenanceBrick.setVisibility(View.VISIBLE);
                mLastUpdateTimeTextView.setVisibility(View.GONE);
                mCanMessageBrick.setVisibility(View.VISIBLE);

                mMainCarKeyListAdapter.enableAddNewCarFunction(false);

                break;
            }

            case USER_ACCOUNT_TYPE_VIG: {
                mSMenuCarModelTextView.setVisibility(View.VISIBLE);
                mSMenuPlateNumberTextView.setVisibility(View.VISIBLE);
                mSMenuAddNewCarTextView.setVisibility(View.GONE);
                mSMenuStolenCarSpeedLimitTextView.setVisibility(View.GONE);
                mSMenuARRemoteControlTextView.setVisibility(View.VISIBLE);
                mSMenuCarStatusReviewTextView.setVisibility(View.VISIBLE);
                mSMenuOneKeyWaterMarkTextView.setVisibility(View.VISIBLE);
                if (mSelectedCarKeyData != null && (mSelectedCarKeyData.isDriver() || mSelectedCarKeyData.isOwner())) {
                    mSMenuCarBonusTextView.setVisibility(View.VISIBLE);
                } else {
                    mSMenuCarBonusTextView.setVisibility(View.GONE);
                }
                mSMenuDigitalKeyTextView.setVisibility(View.VISIBLE);

                mARRemoteControlBlock.setVisibility(View.VISIBLE);
                mCarStatusBlock.setVisibility(View.VISIBLE);
                mMaintenanceBrick.setVisibility(View.GONE);
                mLastUpdateTimeTextView.setVisibility(View.VISIBLE);
                mCanMessageBrick.setVisibility(View.VISIBLE);

                if (mUserInfo != null && !mUserInfo.isDMS()) {
                    mMainCarKeyListAdapter.enableAddNewCarFunction(true);
                }

                break;
            }

            default:
        }
    }

    private void refreshCarKeyList() {
        if (mFunctionDeque.isEmpty()) {
            mFunctionDeque.offer(FUNC_ID_SHOW_PROGRESS_DIALOG);
            mFunctionDeque.offer(FUNC_ID_GET_USER_INFO);
            mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_KEY_LIST);
            mFunctionDeque.offer(FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA);
            mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_EMPLOYEE_DATA);
            mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_STATUS);
            mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_PM25);
            mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
            pollAndRunFunctionInDeque();
        } else {
            mFunctionDeque.offer(FUNC_ID_SHOW_PROGRESS_DIALOG);
            mFunctionDeque.offer(FUNC_ID_GET_USER_INFO);
            mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_KEY_LIST);
            mFunctionDeque.offer(FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA);
            mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_EMPLOYEE_DATA);
            mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_CAR_STATUS);
            mFunctionDeque.offer(FUNC_ID_CALL_API_TO_GET_PM25);
            mFunctionDeque.offer(FUNC_ID_DISMISS_PROGRESS_DIALOG);
        }
    }

    private boolean isSelectedCarKeyDataActive() {
        boolean isSelectedCarKeyDataActive = false;
        int selectedCarKeyDataPosition = mMainCarKeyListAdapter.getSelectedItemPosition();
        if (selectedCarKeyDataPosition > -1 && selectedCarKeyDataPosition < mMainCarKeyListAdapter.getItemCount()) {
            isSelectedCarKeyDataActive = ((MainCarKeyListAdapter.ExtraData) mMainCarKeyListAdapter.getItem(selectedCarKeyDataPosition).getExtraDataObject()).isCarKeyActive();
        }

        return isSelectedCarKeyDataActive;
    }

    private void showInactiveMasterKeyAlertDialog() {
        if (mAlertDialog1.isShowing()) {
            mAlertDialog1.dismiss();
        }

        mAlertDialog1.restoreDefault();
        mAlertDialog1.setTitle(R.string.main_dialog_master_key_is_inactive_title);
        mAlertDialog1.setMessage(getString(R.string.main_dialog_master_key_is_inactive_message));
        mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                getString(R.string.btn_confirm),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mAlertDialog1.dismiss();
                    }
                });
        mAlertDialog1.show();
    }

    private void translateCountry(Location location) {
        Intent intent = new Intent(this, GeocodeCountryIntentService.class);
        intent.putExtra(GeocodeCountryIntentService.RECEIVER, mCountryResultReceiver);
        intent.putExtra(GeocodeCountryIntentService.LOCATION_DATA_EXTRA, location);
        startService(intent);
    }

    private void pollAndRunFunctionInDeque() {
        Integer functionId = mFunctionDeque.poll();
        if (functionId != null) {
            executeFunction(functionId);
        }
    }

    private void clearFunctionQueue() {
        boolean needToDismissProgressDialog = false;
        boolean needToSendBackgroundRefreshMessage = false;
        boolean needToSetInitDone = false;
        Integer functionId;

        while ((functionId = mFunctionDeque.poll()) != null) {
            switch (functionId) {
                case FUNC_ID_DISMISS_PROGRESS_DIALOG: {
                    needToDismissProgressDialog = true;
                    break;
                }

                case FUNC_ID_SEND_BACKGROUND_REFRESH_MESSAGE: {
                    needToSendBackgroundRefreshMessage = true;
                    break;
                }

                case FUNC_ID_SET_INIT_DONE: {
                    needToSetInitDone = true;
                }
            }
        }

        if (needToDismissProgressDialog) {
            executeFunction(FUNC_ID_DISMISS_PROGRESS_DIALOG);
        }

        if (needToSendBackgroundRefreshMessage) {
            executeFunction(FUNC_ID_SEND_BACKGROUND_REFRESH_MESSAGE);
        }

        if (needToSetInitDone) {
            executeFunction(FUNC_ID_SET_INIT_DONE);
        }
    }

    private void executeFunction(int functionId) {
        switch (functionId) {
            case FUNC_ID_SHOW_PROGRESS_DIALOG: {
                showProgressDialog();
                pollAndRunFunctionInDeque();
                break;
            }

            case FUNC_ID_DISMISS_PROGRESS_DIALOG: {
                dismissProgressDialog();
                pollAndRunFunctionInDeque();
                break;
            }

            case FUNC_ID_SEND_BACKGROUND_REFRESH_MESSAGE: {
                mMessageHandler.sendEmptyMessageDelayed(CommonDefs.INNER_MSG_WHAT_REFRESH_IN_BACKGROUND,
                        CommonDefs.MAIN_REFRESH_IN_BACKGROUND_DELAY_TIME);
                pollAndRunFunctionInDeque();
                break;
            }

            case FUNC_ID_REMOVE_BACKGROUND_REFRESH_MESSAGE: {
                mMessageHandler.removeMessages(CommonDefs.INNER_MSG_WHAT_REFRESH_IN_BACKGROUND);
                pollAndRunFunctionInDeque();
                break;
            }

            case FUNC_ID_CHECK_APP_UPDATE: {
                PlayStoreChecker playStoreChecker = new PlayStoreChecker(this, new PlayStoreChecker.CheckResultListener() {
                    @Override
                    public void noNeedToUpdate() {
                        pollAndRunFunctionInDeque();
                    }
                });
                playStoreChecker.start();

                break;
            }

            case FUNC_ID_GET_USER_INFO: {
                if (mGetUserInfoAsyncTask == null) {
                    mGetUserInfoAsyncTask = new GetUserInfoAsyncTask();
                    mGetUserInfoAsyncTask.execute();
                }

                break;
            }

            case FUNC_ID_CALL_API_TO_GET_IKEY_UID: {
                if (mUserInfo != null) {
                    if (mUserInfo.isDMS()) {
                        GetIkeyUidRequestData data = new GetIkeyUidRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken());

                        UserWebApi userWebApi = UserWebApi.getInstance(this);
                        userWebApi.getIkeyUid(data, mGetIkeyUidApiListener);
                    } else {
                        updateDisplayRelatedToUserInfo();
                        pollAndRunFunctionInDeque();
                    }

                } else {
                    pollAndRunFunctionInDeque();
                }

                break;
            }

            case FUNC_ID_CALL_API_TO_GET_INITIAL_DATA: {
                AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(this);
                assistantWebApi.getInitialData(mGetInitialDataApiListener);

                break;
            }

            case FUNC_ID_CALL_API_TO_GET_PUSH_LIST: {
                if (mUserInfo != null) {
                    GetPushRequestData data = new GetPushRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken());

                    AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(this);
                    assistantWebApi.getPushList(data, mGetPushListApiListener);
                } else {
                    pollAndRunFunctionInDeque();
                }

                break;
            }

            case FUNC_ID_CALL_API_TO_GET_ACTIVITY_NEWS: {
                AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(this);
                assistantWebApi.getCampaign(mGetActivityNewsApiListener);

                break;
            }

            case FUNC_ID_CALL_API_TO_GET_CAR_KEY_LIST: {
                if (mUserInfo != null) {
                    if (mIsCarKeyListOpened) {
                        // switchCarKeyListDisplay();
                    }

                    GetCarKeyRequestData data = new GetCarKeyRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken());

                    CarWebApi carWebApi = CarWebApi.getInstance(this);
                    carWebApi.getCarKeyList(data, mGetCarKeyListApiListener);
                } else {
                    pollAndRunFunctionInDeque();
                }

                break;
            }

            case FUNC_ID_CHECK_ALL_KEYS_ACTIVATION_STATUS_AND_SELECT_CAR_KEY_DATA: {
                if (mUserInfo != null) {
                    if (mCheckAllKeysActivationStatusAndSelectCarKeyDataAsyncTask == null) {
                        mCheckAllKeysActivationStatusAndSelectCarKeyDataAsyncTask = new CheckAllKeysActivationStatusAndSelectCarKeyDataAsyncTask();
                        mCheckAllKeysActivationStatusAndSelectCarKeyDataAsyncTask.execute();
                    }
                } else {
                    pollAndRunFunctionInDeque();
                }

                break;
            }

            case FUNC_ID_CALL_API_TO_DO_IKEY_LOGIN: {
                if (mUserInfo != null) {
                    Intent intent = new Intent(this, CommunicationAgentService.class);
                    intent.putExtra(CommunicationAgentService.CA_EXTRA_IKEY_LOGIN, mUserInfo.getKeyUid());
                    startService(intent);
                } else {
                    pollAndRunFunctionInDeque();
                }
                break;
            }

            case FUNC_ID_CALL_API_TO_GET_KEY_CHAIN_LIST: {
                if (mUserInfo != null && mVigCarIdOfAbnormalCarKeyData != null) {
                    GetKeyChainListRequestData data = new GetKeyChainListRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken(), mVigCarIdOfAbnormalCarKeyData);

                    CarWebApi carWebApi = CarWebApi.getInstance(this);
                    carWebApi.getKeyChainList(data, mOnGetKeyChainListApiListener);
                } else {
                    pollAndRunFunctionInDeque();
                }
                break;
            }

            case FUNC_ID_CALL_API_TO_GET_EMPLOYEE_DATA: {
                if (mUserInfo != null && mSelectedCarKeyData != null &&
                        (mSelectedCarKeyData.isOwner() || mSelectedCarKeyData.isDriver())) {
                    GetIkeyEmployeeDataRequestData data = new GetIkeyEmployeeDataRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken());

                    UserWebApi userWebApi = UserWebApi.getInstance(this);
                    userWebApi.getIkeyEmployeeData(data, mOnGetEmployeeDataApiListener);
                } else {
                    updateDisplayRelatedToSpecialist(false);

                    pollAndRunFunctionInDeque();
                }
                break;
            }

            case FUNC_ID_CALL_API_TO_GET_CAR_STATUS: {
                if (mUserInfo != null && mSelectedCarKeyData != null && mCurrentUserAccountType == USER_ACCOUNT_TYPE_VIG) {
                    VigRequestData data = new VigRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken(), mSelectedCarKeyData.getCarId());

                    CarWebApi carWebApi = CarWebApi.getInstance(this);
                    carWebApi.getCarStatus(data, mGetCarStatusApiListener);
                } else {
                    pollAndRunFunctionInDeque();
                }

                break;
            }

            case FUNC_ID_CALL_API_TO_GET_PM25: {
                if (mUserInfo != null && mSelectedCarKeyData != null && mSelectedCarStatusData != null &&
                        mCurrentUserAccountType == USER_ACCOUNT_TYPE_VIG) {
                    VigRequestData data = new VigRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken(), mSelectedCarKeyData.getCarId());

                    AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(this);
                    assistantWebApi.getPm25(data, mGetPM25ApiListener);
                } else {
                    pollAndRunFunctionInDeque();
                }

                break;
            }

            case FUNC_ID_CALL_API_TO_GET_CAR_REPAIR_PROGRESS: {
                if (mUserInfo != null && mSelectedCarKeyData != null) {
                    GetCarRepairProgressRequestData data = new GetCarRepairProgressRequestData(
                            mUserInfo.getKeyUid(), mUserInfo.getToken(), mSelectedCarKeyData.getCarNo());

                    AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(this);
                    assistantWebApi.getCarRepairProgress(data, mOnGetCarRepairProgressApiListener);
                } else {
                    pollAndRunFunctionInDeque();
                }

                break;
            }

            case FUNC_ID_CALL_API_TO_GET_CAR_REPAIR_PLAN: {
                if (mUserInfo != null && mSelectedCarKeyData != null && !mHaveGottonCarRepairProgress) {
                    GetCarRepairPlanRequestData data = new GetCarRepairPlanRequestData(
                            mUserInfo.getKeyUid(), mUserInfo.getToken(), mSelectedCarKeyData.getCarNo());

                    AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(this);
                    assistantWebApi.getCarRepairPlan(data, mOnGetCarRepairPlanApiListener);
                } else {
                    pollAndRunFunctionInDeque();
                }
                mHaveGottonCarRepairProgress = false;

                break;
            }

            case FUNC_ID_CALL_API_TO_GET_SPEED_LIMIT_STATUS: {
                if (mUserInfo != null && mSelectedCarKeyData != null &&
                        mCurrentUserAccountType == USER_ACCOUNT_TYPE_VIG) {
                    GetSpeedLimitStatusRequestData data = new GetSpeedLimitStatusRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken());
                    data.setCarId(mSelectedCarKeyData.getVigCarId());

                    CarWebApi carWebApi = CarWebApi.getInstance(this);
                    carWebApi.getSpeedLimitStatus(data, mOnGetSpeedLimitStatusApiListener);
                } else {
                    mSMenuStolenCarSpeedLimitTextView.setVisibility(View.GONE);
                    mStolenCarSpeedLimitTextView.setVisibility(View.GONE);

                    pollAndRunFunctionInDeque();
                }

                break;
            }

            case FUNC_ID_SET_INIT_DONE: {
                mIsInitDone = true;
                pollAndRunFunctionInDeque();

                break;
            }

            case FUNC_ID_GET_GOOGLE_CALENDAR_EVENT_INDEPENDENTLY: {
                if (mGetGoogleCalendarEventAsyncTask == null) {
                    mGetGoogleCalendarEventAsyncTask = new GetGoogleCalendarEventAsyncTask();
                    mGetGoogleCalendarEventAsyncTask.execute();
                }

                break;
            }

            case FUNC_ID_CALL_API_TO_GET_WEATHER_INDEPENDENTLY: {
                if (mCountry != null) {
                    GetWeatherRequestData data = new GetWeatherRequestData(mCountry);

                    AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(this);
                    assistantWebApi.getWeather(data, mGetWeatherApiListener);
                }

                break;
            }

            case FUNC_ID_CALL_API_TO_GET_PUSH_LIST_INDEPENDENTLY: {
                if (mUserInfo != null) {
                    GetPushRequestData data = new GetPushRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken());

                    AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(this);
                    assistantWebApi.getPushList(data, mGetPushListApiIndependentlyListener);
                }

                break;
            }

            case FUNC_ID_CALL_API_TO_LOGOUT: {
                if (mUserInfo != null) {
                    LogoutRequestData data = new LogoutRequestData(mUserInfo.getKeyUid());

                    UserWebApi userWebApi = UserWebApi.getInstance(this);
                    userWebApi.logout(data, mLogoutApiIndependentlyListener);
                } else {
                    pollAndRunFunctionInDeque();
                }

                break;
            }

            case FUNC_ID_MULTILOGIN_RELOGIN: {
                mIsLogoutInProgress = false;
                clearLocalData();

                startActivity(new Intent(MainActivity.this, SplashActivity.class));
                pollAndRunFunctionInDeque();
                finish();
                break;
            }

        }
    }

    /* Start: About FCM */
    private void setupFcm() {
        final boolean isFcmTokenEmpty = Prefs.isFcmTokenEmpty(this);

        if (Prefs.isNotificationEnable(this) && !isFcmTokenEmpty) {
            initFcm();
        } else if (!Prefs.isNotificationEnable(this)) {
            doNotificationEnable();
        } else if (isFcmTokenEmpty) {
            setFcmToken();
        }
    }

    private void doNotificationEnable() {
        if (mAlertDialog1.isShowing()) {
            mAlertDialog1.dismiss();
        }
        mAlertDialog1.restoreDefault();
        mAlertDialog1.setTitle(R.string.dialog_title_notification_access);
        mAlertDialog1.setMessage(getString(R.string.dialog_message_notification_access));
        mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                getString(R.string.btn_accept),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Prefs.setNotificationAccept(MainActivity.this, true);
                        setFcmToken();
                        mAlertDialog1.dismiss();
                    }
                });
        mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                getString(R.string.btn_reject),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Prefs.setNotificationAccept(MainActivity.this, false);
                        mAlertDialog1.dismiss();
                    }
                });
        mAlertDialog1.show();
    }

    /* Start: About FCM */

}
