package com.luxgen.remote.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.github.mikephil.charting.utils.Utils;
import com.luxgen.remote.R;
import com.luxgen.remote.ca.CommunicationAgentService;
import com.luxgen.remote.custom.CommonDefs;
import com.luxgen.remote.custom.CustomAppCompatActivity;
import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.model.KeyInfo;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.CarWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.UserWebApi;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.car.CheckPasswordRequestData;
import com.luxgen.remote.network.model.user.QueryUserRequestData;
import com.luxgen.remote.network.model.user.QueryUserResponseData;
import com.luxgen.remote.network.model.user.UserDeviceData;
import com.luxgen.remote.ui.viewmodel.AddNewSubKeyViewModel;
import com.luxgen.remote.util.LanguageUtils;
import com.luxgen.remote.util.Prefs;

import java.util.ArrayList;

public class AddNewSubKeyActivity extends CustomAppCompatActivity {

    public static Intent newIntent(Context context, int position) {
        Intent intent = new Intent(context, AddNewSubKeyActivity.class);
        intent.putExtra("position", position);

        return intent;
    }

    private final int STEP_1 = 0;
    private final int STEP_2 = 1;

    private GetUserInfoAsyncTask mGetUserInfoAsyncTask = null;
    private GetSelectedCarKeyDataAsyncTask mGetSelectedCarKeyDataAsyncTask = null;

    private UserInfo mUserInfo = null;
    private CarKeyData mSelectedCarKeyData = null;
    private AddNewSubKeyViewModel mViewModel = null;
    private int mKeyPosition = -1;

    // Input Password
    private ViewGroup mInputPasswordLayout = null;
    private EditText mPasswordEditText = null;
    private Button mPasswordConfirmButton = null;

    // Input Owner Info
    private ViewGroup mInputOwnerInfoLayout = null;
    private EditText mOwnerPhoneEditText = null;
    private EditText mOwnerNameEditText = null;
    private Button mOwnerInfoNextButton = null;

    private RadioGroup mUserAccountListDialogCustomContentView = null;

    private int mCurrentStrp = STEP_1;

    private String mOwnerPhone = null;
    private String mOwnerDisplayName = null;
    private String mTargetUserIKeyUID = null;
    private String mTargetUserDeviceID = null;
    private long mTargetUserDeviceCreateTime = 0;

    private boolean mIsInitDone = false;
    private boolean mIsInitInProgress = false;

    private class GetUserInfoAsyncTask extends AsyncTask<Void, Void, UserInfo> {

        @Override
        protected UserInfo doInBackground(Void... voids) {
            return Prefs.getUserInfo(AddNewSubKeyActivity.this);
        }

        @Override
        protected void onPostExecute(UserInfo userInfo) {
            mGetUserInfoAsyncTask = null;

            mUserInfo = userInfo;
            getSelectedCarKeyData();
        }
    }

    private class GetSelectedCarKeyDataAsyncTask extends AsyncTask<Void, Void, CarKeyData> {

        @Override
        protected CarKeyData doInBackground(Void... voids) {
            return Prefs.getCarKeyData(AddNewSubKeyActivity.this);
        }

        @Override
        protected void onPostExecute(CarKeyData carKeyData) {
            mGetSelectedCarKeyDataAsyncTask = null;
            dismissProgressDialog();

            mSelectedCarKeyData = carKeyData;
            mIsInitDone = true;
            mIsInitInProgress = false;
        }
    }

    private View.OnClickListener mOnPasswordConfrimButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            String password = mPasswordEditText.getText().toString().trim();
            if (TextUtils.isEmpty(password)) {
                mPasswordEditText.setError(getString(R.string.add_new_sub_key_input_error));
            } else {
                callAPIToCheckPassword();
            }

        }
    };

    private View.OnClickListener mOnOwnerInfoNextButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            mOwnerPhone = mOwnerPhoneEditText.getText().toString().trim();
            mOwnerDisplayName = mOwnerNameEditText.getText().toString().trim();
            int cnCount = LanguageUtils.getChineseCharacterCount(mOwnerDisplayName);
            int enCount = mOwnerDisplayName.length() - cnCount;
            if (TextUtils.isEmpty(mOwnerPhone)) {
                mOwnerPhoneEditText.requestFocus();
                mOwnerPhoneEditText.setError(getString(R.string.add_new_sub_key_input_error));
            } else if ((cnCount * 2 + enCount) > CommonDefs.DIGITAL_KEY_DISPLAYNAME_MAX_LENGTH) {
                mOwnerNameEditText.requestFocus();
                mOwnerNameEditText.setError(getString(R.string.add_new_sub_key_input_owner_name_hint));
            } else {
                callAPIToGetUserData();
            }
        }
    };

    private View.OnClickListener mOnSendSubKeyDialogPositionButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            mAlertDialog1.dismiss();
            callCAServiceAPIToAddSubKey();
        }
    };

    private View.OnClickListener mOnSendSubKeyDialogNegtiveButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            mAlertDialog1.dismiss();
        }
    };

    private OnApiListener<ResponseData> mCheckPasswordApiListener = new OnApiListener<ResponseData>() {

        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            dismissProgressDialog();
            if (responseData.isSuccessful()) {
                mCurrentStrp = STEP_2;
                switchStepDisplay();
            } else {
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(getString(R.string.dialog_title_error), failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<QueryUserResponseData> mGetUserDataApiListener = new OnApiListener<QueryUserResponseData>() {

        @Override
        public void onApiTaskSuccessful(QueryUserResponseData responseData) {
            if (responseData.isSuccessful() && responseData.getSize() != 0) {
                ArrayList<UserDeviceData> userDataList = responseData.getList();
                if (userDataList.size() > 0) {
                    if (userDataList.size() > 1) {
                        showUserAccountListDialog(userDataList);
                        dismissProgressDialog();
                    } else {
                        UserDeviceData userDeviceData = userDataList.get(0);
                        mTargetUserIKeyUID = userDeviceData.getKeyUid();
                        mTargetUserDeviceID = userDeviceData.getDeviceId();
                        mTargetUserDeviceCreateTime = userDeviceData.getDeviceCreateTime();
                        callCAServiceAPIToAddSubKey();
                    }
                } else {
                    dismissProgressDialog();
                }
            } else {
                dismissProgressDialog();
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(getString(R.string.dialog_title_error), failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_sub_key);

        LinearLayout mainContainer = findViewById(R.id.activity_container);
        setWheelScreenLayout(mainContainer);

        ImageView wheelRightHandle = findViewById(R.id.wheel_right_handle_image_view);
        ImageView wheelLeftHandle = findViewById(R.id.wheel_left_handle_image_view);
        setWheelHandles(wheelRightHandle, wheelLeftHandle);

        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            toolBar.setTitle(R.string.activity_add_new_sub_key_title);
            setSupportActionBar(toolBar);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (getIntent() != null) {
            mKeyPosition = getIntent().getIntExtra("position", -1);
        }

        mViewModel = ViewModelProviders.of(this).get(AddNewSubKeyViewModel.class);

        // Input Password
        mInputPasswordLayout = findViewById(R.id.activity_add_new_sub_key_input_password_layout);
        mPasswordEditText = findViewById(R.id.activity_add_new_sub_key_password_edit_text);
        mPasswordConfirmButton = findViewById(R.id.activity_add_new_sub_key_password_confirm_button);
        mPasswordConfirmButton.setOnClickListener(mOnPasswordConfrimButtonClickListener);

        // Input Owner Info
        mInputOwnerInfoLayout = findViewById(R.id.activity_add_new_sub_key_input_owner_info_layout);
        mOwnerPhoneEditText = findViewById(R.id.activity_add_new_sub_key_owner_phone_edit_text);
        mOwnerNameEditText = findViewById(R.id.activity_add_new_sub_key_owner_name_edit_text);
        mOwnerInfoNextButton = findViewById(R.id.activity_add_new_sub_key_next_button);
        mOwnerInfoNextButton.setOnClickListener(mOnOwnerInfoNextButtonClickListener);

        mUserAccountListDialogCustomContentView = (RadioGroup) getLayoutInflater().inflate(R.layout.dialog_custom_user_account_radio_group, null, false);

        mPasswordEditText.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        Utils.init(this);

        bindCaService();
    }

    @Override
    protected void onCaServiceConnected() {
        if (!mIsInitInProgress) {
            mIsInitInProgress = true;
            init();
        }

        super.onCaServiceConnected();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        if (mGetUserInfoAsyncTask != null) {
            mGetUserInfoAsyncTask.cancel(true);
        }

        if (mGetSelectedCarKeyDataAsyncTask != null) {
            mGetSelectedCarKeyDataAsyncTask.cancel(true);
        }

        unbindCaService();
        super.onDestroy();
    }

    @Override
    protected void onReloadSelectedCarKeyData(CarKeyData selectedCarKeyData) {
        mSelectedCarKeyData = selectedCarKeyData;
    }

    @Override
    protected void onReceiveBroadcastFromCaService(Context context, Intent intent) {
        Prefs.setRefreshDigitalKeyListFlag(AddNewSubKeyActivity.this, true);

        if (CommunicationAgentService.CA_STATUS_ADD_SUBKEY.equals(intent.getAction())) {
            dismissProgressDialog();

            int returnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            if (returnCode == CommunicationAgentService.CA_EXTRA_ADD_SUBKEY_SUCCESS) {
                showConfirmDialog(getString(R.string.add_new_sub_key_dialog_sub_key_sent_title),
                        getString(R.string.add_new_sub_key_dialog_sub_key_sent_message),
                        new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        });
            } else if (returnCode == CommunicationAgentService.CA_EXTRA_ADD_SUBKEY_PREPARED_SUCCESS) {
                showConfirmDialog(getString(R.string.add_new_sub_key_dialog_sub_key_prepared_sent_title),
                        getString(R.string.add_new_sub_key_dialog_sub_key_prepared_sent_message),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        });
            } else {
                showConfirmDialog(getString(R.string.add_new_sub_key_dialog_add_sub_key_fail_title),
                        getString(R.string.add_new_sub_key_dialog_add_sub_key_fail_message));
            }

            return;
        } else if (CommunicationAgentService.CA_STATUS_VIG_WAKEUP_SUCCESS.equals(intent.getAction())) {
            dismissProgressDialog();
            return;
        } else if (CommunicationAgentService.CA_STATUS_VIG_WAKEUP_FAILED.equals(intent.getAction())) {
            dismissProgressDialog();
            showVigConnectionFailWithBluetooth();
            return;
        } else if (CommunicationAgentService.CA_STATUS_IKEY_FUNCTOSERVER_TIMEOUT.equals(intent.getAction())) {
            dismissProgressDialog();
            LogCat.d("CA_STATUS_IKEY_FUNCTOSERVER_TIMEOUT");
            return;
        }

        super.onReceiveBroadcastFromCaService(context, intent);
    }

    private void init() {
        showProgressDialog();
        if (mGetUserInfoAsyncTask == null) {
            mGetUserInfoAsyncTask = new GetUserInfoAsyncTask();
            mGetUserInfoAsyncTask.execute();
        }
    }

    private void getSelectedCarKeyData() {
        showProgressDialog();
        if (mGetSelectedCarKeyDataAsyncTask == null) {
            mGetSelectedCarKeyDataAsyncTask = new GetSelectedCarKeyDataAsyncTask();
            mGetSelectedCarKeyDataAsyncTask.execute();
        }
    }

    private void callAPIToCheckPassword() {
        showProgressDialog();
        CheckPasswordRequestData checkPasswordRequestData = new CheckPasswordRequestData(mUserInfo.getKeyUid(), mPasswordEditText.getText().toString().trim());

        CarWebApi carWebApi = CarWebApi.getInstance(this);
        carWebApi.checkPassword(checkPasswordRequestData, mCheckPasswordApiListener);
    }

    private void callAPIToGetUserData() {
        showProgressDialog();
        QueryUserRequestData queryUserRequestData = new QueryUserRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken(), mOwnerPhone);

        UserWebApi userWebApi = UserWebApi.getInstance(this);
        userWebApi.getUserData(queryUserRequestData, mGetUserDataApiListener);
    }

    private void callCAServiceAPIToAddSubKey() {
        if (mCommunicationAgentService != null) {

            boolean existInDb = false;
            ArrayList<KeyInfo> keyInfoList = mViewModel.getKeyInfoListFromDb(mSelectedCarKeyData.getCarId());
            for (int position = 1; position < 5; position++) {
                KeyInfo keyInfo = keyInfoList.get(position);
                if(keyInfo.getUserId().equals(mTargetUserIKeyUID) &&
                        keyInfo.getDeviceId().equals(mTargetUserDeviceID)) {
                    existInDb = true;
                    break;
                }
            }

            if(existInDb) {
                showConfirmDialog(getString(R.string.add_new_sub_key_dialog_add_sub_key_fail_title),
                        getString(R.string.add_new_sub_key_dialog_add_sub_key_fail_message));
            } else {
                showProgressDialog();
                mCommunicationAgentService.addSubKey(mKeyPosition, mTargetUserIKeyUID, mTargetUserDeviceID, mTargetUserDeviceCreateTime, mOwnerDisplayName);
            }
        }
    }

    private void showUserAccountListDialog(ArrayList<UserDeviceData> userDataList) {

        mUserAccountListDialogCustomContentView.clearCheck();
        mUserAccountListDialogCustomContentView.removeAllViews();

        for (int index = 0; index < userDataList.size(); index++) {
            UserDeviceData data = userDataList.get(index);

            RadioButton button = (RadioButton) getLayoutInflater().inflate(R.layout.dialog_custom_user_account_radio_button, null, false);
            button.setId(index);
            button.setText(data.getKeyAccount());
            button.setTag(data);

            RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMarginStart((int) Utils.convertDpToPixel(24));
            layoutParams.setMarginEnd((int) Utils.convertDpToPixel(24));
            layoutParams.topMargin = (int) Utils.convertDpToPixel(10);
            layoutParams.bottomMargin = (int) Utils.convertDpToPixel(10);

            mUserAccountListDialogCustomContentView.addView(button, index, layoutParams);
        }

        mUserAccountListDialogCustomContentView.check(0);

        if (mAlertDialog1.isShowing()) {
            mAlertDialog1.dismiss();
        }
        mAlertDialog1.restoreDefault();
        mAlertDialog1.setTitle(getString(R.string.add_new_sub_key_dialog_select_user_account_title));
        mAlertDialog1.setCustomContentView(mUserAccountListDialogCustomContentView);
        mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                getResources().getString(R.string.btn_confirm),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        RadioButton radioButton = (RadioButton) mUserAccountListDialogCustomContentView.getChildAt(mUserAccountListDialogCustomContentView.getCheckedRadioButtonId());
                        UserDeviceData data = (UserDeviceData) radioButton.getTag();
                        mTargetUserIKeyUID = data.getKeyUid();
                        mTargetUserDeviceID = data.getDeviceId();
                        mTargetUserDeviceCreateTime = data.getDeviceCreateTime();
                        mAlertDialog1.dismiss();

                        showAddSubKeyConfirmDialog();
                    }
                });
        mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                getResources().getString(R.string.btn_cancel),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mAlertDialog1.dismiss();
                    }
                });
        mAlertDialog1.show();
    }

    private void showAddSubKeyConfirmDialog() {
        mAlertDialog1.restoreDefault();
        mAlertDialog1.setTitle(R.string.add_new_sub_key_dialog_send_sub_key_title);
        String message = getString(R.string.add_new_sub_key_dialog_send_sub_key_message);
        message = String.format(message, mOwnerPhone);
        mAlertDialog1.setMessage(message);
        mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                getString(R.string.btn_confirm),
                mOnSendSubKeyDialogPositionButtonClickListener);
        mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                getString(R.string.btn_cancel),
                mOnSendSubKeyDialogNegtiveButtonClickListener);
        mAlertDialog1.show();
    }

    private void showVigConnectionFailWithBluetooth() {
        if (mAlertDialog1.isShowing()) {
            mAlertDialog1.dismiss();
        }
        mAlertDialog1.restoreDefault();
        mAlertDialog1.setTitle(getString(R.string.dialog_title_vig_connect_fail));
        mAlertDialog1.setMessage(getString(R.string.dialog_message_vig_connect_fail_with_bluetooth));
        mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                getResources().getString(R.string.btn_confirm),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mAlertDialog1.dismiss();
                    }
                });
        mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                getResources().getString(R.string.btn_near_end_connection),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent intent = NearEndConnectionTeachingActivity.newIntentToAddSubKey(AddNewSubKeyActivity.this,
                                mKeyPosition, mTargetUserIKeyUID, mTargetUserDeviceID, mTargetUserDeviceCreateTime, mOwnerDisplayName);
                        startActivity(intent);
                        mAlertDialog1.dismiss();
                        finish();
                    }
                });
        mAlertDialog1.show();
    }

    private void switchStepDisplay() {
        switch (mCurrentStrp) {
            case STEP_1: {
                mInputPasswordLayout.setVisibility(View.VISIBLE);
                mInputOwnerInfoLayout.setVisibility(View.INVISIBLE);
            }
            break;

            case STEP_2: {
                mInputPasswordLayout.setVisibility(View.INVISIBLE);
                mInputOwnerInfoLayout.setVisibility(View.VISIBLE);
            }
            break;

            default:
        }
    }
}
