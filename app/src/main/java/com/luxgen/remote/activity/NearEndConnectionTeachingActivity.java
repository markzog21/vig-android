package com.luxgen.remote.activity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.luxgen.remote.R;
import com.luxgen.remote.ca.CommunicationAgentService;
import com.luxgen.remote.custom.CustomAppCompatActivity;


public class NearEndConnectionTeachingActivity extends CustomAppCompatActivity {

    public static final int ACTION_NONE = -1;
    public static final int ACTION_ADD_SUB_KEY = 0;
    public static final int ACTION_DELETE_SUB_KEY = 1;

    public static Intent newIntentToAddSubKey(Context context, int keyPosition, String iKeyUid, String deviceId, long deviceCreateTime, String displayName) {
        Intent intent = new Intent(context, NearEndConnectionTeachingActivity.class);
        intent.putExtra("action", ACTION_ADD_SUB_KEY);
        intent.putExtra("position", keyPosition);
        intent.putExtra("i_key_uid", iKeyUid);
        intent.putExtra("device_id", deviceId);
        intent.putExtra("device_create_time", deviceCreateTime);
        intent.putExtra("display_name", displayName);
        return intent;
    }

    public static Intent newIntentToDeleteSubKey(Context context, String keyType, String iKeyUid, String deviceId, long deviceCreateTime) {
        Intent intent = new Intent(context, NearEndConnectionTeachingActivity.class);
        intent.putExtra("action", ACTION_DELETE_SUB_KEY);
        intent.putExtra("key_type", keyType);
        intent.putExtra("i_key_uid", iKeyUid);
        intent.putExtra("device_id", deviceId);
        intent.putExtra("device_create_time", deviceCreateTime);
        return intent;
    }

    private Button mReconnectButton = null;
    private Button mNoConnectButton = null;

    private int mAction = ACTION_NONE;
    private int mKeyPosition = -1;
    private String mKeyType = null;
    private String mIKeyUid = null;
    private String mDeviceId = null;
    private long mDeviceCreateTime = 0;
    private String mDisplayName = "";

    private OnClickListener mOnReconnectButtonClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            showProgressDialog();
            switch (mAction) {
                case ACTION_ADD_SUB_KEY: {
                    mCommunicationAgentService.addSubKey(mKeyPosition, mIKeyUid, mDeviceId, mDeviceCreateTime, "");
                    break;
                }

                case ACTION_DELETE_SUB_KEY: {
                    mCommunicationAgentService.delSubKey(mKeyType, mIKeyUid, mDeviceId, mDeviceCreateTime);
                    break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_near_end_connection_teaching);

        LinearLayout mainContainer = findViewById(R.id.activity_container);
        setWheelScreenLayout(mainContainer);

        ImageView wheelRightHandle = findViewById(R.id.wheel_right_handle_image_view);
        ImageView wheelLeftHandle = findViewById(R.id.wheel_left_handle_image_view);
        setWheelHandles(wheelRightHandle, wheelLeftHandle);

        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            toolBar.setTitle(R.string.activity_near_end_connection_teaching_title);
            setSupportActionBar(toolBar);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        if (intent != null) {
            mAction = intent.getIntExtra("action", ACTION_NONE);
            switch (mAction) {
                case ACTION_ADD_SUB_KEY: {
                    mKeyPosition = intent.getIntExtra("position", -1);
                    mDisplayName = intent.getStringExtra("display_name");
                    break;
                }

                case ACTION_DELETE_SUB_KEY: {
                    mKeyType = intent.getStringExtra("key_type");
                    break;
                }
            }

            mIKeyUid = intent.getStringExtra("i_key_uid");
            mDeviceId = intent.getStringExtra("device_id");
            mDeviceCreateTime = intent.getLongExtra("device_create_time", 0);
        }

        mReconnectButton = findViewById(R.id.activity_near_end_connection_teaching_reconnect_button);
        mReconnectButton.setOnClickListener(mOnReconnectButtonClickListener);
        mNoConnectButton = findViewById(R.id.activity_near_end_connection_teaching_no_connect_button);

        bindCaService();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        unbindCaService();
        super.onDestroy();
    }

    @Override
    protected void onCaServiceConnected() {
        if (mCommunicationAgentService.getVigState() == CommunicationAgentService.VIG_READY_TO_GO) {
            enableButton();
        } else {
            disableButton();
        }
    }

    @Override
    protected void addActionToReceiveBroadcastFromCaService(IntentFilter intentFilter) {
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_VIG_ONLINE);
    }

    @Override
    protected void onReceiveBroadcastFromCaService(Context context, Intent intent) {
        if (CommunicationAgentService.CA_STATUS_VIG_ONLINE.equals(intent.getAction())) {
            enableButton();
            return;
        } else if (CommunicationAgentService.CA_STATUS_ADD_SUBKEY.equals(intent.getAction())) {
            dismissProgressDialog();
            int extraReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            switch (extraReturnCode) {
                case CommunicationAgentService.CA_EXTRA_ADD_SUBKEY_SUCCESS: {
                    showConfirmDialog(getString(R.string.add_new_sub_key_dialog_sub_key_sent_title),
                            getString(R.string.add_new_sub_key_dialog_sub_key_sent_message),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    finish();
                                }
                            });
                    break;
                }

                case CommunicationAgentService.CA_EXTRA_ADD_SUBKEY_PREPARED_SUCCESS: {
                    showConfirmDialog(getString(R.string.add_new_sub_key_dialog_sub_key_prepared_sent_title),
                            getString(R.string.add_new_sub_key_dialog_sub_key_prepared_sent_message),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    finish();
                                }
                            });
                    break;
                }

                default: {
                    showConfirmDialog(getString(R.string.add_new_sub_key_dialog_add_sub_key_fail_title),
                            getString(R.string.add_new_sub_key_dialog_add_sub_key_fail_message));
                }
            }

            return;
        } else if (CommunicationAgentService.CA_STATUS_DEL_SUBKEY.equals(intent.getAction())) {
            dismissProgressDialog();
            int extraReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            switch (extraReturnCode) {
                case CommunicationAgentService.CA_EXTRA_DEL_SUBKEY_SUCCESS_START: {
                    showConfirmDialog(getString(R.string.edit_digital_key_dialog_sub_key_deleted_title),
                            getString(R.string.edit_digital_key_dialog_sub_key_deleted_message),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    finish();
                                }
                            });
                    break;
                }

                case CommunicationAgentService.CA_EXTRA_DEL_SUBKEY_PREPARED_SUCCESS: {
                    showConfirmDialog(getString(R.string.edit_digital_key_dialog_sub_key_prepared_deleted_title),
                            getString(R.string.edit_digital_key_dialog_sub_key_prepared_deleted_message),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    finish();
                                }
                            });
                    break;
                }

                default: {
                    showConfirmDialog(getString(R.string.edit_digital_key_dialog_delete_sub_key_fail_title),
                            getString(R.string.edit_digital_key_dialog_delete_sub_key_fail_message));
                }
            }

            return;
        } else if (CommunicationAgentService.CA_STATUS_VIG_WAKEUP_SUCCESS.equals(intent.getAction())) {
            dismissProgressDialog();
            return;
        } else if (CommunicationAgentService.CA_STATUS_VIG_WAKEUP_FAILED.equals(intent.getAction())) {
            dismissProgressDialog();
            showConfirmDialog(getString(R.string.dialog_title_vig_connect_fail),
                    getString(R.string.dialog_message_vig_connect_fail));

            return;
        }

        super.onReceiveBroadcastFromCaService(context, intent);
    }

    private void enableButton() {
        mReconnectButton.setVisibility(View.VISIBLE);
        mNoConnectButton.setVisibility(View.GONE);
    }

    private void disableButton() {
        mReconnectButton.setVisibility(View.GONE);
        mNoConnectButton.setVisibility(View.VISIBLE);
    }
}

