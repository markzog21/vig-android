package com.luxgen.remote.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.adapter.DigitalKeySubKeyOwnerAdapter;
import com.luxgen.remote.adapter.DigitalKeyTempKeyOwnerAdapter;
import com.luxgen.remote.ca.CommunicationAgentService;
import com.luxgen.remote.custom.CommonDefs;
import com.luxgen.remote.custom.CustomAppCompatActivity;
import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.model.KeyInfo;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.option.BeaconOptions;
import com.luxgen.remote.model.option.CarOptions;
import com.luxgen.remote.network.CarWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.UserWebApi;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.VigRequestData;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.car.GetCarKeyRequestData;
import com.luxgen.remote.network.model.car.GetCarKeyResponseData;
import com.luxgen.remote.network.model.car.GetKeyListResponseData;
import com.luxgen.remote.network.model.car.KeyData;
import com.luxgen.remote.network.model.car.SetBeaconLearingStatusRequestData;
import com.luxgen.remote.network.model.user.GetMasterKeyUserRequestData;
import com.luxgen.remote.network.model.user.GetMasterKeyUserResponseData;
import com.luxgen.remote.ui.viewmodel.DigitalKeyViewModel;
import com.luxgen.remote.util.DialUtils;
import com.luxgen.remote.util.Prefs;
import com.luxgen.remote.util.TimeUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import tw.com.haitec.ae.project.smartphoneikey.snappydb.IKeyDataBean;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.MasterKeyDataBean;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.SnapKeyDataBean;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.SnapTicket;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.SubKeyDataBean;

public class DigitalKeyActivity extends CustomAppCompatActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, DigitalKeyActivity.class);
    }

    private final int DIGITAL_KEY_MODE_NONE = -1;
    private final int DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE = 0;
    private final int DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE = 1;
    private final int DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE_CONNECT = 2;
    private final int DIGITAL_KEY_MODE_MASTER_KEY_IN_USING = 3;
    private final int DIGITAL_KEY_MODE_MASTER_KEY_IN_USING_DELETING = 4;
    private final int DIGITAL_KEY_MODE_MASTER_KEY_ABNORMAL = 5;
    private final int DIGITAL_KEY_MODE_SUB_KEY = 6;
    private final int DIGITAL_KEY_MODE_TEMPORARY_KEY = 7;

    private GetUserInfoAsyncTask mGetUserInfoAsyncTask = null;
    private GetSelectedCarKeyDataAsyncTask mGetSelectedCarKeyDataAsyncTask = null;

    private SimpleDateFormat mSimpleDateFormatForServer = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.TAIWAN);

    private UserInfo mUserInfo = null;
    private CarKeyData mSelectedCarKeyData = null;
    private KeyInfo mTargetKeyInfo = null;

    private DigitalKeyViewModel mViewModel = null;

    private Menu mOptionsMenu = null;

    // Temporary Key
    private ViewGroup mTempKeyLayout = null;
    private TextView mTempKeyCarModelTextView = null;
    private TextView mTempKeyPlateNumberTextView = null;
    private TextView mTempKeyOwnerTextView = null;
    private TextView mTempKeyHoldingPeriodTextView = null;

    // Sub Key
    private ViewGroup mSubKeyLayout = null;
    private TextView mSubKeyCarModelTextView = null;
    private TextView mSubKeyPlateNumberTextView = null;
    private TextView mSubKeyOwnerTextView = null;

    // Master Key Inactive
    private ViewGroup mMasterKeyInactiveLayout = null;
    private TextView mMasterKeyInactiveActivateButton = null;

    // Master Key Inactive Connect
    private ViewGroup mMasterKeyInactiveConnectLayout = null;

    // Master Key
    private DigitalKeySubKeyOwnerAdapter mDigitalKeySubKeyOwnerAdapter = null;
    private DigitalKeyTempKeyOwnerAdapter mDigitalKeyTempKeyOwnerAdapter = null;
    private ViewGroup mMasterKeyLayout = null;
    private TextView mMasterKeyCarModelTextView = null;
    private TextView mMasterKeyPlateNumberTextView = null;
    private ImageView mMasterKeyKeylessSettingsImageView = null;
    private RecyclerView mMasterKeySubKeyOwnerRecyclerView = null;
    private RecyclerView mMasterKeyTempKeyOwnerRecyclerView = null;

    // Master Key In Using
    private ViewGroup mMasterKeyInUsingLayout = null;
    private TextView mMasterKeyInUsingApplyButton = null;

    // Master Key In Using Deleting
    private ViewGroup mMasterKeyInUsingDeletingLayout = null;

    // Master Key Abnormal
    private ViewGroup mMasterKeyAbnormalLayout = null;
    private TextView mMasterKeyAbnormalCallServiceButton = null;

    private int mDigitalKeyMode = DIGITAL_KEY_MODE_NONE;

    private int mKeyPosition = -1;
    private String mMasterKeyRealKeyType = CarOptions.KEY_NO;

    private boolean mIsInitDone = false;
    private boolean mIsInitInProgress = false;
    private boolean mIsActivityRunning = false;
    private boolean mIsMasterKeyActivationInProgress = false;

    private class GetUserInfoAsyncTask extends AsyncTask<Void, Void, UserInfo> {

        @Override
        protected UserInfo doInBackground(Void... voids) {
            return Prefs.getUserInfo(DigitalKeyActivity.this);
        }

        @Override
        protected void onPostExecute(UserInfo userInfo) {
            mGetUserInfoAsyncTask = null;

            mUserInfo = userInfo;
            getSelectedCarKeyData();
        }
    }

    private class GetSelectedCarKeyDataAsyncTask extends AsyncTask<Void, Void, CarKeyData> {

        @Override
        protected CarKeyData doInBackground(Void... voids) {
            return Prefs.getCarKeyData(DigitalKeyActivity.this);
        }

        @Override
        protected void onPostExecute(CarKeyData carKeyData) {
            mGetSelectedCarKeyDataAsyncTask = null;

            mSelectedCarKeyData = carKeyData;
            clearAP1AndRestoreDP1KeyStatusKeys();
            updateDisplayRelatedToSelectedCarKeyData();

            dismissProgressDialog();

            if (mSelectedCarKeyData.isMasterKey()) {
                callAPIToGetMasterKeyUserData();
            } else {
                mDigitalKeyMode = getDigitalKeyMode();
                setUIDisplayByDigitalKeyMode();
                fetchDataByDigitalKeyMode();
            }
        }
    }

    private DigitalKeySubKeyOwnerAdapter.OnInnerViewsClickListener mOnMasterKeySubKeyOwnerRecyclerViewInnerViewsClickListener = new DigitalKeySubKeyOwnerAdapter.OnInnerViewsClickListener() {

        @Override
        public void onSubKeyOwnerImageViewClick(DigitalKeySubKeyOwnerAdapter adapter, int position) {

        }

        @Override
        public void onSubKeyOwnerPrepareAddingImageViewClick(DigitalKeySubKeyOwnerAdapter adapter, int position) {
            showProgressDialog();
            mTargetKeyInfo = (KeyInfo) adapter.getItem(position).getDataObject();
            mKeyPosition = position;
            mCommunicationAgentService.addSubKey(mKeyPosition, mTargetKeyInfo.getUserId(),
                    mTargetKeyInfo.getDeviceId(), mTargetKeyInfo.getDeviceCreateTime(), mTargetKeyInfo.getDisplayName());
        }

        @Override
        public void onSubKeyOwnerPrepareDeletingImageViewClick(DigitalKeySubKeyOwnerAdapter adapter, int position) {
            showProgressDialog();
            mTargetKeyInfo = (KeyInfo) adapter.getItem(position).getDataObject();
            mCommunicationAgentService.delSubKey(mTargetKeyInfo.getKeyType(),
                    mTargetKeyInfo.getUserId(), mTargetKeyInfo.getDeviceId(), mTargetKeyInfo.getDeviceCreateTime());
        }

        @Override
        public void onAddNewImageViewClick(DigitalKeySubKeyOwnerAdapter adapter, int position) {
            startActivity(AddNewSubKeyActivity.newIntent(DigitalKeyActivity.this, position));
        }
    };

    private DigitalKeyTempKeyOwnerAdapter.OnInnerViewsClickListener mOnMasterKeyTempKeyOwnerRecyclerViewInnerViewsClickListener = new DigitalKeyTempKeyOwnerAdapter.OnInnerViewsClickListener() {

        @Override
        public void onTempKeyOwnerImageViewClick(DigitalKeyTempKeyOwnerAdapter adapter, int position) {
            KeyInfo keyInfo = (KeyInfo) adapter.getItem(position).getDataObject();
            String startTimeString = null;
            String endTimeString = null;
            try {
                startTimeString = TimeUtils.parseToServerFormateString(keyInfo.getSnapKeyStartTime()).split(" ")[0];
                endTimeString = TimeUtils.parseToServerFormateString(keyInfo.getSnapKeyEndTime()).split(" ")[0];
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String periodString = getString(R.string.digital_key_master_key_dialog_temp_key_period_message);
            periodString = String.format(periodString, startTimeString, endTimeString);

            if (mAlertDialog1.isShowing()) {
                mAlertDialog1.dismiss();
            }
            mAlertDialog1.restoreDefault();
            mAlertDialog1.setTitle(getString(R.string.digital_key_master_key_dialog_temp_key_period_title));
            mAlertDialog1.setMessage(periodString);
            mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                    getString(R.string.btn_confirm),
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            mAlertDialog1.dismiss();
                        }
                    });
            mAlertDialog1.show();
        }

        @Override
        public void onSetImageViewClick(DigitalKeyTempKeyOwnerAdapter adapter, int position) {
            startActivity(SetTempKeyActivity.newIntent(DigitalKeyActivity.this, position + 4));
        }

        @Override
        public void onRenewImageViewClick(DigitalKeyTempKeyOwnerAdapter adapter, int position) {
            mAlertDialog1.restoreDefault();
            mAlertDialog1.setTitle(getString(R.string.digital_key_master_key_dialog_renew_temp_key_title));
            mAlertDialog1.setMessage(getString(R.string.digital_key_master_key_dialog_renew_temp_key_message));
            mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                    getString(R.string.btn_confirm),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAlertDialog1.dismiss();
                            showProgressDialog(getString(R.string.digital_key_master_key_progress_dialog_connect_message));
                            callCAServiceAPIToRenewSnapKey();
                        }
                    });
            mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                    getString(R.string.btn_cancel),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAlertDialog1.dismiss();
                        }
                    });
            mAlertDialog1.show();
        }

    };

    private View.OnClickListener mOnMasterKeyInactiveActivateButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            mDigitalKeyMode = DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE_CONNECT;
            setUIDisplayByDigitalKeyMode();
            callCAServiceAPIToAddMasterKey();
        }
    };

    private View.OnClickListener mOnMasterKeyInUsingApplyButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mSelectedCarKeyData.isOwner()) {
                showApplyMasterKeyDialog();
            } else if (mSelectedCarKeyData.isDriver() && mUserInfo != null && mSelectedCarKeyData != null) {
                if (mUserInfo.getDeviceId().equals(mSelectedCarKeyData.getPhoneId())) {
                    // TODO: Driver has different process way. It is just a temporary solution here and must be modified in the future.
                    showApplyMasterKeyDialog();
                } else { // Driver 換手機
                    showApplyMasterKeyDialog();
                }
            }
        }
    };

    private View.OnClickListener mOnMasterKeyAbnormalCallServiceButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            Intent intent = DialUtils.callService(DigitalKeyActivity.this);
            // sendUseLog(intent.getStringExtra(DialUtils.PHONE), getString(R.string.one_key_service_call_service_label));
            startActivity(intent);
        }
    };

    private View.OnClickListener mOnMasterKeyKeylessSettingsImageViewClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            startActivity(KeylessSettingsActivity.newIntent(DigitalKeyActivity.this));
        }
    };

    private OnApiListener<GetKeyListResponseData> mGetKeyListApiListener = new OnApiListener<GetKeyListResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetKeyListResponseData responseData) {
            if (responseData.isSuccessful()) {

                switch (mDigitalKeyMode) {
                    case DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE: {
                        syncKeysFromKeyListInMasterKeyActiveMode(responseData.getCarList());
                        mIsInitDone = true;
                        mIsInitInProgress = false;
                        dismissProgressDialog();

                        // checkKeylessSettings();
                        break;
                    }

                    case DIGITAL_KEY_MODE_TEMPORARY_KEY: {
                        updateDisplayRelatedToKeyListInTempKeyMode(responseData.getCarList());
                        callAPIToGetMasterKeyUserData();
                        break;
                    }

                    default: {
                        dismissProgressDialog();
                    }
                }

            } else {
                mIsInitDone = true;
                mIsInitInProgress = false;
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
                dismissProgressDialog();
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            switch (mDigitalKeyMode) {
                case DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE: {
                    mDigitalKeySubKeyOwnerAdapter.enableKeyAdding(false);
                    mDigitalKeyTempKeyOwnerAdapter.enableKeySetting(false);
                    buildKeysFromKeyListInMasterKeyActiveMode();
                    // checkKeylessSettings();
                    break;
                }

                default: {
                    showServerErrorDialog(getString(R.string.dialog_title_error), failMessage);
                }
            }

            mIsInitDone = true;
            mIsInitInProgress = false;
            dismissProgressDialog();
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetMasterKeyUserResponseData> mGetMasterKeyUserDataApiListener = new OnApiListener<GetMasterKeyUserResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetMasterKeyUserResponseData responseData) {
            if (responseData.isSuccessful()) {

                switch (mDigitalKeyMode) {
                    case DIGITAL_KEY_MODE_NONE: {
                        if (responseData.getStatus() == null) {
                            mMasterKeyRealKeyType = CarOptions.KEY_NO;
                        } else {
                            mMasterKeyRealKeyType = responseData.getStatus();
                        }
                        mDigitalKeyMode = getDigitalKeyMode();
                        setUIDisplayByDigitalKeyMode();
                        fetchDataByDigitalKeyMode();
                        break;
                    }

                    case DIGITAL_KEY_MODE_SUB_KEY: {
                        updateDisplayRelatedToMasterKeyUserDataInSubKeyMode(responseData);
                        mIsInitDone = true;
                        mIsInitInProgress = false;
                        break;
                    }

                    case DIGITAL_KEY_MODE_TEMPORARY_KEY: {
                        updateDisplayRelatedToMasterKeyUserDataInTempKeyMode(responseData);
                        mIsInitDone = true;
                        mIsInitInProgress = false;
                        break;
                    }

                    default: {

                    }
                }

            } else {
                if (DIGITAL_KEY_MODE_NONE == mDigitalKeyMode) {
                    mMasterKeyRealKeyType = CarOptions.KEY_NO;
                    mDigitalKeyMode = getDigitalKeyMode();
                    setUIDisplayByDigitalKeyMode();
                    fetchDataByDigitalKeyMode();
                } else {
                    mIsInitDone = true;
                    mIsInitInProgress = false;
                    showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                            responseData.getErrorMessage());
                }
            }

            dismissProgressDialog();
        }

        @Override
        public void onApiTaskFailure(String failMessage) {

            switch (mDigitalKeyMode) {
                case DIGITAL_KEY_MODE_NONE: {
                    mMasterKeyRealKeyType = mSelectedCarKeyData.getKeyType();
                    mDigitalKeyMode = getDigitalKeyMode();
                    setUIDisplayByDigitalKeyMode();
                    fetchDataByDigitalKeyMode();
                    break;
                }

                default: {
                    mIsInitDone = true;
                    mIsInitInProgress = false;
                    dismissProgressDialog();
                    showServerErrorDialog(getString(R.string.dialog_title_error), failMessage);
                }
            }
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetCarKeyResponseData> mGetCarKeyListApiListener = new OnApiListener<GetCarKeyResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetCarKeyResponseData responseData) {
            dismissProgressDialog();

            if (responseData.isSuccessful()) {
                boolean isMatched = false;
                ArrayList<CarKeyData> carKeyDataList = responseData.getCarKeyDataList();
                for (CarKeyData carKeyData : carKeyDataList) {
                    if (mSelectedCarKeyData.getCarId().equals(carKeyData.getCarId()) &&
                            mSelectedCarKeyData.getCarNo().equals(carKeyData.getCarNo())) {
                        mSelectedCarKeyData = carKeyData;
                        Prefs.setCarKeyData(DigitalKeyActivity.this, mSelectedCarKeyData);
                        Prefs.setCarKeyDataWithIKeyUid(DigitalKeyActivity.this, mUserInfo.getKeyUid(), mSelectedCarKeyData);
                        isMatched = true;
                        break;
                    }
                }

                if (isMatched) {
                    switch (mDigitalKeyMode) {
                        case DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE: {
                            mDigitalKeyMode = getDigitalKeyMode();
                            setUIDisplayByDigitalKeyMode();
                            fetchDataByDigitalKeyMode();
                            break;
                        }

                        case DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE: {
                            mDigitalKeyMode = getDigitalKeyMode();
                            setUIDisplayByDigitalKeyMode();
                            showActivateMasterKeyErrorDialog();
                            break;
                        }

                        case DIGITAL_KEY_MODE_MASTER_KEY_IN_USING_DELETING: {
                            mDigitalKeyMode = getDigitalKeyMode();
                            setUIDisplayByDigitalKeyMode();
                            showMasterKeyDeletedDialog();
                            break;
                        }
                    }

                } else {
                    switch (mDigitalKeyMode) {
                        case DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE: {
                            showActivateMasterKeyErrorDialog();
                            break;
                        }

                        case DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE: {
                            showActivateMasterKeyErrorDialog();
                            break;
                        }

                        case DIGITAL_KEY_MODE_MASTER_KEY_IN_USING_DELETING: {
                            mDigitalKeyMode = DIGITAL_KEY_MODE_MASTER_KEY_IN_USING;
                            setUIDisplayByDigitalKeyMode();
                            showDeleteMasterKeyErrorDialog();
                            break;
                        }
                    }
                }
            } else {
                switch (mDigitalKeyMode) {
                    case DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE: {
                        setUIDisplayByDigitalKeyMode();
                        break;
                    }

                    case DIGITAL_KEY_MODE_MASTER_KEY_IN_USING_DELETING: {
                        mDigitalKeyMode = DIGITAL_KEY_MODE_MASTER_KEY_IN_USING;
                        setUIDisplayByDigitalKeyMode();
                        break;
                    }
                }
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            switch (mDigitalKeyMode) {
                case DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE: {
                    setUIDisplayByDigitalKeyMode();
                    break;
                }

                case DIGITAL_KEY_MODE_MASTER_KEY_IN_USING_DELETING: {
                    mDigitalKeyMode = DIGITAL_KEY_MODE_MASTER_KEY_IN_USING;
                    setUIDisplayByDigitalKeyMode();
                    break;
                }
            }
            showServerErrorDialog(getString(R.string.dialog_title_error), failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            dismissProgressDialog();
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<ResponseData> mSetBeaconLearningStatusApiListener = new OnApiListener<ResponseData>() {

        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            dismissProgressDialog();
            if (responseData.isSuccessful()) {
                mSelectedCarKeyData.setBeaconLearningStatus(BeaconOptions.READ);
                Prefs.setCarKeyData(DigitalKeyActivity.this, mSelectedCarKeyData);
                Prefs.setCarKeyDataWithIKeyUid(DigitalKeyActivity.this, mUserInfo.getKeyUid(), mSelectedCarKeyData);
            } else {
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(getString(R.string.dialog_title_error), failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            dismissProgressDialog();
            showMultiLoginErrorDialog();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digital_key);

        LinearLayout mainContainer = findViewById(R.id.activity_container);
        setWheelScreenLayout(mainContainer);

        ImageView wheelRightHandle = findViewById(R.id.wheel_right_handle_image_view);
        ImageView wheelLeftHandle = findViewById(R.id.wheel_left_handle_image_view);
        setWheelHandles(wheelRightHandle, wheelLeftHandle);

        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            toolBar.setTitle(R.string.activity_digital_key_title);
            setSupportActionBar(toolBar);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mViewModel = ViewModelProviders.of(this).get(DigitalKeyViewModel.class);

        // Temporary Key
        mTempKeyLayout = findViewById(R.id.activity_digital_key_temp_key_layout);
        mTempKeyCarModelTextView = findViewById(R.id.activity_digital_key_temp_key_car_model_text_view);
        mTempKeyPlateNumberTextView = findViewById(R.id.activity_digital_key_temp_key_plate_number_text_view);
        mTempKeyOwnerTextView = findViewById(R.id.activity_digital_key_temp_key_owner_text_view);
        mTempKeyHoldingPeriodTextView = findViewById(R.id.activity_digital_key_temp_key_holding_period_text_view);

        // Sub Key
        mSubKeyLayout = findViewById(R.id.activity_digital_key_sub_key_layout);
        mSubKeyCarModelTextView = findViewById(R.id.activity_digital_key_sub_key_car_model_text_view);
        mSubKeyPlateNumberTextView = findViewById(R.id.activity_digital_key_sub_key_plate_number_text_view);
        mSubKeyOwnerTextView = findViewById(R.id.activity_digital_key_sub_key_owner_text_view);

        // Master Key Inactive
        mMasterKeyInactiveLayout = findViewById(R.id.activity_digital_key_master_key_inactive_layout);
        mMasterKeyInactiveActivateButton = findViewById(R.id.activity_digital_key_master_key_inactive_activate_button);
        mMasterKeyInactiveActivateButton.setOnClickListener(mOnMasterKeyInactiveActivateButtonClickListener);

        // Master Key Inactive Connect
        mMasterKeyInactiveConnectLayout = findViewById(R.id.activity_digital_key_master_key_inactive_connect_layout);

        // Master Key
        mMasterKeyLayout = findViewById(R.id.activity_digital_key_master_key_layout);
        mMasterKeyCarModelTextView = findViewById(R.id.activity_digital_key_master_key_car_model_text_view);
        mMasterKeyPlateNumberTextView = findViewById(R.id.activity_digital_key_master_key_plate_number_text_view);
        mMasterKeyKeylessSettingsImageView = findViewById(R.id.activity_digital_key_master_key_keyless_settings_image_view);
        mMasterKeyKeylessSettingsImageView.setOnClickListener(mOnMasterKeyKeylessSettingsImageViewClickListener);
        mMasterKeySubKeyOwnerRecyclerView = findViewById(R.id.activity_digital_key_master_key_sub_key_owner_recycler_view);
        mMasterKeyTempKeyOwnerRecyclerView = findViewById(R.id.activity_digital_key_master_key_temp_key_owner_recycler_view);

        // Master Key In Using
        mMasterKeyInUsingLayout = findViewById(R.id.activity_digital_key_master_key_in_using_layout);
        mMasterKeyInUsingApplyButton = findViewById(R.id.activity_digital_key_master_key_in_using_apply_button);
        mMasterKeyInUsingApplyButton.setOnClickListener(mOnMasterKeyInUsingApplyButtonClickListener);

        // Master Key In Using Deleting
        mMasterKeyInUsingDeletingLayout = findViewById(R.id.activity_digital_key_master_key_in_using_deleting_layout);

        // Master Key Abnormal
        mMasterKeyAbnormalLayout = findViewById(R.id.activity_digital_key_master_key_abnormal_layout);
        mMasterKeyAbnormalCallServiceButton = findViewById(R.id.activity_digital_key_master_key_abnormal_call_service_button);
        mMasterKeyAbnormalCallServiceButton.setOnClickListener(mOnMasterKeyAbnormalCallServiceButtonClickListener);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mMasterKeySubKeyOwnerRecyclerView.setLayoutManager(linearLayoutManager);

        if (mDigitalKeySubKeyOwnerAdapter == null) {
            mDigitalKeySubKeyOwnerAdapter = new DigitalKeySubKeyOwnerAdapter(this);
            mDigitalKeySubKeyOwnerAdapter.setOnInnerViewsClickListener(mOnMasterKeySubKeyOwnerRecyclerViewInnerViewsClickListener);
        }

        mMasterKeySubKeyOwnerRecyclerView.setAdapter(mDigitalKeySubKeyOwnerAdapter);

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mMasterKeyTempKeyOwnerRecyclerView.setLayoutManager(linearLayoutManager);

        if (mDigitalKeyTempKeyOwnerAdapter == null) {
            mDigitalKeyTempKeyOwnerAdapter = new DigitalKeyTempKeyOwnerAdapter(this);
            mDigitalKeyTempKeyOwnerAdapter.setOnInnerViewsClickListener(mOnMasterKeyTempKeyOwnerRecyclerViewInnerViewsClickListener);
        }

        mMasterKeyTempKeyOwnerRecyclerView.setAdapter(mDigitalKeyTempKeyOwnerAdapter);

        bindCaService();
    }

    @Override
    protected void onCaServiceConnected() {
        if (!mIsInitInProgress) {
            mIsInitInProgress = true;
            Prefs.setRefreshDigitalKeyListFlag(DigitalKeyActivity.this, false);
            init();
        }

        super.onCaServiceConnected();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_digital_key_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mOptionsMenu = menu;
        mOptionsMenu.setGroupVisible(0, false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if (!mIsMasterKeyActivationInProgress) {
                    finish();
                }
                return true;

            case R.id.action_edit:
                startActivity(EditDigitalKeyActivity.newIntent(this));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!mIsMasterKeyActivationInProgress) {
            super.onBackPressed();
        }
    }

    @Override
    public void onStart() {
        mIsActivityRunning = true;

        if (mIsInitDone) {
            if (Prefs.getRefreshDigitalKeyListFlag(DigitalKeyActivity.this) &&
                    mDigitalKeyMode == DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE) {
                Prefs.setRefreshDigitalKeyListFlag(DigitalKeyActivity.this, false);
                callAPIToGetKeyList();
            }
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        mIsActivityRunning = false;
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (mGetUserInfoAsyncTask != null) {
            mGetUserInfoAsyncTask.cancel(true);
        }

        if (mGetSelectedCarKeyDataAsyncTask != null) {
            mGetSelectedCarKeyDataAsyncTask.cancel(true);
        }

        unbindCaService();
        super.onDestroy();
    }

    @Override
    protected void addActionToReceiveBroadcastFromCaService(IntentFilter intentFilter) {
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_RENEW_SNAPKEY);
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_VIG_WAKEUP_SUCCESS);
    }

    @Override
    protected void onReceiveBroadcastFromCaService(Context context, Intent intent) {
        if (CommunicationAgentService.CA_STATUS_ADD_MASTERKEY.equals(intent.getAction())) {
            mIsMasterKeyActivationInProgress = false;

            int extraReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            if (extraReturnCode == CommunicationAgentService.CA_EXTRA_ADD_MASTERKEY_SUCCESS) {
                // Start to scan VIG
                Intent serviceIntent = new Intent(this, CommunicationAgentService.class);
                serviceIntent.putExtra(CommunicationAgentService.VIG_SCAN_VIG, true);
                startService(serviceIntent);

                Prefs.setCheckAllKeysActivationStatusFlag(DigitalKeyActivity.this, true);
                mDigitalKeyMode = DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE;

            } else {
                mDigitalKeyMode = DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE;
            }
            reloadSelectedCarKeyData();

            return;

        } else if (CommunicationAgentService.CA_STATUS_DEL_MASTERKEY.equals(intent.getAction())) {
            int extraReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            if (extraReturnCode == CommunicationAgentService.CA_EXTRA_DEL_MASTERKEY_COMPLETE) {
                mMasterKeyRealKeyType = CarOptions.KEY_NO;
                reloadSelectedCarKeyData();
            } else if (extraReturnCode == CommunicationAgentService.CA_EXTRA_DEL_MASTERKEY_FAILED ||
                    extraReturnCode == CommunicationAgentService.CA_EXTRA_DEL_MASTERKEY_NO_DATA) {
                mDigitalKeyMode = DIGITAL_KEY_MODE_MASTER_KEY_IN_USING;
                setUIDisplayByDigitalKeyMode();
                showDeleteMasterKeyErrorDialog();
            }

            return;

        } else if (CommunicationAgentService.CA_STATUS_RENEW_SNAPKEY.equals(intent.getAction())) {
            dismissProgressDialog();
            int extraReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            if (extraReturnCode == CommunicationAgentService.CA_EXTRA_RENEW_SNAPKEY_SUCCESS) {
                callAPIToGetKeyList();
            } else {
                showRenewSnapKeyErrorDialog();
            }

            return;

        } else if (CommunicationAgentService.CA_STATUS_VIG_WAKEUP_SUCCESS.equals(intent.getAction())) {
            switch (mDigitalKeyMode) {
                case DIGITAL_KEY_MODE_MASTER_KEY_IN_USING: {
                    dismissProgressDialog();
                    mDigitalKeyMode = DIGITAL_KEY_MODE_MASTER_KEY_IN_USING_DELETING;
                    setUIDisplayByDigitalKeyMode();
                    break;
                }

                case DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE: {
                    dismissProgressDialog();
                    break;
                }
            }

            return;

        } else if (CommunicationAgentService.CA_STATUS_VIG_WAKEUP_FAILED.equals(intent.getAction())) {
            switch (mDigitalKeyMode) {
                case DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE_CONNECT: {
                    mDigitalKeyMode = DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE;
                    setUIDisplayByDigitalKeyMode();
                    mIsMasterKeyActivationInProgress = false;
                    showConfirmDialog(getString(R.string.digital_key_master_key_dialog_vig_fail_title),
                            getString(R.string.digital_key_master_key_dialog_activate_master_key_vig_fail_message));
                    break;
                }

                case DIGITAL_KEY_MODE_MASTER_KEY_IN_USING: {
                    dismissProgressDialog();
                    showConfirmDialog(getString(R.string.digital_key_master_key_dialog_vig_fail_title),
                            getString(R.string.digital_key_master_key_dialog_delete_master_key_vig_fail_message));
                    break;
                }

                case DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE: {
                    dismissProgressDialog();
                    showVigConnectionFailWithBluetooth();
                    break;
                }

                default: {
                    showConfirmDialog(getString(R.string.dialog_title_vig_connect_fail),
                            getString(R.string.dialog_message_vig_connect_fail));
                }
            }

            return;

        } else if (CommunicationAgentService.CA_STATUS_ADD_SUBKEY.equals(intent.getAction())) {
            int extraReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            switch (mDigitalKeyMode) {
                case DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE: {
                    if (extraReturnCode == CommunicationAgentService.CA_EXTRA_ADD_SUBKEY_SUCCESS) {
                        showConfirmDialog(getString(R.string.add_new_sub_key_dialog_sub_key_sent_title),
                                getString(R.string.add_new_sub_key_dialog_sub_key_sent_message),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPIToGetKeyList();
                                    }
                                });
                    } else if (extraReturnCode == CommunicationAgentService.CA_EXTRA_ADD_SUBKEY_PREPARED_SUCCESS) {
                        showConfirmDialog(getString(R.string.add_new_sub_key_dialog_sub_key_prepared_sent_title),
                                getString(R.string.add_new_sub_key_dialog_sub_key_prepared_sent_message),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPIToGetKeyList();
                                    }
                                });
                    } else {
                        showConfirmDialog(getString(R.string.add_new_sub_key_dialog_add_sub_key_fail_title),
                                getString(R.string.add_new_sub_key_dialog_add_sub_key_fail_message),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPIToGetKeyList();
                                    }
                                });
                    }
                    break;
                }

                default: {
                    if (extraReturnCode == CommunicationAgentService.CA_EXTRA_ADD_SUBKEY_SUCCESS) {
                        showConfirmDialog(getString(R.string.add_new_sub_key_dialog_sub_key_sent_title),
                                getString(R.string.add_new_sub_key_dialog_sub_key_sent_message));
                    } else if (extraReturnCode == CommunicationAgentService.CA_EXTRA_ADD_SUBKEY_PREPARED_SUCCESS) {
                        showConfirmDialog(getString(R.string.add_new_sub_key_dialog_sub_key_prepared_sent_title),
                                getString(R.string.add_new_sub_key_dialog_sub_key_prepared_sent_message));
                    } else {
                        showConfirmDialog(getString(R.string.add_new_sub_key_dialog_add_sub_key_fail_title),
                                getString(R.string.add_new_sub_key_dialog_add_sub_key_fail_message));
                    }
                }
            }

            return;

        } else if (CommunicationAgentService.CA_STATUS_DEL_SUBKEY.equals(intent.getAction())) {
            int extraReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            switch (mDigitalKeyMode) {
                case DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE: {
                    if (extraReturnCode == CommunicationAgentService.CA_EXTRA_DEL_SUBKEY_SUCCESS_START) {
                        showConfirmDialog(getString(R.string.edit_digital_key_dialog_sub_key_deleted_title),
                                getString(R.string.edit_digital_key_dialog_sub_key_deleted_message),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPIToGetKeyList();
                                    }
                                });
                    } else if (extraReturnCode == CommunicationAgentService.CA_EXTRA_DEL_SUBKEY_PREPARED_SUCCESS) {
                        showConfirmDialog(getString(R.string.edit_digital_key_dialog_sub_key_prepared_deleted_title),
                                getString(R.string.edit_digital_key_dialog_sub_key_prepared_deleted_message),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPIToGetKeyList();
                                    }
                                });
                    } else {
                        showConfirmDialog(getString(R.string.edit_digital_key_dialog_delete_sub_key_fail_title),
                                getString(R.string.edit_digital_key_dialog_delete_sub_key_fail_message),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPIToGetKeyList();
                                    }
                                });
                    }

                    break;
                }

                default: {
                    if (extraReturnCode == CommunicationAgentService.CA_EXTRA_DEL_SUBKEY_SUCCESS_START) {
                        showConfirmDialog(getString(R.string.edit_digital_key_dialog_sub_key_deleted_title),
                                getString(R.string.edit_digital_key_dialog_sub_key_deleted_message));
                    } else if (extraReturnCode == CommunicationAgentService.CA_EXTRA_DEL_SUBKEY_PREPARED_SUCCESS) {
                        showConfirmDialog(getString(R.string.edit_digital_key_dialog_sub_key_prepared_deleted_title),
                                getString(R.string.edit_digital_key_dialog_sub_key_prepared_deleted_message));
                    } else {
                        showConfirmDialog(getString(R.string.edit_digital_key_dialog_delete_sub_key_fail_title),
                                getString(R.string.edit_digital_key_dialog_delete_sub_key_fail_message));
                    }
                }
            }

            return;

        } else if (CommunicationAgentService.CA_STATUS_ALLOCATE_SNAPKEY.equals(intent.getAction())) {
            int extraReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            switch (mDigitalKeyMode) {
                case DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE: {
                    if (extraReturnCode == CommunicationAgentService.CA_EXTRA_ALLOCATE_SNAPKEY_SUCCESS) {
                        showConfirmDialog(getString(R.string.set_temp_key_dialog_temp_key_sent_title),
                                getString(R.string.set_temp_key_dialog_temp_key_sent_message),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPIToGetKeyList();
                                    }
                                });
                    } else {
                        showConfirmDialog(getString(R.string.set_temp_key_dialog_set_temp_key_fail_title),
                                getString(R.string.set_temp_key_dialog_set_temp_key_fail_message));
                    }

                    break;
                }

                default: {
                    if (extraReturnCode == CommunicationAgentService.CA_EXTRA_ALLOCATE_SNAPKEY_SUCCESS) {
                        showConfirmDialog(getString(R.string.set_temp_key_dialog_temp_key_sent_title),
                                getString(R.string.set_temp_key_dialog_temp_key_sent_message));
                    } else {
                        showConfirmDialog(getString(R.string.set_temp_key_dialog_set_temp_key_fail_title),
                                getString(R.string.set_temp_key_dialog_set_temp_key_fail_message));
                    }
                }
            }

            return;

        } else if (CommunicationAgentService.CA_STATUS_TERMINATE_SNAPKEY.equals(intent.getAction())) {
            int extraReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            switch (mDigitalKeyMode) {
                case DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE: {
                    if (extraReturnCode == CommunicationAgentService.CA_EXTRA_TERMINATE_SNAPKEY_SUCCESS) {
                        showConfirmDialog(getString(R.string.edit_digital_key_dialog_temp_key_deleted_title),
                                getString(R.string.edit_digital_key_dialog_temp_key_deleted_message),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPIToGetKeyList();
                                    }
                                });
                    } else {
                        showConfirmDialog(getString(R.string.edit_digital_key_dialog_delete_temp_key_fail_title),
                                getString(R.string.edit_digital_key_dialog_delete_temp_key_fail_message));
                    }

                    break;
                }

                default: {
                    if (extraReturnCode == CommunicationAgentService.CA_EXTRA_TERMINATE_SNAPKEY_SUCCESS) {
                        showConfirmDialog(getString(R.string.edit_digital_key_dialog_temp_key_deleted_title),
                                getString(R.string.edit_digital_key_dialog_temp_key_deleted_message));
                    } else {
                        showConfirmDialog(getString(R.string.edit_digital_key_dialog_delete_temp_key_fail_title),
                                getString(R.string.edit_digital_key_dialog_delete_temp_key_fail_message));
                    }
                }
            }

            return;

        } else if (CommunicationAgentService.CA_STATUS_MASTERKEY_DELETED.equals(intent.getAction())) {
            String deletedVigCarId = intent.getStringExtra(CommunicationAgentService.CA_STATUS_EXTRA_COMMAND);
            if (mSelectedCarKeyData != null && mSelectedCarKeyData.isMasterKey() && mSelectedCarKeyData.getVigCarId().equals(deletedVigCarId)) {
                showConfirmDialog(getString(R.string.digital_key_master_key_dialog_receive_master_key_deleted_title),
                        getString(R.string.digital_key_master_key_dialog_receive_master_key_deleted_message),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mMasterKeyRealKeyType = CarOptions.KEY_NO;
                                reloadSelectedCarKeyData();
                            }
                        });

                Prefs.setRefreshCarKeyListFlag(this, Prefs.FLAG_REFRESH_CAR_KEY_LIST);
            } else {
                showConfirmDialog(getString(R.string.dialog_title_remind),
                        getString(R.string.dialog_message_master_key_deleted));

                // 不用做 Prefs.setRefreshCarKeyListFlag()，在選車的時候就會更新。
            }

            return;
        } else if (CommunicationAgentService.CA_STATUS_IKEY_FUNCTOSERVER_TIMEOUT.equals(intent.getAction())) {
            dismissProgressDialog();

            switch (mDigitalKeyMode) {
                case DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE_CONNECT: {
                    mDigitalKeyMode = DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE;
                    setUIDisplayByDigitalKeyMode();
                    mIsMasterKeyActivationInProgress = false;
                    break;
                }

                case DIGITAL_KEY_MODE_MASTER_KEY_IN_USING_DELETING: {
                    mDigitalKeyMode = DIGITAL_KEY_MODE_MASTER_KEY_IN_USING;
                    setUIDisplayByDigitalKeyMode();
                    break;
                }
            }

            LogCat.d("CA_STATUS_IKEY_FUNCTOSERVER_TIMEOUT");
            return;

        }

        super.onReceiveBroadcastFromCaService(context, intent);
    }

    private void init() {
        showProgressDialog();
        if (mGetUserInfoAsyncTask == null) {
            mGetUserInfoAsyncTask = new GetUserInfoAsyncTask();
            mGetUserInfoAsyncTask.execute();
        }
    }

    private void getSelectedCarKeyData() {
        showProgressDialog();
        if (mGetSelectedCarKeyDataAsyncTask == null) {
            mGetSelectedCarKeyDataAsyncTask = new GetSelectedCarKeyDataAsyncTask();
            mGetSelectedCarKeyDataAsyncTask.execute();
        }
    }

    private void reloadSelectedCarKeyData() {
        showProgressDialog();
        callAPIToGetCarKeyList();
    }

    private int getDigitalKeyMode() {
        if (mUserInfo != null && mSelectedCarKeyData != null && mCommunicationAgentService != null) {
            if (mCommunicationAgentService.isAddingMasterKey()) {
                return DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE_CONNECT;
            }

            if (mCommunicationAgentService.isDeletingMasterKey()) {
                return DIGITAL_KEY_MODE_MASTER_KEY_IN_USING_DELETING;
            }

            if (mSelectedCarKeyData.isMasterKey()) {
                // if (mSelectedCarKeyData.isMasterKeyM1()) {
                if (CarOptions.KEY_M1.equals(mMasterKeyRealKeyType)) {
                    return DIGITAL_KEY_MODE_MASTER_KEY_ABNORMAL;
                } else {
                    boolean isActive = false;
                    ArrayList<MasterKeyDataBean> masterKeyDataList = mCommunicationAgentService.getCA().getMasterKeyDataList();

                    for (MasterKeyDataBean bean : masterKeyDataList) {
                        if (bean.getUserId().equals(mUserInfo.getKeyUid()) &&
                                bean.getDeviceId().equals(mUserInfo.getDeviceId()) &&
                                bean.getCarId().equals(mSelectedCarKeyData.getVigCarId()) &&
                                bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                            isActive = true;
                            break;
                        }
                    }

                    if (isActive) { // 主鑰匙已啟動
                        if (mUserInfo.getDeviceId().equals(mSelectedCarKeyData.getPhoneId())) {
                            return DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE;
                        } else {
                            return DIGITAL_KEY_MODE_MASTER_KEY_IN_USING;
                        }
                    } else {
                        // if (mSelectedCarKeyData.isMasterKeyM2()) {
                        if (CarOptions.KEY_M2.equals(mMasterKeyRealKeyType)) {
                            // TODO
                            return DIGITAL_KEY_MODE_MASTER_KEY_IN_USING;
                            //return DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE;
                        } else {
                            // TODO
                            return DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE;
                            //return DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE;
                        }
                    }
                }
            }

            if (mSelectedCarKeyData.getKeyType().equals(CarOptions.KEY_S2)) {
                return DIGITAL_KEY_MODE_SUB_KEY;
            }

            if (mSelectedCarKeyData.getKeyType().equals(CarOptions.KEY_T2)) {
                return DIGITAL_KEY_MODE_TEMPORARY_KEY;
            }
        }

        return DIGITAL_KEY_MODE_NONE;
    }

    private void fetchDataByDigitalKeyMode() {
        switch (mDigitalKeyMode) {

            case DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE: {
                callAPIToGetKeyList();
                break;
            }

            case DIGITAL_KEY_MODE_SUB_KEY: {
                callAPIToGetMasterKeyUserData();
                break;
            }

            case DIGITAL_KEY_MODE_TEMPORARY_KEY: {
                callAPIToGetKeyList();
                break;
            }

            default: {
                mIsInitDone = true;
                mIsInitInProgress = false;
                dismissProgressDialog();
            }
        }
    }

    private void callAPIToGetKeyList() {
        showProgressDialog();
        VigRequestData data = new VigRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken(), mSelectedCarKeyData.getCarId());

        CarWebApi carWebApi = CarWebApi.getInstance(this);
        carWebApi.getKeyList(data, mGetKeyListApiListener);
    }

    private void callAPIToGetMasterKeyUserData() {
        showProgressDialog();
        GetMasterKeyUserRequestData data = new GetMasterKeyUserRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken(), mSelectedCarKeyData.getCarId());

        UserWebApi userWebApi = UserWebApi.getInstance(this);
        userWebApi.getMasterKeyUserData(data, mGetMasterKeyUserDataApiListener);
    }

    private void callAPIToGetCarKeyList() {
        if (mUserInfo != null) {
            GetCarKeyRequestData data = new GetCarKeyRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken());

            CarWebApi carWebApi = CarWebApi.getInstance(this);
            carWebApi.getCarKeyList(data, mGetCarKeyListApiListener);
        } else {
            switch (mDigitalKeyMode) {
                case DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE: {
                    showActivateMasterKeyErrorDialog();
                    break;
                }

                case DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE: {
                    showActivateMasterKeyErrorDialog();
                    break;
                }

                case DIGITAL_KEY_MODE_MASTER_KEY_IN_USING_DELETING: {
                    mDigitalKeyMode = DIGITAL_KEY_MODE_MASTER_KEY_IN_USING;
                    setUIDisplayByDigitalKeyMode();
                    showDeleteMasterKeyErrorDialog();
                    break;
                }
            }
        }
    }

    private void callCAServiceAPIToAddMasterKey() {
        if (mCommunicationAgentService != null) {
            mIsMasterKeyActivationInProgress = true;
            mCommunicationAgentService.addMasterKey();
        }
    }

    private void callCAServiceAPIToDeleteMasterKey() {
        if (mCommunicationAgentService != null) {
            mCommunicationAgentService.delMasterKey();
        }
    }

    private void callCAServiceAPIToRenewSnapKey() {
        if (mCommunicationAgentService != null) {
            mCommunicationAgentService.renewSnapTicket();
        } else {
            dismissProgressDialog();
        }
    }

    private void clearAP1AndRestoreDP1KeyStatusKeys() {
        ArrayList<KeyInfo> keyInfoList = mViewModel.getKeyInfoListFromDb(mSelectedCarKeyData.getCarId());
        for (KeyInfo keyInfo : keyInfoList) {
            if (keyInfo.getKeyStatus() == CarOptions.KEY_STATUS_AP1) {
                keyInfo.clear();
                mViewModel.updateKeyInfoToDb(keyInfo);
            } else if (keyInfo.getKeyStatus() == CarOptions.KEY_STATUS_DP1) {
                if (keyInfo.getKeyType().equals(CarOptions.KEY_S1)) {
                    keyInfo.setKeyStatus(CarOptions.KEY_STATUS_S1);
                } else if (keyInfo.getKeyType().equals(CarOptions.KEY_S2)) {
                    keyInfo.setKeyStatus(CarOptions.KEY_STATUS_S2);
                }
                mViewModel.updateKeyInfoToDb(keyInfo);
            }
        }
    }

    private void syncKeysFromKeyListInMasterKeyActiveMode(ArrayList<KeyData> keyList) {
        if (keyList != null) {
            KeyData[] subKeyDataArray = new KeyData[CommonDefs.DIGITAL_KEY_SUBKEY_OWNER_MAX_COUNT];
            KeyData[] tempKeyDataArray = new KeyData[CommonDefs.DIGITAL_KEY_TEMPKEY_OWNER_MAX_COUNT];
            ArrayList<KeyData> newAddKeyList = new ArrayList<>();
            ArrayList<KeyData> subKeyList = new ArrayList<>();
            ArrayList<KeyData> tempKeyList = new ArrayList<>();
            for (int index = 0; index < keyList.size(); index++) {
                KeyData keyData = keyList.get(index);
                LogCat.d("keydata.getKeyType(): " + keyData.getKeyType());
                LogCat.d("keydata.getName(): " + keyData.getName());
                LogCat.d("keydata.getPhoneNo(): " + keyData.getPhoneNo());
                if (keyData.getKeyType().equals(CarOptions.KEY_S1) || keyData.getKeyType().equals(CarOptions.KEY_S2)) {
                    subKeyList.add(keyData);
                } else if (keyData.getKeyType().equals(CarOptions.KEY_T1) || keyData.getKeyType().equals(CarOptions.KEY_T2)) {
                    tempKeyList.add(keyData);
                }
            }

            /**
             * MaterKey position is 0.
             * SubKeys are starting from 1 and ended at 4.
             * SnapKeys are starting from 5 and ended at 6.
             */
            ArrayList<KeyInfo> keyInfoList = mViewModel.getKeyInfoListFromDb(mSelectedCarKeyData.getCarId());

            // Deal with subkey
            for (int index = 0; index < subKeyList.size(); index++) {
                boolean isMatched = false;
                KeyData keyData = subKeyList.get(index);
                for (int position = 1; position < 5; position++) {
                    KeyInfo keyInfo = keyInfoList.get(position);
                    if (keyData.getKeyUid().equals(keyInfo.getUserId()) &&
                            keyData.getPhoneId().equals(keyInfo.getDeviceId())) {
                        if(TextUtils.isEmpty(keyData.getDeleteKeyStatus())) {
                            if(keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_D1) {
                                subKeyDataArray[position - 1] = keyData;
                                isMatched = true;
                                break;
                            }
                        } else {
                            if(keyInfo.getKeyStatus() == CarOptions.KEY_STATUS_D1) {
                                subKeyDataArray[position - 1] = keyData;
                                isMatched = true;
                                break;
                            }
                        }
                    }
                }

                if (!isMatched) {
                    newAddKeyList.add(keyData);
                }
            }

            // Sync key list to database
            for (int index = 0; index < subKeyDataArray.length; index++) {
                KeyData keyData = subKeyDataArray[index];
                KeyInfo keyInfo = keyInfoList.get(index + 1);
                if (keyData == null) {
                    if (keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_DEF &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_AP1 &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_AP2 &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_AP2S1 &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_DP1 &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_DP2 &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_DP2D1) {
                        keyInfo.clear();
                        mViewModel.updateKeyInfoToDb(keyInfo);
                    }
                } else {
                    if (keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_DP1 &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_DP2 &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_DP2D1) {
                        syncAndUpdateKeyDataToDB(keyData, keyInfo);
                    }
                }
            }

            // 補到空位
            for (int index = 0; index < newAddKeyList.size(); index++) {
                KeyData keyData = newAddKeyList.get(index);
                for (int position = 1; position < 5; position++) {
                    KeyInfo keyInfo = keyInfoList.get(position);
                    if (keyInfo.getKeyStatus() == CarOptions.KEY_STATUS_DEF) {
                        syncAndUpdateKeyDataToDB(keyData, keyInfo);
                        break;
                    }
                }
            }

            // Deal with temp key
            boolean hasSnapTicket = false;

            IKeyDataBean iKeyDataBean = getIKeyDataBean(mSelectedCarKeyData.getVigCarId());
            if (iKeyDataBean != null && iKeyDataBean.getSnapTicketArray() != null) {
                ArrayList<SnapTicket> snapTicketList = mCommunicationAgentService.getCA().getSnapTicketByIKeyDataBean(iKeyDataBean);
                if (snapTicketList.size() >= CommonDefs.DIGITAL_KEY_TEMPKEY_OWNER_MAX_COUNT) {
                    hasSnapTicket = true;
                }
            }

            newAddKeyList.clear();

            for (int index = 0; index < tempKeyList.size(); index++) {
                boolean isMatched = false;
                KeyData keyData = tempKeyList.get(index);
                for (int position = 5; position < 7; position++) {
                    KeyInfo keyInfo = keyInfoList.get(position);
                    if (keyData.getKeyUid().equals(keyInfo.getUserId()) &&
                            keyData.getPhoneId().equals(keyInfo.getDeviceId())) {
                        tempKeyDataArray[position - 5] = keyData;
                        isMatched = true;
                        break;
                    }
                }

                if (!isMatched) {
                    newAddKeyList.add(keyData);
                }
            }

            // Sync key list to database
            for (int index = 0; index < tempKeyDataArray.length; index++) {
                KeyData keyData = tempKeyDataArray[index];
                KeyInfo keyInfo = keyInfoList.get(index + 5);
                if (keyData == null) {
                    if (keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_SNAP_TERMINATE &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_D1) {
                        keyInfo.clear();
                        if (hasSnapTicket) {
                            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_SNAP_NORMAL);
                        } else {
                            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_SNAP_INIT);
                        }
                        mViewModel.updateKeyInfoToDb(keyInfo);
                    }
                } else {
                    syncAndUpdateKeyDataToDB(keyData, keyInfo);
                }
            }

            // 補到空位
            for (int index = 0; index < newAddKeyList.size(); index++) {
                KeyData keyData = newAddKeyList.get(index);
                for (int position = 5; position < 7; position++) {
                    KeyInfo keyInfo = keyInfoList.get(position);
                    if (keyInfo.getKeyStatus() == CarOptions.KEY_STATUS_SNAP_INIT ||
                            keyInfo.getKeyStatus() == CarOptions.KEY_STATUS_SNAP_NORMAL) {
                        syncAndUpdateKeyDataToDB(keyData, keyInfo);
                        break;
                    }
                }
            }

            mDigitalKeySubKeyOwnerAdapter.enableKeyAdding(true);
            mDigitalKeyTempKeyOwnerAdapter.enableKeySetting(true);
            buildKeysFromKeyListInMasterKeyActiveMode();
        }
    }

    private void buildKeysFromKeyListInMasterKeyActiveMode() {
        /**
         * MaterKey position is 0.
         * SubKeys are starting from 1 and ended at 4.
         * SnapKeys are starting from 5 and ended at 6.
         */
        ArrayList<KeyInfo> keyInfoList = mViewModel.getKeyInfoListFromDb(mSelectedCarKeyData.getCarId());

        for (int position = 1; position < 5; position++) {
            KeyInfo keyInfo = keyInfoList.get(position);
            switch (keyInfo.getKeyStatus()) {
                case CarOptions.KEY_STATUS_DEF: {
                    mDigitalKeySubKeyOwnerAdapter.buildItemData(null, null, DigitalKeySubKeyOwnerAdapter.DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_ADDNEW, true);
                    break;
                }

                case CarOptions.KEY_STATUS_AP2: {
                    mDigitalKeySubKeyOwnerAdapter.buildItemData(keyInfo, null, DigitalKeySubKeyOwnerAdapter.DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_OWNER_PREPARE_ADD, true);
                    break;
                }

                case CarOptions.KEY_STATUS_DP2: {
                    mDigitalKeySubKeyOwnerAdapter.buildItemData(keyInfo, null, DigitalKeySubKeyOwnerAdapter.DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_OWNER_PREPARE_DELETE, true);
                    break;
                }

                default: {
                    mDigitalKeySubKeyOwnerAdapter.buildItemData(keyInfo, null, DigitalKeySubKeyOwnerAdapter.DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_OWNER, true);
                }
            }
        }

        mDigitalKeySubKeyOwnerAdapter.commitBuildItemData();
        mDigitalKeySubKeyOwnerAdapter.notifyDataSetChanged();

        for (int position = 5; position < 7; position++) {
            KeyInfo keyInfo = keyInfoList.get(position);
            switch (keyInfo.getKeyStatus()) {
                case CarOptions.KEY_STATUS_DEF: {
                    mDigitalKeyTempKeyOwnerAdapter.buildItemData(null, null, DigitalKeyTempKeyOwnerAdapter.DIGITALKEY_TEMPKEY_OWNER_ITEM_TYPE_RENEW, true);
                    break;
                }

                case CarOptions.KEY_STATUS_SNAP_INIT: {
                    mDigitalKeyTempKeyOwnerAdapter.buildItemData(null, null, DigitalKeyTempKeyOwnerAdapter.DIGITALKEY_TEMPKEY_OWNER_ITEM_TYPE_RENEW, true);
                    break;
                }

                case CarOptions.KEY_STATUS_SNAP_NORMAL: {
                    mDigitalKeyTempKeyOwnerAdapter.buildItemData(null, null, DigitalKeyTempKeyOwnerAdapter.DIGITALKEY_TEMPKEY_OWNER_ITEM_TYPE_SET, true);
                    break;
                }

                case CarOptions.KEY_STATUS_SNAP_TERMINATE: {
                    mDigitalKeyTempKeyOwnerAdapter.buildItemData(null, null, DigitalKeyTempKeyOwnerAdapter.DIGITALKEY_TEMPKEY_OWNER_ITEM_TYPE_RENEW, true);
                    break;
                }

                default: {
                    if (System.currentTimeMillis() > keyInfo.getSnapKeyEndTime()) {
                        mDigitalKeyTempKeyOwnerAdapter.buildItemData(null, null, DigitalKeyTempKeyOwnerAdapter.DIGITALKEY_TEMPKEY_OWNER_ITEM_TYPE_RENEW, true);
                    } else {
                        mDigitalKeyTempKeyOwnerAdapter.buildItemData(keyInfo, null, DigitalKeySubKeyOwnerAdapter.DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_OWNER, true);
                    }
                }
            }
        }

        mDigitalKeyTempKeyOwnerAdapter.commitBuildItemData();
        mDigitalKeyTempKeyOwnerAdapter.notifyDataSetChanged();
    }

    private void syncAndUpdateKeyDataToDB(KeyData keyData, KeyInfo keyInfo) {
        keyInfo.setUserId(keyData.getKeyUid());
        keyInfo.setPhone(keyData.getPhoneNo());
        keyInfo.setName(keyData.getName());
        keyInfo.setEmail(keyData.getEmail());
        keyInfo.setKeyType(keyData.getKeyType());
        keyInfo.setDeviceId(keyData.getPhoneId());
        if (keyData.getDeleteKeyStatus() != null) {
            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_D1);
        } else if (keyData.getKeyType().equals(CarOptions.KEY_S1)) {
            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_S1);
        } else if (keyData.getKeyType().equals(CarOptions.KEY_S2)) {
            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_S2);
        } else if (keyData.getKeyType().equals(CarOptions.KEY_T1)) {
            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_T1);
        } else if (keyData.getKeyType().equals(CarOptions.KEY_T2)) {
            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_T2);
        }
        try {
            if (keyData.getKeyStartDate() != null && keyData.getKeyEndDate() != null) {
                keyInfo.setSnapKeyStartTime(mSimpleDateFormatForServer.parse(keyData.getKeyStartDate()).getTime());
                keyInfo.setSnapKeyEndTime(mSimpleDateFormatForServer.parse(keyData.getKeyEndDate()).getTime());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        mViewModel.updateKeyInfoToDb(keyInfo);
    }

    private IKeyDataBean getIKeyDataBean(String vigCarId) {
        ArrayList<MasterKeyDataBean> masterKeyDataList = mCommunicationAgentService.getCA().getMasterKeyDataList();
        if (masterKeyDataList.size() > 0) {
            for (MasterKeyDataBean bean : masterKeyDataList) {
                if (bean.getCarId().equalsIgnoreCase(vigCarId) &&
                        bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                    return bean;
                }
            }
        } else {
            ArrayList<SubKeyDataBean> subKeyDataBeanList = mCommunicationAgentService.getCA().getSubKeyDataList();
            if (subKeyDataBeanList.size() > 0) {
                for (SubKeyDataBean bean : subKeyDataBeanList) {
                    if (bean.getCarId().equalsIgnoreCase(vigCarId) &&
                            bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                        return bean;
                    }
                }
            } else {
                ArrayList<SnapKeyDataBean> snapKeyDataBeanList = mCommunicationAgentService.getCA().getSnapKeyDataListByCarId(vigCarId);
                if (snapKeyDataBeanList.size() > 0) {
                    for (SnapKeyDataBean bean : snapKeyDataBeanList) {
                        if (bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                            return bean;
                        }
                    }
                }
            }
        }
        return null;
    }

    private void updateDisplayRelatedToMasterKeyUserDataInSubKeyMode(GetMasterKeyUserResponseData data) {
        mSubKeyOwnerTextView.setText(data.getName());
    }

    private void updateDisplayRelatedToKeyListInTempKeyMode(ArrayList<KeyData> keyList) {
        if (keyList != null) {
            for (int index = 0; index < keyList.size(); index++) {
                KeyData keyData = keyList.get(index);
                LogCat.e("keydata.getKeyUid(): " + keyData.getKeyUid());
                LogCat.e("keydata.getKeyType(): " + keyData.getKeyType());
                LogCat.e("keydata.getName(): " + keyData.getName());
                LogCat.e("keydata.getPhoneNo(): " + keyData.getPhoneNo());
                if (keyData.getKeyType().equals(CarOptions.KEY_T2) && keyData.getKeyUid().equals(mUserInfo.getKeyUid())) {
                    String holdingPeriodString = getString(R.string.digital_key_holding_period);
                    holdingPeriodString = String.format(holdingPeriodString, keyData.getKeyStartDate(), keyData.getKeyEndDate());
                    mTempKeyHoldingPeriodTextView.setText(holdingPeriodString);
                    break;
                }
            }
        }
    }

    private void updateDisplayRelatedToMasterKeyUserDataInTempKeyMode(GetMasterKeyUserResponseData data) {
        mTempKeyOwnerTextView.setText(data.getName());
    }

    private void updateDisplayRelatedToSelectedCarKeyData() {
        String carModel = null;
        if(mSelectedCarKeyData != null) {
            carModel = mSelectedCarKeyData.getCarModel();
        }

        if (carModel != null) {
            mMasterKeyCarModelTextView.setText(carModel);
            mSubKeyCarModelTextView.setText(carModel);
            mTempKeyCarModelTextView.setText(carModel);
        } else {
            mMasterKeyCarModelTextView.setText(R.string.common_car_model_none);
            mSubKeyCarModelTextView.setText(R.string.common_car_model_none);
            mTempKeyCarModelTextView.setText(R.string.common_car_model_none);
        }

        if (mSelectedCarKeyData != null) {
            mMasterKeyPlateNumberTextView.setText(mSelectedCarKeyData.getCarNo());
            mSubKeyPlateNumberTextView.setText(mSelectedCarKeyData.getCarNo());
            mTempKeyPlateNumberTextView.setText(mSelectedCarKeyData.getCarNo());
        } else {
            mMasterKeyPlateNumberTextView.setText(R.string.common_car_plate_number_none);
            mSubKeyPlateNumberTextView.setText(R.string.common_car_plate_number_none);
            mTempKeyPlateNumberTextView.setText(R.string.common_car_plate_number_none);
        }

    }

    private void setUIDisplayByDigitalKeyMode() {
        switch (mDigitalKeyMode) {
            case DIGITAL_KEY_MODE_MASTER_KEY_ACTIVE: {
                mMasterKeyLayout.setVisibility(View.VISIBLE);
                mMasterKeyInactiveLayout.setVisibility(View.GONE);
                mMasterKeyInactiveConnectLayout.setVisibility(View.GONE);
                mMasterKeyInUsingLayout.setVisibility(View.GONE);
                mMasterKeyInUsingDeletingLayout.setVisibility(View.GONE);
                mMasterKeyAbnormalLayout.setVisibility(View.GONE);
                mSubKeyLayout.setVisibility(View.GONE);
                mTempKeyLayout.setVisibility(View.GONE);
                mOptionsMenu.setGroupVisible(0, true);

                break;
            }

            case DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE: {
                mMasterKeyLayout.setVisibility(View.GONE);
                mMasterKeyInactiveLayout.setVisibility(View.VISIBLE);
                mMasterKeyInactiveConnectLayout.setVisibility(View.GONE);
                mMasterKeyInUsingLayout.setVisibility(View.GONE);
                mMasterKeyInUsingDeletingLayout.setVisibility(View.GONE);
                mMasterKeyAbnormalLayout.setVisibility(View.GONE);
                mSubKeyLayout.setVisibility(View.GONE);
                mTempKeyLayout.setVisibility(View.GONE);
                mOptionsMenu.setGroupVisible(0, false);

                if (!Prefs.getPreferenceKeyValue(this, Prefs.KEY_NO_LONGER_REMIND_BT_IN_DIGITAL_KEY_INACTIVE)) {
                    showNoLongerRemindDialog(getString(R.string.dialog_message_bt_4g_enable_check), Prefs.KEY_NO_LONGER_REMIND_BT_IN_DIGITAL_KEY_INACTIVE);
                }

                break;
            }

            case DIGITAL_KEY_MODE_MASTER_KEY_INACTIVE_CONNECT: {
                mMasterKeyLayout.setVisibility(View.GONE);
                mMasterKeyInactiveLayout.setVisibility(View.GONE);
                mMasterKeyInactiveConnectLayout.setVisibility(View.VISIBLE);
                mMasterKeyInUsingLayout.setVisibility(View.GONE);
                mMasterKeyInUsingDeletingLayout.setVisibility(View.GONE);
                mMasterKeyAbnormalLayout.setVisibility(View.GONE);
                mSubKeyLayout.setVisibility(View.GONE);
                mTempKeyLayout.setVisibility(View.GONE);
                mOptionsMenu.setGroupVisible(0, false);

                break;
            }

            case DIGITAL_KEY_MODE_MASTER_KEY_IN_USING: {
                mMasterKeyLayout.setVisibility(View.GONE);
                mMasterKeyInactiveLayout.setVisibility(View.GONE);
                mMasterKeyInactiveConnectLayout.setVisibility(View.GONE);
                mMasterKeyInUsingLayout.setVisibility(View.VISIBLE);
                mMasterKeyInUsingDeletingLayout.setVisibility(View.GONE);
                mMasterKeyAbnormalLayout.setVisibility(View.GONE);
                mSubKeyLayout.setVisibility(View.GONE);
                mTempKeyLayout.setVisibility(View.GONE);
                mOptionsMenu.setGroupVisible(0, false);

                if (!Prefs.getPreferenceKeyValue(this, Prefs.KEY_NO_LONGER_REMIND_BT_IN_DIGITAL_KEY_INACTIVE)) {
                    showNoLongerRemindDialog(getString(R.string.dialog_message_bt_4g_enable_check), Prefs.KEY_NO_LONGER_REMIND_BT_IN_DIGITAL_KEY_INACTIVE);
                }

                break;
            }

            case DIGITAL_KEY_MODE_MASTER_KEY_IN_USING_DELETING: {
                mMasterKeyLayout.setVisibility(View.GONE);
                mMasterKeyInactiveLayout.setVisibility(View.GONE);
                mMasterKeyInactiveConnectLayout.setVisibility(View.GONE);
                mMasterKeyInUsingLayout.setVisibility(View.GONE);
                mMasterKeyInUsingDeletingLayout.setVisibility(View.VISIBLE);
                mMasterKeyAbnormalLayout.setVisibility(View.GONE);
                mSubKeyLayout.setVisibility(View.GONE);
                mTempKeyLayout.setVisibility(View.GONE);
                mOptionsMenu.setGroupVisible(0, false);

                break;
            }

            case DIGITAL_KEY_MODE_MASTER_KEY_ABNORMAL: {
                mMasterKeyLayout.setVisibility(View.GONE);
                mMasterKeyInactiveLayout.setVisibility(View.GONE);
                mMasterKeyInactiveConnectLayout.setVisibility(View.GONE);
                mMasterKeyInUsingLayout.setVisibility(View.GONE);
                mMasterKeyInUsingDeletingLayout.setVisibility(View.GONE);
                mMasterKeyAbnormalLayout.setVisibility(View.VISIBLE);
                mSubKeyLayout.setVisibility(View.GONE);
                mTempKeyLayout.setVisibility(View.GONE);
                mOptionsMenu.setGroupVisible(0, false);

                break;
            }

            case DIGITAL_KEY_MODE_SUB_KEY: {
                mMasterKeyLayout.setVisibility(View.GONE);
                mMasterKeyInactiveLayout.setVisibility(View.GONE);
                mMasterKeyInactiveConnectLayout.setVisibility(View.GONE);
                mMasterKeyInUsingLayout.setVisibility(View.GONE);
                mMasterKeyInUsingDeletingLayout.setVisibility(View.GONE);
                mMasterKeyAbnormalLayout.setVisibility(View.GONE);
                mSubKeyLayout.setVisibility(View.VISIBLE);
                mTempKeyLayout.setVisibility(View.GONE);
                mOptionsMenu.setGroupVisible(0, false);

                break;
            }

            case DIGITAL_KEY_MODE_TEMPORARY_KEY: {
                mMasterKeyLayout.setVisibility(View.GONE);
                mMasterKeyInactiveLayout.setVisibility(View.GONE);
                mMasterKeyInactiveConnectLayout.setVisibility(View.GONE);
                mMasterKeyInUsingLayout.setVisibility(View.GONE);
                mMasterKeyInUsingDeletingLayout.setVisibility(View.GONE);
                mMasterKeyAbnormalLayout.setVisibility(View.GONE);
                mSubKeyLayout.setVisibility(View.GONE);
                mTempKeyLayout.setVisibility(View.VISIBLE);
                mOptionsMenu.setGroupVisible(0, false);

                break;
            }

            default: {
                mMasterKeyLayout.setVisibility(View.GONE);
                mMasterKeyInactiveLayout.setVisibility(View.GONE);
                mMasterKeyInactiveConnectLayout.setVisibility(View.GONE);
                mMasterKeyInUsingLayout.setVisibility(View.GONE);
                mMasterKeyInUsingDeletingLayout.setVisibility(View.GONE);
                mMasterKeyAbnormalLayout.setVisibility(View.GONE);
                mSubKeyLayout.setVisibility(View.GONE);
                mTempKeyLayout.setVisibility(View.GONE);
                mOptionsMenu.setGroupVisible(0, false);
            }
        }
    }

    private void showApplyMasterKeyDialog() {
        if (mAlertDialog1.isShowing()) {
            mAlertDialog1.dismiss();
        }
        mAlertDialog1.restoreDefault();
        mAlertDialog1.setTitle(getString(R.string.digital_key_master_key_dialog_apply_master_key_title));
        mAlertDialog1.setMessage(getString(R.string.digital_key_master_key_dialog_apply_master_key_message));
        mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                getString(R.string.btn_confirm),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        showProgressDialog(getString(R.string.digital_key_master_key_progress_dialog_connect_message));
                        callCAServiceAPIToDeleteMasterKey();
                        mAlertDialog1.dismiss();
                    }
                });
        mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                getString(R.string.btn_cancel),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mAlertDialog1.dismiss();
                    }
                });
        mAlertDialog1.show();
    }

    private void checkKeylessSettings() {
        if (!BeaconOptions.LEARNT.equals(mSelectedCarKeyData.getBeaconLearningStatus()) &&
                !BeaconOptions.READ.equals(mSelectedCarKeyData.getBeaconLearningStatus()) &&
                mSelectedCarKeyData.isMasterKey()) {
            if (mAlertDialog1.isShowing()) {
                mAlertDialog1.dismiss();
            }
            mAlertDialog1.restoreDefault();
            mAlertDialog1.setTitle(getString(R.string.digital_key_master_key_dialog_keyless_is_incomplete_title));
            mAlertDialog1.setMessage(getString(R.string.digital_key_master_key_dialog_keyless_is_incomplete_message));
            mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                    getString(R.string.btn_confirm),
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            mAlertDialog1.dismiss();
                            startActivity(KeylessSettingsActivity.newIntent(DigitalKeyActivity.this));
                        }
                    });
            mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                    getString(R.string.btn_cancel),
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            mAlertDialog1.dismiss();
                            callAPIToSetBeaconLearningStatus();
                        }
                    });
            mAlertDialog1.show();
        }
    }

    private void callAPIToSetBeaconLearningStatus() {
        showProgressDialog();
        SetBeaconLearingStatusRequestData data = new SetBeaconLearingStatusRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken(), mSelectedCarKeyData.getCarId());

        CarWebApi carWebApi = CarWebApi.getInstance(this);
        carWebApi.setBeaconLearingStatus(data, mSetBeaconLearningStatusApiListener);
    }

    private void showActivateMasterKeyErrorDialog() {
        showConfirmDialog(getString(R.string.digital_key_master_key_dialog_activate_master_key_fail_title),
                getString(R.string.digital_key_master_key_dialog_activate_master_key_fail_message));
    }

    private void showMasterKeyDeletedDialog() {
        showConfirmDialog(getString(R.string.digital_key_master_key_dialog_master_key_deleted_title),
                getString(R.string.digital_key_master_key_dialog_master_key_deleted_message));
    }

    private void showDeleteMasterKeyErrorDialog() {
        showConfirmDialog(getString(R.string.digital_key_master_key_dialog_delete_master_key_fail_title),
                getString(R.string.digital_key_master_key_dialog_delete_master_key_fail_message));
    }

    private void showRenewSnapKeyErrorDialog() {
        showConfirmDialog(getString(R.string.digital_key_master_key_dialog_renew_temp_key_fail_title),
                getString(R.string.digital_key_master_key_dialog_renew_temp_key_fail_message));
    }

    private void showVigConnectionFailWithBluetooth() {
        if (mAlertDialog1.isShowing()) {
            mAlertDialog1.dismiss();
        }
        mAlertDialog1.restoreDefault();

        if (mTargetKeyInfo.getKeyStatus() == CarOptions.KEY_STATUS_AP2) {
            mAlertDialog1.setTitle(getString(R.string.dialog_title_vig_connect_fail));
            mAlertDialog1.setMessage(getString(R.string.dialog_message_vig_connect_fail_with_bluetooth));
            mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                    getResources().getString(R.string.btn_confirm),
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            mAlertDialog1.dismiss();
                        }
                    });
            mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                    getResources().getString(R.string.btn_near_end_connection),
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            Prefs.setRefreshDigitalKeyListFlag(DigitalKeyActivity.this, true);
                            Intent intent = NearEndConnectionTeachingActivity.newIntentToAddSubKey(DigitalKeyActivity.this,
                                    mKeyPosition, mTargetKeyInfo.getUserId(), mTargetKeyInfo.getDeviceId(),
                                    mTargetKeyInfo.getDeviceCreateTime(), mTargetKeyInfo.getDisplayName());
                            startActivity(intent);
                            mAlertDialog1.dismiss();
                        }
                    });
            mAlertDialog1.show();

        } else if (mTargetKeyInfo.getKeyStatus() == CarOptions.KEY_STATUS_DP2) {
            mAlertDialog1.setTitle(getString(R.string.dialog_title_vig_connect_fail));
            mAlertDialog1.setMessage(getString(R.string.dialog_message_vig_connect_fail_with_bluetooth));
            mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                    getResources().getString(R.string.btn_confirm),
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            mAlertDialog1.dismiss();
                        }
                    });
            mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                    getResources().getString(R.string.btn_near_end_connection),
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            Prefs.setRefreshDigitalKeyListFlag(DigitalKeyActivity.this, true);
                            Intent intent = NearEndConnectionTeachingActivity.newIntentToDeleteSubKey(DigitalKeyActivity.this,
                                    mTargetKeyInfo.getKeyType(), mTargetKeyInfo.getUserId(), mTargetKeyInfo.getDeviceId(), mTargetKeyInfo.getDeviceCreateTime());
                            startActivity(intent);
                            mAlertDialog1.dismiss();
                        }
                    });
            mAlertDialog1.show();
        }
    }
}
