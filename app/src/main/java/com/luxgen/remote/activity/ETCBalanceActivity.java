package com.luxgen.remote.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomAppCompatActivity;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.assistant.GetEtcRequestData;
import com.luxgen.remote.network.model.assistant.GetEtcResponseData;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.util.Prefs;

public class ETCBalanceActivity extends CustomAppCompatActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, ETCBalanceActivity.class);
    }

    private static final int WANT_TO_DO_NOTHING = 0;
    private static final int WANT_TO_SAVE_PLATE_NUMBER_WITH_DASH = 1;
    private static final int WANT_TO_CLEAR_PLATE_NUMBER_WITH_DASH = 2;

    private GetUserInfoAsyncTask mGetUserInfoAsyncTask = null;
    private GetSelectedCarKeyDataAsyncTask mGetSelectedCarKeyDataAsyncTask = null;
    private UserInfo mUserInfo = null;
    private CarKeyData mSelectedCarKeyData = null;

    private EditText mDialogEtcOwnerPlateNumberPreSectionEditText = null;
    private EditText mDialogEtcOwnerPlateNumberRearSectionEditText = null;
    private EditText mDialogEtcAuthenticationIDCardNumberEditText = null;
    private EditText mDialogEtcAuthenticationPlateNumberPreSectionEditText = null;
    private EditText mDialogEtcAuthenticationPlateNumberRearSectionEditText = null;

    private int mWhatWantToDoAfterGettingEtcSuccess = WANT_TO_DO_NOTHING;

    private String mPlateNumberWithDash = null;

    private class GetUserInfoAsyncTask extends AsyncTask<Void, Void, UserInfo> {

        @Override
        protected UserInfo doInBackground(Void... voids) {
            return Prefs.getUserInfo(ETCBalanceActivity.this);
        }

        @Override
        protected void onPostExecute(UserInfo userInfo) {
            mGetUserInfoAsyncTask = null;

            mUserInfo = userInfo;
            getSelectedCarKeyData();
        }
    }

    private class GetSelectedCarKeyDataAsyncTask extends AsyncTask<Void, Void, CarKeyData> {

        @Override
        protected CarKeyData doInBackground(Void... voids) {
            CarKeyData selectedCarKeyData = Prefs.getCarKeyData(ETCBalanceActivity.this);
            return selectedCarKeyData;
        }

        @Override
        protected void onPostExecute(CarKeyData selectedCarKeyData) {
            mGetSelectedCarKeyDataAsyncTask = null;

            mSelectedCarKeyData = selectedCarKeyData;
            getEtcBalance();
        }
    }

    View.OnClickListener mOnDialogEtcOwnerPlateNumberPositiveButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            String plateNumberPreSection = mDialogEtcOwnerPlateNumberPreSectionEditText.getText().toString().trim().toUpperCase();
            String plateNumberRearSection = mDialogEtcOwnerPlateNumberRearSectionEditText.getText().toString().trim().toUpperCase();
            if (TextUtils.isEmpty(plateNumberPreSection)) {
                mDialogEtcOwnerPlateNumberPreSectionEditText.setError(getResources().getString(R.string.etc_balance_dialog_etc_input_error));
            } else if (TextUtils.isEmpty(plateNumberRearSection)) {
                mDialogEtcOwnerPlateNumberRearSectionEditText.setError(getResources().getString(R.string.etc_balance_dialog_etc_input_error));
            } else {
                showProgressDialog();
                mPlateNumberWithDash = plateNumberPreSection + "-" + plateNumberRearSection;
                callAPIToGetEtcBalance(mPlateNumberWithDash,
                        null,
                        mUserInfo.getKeyUid(),
                        mUserInfo.getToken(),
                        mSelectedCarKeyData.getCarId());
                mAlertDialog1.dismiss();
            }
        }
    };

    View.OnClickListener mOnDialogEtcOwnerPlateNumberConfirmPositiveButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            showProgressDialog();
            callAPIToGetEtcBalance(mPlateNumberWithDash,
                    null,
                    mUserInfo.getKeyUid(),
                    mUserInfo.getToken(),
                    mSelectedCarKeyData.getCarId());
            mAlertDialog1.dismiss();
        }
    };

    View.OnClickListener mOnDialogEtcAuthenticationPositiveButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            String idCardNumber = mDialogEtcAuthenticationIDCardNumberEditText.getText().toString().trim().toUpperCase();
            String plateNumberPreSection = mDialogEtcAuthenticationPlateNumberPreSectionEditText.getText().toString().trim().toUpperCase();
            String plateNumberRearSection = mDialogEtcAuthenticationPlateNumberRearSectionEditText.getText().toString().trim().toUpperCase();
            if (TextUtils.isEmpty(idCardNumber)) {
                mDialogEtcAuthenticationIDCardNumberEditText.setError(getResources().getString(R.string.etc_balance_dialog_etc_input_error));
            } else if (TextUtils.isEmpty(plateNumberPreSection)) {
                mDialogEtcAuthenticationPlateNumberPreSectionEditText.setError(getResources().getString(R.string.etc_balance_dialog_etc_input_error));
            } else if (TextUtils.isEmpty(plateNumberRearSection)) {
                mDialogEtcAuthenticationPlateNumberRearSectionEditText.setError(getResources().getString(R.string.etc_balance_dialog_etc_input_error));
            } else {
                showProgressDialog();
                if (mSelectedCarKeyData != null && mSelectedCarKeyData.isOwner()) {
                    mWhatWantToDoAfterGettingEtcSuccess = WANT_TO_CLEAR_PLATE_NUMBER_WITH_DASH;
                }
                String plateNumber = plateNumberPreSection + "-" + plateNumberRearSection;
                callAPIToGetEtcBalance(plateNumber, idCardNumber, null, null, null);
                mAlertDialog1.dismiss();
            }
        }
    };

    View.OnClickListener mOnDialogErrorPositiveButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            finish();
            overridePendingTransition(0, 0);
        }
    };

    private OnApiListener<GetEtcResponseData> mGetEtcApiListener = new OnApiListener<GetEtcResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetEtcResponseData responseData) {
            dismissProgressDialog();
            if (responseData.isSuccessful()) {
                switch (mWhatWantToDoAfterGettingEtcSuccess) {
                    case WANT_TO_SAVE_PLATE_NUMBER_WITH_DASH: {
                        Prefs.setPlateNumberComparison(ETCBalanceActivity.this, mSelectedCarKeyData.getCarNo(), mPlateNumberWithDash);
                        break;
                    }

                    case WANT_TO_CLEAR_PLATE_NUMBER_WITH_DASH: {
                        Prefs.clearPlateNumberComparison(ETCBalanceActivity.this, mSelectedCarKeyData.getCarNo());
                    }
                }

                showEtcAVABalanceDialog(responseData.getAvaBalance());
            } else {
                showServerErrorDialog(getResources().getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage(),
                        mOnDialogErrorPositiveButtonClickListener);
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(getResources().getString(R.string.dialog_title_error), failMessage, mOnDialogErrorPositiveButtonClickListener);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_etc_balance);

        bindCaService();
    }

    @Override
    protected void onCaServiceConnected() {
        init();
        super.onCaServiceConnected();
    }

    @Override
    protected void onDestroy() {
        if (mGetUserInfoAsyncTask != null) {
            mGetUserInfoAsyncTask.cancel(true);
        }

        if (mGetSelectedCarKeyDataAsyncTask != null) {
            mGetSelectedCarKeyDataAsyncTask.cancel(true);
        }

        unbindCaService();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onReloadSelectedCarKeyData(CarKeyData selectedCarKeyData) {
        mSelectedCarKeyData = selectedCarKeyData;
    }

    private void init() {
        showProgressDialog();
        if (mGetUserInfoAsyncTask == null) {
            mGetUserInfoAsyncTask = new GetUserInfoAsyncTask();
            mGetUserInfoAsyncTask.execute();
        }
    }

    private void getSelectedCarKeyData() {
        if (mGetSelectedCarKeyDataAsyncTask == null) {
            mGetSelectedCarKeyDataAsyncTask = new GetSelectedCarKeyDataAsyncTask();
            mGetSelectedCarKeyDataAsyncTask.execute();
        }
    }

    private void getEtcBalance() {
        boolean isOwner = false;
        if (mSelectedCarKeyData != null && mSelectedCarKeyData.isOwner()) {
            isOwner = true;
        }

        dismissProgressDialog();
        if (isOwner) {
            mPlateNumberWithDash = Prefs.getPlateNumberComparison(this, mSelectedCarKeyData.getCarNo());
            if (TextUtils.isEmpty(mPlateNumberWithDash)) {
                mWhatWantToDoAfterGettingEtcSuccess = WANT_TO_SAVE_PLATE_NUMBER_WITH_DASH;
                showPlateNumberInputDialog();
            } else {
                mWhatWantToDoAfterGettingEtcSuccess = WANT_TO_DO_NOTHING;

                String message = getString(R.string.etc_balance_dialog_etc_owner_plate_number_confirm_message);
                message = String.format(message, mPlateNumberWithDash);

                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.etc_balance_dialog_etc_owner_plate_number_confirm_title);
                mAlertDialog1.setMessage(message);
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getResources().getString(android.R.string.yes),
                        mOnDialogEtcOwnerPlateNumberConfirmPositiveButtonClickListener);
                mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                        getResources().getString(android.R.string.no),
                        new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                mAlertDialog1.dismiss();
                                showIdAndPlateNumberInputDialog();
                            }
                        });
                mAlertDialog1.show();
            }

        } else {
            mWhatWantToDoAfterGettingEtcSuccess = WANT_TO_DO_NOTHING;
            showIdAndPlateNumberInputDialog();
        }
    }

    private void callAPIToGetEtcBalance(String plateNumber, String idCardNumber, String iKeyUID, String iKeyToken, String carID) {
        AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(this);
        GetEtcRequestData getEtcRequestData = new GetEtcRequestData(iKeyUID, iKeyToken, carID);
        if (plateNumber != null && !TextUtils.isEmpty(plateNumber)) {
            getEtcRequestData.setCarNo(plateNumber);
        }
        if (idCardNumber != null && !TextUtils.isEmpty(idCardNumber)) {
            getEtcRequestData.setUserId(idCardNumber);
        }
        assistantWebApi.getEtc(getEtcRequestData, mGetEtcApiListener);
    }

    private void showEtcAVABalanceDialog(int avaBalance) {
        mAlertDialog1.restoreDefault();
        mAlertDialog1.setTitle(R.string.etc_balance_dialog_etc_ava_balance_title);
        String message = getResources().getString(R.string.etc_balance_dialog_etc_ava_balance_message);
        message = String.format(message, avaBalance);
        mAlertDialog1.setMessage(message);
        mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                getResources().getString(R.string.btn_confirm),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mAlertDialog1.dismiss();
                        finish();
                        overridePendingTransition(0, 0);
                    }
                });
        mAlertDialog1.show();
    }

    private void showPlateNumberInputDialog() {
        View customContentView = getLayoutInflater().inflate(R.layout.dialog_custom_etc_owner_plate_number, null);
        mDialogEtcOwnerPlateNumberPreSectionEditText = customContentView.findViewById(R.id.dialog_custom_etc_owner_plate_number_pre_section_edit_text);
        mDialogEtcOwnerPlateNumberRearSectionEditText = customContentView.findViewById(R.id.dialog_custom_etc_owner_plate_number_rear_section_edit_text);
        mDialogEtcOwnerPlateNumberPreSectionEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    mAlertDialog1.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                } else {
                    mAlertDialog1.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                }
            }
        });
        mDialogEtcOwnerPlateNumberPreSectionEditText.requestFocus();

        mAlertDialog1.restoreDefault();
        mAlertDialog1.setTitle(R.string.etc_balance_dialog_etc_owner_plate_number_title);
        mAlertDialog1.setCustomContentView(customContentView);
        mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                getResources().getString(R.string.btn_confirm),
                mOnDialogEtcOwnerPlateNumberPositiveButtonClickListener);
        mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                getResources().getString(R.string.btn_cancel),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mAlertDialog1.dismiss();
                        finish();
                        overridePendingTransition(0, 0);
                    }
                });
        mAlertDialog1.show();
    }

    private void showIdAndPlateNumberInputDialog() {
        View customContentView = getLayoutInflater().inflate(R.layout.dialog_custom_etc_authentication, null);
        mDialogEtcAuthenticationIDCardNumberEditText = customContentView.findViewById(R.id.dialog_custom_etc_authentication_id_card_number_edit_text);
        mDialogEtcAuthenticationPlateNumberPreSectionEditText = customContentView.findViewById(R.id.dialog_custom_etc_authentication_plate_number_pre_section_edit_text);
        mDialogEtcAuthenticationPlateNumberRearSectionEditText = customContentView.findViewById(R.id.dialog_custom_etc_authentication_plate_number_rear_section_edit_text);
        mDialogEtcAuthenticationIDCardNumberEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    mAlertDialog1.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                } else {
                    mAlertDialog1.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                }
            }
        });
        mDialogEtcAuthenticationIDCardNumberEditText.requestFocus();

        mAlertDialog1.restoreDefault();
        mAlertDialog1.setTitle(R.string.etc_balance_dialog_etc_authentication_title);
        mAlertDialog1.setCustomContentView(customContentView);
        mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                getResources().getString(R.string.btn_confirm),
                mOnDialogEtcAuthenticationPositiveButtonClickListener);
        mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                getResources().getString(R.string.btn_cancel),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mAlertDialog1.dismiss();
                        finish();
                        overridePendingTransition(0, 0);
                    }
                });
        mAlertDialog1.show();
    }
}
