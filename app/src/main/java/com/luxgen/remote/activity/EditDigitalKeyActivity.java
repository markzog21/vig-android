package com.luxgen.remote.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.luxgen.remote.R;
import com.luxgen.remote.adapter.EditDigitalKeyAdapter;
import com.luxgen.remote.ca.CommunicationAgentService;
import com.luxgen.remote.custom.CommonDefs;
import com.luxgen.remote.custom.CustomAppCompatActivity;
import com.luxgen.remote.custom.DataKeys;
import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.model.KeyInfo;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.option.CarOptions;
import com.luxgen.remote.network.CarWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.UserWebApi;
import com.luxgen.remote.network.model.VigRequestData;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.car.GetKeyListResponseData;
import com.luxgen.remote.network.model.car.KeyData;
import com.luxgen.remote.network.model.user.QueryUserRequestData;
import com.luxgen.remote.network.model.user.QueryUserResponseData;
import com.luxgen.remote.network.model.user.UserDeviceData;
import com.luxgen.remote.ui.viewmodel.DigitalKeyViewModel;
import com.luxgen.remote.util.DialUtils;
import com.luxgen.remote.util.Prefs;
import com.luxgen.remote.util.TimeUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;

import tw.com.haitec.ae.project.smartphoneikey.snappydb.IKeyDataBean;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.MasterKeyDataBean;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.SnapKeyDataBean;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.SnapTicket;
import tw.com.haitec.ae.project.smartphoneikey.snappydb.SubKeyDataBean;

public class EditDigitalKeyActivity extends CustomAppCompatActivity {

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, EditDigitalKeyActivity.class);
        return intent;
    }

    private EditDigitalKeyAdapter mEditDigitalKeyAdapter = null;
    private KeyInfo mTargetKeyInfo = null;
    private UserDeviceData mTargetUserData = null;
    private int mTargetCarKeyInfoPositionInUI;

    private GetUserInfoAsyncTask mGetUserInfoAsyncTask = null;
    private GetSelectedCarKeyDataAsyncTask mGetSelectedCarKeyDataAsyncTask = null;

    private UserInfo mUserInfo = null;
    private CarKeyData mSelectedCarKeyData = null;
    private DigitalKeyViewModel mViewModel = null;

    private boolean mIsInitDone = false;
    private boolean mIsInitInProgress = false;

    private class GetUserInfoAsyncTask extends AsyncTask<Void, Void, UserInfo> {

        @Override
        protected UserInfo doInBackground(Void... voids) {
            return Prefs.getUserInfo(EditDigitalKeyActivity.this);
        }

        @Override
        protected void onPostExecute(UserInfo userInfo) {
            mGetUserInfoAsyncTask = null;

            mUserInfo = userInfo;
            getSelectedCarKeyData();
        }
    }

    private class GetSelectedCarKeyDataAsyncTask extends AsyncTask<Void, Void, CarKeyData> {

        @Override
        protected CarKeyData doInBackground(Void... voids) {
            return Prefs.getCarKeyData(EditDigitalKeyActivity.this);
        }

        @Override
        protected void onPostExecute(CarKeyData carKeyData) {
            mGetSelectedCarKeyDataAsyncTask = null;
            dismissProgressDialog();

            mSelectedCarKeyData = carKeyData;
            callAPIToGetKeyList();
        }
    }

    private EditDigitalKeyAdapter.OnInnerViewsClickListener mOnRecyclerViewInnerViewsClickListener = new EditDigitalKeyAdapter.OnInnerViewsClickListener() {

        @Override
        public void onSubKeyPrepareDeleteImageViewClick(EditDigitalKeyAdapter adapter, int position) {
            mTargetKeyInfo = (KeyInfo) adapter.getItem(position).getDataObject();
            mAlertDialog1.restoreDefault();
            mAlertDialog1.setTitle(R.string.edit_digital_key_dialog_delete_sub_key_title);
            mAlertDialog1.setMessage(getResources().getString(R.string.edit_digital_key_dialog_delete_sub_key_message));
            mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                    getResources().getString(R.string.btn_confirm),
                    mOnDeleteSubKeyPrepareConfirmDialogPositiveButtonClickListener);
            mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                    getResources().getString(R.string.btn_cancel),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAlertDialog1.dismiss();
                        }
                    });
            mAlertDialog1.show();
        }

        @Override
        public void onSubKeyDeleteImageViewClick(EditDigitalKeyAdapter adapter, int position) {
            mTargetKeyInfo = (KeyInfo) adapter.getItem(position).getDataObject();
            if (mTargetKeyInfo.getKeyStatus() == CarOptions.KEY_STATUS_S2) {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.edit_digital_key_dialog_delete_sub_key_title);
                mAlertDialog1.setMessage(getResources().getString(R.string.edit_digital_key_dialog_delete_sub_key_message));
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getResources().getString(R.string.btn_confirm),
                        mOnDeleteSubKeyConfirmDialogPositiveButtonClickListener);
                mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                        getResources().getString(R.string.btn_cancel),
                        mOnDeleteSubKeyConfirmDialogNegativeButtonClickListener);
                mAlertDialog1.show();
            } else {
                mAlertDialog1.restoreDefault();
                mAlertDialog1.setTitle(R.string.edit_digital_key_dialog_delete_sub_key_s1_title);
                mAlertDialog1.setMessage(getResources().getString(R.string.edit_digital_key_dialog_delete_sub_key_s1_message));
                mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                        getResources().getString(R.string.btn_confirm),
                        new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                mAlertDialog1.dismiss();
                                finish();
                            }
                        });
                mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                        getResources().getString(R.string.btn_call_service),
                        new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                mAlertDialog1.dismiss();
                                startActivity(DialUtils.callService(getApplicationContext()));
                            }
                        });
                mAlertDialog1.show();
            }
        }

        @Override
        public void onTempKeyDeleteImageViewClick(EditDigitalKeyAdapter adapter, int position) {
            mTargetKeyInfo = (KeyInfo) adapter.getItem(position).getDataObject();
            mAlertDialog1.restoreDefault();
            mAlertDialog1.setTitle(R.string.edit_digital_key_dialog_delete_temp_key_title);
            mAlertDialog1.setMessage(getResources().getString(R.string.edit_digital_key_dialog_delete_temp_key_message));
            mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                    getResources().getString(R.string.btn_confirm),
                    mOnDeleteTempKeyConfirmDialogPositiveButtonClickListener);
            mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                    getResources().getString(R.string.btn_cancel),
                    mOnDeleteTempKeyConfirmDialogNegativeButtonClickListener);
            mAlertDialog1.show();
        }

        @Override
        public void onCheckImageViewClick(KeyInfo changedKeyInfo) {
            mViewModel.updateKeyInfoToDb(changedKeyInfo);
        }
    };

    private View.OnClickListener mOnDeleteSubKeyPrepareConfirmDialogPositiveButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            mCommunicationAgentService.delSubKeyPrepare(mTargetKeyInfo.getKeyStatus() == CarOptions.KEY_STATUS_AP1,
                    mTargetKeyInfo.getKeyType(),
                    mTargetKeyInfo.getUserId(),
                    mTargetKeyInfo.getDeviceId(),
                    mTargetKeyInfo.getDeviceCreateTime());
        }
    };

    private View.OnClickListener mOnDeleteSubKeyConfirmDialogPositiveButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            callAPIToGetSubKeyUserData();
        }
    };

    private View.OnClickListener mOnDeleteSubKeyConfirmDialogNegativeButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            mAlertDialog1.dismiss();
        }
    };

    private View.OnClickListener mOnDeleteTempKeyConfirmDialogPositiveButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            callAPIToGetTempKeyUserData();
        }
    };

    private View.OnClickListener mOnDeleteTempKeyConfirmDialogNegativeButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            mAlertDialog1.dismiss();
        }
    };

    private OnApiListener<GetKeyListResponseData> mGetKeyListApiListener = new OnApiListener<GetKeyListResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetKeyListResponseData responseData) {
            if (responseData.isSuccessful()) {
                syncKeysFromKeyList(responseData.getCarList());
                mIsInitDone = true;
                mIsInitInProgress = false;
            } else {
                mIsInitDone = true;
                mIsInitInProgress = false;
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }

            dismissProgressDialog();
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            buildKeysFromKeyList();
            mIsInitDone = true;
            mIsInitInProgress = false;
            dismissProgressDialog();
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<QueryUserResponseData> mGetSubKeyUserDataApiListener = new OnApiListener<QueryUserResponseData>() {

        @Override
        public void onApiTaskSuccessful(QueryUserResponseData responseData) {
            if (responseData.isSuccessful() && responseData.getSize() != 0) {
                for (UserDeviceData data : responseData.getList()) {
                    if (data.getKeyUid().equals(mTargetKeyInfo.getUserId())) {
                        mTargetUserData = data;
                    }
                }
                callCAServiceAPIToDeleteSubKey();
            } else {
                dismissProgressDialog();
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(getString(R.string.dialog_title_error), failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<QueryUserResponseData> mGetTempKeyUserDataApiListener = new OnApiListener<QueryUserResponseData>() {

        @Override
        public void onApiTaskSuccessful(QueryUserResponseData responseData) {
            if (responseData.isSuccessful() && responseData.getSize() != 0) {
                for (UserDeviceData data : responseData.getList()) {
                    if (data.getKeyUid().equals(mTargetKeyInfo.getUserId())) {
                        mTargetUserData = data;
                    }
                }
                callCAServiceAPIToTerminateSnapKey();
            } else {
                dismissProgressDialog();
                showServerErrorDialog(getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(getString(R.string.dialog_title_error), failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_digital_key);

        LinearLayout mainContainer = findViewById(R.id.activity_container);
        setWheelScreenLayout(mainContainer);

        ImageView wheelRightHandle = findViewById(R.id.wheel_right_handle_image_view);
        ImageView wheelLeftHandle = findViewById(R.id.wheel_left_handle_image_view);
        setWheelHandles(wheelRightHandle, wheelLeftHandle);

        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            toolBar.setTitle(R.string.activity_edit_digital_key_title);
            setSupportActionBar(toolBar);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mViewModel = ViewModelProviders.of(this).get(DigitalKeyViewModel.class);

        RecyclerView recyclerView = findViewById(R.id.activity_edit_digital_key_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        if (mEditDigitalKeyAdapter == null) {
            mEditDigitalKeyAdapter = new EditDigitalKeyAdapter(this);
            mEditDigitalKeyAdapter.setOnInnerViewsClickListener(mOnRecyclerViewInnerViewsClickListener);
        }

        recyclerView.setAdapter(mEditDigitalKeyAdapter);

        bindCaService();
    }

    @Override
    protected void onCaServiceConnected() {
        if (!mIsInitInProgress) {
            mIsInitInProgress = true;
            init();
        }

        super.onCaServiceConnected();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        if (mGetUserInfoAsyncTask != null) {
            mGetUserInfoAsyncTask.cancel(true);
        }

        if (mGetSelectedCarKeyDataAsyncTask != null) {
            mGetSelectedCarKeyDataAsyncTask.cancel(true);
        }

        unbindCaService();
        super.onDestroy();
    }

    @Override
    protected void onReloadSelectedCarKeyData(CarKeyData selectedCarKeyData) {
        mSelectedCarKeyData = selectedCarKeyData;
    }

    @Override
    protected void onReceiveBroadcastFromCaService(Context context, Intent intent) {
        Prefs.setRefreshDigitalKeyListFlag(EditDigitalKeyActivity.this, true);

        if (CommunicationAgentService.CA_STATUS_DEL_SUBKEY.equals(intent.getAction())) {
            dismissProgressDialog();

            int extraReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            if (extraReturnCode == CommunicationAgentService.CA_EXTRA_DEL_SUBKEY_SUCCESS_START) {
                showConfirmDialog(getString(R.string.edit_digital_key_dialog_sub_key_deleted_title),
                        getString(R.string.edit_digital_key_dialog_sub_key_deleted_message),
                        new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        });
            } else if (extraReturnCode == CommunicationAgentService.CA_EXTRA_DEL_SUBKEY_PREPARED_SUCCESS) {
                showConfirmDialog(getString(R.string.edit_digital_key_dialog_sub_key_prepared_deleted_title),
                        getString(R.string.edit_digital_key_dialog_sub_key_prepared_deleted_message),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        });
            } else {
                showConfirmDialog(getString(R.string.edit_digital_key_dialog_delete_sub_key_fail_title),
                        getString(R.string.edit_digital_key_dialog_delete_sub_key_fail_message));
            }

            return;
        } else if (CommunicationAgentService.CA_STATUS_TERMINATE_SNAPKEY.equals(intent.getAction())) {
            dismissProgressDialog();

            int extraReturnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
            if (extraReturnCode == CommunicationAgentService.CA_EXTRA_TERMINATE_SNAPKEY_SUCCESS) {
                showConfirmDialog(getString(R.string.edit_digital_key_dialog_temp_key_deleted_title),
                        getString(R.string.edit_digital_key_dialog_temp_key_deleted_message),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        });
            } else {
                showConfirmDialog(getString(R.string.edit_digital_key_dialog_delete_temp_key_fail_title),
                        getString(R.string.edit_digital_key_dialog_delete_temp_key_fail_message));
            }

            return;
        } else if (CommunicationAgentService.CA_STATUS_VIG_WAKEUP_FAILED.equals(intent.getAction())) {
            dismissProgressDialog();
            showVigConnectionFailWithBluetooth();
            return;
        } else if (CommunicationAgentService.CA_STATUS_IKEY_FUNCTOSERVER_TIMEOUT.equals(intent.getAction())) {
            dismissProgressDialog();
            LogCat.d("CA_STATUS_IKEY_FUNCTOSERVER_TIMEOUT");
            return;
        }

        super.onReceiveBroadcastFromCaService(context, intent);
    }

    private void init() {
        showProgressDialog();
        if (mGetUserInfoAsyncTask == null) {
            mGetUserInfoAsyncTask = new GetUserInfoAsyncTask();
            mGetUserInfoAsyncTask.execute();
        }
    }

    private void getSelectedCarKeyData() {
        showProgressDialog();
        if (mGetSelectedCarKeyDataAsyncTask == null) {
            mGetSelectedCarKeyDataAsyncTask = new GetSelectedCarKeyDataAsyncTask();
            mGetSelectedCarKeyDataAsyncTask.execute();
        }
    }

    private void callAPIToGetKeyList() {
        showProgressDialog();
        if (mUserInfo != null && mSelectedCarKeyData != null) {
            VigRequestData data = new VigRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken(), mSelectedCarKeyData.getCarId());

            CarWebApi carWebApi = CarWebApi.getInstance(this);
            carWebApi.getKeyList(data, mGetKeyListApiListener);
        } else {
            dismissProgressDialog();
        }
    }

    private void callAPIToGetSubKeyUserData() {
        showProgressDialog();
        if (mUserInfo != null && mTargetKeyInfo.getPhone() != null && !TextUtils.isEmpty(mTargetKeyInfo.getPhone())) {
            QueryUserRequestData data = new QueryUserRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken(), mTargetKeyInfo.getPhone());

            UserWebApi userWebApi = UserWebApi.getInstance(this);
            userWebApi.getUserData(data, mGetSubKeyUserDataApiListener);
        } else {
            dismissProgressDialog();
        }
    }

    private void callAPIToGetTempKeyUserData() {
        showProgressDialog();
        if (mUserInfo != null && mTargetKeyInfo.getPhone() != null && !TextUtils.isEmpty(mTargetKeyInfo.getPhone())) {
            QueryUserRequestData data = new QueryUserRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken(), mTargetKeyInfo.getPhone());

            UserWebApi userWebApi = UserWebApi.getInstance(this);
            userWebApi.getUserData(data, mGetTempKeyUserDataApiListener);
        } else {
            dismissProgressDialog();
        }
    }

    private void callCAServiceAPIToDeleteSubKey() {
        if (mCommunicationAgentService != null && mTargetUserData != null) {
            showProgressDialog();
            mCommunicationAgentService.delSubKey(mTargetKeyInfo.getKeyType(), mTargetUserData.getKeyUid(), mTargetUserData.getDeviceId(), mTargetUserData.getDeviceCreateTime());
        }
    }

    private void callCAServiceAPIToTerminateSnapKey() {
        if (mCommunicationAgentService != null && mTargetUserData != null) {
            showProgressDialog();
            mCommunicationAgentService.terminateSnapTicket(mTargetKeyInfo.getPosition(), mTargetUserData.getKeyUid(), mTargetUserData.getDeviceId(), mTargetUserData.getDeviceCreateTime());
        } else {
            dismissProgressDialog();
        }
    }

    private void syncKeysFromKeyList(ArrayList<KeyData> keyList) {
        if (keyList != null) {
            KeyData[] subKeyDataArray = new KeyData[CommonDefs.DIGITAL_KEY_SUBKEY_OWNER_MAX_COUNT];
            KeyData[] tempKeyDataArray = new KeyData[CommonDefs.DIGITAL_KEY_TEMPKEY_OWNER_MAX_COUNT];
            ArrayList<KeyData> newAddKeyList = new ArrayList<>();
            ArrayList<KeyData> subKeyList = new ArrayList<>();
            ArrayList<KeyData> tempKeyList = new ArrayList<>();
            for (int index = 0; index < keyList.size(); index++) {
                KeyData keyData = keyList.get(index);
                if (keyData.getKeyType().equals(CarOptions.KEY_S1) || keyData.getKeyType().equals(CarOptions.KEY_S2)) {
                    subKeyList.add(keyData);
                } else if (keyData.getKeyType().equals(CarOptions.KEY_T1) || keyData.getKeyType().equals(CarOptions.KEY_T2)) {
                    tempKeyList.add(keyData);
                }
            }

            /**
             * MaterKey position is 0.
             * SubKeys are starting from 1 and ended at 4.
             * SnapKeys are starting from 5 and ended at 6.
             */
            ArrayList<KeyInfo> keyInfoList = mViewModel.getKeyInfoListFromDb(mSelectedCarKeyData.getCarId());

            // Deal with subkey
            for (int index = 0; index < subKeyList.size(); index++) {
                boolean isMatched = false;
                KeyData keyData = subKeyList.get(index);
                for (int position = 1; position < 5; position++) {
                    KeyInfo keyInfo = keyInfoList.get(position);
                    if (keyData.getKeyUid().equals(keyInfo.getUserId()) &&
                            keyData.getPhoneId().equals(keyInfo.getDeviceId())) {
                        if(TextUtils.isEmpty(keyData.getDeleteKeyStatus())) {
                            if(keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_D1) {
                                subKeyDataArray[position - 1] = keyData;
                                isMatched = true;
                                break;
                            }
                        } else {
                            if(keyInfo.getKeyStatus() == CarOptions.KEY_STATUS_D1) {
                                subKeyDataArray[position - 1] = keyData;
                                isMatched = true;
                                break;
                            }
                        }
                    }
                }

                if (!isMatched) {
                    newAddKeyList.add(keyData);
                }
            }

            // Sync key list to database
            for (int index = 0; index < subKeyDataArray.length; index++) {
                KeyData keyData = subKeyDataArray[index];
                KeyInfo keyInfo = keyInfoList.get(index + 1);
                if (keyData == null) {
                    if (keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_DEF &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_AP1 &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_AP2 &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_AP2S1 &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_DP1 &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_DP2 &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_DP2D1) {
                        keyInfo.clear();
                        mViewModel.updateKeyInfoToDb(keyInfo);
                    }
                } else {
                    if (keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_DP1 &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_DP2 &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_DP2D1) {
                        syncAndUpdateKeyDataToDB(keyData, keyInfo);
                    }
                }
            }

            // 補到空位
            for (int index = 0; index < newAddKeyList.size(); index++) {
                KeyData keyData = newAddKeyList.get(index);
                for (int position = 1; position < 5; position++) {
                    KeyInfo keyInfo = keyInfoList.get(position);
                    if (keyInfo.getKeyStatus() == CarOptions.KEY_STATUS_DEF) {
                        syncAndUpdateKeyDataToDB(keyData, keyInfo);
                        break;
                    }
                }
            }

            // Deal with temp key
            boolean hasSnapTicket = false;

            IKeyDataBean iKeyDataBean = getIKeyDataBean(mSelectedCarKeyData.getVigCarId());
            if (iKeyDataBean != null && iKeyDataBean.getSnapTicketArray() != null) {
                ArrayList<SnapTicket> snapTicketList = mCommunicationAgentService.getCA().getSnapTicketByIKeyDataBean(iKeyDataBean);
                if (snapTicketList.size() >= CommonDefs.DIGITAL_KEY_TEMPKEY_OWNER_MAX_COUNT) {
                    hasSnapTicket = true;
                }
            }

            newAddKeyList.clear();

            for (int index = 0; index < tempKeyList.size(); index++) {
                boolean isMatched = false;
                KeyData keyData = tempKeyList.get(index);
                for (int position = 5; position < 7; position++) {
                    KeyInfo keyInfo = keyInfoList.get(position);
                    if (keyData.getKeyUid().equals(keyInfo.getUserId()) &&
                            keyData.getPhoneId().equals(keyInfo.getDeviceId())) {
                        tempKeyDataArray[position - 5] = keyData;
                        isMatched = true;
                        break;
                    }
                }

                if (!isMatched) {
                    newAddKeyList.add(keyData);
                }
            }

            // Sync key list to database
            for (int index = 0; index < tempKeyDataArray.length; index++) {
                KeyData keyData = tempKeyDataArray[index];
                KeyInfo keyInfo = keyInfoList.get(index + 5);
                if (keyData == null) {
                    if (keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_SNAP_TERMINATE &&
                            keyInfo.getKeyStatus() != CarOptions.KEY_STATUS_D1) {
                        keyInfo.clear();
                        if (hasSnapTicket) {
                            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_SNAP_NORMAL);
                        } else {
                            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_SNAP_INIT);
                        }
                        mViewModel.updateKeyInfoToDb(keyInfo);
                    }
                } else {
                    syncAndUpdateKeyDataToDB(keyData, keyInfo);
                }
            }

            // 補到空位
            for (int index = 0; index < newAddKeyList.size(); index++) {
                KeyData keyData = newAddKeyList.get(index);
                for (int position = 5; position < 7; position++) {
                    KeyInfo keyInfo = keyInfoList.get(position);
                    if (keyInfo.getKeyStatus() == CarOptions.KEY_STATUS_SNAP_INIT ||
                            keyInfo.getKeyStatus() == CarOptions.KEY_STATUS_SNAP_NORMAL) {
                        syncAndUpdateKeyDataToDB(keyData, keyInfo);
                        break;
                    }
                }
            }

            buildKeysFromKeyList();
        }
    }

    private void buildKeysFromKeyList() {
        ArrayList<KeyInfo> keyInfoList = mViewModel.getKeyInfoListFromDb(mSelectedCarKeyData.getCarId());

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(DataKeys.KEY_LABEL, getString(R.string.edit_digital_key_sub_key_owner_label));
            mEditDigitalKeyAdapter.buildItemData(jsonObject, null, EditDigitalKeyAdapter.EDITDIGITALKEY_ITEM_TYPE_LABEL, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int position = 1; position < 5; position++) {
            KeyInfo keyInfo = keyInfoList.get(position);
            switch (keyInfo.getKeyStatus()) {

                case CarOptions.KEY_STATUS_AP2:
                case CarOptions.KEY_STATUS_DP2: {
                    if (position < 4) {
                        mEditDigitalKeyAdapter.buildItemData(keyInfo, null,
                                EditDigitalKeyAdapter.EDITDIGITALKEY_ITEM_TYPE_SUBKEY_PREPARE, true);
                        mEditDigitalKeyAdapter.buildItemData(null, null,
                                EditDigitalKeyAdapter.EDITDIGITALKEY_ITEM_TYPE_DIVIDER, false);
                    } else {
                        mEditDigitalKeyAdapter.buildItemData(keyInfo, null,
                                EditDigitalKeyAdapter.EDITDIGITALKEY_ITEM_TYPE_SUBKEY_PREPARE, true);
                    }
                    break;
                }

                case CarOptions.KEY_STATUS_S1:
                case CarOptions.KEY_STATUS_S2: {
                    if (position < 4) {
                        mEditDigitalKeyAdapter.buildItemData(keyInfo, null,
                                EditDigitalKeyAdapter.EDITDIGITALKEY_ITEM_TYPE_SUBKEY, true);
                        mEditDigitalKeyAdapter.buildItemData(null, null,
                                EditDigitalKeyAdapter.EDITDIGITALKEY_ITEM_TYPE_DIVIDER, false);
                    } else {
                        mEditDigitalKeyAdapter.buildItemData(keyInfo, null,
                                EditDigitalKeyAdapter.EDITDIGITALKEY_ITEM_TYPE_SUBKEY, true);
                    }
                    break;
                }

                default: {

                }
            }
        }

        /*
        jsonObject = new JSONObject();
        try {
            jsonObject.put(DataKeys.KEY_LABEL, getString(R.string.edit_digital_key_temp_key_owner_label));
            mEditDigitalKeyAdapter.buildItemData(jsonObject, null, EditDigitalKeyAdapter.EDITDIGITALKEY_ITEM_TYPE_LABEL, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int position = 5; position < 7; position++) {
            KeyInfo carKeyInfo = keyInfoList.get(position);
            switch (carKeyInfo.getKeyStatus()) {
                case CarOptions.KEY_STATUS_T1:
                case CarOptions.KEY_STATUS_T2: {
                    if (position < 6) {
                        mEditDigitalKeyAdapter.buildItemData(carKeyInfo, null,
                                EditDigitalKeyAdapter.EDITDIGITALKEY_ITEM_TYPE_TEMPKEY, true);
                        mEditDigitalKeyAdapter.buildItemData(null, null,
                                EditDigitalKeyAdapter.EDITDIGITALKEY_ITEM_TYPE_DIVIDER, false);
                    } else {
                        mEditDigitalKeyAdapter.buildItemData(carKeyInfo, null,
                                EditDigitalKeyAdapter.EDITDIGITALKEY_ITEM_TYPE_TEMPKEY, true);
                    }
                    break;
                }

                default: {

                }
            }
        }
*/
        mEditDigitalKeyAdapter.commitBuildItemData();
        mEditDigitalKeyAdapter.notifyDataSetChanged();
    }

    private IKeyDataBean getIKeyDataBean(String vigCarId) {
        ArrayList<MasterKeyDataBean> masterKeyDataList = mCommunicationAgentService.getCA().getMasterKeyDataList();
        if (masterKeyDataList.size() > 0) {
            for (MasterKeyDataBean bean : masterKeyDataList) {
                if (bean.getCarId().equalsIgnoreCase(vigCarId) &&
                        bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                    return bean;
                }
            }
        } else {
            ArrayList<SubKeyDataBean> subKeyDataBeanList = mCommunicationAgentService.getCA().getSubKeyDataList();
            if (subKeyDataBeanList.size() > 0) {
                for (SubKeyDataBean bean : subKeyDataBeanList) {
                    if (bean.getCarId().equalsIgnoreCase(vigCarId) &&
                            bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                        return bean;
                    }
                }
            } else {
                ArrayList<SnapKeyDataBean> snapKeyDataBeanList = mCommunicationAgentService.getCA().getSnapKeyDataListByCarId(vigCarId);
                if (snapKeyDataBeanList.size() > 0) {
                    for (SnapKeyDataBean bean : snapKeyDataBeanList) {
                        if (bean.getStatus().equals(IKeyDataBean.STATUS_NORMAL)) {
                            return bean;
                        }
                    }
                }
            }
        }
        return null;
    }

    private void syncAndUpdateKeyDataToDB(KeyData keyData, KeyInfo keyInfo) {
        keyInfo.setUserId(keyData.getKeyUid());
        keyInfo.setPhone(keyData.getPhoneNo());
        keyInfo.setName(keyData.getName());
        keyInfo.setEmail(keyData.getEmail());
        keyInfo.setKeyType(keyData.getKeyType());
        keyInfo.setDeviceId(keyData.getPhoneId());
        if (keyData.getDeleteKeyStatus() != null) {
            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_D1);
        } else if (keyData.getKeyType().equals(CarOptions.KEY_S1)) {
            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_S1);
        } else if (keyData.getKeyType().equals(CarOptions.KEY_S2)) {
            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_S2);
        } else if (keyData.getKeyType().equals(CarOptions.KEY_T1)) {
            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_T1);
        } else if (keyData.getKeyType().equals(CarOptions.KEY_T2)) {
            keyInfo.setKeyStatus(CarOptions.KEY_STATUS_T2);
        }
        try {
            if (keyData.getKeyStartDate() != null && keyData.getKeyEndDate() != null) {
                keyInfo.setSnapKeyStartTime(TimeUtils.parseToLong(keyData.getKeyStartDate()));
                keyInfo.setSnapKeyEndTime(TimeUtils.parseToLong(keyData.getKeyEndDate()));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        mViewModel.updateKeyInfoToDb(keyInfo);
    }

    private void showVigConnectionFailWithBluetooth() {
        if (mAlertDialog1.isShowing()) {
            mAlertDialog1.dismiss();
        }
        mAlertDialog1.restoreDefault();
        mAlertDialog1.setTitle(getString(R.string.dialog_title_vig_connect_fail));
        mAlertDialog1.setMessage(getString(R.string.dialog_message_vig_connect_fail_with_bluetooth));
        mAlertDialog1.setButton(DialogInterface.BUTTON_POSITIVE,
                getResources().getString(R.string.btn_confirm),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mAlertDialog1.dismiss();
                    }
                });
        mAlertDialog1.setButton(DialogInterface.BUTTON_NEGATIVE,
                getResources().getString(R.string.btn_near_end_connection),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent intent = NearEndConnectionTeachingActivity.newIntentToDeleteSubKey(EditDigitalKeyActivity.this,
                                mTargetKeyInfo.getKeyType(), mTargetUserData.getKeyUid(), mTargetUserData.getDeviceId(), mTargetUserData.getDeviceCreateTime());
                        startActivity(intent);
                        mAlertDialog1.dismiss();
                        finish();
                    }
                });
        mAlertDialog1.show();
    }

}
