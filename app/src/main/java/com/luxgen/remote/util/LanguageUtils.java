package com.luxgen.remote.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LanguageUtils {
    private static final String REGEX_CHINESE = "[\\u3400-\\u9fff]"; // unicode編碼，判斷是否為漢字

    public static int getChineseCharacterCount(String str) {
        int count = 0;
        Pattern p = Pattern.compile(REGEX_CHINESE);
        Matcher m = p.matcher(str);
        while (m.find()) {
            for (int i = 0; i <= m.groupCount(); i++) {
                count = count + 1;
            }
        }
        return count;
    }
}
