package com.luxgen.remote.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.luxgen.remote.BuildConfig;
import com.luxgen.remote.R;
import com.luxgen.remote.custom.LogCat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class PlayStoreChecker {

    public final static String CONFIG_VERSION = "version";
    public final static String CONFIG_VERSION_TEST = "version_test";

    public interface CheckResultListener {
        void noNeedToUpdate();
    }

    private Activity mActivity;
    private CheckResultListener mCheckResultListener;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    private void hasNewVersion(boolean has) {
        if(!has) {
            mCheckResultListener.noNeedToUpdate();
            return;
        }

        String msg = "Google Play 上有新的版本，請先更新後再繼續使用。\n" +
                "若發生連結失效，請到 Google Play 搜尋「" + mActivity.getString(R.string.app_name) + "」執行更新。";
        new AlertDialog.Builder(mActivity)
                .setTitle("有新的版本")
                .setMessage(msg)
                .setPositiveButton("前往", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String url = "market://details?id=com.luxgen.vig";
                        mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                        mActivity.finishAffinity();
                    }
                })
                .setNegativeButton("關閉", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        closeApp();
                    }
                })
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        closeApp();
                    }
                })
                .show();
    }

    private void closeApp() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mActivity.finishAndRemoveTask();
        } else {
            mActivity.finishAffinity();
        }
    }

    public PlayStoreChecker(Activity activity, CheckResultListener listener) {
        mActivity = activity;
        mCheckResultListener = listener;
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings frcSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(0)
                .build();
        mFirebaseRemoteConfig.setConfigSettingsAsync(frcSettings);
    }

    public void start() {

        mFirebaseRemoteConfig.fetchAndActivate().addOnCompleteListener(new OnCompleteListener<Boolean>() {
            @Override
            public void onComplete(@NonNull Task<Boolean> task) {
                boolean hasNewVersion = false;
                if(task.isSuccessful()) {
                    long version = BuildConfig.IS_OFFICIAL ?
                            mFirebaseRemoteConfig.getLong(CONFIG_VERSION) :
                            mFirebaseRemoteConfig.getLong(CONFIG_VERSION_TEST);
                    if(BuildConfig.VERSION_CODE < version) {
                        hasNewVersion = true;
                    }
                }
                hasNewVersion(hasNewVersion);
            }
        });
    }
}
