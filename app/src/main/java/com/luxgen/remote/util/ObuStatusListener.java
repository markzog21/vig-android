package com.luxgen.remote.util;

import com.luxgen.remote.model.PushData;

import java.util.ArrayList;

import luxgen.service.StatusListener;

public class ObuStatusListener extends StatusListener {
    private ObuManager mManager;

    public ObuStatusListener(ObuManager manager) {
        mManager = manager;
    }

    public void syncPushList(ArrayList<PushData> list) {
        mManager.syncPushList(list);
    }

    @Override
    public void onRemoteConnected() {
        super.onRemoteConnected();
    }
}
