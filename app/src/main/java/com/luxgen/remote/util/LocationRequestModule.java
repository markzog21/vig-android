package com.luxgen.remote.util;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.luxgen.remote.custom.LogCat;

import static android.app.Activity.RESULT_OK;

public class LocationRequestModule {

    private Activity mActivity;

    private static final String TAG = "LocationRequestModule";

    public static final int REQUEST_LOCATION_PERMISSION = 19527;
    public static final int REQUEST_CHECK_SETTINGS = 19528;

    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private LocationListener mLocationListener;
    private Boolean mStartLocationUpdates = false;

    private OnSuccessListener<Location> mOnLastLocationQuerySuccessListener = new OnSuccessListener<Location>() {
        @Override
        public void onSuccess(Location location) {
            if (location != null && mLocationListener != null) {
                mLocationListener.onLocationChanged(location);
            }
        }
    };

    private OnSuccessListener<LocationSettingsResponse> mOnSettingsQuerySuccessListener = locationSettingsResponse -> {
        if (locationSettingsResponse.getLocationSettingsStates().isLocationUsable()) {
            startLocationUpdates();
        } else {
            LogCat.w("no usable location service");
        }
    };

    private OnFailureListener mOnSettingsQueryFailureListener = new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
            int statusCode = ((ApiException) e).getStatusCode();
            switch (statusCode) {
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    try {
                        ResolvableApiException rae = (ResolvableApiException) e;
                        rae.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sie) {
                        sie.printStackTrace();
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    LogCat.w("settings change unavailable");
            }
        }
    };

    public LocationRequestModule(Activity activity) {
        this.mActivity = activity;
    }

    public void init() {
        initialLocationServices();
        createLocationCallback();
        createLocationRequest();
        doCheckLocationSettings();
    }

    public void startLocationUpdates() {
        if (mStartLocationUpdates) {
            return;
        }

        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mStartLocationUpdates = true;
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
        mFusedLocationClient.getLastLocation().addOnSuccessListener(mOnLastLocationQuerySuccessListener);
    }

    public void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        mStartLocationUpdates = false;
    }

    private synchronized void initialLocationServices() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mActivity);
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location location = locationResult.getLastLocation();
                if (location != null && mLocationListener != null) {
                    mLocationListener.onLocationChanged(location);
                    stopLocationUpdates();
                }
            }
        };
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void doCheckLocationSettings() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest mLocationSettingsRequest = builder.build();

        SettingsClient mSettingsClient = LocationServices.getSettingsClient(mActivity);
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(mActivity, mOnSettingsQuerySuccessListener)
                .addOnFailureListener(mActivity, mOnSettingsQueryFailureListener);
    }

    public void setLocationChangedListener(LocationListener locationListener) {
        this.mLocationListener = locationListener;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_OK) {
                startLocationUpdates();
            } else {
                LogCat.w("user canceled settings request");
            }
        }
    }
}
