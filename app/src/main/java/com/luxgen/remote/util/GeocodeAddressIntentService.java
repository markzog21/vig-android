package com.luxgen.remote.util;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.LogCat;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GeocodeAddressIntentService extends IntentService {

    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final int DETAILED_RESULT = 2;
    public static final String PACKAGE_NAME = "com.luxgen.remote";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";
    public static final String DETAILED_DATA_EXTRA = PACKAGE_NAME + ".DETAILED_DATA_EXTRA";

    private ResultReceiver resultReceiver;
    private static final String TAG = "GeocodeAddress";

    public GeocodeAddressIntentService() {
        super("GeocodeAddressIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;

        resultReceiver = intent.getParcelableExtra(RECEIVER);
        Location location = intent.getParcelableExtra(LOCATION_DATA_EXTRA);
        boolean detailed = intent.getBooleanExtra(DETAILED_DATA_EXTRA, false);

        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        } catch (IOException ioException) {
            LogCat.e("Service Not Available" + "\n" + ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            LogCat.e("Invalid Latitude or Longitude Used. " +
                    "Latitude = " + location.getLatitude() + ", Longitude = " +
                    location.getLongitude() + "\n" + illegalArgumentException);
        }

        if (addresses == null || addresses.size() == 0) {
            deliverResultToReceiver(FAILURE_RESULT, getString(R.string.location_not_found));
        } else {
            Address address = addresses.get(0);

            String adminArea = address.getAdminArea();
            String subAdminArea = address.getSubAdminArea();
            String locality = address.getLocality();
            String thoroughfare = address.getThoroughfare();
            String featureName = address.getFeatureName();
            String postalCode = address.getPostalCode();

            // Return the text
            StringBuilder detailAddress = new StringBuilder();
            StringBuilder detailAddress2 = new StringBuilder();

            if (adminArea != null) {
                detailAddress.append(adminArea);
            }

            if (subAdminArea != null) {
                detailAddress.append(subAdminArea);
            }

            if (locality != null) {
                detailAddress.append(locality);
            }

            if (thoroughfare != null) {
                detailAddress.append(thoroughfare);
                detailAddress2.append(thoroughfare);
            }

            if (featureName != null && thoroughfare != null && !featureName.equals(thoroughfare)) {
                detailAddress.append(featureName);
                detailAddress2.append(featureName);

                if (!featureName.endsWith("號")) {
                    detailAddress.append("號");
                    detailAddress2.append("號");
                }
            }

            if (detailed) {
                Bundle bundle = new Bundle();
                bundle.putString("area", adminArea );
                bundle.putString("region", subAdminArea == null ? locality : subAdminArea);
                bundle.putString("code", postalCode);
                bundle.putString("address", detailAddress2.toString());
                resultReceiver.send(DETAILED_RESULT, bundle);
            } else {
                deliverResultToReceiver(SUCCESS_RESULT, detailAddress.toString());
            }
        }
    }

    private void deliverResultToReceiver(int resultCode, String message) {
        Bundle bundle = new Bundle();
        bundle.putString(RESULT_DATA_KEY, message);
        resultReceiver.send(resultCode, bundle);
    }
}
