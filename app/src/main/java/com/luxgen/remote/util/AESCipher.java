package com.luxgen.remote.util;

/**
 * Created by FrankCheng on 2016/12/27.
 */

import android.util.Base64;

import com.luxgen.remote.custom.LogCat;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

//import android.util.Base64.Decoder;
//import android.util.Base64.Encoder;

public class AESCipher {
	private static AESCipher ourInstance = new AESCipher();

	public static AESCipher getInstance() {
		return ourInstance;
	}

	private AESCipher() {
	}
	private static String Encryption_key_STRING = "";
	private static String IV_STRING = "";

	public static String safeEncryptAES(String content){
		String ret = "";
		try {
			if(content!=null){
				ret = encryptAES(content);
			}
			else{
				ret =  null;
			}
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			ret = ""+ e.getMessage();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			ret = ""+ e.getMessage();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
			ret = ""+ e.getMessage();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			ret = ""+ e.getMessage();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
			ret = ""+ e.getMessage();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
			ret = ""+ e.getMessage();
		} catch (BadPaddingException e) {
			e.printStackTrace();
			ret = ""+ e.getMessage();
		}
		//if(!TextUtils.isEmpty(ret)) Log.e("AESCipher", ""+content+ " safeEncryptAES: "+ret);

		return ret;
	}
	public static String encryptAES(String content)
			throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, UnsupportedEncodingException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

		if(Encryption_key_STRING.length()==0) {
			genKey();
		}

		byte[] byteContent = content.getBytes(StandardCharsets.UTF_8);

		byte[] enCodeFormat = Encryption_key_STRING.getBytes();
		SecretKeySpec secretKeySpec = new SecretKeySpec(enCodeFormat, "AES");

		byte[] initParam = IV_STRING.getBytes();
		IvParameterSpec ivParameterSpec = new IvParameterSpec(initParam);

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);

		byte[] encryptedBytes = cipher.doFinal(byteContent);

		//Encoder encoder = Base64.getEncoder();
		//return encoder.encodeToString(encryptedBytes);
		return Base64.encodeToString(encryptedBytes, Base64.NO_WRAP);
	}

	public static String safeDecryptAES(String content){
		String strError = "";
		String ret = "";
		try {
			if(content!=null){
				ret = decryptAES(content);
			}
			else{
				ret = decryptAES("");
			}
		}catch (Exception e){
			ret = content;
			LogCat.e(String.format("Exception in safeDecryptAES:%s,%s",content,e.getMessage()));
			e.printStackTrace();
		}
//		if(!TextUtils.isEmpty(strError)){
//			Log.e("AESCipher", ""+content+ " safeDecryptAES: "+strError);
//		}
		return ret;
	}
	public static String decryptAES(String content)
			throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidAlgorithmParameterException,
			IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {

		if(Encryption_key_STRING.length()==0) {
			genKey();
		}

		//Decoder decoder = Base64.getDecoder();
		//byte[] encryptedBytes = decoder.decode(content);
		byte[] encryptedBytes = Base64.decode(content, Base64.NO_WRAP);

		byte[] enCodeFormat = Encryption_key_STRING.getBytes();
		SecretKeySpec secretKey = new SecretKeySpec(enCodeFormat, "AES");

		byte[] initParam = IV_STRING.getBytes();
		IvParameterSpec ivParameterSpec = new IvParameterSpec(initParam);

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec);

		byte[] result = cipher.doFinal(encryptedBytes);
		String ret = (result == null) ? content : new String(result, StandardCharsets.UTF_8);
		return ret;
	}

	private static void genKey() throws NoSuchAlgorithmException {

		String key = "LuxgenWonderful0927";
		byte[] keyBuf = key.getBytes();
		MessageDigest md5Obj = MessageDigest.getInstance("MD5");
		md5Obj.update(keyBuf);

		byte[] mdbytes = md5Obj.digest();
		StringBuffer sb = new StringBuffer();

		for(int i = 0; i < mdbytes.length; i++) {
			sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
		}

		String medKey = sb.toString().toUpperCase();

		Encryption_key_STRING = medKey.substring(0, 16);
		IV_STRING = medKey.substring(16);
//		System.out.println("Digest(in hex format)::" + sb.toString().toUpperCase());
//		System.out.println("Encryption key::" + Encryption_key_STRING);
//		System.out.println("IV key::" + IV_STRING);
	}
}
