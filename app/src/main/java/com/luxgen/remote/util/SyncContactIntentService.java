package com.luxgen.remote.util;

import android.Manifest;
import android.app.IntentService;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;

import com.luxgen.remote.network.model.user.EmployeeData;

import java.util.ArrayList;

public class SyncContactIntentService extends IntentService {

    public static final String EXTRA_EMPLOYEE_DATA = "extra_employee_data";
    public static final String RESULT_RECEIVER = "result_receiver";
    public static final String RESULT_ADVISER_NAME = "result_adviser_name";
    public static final String RESULT_CAR_NO = "result_car_no";
    public static final int RESULT_CODE_SYNC_ADVISOR = 9527;
    public static final int RESULT_CODE_SYNC_FINISHED = 9528;

    private ResultReceiver resultReceiver;
    private ArrayList<EmployeeData> mEmployeeData;

    public SyncContactIntentService() {
        super("SyncContactIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            resultReceiver = intent.getParcelableExtra(RESULT_RECEIVER);
            mEmployeeData = intent.getParcelableArrayListExtra(EXTRA_EMPLOYEE_DATA);
        }

        if (mEmployeeData != null) {
            for (EmployeeData employeeData : mEmployeeData) {
                if (!findContactByPhone(employeeData.getPhone())) {
                    if (employeeData.isAdvisor()) {
                        addContact(employeeData.getName(), employeeData.getPhone(), false);
                        notifyNewAdvisor(employeeData.getName(), employeeData.getCarNo());
                    }
                }
            }
        }

        notifySyncFinished();
    }

    private void notifyNewAdvisor(String name, String carNo) {
        if (resultReceiver != null) {
            Bundle bundle = new Bundle();
            bundle.putString(RESULT_ADVISER_NAME, name);
            bundle.putString(RESULT_CAR_NO, carNo);
            resultReceiver.send(RESULT_CODE_SYNC_ADVISOR, bundle);
        }
    }

    private void notifySyncFinished() {
        if (resultReceiver != null) {
            resultReceiver.send(RESULT_CODE_SYNC_FINISHED, null);
        }
    }

    private boolean findContactByPhone(String phone) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

            String[] contactProjection = new String[]{ContactsContract.Data.CONTACT_ID, ContactsContract.CommonDataKinds.Phone.NUMBER};
            String contactSelection = ContactsContract.CommonDataKinds.Phone.NUMBER + "=?";
            String[] contactSelectionArgs = new String[]{phone};

            try (Cursor cursor = getContentResolver().query(ContactsContract.Data.CONTENT_URI, contactProjection, contactSelection, contactSelectionArgs, null)) {
                if (cursor != null && cursor.moveToFirst()) {
                    return true;
                }
            }
        }

        return false;
    }

    private String findContact(boolean bService) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

            String[] contactProjection = new String[]{ContactsContract.Data.CONTACT_ID, ContactsContract.CommonDataKinds.Organization.TITLE, ContactsContract.CommonDataKinds.Organization.COMPANY};
            String contactSelection = ContactsContract.CommonDataKinds.Organization.TITLE + "=?" + " AND " +
                    ContactsContract.CommonDataKinds.Organization.COMPANY + "=?";
            String[] contactSelectionArgs = new String[]{bService ? "服務專員" : "即時保母", "Luxgen"};

            try (Cursor cursor = getContentResolver().query(ContactsContract.Data.CONTENT_URI, contactProjection, contactSelection, contactSelectionArgs, null)) {
                if (cursor != null && cursor.moveToFirst()) {
                    return cursor.getString(cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID));
                }
            }
        }

        return null;
    }

    private String getContactPhoneById(String id) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

            String[] contactProjection = new String[]{ContactsContract.CommonDataKinds.Phone.CONTACT_ID, ContactsContract.CommonDataKinds.Phone.NUMBER};
            String contactSelection = ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?";
            String[] contactSelectionArgs = new String[]{id};
            try (Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, contactProjection, contactSelection, contactSelectionArgs, null)) {
                if (cursor != null && cursor.moveToFirst()) {
                    return cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                }
            }
        }

        return null;
    }

    private void addContact(String name, String mobile, boolean service) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

            if (mobile == null || mobile.length() == 0) {
                return;
            }

            ArrayList<ContentProviderOperation> ops = new ArrayList<>();
            int rawContactInsertIndex = ops.size();

            ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build());
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name + (service ? " - 服務專員" : " - 即時保母"))
                    .build());
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, mobile)
                    .build());
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Organization.TITLE, (service ? "服務專員" : "即時保母"))
                    .withValue(ContactsContract.CommonDataKinds.Organization.COMPANY, "Luxgen")
                    .build());
            try {
                ContentProviderResult[] res = getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            } catch (RemoteException | OperationApplicationException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateContact(String id, String name, String mobile, boolean service) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

            if (mobile == null || mobile.length() == 0) {
                return;
            }

            ArrayList<ContentProviderOperation> ops = new ArrayList<>();

            ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                    .withSelection(ContactsContract.Data.CONTACT_ID + "=? AND " + ContactsContract.Data.MIMETYPE + "=?",
                            new String[]{id, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE})
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name + (service ? " - 服務專員" : " - 即時保母"))
                    .build());

            ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                    .withSelection(ContactsContract.Data.CONTACT_ID + "=? AND " + ContactsContract.Data.MIMETYPE + "=? AND " + ContactsContract.CommonDataKinds.Phone.TYPE + "=?",
                            new String[]{id, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, String.valueOf(ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)})
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, mobile)
                    .build());

            try {
                ContentProviderResult[] res = getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            } catch (RemoteException | OperationApplicationException e) {
                e.printStackTrace();
            }
        }
    }

    private void removeContacts() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

            String[] contactProjection = new String[]{ContactsContract.Data.CONTACT_ID, ContactsContract.CommonDataKinds.Organization.TITLE, ContactsContract.CommonDataKinds.Organization.COMPANY};
            String contactSelection = "(" + ContactsContract.CommonDataKinds.Organization.TITLE + "=? OR " +
                    ContactsContract.CommonDataKinds.Organization.TITLE + "=? )" +
                    " AND " + ContactsContract.CommonDataKinds.Organization.COMPANY + "=?";
            String[] contactSelectionArgs = new String[]{"服務專員", "即時保母", "Luxgen"};

            try (Cursor cursor = getContentResolver().query(ContactsContract.Data.CONTENT_URI, contactProjection, contactSelection, contactSelectionArgs, null)) {
                while (cursor != null && cursor.moveToNext()) {
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID));
                    removeContact(id);
                }
            }
        }
    }

    private void removeContact(String id) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

            ArrayList<ContentProviderOperation> ops = new ArrayList<>();

            ops.add(ContentProviderOperation.newDelete(ContactsContract.RawContacts.CONTENT_URI)
                    .withSelection(ContactsContract.RawContacts._ID + "=?", new String[]{id})
                    .build());

            try {
                ContentProviderResult[] res = getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            } catch (RemoteException | OperationApplicationException e) {
                e.printStackTrace();
            }
        }
    }
}
