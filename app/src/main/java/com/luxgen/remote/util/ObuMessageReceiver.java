package com.luxgen.remote.util;

import android.content.Context;

import com.google.gson.Gson;
import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.model.ObuPushListWrapper;

import java.util.HashMap;
import java.util.Map;

import luxgen.service.IMessageListener;

public class ObuMessageReceiver implements IMessageListener {
    ObuManager mManager;
    Context mContext;

    public ObuMessageReceiver(ObuManager manager, Context context) {
        mManager = manager;
        mContext = context;
    }

    @Override
    public byte[] onMessageArrive(String s, byte[] bytes) throws Exception {
        String msg = new String(bytes);
        LogCat.d("onMessageArrive:" + msg);
        Gson gson = new Gson();
        Map<String, String> myMap = new HashMap<>();
        String errCde = "";
        ObuPushListWrapper wrapper = gson.fromJson(msg, ObuPushListWrapper.class);
        switch (wrapper.getAction()) {
            case ObuManager.REQUEST_ACTION_GET_PUSH_LIST:
                // TODO: 從 Network 取得 Push List，好像沒有實作的必要，再觀察
                break;
            case ObuManager.REQUEST_ACTION_SET_READ_STATUS://OBU call SetReadStatus

                myMap = ObuManager.getResponseObuMap(mContext, errCde, "");
                break;
            default:
                errCde = ObuManager.ERR_954_WRONG_ACTION;
                myMap = ObuManager.getResponseObuMap(mContext, errCde, "");
        }
        String json = gson.toJson(myMap);
        mManager.sendMessageToObu(json, wrapper.getAction());
        return new byte[]{};
    }
}
