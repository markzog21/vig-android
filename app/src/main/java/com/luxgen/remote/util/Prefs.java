package com.luxgen.remote.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.luxgen.remote.model.Beacon;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.option.BeaconOptions;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.ikey.IkeyLoginSettingData;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.UUID;

public class Prefs {

    public final static int MAX_NUMBER_SAMPLE_PM25 = 6;

    public final static int PREFS_MEMBER = 0x01;
    public final static int PREFS_REMOTE_CONTROL = 0x02;
    public final static int PREFS_SETTINGS = 0x04;
    public final static int PREFS_INITIAL_DATA = 0x08;
    public final static int PREFS_BEACONS = 0x10;
    public final static int PREFS_PM25 = 0x20;
    public final static int PREFS_DEVICE_ID = 0x40;
    public final static int PREFS_CAR_IDS = 0x80;
    // exclude device id
    public final static int PREFS_ALL = PREFS_MEMBER | PREFS_REMOTE_CONTROL | PREFS_SETTINGS | PREFS_INITIAL_DATA | PREFS_BEACONS | PREFS_PM25;

    private final static String PREFS_MEMBER_FN = "user_info";
    private final static String PREFS_REMOTE_CONTROL_FN = "remote_control";
    private final static String PREFS_SETTINGS_FN = "settings";
    private final static String PREFS_INITIAL_DATA_FN = "initial_data";
    private final static String PREFS_BEACONS_FN = "beacons";
    private final static String PREFS_FCM_TOKEN = "fcm_token";
    private final static String PREFS_PM25_FN = "pm25";
    private final static String PREFS_DEVICE_ID_FN = "device_id";
    private final static String PREFS_USER_IDS_FN = "user_ids";
    private final static String FILE_DEVICE_SENTRY_FN = "device_id_sentry";
    private final static String PREFS_PROGRESS = "repair_progress";

    private final static String KEY_DEVICE_ID = "deviceId";
    private final static String KEY_DEVICE_ID_CREATE_TIME = "deviceIdCreateTime";

    public final static String KEY_NO_LONGER_REMIND_BT_IN_DIGITAL_KEY_INACTIVE = "no_longer_remind_bt_in_digital_key_inactive";
    public final static String KEY_NO_LONGER_REMIND_BT_IN_REMOTE_CONTROL_AIR_DATA = "no_longer_remind_bt_in_remote_control_air_data";
    public final static String KEY_NO_LONGER_REMIND_BT_IN_REMOTE_CONTROL = "no_longer_remind_bt_in_remote_control";

    public final static String KEY_NO_LONGER_REMIND_AIR_POLLUTION = "no_longer_remind_air_pollution";

    public static String getEmployeeResponseDataJsonString(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_INITIAL_DATA_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getString("employee_response_data", "");
    }

    public static void setEmployeeResponseDataJsonString(Context context, String responseData) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_INITIAL_DATA_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("employee_response_data", responseData);
        editor.apply();
    }

    public static String getBeaconUUID(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_INITIAL_DATA_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getString("uuid", "");
    }

    public static void setBeaconUUID(Context context, String uuid) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_INITIAL_DATA_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("uuid", uuid);
        editor.apply();
    }

    public static void setBeacon(Context context, String place, Beacon beacon, String vigCarId) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_BEACONS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (beacon != null) {
            editor.putString("beacon_" + vigCarId + "_" + place + "_uuid", beacon.getUUID());
            editor.putInt("beacon_" + vigCarId + "_" + place + "_major", beacon.getMajor());
            editor.putInt("beacon_" + vigCarId + "_" + place + "_minor", beacon.getMinor());
            editor.putInt("beacon_" + vigCarId + "_" + place + "_rssi", beacon.getRssi());
            editor.putString("beacon_" + vigCarId + "_" + place + "_mac", beacon.getMac());
        } else {
            editor.remove("beacon_" + vigCarId + "_" + place + "_uuid");
            editor.remove("beacon_" + vigCarId + "_" + place + "_major");
            editor.remove("beacon_" + vigCarId + "_" + place + "_minor");
            editor.remove("beacon_" + vigCarId + "_" + place + "_rssi");
            editor.remove("beacon_" + vigCarId + "_" + place + "_mac");
        }
        editor.apply();
    }

    public static Beacon getBeacon(Context context, String place, String vigCarId) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_BEACONS_FN, Context.MODE_PRIVATE);
        String uuid = sharedPreferences.getString("beacon_" + vigCarId + "_" + place + "_uuid", null);
        int major = sharedPreferences.getInt("beacon_" + vigCarId + "_" + place + "_major", 0);
        int minor = sharedPreferences.getInt("beacon_" + vigCarId + "_" + place + "_minor", 0);
        int rssi = sharedPreferences.getInt("beacon_" + vigCarId + "_" + place + "_rssi", 0);
        String mac = sharedPreferences.getString("beacon_" + vigCarId + "_" + place + "_mac", null);

        Beacon beacon = new Beacon(uuid, major, minor);
        beacon.setRssi(rssi);
        beacon.setMac(mac);

        if (beacon.isValid()) {
            return beacon;
        }

        return null;
    }

    private static boolean hasBeacon(Context context, String place, String vigCarId) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_BEACONS_FN, Context.MODE_PRIVATE);
        String uuid = sharedPreferences.getString("beacon_" + vigCarId + "_" + place + "_uuid", null);
        int major = sharedPreferences.getInt("beacon_" + vigCarId + "_" + place + "_major", 0);
        int minor = sharedPreferences.getInt("beacon_" + vigCarId + "_" + place + "_minor", 0);

        return uuid != null && major > 0 && minor > 0;
    }

    public static boolean isKeylessDone(Context context, String vigCarId) {
        return hasBeacon(context, BeaconOptions.LEFT_FRONT, vigCarId) &&
                hasBeacon(context, BeaconOptions.RIGHT_FRONT, vigCarId) &&
                hasBeacon(context, BeaconOptions.TAILGATE, vigCarId);
    }

    public static int getPinCodeMaxTime(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_INITIAL_DATA_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getInt("pincode_max_time", 10);
    }

    public static void setPinCodeMaxTime(Context context, int maxTime) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_INITIAL_DATA_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("pincode_max_time", maxTime);
        editor.apply();
    }

    public static String getRepairCarQuestionnaireURL(Context context, String RoNo) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_INITIAL_DATA_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getString("repair_questionnaire", "") + "&entry.1410001611=" + RoNo;
    }

    public static void setRepairCarQuestionnaireURL(Context context, String url) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_INITIAL_DATA_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("repair_questionnaire", url);
        editor.apply();
    }

    public static long getDeviceIdCreateTime(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_DEVICE_ID_FN, Context.MODE_PRIVATE);
        Long time = sharedPreferences.getLong(KEY_DEVICE_ID_CREATE_TIME, 0);
        if (time == 0) {
            time = setDeviceIdCreateTime(context);
        }

        return time;
    }

    public static String getDeviceId(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_DEVICE_ID_FN, Context.MODE_PRIVATE);
        String deviceId = sharedPreferences.getString(KEY_DEVICE_ID, "");

        if (TextUtils.isEmpty(deviceId) || shouldRegenerateDeviceId(context)) {
            deviceId = UUID.randomUUID().toString();
            setDeviceId(context, deviceId);
            saveDeviceIdSentry(context, setDeviceIdCreateTime(context));
        }
        return deviceId;
    }

    private static void setDeviceId(Context context, String id) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_DEVICE_ID_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_DEVICE_ID, id);

        editor.apply();
    }

    private static void saveDeviceIdSentry(Context context, long time) {
        try {
            FileOutputStream fos = context.openFileOutput(FILE_DEVICE_SENTRY_FN, Context.MODE_PRIVATE);
            Writer out = new OutputStreamWriter(fos);
            out.write(String.valueOf(time));
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static long loadDeviceIdSentry(Context context) {
        try {
            FileInputStream fis = context.openFileInput(FILE_DEVICE_SENTRY_FN);
            BufferedReader r = new BufferedReader(new InputStreamReader(fis));
            String line = r.readLine();
            r.close();
            return Long.parseLong(line);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private static boolean shouldRegenerateDeviceId(Context context) {
        File file = context.getFileStreamPath(FILE_DEVICE_SENTRY_FN);
        if (file == null || !file.exists()) {
            return true;
        }

//        long sentryTime = loadDeviceIdSentry(context);
//        long deviceTime = getDeviceIdCreateTime(context);
//        if (Math.abs(sentryTime - deviceTime) > 5000) {
//            return true;
//        }

        return false;
    }

    private static long setDeviceIdCreateTime(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_DEVICE_ID_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        long time = System.currentTimeMillis();
        editor.putLong(KEY_DEVICE_ID_CREATE_TIME, time);

        editor.apply();

        return time;
    }

    public static IkeyLoginSettingData getLoginSettings(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_REMOTE_CONTROL_FN, Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("token", "");
        String timestamp = sharedPreferences.getString("timestamp", "");
        if (TextUtils.isEmpty(token) || TextUtils.isEmpty(timestamp)) {
            return null;
        }

        IkeyLoginSettingData data = new IkeyLoginSettingData();
        data.setToken(token);
        data.setTimeStamp(timestamp);

        return data;
    }

    public static void setLoginSettings(Context context, IkeyLoginSettingData data) {
        if (data == null) {
            return;
        }
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_REMOTE_CONTROL_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("token", data.getToken());
        editor.putString("timestamp", data.getTimeStamp());

        editor.apply();
    }

    public static String getKeyUid(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getString("ikeyUID", null);
    }

    public static String getGooglePassword(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getString("googlePassword", null);
    }

    public static String getFacebookPassword(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getString("facebookPassword", null);
    }

    public static UserInfo getUserInfo(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        String account = sharedPreferences.getString("account", null);
        String password = sharedPreferences.getString("password", null);

        if (TextUtils.isEmpty(account) || TextUtils.isEmpty(password)) {
            return null;
        }

        UserInfo userInfo = new UserInfo(account, password);
        userInfo.setEmail(sharedPreferences.getString("email", null));
        userInfo.setPhone(sharedPreferences.getString("phone", null));
        userInfo.setUid(sharedPreferences.getString("UID", null));
        userInfo.setKeyUid(sharedPreferences.getString("ikeyUID", null));
        userInfo.setToken(sharedPreferences.getString("ikeyToken", null));
        userInfo.setDeviceId(getDeviceId(context));
        userInfo.setFacebookPassword(sharedPreferences.getString("facebookPassword", null));
        userInfo.setGooglePassword(sharedPreferences.getString("googlePassword", null));

        return userInfo;
    }

    public static void setUserInfo(Context context, UserInfo userInfo) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("account", userInfo.getAccount());
        editor.putString("password", userInfo.getPassword());
        editor.putString("email", userInfo.getEmail());
        editor.putString("phone", userInfo.getPhone());
        editor.putString("UID", userInfo.getUid());
        editor.putString("ikeyUID", userInfo.getKeyUid());
        editor.putString("ikeyToken", userInfo.getToken());
        editor.putString("deviceId", userInfo.getDeviceId(context));
        editor.putString("facebookPassword", userInfo.getFacebookPassword());
        editor.putString("googlePassword", userInfo.getGooglePassword());

        editor.apply();
    }

    public static String getUserName(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        String name = sharedPreferences.getString("username", null);
        if (TextUtils.isEmpty(name)) {
            name = sharedPreferences.getString("email", null);
            name = name.substring(0, name.indexOf("@"));
        }
        return name;
    }

    public static void setUserName(Context context, String username) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("username", username);

        editor.apply();
    }

    // 讓使用者登出再登入時，可以找到最近一次選到的車輛
    public static CarKeyData getCarKeyDataByIKeyUid(Context context, String iKeyUid) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_USER_IDS_FN, Context.MODE_PRIVATE);
        CarKeyData carKeyData = null;

        try {
            carKeyData = (CarKeyData) ObjectSerializer.deserialize(
                    sharedPreferences.getString("car_key_data_with_uid_" + iKeyUid, null));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return carKeyData;
    }

    // 讓使用者登出再登入時，可以找到最近一次選到的車輛
    public static void setCarKeyDataWithIKeyUid(Context context, String iKeyUid, CarKeyData carKeyData) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_USER_IDS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        try {
            editor.putString("car_key_data_with_uid_" + iKeyUid, ObjectSerializer.serialize(carKeyData));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.apply();
    }

    // 讓使用者登出再登入時，可以找到最近一次選到的車輛
    public static void clearCarKeyDataWithIKeyUid(Context context, String iKeyUid) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_USER_IDS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.remove("car_key_data_with_uid_" + iKeyUid);
        editor.apply();
    }
/*
    public static String getKeyOwnerNickName(Context context, String iKeyUid, String phoneNumber) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_USER_IDS_FN, Context.MODE_PRIVATE);

        return sharedPreferences.getString("nickname_" + iKeyUid + "_" + phoneNumber, null);
    }

    public static void setKeyOwnerNickName(Context context, String iKeyUid, String phoneNumber, String nickname) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_USER_IDS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("nickname_" + iKeyUid + "_" + phoneNumber, nickname);
        editor.apply();
    }*/

    public static CarKeyData getCarKeyData(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        CarKeyData carKeyData = null;

        try {
            carKeyData = (CarKeyData) ObjectSerializer.deserialize(
                    sharedPreferences.getString("car_key_data", null));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return carKeyData;
    }

    public static void setCarKeyData(Context context, CarKeyData carKeyData) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        try {
            editor.putString("car_key_data", ObjectSerializer.serialize(carKeyData));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.apply();
    }

    public static void clearCarKeyData(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.remove("car_key_data");

        editor.apply();
    }

    public static int getPM25DataCount(Context context, String carId) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_PM25_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getInt("pm25_num_" + carId, 0);
    }

    public static long getPM25Time(Context context, String carId) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_PM25_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getLong("pm25_time_" + carId, 0);
    }

    public static String[] getPM25Data(Context context, String carId) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_PM25_FN, Context.MODE_PRIVATE);
        String rawData = sharedPreferences.getString("pm25_data_" + carId, null);
        if (rawData == null) {
            return null;
        }

        return rawData.split(",");
    }

    public static int updatePM25Data(Context context, String carId, long time, String val) {
        if (carId == null) {
            return -1;
        }

        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_PM25_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        String rawData = sharedPreferences.getString("pm25_data_" + carId, "");
        rawData = rawData.concat(val + ",");

        Integer numOfData = sharedPreferences.getInt("pm25_num_" + carId, 0);
        numOfData++;

        editor.putString("pm25_data_" + carId, rawData);
        editor.putLong("pm25_time_" + carId, time);
        editor.putInt("pm25_num_" + carId, numOfData);
        editor.apply();

        return numOfData;
    }

    public static void setCurrentNumOfCarKeyData(Context context, int numOfCarKeyData) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt("num_of_car_key_data", numOfCarKeyData);

        editor.apply();
    }

    public static int getCurrentNumOfCarKeyData(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getInt("num_of_car_key_data", 0);
    }

    public static void setRefreshCarStatusFlag(Context context, boolean needToRefresh) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean("refresh_car_status", needToRefresh);

        editor.apply();
    }

    public static boolean getRefreshCarStatusFlag(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("refresh_car_status", false);
    }

    public final static int FLAG_REFRESH_CAR_KEY_LIST_DONE = 0;
    public final static int FLAG_REFRESH_CAR_KEY_LIST_AND_OPEN_IT = 1;
    public final static int FLAG_REFRESH_CAR_KEY_LIST = 2;

    public static void setRefreshCarKeyListFlag(Context context, int flag) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt("refresh_car_key_list", flag);

        editor.apply();
    }

    public static int getRefreshCarKeyListFlag(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        // convertOldRefreshCarKeyListFlag(context);
        return sharedPreferences.getInt("refresh_car_key_list", FLAG_REFRESH_CAR_KEY_LIST_DONE);
    }

    private static void convertOldRefreshCarKeyListFlag(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        boolean newRefreshCarKeyList = sharedPreferences.getBoolean("new_refresh_car_key_list", false);
        if (!newRefreshCarKeyList) {
            try {
                boolean oldFlag = sharedPreferences.getBoolean("refresh_car_key_list", false);
                editor.putInt("refresh_car_key_list", oldFlag ? 1 : 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            editor.putBoolean("new_refresh_car_key_list", true);
            editor.apply();
        }
    }

    public static void setCheckAllKeysActivationStatusFlag(Context context, boolean needToCheck) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean("check_all_master_keys_activation_status", needToCheck);

        editor.apply();
    }

    public static boolean getCheckAllKeysActivationStatusFlag(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("check_all_master_keys_activation_status", false);
    }

    public static void setRefreshDigitalKeyListFlag(Context context, boolean needToRefresh) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean("refresh_digital_key_list", needToRefresh);

        editor.apply();
    }

    public static boolean getRefreshDigitalKeyListFlag(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("refresh_digital_key_list", false);
    }

    public static void setMaintenanceCity(Context context, String city) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("maintenance_city", city);

        editor.apply();
    }

    public static String getMaintenanceCity(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getString("maintenance_city", null);
    }

    public static void setMaintenancePlant(Context context, String plant) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("maintenance_plant", plant);

        editor.apply();
    }

    public static String getMaintenancePlant(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getString("maintenance_plant", null);
    }

    public static void clearMaintenancePlant(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.remove("maintenance_plant");

        editor.apply();
    }

    public static void clearCarModel(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.remove("car_model");

        editor.apply();
    }

    public static String getCarStatusLastUpdateTime(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getString("car_status_last_update_time", "");
    }

    public static void setCarStatusLastUpdateTime(Context context, String time) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("car_status_last_update_time", time);

        editor.apply();
    }

    public static String getPlateNumberComparison(Context context, String plateNumberWithoutDash) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getString(plateNumberWithoutDash, null);
    }

    public static void setPlateNumberComparison(Context context, String plateNumberWithoutDash, String plateNumberWithDash) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(plateNumberWithoutDash, plateNumberWithDash);

        editor.apply();
    }

    public static void clearPlateNumberComparison(Context context, String plateNumberWithoutDash) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.remove(plateNumberWithoutDash);
        editor.apply();
    }

    public static float getCarSetupTemperature(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_REMOTE_CONTROL_FN, Context.MODE_PRIVATE);

        return sharedPreferences.getFloat("temperature", -1);
    }

    public static void setCarSetupTemperature(Context context, float temperature) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_REMOTE_CONTROL_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putFloat("temperature", temperature);
        editor.apply();
    }

    public static int getOptimizeStep(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_REMOTE_CONTROL_FN, Context.MODE_PRIVATE);

        return sharedPreferences.getInt("optimize_step", MAX_NUMBER_SAMPLE_PM25);
    }

    public static void setOptimizeStep(Context context, int step) {
        if (step <= MAX_NUMBER_SAMPLE_PM25) {
            SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_REMOTE_CONTROL_FN, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            editor.putInt("optimize_step", step);
            editor.apply();
        }
    }

    public static long getOptimizeTime(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_REMOTE_CONTROL_FN, Context.MODE_PRIVATE);

        return sharedPreferences.getLong("optimize_time", -1);
    }

    public static void setOptimizeTime(Context context, long time) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_REMOTE_CONTROL_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putLong("optimize_time", time);
        editor.apply();
    }

    public static boolean isLastOptimizeSuccess(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_REMOTE_CONTROL_FN, Context.MODE_PRIVATE);

        return sharedPreferences.getBoolean("optimize_success", true);
    }

    public static void setLastOptimizeSuccess(Context context, boolean success) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_REMOTE_CONTROL_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean("optimize_success", success);
        editor.apply();
    }

    public static void setPinShowTime(Context context, long time) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("pin_time", time);
        editor.apply();
    }

    public static long getPinShowTime(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getLong("pin_time", 0);
    }

    public static String getPinCode(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);

        return sharedPreferences.getString("pincode", null);
    }

    public static void setPinCode(Context context, String pinCode) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("pincode", pinCode);
        editor.apply();
    }

    public static int incPinCodeErrorCount(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        int count = sharedPreferences.getInt("pinerrorcount", 0) + 1;

        editor.putInt("pinerrorcount", count);
        editor.apply();

        return count;
    }

    public static void resetPinCodeErrorCount(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.remove("pinerrorcount");
        editor.apply();
    }

    public static Boolean isNotificationEnable(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);

        return sharedPreferences.getBoolean("notification_enable", false);
    }

    public static void setNotificationAccept(Context context, boolean enable) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean("notification_enable", enable);
        editor.apply();
    }

    public static Boolean isFcmTokenEmpty(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);

        String token = sharedPreferences.getString(PREFS_FCM_TOKEN, "");
        return TextUtils.isEmpty(token);
    }

    public static void setFcmToken(Context context, String token) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(PREFS_FCM_TOKEN, token);
        editor.apply();
    }

    public static String getRepairProgress(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getString(PREFS_PROGRESS, "");
    }

    public static void setRepairProgress(Context context, String progress) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(PREFS_PROGRESS, progress);
        editor.apply();
    }

    public static String getFcmToken(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getString(PREFS_FCM_TOKEN, "");
    }

    public static void setMasterKeyDeleteTime(Context context, long time) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putLong("master_key_delete_time", time);
        editor.apply();
    }

    public static long getMasterKeyDeleteTime(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getLong("master_key_delete_time", 0);
    }

    public static void setMasterKeyDeleteId(Context context, String id) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("master_key_delete_id", id);
        editor.apply();
    }

    public static String getMasterKeyDeleteId(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getString("master_key_delete_id", "");
    }

    public static void clearMasterKeyDeleteProcess(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.remove("master_key_delete_id");
        editor.remove("master_key_delete_time");
        editor.apply();
    }

    public static void clear(Context context, int prefs) {
        if ((prefs & PREFS_MEMBER) == PREFS_MEMBER) {
            SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_MEMBER_FN, Context.MODE_PRIVATE);
            String facebookPassword = sharedPreferences.getString("facebookPassword", null);
            String googlePassword = sharedPreferences.getString("googlePassword", null);
            sharedPreferences.edit().clear().apply();
            sharedPreferences.edit().putString("facebookPassword", facebookPassword).apply();
            sharedPreferences.edit().putString("googlePassword", googlePassword).apply();
        }

        if ((prefs & PREFS_REMOTE_CONTROL) == PREFS_REMOTE_CONTROL) {
            SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_REMOTE_CONTROL_FN, Context.MODE_PRIVATE);
            sharedPreferences.edit().clear().apply();
        }

        if ((prefs & PREFS_SETTINGS) == PREFS_SETTINGS) {
            SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
            sharedPreferences.edit().clear().apply();
            setNotificationAccept(context, true);
        }

        if ((prefs & PREFS_INITIAL_DATA) == PREFS_INITIAL_DATA) {
            SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_INITIAL_DATA_FN, Context.MODE_PRIVATE);
            sharedPreferences.edit().clear().apply();
        }

        if ((prefs & PREFS_BEACONS) == PREFS_BEACONS) {
            SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_BEACONS_FN, Context.MODE_PRIVATE);
            sharedPreferences.edit().clear().apply();
        }

        if ((prefs & PREFS_PM25) == PREFS_PM25) {
            SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_PM25_FN, Context.MODE_PRIVATE);
            sharedPreferences.edit().clear().apply();
        }

        if ((prefs & PREFS_DEVICE_ID) == PREFS_DEVICE_ID) {
            SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_DEVICE_ID_FN, Context.MODE_PRIVATE);
            sharedPreferences.edit().clear().apply();
            File file = context.getFileStreamPath(FILE_DEVICE_SENTRY_FN);
            if (file != null && file.exists()) {
                file.delete();
            }
        }
    }

    public static int getAvailableActionId(Context context, int preAllocate) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        int actionId = sharedPreferences.getInt("actionId", 0);
        actionId++;

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("actionId", actionId + preAllocate);
        editor.apply();

        return actionId;
    }

    public static int getAvailableActionId(Context context) {
        return getAvailableActionId(context, 0);
    }

    public static void resetActionId(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.remove("actionId");
        editor.apply();
    }

    public static void setPreferenceKeyValue(Context context, String key, boolean val) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean(key, val);
        editor.apply();
    }

    public static boolean getPreferenceKeyValue(Context context, String key) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key, false);
    }

    public static void toggleCaLog(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        boolean showLog = sharedPreferences.getBoolean("show_ca_log", false);
        editor.putBoolean("show_ca_log", !showLog);
        editor.apply();
    }

    public static boolean shouldShowCaLog(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("show_ca_log", false);
    }

    public static boolean isWheelEnabled(Context context) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_SETTINGS_FN, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("wheel_menu", false);
    }
}
