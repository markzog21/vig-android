package com.luxgen.remote.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.luxgen.remote.R;

public class DialUtils {
    public static final String PHONE = "phone";

    public static Intent callPolice() {
        String phone = "110";
        return getIntent(phone);
    }

    public static Intent videoPolice(Context context) {
        String link = "tw.gov.npa.videocall";
        return goOtherApp(context, link);
    }

    public static Intent smartEats(Context context) {
        String link = "com.molife.foodnearby";
        return goOtherApp(context, link);
    }

    public static Intent soundPlus(Context context) {
        String link = "com.ryan.audioplayer";
        return goOtherApp(context, link);
    }

    public static Intent callSales(Context context) {
        return getIntent(context.getString(R.string.dial_phone_sales));
    }

    public static Intent callService(Context context) {
        return getIntent(context.getString(R.string.dial_phone_service));
    }

    public static Intent callRoadRescue(Context context) {
        return getIntent(context.getString(R.string.dial_phone_road_rescue));
    }

    public static Intent callSecretary(Context context) {
        return getIntent(context.getString(R.string.dial_phone_secretary));
    }

    public static Intent callTaxi(Context context) {
        return getIntent(context.getString(R.string.dial_phone_taxi));
    }

    public static Intent callReservation(Context context) {
        return getIntent(context.getString(R.string.dial_phone_reservation));
    }

    private static Intent getIntent(String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
        intent.putExtra(PHONE, phone);

        return intent;
    }

    private static Intent goOtherApp(Context context, String packageName) {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);

        if (intent != null) {
            intent.putExtra(PHONE, packageName);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        } else {
            try {
                // Open app with Google Play app
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            } catch (android.content.ActivityNotFoundException anfe) {
                // Open Google Play website
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + packageName));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        }

        return intent;
    }
}
