package com.luxgen.remote.util;

import android.content.Context;

import com.google.gson.Gson;
import com.luxgen.remote.R;
import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.custom.SendObuMsgResultListener;
import com.luxgen.remote.model.ObuPushData;
import com.luxgen.remote.model.ObuPushListWrapper;
import com.luxgen.remote.model.PushData;
import com.luxgen.remote.network.model.assistant.GetPushResponseData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import luxgen.service.LuxgenLinkService;

public class ObuManager {
    private static final String OBU_PACKAGE_NAME = "com.antec.luxgen.lwp";
    private static final String OBU_TAG = "performSyncPushList";
    private static final String OBU_TAG_READ_STATUS = "SyncReadStatus";
    private static final String OBU_TAG_LOGOUT = "syncLogoutWrapper";
    private static final String ACTION_SYNC_PUSH_LIST = "SyncPushList";
    private static final String ACTION_SYNC_READ_STATUS = "SyncReadStatus";
    private static final String ACTION_SYNC_LOGOUT = "Logout";
    private static final String DEV_OBU_PAYLOAD = "obu_payload";
    public static final String REQUEST_ACTION_GET_PUSH_LIST = "GetPushList";
    public static final String REQUEST_ACTION_SET_READ_STATUS = "SetReadStatus";

    public static final String KEY_ERR_CDE = "errCde";
    public static final String KEY_ERR_MSG = "errMsg";

    public static final String ERR_954_WRONG_ACTION = "-954";//Action無法識別(-954)
    public static final String ERR_962_WRONG_READ_STATUS = "-962";//閱讀狀態格式設定有誤(-962)
    public static final String ERR_952_NO_NEWWORK = "-952";//手機無網路(-952)
    public static final String ERR_949_NOT_LOGIN = "-949";//使用者未登入(-949)
    public static final String ERR_950_SERVER_RETURN_ABNORMAL = "-950";//server回應異常(-950)
    public static final String ERR_999_EXCEPTION = "-999";//系統發生錯誤，請聯繫客服人員(-999)

    // Status
    private static final String OK00 = "00";//成功驗證
    private static final String ERR_01 = "01";//代表手機處於無網路狀態
    private static final String ERR_02 = "02";//代表Server端回傳資料有誤
    private static final String ERR_99 = "99";//代表預料外錯誤
    private static final String ERR_MSG_01 = "no network";//代表手機處於無網路狀態
    private static final String ERR_MSG_02_SERVER_BODY_NULL = "server body is null";
    private static final String ERR_MSG_02_ERR_CDE_NULL = "errCde is null";

    volatile static ObuManager instance;

    private Context mContext;

    public static ObuManager getInstance(Context context) {
        if (instance == null) {
            synchronized (ObuManager.class) {
                instance = new ObuManager(context);
            }
        }
        return instance;
    }

    private ObuManager(Context context) {
        mContext = context;
    }

    private void syncPushList(GetPushResponseData data) {
        ObuPushListWrapper wrapper = new ObuPushListWrapper();
        wrapper.setAction(ACTION_SYNC_PUSH_LIST);

        if (data.getErrorCode().equals(OK00)) {
            wrapper.setStatus(OK00);
            wrapper.setSize(data.getSize());

            final List<ObuPushData> opds = convert(data.getPushList());
            wrapper.setPushList(opds);
        } else {
            wrapper.setStatus(ERR_02);
            wrapper.setStatusMsg(data.getErrorMessage());
        }

        sync(wrapper, OBU_TAG);
    }

    public void syncPushList(ArrayList<PushData> list) {
        if (list == null || list.size() <= 0) {
            return;
        }

        ObuPushListWrapper wrapper = new ObuPushListWrapper();
        wrapper.setAction(ACTION_SYNC_PUSH_LIST);
        wrapper.setStatus(OK00);
        wrapper.setSize(list.size());
        wrapper.setPushList(convert(list));
        sync(wrapper, OBU_TAG);
    }

    public void syncReadStatus(ArrayList<PushData> list) {
        if (list.size() <= 0) {
            return;
        }

        ObuPushListWrapper wrapper = new ObuPushListWrapper();
        wrapper.setAction(ACTION_SYNC_READ_STATUS);
        wrapper.setStatus(OK00);

        final List<ObuPushData> opds = convert(list);
        wrapper.setPushList(opds);

        sync(wrapper, OBU_TAG_READ_STATUS);
    }

    public void syncLogout() {
        ObuPushListWrapper wrapper = new ObuPushListWrapper();
        wrapper.setAction(ACTION_SYNC_LOGOUT);

        sync(wrapper, OBU_TAG_LOGOUT);
    }

    private void sync(ObuPushListWrapper wrapper, String tag) {
        SendObuMsgResultListener sendObuMsgResultListener = new SendObuMsgResultListener();
        sendObuMsgResultListener.setTag(tag);

        String json = new Gson().toJson(wrapper);
        LogCat.d("sync:" + json);
        LuxgenLinkService.getInstance(mContext)
                .sendMessage(OBU_PACKAGE_NAME, json.getBytes(), sendObuMsgResultListener);
    }

    public void sendMessageToObu(String msg, String tag) {
        if (msg == null) {
            return;
        }
        LogCat.d("sendMessageToObu:" + msg);
        SendObuMsgResultListener sendObuMsgResultListener = new SendObuMsgResultListener();
        sendObuMsgResultListener.setTag(tag);
        LuxgenLinkService.getInstance(mContext).sendMessage(mContext.getPackageName(), msg.getBytes(), sendObuMsgResultListener);
    }

    private List<ObuPushData> convert(ArrayList<PushData> list) {
        List<ObuPushData> result = new ArrayList<>();
        for (PushData pd : list) {
            result.add(ObuPushData.convert(pd));
        }
        return result;
    }

    public static Map<String, String> getResponseObuMap(Context context, String errCde, String defaultErrMsg) {
        Map<String, String> myMap = new HashMap<String, String>();
        switch (errCde) {
            case OK00:
                myMap.put(KEY_ERR_CDE, errCde);
                break;
            case ERR_954_WRONG_ACTION:
                myMap.put(KEY_ERR_CDE, errCde);
                myMap.put(KEY_ERR_MSG, context.getString(R.string.err_msg_954));
                break;
            case ERR_962_WRONG_READ_STATUS:
                myMap.put(KEY_ERR_CDE, errCde);
                myMap.put(KEY_ERR_MSG, context.getString(R.string.err_msg_962));
                break;
            case ERR_999_EXCEPTION:
                myMap.put(KEY_ERR_CDE, errCde);
                myMap.put(KEY_ERR_MSG, context.getString(R.string.err_msg_999_obu));
                break;
            case ERR_952_NO_NEWWORK:
                myMap.put(KEY_ERR_CDE, errCde);
                myMap.put(KEY_ERR_MSG, context.getString(R.string.err_msg_952));
                break;
            case ERR_949_NOT_LOGIN:
                myMap.put(KEY_ERR_CDE, errCde);
                myMap.put(KEY_ERR_MSG, context.getString(R.string.err_msg_949));
                break;
            case ERR_950_SERVER_RETURN_ABNORMAL:
                myMap.put(KEY_ERR_CDE, errCde);
                myMap.put(KEY_ERR_MSG, context.getString(R.string.err_msg_950));
                break;
            default:
                myMap.put(KEY_ERR_CDE, errCde);
                myMap.put(KEY_ERR_MSG, defaultErrMsg);

        }
        return myMap;
    }
}
