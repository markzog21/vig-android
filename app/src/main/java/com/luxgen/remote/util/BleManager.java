package com.luxgen.remote.util;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.ParcelUuid;
import android.text.TextUtils;

import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.model.Beacon;

import java.util.ArrayList;
import java.util.List;

public class BleManager {

    public interface ScanResultCallback {
        void onReceiveBeacon(Beacon beacon);

        void onReceiveVig(BluetoothDevice vig);

        void onTimeout();
    }

    private static final long SCAN_PERIOD = 10000; //10 seconds
    private final static ParcelUuid LUXGEN_SERVICE_UUID = ParcelUuid.fromString("6E400001-B5A3-F393-E0A9-E50E24DCCA9E");

    private Context context;
    private Handler mHandler;

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mBluetoothLeScanner;
    private ScanResultCallback mScanResultCallback;

    private String mFilterManufacturerUUID = null;
    private int mFilterManufacturerId = -1;

    private boolean mScanForBeacon = false;
    private boolean mScanForVig = false;
    private long mScanPeriod = SCAN_PERIOD;
    private boolean mEnableBeaconThreshold = false;

    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            ScanRecord scanRecord = result.getScanRecord();
            if (scanRecord == null) {
                return;
            }

            byte[] data = scanRecord.getBytes();
            if (data == null) {
                return;
            }

            int rssi = result.getRssi();
            BluetoothDevice device = result.getDevice();
            String address = device.getAddress();

            if (mScanForBeacon && mScanResultCallback != null) {
                Beacon beacon = new Beacon.Builder()
                        .setData(data)
                        .setAddress(address)
                        .setRssi(rssi)
                        .build();

                if (beacon.isValid()) {
                    if (mEnableBeaconThreshold) {
                        if (beacon.isNearby()) {
                            mScanResultCallback.onReceiveBeacon(beacon);
                        }
                    } else {
                        mScanResultCallback.onReceiveBeacon(beacon);
                    }
                }
            }

            if (mScanForVig && mScanResultCallback != null) {
                String deviceName = device.getName();
                if (deviceName != null && deviceName.startsWith("VIG")) {
                    mScanResultCallback.onReceiveVig(device);
                }
            }
        }
    };

    private Runnable mStopBleScanRunnable = new Runnable() {
        @Override
        public void run() {
            if (mBluetoothLeScanner != null) {
                mBluetoothLeScanner.stopScan(mScanCallback);
                if (mScanResultCallback != null) {
                    mScanResultCallback.onTimeout();
                }
            }
        }
    };

    public BleManager(Context context) {
        this.context = context;
        this.mHandler = new Handler(context.getMainLooper());

        init();
    }

    private void init() {
        BluetoothManager mBluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        if (mBluetoothManager != null) {
            mBluetoothAdapter = mBluetoothManager.getAdapter();
            if (mBluetoothAdapter != null) {
                mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
            }
        }
    }

    public boolean isDeviceSupportBLE() {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
    }

    public boolean isOpenBluetoothForAll() {
        if (mBluetoothAdapter == null) {
            return false;
        }

        if (!mBluetoothAdapter.isEnabled() ||
                (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_TURNING_OFF) ||
                (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_OFF)) {
            return false;
        }

        return true;
    }

    public boolean isOpenBluetooth() {
        if (isOpenBluetoothForAll()) {
            if (mBluetoothLeScanner == null) {
                mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
            }

            return (mBluetoothLeScanner != null);
        }

        return false;
    }

    private ScanFilter getBeaconFilter() {
        final ScanFilter.Builder builder = new ScanFilter.Builder();
        if (mFilterManufacturerId != -1 && mFilterManufacturerUUID != null && mFilterManufacturerUUID.length() > 0) {
            byte[] manData = new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            byte[] mask = new byte[]{0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0};
            System.arraycopy(Hex.toByteArray(mFilterManufacturerUUID.replace("-", "")), 0, manData, 2, 16);
            builder.setManufacturerData(mFilterManufacturerId, manData, mask);
        } else {
            LogCat.d("mFilterManufacturerId:" + mFilterManufacturerId);
            LogCat.d("mFilterManufacturerUUID:" + mFilterManufacturerUUID);
        }
        return builder.build();
    }

    private ScanFilter getVigFilter() {
        final ScanFilter.Builder builder = new ScanFilter.Builder();
        builder.setServiceUuid(LUXGEN_SERVICE_UUID);
        return builder.build();
    }

    private List<ScanFilter> getScanFilters() {
        List<ScanFilter> scanFilters = new ArrayList<>();
        scanFilters.add(getBeaconFilter());
        scanFilters.add(getVigFilter());

        return scanFilters;
    }

    private ScanSettings getScanSettings() {
        final ScanSettings.Builder builder = new ScanSettings.Builder();
        builder.setReportDelay(0);
        builder.setScanMode(ScanSettings.SCAN_MODE_BALANCED);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            builder.setMatchMode(android.bluetooth.le.ScanSettings.MATCH_MODE_AGGRESSIVE);
            builder.setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES);
        }
        return builder.build();
    }

    private void scanLeDevice(final boolean enable) {
        mHandler.removeCallbacks(mStopBleScanRunnable);

        if (!isDeviceSupportBLE() || !isOpenBluetooth()) {
            return;
        }

        if (enable) {
            if (mScanPeriod != -1) {
                mHandler.postDelayed(mStopBleScanRunnable, mScanPeriod);
            }
            List<ScanFilter> scanFilters = getScanFilters();
            ScanSettings scanSettings = getScanSettings();
            mBluetoothLeScanner.startScan(scanFilters, scanSettings, mScanCallback);
        } else {
            mBluetoothLeScanner.stopScan(mScanCallback);
        }
    }

    public void stop() {
        scanLeDevice(false);
    }

    public void start() {
        scanLeDevice(true);
    }

    public void setFilterManufacturer(int id, String uuid) {
        mFilterManufacturerId = id;
        mFilterManufacturerUUID = uuid;
    }

    public void setScanResultCallback(ScanResultCallback callback) {
        mScanResultCallback = callback;
    }

    public void scanForBeacon(boolean set) {
        mScanForBeacon = set;
    }

    public void scanForVig(boolean set) {
        mScanForVig = set;
    }

    public void setScanPeriod(long period) {
        mScanPeriod = period;
    }

    public void enableBeaconThreshold(boolean enable) {
        mEnableBeaconThreshold = enable;
    }

    public boolean isScanningForBeacon() {
        return mScanForBeacon;
    }
}
