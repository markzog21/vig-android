package com.luxgen.remote.util;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimeUtils {

    private static final String SERVER_FORMAT = "yyyy/MM/dd HH:mm:ss";
    private static final String FILE_FORMAT = "yyyyMMdd_HHmmss";

    private static Date parseServerData(String time) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(SERVER_FORMAT, Locale.TAIWAN);
        Date date;
        date = formatter.parse(time);
        return date;
    }

    @SuppressLint("SimpleDateFormat")
    public static String getCurrentTimeString() {
        return new SimpleDateFormat(FILE_FORMAT).format(new Date());
    }

    public static long getCurrentTime() {
        return new Date().getTime();
    }

    public static long parseToLong(String serverTimeFormat) throws ParseException {
        Date date = parseServerData(serverTimeFormat);
        return date == null ? 0 : dateToLong(date);
    }

    public static String parseToServerFormateString(long serverTime) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(SERVER_FORMAT, Locale.TAIWAN);
        return formatter.format(new Date(serverTime));
    }

    private static long dateToLong(Date date) {
        return date.getTime();
    }
}
