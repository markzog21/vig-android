package com.luxgen.remote.util;

import android.content.Context;
import android.util.DisplayMetrics;

public class UnitUtils {

    /**
     * Covert sp to px
     *
     * @param sp
     * @param context
     * @return pixel
     */
    public static int convertSpToPixel(float sp, Context context) {
        float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return Math.round(sp * fontScale + 0.5f);
    }

    /**
     * 取得螢幕密度
     * 120dpi = 0.75
     * 160dpi = 1 (default)
     * 240dpi = 1.5
     *
     * @param context
     * @return
     */
    public static float getDensity(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.density;
    }
}
