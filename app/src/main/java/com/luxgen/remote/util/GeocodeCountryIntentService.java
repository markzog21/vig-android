package com.luxgen.remote.util;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.LogCat;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GeocodeCountryIntentService extends IntentService {

    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String PACKAGE_NAME = "com.luxgen.remote";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";

    private ResultReceiver resultReceiver;
    private static final String TAG = "GeocodeCountry";

    public GeocodeCountryIntentService() {
        super("GeocodeCountryIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;

        resultReceiver = intent.getParcelableExtra(RECEIVER);
        Location location = intent.getParcelableExtra(LOCATION_DATA_EXTRA);

        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        } catch (IOException ioException) {
            LogCat.e("Service Not Available" + "\n" + ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            LogCat.e("Invalid Latitude or Longitude Used. " +
                    "Latitude = " + location.getLatitude() + ", Longitude = " +
                    location.getLongitude() + "\n" + illegalArgumentException);
        }

        if (addresses == null || addresses.size() == 0) {
            deliverResultToReceiver(FAILURE_RESULT, getString(R.string.location_not_found));
        } else {
            Address address = addresses.get(0);

            String adminArea = address.getAdminArea();
            String subAdminArea = address.getSubAdminArea();
            if (adminArea != null) {
                deliverResultToReceiver(SUCCESS_RESULT, adminArea);
            } else {
                deliverResultToReceiver(SUCCESS_RESULT, subAdminArea);
            }
        }
    }

    private void deliverResultToReceiver(int resultCode, String message) {
        Bundle bundle = new Bundle();
        bundle.putString(RESULT_DATA_KEY, message);
        resultReceiver.send(resultCode, bundle);
    }
}
