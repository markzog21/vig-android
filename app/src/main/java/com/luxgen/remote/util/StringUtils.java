package com.luxgen.remote.util;

import android.arch.persistence.room.util.StringUtil;

public class StringUtils extends StringUtil {
    public static StringBuilder appendComma(StringBuilder sb, String s) {
        if (sb == null) {
            sb = new StringBuilder();
        }

        if (sb.length() > 0) {
            sb.append(", ");
        }

        sb.append(s);
        return sb;
    }
}
