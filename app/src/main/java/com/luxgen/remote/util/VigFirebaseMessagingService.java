package com.luxgen.remote.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.luxgen.remote.R;
import com.luxgen.remote.activity.MainActivity;
import com.luxgen.remote.custom.LogCat;

import org.json.JSONException;

public class VigFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            if (Prefs.isNotificationEnable(this)) {
                handleMessage(remoteMessage);
            }
        } catch (Exception e) {
            LogCat.e(e.toString());
        }
    }

    private void handleMessage(RemoteMessage remoteMessage) throws InterruptedException, JSONException {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.notification_channel_id);

        String title = getString(R.string.app_name);
        String content = TextUtils.isEmpty(remoteMessage.getNotification().getBody()) ? "" : remoteMessage.getNotification().getBody();

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(content)
                        .setAutoCancel(true)
                        .setContentIntent(pi)
                        .setDefaults(Notification.DEFAULT_ALL);

        Notification notify = notificationBuilder.build();
//        notify.flags = Notification.FLAG_NO_CLEAR|Notification.FLAG_ONGOING_EVENT;

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    getString(R.string.notification_channel_name),
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setShowBadge(true);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0, notify);
    }
}
