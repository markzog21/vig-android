package com.luxgen.remote.util;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.VigRequestData;
import com.luxgen.remote.network.model.assistant.GetPm25ResponseData;
import com.luxgen.remote.network.model.assistant.Pm25Data;

import java.text.ParseException;
import java.util.ArrayList;

public class PM25Service extends Service {

    public final static String PM25_ERROR_REASON_MULTI = "pm25_error_reason_multi";
    public final static String PM25_ERROR_REASON_SERVER = "pm25_error_reason_server";
    public final static String PM25_ERROR_EXTRA_MESSAGE = "pm25_error_extra_message";
    public final static String PM25_UPDATE_DATA = "pm25_update_data";
    public final static String PM25_INPUT_CARID = "pm25_input_carid";

    private final static int CHECK_PM25_INTERVAL_MS = 28000;

    private String mCarId;
    private Handler mHandler;
    private boolean mSkipForFirstTime = true;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            if (intent.hasExtra(PM25_INPUT_CARID) && mSkipForFirstTime) {
                mCarId = intent.getStringExtra(PM25_INPUT_CARID);
                queryPM25(mCarId);
            }
        }
        return START_STICKY;
    }

    private Context getContext() {
        return this;
    }

    private String getCarId() {
        return mCarId;
    }

    private int saveNewData(String carId, Pm25Data pm25Data) {
        if (carId == null || pm25Data == null) {
            return -1;
        }

        String val = String.valueOf(pm25Data.getRange());
        long time = 0;
        try {
            time = TimeUtils.parseToLong(pm25Data.getTime());
        } catch (ParseException ignored) {
        }

        long lastTimeStamp = Prefs.getPM25Time(getContext(), carId);
        if (pm25Data.getTimeStamp() > lastTimeStamp) {
            return Prefs.updatePM25Data(getContext(), carId, time, val);
        }

        return Prefs.getPM25DataCount(getContext(), carId);
    }

    private void showServerErrorDialog(String failMessage) {
        Intent intent = new Intent();
        intent.setAction(PM25_ERROR_REASON_SERVER);
        intent.putExtra(PM25_ERROR_EXTRA_MESSAGE, failMessage);
        LocalBroadcastManager.getInstance(this.getApplicationContext()).sendBroadcast(intent);
    }

    private void showMultiLoginErrorDialog() {
        Intent intent = new Intent();
        intent.setAction(PM25_ERROR_REASON_MULTI);
        LocalBroadcastManager.getInstance(this.getApplicationContext()).sendBroadcast(intent);
    }

    private void notifyUpdates() {
        Intent intent = new Intent();
        intent.setAction(PM25_UPDATE_DATA);
        LocalBroadcastManager.getInstance(this.getApplicationContext()).sendBroadcast(intent);
    }

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            queryPM25(getCarId());
        }
    };

    private int syncLostPM25Data(ArrayList<Pm25Data> pm25DataArrayList) {
        int currentPM25Count = Prefs.getPM25DataCount(getContext(), getCarId());
        if (currentPM25Count == 0) {
            return 0;
        }
        int remainPM25Count = Prefs.MAX_NUMBER_SAMPLE_PM25 - currentPM25Count;
        int addedPM25Count = 0;
        long lastPM25Time = Prefs.getPM25Time(getContext(), getCarId());
        for (int i = 0; i < pm25DataArrayList.size(); i++) {
            Pm25Data pm25DataFromServer = pm25DataArrayList.get(i);
            if (pm25DataFromServer.getTimeStamp() == lastPM25Time) {
                for (; addedPM25Count < remainPM25Count; addedPM25Count++) {
                    int index = i - (addedPM25Count + 1);
                    if (index > 0) {
                        saveNewData(getCarId(), pm25DataArrayList.get(index));
                    } else {
                        break;
                    }
                }
                break;
            }
        }
        return currentPM25Count + addedPM25Count;
    }

    private int getPosOfPm25Data(ArrayList<Pm25Data> pm25DataArrayList, long pm25Time) {
        if (pm25DataArrayList == null) {
            return -1;
        }

        for (int i = 0; i < pm25DataArrayList.size(); i++) {
            Pm25Data pm25DataFromServer = pm25DataArrayList.get(i);
            if (pm25DataFromServer.getTimeStamp() < pm25Time) {
                return i;
            }
        }

        return -1;
    }

    private OnApiListener<GetPm25ResponseData> mOnPM25ApiListener = new OnApiListener<GetPm25ResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetPm25ResponseData responseData) {
            if (responseData.isSuccessful() && responseData.getSize() != 0) {
                ArrayList<Pm25Data> pm25DataArrayList = responseData.getList();
                int num = Prefs.getPM25DataCount(getContext(), getCarId());
                if (mSkipForFirstTime) { // 同步失去的數據
                    if (num != 0) {
                        int pos = getPosOfPm25Data(pm25DataArrayList, Prefs.getOptimizeTime(getContext()));
                        if (pos > Prefs.MAX_NUMBER_SAMPLE_PM25 - 1 || num == -1) {
                            pos = Prefs.MAX_NUMBER_SAMPLE_PM25 - 1;
                        }
                        Prefs.clear(getContext(), Prefs.PREFS_PM25);
                        for (int i = pos; i >= 0; i--) {
                            Pm25Data pm25Data = pm25DataArrayList.get(i);
                            saveNewData(getCarId(), pm25Data);
                        }
                    }
                    mSkipForFirstTime = false;
                } else {
                    int arraySize = pm25DataArrayList.size();
                    if (num > arraySize) {
                        num = arraySize;
                    }
                    if (num > Prefs.MAX_NUMBER_SAMPLE_PM25 - 1) {
                        num = Prefs.MAX_NUMBER_SAMPLE_PM25 - 1;
                    }
                    Prefs.clear(getContext(), Prefs.PREFS_PM25);
                    for (int i = num; i >= 0; i--) {
                        Pm25Data pm25Data = pm25DataArrayList.get(i);
                        saveNewData(getCarId(), pm25Data);
                    }
                }

                mHandler.postDelayed(mRunnable, CHECK_PM25_INTERVAL_MS);
                notifyUpdates();
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private void queryPM25(String carId) {
        if (carId == null) {
            return;
        }

        UserInfo userInfo = Prefs.getUserInfo(getContext());
        if (userInfo == null) {
            return;
        }

        VigRequestData data = new VigRequestData(userInfo.getKeyUid(), userInfo.getToken(), carId);

        AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(getContext());
        assistantWebApi.getPm25(data, mOnPM25ApiListener);
    }
}
