package com.luxgen.remote.util;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.google.android.gms.maps.model.LatLng;
import com.luxgen.remote.custom.LogCat;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GeocodeLocationIntentService extends IntentService {

    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String PACKAGE_NAME = "com.luxgen.remote";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";

    private ResultReceiver resultReceiver;
    private static final String TAG = "GeocodeLocation";

    public GeocodeLocationIntentService() {
        super("GeocodeLocationIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;

        resultReceiver = intent.getParcelableExtra(RECEIVER);
        String address = intent.getStringExtra(LOCATION_DATA_EXTRA);

        try {
            addresses = geocoder.getFromLocationName(address, 1);
        } catch (IOException ioException) {
            LogCat.e("Service Not Available" + "\n" + ioException);
        }

        if (addresses == null || addresses.size() == 0) {
            deliverResultToReceiver(FAILURE_RESULT, null);
        } else {
            LatLng latLng = new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
            deliverResultToReceiver(SUCCESS_RESULT, latLng);
        }
    }

    private void deliverResultToReceiver(int resultCode, LatLng latlng) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(RESULT_DATA_KEY, latlng);
        resultReceiver.send(resultCode, bundle);
    }
}
