package com.luxgen.remote.ui.maintenance;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomFragmentsAppCompatActivity;
import com.luxgen.remote.network.model.maintenance.RepairHistory;

public class MaintenanceHistoryDetailActivity extends CustomFragmentsAppCompatActivity {

    private RepairHistory mData;

    public static Intent newIntent(Context context, RepairHistory data) {
        Intent intent = new Intent(context, MaintenanceHistoryDetailActivity.class);
        intent.putExtra("data", data);
        return intent;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_maintenance_history_detail;
    }

    @Override
    protected void setupTitle(String tag) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getParcelableExtra("data");
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.activity_maintenance_history_title);
        } else {
            setTitle(R.string.activity_maintenance_history_title);
        }

        show(MaintenanceHistoryDetailFragment.newInstance(mData));
        bindCaService();
    }

    @Override
    protected void onDestroy() {
        unbindCaService();
        super.onDestroy();
    }
}
