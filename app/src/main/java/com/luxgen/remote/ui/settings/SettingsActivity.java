package com.luxgen.remote.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomFragmentsAppCompatActivity;

public class SettingsActivity extends CustomFragmentsAppCompatActivity {

    public static final int SETTINGS_MENU = 0;
    public static final int SETTINGS_PIN = 1;
    public static final int SETTINGS_INFO = 2;
    public static final int SETTINGS_CHANGE_PASSWORD = 3;

    private static final String BUNDLE_KEY_START_PAGE = "startPage";

    private int mStartPage = SETTINGS_MENU;

    public static Intent newIntent(Context context, int startPage) {
        Intent intent = new Intent(context, SettingsActivity.class);
        intent.putExtra(BUNDLE_KEY_START_PAGE, startPage);
        return intent;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_settings;
    }

    @Override
    protected void setupTitle(String tag) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (intent != null) {
            mStartPage = intent.getIntExtra(BUNDLE_KEY_START_PAGE, mStartPage);
        }

        if (mStartPage == SETTINGS_PIN) {
            show(SettingsPinCodeFragment.newInstance());
        } else if (mStartPage == SETTINGS_INFO) {
            showInfoMenu(false);
        } else if (mStartPage == SETTINGS_CHANGE_PASSWORD) {
            showInfoMenu(true);
        } else {
            show(new SettingsMenuFragment());
        }
        bindCaService();
    }

    @Override
    protected void onDestroy() {
        unbindCaService();
        super.onDestroy();
    }

    private void showInfoMenu(boolean forgetPassword) {
        show(SettingsUserInfoFragment.newInstance(forgetPassword));
    }
}
