package com.luxgen.remote.ui.arremote;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.luxgen.remote.R;
import com.luxgen.remote.ca.CommunicationAgentService;
import com.luxgen.remote.custom.LocationRequestFragment;
import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.network.model.maintenance.GetCarProgressInWorkshopResponseData;
import com.luxgen.remote.ui.dialog.AlertDialogFragment;
import com.luxgen.remote.ui.dialog.OptimizeDialogFragment;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.VigRequestData;
import com.luxgen.remote.network.model.assistant.GetOpenDataAirQualityRequestData;
import com.luxgen.remote.network.model.assistant.GetOpenDataAirQualityResponseData;
import com.luxgen.remote.network.model.assistant.GetPm25ResponseData;
import com.luxgen.remote.network.model.assistant.GetWeatherRequestData;
import com.luxgen.remote.network.model.assistant.GetWeatherResponseData;
import com.luxgen.remote.network.model.assistant.Pm25Data;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.util.GeocodeCountryIntentService;
import com.luxgen.remote.util.NetUtils;
import com.luxgen.remote.util.PM25Service;
import com.luxgen.remote.util.Prefs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static com.luxgen.remote.ca.CommunicationAgentService.REMOTE_COMMAND_DOOR_LOCK;
import static com.luxgen.remote.ca.CommunicationAgentService.REMOTE_COMMAND_DOOR_UNLOCK;
import static com.luxgen.remote.ca.CommunicationAgentService.REMOTE_COMMAND_FIND_CAR;
import static com.luxgen.remote.ca.CommunicationAgentService.REMOTE_COMMAND_ONE_CLICK_OPTIMIZE;
import static com.luxgen.remote.ca.CommunicationAgentService.REMOTE_COMMAND_OPEN_BACKDOOR;

public class RemoteControlAirDataFragment extends LocationRequestFragment {

    private final static int CHECK_PM25_INTERVAL_MS = 28000;
    public final static int MAX_TIME_FOR_PM25 = 1000 * 60 * 20; // 20 minutes

    private CountryResultReceiver mResultReceiver;
    private BarChart mChart;
    private TextView mDownRateTextView;
    private TextView mDownRateTitleTextView;
    private TextView mAirQualityInCarTextView;
    private TextView mWeatherTextView;
    private TextView mAirQualityTextView;
    private TextView mDefaultTemperatureTextView;
    private TextView mLatestPM25TimeTextView;
    private Button mOptimizeButton;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private int mLastDownRate = 0;
    private String executeActionFrom;
    private RelativeLayout mProgressLayout;

    public static RemoteControlAirDataFragment newInstance(boolean autoExec) {
        RemoteControlAirDataFragment f = new RemoteControlAirDataFragment();

        Bundle args = new Bundle();
        args.putBoolean("autoExec", autoExec);
        f.setArguments(args);

        return f;
    }

    protected CommunicationAgentService mCommunicationAgentService = null;

    private ServiceConnection caServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder serviceBinder) {
            mCommunicationAgentService = ((CommunicationAgentService.LocalBinder) serviceBinder).getService();

            Intent intent = mCommunicationAgentService.getLastResult();
            if (intent != null) {
                onReceiveBroadcastFromCaService(getContext(), intent);
            }
        }

        public void onServiceDisconnected(ComponentName name) {
            mCommunicationAgentService = null;
        }
    };

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            queryPM25();
        }
    };

    class CountryResultReceiver extends ResultReceiver {
        public CountryResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (resultData == null) {
                return;
            }

            String country = resultData.getString(GeocodeCountryIntentService.RESULT_DATA_KEY);
            if (!TextUtils.isEmpty(country)) {
                // dirty hack
                country = country.replace("台", "臺");
                queryWeatherData(country);
            }
        }
    }

    private OnApiListener<GetWeatherResponseData> mOnWeatherApiListener = new OnApiListener<GetWeatherResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetWeatherResponseData responseData) {
            if (responseData.isSuccessful()) {
                mWeatherTextView.setText(String.format(Locale.TAIWAN, "%s°", responseData.getTemperature()).replace(".0", ""));
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetOpenDataAirQualityResponseData> mOnAirQualityApiListener = new OnApiListener<GetOpenDataAirQualityResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetOpenDataAirQualityResponseData responseData) {
            if (responseData.isSuccessful()) {
                mAirQualityTextView.setText(String.valueOf(responseData.getPM25()));
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetPm25ResponseData> mOnPM25ApiListener = new OnApiListener<GetPm25ResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetPm25ResponseData responseData) {
            if (responseData.isSuccessful()) {
                ArrayList<BarEntry> pm25array = buildPM25BarEntryArray(responseData.getList());
                updateAllInfo(pm25array, CHECK_PM25_INTERVAL_MS);
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private OptimizeDialogFragment.Callbacks mOptimizeDialogCallback = new OptimizeDialogFragment.Callbacks() {
        @Override
        public void onPositiveButtonClicked(float temperature) {
            setCarTemperature(temperature);
        }

        @Override
        public void onNegativeButtonClicked() {
        }
    };

    private OptimizeDialogFragment.Callbacks mOneClickOptimizeDialogCallback = new OptimizeDialogFragment.Callbacks() {
        @Override
        public void onPositiveButtonClicked(float temperature) {
            setCarTemperature(temperature);
            doRealOptimization(temperature);
        }

        @Override
        public void onNegativeButtonClicked() {
        }
    };

    @Override
    protected void onLocationUpdated(Location location) {
//        https://maps.google.com/maps/api/geocode/json?latlng=lat,lng&language=zh-TW&sensor=true&key=AIzaSyBBT9CgVaUGn78lpyk7Jx650wADylj6ZkI
        translateCountry(location);
        queryOpenDataAirQuality(location.getLatitude(), location.getLongitude());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mResultReceiver = new CountryResultReceiver(mHandler);

        bindCaService();
    }

    @Override
    public void onDestroy() {
        unbindCaService();
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mCommunicationAgentService != null) {
            Intent intent = mCommunicationAgentService.getLastResult();
            if (intent != null) {
                onReceiveBroadcastFromCaService(getContext(), intent);
            }
        }
    }

    public void bindCaService() {
        Intent intent = new Intent(getContext(), CommunicationAgentService.class);
        getActivity().bindService(intent, caServiceConnection, Context.BIND_AUTO_CREATE);
    }

    public void unbindCaService() {
        getContext().unbindService(caServiceConnection);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        boolean autoExec = args.getBoolean("autoExec", false);

        View v = inflater.inflate(R.layout.fragment_remote_control_air, container, false);

        mProgressLayout = v.findViewById(R.id.progress_container);
        mProgressLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        mChart = v.findViewById(R.id.chart);
        initialChart();

        mDownRateTextView = v.findViewById(R.id.down_rate);
        mDownRateTitleTextView = v.findViewById(R.id.down_rate_title);
        mAirQualityInCarTextView = v.findViewById(R.id.txt_air_quality_in_car);
        mDefaultTemperatureTextView = v.findViewById(R.id.txt_default_temperature);
        mWeatherTextView = v.findViewById(R.id.txt_current_weather);
        mAirQualityTextView = v.findViewById(R.id.txt_air_quality);
        mLatestPM25TimeTextView = v.findViewById(R.id.latest_pm25_time);

        mOptimizeButton = v.findViewById(R.id.btn_optimize);
        mOptimizeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeActionFrom = "mOptimizeButton_click";
                checkCarProgress();
            }
        });

        updateDefaultTemperature();

        if (autoExec) {
            executeActionFrom = "autoExec_Optimization";
            checkCarProgress();
        }

        if (!NetUtils.isConnectToInternet(getContext())) {
            showServerErrorDialog("");
        }

        return v;
    }

    private void enableOptimizeButton(boolean enable) {
        mOptimizeButton.setEnabled(enable);
        if (enable) {
            mProgressLayout.setVisibility(View.GONE);
            mOptimizeButton.setBackgroundResource(R.drawable.btn_bg_rect_default);
        } else {
            mProgressLayout.setVisibility(View.VISIBLE);
            mOptimizeButton.setBackgroundResource(R.drawable.btn_bg_rect_disable);
        }
    }

    private void initialChart() {
        // set default text
        mChart.setNoDataText(getString(R.string.remote_control_air_data_nothing));
        mChart.setNoDataTextColor(ContextCompat.getColor(getContext(), R.color.textColorLight));

        // remove graphic effect
        mChart.setDrawBarShadow(false);
        mChart.setDrawGridBackground(false);
        mChart.getDescription().setEnabled(false);

        // remove all touch controls
        mChart.setPinchZoom(false);
        mChart.setDoubleTapToZoomEnabled(false);
        mChart.setTouchEnabled(false);
        mChart.setDragEnabled(false);
        mChart.setScaleEnabled(false);

        IAxisValueFormatter xAxisFormatter = new PM25AxisValueFormatter();
        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setTextColor(ContextCompat.getColor(getContext(), R.color.bgColorLight));
        xAxis.setAxisMinimum(-0.4f);
        xAxis.setAxisMaximum(5.4f);
        xAxis.setLabelCount(4, true);
        xAxis.setValueFormatter(xAxisFormatter);
        xAxis.setDrawLabels(true);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setSpaceTop(15f);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setDrawGridLines(false);
        leftAxis.setDrawAxisLine(false);
        leftAxis.setTextColor(ContextCompat.getColor(getContext(), R.color.bgColorLight));
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        // remove right x-axis
        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);

        // remove legend (at bottom)
        Legend l = mChart.getLegend();
        l.setEnabled(false);
    }

    private ArrayList<Pm25Data> mPm25DataArrayInternal = new ArrayList<>();

    private boolean syncInternalArray(String[] savedData, ArrayList<Pm25Data> pm25DataArrayList) {
        if (savedData == null) {
            if (pm25DataArrayList != null && mPm25DataArrayInternal.size() == 0) {
                int size = pm25DataArrayList.size();
                if (size > Prefs.MAX_NUMBER_SAMPLE_PM25) {
                    size = Prefs.MAX_NUMBER_SAMPLE_PM25;
                }
                for (int i = 0; i < size; i++) {
                    mPm25DataArrayInternal.add(pm25DataArrayList.get(i));
                }
                return true;
            }
        } else {
            if (mPm25DataArrayInternal.size() < Prefs.MAX_NUMBER_SAMPLE_PM25) {
                mPm25DataArrayInternal.clear();
                int size = savedData.length;
                for (int i = 0; i < size; i++) {
                    int index = size - i - 1;
                    mPm25DataArrayInternal.add(new Pm25Data(Double.parseDouble(savedData[index])));
                }
                return true;
            }
        }

        return false;
    }

    private ArrayList<BarEntry> buildPM25BarEntryArray(ArrayList<Pm25Data> pm25DataArrayList) {
        ArrayList<BarEntry> yVals1 = new ArrayList<>();

        String carId = getCarId();
        if (carId == null) {
            return yVals1;
        }

        String[] lastPM25DataArray = null;
        if (getContext() != null) {
            lastPM25DataArray = Prefs.getPM25Data(getContext(), carId);
        }
        if (!syncInternalArray(lastPM25DataArray, pm25DataArrayList)) {
            if (pm25DataArrayList != null && pm25DataArrayList.size() > 0) {
                Pm25Data lastPm25Data = mPm25DataArrayInternal.get(0);
                Pm25Data currPm25Data = pm25DataArrayList.get(0);
                if (currPm25Data.getTimeStamp() > lastPm25Data.getTimeStamp()) {
                    mPm25DataArrayInternal.add(0, currPm25Data);
                    if (mPm25DataArrayInternal.size() > Prefs.MAX_NUMBER_SAMPLE_PM25) {
                        mPm25DataArrayInternal.remove(Prefs.MAX_NUMBER_SAMPLE_PM25);
                    }
                }
            }
        }

        int size = mPm25DataArrayInternal.size();
        for (int i = 0; i < Prefs.MAX_NUMBER_SAMPLE_PM25; i++) {
            int barEntryIndex = Prefs.MAX_NUMBER_SAMPLE_PM25 - i - 1;
            float barEntryVal = 0;
            if (i < size) {
                Pm25Data pm25Data = mPm25DataArrayInternal.get(i);
                barEntryVal = ((Double) pm25Data.getRange()).floatValue();
            }
            yVals1.add(new BarEntry(barEntryIndex, barEntryVal));
        }

        return yVals1;
    }

    private int getBestPM25Val(ArrayList<BarEntry> yVals1) {
        int min = Integer.MAX_VALUE;
        for (BarEntry entry : yVals1) {
            if (entry.getY() < min && entry.getY() != 0) {
                min = (int) entry.getY();
            }
        }

        return min;
    }

    private int getLatestPM25Val(ArrayList<BarEntry> yVals1) {
        if (yVals1 != null && yVals1.size() > 0) {
            return (int) yVals1.get(0).getY();
        } else {
            return 0;
        }
    }

    private int getWorstPM25Val(ArrayList<BarEntry> yVals1) {
        int max = Integer.MIN_VALUE;
        for (BarEntry entry : yVals1) {
            if (entry.getY() > max) {
                max = (int) entry.getY();
            }
        }

        return max;
    }

    private void updateLatestPM25Time() {
        int size = mPm25DataArrayInternal.size();
        if (size > 0) {
            Pm25Data pm25Data = mPm25DataArrayInternal.get(0);
            String timeStamp = pm25Data.getTime();
            if (timeStamp.length() == 0) {
                if (getContext() != null) {
                    Date date = new Date(Prefs.getPM25Time(getContext(), getCarId()));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.TAIWAN);
                    timeStamp = sdf.format(date);
                }
            }
            mLatestPM25TimeTextView.setText(timeStamp);
        } else {
            mLatestPM25TimeTextView.setText("");
        }
    }

    private void updateDownRate(ArrayList<BarEntry> yVals1) {
        if (yVals1.size() == 0) {
            mDownRateTextView.setText(R.string.remote_control_air_data_no_value);
            mDownRateTitleTextView.setVisibility(View.VISIBLE);
            return;
        }

        int maxNumberSamplePM25 = mPm25DataArrayInternal.size();
        int first = 0, last = 0;
        if (maxNumberSamplePM25 != 0) {
            last = (int) (yVals1.get(0).getY());
            first = (int) (yVals1.get(maxNumberSamplePM25 - 1).getY());
        }
        if (first == 0 || last == 0) {
            mDownRateTextView.setText(R.string.remote_control_air_data_no_value);
        } else {
            int rate = (int) ((first - last) / (first * 1f) * 100);
            mDownRateTextView.setText(String.format(Locale.TAIWAN, "%d%%", rate));
            mLastDownRate = rate;
        }
        mDownRateTitleTextView.setVisibility(View.VISIBLE);
    }

    private void updateInCarAQI(int best) {
        if (best == Integer.MAX_VALUE || best == 0) {
            mAirQualityInCarTextView.setText(R.string.remote_control_air_data_no_value);
        } else {
            mAirQualityInCarTextView.setText(String.valueOf(best));
        }
    }

    private void updateYAxis(int worst) {
        float max = ((worst / 10) + 1) * 10;

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setAxisMaximum(max);
        leftAxis.setLabelCount((int) (max / 10));
    }

    private void updateChart(ArrayList<BarEntry> yVals1) {
        BarDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);

            set1.setDrawIcons(false);
            set1.setDrawValues(false);
            set1.setColor(ContextCompat.getColor(getContext(), R.color.bgColorTranslucent));
        } else {
            set1 = new BarDataSet(yVals1, "");

            set1.setDrawIcons(false);
            set1.setDrawValues(false);
            set1.setColor(ContextCompat.getColor(getContext(), R.color.bgColorTranslucent));

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setBarWidth(0.5f);

            mChart.setData(data);
        }

        mChart.getData().notifyDataChanged();
        mChart.notifyDataSetChanged();
        mChart.invalidate();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            updateDefaultTemperature();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        if (!Prefs.getPreferenceKeyValue(getContext(), Prefs.KEY_NO_LONGER_REMIND_BT_IN_REMOTE_CONTROL_AIR_DATA)) {
            showNoLongerRemindDialog(getString(R.string.dialog_message_bt_4g_enable_check),
                    Prefs.KEY_NO_LONGER_REMIND_BT_IN_REMOTE_CONTROL_AIR_DATA,
                    getString(R.string.btn_confirm),
                    null,
                    null);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.remote_control, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_setup:
                setupCarTemperature();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.action_refresh);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_setup);
        if (item != null) {
            item.setVisible(true);
        }
    }

    private void checkPM25Data() {
        if (getCarId() == null) {
            return;
        }

        long diff = System.currentTimeMillis() - Prefs.getOptimizeTime(getContext());
        int count = Prefs.getPM25DataCount(getContext(), getCarId());
        if (diff < MAX_TIME_FOR_PM25 || count != 0) {
            startPM25Service();

            ArrayList<BarEntry> pm25array = buildPM25BarEntryArray(null);
            if (count == Prefs.MAX_NUMBER_SAMPLE_PM25) {
                updateAllInfo(pm25array, CHECK_PM25_INTERVAL_MS);
            } else {
                updateAllInfo(pm25array, 0);
            }
        } else {
            queryPM25();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        checkPM25Data();
        registerPM25ServiceReceiver();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterPM25ServiceReceiver();
        mHandler.removeCallbacks(mRunnable);
    }

    private boolean isDoingOptimization() {
        long diff = System.currentTimeMillis() - Prefs.getOptimizeTime(getContext());
        return diff < MAX_TIME_FOR_PM25;
    }

    private void updateAllInfo(ArrayList<BarEntry> pm25array, long nextUpdateInterval) {
        if (!isAdded() || getContext() == null) {
            return;
        }
        int inCarVal = getLatestPM25Val(pm25array);
        int worst = getWorstPM25Val(pm25array);

        updateDownRate(pm25array);
        updateInCarAQI(inCarVal);
        updateYAxis(worst);
        updateChart(pm25array);
        updateLatestPM25Time();
        mHandler.removeCallbacks(mRunnable);
        if (nextUpdateInterval != 0) {
            mHandler.postDelayed(mRunnable, nextUpdateInterval);
        }
    }

    private void doRealOptimization(float temperature) {
        enableOptimizeButton(false);
        startNearCommand(REMOTE_COMMAND_ONE_CLICK_OPTIMIZE, String.valueOf(temperature));
    }

    private void doOptimization() {
        if (getCarTemperature() == -1) {
            OptimizeDialogFragment dialog = OptimizeDialogFragment.newInstance(
                    getString(R.string.remote_control_optimize_title),
                    getString(R.string.remote_control_optimize_message));
            dialog.setButtons(getString(R.string.btn_confirm), getString(R.string.btn_cancel), mOneClickOptimizeDialogCallback);
            dialog.setTemperature(OptimizeDialogFragment.DEFAULT_TEMPERATURE);
            dialog.show(getFragmentManager(), RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_OPTIMIZE);
        } else {
            doRealOptimization(getCarTemperature());
        }
    }

    private void startPM25Service() {
        String carId = getCarId();
        if (carId != null) {
            Intent intent = new Intent(getActivity(), PM25Service.class);
            intent.putExtra(PM25Service.PM25_INPUT_CARID, carId);
            getActivity().startService(intent);
        }
    }

    private String getErrorString(String command) {
        if (REMOTE_COMMAND_ONE_CLICK_OPTIMIZE.equals(command)) {
            return getString(R.string.remote_control_op_optimize_failed_message);
        } else if (REMOTE_COMMAND_OPEN_BACKDOOR.equals(command)) {
            return getString(R.string.remote_control_op_open_backdoor_failed_message);
        } else if (REMOTE_COMMAND_DOOR_LOCK.equals(command)) {
            return getString(R.string.remote_control_op_lock_car_failed_message);
        } else if (REMOTE_COMMAND_DOOR_UNLOCK.equals(command)) {
            return getString(R.string.remote_control_op_unlock_car_failed_message);
        } else if (REMOTE_COMMAND_FIND_CAR.equals(command)) {
            return getString(R.string.remote_control_op_find_car_failed_message);
        } else {
            return getString(R.string.remote_control_op_error_failed);
        }
    }

    @Override
    protected void addActionToReceiveBroadcastFromCaService(IntentFilter intentFilter) {
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_REMOTE_CONTROL_DONE);
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_REMOTE_CONTROL_INTERNAL_ERROR);
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_REMOTE_CONTROL_FAILED);
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_REMOTE_CONTROL_BLOCKED);
    }

    @Override
    protected void onReceiveBroadcastFromCaService(Context context, Intent intent) {
        String command = null;
        if (intent.hasExtra(CommunicationAgentService.CA_STATUS_EXTRA_COMMAND)) {
            command = intent.getStringExtra(CommunicationAgentService.CA_STATUS_EXTRA_COMMAND);
        }

        // 清除 result
        if (mCommunicationAgentService != null) {
            mCommunicationAgentService.getLastResult();
        }

        if (CommunicationAgentService.CA_STATUS_REMOTE_CONTROL_DONE.equals(intent.getAction())) {
            showConfirmDialog(getString(R.string.remote_control_op_success_title), getString(R.string.remote_control_op_success_message));
            if (REMOTE_COMMAND_ONE_CLICK_OPTIMIZE.equals(command)) {
                long diff = System.currentTimeMillis() - Prefs.getOptimizeTime(getContext());
                if (diff >= MAX_TIME_FOR_PM25) {
                    startPM25Service();
                    Prefs.clear(getContext(), Prefs.PREFS_PM25);
                    Prefs.setOptimizeTime(getContext(), System.currentTimeMillis());
                    mPm25DataArrayInternal.clear();
                    updateAllInfo(new ArrayList<BarEntry>(), 0);
                }
            }

            enableOptimizeButton(true);
            return;
        } else if (CommunicationAgentService.CA_STATUS_REMOTE_CONTROL_FAILED.equals(intent.getAction())) {
            if (intent.hasExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE)) {
                int errorString = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, -1);
                if (errorString != -1) {
                    if (errorString == R.string.remote_control_op_vig_failed_message) {
                        showConfirmDialog(getString(R.string.remote_control_op_vig_failed_title), getString(R.string.remote_control_op_vig_failed_message));
                    } else if (errorString == R.string.remote_control_op_bt_disconnect_during_rc_message) {
                        showConfirmDialog(getString(R.string.remote_control_op_bt_disconnect_during_rc_title), getString(R.string.remote_control_op_bt_disconnect_during_rc_message));
                    } else if (errorString == R.string.remote_control_op_error_offline) {
                        showConfirmDialog(getString(R.string.remote_control_op_error_offline_title), getString(R.string.remote_control_op_error_offline));
                    } else if (errorString == R.string.remote_control_op_error_rejected_optimize) {
                        showConfirmDialog(getString(R.string.remote_control_op_error_optimize_title), getString(R.string.remote_control_op_error_rejected_optimize));
                    } else {
                        showConfirmDialog(getString(R.string.remote_control_op_error_title), getString(errorString));
                    }
                } else {
                    showConfirmDialog(getString(R.string.remote_control_op_error_title), getErrorString(command));
                }
            } else {
                showConfirmDialog(getString(R.string.remote_control_network_error_title), getString(R.string.remote_control_network_error_message));
            }

            enableOptimizeButton(true);
            return;
        } else if (CommunicationAgentService.CA_STATUS_REMOTE_CONTROL_INTERNAL_ERROR.equals(intent.getAction())) {
            showConfirmDialog(getString(R.string.remote_control_op_error_title), getString(R.string.remote_control_internal_error));

            enableOptimizeButton(true);
            return;
        } else if (CommunicationAgentService.CA_STATUS_REMOTE_CONTROL_BLOCKED.equals(intent.getAction())) {
            showConfirmDialog(getString(R.string.remote_control_op_error_title), getString(R.string.remote_control_blocked_error));

            enableOptimizeButton(true);
            return;
        }

        super.onReceiveBroadcastFromCaService(context, intent);
    }

    private final BroadcastReceiver mPm25ServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if (PM25Service.PM25_ERROR_REASON_SERVER.equals(action)) {
                    showServerErrorDialog(intent.getStringExtra(PM25Service.PM25_ERROR_EXTRA_MESSAGE));
                } else if (PM25Service.PM25_ERROR_REASON_MULTI.equals(action)) {
                    showMultiLoginErrorDialog();
                } else if (PM25Service.PM25_UPDATE_DATA.equals(action)) {
                    if (getCarId() != null) {
                        ArrayList<BarEntry> pm25array = buildPM25BarEntryArray(null);
                        String[] lastPM25Data = Prefs.getPM25Data(context, getCarId());
                        if (lastPM25Data != null && lastPM25Data.length == Prefs.MAX_NUMBER_SAMPLE_PM25) {
                            updateAllInfo(pm25array, CHECK_PM25_INTERVAL_MS);
                        } else {
                            updateAllInfo(pm25array, 0);
                        }
                    }
                }
            }
        }
    };

    private void unregisterPM25ServiceReceiver() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mPm25ServiceReceiver);
    }

    private void registerPM25ServiceReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(PM25Service.PM25_ERROR_REASON_SERVER);
        filter.addAction(PM25Service.PM25_ERROR_REASON_MULTI);
        filter.addAction(PM25Service.PM25_UPDATE_DATA);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mPm25ServiceReceiver, filter);
    }

    private void startNearCommand(String command, String commandValue) {
        Intent intent = new Intent(getActivity(), CommunicationAgentService.class);
        intent.putExtra(CommunicationAgentService.VIG_BLE_COMMAND, command);
        intent.putExtra(CommunicationAgentService.VIG_BLE_COMMAND_VALUE, commandValue);
        getActivity().startService(intent);
    }

    private void updateDefaultTemperature() {
        float temp = getCarTemperature();
        if (temp == -1) {
            temp = OptimizeDialogFragment.DEFAULT_TEMPERATURE;
        }
        mDefaultTemperatureTextView.setText(String.format(Locale.TAIWAN, "%.1f°", temp).replace(".0", ""));
    }

    private void setupCarTemperature() {
        OptimizeDialogFragment dialog = OptimizeDialogFragment.newInstance(
                getString(R.string.remote_control_optimize_title),
                getString(R.string.remote_control_optimize_message));
        dialog.setButtons(getString(R.string.btn_confirm), getString(R.string.btn_cancel), mOptimizeDialogCallback);
        dialog.setTemperature(getCarTemperature());

        // same with what navigate() doing
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        this.setUserVisibleHint(false);
        dialog.show(transaction, RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_OPTIMIZE);
    }

    private float getCarTemperature() {
        return Prefs.getCarSetupTemperature(getContext());
    }

    private void setCarTemperature(float temperature) {
        Prefs.setCarSetupTemperature(getContext(), temperature);
    }

    private void translateCountry(Location loc) {
        Intent intent = new Intent(getActivity(), GeocodeCountryIntentService.class);
        intent.putExtra(GeocodeCountryIntentService.RECEIVER, mResultReceiver);
        intent.putExtra(GeocodeCountryIntentService.LOCATION_DATA_EXTRA, loc);
        getActivity().startService(intent);
    }

    private GetWeatherRequestData buildWeatherRequestData(String country) {
        GetWeatherRequestData data = new GetWeatherRequestData(country);

        return data;
    }

    private void queryWeatherData(String country) {
        if (!isAdded() || getContext() == null) {
            return;
        }

        if (!NetUtils.isConnectToInternet(getContext())) {
            return;
        }

        GetWeatherRequestData data = buildWeatherRequestData(country);

        AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(getContext());
        assistantWebApi.getWeather(data, mOnWeatherApiListener);
    }

    private GetOpenDataAirQualityRequestData buildAirQualityRequestData(double lat, double lng) {
        GetOpenDataAirQualityRequestData data = new GetOpenDataAirQualityRequestData(lat, lng);

        return data;
    }

    private void queryOpenDataAirQuality(double lat, double lng) {
        if (!NetUtils.isConnectToInternet(getContext())) {
            return;
        }

        GetOpenDataAirQualityRequestData data = buildAirQualityRequestData(lat, lng);

        AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(getContext());
        assistantWebApi.getOpenDataAirQuality(data, mOnAirQualityApiListener);
    }

    private String getCarId() {
        if (getContext() == null) {
            return null;
        }

        CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
        if (carKeyData == null) {
            return null;
        }

        return carKeyData.getCarId();
    }

    private void queryPM25() {
        if (!NetUtils.isConnectToInternet(getContext())) {
            return;
        }

        if (getContext() == null) {
            return;
        }

        UserInfo userInfo = Prefs.getUserInfo(getContext());
        String carId = getCarId();
        if (userInfo == null || carId == null) {
            return;
        }

        VigRequestData data = new VigRequestData(userInfo.getKeyUid(), userInfo.getToken(), carId);

        AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(getContext());
        assistantWebApi.getPm25(data, mOnPM25ApiListener);
    }

    private boolean isPinCodeVerified() {
        String mPinCode = Prefs.getPinCode(getContext());
        if (mPinCode == null || mPinCode.isEmpty()) {
            RemoteControlSetupPinFragment fragment = RemoteControlSetupPinFragment.newInstance(RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_AIR_DATA, RemoteControlFragment.REQUEST_OPTIMIZE);
            fragment.setTargetFragment(this, RemoteControlFragment.REQUEST_OPTIMIZE);
            navigate(RemoteControlAirDataFragment.this, fragment,
                    RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_PIN_SETUP, true);
            return false;
        } else {
            long now = getCurrentTime();
            long showTime = Prefs.getPinShowTime(getContext());
            long diff = RemoteControlFragment.PIN_TIMEOUT_MS - (now - showTime);
            if (diff > 0) {
                Prefs.setPinShowTime(getContext(), now);
                return true;
            } else {
                RemoteControlVerifyFragment fragment = RemoteControlVerifyFragment.newInstance();
                fragment.setTargetFragment(this, RemoteControlFragment.REQUEST_OPTIMIZE);
                ((RemoteControlActivity) getActivity()).navigate(this, fragment,
                        RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_PIN_VERIFY, true);
                return false;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == RemoteControlFragment.REQUEST_OPTIMIZE) {
            Prefs.setPinShowTime(getContext(), getCurrentTime());
            executeActionFrom = "activityResult_Optimization";
            checkCarProgress();
        }
    }

    private void doProgressAction(String mProgress) {
        if(mProgress.equalsIgnoreCase("0")) {
            LogCat.d("Workshop-executeActionFrom:"+executeActionFrom);
            switch (executeActionFrom) {
                case "mOptimizeButton_click":
                    LogCat.d("Workshop-mOptimizeButton_click:");
                    if (!Prefs.getPreferenceKeyValue(getContext(), Prefs.KEY_NO_LONGER_REMIND_AIR_POLLUTION)) {
                        showNoLongerRemindDialog(getString(R.string.remote_control_dialog_message_air_pollution),
                                Prefs.KEY_NO_LONGER_REMIND_AIR_POLLUTION,
                                getString(R.string.btn_confirm),
                                getString(R.string.btn_cancel),
                                new AlertDialogFragment.Callbacks() {

                                    @Override
                                    public void onPositiveButtonClicked() {
                                        if (isPinCodeVerified()) {
                                            doOptimization();
                                        }
                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }

                                    @Override
                                    public void onBackKeyPressed() {

                                    }
                                });
                    } else {
                        if (isPinCodeVerified()) {
                            doOptimization();
                        }
                    }
                    break;
                case "autoExec_Optimization":
                case "activityResult_Optimization":
                    LogCat.d("Workshop-activityResult_Optimization:");
                    doOptimization();
                    break;
            }
        } else if(mProgress.equalsIgnoreCase("1")) {
            showConfirmDialog(getString(R.string.car_progress_in_workshop_error_title), getString(R.string.car_progress_in_workshop_lock_body));
        }
    }

    private OnApiListener<GetCarProgressInWorkshopResponseData> mGetCarProgressInWorkshopApiListener = new OnApiListener<GetCarProgressInWorkshopResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetCarProgressInWorkshopResponseData responseData) {
            if (responseData.isSuccessful()) {
                if(responseData.getWsResult() != null) {
                    LogCat.d("Workshop-Progress1:"+responseData.getWsResult().getmProgress());
                    Prefs.setRepairProgress(getContext(), responseData.getWsResult().getmProgress());
                    doProgressAction(responseData.getWsResult().getmProgress());
                }
            } else {
                if(responseData.getErrorCode().equalsIgnoreCase("-9999")) {
                    showConfirmDialog(getString(R.string.car_progress_in_workshop_error_title), getString(R.string.car_progress_in_workshop_error_body));
                } else if(responseData.getErrorCode().equalsIgnoreCase("-1017") || responseData.getErrorCode().equalsIgnoreCase("-889")) {
                    LogCat.d("Workshop-getErrorMessage:"+responseData.getErrorMessage());
                    Prefs.setRepairProgress(getContext(), "0");
                    doProgressAction("0");
                } else {
                    showServerErrorDialog(responseData.getErrorMessage());
                }
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            LogCat.e("Workshop-failMessage:"+failMessage);
            String mProgress = Prefs.getRepairProgress(getContext());
            doProgressAction(mProgress);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {

        }
    };

    private void checkCarProgress() {
        ((RemoteControlActivity) getActivity()).callAPIToGetCarProgressInWorkshop(mGetCarProgressInWorkshopApiListener);
    }
}
