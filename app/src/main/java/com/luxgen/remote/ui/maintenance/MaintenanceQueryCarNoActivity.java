package com.luxgen.remote.ui.maintenance;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomFragmentsAppCompatActivity;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.maintenance.ValidateVehicleRequestData;
import com.luxgen.remote.util.NetUtils;
import com.luxgen.remote.util.Prefs;

public class MaintenanceQueryCarNoActivity extends CustomFragmentsAppCompatActivity {

    private EditText mCarNoEditText;
    private int mStartPage = 0;

    private OnApiListener<ResponseData> mOnApiListener = new OnApiListener<ResponseData>() {
        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            dismissProgressDialog();
            if (responseData.isSuccessful()) {
                startActivity(MaintenanceActivity.newIntent(MaintenanceQueryCarNoActivity.this, mStartPage, mCarNoEditText.getEditableText().toString().toUpperCase()));
                finish();
            } else {
                showServerErrorDialog(responseData.getErrorCode(), responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            if (!NetUtils.isConnectToInternet(MaintenanceQueryCarNoActivity.this)) {
                showServerErrorDialog();
            }
        }

        @Override
        public void onPreApiTask() {
            showProgressDialog(getString(R.string.maintenance_fetching_data));
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            dismissProgressDialog();
            showMultiLoginErrorDialog();
        }
    };

    @Override
    protected int getLayoutId() {
        return R.layout.activity_maintenance_query_carno;
    }

    @Override
    protected void setupTitle(String tag) {

    }

    public static Intent newIntent(Context context, int start) {
        Intent intent = new Intent(context, MaintenanceQueryCarNoActivity.class);
        intent.putExtra("start", start);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (intent != null) {
            mStartPage = intent.getIntExtra("start", mStartPage);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.activity_maintenance_title);
        } else {
            setTitle(R.string.activity_maintenance_title);
        }

        setupView();
        bindCaService();
    }

    @Override
    protected void onDestroy() {
        unbindCaService();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupView() {
        RadioButton selectCurrentCarRadioButton = findViewById(R.id.radio_button_current_car);
        RadioButton selectSpecificCarRadioButton = findViewById(R.id.radio_button_specific_car);
        mCarNoEditText = findViewById(R.id.edit_carno);
        mCarNoEditText.setText(getCarNo());
        Button nextButton = findViewById(R.id.btn_next);

        // only dms has car
        if (isDMS() && !TextUtils.isEmpty(getCarNo())) {
            selectCurrentCarRadioButton.setEnabled(true);
            selectCurrentCarRadioButton.setChecked(true);
        } else {
            selectCurrentCarRadioButton.setEnabled(false);
            selectSpecificCarRadioButton.setChecked(true);
        }

        selectCurrentCarRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCarNoEditText.setText(getCarNo());
                }
            }
        });

        selectSpecificCarRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCarNoEditText.setText("");
                }
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAllFieldValid()) {
                    queryCarNo();
                }
            }
        });
    }

    private String getCarNo() {
        CarKeyData carKeyData = Prefs.getCarKeyData(this);
        if (carKeyData == null) {
            return null;
        }

        return carKeyData.getCarNo();
    }

    private boolean isDMS() {
        UserInfo userInfo = Prefs.getUserInfo(this);
        if (userInfo != null) {
            return userInfo.isDMS();
        }

        return false;
    }

    private boolean isAllFieldValid() {
        String carNo = mCarNoEditText.getEditableText().toString();
        if (TextUtils.isEmpty(carNo)) {
            return false;
        }

        return true;
    }

    private void queryCarNo() {
        UserInfo userInfo = Prefs.getUserInfo(this);
        if (userInfo == null) {
            return;
        }

        String iKeyUid = userInfo.getKeyUid();
        String iKeyToken = userInfo.getToken();
        String carNo = mCarNoEditText.getEditableText().toString();

        ValidateVehicleRequestData data = new ValidateVehicleRequestData(iKeyUid, iKeyToken, carNo);

        AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(this);
        assistantWebApi.validateVehicle(data, mOnApiListener);
    }
}
