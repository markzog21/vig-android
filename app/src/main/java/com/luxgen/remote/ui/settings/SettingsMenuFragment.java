package com.luxgen.remote.ui.settings;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.util.NetUtils;

import java.util.Objects;

public class SettingsMenuFragment extends CustomBaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(R.string.settings_menu_title);

        View v = inflater.inflate(R.layout.fragment_settings_menu, container, false);

        setSubSettings(v, R.id.settings_userinfo, SettingsUserInfoFragment.newInstance(false));
        setSubSettings(v, R.id.settings_pincode, SettingsPinCodeFragment.newInstance());
        setSubSettings(v, R.id.settings_license, SettingsSubMenuFragment.newInstance(R.string.settings_license_title, R.layout.fragment_settings_license));
        setSubSettings(v, R.id.settings_contact, SettingsSubMenuFragment.newInstance(R.string.settings_contact_title, R.layout.fragment_settings_contact));
        setSubSettings(v, R.id.settings_voice, SettingsSubMenuFragment.newInstance(R.string.settings_voice_title, R.layout.fragment_settings_voice));



        View linkPlusView = v.findViewById(R.id.settings_link_plus);
        linkPlusView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://reurl.cc/e9LAax"));
                startActivity(browserIntent);
            }
        });

        TextView versionTextView = v.findViewById(R.id.settings_version_number);
        versionTextView.setText("v" + getAppVersion());

        return v;
    }

    private void setSubSettings(View parent, @IdRes int settingId, final Fragment fragment) {
        View v = parent.findViewById(settingId);
        if (v != null) {
            switch(settingId) {
                case R.id.settings_userinfo: {
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (NetUtils.isConnectToInternet(Objects.requireNonNull(getContext()))) {
                                show(fragment);
                            } else {
                                showServerErrorDialog();
                            }
                        }
                    });
                    break;
                }

                default: {
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            show(fragment);
                        }
                    });
                }
            }
        }
    }

    private String getAppVersion() {
        try {
            PackageInfo packageInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
}
