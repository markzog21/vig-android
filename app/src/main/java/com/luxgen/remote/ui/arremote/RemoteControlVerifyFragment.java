package com.luxgen.remote.ui.arremote;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.util.Prefs;

public class RemoteControlVerifyFragment extends CustomBaseFragment {

    private EditText mPinEditText;

    private void verifyPinCode() {
        boolean valid = isValidPinCode(mPinEditText.getText().toString().trim());
        if (valid) {
            Prefs.resetPinCodeErrorCount(getContext());
            exit();
        } else {
            int count = Prefs.incPinCodeErrorCount(getContext());
            if (count <= 3) {
                showPinCodeErrorDialog();
            } else {
                showResetPinDialog();
            }
            mPinEditText.setText("");
        }
    }

    private EditText.OnEditorActionListener mOnEditorActionListener = new EditText.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                hideSoftKeyboard(mPinEditText);
                verifyPinCode();
                return true;
            }
            return false;
        }
    };

    public static RemoteControlVerifyFragment newInstance() {
        RemoteControlVerifyFragment f = new RemoteControlVerifyFragment();

        return f;
    }

    public static RemoteControlVerifyFragment newInstance(int request) {
        RemoteControlVerifyFragment f = new RemoteControlVerifyFragment();

        Bundle args = new Bundle();
        args.putInt("request", request);
        f.setArguments(args);

        return f;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_remote_control_verify_pin, container, false);

        mPinEditText = v.findViewById(R.id.pincode);
        mPinEditText.setOnEditorActionListener(mOnEditorActionListener);

        Button finishButton = v.findViewById(R.id.btn_finish);
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyPinCode();
            }
        });

        return v;
    }

    private void exit() {
        if (getTargetFragment() != null) {
            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
        } else {
            final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
            int request = args.getInt("request", RemoteControlFragment.REQUEST_NONE);

            Prefs.setPinShowTime(getContext(), getCurrentTime());
            if (request == RemoteControlFragment.REQUEST_OPTIMIZE) {
                show(RemoteControlAirDataFragment.newInstance(true), RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_AIR_DATA, false);
            } else {
                show(RemoteControlFragment.newInstance(request), RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_MENU, false);
            }
        }

        popFragment();
    }

    private String getPinCode() {
        return Prefs.getPinCode(getContext());
    }

    private boolean isValidPinCode(String input) {
        return input.equals(getPinCode());
    }

    private void showPinCodeErrorDialog() {
        showConfirmDialog(getString(R.string.remote_control_pincode_error_title),
                getString(R.string.remote_control_pincode_error_message));
    }

    private void showResetPinDialog() {
        showConfirmDialog(getString(R.string.remote_control_pincode_reset_title),
                getString(R.string.remote_control_pincode_reset_message));
    }
}

