package com.luxgen.remote.ui.push;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luxgen.remote.R;
import com.luxgen.remote.model.PushData;

import java.util.ArrayList;
import java.util.List;

public class PushListAdapter extends RecyclerView.Adapter {
    private ArrayList<PushData> items;
    private LayoutInflater inflater;

    public PushListAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
        items = new ArrayList<>();
    }

    public ArrayList<PushData> getItems() {
        if (items == null) {
            items = new ArrayList<>();
        }
        return items;
    }

    public void setItems(List<PushData> list) {
        items.clear();

        items.addAll(list);
        notifyDataSetChanged();
    }

    public void clearSelect() {
        for (PushData item : items) {
            item.setSelect(false);
        }
    }

    public void selectAll() {
        for (PushData item : items) {
            item.setSelect(true);
        }
    }

    public boolean hasSelectItem() {
        for (PushData item : items) {
            if (item.isSelected()) {
                return true;
            }
        }

        return false;
    }

    public ArrayList<PushData> deleteSelectItems() {
        ArrayList<PushData> selectedItems = new ArrayList<>();
        for (final PushData item : items) {
            if (item.isSelected()) {
                item.changeToDeleteStatus();
                item.setNeedSyncToServer(true);
                selectedItems.add(item);
            }
        }

        items.removeAll(selectedItems);
        notifyDataSetChanged();
        return selectedItems;
    }

    public ArrayList<PushData> deleteSelectItems(PushData data) {
        ArrayList<PushData> selectedItems = new ArrayList<>();
        for (final PushData item : items) {
            if (item.getUuid().equals(data.getUuid())) {
                item.changeToDeleteStatus();
                item.setNeedSyncToServer(true);
                selectedItems.add(item);
                break;
            }
        }

        items.removeAll(selectedItems);
        notifyDataSetChanged();
        return selectedItems;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_push_list, parent, false);
        return new PushListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        boolean enableDivider = position < (getItemCount() - 1);
        ((PushListViewHolder) holder).setItem(items.get(position), enableDivider);
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }
}
