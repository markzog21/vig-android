package com.luxgen.remote.ui.maintenance;

import android.content.Intent;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.ui.maintenance.MaintenanceHistoryDetailActivity;
import com.luxgen.remote.network.model.maintenance.RepairHistory;

public class RepairHistoryViewHolder extends ViewHolder {

    private TextView mPlantMaintenanceTextView;
    private TextView mDateTextView;

    private RepairHistory item;

    public RepairHistoryViewHolder(View itemView) {
        super(itemView);

        mPlantMaintenanceTextView = itemView.findViewById(R.id.plant_maintenance);
        mDateTextView = itemView.findViewById(R.id.date);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = MaintenanceHistoryDetailActivity.newIntent(v.getContext(), item);
                v.getContext().startActivity(intent);
            }
        });
    }

    public void setItem(RepairHistory item) {
        this.item = item;
        mPlantMaintenanceTextView.setText(item.getDept());
        mDateTextView.setText(item.getDate());
    }
}
