package com.luxgen.remote.ui.oks;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.location.LocationListener;
import com.luxgen.remote.R;
import com.luxgen.remote.custom.CommonDefs;
import com.luxgen.remote.custom.CustomAppCompatActivity;
import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.db.VigDatabase;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.SoapWebApi;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.assistant.SetUseLogRequestData;
import com.luxgen.remote.network.model.assistant.UseLogData;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.soap.RequestEnvelope;
import com.luxgen.remote.network.model.soap.RescueRequestBody;
import com.luxgen.remote.network.model.soap.RescueRequestData;
import com.luxgen.remote.network.model.soap.ResponseEnvelope;
import com.luxgen.remote.ui.viewmodel.UseLogRepository;
import com.luxgen.remote.util.DialUtils;
import com.luxgen.remote.util.GeocodeAddressIntentService;
import com.luxgen.remote.util.GpsUtils;
import com.luxgen.remote.util.LocationRequestModule;
import com.luxgen.remote.util.Prefs;
import com.luxgen.remote.util.TimeUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class OneKeyServiceActivity extends CustomAppCompatActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, OneKeyServiceActivity.class);
    }

    private ImageView mCallServiceImageView = null;
    private ImageView mRoadRescueImageView = null;
    private ImageView mCallSalesImageView = null;
    private ImageView mCallSecretaryImageView = null;
    private ImageView mDesignatedDriverImageView = null;
    private ImageView mCall110ImageView = null;
    private ImageView mCallReservationImageView = null;
    private ImageView mTheftImageView = null;
    private ImageView mAccidentImageView = null;

    private GetUserInfoAsyncTask mGetUserInfoAsyncTask = null;

    private UserInfo mUserInfo = null;
    private UseLogData mUseLogData = null;

    private boolean mIsGpsStatusReceiverRegistered = false;
    private Location mLocation = null;
    private CountryResultReceiver mCountryResultReceiver = null;
    private LocationRequestModule mLocationRequestModule = null;
    private String mCurrentArea = "unknown";
    private String mCurrentRegion = "unknown";
    private String mCurrentPostalCode = "unknown";
    private String mCurrentAddress = "unknown";

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            translateCountry(location);
            mLocation = location;
        }
    };

    private class CountryResultReceiver extends ResultReceiver {
        public CountryResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (resultData == null) {
                return;
            }

            mCurrentArea = resultData.getString("area", "unknown");
            mCurrentRegion = resultData.getString("region", "unknown");
            mCurrentPostalCode = resultData.getString("code", "unknown");
            mCurrentAddress = resultData.getString("address", "unknown");
//
//            if (mLocation != null) {
//                LogCat.d("loc:" + mLocation.getLongitude() + ":" + mLocation.getLatitude());
//            }
//            LogCat.d("mCurrentArea:" + mCurrentArea);
//            LogCat.d("mCurrentRegion:" + mCurrentRegion);
//            LogCat.d("mCurrentPostalCode:" + mCurrentPostalCode);
//            LogCat.d("mCurrentAddress:" + mCurrentAddress);
        }
    }

    private final BroadcastReceiver mGpsStatusBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (GpsUtils.isGpsOpened(OneKeyServiceActivity.this)) {
                unregisterReceiver(mGpsStatusBroadcastReceiver);
                mIsGpsStatusReceiverRegistered = false;
                if (mLocationRequestModule != null) {
                    mLocationRequestModule.startLocationUpdates();
                }
            }
        }
    };

    private OnClickListener mOnCallServiceImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = DialUtils.callService(getApplicationContext());
            sendUseLog(intent.getStringExtra(DialUtils.PHONE), getString(R.string.one_key_service_call_service_label));
            startActivity(intent);
        }
    };

    private OnClickListener mOnRoadRescueImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = DialUtils.callRoadRescue(getApplicationContext());
            sendUseLog(intent.getStringExtra(DialUtils.PHONE), getString(R.string.one_key_service_road_rescue_label));
            startActivity(intent);
            uploadRescueData();
        }
    };

    private OnClickListener mOnCallSalesImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = DialUtils.callSales(getApplicationContext());
            sendUseLog(intent.getStringExtra(DialUtils.PHONE), getString(R.string.one_key_service_call_salse_label));
            startActivity(intent);
        }
    };

    private OnClickListener mOnCallSecretaryImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = DialUtils.callSecretary(getApplicationContext());
            sendUseLog(intent.getStringExtra(DialUtils.PHONE), getString(R.string.one_key_service_call_secretary_label));
            startActivity(intent);
        }
    };

    private OnClickListener mOnDesignatedDriverImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = DialUtils.callTaxi(getApplicationContext());
            sendUseLog(intent.getStringExtra(DialUtils.PHONE), getString(R.string.one_key_service_designated_driver_label));
            startActivity(intent);
        }
    };

    private OnClickListener mOnCall110ImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = DialUtils.videoPolice(getApplicationContext());
            sendUseLog(intent.getStringExtra(DialUtils.PHONE), getString(R.string.one_key_service_call_110_label));
            startActivity(intent);
        }
    };

    private OnClickListener mOnCallReservationImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = DialUtils.callReservation(getApplicationContext());
            sendUseLog(intent.getStringExtra(DialUtils.PHONE), getString(R.string.one_key_service_call_reservation_label));
            startActivity(intent);
        }
    };

    private OnClickListener mOnTheftImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = CarStolenActivity.newIntent(OneKeyServiceActivity.this);
            startActivity(intent);
        }
    };

    private OnClickListener mOnAccidentImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = CarAccidentActivity.newIntent(OneKeyServiceActivity.this);
            startActivity(intent);
        }
    };

    private class GetUserInfoAsyncTask extends AsyncTask<Void, Void, UserInfo> {

        @Override
        protected UserInfo doInBackground(Void... voids) {
            return Prefs.getUserInfo(OneKeyServiceActivity.this);
        }

        @Override
        protected void onPostExecute(UserInfo userInfo) {
            dismissProgressDialog();

            mGetUserInfoAsyncTask = null;
            mUserInfo = userInfo;
        }
    }

    private OnApiListener<ResponseData> mSetUseLogApiListener = new OnApiListener<ResponseData>() {
        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            mUseLogData = null;
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            if (mUseLogData != null) {
                VigDatabase db = VigDatabase.getInstance(OneKeyServiceActivity.this);
                AssistantWebApi api = AssistantWebApi.getInstance(OneKeyServiceActivity.this);
                UseLogRepository repo = new UseLogRepository(mUserInfo, db, api);
                repo.setUseLog(mUserInfo, mUseLogData);
            }
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_key_service);

        LinearLayout mainContainer = findViewById(R.id.activity_container);
        setWheelScreenLayout(mainContainer);

        ImageView wheelRightHandle = findViewById(R.id.wheel_right_handle_image_view);
        ImageView wheelLeftHandle = findViewById(R.id.wheel_left_handle_image_view);
        setWheelHandles(wheelRightHandle, wheelLeftHandle);

        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            toolBar.setTitle(R.string.activity_one_key_service_title);
            setSupportActionBar(toolBar);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mCallServiceImageView = findViewById(R.id.activity_one_key_service_call_service_image_view);
        mCallServiceImageView.setOnClickListener(mOnCallServiceImageViewClickListener);
        mRoadRescueImageView = findViewById(R.id.activity_one_key_service_road_rescue_image_view);
        mRoadRescueImageView.setOnClickListener(mOnRoadRescueImageViewClickListener);
        mCallSalesImageView = findViewById(R.id.activity_one_key_service_call_sales_image_view);
        mCallSalesImageView.setOnClickListener(mOnCallSalesImageViewClickListener);
        mCallSecretaryImageView = findViewById(R.id.activity_one_key_service_call_secretary_image_view);
        mCallSecretaryImageView.setOnClickListener(mOnCallSecretaryImageViewClickListener);
        mDesignatedDriverImageView = findViewById(R.id.activity_one_key_service_designated_driver_image_view);
        mDesignatedDriverImageView.setOnClickListener(mOnDesignatedDriverImageViewClickListener);
        mCall110ImageView = findViewById(R.id.activity_one_key_service_call_110_image_view);
        mCall110ImageView.setOnClickListener(mOnCall110ImageViewClickListener);
        mCallReservationImageView = findViewById(R.id.activity_one_key_service_call_reservation_image_view);
        mCallReservationImageView.setOnClickListener(mOnCallReservationImageViewClickListener);
        mTheftImageView = findViewById(R.id.activity_one_key_service_theft_image_view);
        mTheftImageView.setOnClickListener(mOnTheftImageViewClickListener);
        mAccidentImageView = findViewById(R.id.activity_one_key_service_accident_image_view);
        mAccidentImageView.setOnClickListener(mOnAccidentImageViewClickListener);

        mCountryResultReceiver = new CountryResultReceiver(new Handler());
        mLocationRequestModule = new LocationRequestModule(this);
        mLocationRequestModule.init();
        mLocationRequestModule.setLocationChangedListener(mLocationListener);

        bindCaService();
    }

    @Override
    protected void onCaServiceConnected() {
        init();
        super.onCaServiceConnected();
    }

    @Override
    public void onStart() {
        ArrayList<String> permissionArrayList = new ArrayList<>();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionArrayList.add(Manifest.permission.ACCESS_FINE_LOCATION);
            permissionArrayList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        } else {
            if (GpsUtils.isGpsOpened(this)) {
                if (mLocationRequestModule != null) {
                    mLocationRequestModule.startLocationUpdates();
                }
            } else {
                if (!mIsGpsStatusReceiverRegistered) {
                    registerReceiver(mGpsStatusBroadcastReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
                    mIsGpsStatusReceiverRegistered = true;
                }
            }
        }

        if (permissionArrayList.size() > 0) {
            String[] permisssionStringArray = permissionArrayList.toArray(new String[permissionArrayList.size()]);
            ActivityCompat.requestPermissions(OneKeyServiceActivity.this,
                    permisssionStringArray,
                    CommonDefs.REQUEST_CODE_REQUEST_PERMISSIONS);
        }

        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mLocationRequestModule != null) {
            mLocationRequestModule.stopLocationUpdates();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mLocationRequestModule != null) {
            mLocationRequestModule.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CommonDefs.REQUEST_CODE_REQUEST_PERMISSIONS && grantResults.length > 0) {
            boolean getPermissionFineLocation = false;
            boolean getPermissionCoarseLocation = false;
            int permissionCount = permissions.length;

            for (int index = 0; index < permissionCount; index++) {
                if (permissions[index].equals(Manifest.permission.ACCESS_FINE_LOCATION) && grantResults[index] == PackageManager.PERMISSION_GRANTED) {
                    getPermissionFineLocation = true;
                    continue;
                }

                if (permissions[index].equals(Manifest.permission.ACCESS_COARSE_LOCATION) && grantResults[index] == PackageManager.PERMISSION_GRANTED) {
                    getPermissionCoarseLocation = true;
                }
            }

            if (getPermissionFineLocation && getPermissionCoarseLocation) {
                if (mLocationRequestModule != null) {
                    mLocationRequestModule.startLocationUpdates();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (mGetUserInfoAsyncTask != null) {
            mGetUserInfoAsyncTask.cancel(true);
        }

        if (mIsGpsStatusReceiverRegistered) {
            unregisterReceiver(mGpsStatusBroadcastReceiver);
            mIsGpsStatusReceiverRegistered = false;
        }

        unbindCaService();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void init() {
        showProgressDialog();
        if (mGetUserInfoAsyncTask == null) {
            mGetUserInfoAsyncTask = new GetUserInfoAsyncTask();
            mGetUserInfoAsyncTask.execute();
        }
    }

    private void sendUseLog(String phone, String label) {

        mUseLogData = new UseLogData(mUserInfo.getPhone(), label, getCurrentTimeString());
        SetUseLogRequestData request = new SetUseLogRequestData(mUserInfo.getToken(), mUserInfo.getKeyUid());
        request.setLogData(mUseLogData);

        LogCat.d("send UseLog -> phone:" + mUserInfo.getPhone() + "; label: " + label);
        AssistantWebApi.getInstance(getApplicationContext())
                .setUseLog(request, mSetUseLogApiListener);
    }

    private String getCurrentTimeString() {
        SimpleDateFormat mDateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.TAIWAN);
        Calendar calendar = Calendar.getInstance();
        mDateFormatter.setTimeZone(calendar.getTimeZone());
        return mDateFormatter.format(calendar.getTime());
    }

    private void translateCountry(Location location) {
        Intent intent = new Intent(this, GeocodeAddressIntentService.class);
        intent.putExtra(GeocodeAddressIntentService.RECEIVER, mCountryResultReceiver);
        intent.putExtra(GeocodeAddressIntentService.LOCATION_DATA_EXTRA, location);
        intent.putExtra(GeocodeAddressIntentService.DETAILED_DATA_EXTRA, true);
        startService(intent);
    }

    private void uploadRescueData() {
        CarKeyData carKeyData = Prefs.getCarKeyData(this);
        UserInfo userInfo = Prefs.getUserInfo(this);
        if (carKeyData == null || userInfo == null) {
            return;
        }

        String userName = Prefs.getUserName(this);
        String userPhone = userInfo.getPhone();
        String carNo = carKeyData.getCarNo();

        SoapWebApi soapWebApi = SoapWebApi.getInstance(this);
        RescueRequestData requestData = new RescueRequestData();

        requestData.setName(userName);
        requestData.setPlateNo(carNo);
        requestData.setMobile(userPhone);
        requestData.setType(1); //TODO: how to know ?
        requestData.setArea(mCurrentArea);
        requestData.setRegion(mCurrentRegion);
        requestData.setCode(mCurrentPostalCode);
        requestData.setAddress(mCurrentAddress);
        if (mLocation != null) {
            requestData.setLongitude(mLocation.getLongitude());
            requestData.setLatitude(mLocation.getLatitude());
        }
        try {
            requestData.setDate(TimeUtils.parseToServerFormateString(System.currentTimeMillis()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        RescueRequestBody rescueRequestBody = new RescueRequestBody();
        rescueRequestBody.setRescueRequestData(requestData);
        RequestEnvelope requestEnvelope = new RequestEnvelope();
        requestEnvelope.setBody(rescueRequestBody);

        soapWebApi.uploadRescueData(requestEnvelope, new OnApiListener<ResponseEnvelope>() {
            @Override
            public void onApiTaskSuccessful(ResponseEnvelope responseData) {
                if (responseData.isSuccess()) {
                    LogCat.d("成功上傳救援資訊");
                }
            }

            @Override
            public void onApiTaskFailure(String failMessage) {

            }

            @Override
            public void onPreApiTask() {

            }

            @Override
            public void onApiProgress(long value) {

            }

            @Override
            public void onAuthFailure() {

            }
        });
    }
}

