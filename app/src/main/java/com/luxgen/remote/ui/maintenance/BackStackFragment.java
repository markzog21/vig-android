package com.luxgen.remote.ui.maintenance;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.luxgen.remote.custom.CustomBaseFragment;

public abstract class BackStackFragment extends CustomBaseFragment {

    public static boolean handleBackPressed(FragmentManager fm) {
        if (fm.getFragments() != null) {
            for (Fragment frag : fm.getFragments()) {
                if (frag != null && frag.isVisible() && frag instanceof BackStackFragment) {
                    if (((BackStackFragment) frag).onBackPressed()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    protected boolean onBackPressed() {
        FragmentManager fm = getChildFragmentManager();
        if (handleBackPressed(fm)) {
            return true;
        } else if (hasFragments(fm)) {
            fm.popBackStack();
            return true;
        }
        return false;
    }

    public boolean hasFragments() {
        FragmentManager fm = getChildFragmentManager();
        return hasFragments(fm);
    }

    private boolean hasFragments(FragmentManager fm) {
        return (getUserVisibleHint() && fm.getBackStackEntryCount() > 0);
    }
}
