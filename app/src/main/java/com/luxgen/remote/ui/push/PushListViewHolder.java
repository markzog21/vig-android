package com.luxgen.remote.ui.push;

import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.model.PushData;

public class PushListViewHolder extends RecyclerView.ViewHolder {

    private PushListListener mListener;

    private TextView mTitleTextView;
    private TextView mDateTextView;
    private View mDividerView;
    private AppCompatCheckBox mCheckBox;
    private ImageView mUnReadImageView;

    public PushListViewHolder(View itemView) {
        super(itemView);

        mListener = (PushListListener) itemView.getContext();

        mTitleTextView = itemView.findViewById(R.id.push_list_item_title_text);
        mDateTextView = itemView.findViewById(R.id.push_list_item_date_text);
        mDividerView = itemView.findViewById(R.id.push_list_item_divider);
        mCheckBox = itemView.findViewById(R.id.push_list_item_checkbok);
        mUnReadImageView = itemView.findViewById(R.id.push_list_item_unread);
    }

    public void setItem(PushData data, boolean enableDivider) {
        mTitleTextView.setText(data.getTitle());
        mDateTextView.setText(data.getDate());
        mDividerView.setVisibility(enableDivider ? View.VISIBLE : View.GONE);

        mCheckBox.setVisibility(mListener.isEditable() ? View.VISIBLE : View.GONE);
        mCheckBox.setChecked(data.isSelected());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener.isEditable()) {
                    onItemSelectChange(data);
                    return;
                }

                data.changeReadStatus();
                mListener.click(data);
            }
        });

        mUnReadImageView.setVisibility(data.isRead() ? View.INVISIBLE : View.VISIBLE);
    }

    private void onItemSelectChange(PushData data) {
        final boolean isChecked = !mCheckBox.isChecked();
        mCheckBox.setChecked(isChecked);
        data.setSelect(isChecked);
        mListener.select(isChecked, data);
    }

    public interface PushListListener {
        boolean isEditable();

        void select(boolean isChecked, PushData data);

        void click(PushData data);
    }
}
