package com.luxgen.remote.ui.maintenance;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.ui.dialog.AlertDialogFragment;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.ui.dialog.ProgressDialogFragment;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.maintenance.SetCarRepairPeriodRequestData;
import com.luxgen.remote.network.model.maintenance.SetCarRepairPeriodResponseData;
import com.luxgen.remote.util.Prefs;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class MaintenanceScheduleFragment3 extends CustomBaseFragment {

    private String mPlate;
    private String mMileage;
    private String mDealerCode;
    private String mDeptCode;
    private Boolean mSchedule, mRepair;
    private String mNotes;
    private long mBookingDate;
    private String mPlant;

    private SimpleDateFormat mDateFormatter = new SimpleDateFormat("MM月 dd日 EEE a hh:mm", Locale.TAIWAN);
    private SimpleDateFormat mDateFormatterForServer = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.TAIWAN);
    Calendar mChosenDate = Calendar.getInstance();

    private ProgressDialogFragment mProgressDialogFragment;

    private void dismissDialog() {
        if (isAdded() && mProgressDialogFragment != null) {
            mProgressDialogFragment.dismiss();
        }
    }

    public static MaintenanceScheduleFragment3 newInstance(Bundle oldArgs, Long bookingDate, String plant, String dealerCode, String deptCode) {
        if (oldArgs == null) {
            oldArgs = new Bundle();
        }
        oldArgs.putLong("bookingDate", bookingDate);
        oldArgs.putString("plant", plant);
        oldArgs.putString("dealerCode", dealerCode);
        oldArgs.putString("deptCode", deptCode);

        MaintenanceScheduleFragment3 f = new MaintenanceScheduleFragment3();
        f.setArguments(oldArgs);
        return f;
    }

    OnApiListener<SetCarRepairPeriodResponseData> mOnSetCarRepairPeriodApiListener = new OnApiListener<SetCarRepairPeriodResponseData>() {

        @Override
        public void onApiTaskSuccessful(SetCarRepairPeriodResponseData responseData) {
            dismissDialog();
            if (responseData.isSuccessful()) {
                ((MaintenanceActivity) getActivity()).finishAndGoReservations();
            } else {
                if (responseData.getErrorCode().equals("-855")) {
                    showServerErrorDialog(getString(R.string.maintenance_reservation_max_title), responseData.getErrorMessage(), true,
                            new AlertDialogFragment.Callbacks() {
                                @Override
                                public void onPositiveButtonClicked() {
                                    ((MaintenanceActivity) getActivity()).finishAndGoReservations();
                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }

                                @Override
                                public void onBackKeyPressed() {

                                }
                            });
                } else {
                    showServerErrorDialog(responseData.getErrorMessage());
                }
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissDialog();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mProgressDialogFragment = ProgressDialogFragment.newInstance("預約中..");
            mProgressDialogFragment.show(getFragmentManager(), "setCarRepairPeriodDialog");
        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            dismissDialog();
            showMultiLoginErrorDialog();
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_maintenance_schedule_3, container, false);

        Bundle args = getArguments();
        if (args != null) {
            mPlate = args.getString("plate");
            mMileage = args.getString("mileage");
            mDealerCode = args.getString("dealerCode");
            mDeptCode = args.getString("deptCode");
            mSchedule = args.getBoolean("schedule");
            mRepair = args.getBoolean("repair");
            mNotes = args.getString("notes");
            mBookingDate = args.getLong("bookingDate");
            mPlant = args.getString("plant");
        }

        TextView dateTextView = v.findViewById(R.id.date);
        mChosenDate.setTimeInMillis(mBookingDate);
        mDateFormatter.setTimeZone(mChosenDate.getTimeZone());
        dateTextView.setText(mDateFormatter.format(mChosenDate.getTime()));

        TextView plantTextView = v.findViewById(R.id.plant);
        plantTextView.setText(mPlant);

        TextView plateTextView = v.findViewById(R.id.plate);
        plateTextView.setText(mPlate);

        TextView mileageTextView = v.findViewById(R.id.mileage);
        if (TextUtils.isEmpty(mMileage)) {
            mileageTextView.setText(getString(R.string.maintenance_reservation_mileage_unit, "0"));
        } else {
            mileageTextView.setText(getString(R.string.maintenance_reservation_mileage_unit, mMileage));
        }

        TextView maintenanceTextView = v.findViewById(R.id.amount);
        if (mSchedule && mRepair) {
            maintenanceTextView.setText(R.string.maintenance_schedule_maintenance_n_repair);
        } else if (mSchedule) {
            maintenanceTextView.setText(R.string.maintenance_schedule_maintenance);
        } else if (mRepair) {
            maintenanceTextView.setText(R.string.maintenance_schedule_repair);
        }

        TextView notesTextView = v.findViewById(R.id.notes);
        if (mNotes == null || mNotes.isEmpty()) {
            notesTextView.setText(R.string.maintenance_schedule_notes_none);
        } else {
            notesTextView.setText(mNotes);
        }

        Button prevButton = v.findViewById(R.id.btn_prev);
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popFragment();
            }
        });

        Button nextButton = v.findViewById(R.id.btn_reserve);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAPIToReserveCarRepair();
            }
        });

        return v;
    }

    private void callAPIToReserveCarRepair() {
        UserInfo userInfo = Prefs.getUserInfo(getContext());
        String bookingDateString = mDateFormatterForServer.format(mChosenDate.getTime());
        SetCarRepairPeriodRequestData data = new SetCarRepairPeriodRequestData(userInfo.getKeyUid(),
                userInfo.getToken(),
                mPlate,
                bookingDateString,
                mDealerCode,
                mDeptCode);
        data.setOdo(mMileage);
        data.setMile(mSchedule ? "Y" : "");
        data.setItem(mRepair ? "Y" : "");
        data.setNotes(mNotes);

        AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(getContext());
        assistantWebApi.setCarRepairPeriod(data, mOnSetCarRepairPeriodApiListener);
    }
}
