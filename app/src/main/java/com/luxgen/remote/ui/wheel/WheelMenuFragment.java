package com.luxgen.remote.ui.wheel;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.luxgen.remote.R;
import com.luxgen.remote.ui.dialog.AlertDialogFragment;
import com.luxgen.remote.custom.CustomBaseFragmentWithCaService;
import com.luxgen.remote.custom.OnSwipeListener;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.WheelPage;
import com.luxgen.remote.model.WheelTileItem;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.util.BitmapUtils;
import com.luxgen.remote.util.NetUtils;
import com.luxgen.remote.util.Prefs;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;
import static com.luxgen.remote.ui.wheel.WheelMenuItems.EXTRA_CHECK_KEY;

public class WheelMenuFragment extends CustomBaseFragmentWithCaService {

    private final static int REQUEST_ADD_WHEEL_MENU_ITEM = 9527;

    private WheelMenuLayout mWheelMenuLayout;
    private GestureDetectorCompat mGestureDetector;
    private boolean mOnRight = false;
    private SharedPreferences mSharedPreferences;
    private int mCurrentTile = -1;
    private String mKeyUid;
    private ImageView mSettingsImageView;

    public static WheelMenuFragment newInstance(boolean right) {
        Bundle args = new Bundle();
        args.putBoolean("right", right);

        WheelMenuFragment fragment = new WheelMenuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private View.OnClickListener mOnHandleImageViewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            getActivity().finish();
        }
    };

    private View.OnClickListener mOnSettingsImageViewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            oneClickMenuClosed(false);
            startActivity(WheelMenuSetupActivity.newIntent(getActivity()));
        }
    };

    private OnSwipeListener mOnSwipeListener = new OnSwipeListener() {

        private boolean checkNetworkFirst(int actionId) {
            return actionId == WheelTileItem.ACTION_FIX_RESERVATION || actionId == WheelTileItem.ACTION_YOUR_RESERVATION ||
                    actionId == WheelTileItem.ACTION_RESERVATION_PROGRESS || actionId == WheelTileItem.ACTION_RESERVATION_RECORD ||
                    actionId == WheelTileItem.ACTION_CAR_STATUS || actionId == WheelTileItem.ACTION_ETC_BALANCE ||
                    actionId == WheelTileItem.ACTION_ACCOUNT_MODIFY;
        }

        private boolean isRemoteControls(int actionId) {
            return actionId == WheelTileItem.ACTION_FRESH_AIR || actionId == WheelTileItem.ACTION_FIND_CAR ||
                    actionId == WheelTileItem.ACTION_CAR_UNLOCK || actionId == WheelTileItem.ACTION_BACKDOOR_OPEN ||
                    actionId == WheelTileItem.ACTION_CAR_LOCK || actionId == WheelTileItem.ACTION_ENGINE_OFF;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            if (mWheelMenuLayout != null) {
                int actionId = mWheelMenuLayout.onSingleTapConfirmed(e);
                if (actionId != -1) {
                    if (actionId == WheelTileItem.ACTION_EMPTY) {
                        mCurrentTile = mWheelMenuLayout.whichTile(e);
                        startWheelMenuItemChooser();
                        return true;
                    } else {
                        Intent intent = WheelMenuItems.getActionIntent(getContext(), actionId);
                        if (intent.hasExtra(EXTRA_CHECK_KEY)) {
                            if (mCommunicationAgentService != null) {
                                if (mCommunicationAgentService.hasAnyKey()) {
                                    if (checkNetworkFirst(actionId) && !NetUtils.isConnectToInternet(getActivity())) {
                                        showServerErrorDialog();
                                        return true;
                                    } else if (isRemoteControls(actionId) && !mCommunicationAgentService.isVigReady() && !NetUtils.isConnectToInternet(getActivity())) {
                                        showServerErrorDialog();
                                        return true;
                                    }
                                    startActivity(intent);
                                    getActivity().finish();
                                } else {
                                    showKeyActivationDialog();
                                }
                            }
                        } else {
                            if (checkNetworkFirst(actionId) && !NetUtils.isConnectToInternet(getActivity())) {
                                showServerErrorDialog();
                                return true;
                            } else if (isRemoteControls(actionId) && !mCommunicationAgentService.isVigReady() && !NetUtils.isConnectToInternet(getActivity())) {
                                showServerErrorDialog();
                                return true;
                            }
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }
                }
            }
            return super.onSingleTapUp(e);
        }

        @Override
        public void onLongPress(MotionEvent e) {
            if (mWheelMenuLayout != null) {
                mWheelMenuLayout.onLongPress(e);
            }
            super.onLongPress(e);
        }

        @Override
        public boolean onSwipe(Direction direction) {
            if (direction == Direction.down) {
                if (mWheelMenuLayout != null) {
                    mWheelMenuLayout.swipeDown();
                }
            } else if (direction == Direction.up) {
                if (mWheelMenuLayout != null) {
                    mWheelMenuLayout.swipeUp();
                }
            } else if (direction == Direction.right) {
                if (mOnRight) {
                    getActivity().finish();
                }
            } else if (direction == Direction.left) {
                if (!mOnRight) {
                    getActivity().finish();
                }
            }
            return super.onSwipe(direction);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPreferences = getActivity().getSharedPreferences("wheelmenu", Context.MODE_PRIVATE);
        mKeyUid = Prefs.getKeyUid(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        mOnRight = args.getBoolean("right", mOnRight);

        View v = inflater.inflate(mOnRight ? R.layout.fragment_wheel_menu_right : R.layout.fragment_wheel_menu_left,
                container, false);

        mWheelMenuLayout = v.findViewById(R.id.wheel_menu);
        mWheelMenuLayout.setWheelMenuEventListener(new WheelMenuLayout.WheelMenuEventListener() {
            @Override
            public void onLevelChanged(int level) {
                if (mSettingsImageView != null) {
                    mSettingsImageView.setVisibility(level == 0 ? View.VISIBLE : View.INVISIBLE);
                }
            }
        });
        ImageView mHandleImageView = v.findViewById(R.id.handle);
        mHandleImageView.setOnClickListener(mOnHandleImageViewClickListener);

        mSettingsImageView = v.findViewById(R.id.settings);
        mSettingsImageView.setOnClickListener(mOnSettingsImageViewClickListener);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mGestureDetector = new GestureDetectorCompat(getActivity(), mOnSwipeListener);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (mWheelMenuLayout != null) {
                    mWheelMenuLayout.onTouchEvent(v, event);
                }

                return mGestureDetector.onTouchEvent(event);
            }
        });

        Bitmap originBackground = BitmapUtils.loadImage(getContext(), "background");
        BitmapDrawable backgroundDrawable = new BitmapDrawable(getResources(), originBackground);
        ImageView background = view.findViewById(R.id.wheel_background);
        background.setImageDrawable(backgroundDrawable);
        background.setAnimation(getFadeInAnimation(250));
    }

    private Animation getFadeInAnimation(int duration) {
        Animation fadeIn = new AlphaAnimation(0.5f, 0.95f);
        fadeIn.setDuration(duration);

        return fadeIn;
    }

    private boolean oneClickMenuClosed(boolean animation) {
        if (mWheelMenuLayout != null && mWheelMenuLayout.isInOneClickMenu()) {
            mWheelMenuLayout.closeOneClickItems(animation);
            return true;
        }
        return false;
    }

    public boolean onBackPressed() {
        if (oneClickMenuClosed(true)) {
            return true;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mWheelMenuLayout != null) {
            UserInfo userInfo = Prefs.getUserInfo(getContext());
            CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
            mWheelMenuLayout.setVig((carKeyData != null) && carKeyData.isVig());
            mWheelMenuLayout.setDms((userInfo != null) && userInfo.isDMS());

            mWheelMenuLayout.setWheelPages(loadWheelMenu());
            mWheelMenuLayout.start();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mWheelMenuLayout != null) {
            mWheelMenuLayout.stop();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_ADD_WHEEL_MENU_ITEM) {
            WheelTileItem item = data.getParcelableExtra("item");
            mWheelMenuLayout.setWheelTile(mCurrentTile, item);
            saveWheelMenu(mWheelMenuLayout.getWheelPages());
        }
    }

    private void startWheelMenuItemChooser() {
        ArrayList<WheelPage> mWheelPages = loadWheelMenuArrayList();
        WheelMenuItemChooserFragment fragment = WheelMenuItemChooserFragment.newInstance(mWheelPages, true);
        fragment.setTargetFragment(this, REQUEST_ADD_WHEEL_MENU_ITEM);
        show(fragment);
    }

    private ArrayList<WheelPage> loadWheelMenuArrayList() {
        int numPages = mSharedPreferences.getInt(mKeyUid + "_num_pages", 1);

        ArrayList<WheelPage> pages = new ArrayList<>();
        for (int i = 0; i < numPages; i++) {
            WheelPage page = new WheelPage(null);
            for (int j = 0; j < WheelMenuLayout.WHEEL_TILE_MAX; j++) {
                int actionId = mSharedPreferences.getInt(mKeyUid + "_page_" + i + "_pos_" + j, -1);
                if (actionId != -1) {
                    WheelTileItem item = WheelMenuItems.get(actionId);
                    page.setItem(item, j);
                }
            }
            pages.add(page);
        }

        return pages;
    }

    private void initialWheelMenu() {
        SharedPreferences.Editor mEdit = mSharedPreferences.edit();
        mEdit.putInt(mKeyUid + "_num_pages", 1);
        mEdit.putInt(mKeyUid + "_page_0_pos_0", WheelTileItem.ACTION_FIX_RESERVATION);
        mEdit.putInt(mKeyUid + "_page_0_pos_1", WheelTileItem.ACTION_CLICK_SERVICE);
        mEdit.putInt(mKeyUid + "_page_0_pos_2", WheelTileItem.ACTION_CAR_LOCK);
        mEdit.putInt(mKeyUid + "_page_0_pos_3", WheelTileItem.ACTION_FIND_CAR);

        mEdit.apply();
    }

    private WheelPage[] loadWheelMenu() {
        int numPages = mSharedPreferences.getInt(mKeyUid + "_num_pages", 0);
        if (numPages == 0) {
            numPages = 1;
            initialWheelMenu();
        }

        WheelPage[] pages = new WheelPage[WheelMenuLayout.WHEEL_PAGES_MAX];
        for (int i = 0; i < numPages; i++) {
            WheelPage page = new WheelPage(null);
            for (int j = 0; j < WheelMenuLayout.WHEEL_TILE_MAX; j++) {
                int actionId = mSharedPreferences.getInt(mKeyUid + "_page_" + i + "_pos_" + j, -1);
                if (actionId != -1) {
                    WheelTileItem item = WheelMenuItems.get(actionId);
                    page.setItem(item, j);
                }
            }
            pages[i] = page;
        }

        return pages;
    }

    private void saveWheelMenu(ArrayList<WheelPage> pages) {
        SharedPreferences.Editor mEdit = mSharedPreferences.edit();

        int numPages = pages.size();
        mEdit.putInt(mKeyUid + "_num_pages", numPages);

        for (int i = 0; i < numPages; i++) {
            WheelPage page = pages.get(i);
            if (page == null) {
                continue;
            }

            WheelTileItem[] items = page.getItems();
            for (int j = 0; j < WheelMenuLayout.WHEEL_TILE_MAX; j++) {
                WheelTileItem item = items[j];
                mEdit.putInt(mKeyUid + "_page_" + i + "_pos_" + j, item == null ? -1 : item.getActionId());
            }
        }

        mEdit.apply();
    }

    private void showKeyActivationDialog() {
        AlertDialogFragment dialog = AlertDialogFragment.newInstance(
                getString(R.string.main_dialog_master_key_is_inactive_title), getString(R.string.main_dialog_master_key_is_inactive_message));
        dialog.setButtons(getString(R.string.btn_confirm), null, null);
        dialog.setFinishOnClose(true);
        dialog.show(getFragmentManager(), "showKeyActivationDialog");
    }
}
