package com.luxgen.remote.ui.oks;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomFragmentsAppCompatActivity;

public class CarStolenTrackingActivity extends CustomFragmentsAppCompatActivity {

    public static final String INTENT_EXTRA_NAME_LATITUDE = "lat";
    public static final String INTENT_EXTRA_NAME_LONGITUDE = "lng";

    public static Intent newIntent(Context context, double lat, double lng) {
        Intent intent = new Intent(context, CarStolenTrackingActivity.class);
        intent.putExtra(INTENT_EXTRA_NAME_LATITUDE, lat);
        intent.putExtra(INTENT_EXTRA_NAME_LONGITUDE, lng);

        return intent;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_car_stolen_tracking;
    }

    @Override
    protected void setupTitle(String tag) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.car_stolen_tracking_title);
        } else {
            setTitle(R.string.car_stolen_tracking_title);
        }

        CarStolenTrackingFragment fragment = new CarStolenTrackingFragment();
        fragment.setArguments(getIntent().getExtras());
        show(fragment);
        bindCaService();
    }

    @Override
    protected void onDestroy() {
        unbindCaService();
        super.onDestroy();
    }
}
