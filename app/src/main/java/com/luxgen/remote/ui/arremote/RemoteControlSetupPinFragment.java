package com.luxgen.remote.ui.arremote;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.util.Prefs;

import static com.luxgen.remote.ui.arremote.RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_AIR_DATA;
import static com.luxgen.remote.ui.arremote.RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_MENU;
import static com.luxgen.remote.ui.arremote.RemoteControlFragment.REQUEST_NONE;

public class RemoteControlSetupPinFragment extends CustomBaseFragment {

    private final static String INTENT_EXTRA_NAME_STARTUP_FRAGMENT = "startUp";
    private final static String INTENT_EXTRA_NAME_FUNCTION = "function";

    private EditText mNewPinCodeEditText, mConfirmPinCodeEditText;

    public static RemoteControlSetupPinFragment newInstance(String startUpFragment, int function) {
        Bundle args = new Bundle();
        args.putString(INTENT_EXTRA_NAME_STARTUP_FRAGMENT, startUpFragment);
        args.putInt(INTENT_EXTRA_NAME_FUNCTION, function);
        RemoteControlSetupPinFragment f = new RemoteControlSetupPinFragment();
        f.setArguments(args);
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_remote_control_setup_pin, container, false);

        mNewPinCodeEditText = v.findViewById(R.id.pincode_new);
        mConfirmPinCodeEditText = v.findViewById(R.id.pincode_confirm);
        mConfirmPinCodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                if (!str.equals(mNewPinCodeEditText.getText().toString().trim())) {
                    mConfirmPinCodeEditText.setError(getString(R.string.remote_control_pincode_error_not_match));
                } else {
                    mConfirmPinCodeEditText.setError(null);
                }
            }
        });

        Button finishButton = v.findViewById(R.id.btn_finish);
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPinCode();
            }
        });

        return v;
    }

    private int getFunction() {
        Bundle args = getArguments();
        if (args != null) {
            return args.getInt(INTENT_EXTRA_NAME_FUNCTION, REQUEST_NONE);
        }

        return REQUEST_NONE;
    }

    private String getStartUpFragment() {
        String startUpFragment = FRAGMENT_TAG_REMOTE_CONTROL_MENU;
        Bundle args = getArguments();
        if (args != null) {
            startUpFragment = args.getString(INTENT_EXTRA_NAME_STARTUP_FRAGMENT);
        }

        return startUpFragment;
    }

    private void continueRemoteControl() {
        hideSoftKeyboard(mNewPinCodeEditText);
        hideSoftKeyboard(mConfirmPinCodeEditText);
        popFragment();
        if (FRAGMENT_TAG_REMOTE_CONTROL_AIR_DATA.equals(getStartUpFragment())) {
            if (getTargetFragment() != null) {
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
            }
        } else {
            ((RemoteControlActivity) getActivity()).navigate(this, RemoteControlFragment.newInstance(getFunction()), FRAGMENT_TAG_REMOTE_CONTROL_MENU, false);
        }
    }

    private void setPinCode() {
        String newPinCode = mNewPinCodeEditText.getText().toString().trim();
        String confirmPinCode = mConfirmPinCodeEditText.getText().toString().trim();

        if (newPinCode.isEmpty()) {
            mNewPinCodeEditText.requestFocus();
            mNewPinCodeEditText.setError(getString(R.string.remote_control_pincode_error_empty));
            return;
        }

        if (newPinCode.length() < 4) {
            mNewPinCodeEditText.requestFocus();
            mNewPinCodeEditText.setError(getString(R.string.remote_control_pincode_error_short));
            return;
        }

        if (confirmPinCode.isEmpty()) {
            mConfirmPinCodeEditText.requestFocus();
            mConfirmPinCodeEditText.setError(getString(R.string.remote_control_pincode_error_empty));
            return;
        }

        if (!newPinCode.equals(confirmPinCode)) {
            mConfirmPinCodeEditText.requestFocus();
            mConfirmPinCodeEditText.selectAll();
            mConfirmPinCodeEditText.setError(getString(R.string.remote_control_pincode_error_not_match));
            return;
        }

        setPinCode(confirmPinCode);
        continueRemoteControl();
    }

    private void setPinCode(String pinCode) {
        Prefs.setPinCode(getContext(), pinCode);
        Prefs.setPinShowTime(getContext(), getCurrentTime());
    }
}
