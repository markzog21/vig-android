package com.luxgen.remote.ui.oks;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;

public class CarAccidentFragment extends CustomBaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_car_accident, container, false);

        Button yesButton = v.findViewById(R.id.btn_yes);
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show(CarAccidentReportFragment.newInstance(true));
            }
        });

        Button noButton = v.findViewById(R.id.btn_no);
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show(CarAccidentReportFragment.newInstance(false));
            }
        });

        return v;
    }
}
