package com.luxgen.remote.ui.login;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.luxgen.remote.R;
import com.luxgen.remote.ui.dialog.CountDownDialogFragment;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.ui.dialog.ProgressDialogFragment;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.UserWebApi;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.user.ReSendPasswordRequestData;

public class LoginForgetPasswordFragment extends CustomBaseFragment {

    private final static int COUNT_DOWN_SECONDS = 3;
    private EditText mEmailEditText;
    private EditText mPhoneEditText;
    private ProgressDialogFragment mProgressFragment;

    private OnApiListener<ResponseData> mOnApiListener = new OnApiListener<ResponseData>() {
        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            mProgressFragment.dismiss();
            if (responseData.isSuccessful()) {
                showAutoRedirectDialog();
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            mProgressFragment.dismiss();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mProgressFragment = ProgressDialogFragment.newInstance(getString(R.string.login_forget_password_verify));
            mProgressFragment.show(getFragmentManager(), "resendPasswordProgressDialog");
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            // TODO: check if need this
            mProgressFragment.dismiss();
            showMultiLoginErrorDialog();
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(R.string.login_forget_password);

        View v = inflater.inflate(R.layout.fragment_login_forget_password, container, false);

        mEmailEditText = v.findViewById(R.id.edit_email);
        mPhoneEditText = v.findViewById(R.id.edit_phone);

        Button confirmButton = v.findViewById(R.id.btn_confirm);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    requestNewPassword();
                }
            }
        });

        return v;
    }

    private boolean validate() {
        String email = mEmailEditText.getText().toString().trim();
        String phone = mPhoneEditText.getText().toString().trim();

        if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            showWrongInfoDialog();
            return false;
        }

        if (TextUtils.isEmpty(phone) || !Patterns.PHONE.matcher(phone).matches()) {
            showWrongInfoDialog();
            return false;
        }

        return true;
    }

    private ReSendPasswordRequestData buildReSendPasswordData(String account, String phone) {
        ReSendPasswordRequestData data = new ReSendPasswordRequestData(account);
        data.setPhone(phone);

        return data;
    }

    private void requestNewPassword() {
        String email = mEmailEditText.getText().toString().trim();
        String phone = mPhoneEditText.getText().toString().trim();

        ReSendPasswordRequestData data = buildReSendPasswordData(email, phone);

        UserWebApi userWebApi = UserWebApi.getInstance(getActivity());
        userWebApi.reSendPassword(data, mOnApiListener);
    }

    private void showAutoRedirectDialog() {
        CountDownDialogFragment autoRedirectDialog = CountDownDialogFragment.newInstance(
                getString(R.string.login_password_sent), COUNT_DOWN_SECONDS);
        autoRedirectDialog.show(getFragmentManager(), "showAutoRedirectDialog");
    }

    private void showWrongInfoDialog() {
        showConfirmDialog(getString(R.string.login_wrong_info_title), getString(R.string.login_wrong_info_message));
    }
}
