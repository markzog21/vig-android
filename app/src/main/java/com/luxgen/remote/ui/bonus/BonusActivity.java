package com.luxgen.remote.ui.bonus;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomAppCompatActivity;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.UserWebApi;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.user.GetCarBonusRequestData;
import com.luxgen.remote.network.model.user.GetCarBonusResponseData;
import com.luxgen.remote.util.Prefs;

public class BonusActivity extends CustomAppCompatActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, BonusActivity.class);
    }

    private GetUserInfoAsyncTask mGetUserInfoAsyncTask = null;
    private GetSelectedCarKeyDataAsyncTask mGetSelectedCarKeyDataAsyncTask = null;
    private UserInfo mUserInfo = null;
    private CarKeyData mSelectedCarKeyData = null;

    private AppCompatTextView mPlateNumberTextView;
    private AppCompatTextView mPointsTextView;

    private class GetUserInfoAsyncTask extends AsyncTask<Void, Void, UserInfo> {

        @Override
        protected UserInfo doInBackground(Void... voids) {
            return Prefs.getUserInfo(BonusActivity.this);
        }

        @Override
        protected void onPostExecute(UserInfo userInfo) {
            mGetUserInfoAsyncTask = null;

            mUserInfo = userInfo;
            getSelectedCarKeyData();
        }
    }

    private class GetSelectedCarKeyDataAsyncTask extends AsyncTask<Void, Void, CarKeyData> {

        @Override
        protected CarKeyData doInBackground(Void... voids) {
            return Prefs.getCarKeyData(BonusActivity.this);
        }

        @Override
        protected void onPostExecute(CarKeyData selectedCarKeyData) {
            mGetSelectedCarKeyDataAsyncTask = null;

            mSelectedCarKeyData = selectedCarKeyData;
            if (mSelectedCarKeyData != null) {
                String plateNumber = String.format(getString(R.string.bonus_plate_number_text), mSelectedCarKeyData.getCarNo());
                mPlateNumberTextView.setText(plateNumber);
            }

            callApiToGetCarBonus();
        }
    }

    View.OnClickListener mOnErrorDialogPositiveButtonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            finish();
            overridePendingTransition(0, 0);
        }
    };

    private OnApiListener<GetCarBonusResponseData> mGetCarBonusApiListener = new OnApiListener<GetCarBonusResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetCarBonusResponseData responseData) {
            dismissProgressDialog();
            if (responseData.isSuccessful()) {
                String pointsText = String.format(getString(R.string.bonus_points_text), responseData.getResult().getPoints());
                mPointsTextView.setText(pointsText);
            } else {
                showServerErrorDialog(getResources().getString(R.string.dialog_title_error) + responseData.getErrorCode(),
                        responseData.getErrorMessage(),
                        mOnErrorDialogPositiveButtonClickListener);
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(getResources().getString(R.string.dialog_title_error), failMessage, mOnErrorDialogPositiveButtonClickListener);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bonus);

        LinearLayout mainContainer = findViewById(R.id.activity_container);
        setWheelScreenLayout(mainContainer);

        ImageView wheelRightHandle = findViewById(R.id.wheel_right_handle_image_view);
        ImageView wheelLeftHandle = findViewById(R.id.wheel_left_handle_image_view);
        setWheelHandles(wheelRightHandle, wheelLeftHandle);

        Toolbar toolBar = findViewById(R.id.toolbar);
        if (toolBar != null) {
            toolBar.setTitle(R.string.activity_bonus_title);
            setSupportActionBar(toolBar);
            toolBar.setNavigationIcon(R.drawable.ic_back);
        }

        mPlateNumberTextView = findViewById(R.id.activity_bonus_plate_number_text_view);
        mPointsTextView = findViewById(R.id.activity_bonus_points_text_view);

        bindCaService();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        if (mGetUserInfoAsyncTask != null) {
            mGetUserInfoAsyncTask.cancel(true);
        }

        if (mGetSelectedCarKeyDataAsyncTask != null) {
            mGetSelectedCarKeyDataAsyncTask.cancel(true);
        }

        unbindCaService();
        super.onDestroy();
    }

    @Override
    protected void onCaServiceConnected() {
        init();
        super.onCaServiceConnected();
    }

    private void init() {
        showProgressDialog();
        if (mGetUserInfoAsyncTask == null) {
            mGetUserInfoAsyncTask = new GetUserInfoAsyncTask();
            mGetUserInfoAsyncTask.execute();
        }
    }

    private void getSelectedCarKeyData() {
        if (mGetSelectedCarKeyDataAsyncTask == null) {
            mGetSelectedCarKeyDataAsyncTask = new GetSelectedCarKeyDataAsyncTask();
            mGetSelectedCarKeyDataAsyncTask.execute();
        }
    }

    private void callApiToGetCarBonus() {
        if (mUserInfo != null && mSelectedCarKeyData != null) {
            UserWebApi userWebApi = UserWebApi.getInstance(this);
            GetCarBonusRequestData data = new GetCarBonusRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken(), mSelectedCarKeyData.getCarNo());
            userWebApi.getCarBonus(data, mGetCarBonusApiListener);
        } else {
            dismissProgressDialog();
            showServerErrorDialog(getResources().getString(R.string.dialog_title_error), "Init failed.", mOnErrorDialogPositiveButtonClickListener);
        }
    }
}
