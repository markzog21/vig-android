package com.luxgen.remote.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.luxgen.remote.R;

public class CountDownDialogFragment extends DialogFragment {

    private static int COUNT_DOWN_INTERVAL = 1000; // 1s

    private int mSeconds;
    private Handler mHandler;
    private TextView mCountDownTextView;

    public static CountDownDialogFragment newInstance(String message, int seconds) {
        Bundle args = new Bundle();
        args.putString("message", message);
        args.putInt("seconds", seconds);

        CountDownDialogFragment f = new CountDownDialogFragment();
        f.setArguments(args);
        return f;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();

        String message = args.getString("message");
        mSeconds = args.getInt("seconds");

        mHandler = new Handler(getActivity().getMainLooper());

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View customLayout = inflater.inflate(R.layout.dialog_count_down, null);
        TextView messageTextView = customLayout.findViewById(R.id.message);
        messageTextView.setText(message);

        mCountDownTextView = customLayout.findViewById(R.id.count_down);
        mCountDownTextView.setText(getString(R.string.login_finish_count_down, mSeconds));

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(customLayout)
                .setCancelable(false);

        AlertDialog mAlertDialog = builder.create();
        mAlertDialog.setCanceledOnTouchOutside(false);

        countdown();

        return mAlertDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public void onDestroyView() {
        mHandler.removeCallbacksAndMessages(null);

        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }

        super.onDestroyView();
    }

    private void countdown() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSeconds == 1) {
                    Activity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                    }
                } else {
                    mSeconds--;
                    mCountDownTextView.setText(getString(R.string.login_finish_count_down, mSeconds));
                    countdown();
                }
            }
        }, COUNT_DOWN_INTERVAL);
    }
}
