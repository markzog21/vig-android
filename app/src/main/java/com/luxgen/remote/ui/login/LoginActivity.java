package com.luxgen.remote.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.luxgen.remote.R;
import com.luxgen.remote.activity.MainActivity;
import com.luxgen.remote.custom.CustomFragmentsAppCompatActivity;

public class LoginActivity extends CustomFragmentsAppCompatActivity implements LoginFragment.OnLoginListener {

    public final static String FRAGMENT_TAG_LOGIN = "login";
    public final static String FRAGMENT_TAG_LOGIN_REGISTER_MAIN = "login_register_main";
    public final static String FRAGMENT_TAG_LOGIN_REGISTER_PASSWORD = "login_register_password";
    public final static String FRAGMENT_TAG_LOGIN_REGISTER_ADD_CAR = "login_register_add_car";
    public final static String FRAGMENT_TAG_LOGIN_REGISTER_ADD_PHONE = "login_register_add_phone";
    public final static String FRAGMENT_TAG_LOGIN_REGISTER_VERIFY_PHONE = "login_register_verify_phone";
    public final static String FRAGMENT_TAG_LOGIN_REGISTER_KEY_ACTIVATION = "login_register_key_activation";
    public final static String FRAGMENT_TAG_LOGIN_REGISTER_ACTIVATING_KEY = "login_register_activating_key";
    public final static String FRAGMENT_TAG_BLUETOOTH_INSTRUCTION = "bluetooth_instruction";

    private boolean isSocialRegisterInLoginFragment = false;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void setupTitle(String tag) {
        if (FRAGMENT_TAG_LOGIN_REGISTER_MAIN.equals(tag) || FRAGMENT_TAG_LOGIN.equals(tag)) {
            getSupportActionBar().hide();
            return;
        } else {
            getSupportActionBar().show();
        }

        if (FRAGMENT_TAG_LOGIN_REGISTER_KEY_ACTIVATION.equals(tag) || FRAGMENT_TAG_LOGIN_REGISTER_ACTIVATING_KEY.equals(tag)) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            setTitle(R.string.login_register_key_activation);
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (FRAGMENT_TAG_LOGIN_REGISTER_PASSWORD.equals(tag)) {
            setTitle(R.string.login_register_password);
        } else if (FRAGMENT_TAG_LOGIN_REGISTER_ADD_CAR.equals(tag)) {
            setTitle(R.string.login_register_add_new_car);
        } else if (FRAGMENT_TAG_LOGIN_REGISTER_ADD_PHONE.equals(tag) || FRAGMENT_TAG_LOGIN_REGISTER_VERIFY_PHONE.equals(tag)) {
            setTitle(R.string.login_register_add_phone);
        } else if (FRAGMENT_TAG_BLUETOOTH_INSTRUCTION.equals(tag)) {
            setTitle(R.string.bluetooth_instruction);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        show(LoginFragment.newInstance(), FRAGMENT_TAG_LOGIN);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        String tag = fragment == null ? "" : fragment.getTag();
        if (FRAGMENT_TAG_LOGIN_REGISTER_KEY_ACTIVATION.equals(tag)) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else if (isSocialRegisterInLoginFragment &&
                fragment instanceof LoginRegisterPasswordFragment
        ) {
            isSocialRegisterInLoginFragment = false;
            int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
            for (int i = 0; i < backStackCount; i++) {

                // Get the back stack fragment id.
                int backStackId = getSupportFragmentManager().getBackStackEntryAt(i).getId();

                getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
            show(LoginFragment.newInstance(), FRAGMENT_TAG_LOGIN);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onSocialRegisterInLoginFragment() {
        isSocialRegisterInLoginFragment = true;
    }
}
