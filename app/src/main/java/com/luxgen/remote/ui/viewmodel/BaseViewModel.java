package com.luxgen.remote.ui.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.db.VigDatabase;
import com.luxgen.remote.util.Prefs;

public class BaseViewModel extends AndroidViewModel {
    private static final String TAG = BaseViewModel.class.getSimpleName();

    private VigDatabase database;
    private UserInfo userInfo;

    public UserInfo getUserInfo() {
        return this.userInfo;
    }

    protected VigDatabase getDatabase() {
        return this.database;
    }

    public BaseViewModel(@NonNull Application application) {
        super(application);

        database = VigDatabase.getInstance(application);
        userInfo = Prefs.getUserInfo(application.getApplicationContext());
        LogCat.printUser(TAG, userInfo);
    }

}
