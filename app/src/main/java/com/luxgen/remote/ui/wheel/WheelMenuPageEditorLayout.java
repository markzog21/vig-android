package com.luxgen.remote.ui.wheel;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.luxgen.remote.R;

import static com.luxgen.remote.ui.wheel.WheelMenuLayout.WHEEL_TILE_MAX;

public class WheelMenuPageEditorLayout extends LinearLayout {

    public interface OnItemClickListener {
        void onAddItemClick(ImageView v, int page, int position);

        void onDelItemClick(ImageView v, int page, int position);

        void onDelPageClick(int pageNumber);
    }

    private TextView mTitleTextView;
    private ImageView[] mMenuItemsImageView = new ImageView[WHEEL_TILE_MAX];
    private ImageView[] mMenuRemovalsImageView = new ImageView[WHEEL_TILE_MAX];
    private ImageView mPageRemovalImageView;
    private boolean[] mHasMenuItem = new boolean[WHEEL_TILE_MAX];

    private OnItemClickListener mOnItemClickListener;

    private boolean mInEditMode = false;
    private int mPageNumber;

    public WheelMenuPageEditorLayout(Context context) {
        super(context);
        init(context, null, 0, 0);
    }

    public WheelMenuPageEditorLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0, 0);
    }

    public WheelMenuPageEditorLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr, 0);
    }

    public WheelMenuPageEditorLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        View v = LayoutInflater.from(context).inflate(R.layout.template_wheel_menu_page_editor, this);

        mTitleTextView = v.findViewById(R.id.title);

        mPageRemovalImageView = v.findViewById(R.id.wheel_page_removal);
        mPageRemovalImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onDelPageClick(mPageNumber);
                }
            }
        });

        mMenuItemsImageView[0] = v.findViewById(R.id.wheel_tile_1);
        mMenuItemsImageView[1] = v.findViewById(R.id.wheel_tile_2);
        mMenuItemsImageView[2] = v.findViewById(R.id.wheel_tile_3);
        mMenuItemsImageView[3] = v.findViewById(R.id.wheel_tile_4);
        mMenuItemsImageView[4] = v.findViewById(R.id.wheel_tile_5);

        for (int i = 0; i < mMenuItemsImageView.length; i++) {
            mMenuItemsImageView[i].setTag(i);
            mMenuItemsImageView[i].setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mInEditMode) {
                        return;
                    }

                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onAddItemClick((ImageView) v, mPageNumber, (int) v.getTag());
                    }
                }
            });
        }

        mMenuRemovalsImageView[0] = v.findViewById(R.id.wheel_tile_1_removal);
        mMenuRemovalsImageView[1] = v.findViewById(R.id.wheel_tile_2_removal);
        mMenuRemovalsImageView[2] = v.findViewById(R.id.wheel_tile_3_removal);
        mMenuRemovalsImageView[3] = v.findViewById(R.id.wheel_tile_4_removal);
        mMenuRemovalsImageView[4] = v.findViewById(R.id.wheel_tile_5_removal);

        for (int i = 0; i < mMenuRemovalsImageView.length; i++) {
            mMenuRemovalsImageView[i].setTag(i);
            mMenuRemovalsImageView[i].setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        int pos = (int) v.getTag();
                        mOnItemClickListener.onDelItemClick(mMenuItemsImageView[pos], mPageNumber, pos);
                    }
                }
            });
        }
    }

    public void setInEditMode(boolean mode) {
        if (mInEditMode == mode) {
            return;
        }

        mInEditMode = mode;
        int visibility = mInEditMode ? View.VISIBLE : View.GONE;

        mPageRemovalImageView.setVisibility(visibility);
        for (int i = 0; i < mMenuRemovalsImageView.length; i++) {
            if (mHasMenuItem[i]) {
                mMenuRemovalsImageView[i].setVisibility(visibility);
            }
        }
    }

    public void hidePageRemovalImageView() {
        mPageRemovalImageView.setVisibility(View.GONE);
    }

    public void setItemImageResource(int num, @DrawableRes int resId) {
        if (num >= 0 && num < WHEEL_TILE_MAX) {
            mMenuItemsImageView[num].setImageResource(resId);
            mHasMenuItem[num] = resId != R.drawable.ic_smart_wheel_set_add_new_item;
            mMenuRemovalsImageView[num].setVisibility(GONE);
        }
    }

    public void setPageNumber(int num) {
        mPageNumber = num;
        mTitleTextView.setText(getContext().getString(R.string.page_number, num + 1));
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }
}
