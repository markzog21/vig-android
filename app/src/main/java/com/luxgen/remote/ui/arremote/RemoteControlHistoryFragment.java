package com.luxgen.remote.ui.arremote;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.custom.RecyclerViewEmptySupport;
import com.luxgen.remote.custom.SimpleDividerItemDecoration;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.CarWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.VigRequestData;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.car.GetRemoteControlLogResponseData;
import com.luxgen.remote.util.Prefs;

public class RemoteControlHistoryFragment extends CustomBaseFragment {

    private final static int DIVIDER_START_SPACING_DP = 24;
    private RemoteControlHistoryAdapter mAdapter;

    public static RemoteControlHistoryFragment newInstance() {
        RemoteControlHistoryFragment f = new RemoteControlHistoryFragment();

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_remote_control_history, container, false);

        View emptyView = v.findViewById(R.id.list_empty);
        RecyclerViewEmptySupport recyclerView = v.findViewById(R.id.list);
        mAdapter = new RemoteControlHistoryAdapter(inflater);

        setupRecyclerView(recyclerView, null, emptyView, mAdapter);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        fetchRemoteControlHistory();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.action_refresh);
        if (item != null) {
            item.setVisible(false);
        }
    }

    private void setupRecyclerView(RecyclerViewEmptySupport recyclerView, View headerView, View emptyView, RemoteControlHistoryAdapter adapter) {
        Context context = recyclerView.getContext();
        int spacing = dp2px(recyclerView.getContext(), DIVIDER_START_SPACING_DP);

        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(context, R.drawable.recyclerview_horizontal_divider, spacing));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        if (headerView != null) {
            recyclerView.setHeaderView(headerView);
        }
        if (emptyView != null) {
            recyclerView.setEmptyView(emptyView);
        }
        recyclerView.setAdapter(adapter);
    }

    private int dp2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    private OnApiListener<GetRemoteControlLogResponseData> mOnGetRemoteControlApiListener = new OnApiListener<GetRemoteControlLogResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetRemoteControlLogResponseData responseData) {
            if (responseData.isSuccessful()) {
                mAdapter.updateItems(responseData.getList());
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private void fetchRemoteControlHistory() {
        UserInfo userInfo = Prefs.getUserInfo(getContext());
        CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
        if (userInfo == null || carKeyData == null) {
            return;
        }

        VigRequestData data = new VigRequestData(userInfo.getKeyUid(), userInfo.getToken(), carKeyData.getCarId());

        CarWebApi carWebApi = CarWebApi.getInstance(getContext());
        carWebApi.getRemoteControlLog(data, mOnGetRemoteControlApiListener);
    }
}
