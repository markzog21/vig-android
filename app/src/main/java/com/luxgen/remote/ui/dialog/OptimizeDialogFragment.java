package com.luxgen.remote.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.luxgen.remote.R;
import com.luxgen.remote.ui.arremote.SeekBarIndicated;

import java.util.Locale;

public class OptimizeDialogFragment extends DialogFragment {

    public final static int DEFAULT_TEMPERATURE = 24;

    public interface Callbacks {
        void onPositiveButtonClicked(float temperature);

        void onNegativeButtonClicked();
    }

    private Activity mActivity;
    private Callbacks callbacks;
    private String positiveMsg, negativeMsg;
    private boolean finishOnClose = false;
    private SeekBarIndicated seekBarIndicated;
    private float mDefaultTemperature = DEFAULT_TEMPERATURE;

    public static OptimizeDialogFragment newInstance(String title, String message) {
        OptimizeDialogFragment f = new OptimizeDialogFragment();

        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);

        f.setArguments(args);
        return f;
    }

    private final DialogInterface.OnClickListener buttonListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    if (callbacks != null) {
                        callbacks.onPositiveButtonClicked(seekBarIndicated.getProgress());
                    }
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    if (callbacks != null) {
                        callbacks.onNegativeButtonClicked();
                    }
                    break;
            }
            dialog.dismiss();

            if (finishOnClose) {
                mActivity.finish();
            }
        }
    };

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();

        String title = args.getString("title");
        String message = args.getString("message");

        if (savedInstanceState != null) {
            positiveMsg = savedInstanceState.getString("positiveMsg", null);
            negativeMsg = savedInstanceState.getString("negativeMsg", null);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false);

        if (positiveMsg != null) {
            builder.setPositiveButton(positiveMsg, buttonListener);
        }

        if (negativeMsg != null) {
            builder.setNegativeButton(negativeMsg, buttonListener);
        }

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_optimize, null);
        seekBarIndicated = view.findViewById(R.id.seekbarindicated);
        seekBarIndicated.setMinValBaseText("%d°");
        seekBarIndicated.setMaxValBaseText("%d°");
        seekBarIndicated.setMin(18);
        seekBarIndicated.setMax(32);
        seekBarIndicated.setValue(mDefaultTemperature);
        seekBarIndicated.setTextProviderIndicator(new SeekBarIndicated.TextProvider() {
            @Override
            public String provideText(float progress) {
                return String.format(Locale.ENGLISH, "%.1f°", progress);
            }
        });
        builder.setView(view);

        AlertDialog mAlertDialog = builder.create();
        mAlertDialog.setCanceledOnTouchOutside(false);

        return mAlertDialog;
    }

    public void setButtons(String positiveMsg, String negativeMsg, Callbacks callbacks) {
        this.positiveMsg = positiveMsg;
        this.negativeMsg = negativeMsg;
        this.callbacks = callbacks;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("positiveMsg", positiveMsg);
        outState.putString("negativeMsg", negativeMsg);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    public void setFinishOnClose(boolean enable) {
        finishOnClose = enable;
    }

    public void setTemperature(float temp) {
        if (temp == -1) {
            temp = DEFAULT_TEMPERATURE;
        }
        mDefaultTemperature = temp;
    }
}
