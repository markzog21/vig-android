package com.luxgen.remote.ui.push;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.activity.DigitalKeyActivity;
import com.luxgen.remote.ui.dialog.AlertDialogFragment;
import com.luxgen.remote.custom.CustomAppCompatActivity;
import com.luxgen.remote.model.PushData;

import ru.noties.markwon.Markwon;

public class PushDetailActivity extends CustomAppCompatActivity {

    private static final String BUNDLE = "bundel";
    private final String TAG_ADD_SUB_KEY = "#addsubkey#";

    public static Intent newInstance(Context context, PushData pushData) {
        Bundle args = new Bundle();
        args.putString("uuid", pushData.getUuid());
        args.putString("title", pushData.getTitle());
        args.putString("date", pushData.getDate());
        args.putString("content", pushData.getContents());
        args.putString("provider", pushData.getProvider());

        Intent intent = new Intent(context, PushDetailActivity.class);
        intent.putExtra(BUNDLE, args);
        return intent;
    }

    private AlertDialogFragment.Callbacks mDeleteDialogCallback = new AlertDialogFragment.Callbacks() {
        @Override
        public void onPositiveButtonClicked() {
            Intent result = new Intent();
            result.putExtra("uuid", mUuid);
            setResult(RESULT_OK, result);
            finish();
        }

        @Override
        public void onNegativeButtonClicked() {

        }

        @Override
        public void onBackKeyPressed() {

        }
    };

    private String mUuid = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_deatil);

        LinearLayout mainContainer = findViewById(R.id.activity_container);
        setWheelScreenLayout(mainContainer);

        ImageView wheelRightHandle = findViewById(R.id.wheel_right_handle_image_view);
        ImageView wheelLeftHandle = findViewById(R.id.wheel_left_handle_image_view);
        setWheelHandles(wheelRightHandle, wheelLeftHandle);

        Toolbar toolBar = findViewById(R.id.toolbar);
        toolBar.setTitle(R.string.activity_push_title);
        setSupportActionBar(toolBar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolBar.setNavigationOnClickListener(v -> onBackPressed());

        Bundle args = getIntent().getBundleExtra(BUNDLE);

        mUuid = args.getString("uuid");
        TextView titleTextView = findViewById(R.id.title);
        titleTextView.setText(args.getString("title"));
        TextView dateTextView = findViewById(R.id.date);
        dateTextView.setText(args.getString("date"));
        TextView contentTextView = findViewById(R.id.content);

        String content = args.getString("content", "");
        Markwon.setMarkdown(contentTextView, content);


        Button viewButton = findViewById(R.id.btn_view);
        if (content.contains(TAG_ADD_SUB_KEY)) {
            String str = contentTextView.getText().toString().trim().replace(TAG_ADD_SUB_KEY, "");
            contentTextView.setText(str);
            viewButton.setVisibility(View.VISIBLE);
            viewButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goDigitalKeyActivity();
                }
            });
        } else {
            viewButton.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_push_detail_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case R.id.action_push_delete:
                AlertDialogFragment dialog = AlertDialogFragment.newInstance(
                        getString(R.string.dialog_title_push_data_delete),
                        getString(R.string.dialog_message_push_data_delete));
                dialog.setButtons(
                        getString(R.string.btn_delete),
                        getString(R.string.btn_cancel),
                        mDeleteDialogCallback);
                dialog.show(getSupportFragmentManager(), "delete selected push data");

                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void goDigitalKeyActivity() {
        startActivity(DigitalKeyActivity.newIntent(this));
    }
}
