package com.luxgen.remote.ui.maintenance;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luxgen.remote.R;
import com.luxgen.remote.network.model.maintenance.RepairHistory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RepairHistoryAdapter extends RecyclerView.Adapter {
    private List<RepairHistory> items;
    private LayoutInflater inflater;

    public RepairHistoryAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
        items = new ArrayList<>();
        setHasStableIds(true);
    }

    public void updateItems(final List<RepairHistory> newItems) {
        final List<RepairHistory> oldItems = new ArrayList<>(this.items);
        this.items.clear();
        if (newItems != null) {
            this.items.addAll(newItems);
        }

        DiffUtil.calculateDiff(new DiffUtil.Callback() {
            @Override
            public int getOldListSize() {
                return oldItems.size();
            }

            @Override
            public int getNewListSize() {
                return items.size();
            }

            @Override
            public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                return oldItems.get(oldItemPosition).equals(items.get(newItemPosition));
            }

            @Override
            public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                final RepairHistory oldRepairHistory = oldItems.get(oldItemPosition);
                final RepairHistory newRepairHistory = items.get(newItemPosition);

                return oldRepairHistory.getDate().equals(newRepairHistory.getDate());
            }
        }).dispatchUpdatesTo(this);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_repair_history, parent, false);
        return new RepairHistoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RepairHistoryViewHolder viewHolder = (RepairHistoryViewHolder) holder;
        viewHolder.setItem(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        try {
            return parseToLong(items.get(position).getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return super.getItemId(position);
    }

    private long parseToLong(String time) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.TAIWAN);
        Date date;
        date = formatter.parse(time);
        return date == null ? 0 : date.getTime();
    }
}
