package com.luxgen.remote.ui.push;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.luxgen.remote.R;
import com.luxgen.remote.ui.dialog.AlertDialogFragment;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.ui.dialog.ProgressDialogFragment;
import com.luxgen.remote.model.PushData;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.assistant.GetPushRequestData;
import com.luxgen.remote.network.model.assistant.GetPushResponseData;
import com.luxgen.remote.network.model.assistant.SetPushRequestData;
import com.luxgen.remote.ui.viewmodel.PushListViewModel;
import com.luxgen.remote.util.ObuManager;
import com.luxgen.remote.util.Prefs;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class PushListFragment extends CustomBaseFragment {

    private static final int REQUEST_PUSH_DETAIL = 1807;

    private PushListViewModel mViewModel;
    private PushListAdapter mPushListAdapter;
    private ProgressDialogFragment mPushListDialog;

    private Menu mMenu;
    private ObuManager mObuManager;
    private ArrayList<PushData> mDeletePushDataList;
    private boolean isEditable = false;

    /**
     * 是否為編輯狀態
     */
    public boolean isEditable() {
        return this.isEditable;
    }

    public static PushListFragment newInstance() {
        return new PushListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_push_list, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.list_push_list);
        mPushListAdapter = new PushListAdapter(getLayoutInflater());
        setList(recyclerView);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getPushList();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PUSH_DETAIL && resultCode == RESULT_OK) {
            String uuid = data.getStringExtra("uuid");
            if (mClickData.getUuid().equals(uuid)) {
                deletePushData(mClickData);
            }
        }
    }

    private void setList(RecyclerView recyclerView) {
        Context context = recyclerView.getContext();
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(mPushListAdapter);

        mViewModel = ViewModelProviders.of(this).get(PushListViewModel.class);
        mViewModel.getPushList().observe(this,
                pushDataList -> mPushListAdapter.setItems(pushDataList));
    }

    /*----- Start: About Menu Inflater/Action -----*/
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mMenu = menu;
        setMenu(R.menu.activity_push_list_menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (!isAdded()) {
            return super.onOptionsItemSelected(item);
        }

        switch (item.getItemId()) {
            // 這兩個動只差在有沒有把 isEditable 設定為 true 而已
            case R.id.action_push_edit:
                if (mPushListAdapter.getItemCount() == 0) {
                    break;
                }
                this.isEditable = true;
            case R.id.action_push_clear:
                clearSelected();
                setMenu(R.menu.activity_push_list_edit_menu);
                break;
            case R.id.action_push_select_all:
                selectAllPushItem();
                setMenu(R.menu.activity_push_list_edit_clear_select_menu);
                break;
            case R.id.action_push_delete:

                if (!hasPushItemSelected()) {
                    break;
                }

                AlertDialogFragment dialog = AlertDialogFragment.newInstance(
                        getString(R.string.dialog_title_push_data_delete),
                        getString(R.string.dialog_message_push_data_delete));
                dialog.setButtons(
                        getString(R.string.btn_delete),
                        getString(R.string.btn_cancel),
                        mDeleteDialogCallback);
                dialog.show(getFragmentManager(), "delete selected push data");
                break;
            case R.id.action_push_ok:
                setMenuToDefault();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * 設定 menu 的 layout
     */
    public void setMenu(int resourceId) {
        mMenu.clear();
        getActivity().getMenuInflater().inflate(resourceId, mMenu);
    }

    public void setMenuToDefault() {
        this.isEditable = false;
        clearSelected();
        setMenu(R.menu.activity_push_list_menu);
    }

    private void clearSelected() {
        if (mPushListAdapter.getItemCount() > 0) {
            mPushListAdapter.clearSelect();
            mPushListAdapter.notifyDataSetChanged();
        }
    }

    private void selectAllPushItem() {
        if (mPushListAdapter.getItemCount() > 0) {
            mPushListAdapter.selectAll();
            mPushListAdapter.notifyDataSetChanged();
        }
    }

    public boolean hasPushItemSelected() {
        return mPushListAdapter.getItemCount() > 0 && mPushListAdapter.hasSelectItem();
    }

    private AlertDialogFragment.Callbacks mDeleteDialogCallback = new AlertDialogFragment.Callbacks() {
        @Override
        public void onPositiveButtonClicked() {
            if (!isAdded()) {
                return;
            }
            deletePushData();
        }

        @Override
        public void onNegativeButtonClicked() {

        }

        @Override
        public void onBackKeyPressed() {

        }
    };
    /*----- End: About Menu Inflater/Action -----*/

    /*----- Start: Get Push Data -----*/
    private void getPushList() {
        if (!isAdded()) {
            return;
        }

        UserInfo userInfo = Prefs.getUserInfo(getContext());
        if (userInfo == null) {
            return;
        }

        GetPushRequestData requestData = new GetPushRequestData(userInfo.getKeyUid(), userInfo.getToken());
        AssistantWebApi api = AssistantWebApi.getInstance(getContext());
        api.getPushList(requestData, mPushListener);
    }

    private OnApiListener<GetPushResponseData> mPushListener = new OnApiListener<GetPushResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetPushResponseData responseData) {
            mPushListDialog.dismiss();
            if (responseData.isSuccessful()) {
                ArrayList<PushData> list = responseData.getPushList();
                mPushListAdapter.setItems(list);
                mViewModel.insert(list);

                mObuManager = ObuManager.getInstance(getContext());
                mObuManager.syncReadStatus(list);
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            mPushListDialog.dismiss();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mPushListDialog = ProgressDialogFragment.newInstance(getString(R.string.push_get_loading));
            mPushListDialog.show(getFragmentManager(), "pushListProgressDialog");
        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            mPushListDialog.dismiss();
            showMultiLoginErrorDialog();
        }
    };
    /*----- End: Get Push Data -----*/

    /*----- Start: Change Push Data Status -----*/
    private PushData mClickData;

    public void changePushData(PushData data) {
        if (!isAdded() || getContext() == null) {
            return;
        }

        data.isStatusChanged();
        mClickData = data;
        data.setNeedSyncToServer(true);
        ArrayList<PushData> list = new ArrayList<>();
        list.add(data);
        changePushData(list, false);
    }

    private OnApiListener<ResponseData> getChangeStatusListener(final ArrayList<PushData> list) {
        return new OnApiListener<ResponseData>() {
            @Override
            public void onApiTaskSuccessful(ResponseData responseData) {
                mPushListDialog.dismiss();
                if (responseData.isSuccessful()) {
                    mViewModel.update(list);
                    afterChangePushData();
                    if (isAdded() && getContext() != null && mClickData != null) {
                        startActivityForResult(PushDetailActivity.newInstance(getContext(), mClickData), REQUEST_PUSH_DETAIL);
                    }
                } else {
                    showServerErrorDialog(responseData.getErrorMessage());
                }
            }

            @Override
            public void onApiTaskFailure(String failMessage) {
                mViewModel.update(list);
                mPushListDialog.dismiss();
                showServerErrorDialog(failMessage);
            }

            @Override
            public void onPreApiTask() {
                mPushListDialog = ProgressDialogFragment.newInstance(getString(R.string.push_get_loading));
                mPushListDialog.show(getFragmentManager(), "deletePushListProgressDialog");
            }

            @Override
            public void onApiProgress(long value) {

            }

            @Override
            public void onAuthFailure() {
                mViewModel.update(list);
                mPushListDialog.dismiss();
                showMultiLoginErrorDialog();
            }
        };
    }
    /*----- End: Change Push Data Status -----*/

    private void changePushData(ArrayList<PushData> list, boolean isDelete) {
        UserInfo user = Prefs.getUserInfo(getContext());
        SetPushRequestData requestData = new SetPushRequestData(
                user.getKeyUid(), user.getToken());
        if (isDelete) {
            requestData.setDeleteList(list);
        } else {
            requestData.setReadList(list);
        }

        AssistantWebApi.getInstance(getContext())
                .setPushStatus(requestData,
                        isDelete ? getDeleteListener(list) : getChangeStatusListener(list));
    }

    private void afterChangePushData() {
        mObuManager.syncReadStatus(mPushListAdapter.getItems());
        if (mDeletePushDataList != null) {
            mDeletePushDataList.clear();
        }
        setMenuToDefault();
    }

    /*----- Start: Delete Push Data -----*/
    private void deletePushData() {
        if (mDeletePushDataList == null) {
            mDeletePushDataList = new ArrayList<>();
        }

        mDeletePushDataList.addAll(mPushListAdapter.deleteSelectItems());

        if (mDeletePushDataList.size() <= 0) {
            setMenuToDefault();
            return;
        }

        changePushData(mDeletePushDataList, true);
    }

    private void deletePushData(PushData data) {
        if (mDeletePushDataList == null) {
            mDeletePushDataList = new ArrayList<>();
        }

        mDeletePushDataList.addAll(mPushListAdapter.deleteSelectItems(data));

        changePushData(mDeletePushDataList, true);
    }

    private OnApiListener<ResponseData> getDeleteListener(final ArrayList<PushData> list) {
        return new OnApiListener<ResponseData>() {
            @Override
            public void onApiTaskSuccessful(ResponseData responseData) {
                mPushListDialog.dismiss();
                if (responseData.isSuccessful()) {
                    mViewModel.update(list);
                    afterChangePushData();
                } else {
                    showServerErrorDialog(responseData.getErrorMessage());
                }
            }

            @Override
            public void onApiTaskFailure(String failMessage) {
                mViewModel.update(list);
                mPushListDialog.dismiss();
                showServerErrorDialog(failMessage);
            }

            @Override
            public void onPreApiTask() {
                mPushListDialog = ProgressDialogFragment.newInstance(getString(R.string.push_get_loading));
                mPushListDialog.show(getFragmentManager(), "deletePushListProgressDialog");
            }

            @Override
            public void onApiProgress(long value) {

            }

            @Override
            public void onAuthFailure() {
                mViewModel.update(list);
                mPushListDialog.dismiss();
                showMultiLoginErrorDialog();
            }
        };
    }
    /*----- End: Delete Push Data -----*/


    private ArrayList<PushData> getFakeData() {
        ArrayList<PushData> pushDataArrayList = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            PushData p = new PushData();
            String datebuf = String.format("%02d", i);
            p.setProvider("納智捷汽車");
            p.setDate("2018/03/" + datebuf + " 11:11:11");
            p.setTitle("Item " + i);
            p.setContents("http://www.google.com " + i);
            pushDataArrayList.add(p);
        }
        return pushDataArrayList;
    }
}
