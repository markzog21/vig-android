package com.luxgen.remote.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.ui.dialog.ProgressDialogFragment;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.UserWebApi;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.user.SetAccountRequestData;
import com.luxgen.remote.util.Prefs;

import org.json.JSONObject;

import static com.luxgen.remote.ui.login.LoginActivity.FRAGMENT_TAG_LOGIN_REGISTER_PASSWORD;

public class LoginRegisterFragment extends CustomBaseFragment {

    private final static String TAG = LoginRegisterFragment.class.getSimpleName();

    private final static int REQUEST_GOOGLE_SIGN_IN = 9528;
    public final static int LOGIN_TYPE_EMAIL = 0;
    public final static int LOGIN_TYPE_FACEBOOK = 1;
    public final static int LOGIN_TYPE_GOOGLE = 2;

    private EditText mEmailEditText;
    private View mFacebookSingInButtonMaskView;
    private AppCompatCheckBox mPrivacyPolicyCheckBox;
    private ProgressDialogFragment mCheckRepeatProgressDialogFragment;

    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager mFacebookCallbackManager;

    private int mLoginType = LOGIN_TYPE_EMAIL;
    private String mEmail;
    private String mSocialId;
    private String mSocialAlias;

    public static LoginRegisterFragment newInstance() {
        return newInstance("", LOGIN_TYPE_EMAIL, "", "");
    }

    public static LoginRegisterFragment newInstance(String email, int loginType, String socialId, String socialAlias) {
        Bundle args = new Bundle();
        args.putString("email", email);
        args.putInt("loginType", loginType);
        args.putString("socialId", socialId);
        args.putString("socialAlias", socialAlias);

        LoginRegisterFragment fragment = new LoginRegisterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private OnApiListener<ResponseData> mCheckRepeatListener = new OnApiListener<ResponseData>() {
        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            mCheckRepeatProgressDialogFragment.dismiss();
            mGoogleSignInClient.revokeAccess();
            LoginManager.getInstance().logOut();
            if (responseData.isSuccessful()) {
                showRegisterPasswordScreen();
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            mCheckRepeatProgressDialogFragment.dismiss();
            showServerErrorDialog(failMessage);
            mGoogleSignInClient.revokeAccess();
            LoginManager.getInstance().logOut();
        }

        @Override
        public void onPreApiTask() {
            mCheckRepeatProgressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.login_register_checking_repeat));
            mCheckRepeatProgressDialogFragment.show(getFragmentManager(), "checkRepeatProgressDialog");
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            // TODO: check if need this
            mCheckRepeatProgressDialogFragment.dismiss();
            showMultiLoginErrorDialog();
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initialGoogleSignInClient();
        initialFacebookCallbackManager();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login_register, container, false);

        mEmailEditText = v.findViewById(R.id.edit_email);
        mPrivacyPolicyCheckBox = v.findViewById(R.id.privacy_policy_check_box);
        mPrivacyPolicyCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mFacebookSingInButtonMaskView.setVisibility(View.GONE);
                } else {
                    mFacebookSingInButtonMaskView.setVisibility(View.VISIBLE);
                }
            }
        });

        Button googleSignInButton = v.findViewById(R.id.google_sign_in);
        googleSignInButton.setAllCaps(false);
        googleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPrivacyPolicyCheckBox.isChecked()) {
                    startActivityForResult(mGoogleSignInClient.getSignInIntent(), REQUEST_GOOGLE_SIGN_IN);
                } else {
                    if (isAdded()) {
                        showConfirmDialog(getString(R.string.dialog_title_remind), getString(R.string.dialog_message_privacy_policy));
                    }
                }
            }
        });

        LoginButton facebookSignInButton = v.findViewById(R.id.fb_sign_in);
        facebookSignInButton.setReadPermissions("email");
        facebookSignInButton.setFragment(this);
        facebookSignInButton.registerCallback(mFacebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookSignInResult(loginResult);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        mFacebookSingInButtonMaskView = v.findViewById(R.id.fb_sign_in_mask);
        mFacebookSingInButtonMaskView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showConfirmDialog(getString(R.string.dialog_title_remind), getString(R.string.dialog_message_privacy_policy));
            }
        });

        Button registerButton = v.findViewById(R.id.btn_register);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPrivacyPolicyCheckBox.isChecked()) {
                    mLoginType = LOGIN_TYPE_EMAIL;
                    register();
                } else {
                    if (isAdded()) {
                        showConfirmDialog(getString(R.string.dialog_title_remind), getString(R.string.dialog_message_privacy_policy));
                    }
                }
            }
        });

        AppCompatTextView privacyPolicyTextView = v.findViewById(R.id.privacy_policy_text_view);
        privacyPolicyTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAdded()) {
                    showConfirmDialog(getString(R.string.dialog_title_privacy_polocy), getString(R.string.login_register_privacy_policy_content));
                }
            }
        });

        TextView backTextView = v.findViewById(R.id.txt_back);
        backTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popFragment();
            }
        });

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (fromSocialLogin()) {
            register();
        }
    }

    private boolean fromSocialLogin() {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        mEmail = args.getString("email", null);
        mLoginType = args.getInt("loginType", LOGIN_TYPE_EMAIL);
        mSocialId = args.getString("socialId");
        mSocialAlias = args.getString("socialAlias");

        return !TextUtils.isEmpty(mEmail) && mLoginType != LOGIN_TYPE_EMAIL;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_GOOGLE_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleGoogleSignInResult(task);
        }
    }

    private void handleGoogleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            mEmail = account.getEmail();
            mSocialId = account.getId();
            mSocialAlias = account.getDisplayName();
            mLoginType = LOGIN_TYPE_GOOGLE;
            mEmailEditText.setText(mEmail);
            register();
        } catch (ApiException e) {
            LogCat.w("signInResult:failed code=" + e.getStatusCode());
        }
    }

    private void initialGoogleSignInClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getContext(), gso);
    }

    private void handleFacebookSignInResult(LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        mEmail = object.optString("email", "");
                        mSocialId = object.optString("id", "");
                        mSocialAlias = object.optString("name", "");
                        mLoginType = LOGIN_TYPE_FACEBOOK;
                        mEmailEditText.setText(mEmail);
                        register();
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "email,id,name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void initialFacebookCallbackManager() {
        mFacebookCallbackManager = CallbackManager.Factory.create();
    }

    private boolean validate() {
        if (mLoginType == LOGIN_TYPE_EMAIL) {
            String email = mEmailEditText.getText().toString().trim();

            if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                showWrongAccountFormatDialog();
                return false;
            }
        }

        return true;
    }

    private void register() {
        if (validate()) {
            doRegister();
        }
    }

    private SetAccountRequestData buildCheckRepeatData(String email) {
        SetAccountRequestData data = new SetAccountRequestData();
        data.setAccount(email);

        return data;
    }

    private void doRegister() {
        String email = mEmail;
        if (mLoginType == LOGIN_TYPE_EMAIL) {
            email = mEmailEditText.getText().toString().trim();
        }

        SetAccountRequestData data = buildCheckRepeatData(email);

        UserWebApi userWebApi = UserWebApi.getInstance(getActivity());
        userWebApi.checkRepeat(data, mCheckRepeatListener);
    }

    private void showRegisterPasswordScreen() {
        String email = mEmail;
        if (mLoginType == LOGIN_TYPE_EMAIL) {
            email = mEmailEditText.getText().toString().trim();
        }
        String deviceId = Prefs.getDeviceId(getContext());

        ((LoginActivity) getActivity()).navigate(this,
                LoginRegisterPasswordFragment.newInstance(email, deviceId, mLoginType, mSocialId, mSocialAlias),
                FRAGMENT_TAG_LOGIN_REGISTER_PASSWORD, true);
    }

    private void showWrongAccountFormatDialog() {
        showConfirmDialog(getString(R.string.login_register_wrong_format_title), getString(R.string.login_register_wrong_format_message));
    }
}
