package com.luxgen.remote.ui.push;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomAppCompatActivity;
import com.luxgen.remote.model.PushData;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.model.assistant.SetPushRequestData;
import com.luxgen.remote.util.Prefs;

public class PushListActivity extends CustomAppCompatActivity implements PushListViewHolder.PushListListener {

    public static Intent newIntent(Context context) {
        return new Intent(context, PushListActivity.class);
    }

    private SetPushRequestData mPushStausRequestData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_list);

        if (savedInstanceState != null) {
            return;
        }

        LinearLayout mainContainer = findViewById(R.id.activity_container);
        setWheelScreenLayout(mainContainer);

        ImageView wheelRightHandle = findViewById(R.id.wheel_right_handle_image_view);
        ImageView wheelLeftHandle = findViewById(R.id.wheel_left_handle_image_view);
        setWheelHandles(wheelRightHandle, wheelLeftHandle);

        Toolbar toolBar = findViewById(R.id.toolbar);
        toolBar.setTitle(R.string.activity_push_title);
        setSupportActionBar(toolBar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolBar.setNavigationOnClickListener(v -> onBackPressed());

        showPushList();
        UserInfo userInfo = Prefs.getUserInfo(this);
        if (userInfo == null) {
            return;
        }
        mPushStausRequestData = new SetPushRequestData(userInfo.getKeyUid(), userInfo.getToken());
    }

    @Override
    public void onBackPressed() {
        if (isEditable()) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if (fragment instanceof PushListFragment) {
                ((PushListFragment) fragment).setMenuToDefault();
            }
            return;
        }
        super.onBackPressed();
    }

    private void showPushList() {
        PushListFragment firstFragment = PushListFragment.newInstance();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, firstFragment).commit();
    }

    @Override
    public boolean isEditable() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        return fragment instanceof PushListFragment && ((PushListFragment) fragment).isEditable();
    }

    @Override
    public void select(boolean isChecked, PushData data) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof PushListFragment) {
            ((PushListFragment) fragment).setMenu(((PushListFragment) fragment).hasPushItemSelected() ?
                    R.menu.activity_push_list_edit_has_select_menu : R.menu.activity_push_list_edit_menu);
        }
    }

    @Override
    public void click(PushData data) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof PushListFragment) {
            ((PushListFragment) fragment).changePushData(data);
        }
    }
}
