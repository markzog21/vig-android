package com.luxgen.remote.ui.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.ui.dialog.ProgressDialogFragment;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.UserWebApi;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.user.SetIkeyUidRequestData;
import com.luxgen.remote.network.model.user.SetPasswordRequestData;
import com.luxgen.remote.util.Prefs;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;

public class SettingsUserInfoFragment extends CustomBaseFragment {

    private EditText mEmailEditText;
    private EditText mCurrentPasswordEditText, mNewPasswordEditText, mConfirmPasswordEditText;
    private EditText mPhoneEditText;
    private View mCurrentPasswordContainerView, mPasswordEditorContainerView;
    private boolean mForgetPassword = false;

    private UserInfo mUserInfo;

    private ProgressDialogFragment mProgressDialogFragment;

    private void dismissDialog() {
        if (isAdded() && mProgressDialogFragment != null) {
            mProgressDialogFragment.dismiss();
        }
    }

    private OnApiListener<ResponseData> mSetPasswordRequestListener = new OnApiListener<ResponseData>() {
        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            if (responseData.isSuccessful()) {
                String phone = mPhoneEditText.getText().toString().trim();
                String email = mEmailEditText.getText().toString().trim();
                String password = mConfirmPasswordEditText.getText().toString().trim();

                boolean isPhoneChanged = isPhoneChanged(phone);
                if (isEmailChanged(email) || isPhoneChanged) {
                    doChangeContactInfo(email, isPhoneChanged ? phone : null);
                } else {
                    dismissDialog();

                    saveUserData(email, password, phone);
                    Toast.makeText(getContext(), R.string.settings_userinfo_modification_success, Toast.LENGTH_SHORT).show();
                    if (mForgetPassword) {
                        getActivity().setResult(RESULT_OK);
                        getActivity().finish();
                    } else {
                        popFragment();
                    }
                }
            } else {
                dismissDialog();
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissDialog();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mProgressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.settings_userinfo_saving));
            mProgressDialogFragment.show(getFragmentManager(), "setPasswordProgressDialog");
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            dismissDialog();
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<ResponseData> mSetIkeyUidRequestListener = new OnApiListener<ResponseData>() {
        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            dismissDialog();
            if (responseData.isSuccessful()) {
                String email = mEmailEditText.getText().toString().trim();
                String password = mConfirmPasswordEditText.getText().toString().trim();
                String phone = mPhoneEditText.getText().toString().trim();

                saveUserData(email, password, phone);
                Toast.makeText(getContext(), R.string.settings_userinfo_modification_success, Toast.LENGTH_SHORT).show();
                popFragment();
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissDialog();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            if (mProgressDialogFragment == null) {
                mProgressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.settings_userinfo_saving));
                mProgressDialogFragment.show(getFragmentManager(), "setPasswordProgressDialog");
            }
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            dismissDialog();
            showMultiLoginErrorDialog();
        }
    };

    public static SettingsUserInfoFragment newInstance(boolean forgetPassword) {
        Bundle args = new Bundle();
        args.putBoolean("forgetPassword", forgetPassword);
        SettingsUserInfoFragment f = new SettingsUserInfoFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadUserData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        mForgetPassword = args.getBoolean("forgetPassword");

        getActivity().setTitle(R.string.settings_userinfo_title);

        View v = inflater.inflate(R.layout.fragment_settings_userinfo, container, false);

        mEmailEditText = v.findViewById(R.id.userinfo_email);
        mEmailEditText.setText(getUserEmail());
        if (mForgetPassword) {
            mEmailEditText.setEnabled(false);
        }

        mCurrentPasswordContainerView = v.findViewById(R.id.userinfo_current_password_container);
        mCurrentPasswordContainerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentPasswordContainerView.setVisibility(View.GONE);
                mPasswordEditorContainerView.setVisibility(View.VISIBLE);
                mCurrentPasswordEditText.setEnabled(true);
                mCurrentPasswordEditText.setText(null);
                mCurrentPasswordEditText.requestFocus();
            }
        });
        mCurrentPasswordEditText = v.findViewById(R.id.userinfo_current_password);
        mCurrentPasswordEditText.setText(getUserPassword());
        mCurrentPasswordEditText.setEnabled(false);

        mPasswordEditorContainerView = v.findViewById(R.id.userinfo_password_editor_container);
        mNewPasswordEditText = v.findViewById(R.id.userinfo_new_password);
        mConfirmPasswordEditText = v.findViewById(R.id.userinfo_confirm_password);
        mConfirmPasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                if (!str.equals(mNewPasswordEditText.getText().toString().trim())) {
                    mConfirmPasswordEditText.setError(getString(R.string.settings_userinfo_password_error_not_match));
                } else {
                    mConfirmPasswordEditText.setError(null);
                }
            }
        });

        mPhoneEditText = v.findViewById(R.id.userinfo_phone);
        if (isDMSMember()) {
            v.findViewById(R.id.userinfo_phone_container).setVisibility(View.GONE);
        } else {
            mPhoneEditText.setText(getUserPhone());
        }

        if (mForgetPassword) {
            mCurrentPasswordContainerView.setVisibility(View.GONE);
            mPasswordEditorContainerView.setVisibility(View.VISIBLE);
            mCurrentPasswordEditText.setFocusable(false);
            mNewPasswordEditText.requestFocus();
            v.findViewById(R.id.userinfo_phone_container).setVisibility(View.GONE);
        }

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.settings_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                if (validate()) {
                    String email = mEmailEditText.getText().toString().trim();
                    String currentPassword = mCurrentPasswordEditText.getText().toString().trim();
                    String confirmPassword = mConfirmPasswordEditText.getText().toString().trim();
                    String phone = mPhoneEditText.getText().toString().trim();
                    boolean isPhoneChanged = isPhoneChanged(phone);

                    if (isPasswordChanged(currentPassword, confirmPassword)) {
                        doChangePassword(currentPassword, confirmPassword);
                    } else if (isEmailChanged(email) || isPhoneChanged) {
                        doChangeContactInfo(email, isPhoneChanged ? phone : null);
                    }
                }

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isPasswordMatchRule(String password) {
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=\\S+$).{8,12}$";
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);

        return matcher.matches();
    }

    private boolean validate() {
        String email = mEmailEditText.getText().toString().trim();
        if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmailEditText.requestFocus();
            mEmailEditText.setError(getString(R.string.settings_userinfo_input_format_error));
            return false;
        }

        String oldPassword = mCurrentPasswordEditText.getText().toString().trim();
        String newPassword = mNewPasswordEditText.getText().toString().trim();
        String confirmPassword = mConfirmPasswordEditText.getText().toString().trim();

        if (!newPassword.isEmpty() || !confirmPassword.isEmpty()) {
            if (newPassword.isEmpty()) {
                mNewPasswordEditText.requestFocus();
                mNewPasswordEditText.setError(getString(R.string.settings_userinfo_password_empty));
                return false;
            }

            if (confirmPassword.isEmpty()) {
                mConfirmPasswordEditText.requestFocus();
                mConfirmPasswordEditText.setError(getString(R.string.settings_userinfo_password_empty));
                return false;
            }

            if (!newPassword.equals(confirmPassword)) {
                mConfirmPasswordEditText.requestFocus();
                mConfirmPasswordEditText.selectAll();
                mConfirmPasswordEditText.setError(getString(R.string.settings_userinfo_password_error_not_match));
                return false;
            }

            if (oldPassword.isEmpty()) {
                mCurrentPasswordEditText.requestFocus();
                mCurrentPasswordEditText.setError(getString(R.string.settings_userinfo_password_empty));
                return false;
            }

            if (!isPasswordMatchRule(newPassword)) {
                showWrongPasswordFormatDialog();
                return false;
            }
        }

        if (!isDMSMember()) {
            String phone = mPhoneEditText.getText().toString().trim();
            if (TextUtils.isEmpty(phone) || !Patterns.PHONE.matcher(phone).matches()) {
                mPhoneEditText.requestFocus();
                mPhoneEditText.setError(getString(R.string.settings_userinfo_input_format_error));
                return false;
            }
        }

        return true;
    }

    private boolean isPasswordChanged(String oldPassword, String newPassword) {
        return !TextUtils.isEmpty(oldPassword) && !TextUtils.isEmpty(newPassword);
    }

    private boolean isPhoneChanged(String phone) {
        if (isDMSMember()) {
            return false;
        }
        return !mUserInfo.getPhone().equals(phone);
    }

    private boolean isEmailChanged(String email) {
        return !mUserInfo.getEmail().equals(email);
    }

    private SetPasswordRequestData buildPasswordRequestData(String ikeyUid, String ikeyToken, String oldPassword, String newPassword) {
        SetPasswordRequestData data = new SetPasswordRequestData();
        data.setUid(ikeyUid);
        data.setKeyToken(ikeyToken);
        data.setPassword(oldPassword, newPassword);

        return data;
    }

    private void doChangePassword(String oldPassword, String newPassword) {
        String ikeyUid = mUserInfo.getKeyUid();
        String ikeyToken = mUserInfo.getToken();

        SetPasswordRequestData data = buildPasswordRequestData(ikeyUid, ikeyToken, oldPassword, newPassword);

        UserWebApi userWebApi = UserWebApi.getInstance(getActivity());
        userWebApi.setPassword(data, mSetPasswordRequestListener);
    }

    private SetIkeyUidRequestData buildIkeyUidRequestData(String ikeyUid, String ikeyToken, String email, String phone) {
        SetIkeyUidRequestData data = new SetIkeyUidRequestData(ikeyUid, ikeyToken, email);
        if (phone != null) {
            data.setPhone(phone);
        }

        return data;
    }

    private void doChangeContactInfo(String email, String phone) {
        String ikeyUid = mUserInfo.getKeyUid();
        String ikeyToken = mUserInfo.getToken();

        SetIkeyUidRequestData data = buildIkeyUidRequestData(ikeyUid, ikeyToken, email, phone);

        UserWebApi userWebApi = UserWebApi.getInstance(getActivity());
        userWebApi.setIkeyUid(data, mSetIkeyUidRequestListener);
    }

    private String getUserPassword() {
        return mUserInfo.getPassword();
    }

    private String getUserEmail() {
        return mUserInfo.getEmail();
    }

    private String getUserPhone() {
        return mUserInfo.getPhone();
    }

    private boolean isDMSMember() {
        return mUserInfo.isDMS();
    }

    private void loadUserData() {
        mUserInfo = Prefs.getUserInfo(getContext());
    }

    private void saveUserData(String email, String password, String phone) {
        mUserInfo.setEmail(email);
        if (!TextUtils.isEmpty(password)) {
            mUserInfo.setPassword(password);
        }
        if (!TextUtils.isEmpty(phone)) {
            mUserInfo.setPhone(phone);
        }

        if (getContext() != null) {
            Prefs.setUserInfo(getContext(), mUserInfo);
        }
    }

    private void showWrongPasswordFormatDialog() {
        showConfirmDialog(getString(R.string.login_register_password_error_rule_title),
                getString(R.string.login_register_password_error_rule_message));
    }
}
