package com.luxgen.remote.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.luxgen.remote.custom.LogCat;

import java.util.ArrayList;
import java.util.List;

public class ItemPickerDialogFragment extends DialogFragment {
    public static final String TAG = ItemPickerDialogFragment.class.getSimpleName();

    public static class Item {
        private String title;
        private int intValue;
        private String stringValue;

        private static final String KEY_TITLE = "title";
        private static final String KEY_INT_VALUE = "intValue";
        private static final String KEY_STRING_VALUE = "stringValue";

        public Item(String title, int value) {
            this.title = title;
            this.intValue = value;
        }

        public Item(String title, String value) {
            this.title = title;
            this.stringValue = value;
        }

        public Item(Bundle bundle) {
            title = bundle.getString(KEY_TITLE, null);
            intValue = bundle.getInt(KEY_INT_VALUE, 0);
            stringValue = bundle.getString(KEY_STRING_VALUE, null);
        }

        public Bundle getValuesBundle() {
            Bundle bundle = new Bundle();

            bundle.putString(KEY_TITLE, title);
            bundle.putInt(KEY_INT_VALUE, intValue);
            if (stringValue != null) {
                bundle.putString(KEY_STRING_VALUE, stringValue);
            }

            return bundle;
        }

        public String getTitle() {
            return title;
        }

        public int getIntValue() {
            return intValue;
        }

        public String getStringValue() {
            return stringValue;
        }

        public static Bundle bundleOfItems(List<Item> items) {
            int itemCount = items.size();
            ArrayList<Bundle> itemBundles = new ArrayList<>();
            for (int i = 0; i < itemCount; ++i) {
                itemBundles.add(items.get(i).getValuesBundle());
            }

            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(ARG_ITEMS, itemBundles);
            return bundle;
        }

        public static ArrayList<Item> itemsFromBundle(Bundle bundle) {
            ArrayList<Bundle> itemBundles = bundle.getParcelableArrayList(ARG_ITEMS);
            ArrayList<Item> items = new ArrayList<>();
            for (Bundle itemBundle : itemBundles) {
                items.add(new Item(itemBundle));
            }
            return items;
        }
    }

    public interface OnItemSelectedListener {
        void onItemSelected(ItemPickerDialogFragment fragment, Item item, int index);
    }

    private static final String ARG_TITLE = "ARG_TITLE";
    private static final String ARG_ITEMS = "ARG_ITEMS";
    private static final String ARG_SELECTED_INDEX = "ARG_SELECTED_INDEX";

    public static ItemPickerDialogFragment newInstance(String title, ArrayList<Item> items, int selectedIndex) {
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        args.putBundle(ARG_ITEMS, Item.bundleOfItems(items));
        args.putInt(ARG_SELECTED_INDEX, selectedIndex);

        ItemPickerDialogFragment fragment = new ItemPickerDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private String title;
    private ArrayList<Item> items;
    private int selectedIndex;
    private OnItemSelectedListener onItemSelectedListener;

    public ItemPickerDialogFragment() {
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(ARG_SELECTED_INDEX, selectedIndex);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        if (args != null) {
            title = args.getString(ARG_TITLE, "Dialog");
            items = Item.itemsFromBundle(args.getBundle(ARG_ITEMS));
            selectedIndex = args.getInt(ARG_SELECTED_INDEX, -1);
        }

        if (savedInstanceState != null) {
            selectedIndex = savedInstanceState.getInt(ARG_SELECTED_INDEX, selectedIndex);
        }

        String[] itemTitles = getItemTitlesArray();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LogCat.w("OK button clicked");

                        Activity activity = getActivity();
                        if (activity instanceof OnItemSelectedListener) {
                            if (0 <= selectedIndex && selectedIndex < items.size()) {
                                Item item = items.get(selectedIndex);
                                OnItemSelectedListener listener = (OnItemSelectedListener) activity;
                                listener.onItemSelected(ItemPickerDialogFragment.this, item, selectedIndex);
                            }
                        } else if (onItemSelectedListener != null) {
                            if (0 <= selectedIndex && selectedIndex < items.size()) {
                                Item item = items.get(selectedIndex);
                                onItemSelectedListener.onItemSelected(ItemPickerDialogFragment.this, item, selectedIndex);
                            }
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LogCat.w("Cancel button clicked");
                    }
                })
                .setSingleChoiceItems(itemTitles, selectedIndex, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LogCat.w("User clicked item with index " + which);
                        selectedIndex = which;
                    }
                });

        return builder.create();
    }

    private String[] getItemTitlesArray() {
        final int itemCount = items.size();
        String[] itemTitles = new String[itemCount];
        for (int i = 0; i < itemCount; ++i) {
            itemTitles[i] = items.get(i).getTitle();
        }
        return itemTitles;
    }

    public void setOnItemSelectedListener(OnItemSelectedListener listener) {
        onItemSelectedListener = listener;
    }
}
