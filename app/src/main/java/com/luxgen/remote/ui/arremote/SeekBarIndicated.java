package com.luxgen.remote.ui.arremote;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.luxgen.remote.R;

public class SeekBarIndicated extends FrameLayout implements SeekBar.OnSeekBarChangeListener {

    private ViewGroup mIndicatorContainer;
    private RelativeLayout mSeekbarContainer;
    private TextView mProgressTextView;
    private AppCompatSeekBar mSeekBar;
    private AppCompatSeekBar mSeekBarInvisible;
    private TextView mMinValTextView;
    private String mMinValueBaseText;
    private TextView mMaxValTextView;
    private String mMaxValueBaseText;

    private int mSeekBarMin = 0;

    private SeekBar.OnSeekBarChangeListener mOnSeekBarChangeListener;
    private TextProvider mTextProviderIndicator;

    private int mMeasuredWidth;

    public SeekBarIndicated(Context context) {
        super(context);
        if (!isInEditMode()) {
            init(context);
        }
    }

    public SeekBarIndicated(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            init(context, attrs, 0);
        }
    }

    public SeekBarIndicated(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            init(context, attrs, defStyleAttr);
        }
    }

    private void init(Context context) {
        init(context, null, 0);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        View view = LayoutInflater.from(context).inflate(R.layout.seekbar_indicated, this);
        mSeekbarContainer = view.findViewById(R.id.seekbar_container);
        mIndicatorContainer = view.findViewById(R.id.seekbar_indicator_container);
        mProgressTextView = view.findViewById(R.id.txt_seekbar_indicated_progress);
        mSeekBarInvisible = view.findViewById(R.id.seekbar_invisible);
        mSeekBar = view.findViewById(R.id.seekbar);
        mMinValTextView = view.findViewById(R.id.min_val);
        mMaxValTextView = view.findViewById(R.id.max_val);

        mSeekBar.setOnSeekBarChangeListener(this);
        mSeekBarInvisible.setOnSeekBarChangeListener(this);
        mProgressTextView.setText(String.valueOf(mSeekBar.getProgress()));

        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mMeasuredWidth = mSeekBar.getWidth() - mSeekBar.getPaddingLeft() - mSeekBar.getPaddingRight();

                mSeekBarInvisible.setPadding(mSeekBarInvisible.getPaddingLeft(),
                        mSeekBarInvisible.getPaddingTop(), mSeekBarInvisible.getPaddingRight(),
                        mSeekBarInvisible.getPaddingBottom() + mIndicatorContainer.getHeight() - mSeekBarInvisible.getHeight());

                mSeekBar.setPadding(mSeekBar.getPaddingLeft(),
                        mSeekBar.getPaddingTop()/* + mIndicatorContainer.getHeight()*/,
                        mSeekBar.getPaddingRight(), mSeekBar.getPaddingBottom());

                setIndicator();

                getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        setIndicator();
        if (seekBar.equals(mSeekBar)) {
            mSeekBarInvisible.setProgress(progress);
        } else if (seekBar.equals(mSeekBarInvisible)) {
            mSeekBar.setProgress(progress);
        }
        if (mOnSeekBarChangeListener != null) {
            mOnSeekBarChangeListener.onProgressChanged(seekBar, progress, fromUser);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        if (mOnSeekBarChangeListener != null) {
            mOnSeekBarChangeListener.onStartTrackingTouch(seekBar);
        }
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (mOnSeekBarChangeListener != null) {
            mOnSeekBarChangeListener.onStopTrackingTouch(seekBar);
        }
    }

    private void setIndicator() {
        String val;
        if (mTextProviderIndicator != null) {
            val = mTextProviderIndicator.provideText(getProgress());
        } else {
            val = String.valueOf(getProgress());
        }
        mProgressTextView.setText(val.replace(".0", ""));

        Rect padding = new Rect();
        mSeekBar.getThumb().getPadding(padding);

        int thumbPosX = mSeekBar.getPaddingLeft() + mSeekbarContainer.getPaddingLeft()
                + mMeasuredWidth
                * mSeekBar.getProgress()
                / mSeekBar.getMax();
        thumbPosX = (int) Math.ceil(thumbPosX);
        mIndicatorContainer.setX(thumbPosX - (int) Math.ceil(mIndicatorContainer.getWidth() / 2));
    }

    public void setMax(int max) {
        mSeekBar.setMax((max - mSeekBarMin) * 2);
        mSeekBarInvisible.setMax((max - mSeekBarMin) * 2);
        setFormattedString(mMaxValTextView, mMaxValueBaseText, max);
    }

    public void setMin(int min) {
        mSeekBarMin = min;
        setFormattedString(mMinValTextView, mMinValueBaseText, min);
    }

    public void setValue(float value) {
        int val = ((int) ((value - mSeekBarMin) * 2));
        mSeekBar.setProgress(val);
        mSeekBarInvisible.setProgress(val);
        setIndicator();
    }

    public void setOnSeekBarChangeListener(SeekBar.OnSeekBarChangeListener onSeekBarChangeListener) {
        mOnSeekBarChangeListener = onSeekBarChangeListener;
    }

    public void setTextProviderIndicator(TextProvider textProviderIndicator) {
        mTextProviderIndicator = textProviderIndicator;
    }

    public float getProgress() {
        int unsignedMin = mSeekBarMin < 0 ? mSeekBarMin * -1 : mSeekBarMin;

        return mSeekBar.getProgress() / 2f + unsignedMin;
    }

    public void setMaxValBaseText(String mMaxValueBaseText) {
        this.mMaxValueBaseText = mMaxValueBaseText;
    }

    public void setMinValBaseText(String mMinValueBaseText) {
        this.mMinValueBaseText = mMinValueBaseText;
    }

    private void setFormattedString(TextView textView, String baseText, Object value) {
        try {
            textView.setText(String.format(baseText, value));
        } catch (Exception e) {
            textView.setText(String.valueOf(value));
        }
    }

    public interface TextProvider {
        String provideText(float progress);
    }
}
