package com.luxgen.remote.ui.wheel;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomFragmentsAppCompatActivity;
import com.luxgen.remote.util.Prefs;

public class WheelActivity extends CustomFragmentsAppCompatActivity {

    private SharedPreferences mSharedPreferences;
    private boolean mIsOnRight = true;

    public static Intent newIntent(Context context) {
        return new Intent(context, WheelActivity.class);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_wheel;
    }

    @Override
    protected void setupTitle(String tag) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mSharedPreferences == null) {
            mSharedPreferences = getSharedPreferences("wheelmenu", Context.MODE_PRIVATE);
        }

        show(WheelMenuFragment.newInstance(true));
        bindCaService();
    }

    @Override
    protected void onDestroy() {
        unbindCaService();
        super.onDestroy();
    }

    private boolean handleBackPressed(FragmentManager fm) {
        if (fm.getFragments() != null) {
            for (Fragment frag : fm.getFragments()) {
                if (frag != null && frag.isVisible() && frag instanceof WheelMenuFragment) {
                    if (((WheelMenuFragment) frag).onBackPressed()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (!handleBackPressed(getSupportFragmentManager())) {
            super.onBackPressed();
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, R.anim.anim_activity_wheel_out);
    }

    @Override
    protected void onResume() {
        super.onResume();

        boolean setOnRight = isOnRight();
        if (mIsOnRight != setOnRight) {
            mIsOnRight = setOnRight;

            popFragment();
            show(WheelMenuFragment.newInstance(mIsOnRight));
        }
    }

    private boolean isOnRight() {
        if (mSharedPreferences == null) {
            mSharedPreferences = getSharedPreferences("wheelmenu", Context.MODE_PRIVATE);
        }

        String mKeyUid = Prefs.getKeyUid(this);

        return mSharedPreferences.getBoolean(mKeyUid + "_onRight", true);
    }
}
