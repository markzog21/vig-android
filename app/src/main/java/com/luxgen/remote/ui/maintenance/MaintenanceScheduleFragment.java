package com.luxgen.remote.ui.maintenance;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatRadioButton;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.luxgen.remote.R;
import com.luxgen.remote.ui.dialog.ProgressDialogFragment;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.maintenance.GetRepairRequestData;
import com.luxgen.remote.network.model.maintenance.GetRepairResponseData;
import com.luxgen.remote.util.NetUtils;
import com.luxgen.remote.util.Prefs;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Objects;

public class MaintenanceScheduleFragment extends BackStackFragment {

    private EditText mPlateEditText;
    private EditText mMileageEditText;
    private AppCompatRadioButton mScheduledCheckBox, mRepairCheckBox;
    private EditText mNotesEditText;
    private String[] mCountries;
    private String mPlate, mMileage;
    private ProgressDialogFragment mProgressDialogFragment;
    private boolean mIsVisibleToUser = false;
    private Button mNextButton;

    private void dismissDialog() {
        if (isAdded() && mProgressDialogFragment != null) {
            mProgressDialogFragment.dismiss();
        }
    }

    private OnApiListener<GetRepairResponseData> mOnApiListener = new OnApiListener<GetRepairResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetRepairResponseData responseData) {
            dismissDialog();
            if (responseData.isSuccessful()) {
                setMileage(responseData.getMile());
                setCountries(responseData.getCountries());
            } else {
                mNextButton.setEnabled(false);
                showServerErrorDialog(responseData.getErrorCode(), responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissDialog();
            // showServerErrorDialog(failMessage);
            if (!NetUtils.isConnectToInternet(getContext())) {
                showServerErrorDialog();
            }
        }

        @Override
        public void onPreApiTask() {
            mProgressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.maintenance_fetching_data));
            mProgressDialogFragment.show(getFragmentManager(), "getRepairDataDialog");
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            dismissDialog();
            showMultiLoginErrorDialog();
        }
    };

    public static MaintenanceScheduleFragment newInstance(String carNo) {
        MaintenanceScheduleFragment f = new MaintenanceScheduleFragment();

        if (carNo != null) {
            Bundle args = new Bundle();
            args.putString("carNo", carNo);
            f.setArguments(args);
        }

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_maintenance_schedule, container, false);

        mPlateEditText = v.findViewById(R.id.plate);

        mMileageEditText = v.findViewById(R.id.mileage);
        mMileageEditText.addTextChangedListener(new TextWatcher() {
            boolean isEditing;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (isEditing) return;
                isEditing = true;

                String str = s.toString().replaceAll("[^\\d]", "");
                if (!TextUtils.isEmpty(str)) {
                    long s1 = Long.parseLong(str);

                    NumberFormat nf2 = NumberFormat.getInstance(Locale.ENGLISH);
                    ((DecimalFormat) nf2).applyPattern("###,###,###");
                    s.replace(0, s.length(), nf2.format(s1));
                }

                isEditing = false;
            }
        });

        mScheduledCheckBox = v.findViewById(R.id.scheduled);
        mRepairCheckBox = v.findViewById(R.id.repair);

        mNotesEditText = v.findViewById(R.id.notes);

        mNextButton = v.findViewById(R.id.btn_next);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetUtils.isConnectToInternet(Objects.requireNonNull(getContext()))) {
                    continueStep2();
                } else {
                    showServerErrorDialog();
                }

            }
        });

        if (mIsVisibleToUser) {
            initialDataField();
            getRepairData();
        }

        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        mIsVisibleToUser = isVisibleToUser;
        if (isVisibleToUser && isAdded()) {
            initialDataField();
            getRepairData();
        }
    }

    private void startFragment(Fragment fragment) {
        ((MaintenanceActivity) getActivity()).startFragmentInHost(fragment);
    }

    private void initialDataField() {
        mScheduledCheckBox.setChecked(true);
        mRepairCheckBox.setChecked(false);
        mNotesEditText.setText("");
    }

    private void continueStep2() {
        String plate = mPlateEditText.getText().toString().trim().toUpperCase();
        String mileage = mMileageEditText.getText().toString().trim();
        if (!mileage.isEmpty()) {
            mileage = mileage.replace(",", "");
        }
        boolean isScheduled = mScheduledCheckBox.isChecked();
        boolean isRepair = mRepairCheckBox.isChecked();
        String notes = mNotesEditText.getText().toString().trim();

        if (mCountries == null) {
            mCountries = new String[]{};
        }

        startFragment(MaintenanceScheduleFragment2.newInstance(mPlate == null, plate, mileage, isScheduled, isRepair, notes, mCountries));
    }

    private void setPlate(String plate) {
        mPlate = plate;
        if (mPlateEditText != null) {
            mPlateEditText.setText(mPlate);
        }
    }

    private void setMileage(String mileage) {
        mMileage = mileage;
        if (mMileageEditText != null) {
            mMileageEditText.setText(mMileage);
        }
    }

    private void setCountries(String[] countries) {
        mCountries = countries;
    }

    private void getRepairData() {
        UserInfo userInfo = Prefs.getUserInfo(getContext());
        if (userInfo == null) {
            return;
        }

        String carNo = getUserInputCarNo();
        if (TextUtils.isEmpty(carNo)) {
            CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
            if (carKeyData == null) {
                // no car available
                return;
            }
            carNo = carKeyData.getCarNo();
        }

        String iKeyUid = userInfo.getKeyUid();
        String iKeyToken = userInfo.getToken();

        setPlate(carNo);

        GetRepairRequestData data = new GetRepairRequestData(iKeyUid, iKeyToken, carNo);

        AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(getContext());
        assistantWebApi.getRepairData(data, mOnApiListener);
    }

    private String getUserInputCarNo() {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        return args.getString("carNo");
    }
}
