package com.luxgen.remote.ui.dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    public static final String TAG = DatePickerFragment.class.getSimpleName();

    private int year;
    private int month;
    private int day;

    public interface OnDateSetListener {
        void onDateSet(DatePickerFragment fragment, int year, int month, int day);
    }

    private static final String ARG_YEAR = "ARG_YEAR";
    private static final String ARG_MONTH = "ARG_MONTH";
    private static final String ARG_DAY = "ARG_DAY";

    private OnDateSetListener mOnDateSetListener;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(ARG_YEAR, year);
        outState.putInt(ARG_MONTH, month);
        outState.putInt(ARG_DAY, day);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        if (savedInstanceState != null) {
            year = savedInstanceState.getInt(ARG_YEAR, year);
            month = savedInstanceState.getInt(ARG_MONTH, month);
            day = savedInstanceState.getInt(ARG_DAY, day);
        }

        DatePickerDialog picker = new DatePickerDialog(getActivity(), this, year, month, day);

        return picker;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;

        if (mOnDateSetListener != null) {
            mOnDateSetListener.onDateSet(DatePickerFragment.this, year, month, day);
        }
    }

    public Date getDateFromDatePicker() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        return calendar.getTime();
    }

    public void setOnDateSetListener(OnDateSetListener listener) {
        mOnDateSetListener = listener;
    }
}
