package com.luxgen.remote.ui.viewmodel;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.db.VigDatabase;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.util.Prefs;

public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private UseLogRepository mUseLogRepo;
    private Application mApp;
    private UserInfo mUserInfo;
    private PushInfoRepository mPushInfoRepo;

    public ViewModelFactory(Application application) {
        mApp = application;
        AssistantWebApi api = AssistantWebApi.getInstance(application.getApplicationContext());
        VigDatabase db = VigDatabase.getInstance(application.getApplicationContext());
        mUserInfo = Prefs.getUserInfo(application.getApplicationContext());
        mUseLogRepo = UseLogRepository.getInstance(mUserInfo, db, api);
        mPushInfoRepo = PushInfoRepository.getInstance(db, api);
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {

        if (modelClass.isAssignableFrom(MainViewModel.class)) {
            return (T) new MainViewModel(mApp, mUseLogRepo, mPushInfoRepo);
        }

        BaseViewModel vm = new BaseViewModel(mApp);
        return (T) vm;
    }
}
