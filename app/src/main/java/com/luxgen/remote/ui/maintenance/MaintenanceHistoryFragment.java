package com.luxgen.remote.ui.maintenance;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.luxgen.remote.R;
import com.luxgen.remote.ui.maintenance.RepairHistoryAdapter;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.ui.dialog.ProgressDialogFragment;
import com.luxgen.remote.custom.RecyclerViewEmptySupport;
import com.luxgen.remote.custom.SimpleDividerItemDecoration;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.maintenance.GetRepairHistoryRequestData;
import com.luxgen.remote.network.model.maintenance.GetRepairHistoryResponseData;
import com.luxgen.remote.util.NetUtils;
import com.luxgen.remote.util.Prefs;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

public class MaintenanceHistoryFragment extends CustomBaseFragment {

    private final static int DIVIDER_START_SPACING_DP = 24;
    private RepairHistoryAdapter mAdapter;
    private AppCompatSpinner mYearSpinner;
    private ProgressDialogFragment mProgressDialogFragment;
    private int mSelectedYearPosition = 0;

    public static MaintenanceHistoryFragment newInstance(String carNo) {
        MaintenanceHistoryFragment f = new MaintenanceHistoryFragment();

        if (carNo != null) {
            Bundle args = new Bundle();
            args.putString("carNo", carNo);
            f.setArguments(args);
        }

        return f;
    }

    private void dismissDialog() {
        if (isAdded() && mProgressDialogFragment != null) {
            mProgressDialogFragment.dismiss();
        }
    }

    private AdapterView.OnItemSelectedListener mOnYearSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (NetUtils.isConnectToInternet(Objects.requireNonNull(getContext()))) {
                int year = getCurrentYear() - position;
                if (getUserVisibleHint()) {
                    fetchRepairHistory(year);
                }
            } else {
                showServerErrorDialog();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            if (!NetUtils.isConnectToInternet(Objects.requireNonNull(getContext()))) {
                showServerErrorDialog();
            }
        }
    };

    private OnApiListener<GetRepairHistoryResponseData> mOnGetRepairHistoryApiListener = new OnApiListener<GetRepairHistoryResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetRepairHistoryResponseData responseData) {
            dismissDialog();
            if (responseData.isSuccessful()) {
                mAdapter.updateItems(responseData.getList());
                mSelectedYearPosition = mYearSpinner.getSelectedItemPosition();
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
                mYearSpinner.setSelection(mSelectedYearPosition);
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissDialog();
            showServerErrorDialog(failMessage);
            mYearSpinner.setSelection(mSelectedYearPosition);
        }

        @Override
        public void onPreApiTask() {
            mProgressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.maintenance_fetching_data));
            mProgressDialogFragment.show(getFragmentManager(), "getRepairHistoryDialog");
        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            dismissDialog();
            showMultiLoginErrorDialog();
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maintenance_history, container, false);

        View noAuthorityView = view.findViewById(R.id.no_authority_container);
        View baseView = view.findViewById(R.id.base_container);
        if (isAuthorized() && isCurrentCar()) {
            noAuthorityView.setVisibility(View.GONE);
            baseView.setVisibility(View.VISIBLE);
        } else {
            noAuthorityView.setVisibility(View.VISIBLE);
            baseView.setVisibility(View.GONE);
        }

        mYearSpinner = view.findViewById(R.id.year);
        mAdapter = new RepairHistoryAdapter(getLayoutInflater());

        View headerView = view.findViewById(R.id.header);
        View emptyView = view.findViewById(R.id.list_empty);
        RecyclerViewEmptySupport recyclerView = view.findViewById(R.id.list);

        setupRecyclerView(recyclerView, headerView, emptyView, mAdapter);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayAdapter<String> yearList = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, getYears());

        mYearSpinner.setAdapter(yearList);
        mYearSpinner.setOnItemSelectedListener(mOnYearSelectedListener);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isAdded() && isAuthorized() && isCurrentCar()) {
            fetchRepairHistory(getSelectedYear());
        }
    }

    private int getSelectedYear() {
        int year = getCurrentYear();
        if (mYearSpinner != null) {
            year = year - mYearSpinner.getSelectedItemPosition();
        }
        return year;
    }

    private void setupRecyclerView(RecyclerViewEmptySupport recyclerView, View headerView, View emptyView, RepairHistoryAdapter adapter) {
        Context context = recyclerView.getContext();
        int spacing = dp2px(recyclerView.getContext(), DIVIDER_START_SPACING_DP);

        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(context, R.drawable.recyclerview_horizontal_divider, spacing));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        if (headerView != null) {
            recyclerView.setHeaderView(headerView);
        }
        if (emptyView != null) {
            recyclerView.setEmptyView(emptyView);
        }
        recyclerView.setAdapter(adapter);
    }

    private int dp2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    private boolean isAuthorized() {
        CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
        return carKeyData != null && (carKeyData.isOwner() || carKeyData.isDriver());
    }

    private int getCurrentYear() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR);
    }

    private String[] getYears() {
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        String[] years = new String[4];
        for (int i = 0; i < years.length; i++, currentYear--) {
            years[i] = String.format(Locale.TAIWAN, "%d 年", currentYear);
        }

        return years;
    }

    private String getDateString(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        if (month != -1) {
            calendar.set(Calendar.MONTH, month - 1);
        }
        if (day != -1) {
            calendar.set(Calendar.DAY_OF_MONTH, day);
        }

        SimpleDateFormat mDateFormatter = new SimpleDateFormat("yyyy/MM/dd", Locale.TAIWAN);
        mDateFormatter.setTimeZone(calendar.getTimeZone());

        return mDateFormatter.format(calendar.getTime());
    }

    private void fetchRepairHistory(int year) {
        if (getContext() == null) {
            return;
        }

        UserInfo userInfo = Prefs.getUserInfo(getContext());
        if (userInfo == null) {
            return;
        }

        CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
        if (carKeyData == null) {
            return;
        }

        String carNo = getUserInputCarNo();
        if (TextUtils.isEmpty(carNo)) {
            carNo = carKeyData.getCarNo();
        }

        GetRepairHistoryRequestData data = new GetRepairHistoryRequestData(
                userInfo.getKeyUid(), userInfo.getToken(), carNo,
                getDateString(year, 1, 1), getDateString(year, 12, 31));

        AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(getContext());
        assistantWebApi.getRepairHistory(data, mOnGetRepairHistoryApiListener);
    }

    private String getUserInputCarNo() {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        return args.getString("carNo");
    }

    private boolean isCurrentCar() {
        String carNo = getUserInputCarNo();
        if (TextUtils.isEmpty(carNo)) {
            return true;
        }
        carNo = carNo.replace("-", "");

        CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
        if (carKeyData != null) {
            String carNoInDB = carKeyData.getCarNo();
            if (TextUtils.isEmpty(carNoInDB)) {
                return false;
            }

            return carNoInDB.toUpperCase().replace("-", "").equals(carNo);
        }

        return false;
    }
}
