package com.luxgen.remote.ui.maintenance;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luxgen.remote.R;
import com.luxgen.remote.network.model.maintenance.CarRepairPlanData;

import java.util.ArrayList;
import java.util.List;

public class ReservationAdapter extends RecyclerView.Adapter {
    private List<CarRepairPlanData> items;
    private LayoutInflater inflater;
    private ReservationViewHolder.ReservationListener mReservationListener;

    public ReservationAdapter(LayoutInflater inflater, ReservationViewHolder.ReservationListener reservationListener) {
        this.inflater = inflater;
        this.mReservationListener = reservationListener;
        items = new ArrayList<>();
        setHasStableIds(true);
    }

    public void updateItems(final List<CarRepairPlanData> newItems) {
        final List<CarRepairPlanData> oldItems = new ArrayList<>(this.items);
        this.items.clear();
        if (newItems != null) {
            this.items.addAll(newItems);
        }

        DiffUtil.calculateDiff(new DiffUtil.Callback() {
            @Override
            public int getOldListSize() {
                return oldItems.size();
            }

            @Override
            public int getNewListSize() {
                return items.size();
            }

            @Override
            public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                return oldItems.get(oldItemPosition).equals(items.get(newItemPosition));
            }

            @Override
            public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                final CarRepairPlanData oldCarRepairPlanData = oldItems.get(oldItemPosition);
                final CarRepairPlanData newCarRepairPlanData = items.get(newItemPosition);

                return oldCarRepairPlanData.getBookingNo().equals(newCarRepairPlanData.getBookingNo());
            }
        }).dispatchUpdatesTo(this);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_reservation, parent, false);
        return new ReservationViewHolder(v, mReservationListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ReservationViewHolder viewHolder = (ReservationViewHolder) holder;
        viewHolder.setItem(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(items.get(position).getBookingNo());
    }
}
