package com.luxgen.remote.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomFragmentsAppCompatActivity;

public class LoginForgetPasswordActivity extends CustomFragmentsAppCompatActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, LoginForgetPasswordActivity.class);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_forget_password;
    }

    @Override
    protected void setupTitle(String tag) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        show(new LoginForgetPasswordFragment());
    }
}
