package com.luxgen.remote.ui.maintenance;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luxgen.remote.R;
import com.luxgen.remote.ui.dialog.AlertDialogFragment;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.ui.dialog.ProgressDialogFragment;
import com.luxgen.remote.custom.RecyclerViewEmptySupport;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.option.CarOptions;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.maintenance.CarRepairPlanData;
import com.luxgen.remote.network.model.maintenance.DelCarRepairPlanRequestData;
import com.luxgen.remote.network.model.maintenance.DelCarRepairPlanResponseData;
import com.luxgen.remote.network.model.maintenance.GetCarRepairPlanRequestData;
import com.luxgen.remote.network.model.maintenance.GetCarRepairPlanResponseData;
import com.luxgen.remote.util.Prefs;

import java.util.ArrayList;

public class MaintenanceReservationsFragment extends CustomBaseFragment implements ReservationViewHolder.ReservationListener {

    private ReservationAdapter mAdapter;
    private ArrayList<CarRepairPlanData> mCarRepairPlanDataArrayList;
    private int mDataPosition;
    private ProgressDialogFragment mProgressDialogFragment;
    private boolean mIsVisibleToUser = false;

    public static MaintenanceReservationsFragment newInstance(String carNo) {
        MaintenanceReservationsFragment f = new MaintenanceReservationsFragment();

        if (carNo != null) {
            Bundle args = new Bundle();
            args.putString("carNo", carNo);
            f.setArguments(args);
        }

        return f;
    }

    @Override
    public void onMapClicked(int position) {
        CarRepairPlanData item = mCarRepairPlanDataArrayList.get(position);
        String address = item.getDeptAddress();
        String name = item.getDept();

        startActivity(MaintenanceMapActivity.newIntent(getContext(), address, name));
    }

    @Override
    public void onPhoneClicked(int position) {
        CarRepairPlanData item = mCarRepairPlanDataArrayList.get(position);
        String title = getString(R.string.maintenance_reservation_call_to, item.getDept());
        final String message = item.getDeptPhone1();

        showDialConfirmDialog(title, message, message);
    }

    @Override
    public void onCancelClicked(int position) {
        String title = getString(R.string.maintenance_reservation_cancel_dialog_title);
        String message = getString(R.string.maintenance_reservation_cancel_dialog_message);

        mDataPosition = position;

        showCancelConfirmDialog(title, message);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maintenance_reservations, container, false);

        View noAuthorityView = view.findViewById(R.id.no_authority_container);
        View baseView = view.findViewById(R.id.base_container);

        mAdapter = new ReservationAdapter(getLayoutInflater(), this);

        View emptyView = view.findViewById(R.id.list_empty);
        RecyclerViewEmptySupport recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mAdapter);
        recyclerView.setEmptyView(emptyView);

        if (isAuthorized() && isCurrentCar()) {
            noAuthorityView.setVisibility(View.GONE);
            baseView.setVisibility(View.VISIBLE);
            if (mIsVisibleToUser) {
                fetchReservations();
            }
        } else {
            noAuthorityView.setVisibility(View.VISIBLE);
            baseView.setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        mIsVisibleToUser = isVisibleToUser;
        if (isVisibleToUser && isAdded() && isAuthorized() && isCurrentCar()) {
            fetchReservations();
        }
    }

    private void showDialConfirmDialog(String title, String message, final String phone) {
        AlertDialogFragment dialConfirmDialog = AlertDialogFragment.newInstance(title, message);
        dialConfirmDialog.setButtons(getString(R.string.action_dial), getString(R.string.action_dismiss), new AlertDialogFragment.Callbacks() {
            @Override
            public void onPositiveButtonClicked() {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
            }

            @Override
            public void onNegativeButtonClicked() {
            }

            @Override
            public void onBackKeyPressed() {
            }
        });
        dialConfirmDialog.show(getChildFragmentManager(), "dialConfirmDialog");
    }

    private void showCancelConfirmDialog(String title, String message) {
        showConfirmDialog(title, message, new AlertDialogFragment.Callbacks() {
            @Override
            public void onPositiveButtonClicked() {
                CarRepairPlanData item = mCarRepairPlanDataArrayList.get(mDataPosition);

                String regoNO = item.getCarNo();
                String bookingNO = item.getBookingNo();
                String dealerCode = item.getDealerCode();
                String deptCode = item.getDeptCode();

                cancelReservation(regoNO, bookingNO, dealerCode, deptCode);
            }

            @Override
            public void onNegativeButtonClicked() {
            }

            @Override
            public void onBackKeyPressed() {
            }
        });
    }

    private void dismissDialog() {
        if (isAdded() && mProgressDialogFragment != null) {
            mProgressDialogFragment.dismiss();
        }
    }

    private OnApiListener<GetCarRepairPlanResponseData> mOnGetCarRepairPlanApiListener = new OnApiListener<GetCarRepairPlanResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetCarRepairPlanResponseData responseData) {
            dismissDialog();
            if (responseData.isSuccessful()) {
                mCarRepairPlanDataArrayList = responseData.getRepairPlayList();
                mAdapter.updateItems(mCarRepairPlanDataArrayList);
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissDialog();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            if (mAdapter != null && mAdapter.getItemCount() == 0) {
                mProgressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.maintenance_fetching_data));
                mProgressDialogFragment.show(getFragmentManager(), "getRepairPlanDialog");
            }
        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            dismissDialog();
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<DelCarRepairPlanResponseData> mDelCarRepairPlanApiListener = new OnApiListener<DelCarRepairPlanResponseData>() {
        @Override
        public void onApiTaskSuccessful(DelCarRepairPlanResponseData responseData) {
            if (responseData.isSuccessful()) {
                mCarRepairPlanDataArrayList.remove(mDataPosition);
                mAdapter.updateItems(mCarRepairPlanDataArrayList);
                if (mCarRepairPlanDataArrayList.size() == 0) {
                    // Do nothing
                }
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            showMultiLoginErrorDialog();
        }
    };

    private void fetchReservations() {
        if (getContext() == null) {
            return;
        }

        UserInfo userInfo = Prefs.getUserInfo(getContext());
        if (userInfo == null) {
            return;
        }

        CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
        if (carKeyData == null) {
            return;
        }

        String carNo = getUserInputCarNo();
        if (TextUtils.isEmpty(carNo)) {
            carNo = carKeyData.getCarNo();
        }

        GetCarRepairPlanRequestData data = new GetCarRepairPlanRequestData(userInfo.getKeyUid(), userInfo.getToken(), carNo);

        AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(getContext());
        assistantWebApi.getCarRepairPlan(data, mOnGetCarRepairPlanApiListener);
    }

    private void cancelReservation(String regoNO, String bookingNO, String dealerCode, String deptCode) {
        UserInfo userInfo = Prefs.getUserInfo(getContext());
        DelCarRepairPlanRequestData data = new DelCarRepairPlanRequestData(
                userInfo.getKeyUid(), userInfo.getToken(), regoNO, bookingNO, dealerCode, deptCode);

        AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(getContext());
        assistantWebApi.delCarRepairPlan(data, mDelCarRepairPlanApiListener);
    }

    private boolean isAuthorized() {
        CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
        if (carKeyData != null) {
            if (carKeyData.isOwner() || carKeyData.isDriver()) {
                return true;
            }

            return carKeyData.getKeyType().equals(CarOptions.KEY_M) ||
                    carKeyData.getKeyType().equals(CarOptions.KEY_M2) ||
                    carKeyData.getKeyType().equals(CarOptions.KEY_S1) ||
                    carKeyData.getKeyType().equals(CarOptions.KEY_S2);
        }

        return false;
    }

    private String getUserInputCarNo() {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        return args.getString("carNo");
    }

    private boolean isCurrentCar() {
        String carNo = getUserInputCarNo();
        if (TextUtils.isEmpty(carNo)) {
            return true;
        }
        carNo = carNo.replace("-", "");

        CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
        if (carKeyData != null) {
            String carNoInDB = carKeyData.getCarNo();
            if (TextUtils.isEmpty(carNoInDB)) {
                return false;
            }

            return carNoInDB.toUpperCase().replace("-", "").equals(carNo);
        }

        return false;
    }
}
