package com.luxgen.remote.ui.login;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.ui.dialog.ProgressDialogFragment;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.UserWebApi;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.user.SetAccountRequestData;

import static com.luxgen.remote.ui.login.LoginActivity.FRAGMENT_TAG_LOGIN_REGISTER_ADD_CAR;
import static com.luxgen.remote.ui.login.LoginRegisterFragment.LOGIN_TYPE_FACEBOOK;
import static com.luxgen.remote.ui.login.LoginRegisterFragment.LOGIN_TYPE_GOOGLE;

public class LoginRegisterPasswordFragment extends CustomBaseFragment {

    private EditText mPasswordEditText;
    private EditText mPasswordConfirmEditText;
    private String mEmail;
    private int mLoginType;
    private String mSocialId;
    private String mSocialAlias;
    private ProgressDialogFragment mProgressDialog;

    public static LoginRegisterPasswordFragment newInstance(String email, String deviceId, int loginType, String socialId, String socialAlias) {
        Bundle args = new Bundle();
        args.putString("email", email);
        args.putString("deviceId", deviceId);
        args.putInt("loginType", loginType);
        args.putString("socialId", socialId);
        args.putString("socialAlias", socialAlias);

        LoginRegisterPasswordFragment fragment = new LoginRegisterPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private OnApiListener<ResponseData> mOnApiListener = new OnApiListener<ResponseData>() {
        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            mProgressDialog.dismiss();
            if (responseData.isSuccessful()) {
                showAddCarScreen();
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            mProgressDialog.dismiss();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mProgressDialog = ProgressDialogFragment.newInstance(getString(R.string.login_register_password_creating));
            mProgressDialog.show(getFragmentManager(), "registerProgressDialog");
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            // TODO: check if need this
            mProgressDialog.dismiss();
            showMultiLoginErrorDialog();
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        mEmail = args.getString("email");
        mLoginType = args.getInt("loginType");
        mSocialId = args.getString("socialId");
        mSocialAlias = args.getString("socialAlias");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login_register_password, container, false);

        mPasswordEditText = v.findViewById(R.id.edit_password);
        mPasswordConfirmEditText = v.findViewById(R.id.edit_password_confirm);
        mPasswordConfirmEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideSoftKeyboard(v);
                    addCar();
                    return true;
                }
                return false;
            }
        });
        mPasswordConfirmEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                if (!str.equals(mPasswordEditText.getText().toString().trim())) {
                    mPasswordConfirmEditText.setError(getString(R.string.login_register_password_error_not_match));
                } else {
                    mPasswordConfirmEditText.setError(null);
                }
            }
        });

        Button nextButton = v.findViewById(R.id.btn_next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCar();
            }
        });

        return v;
    }

    private boolean validate() {
        String password = mPasswordEditText.getText().toString().trim();
        String passwordConfirm = mPasswordConfirmEditText.getText().toString().trim();

        if (TextUtils.isEmpty(password)) {
            mPasswordEditText.requestFocus();
            mPasswordEditText.setError(getString(R.string.login_register_password_error_empty));
            return false;
        }

        if (TextUtils.isEmpty(passwordConfirm)) {
            mPasswordConfirmEditText.requestFocus();
            mPasswordConfirmEditText.setError(getString(R.string.login_register_password_error_empty));
            return false;
        }

        if (!password.equals(passwordConfirm)) {
            mPasswordConfirmEditText.requestFocus();
            mPasswordConfirmEditText.selectAll();
            mPasswordConfirmEditText.setError(getString(R.string.login_register_password_error_not_match));
            return false;
        }

        return true;
    }

    private void addCar() {
        if (validate()) {
            register();
        }
    }

    private SetAccountRequestData buildRegisterData() {
        SetAccountRequestData data = new SetAccountRequestData();
        data.setAccount(mEmail);
        data.setPassword(mPasswordConfirmEditText.getText().toString().trim());
        if (mLoginType == LOGIN_TYPE_FACEBOOK) {
            data.setFacebookAccount(mSocialId, mSocialAlias);
        } else if (mLoginType == LOGIN_TYPE_GOOGLE) {
            data.setGoogleAccount(mSocialId, mSocialAlias);
        }

        return data;
    }

    private void register() {
        SetAccountRequestData data = buildRegisterData();

        UserWebApi userWebApi = UserWebApi.getInstance(getActivity());
        userWebApi.setAccount(data, mOnApiListener);
    }

    private void showAddCarScreen() {
        String password = mPasswordConfirmEditText.getText().toString().trim();
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        args.putString("password", password);

        ((LoginActivity) getActivity()).navigate(this, LoginRegisterAddCarFragment.newInstance(args),
                FRAGMENT_TAG_LOGIN_REGISTER_ADD_CAR, true);
    }
}
