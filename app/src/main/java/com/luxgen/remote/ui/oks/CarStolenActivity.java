package com.luxgen.remote.ui.oks;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomFragmentsAppCompatActivity;

public class CarStolenActivity extends CustomFragmentsAppCompatActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, CarStolenActivity.class);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_car_stolen;
    }

    @Override
    protected void setupTitle(String tag) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.oneclick_car_stolen);
        } else {
            setTitle(R.string.oneclick_car_stolen);
        }

        show(new CarStolenFragment());
        bindCaService();
    }

    @Override
    protected void onDestroy() {
        unbindCaService();
        super.onDestroy();
    }
}
