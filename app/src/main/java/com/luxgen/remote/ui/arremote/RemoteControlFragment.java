package com.luxgen.remote.ui.arremote;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.ca.CommunicationAgentService;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.maintenance.GetCarProgressInWorkshopResponseData;
import com.luxgen.remote.ui.dialog.AlertDialogFragment;
import com.luxgen.remote.custom.CustomBaseFragmentWithCaService;
import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.ui.dialog.OptimizeDialogFragment;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.util.NetUtils;
import com.luxgen.remote.util.PM25Service;
import com.luxgen.remote.util.Prefs;

import static com.luxgen.remote.ca.CommunicationAgentService.REMOTE_COMMAND_DOOR_LOCK;
import static com.luxgen.remote.ca.CommunicationAgentService.REMOTE_COMMAND_DOOR_UNLOCK;
import static com.luxgen.remote.ca.CommunicationAgentService.REMOTE_COMMAND_ENGINE_OFF;
import static com.luxgen.remote.ca.CommunicationAgentService.REMOTE_COMMAND_FIND_CAR;
import static com.luxgen.remote.ca.CommunicationAgentService.REMOTE_COMMAND_ONE_CLICK_OPTIMIZE;
import static com.luxgen.remote.ca.CommunicationAgentService.REMOTE_COMMAND_OPEN_BACKDOOR;

public class RemoteControlFragment extends CustomBaseFragmentWithCaService {

    private final static String TAG = RemoteControlFragment.class.getSimpleName();

    public final static int REQUEST_NONE = 9527;
    public final static int REQUEST_OPTIMIZE = REQUEST_NONE + 1;
    public final static int REQUEST_ENGINE_OFF = REQUEST_OPTIMIZE + 1;
    public final static int REQUEST_FIND = REQUEST_ENGINE_OFF + 1;
    public final static int REQUEST_OPEN = REQUEST_FIND + 1;
    public final static int REQUEST_UNLOCK = REQUEST_OPEN + 1;
    public final static int REQUEST_LOCK = REQUEST_UNLOCK + 1;
    public final static int REQUEST_ENABLE_BT = REQUEST_LOCK + 1;
    public final static int PIN_TIMEOUT_MS = 1000 * 60 * 10; // 10 minutes

    private TextView modelTextView;
    private TextView plateTextView;
    private Button optimizeButton;
    private Button engineOffButton;
    private Button findCarButton;
    private Button backDoorButton;
    private Button openButton;
    private Button lockButton;

    private TextView vigStatusTextView;

    private RelativeLayout mProgressLayout;
    private TextView mProgressContentTextView;
    private String executeActionFrom;
    private String mCurrentRemoteControl;
    private boolean mSecurityChannelAlertShown = false;

    private OptimizeDialogFragment.Callbacks mOptimizeDialogCallback = new OptimizeDialogFragment.Callbacks() {
        @Override
        public void onPositiveButtonClicked(float temperature) {
            setCarTemperature(temperature);
            setTemperature(String.valueOf(temperature));
        }

        @Override
        public void onNegativeButtonClicked() {
        }
    };

    public static RemoteControlFragment newInstance(int function) {
        RemoteControlFragment f = new RemoteControlFragment();

        Bundle args = new Bundle();
        args.putInt("function", function);
        f.setArguments(args);

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_remote_control_main, container, false);

        View bleOnlyContainer = v.findViewById(R.id.ble_only_container);
        if (!NetUtils.isConnectToInternet(getActivity())) {
            bleOnlyContainer.setVisibility(View.VISIBLE);
        }

        mProgressLayout = v.findViewById(R.id.progress_container);
        mProgressLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        mProgressContentTextView = v.findViewById(R.id.progress_content);

        modelTextView = v.findViewById(R.id.model);
        plateTextView = v.findViewById(R.id.plate);
        vigStatusTextView = v.findViewById(R.id.vig_status);

        TextView lastUpdateTimeTextView = v.findViewById(R.id.last_update);
        String lastUpdateTimeString = getString(R.string.remote_control_last_update_time);
        lastUpdateTimeString = String.format(lastUpdateTimeString, Prefs.getCarStatusLastUpdateTime(getActivity()));
        lastUpdateTimeTextView.setText(lastUpdateTimeString);

        Button remoteRecodeButton = v.findViewById(R.id.btn_remote_recode);
        remoteRecodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetUtils.isConnectToInternet(getContext())) {
                    showControlHistory();
                } else {
                    showServerErrorDialog();
                }
            }
        });

        Button airDataButton = v.findViewById(R.id.btn_air_data);
        airDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetUtils.isConnectToInternet(getContext())) {
                    showAirData();
                } else {
                    showServerErrorDialog();
                }
            }
        });

        optimizeButton = v.findViewById(R.id.btn_optimize);
        optimizeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeActionFrom = "optimizeButtonClick";
                showProgressLayout(getString(R.string.remote_control_op_optimize_car_progress_msg));
                checkCarProgress();
            }
        });

        engineOffButton = v.findViewById(R.id.btn_engine_off);
        engineOffButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeActionFrom = "engineOffButtonClick";
                showProgressLayout(getString(R.string.remote_control_op_engine_off_progress_msg));
                checkCarProgress();
            }
        });

        findCarButton = v.findViewById(R.id.btn_find_car);
        findCarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeActionFrom = "findCarButtonClick";
                showProgressLayout(getString(R.string.remote_control_op_find_car_progress_msg));
                checkCarProgress();
            }
        });

        backDoorButton = v.findViewById(R.id.btn_back_door);
        backDoorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeActionFrom = "backDoorButtonClick";
                showProgressLayout(getString(R.string.remote_control_op_open_backdoor_progress_msg));
                checkCarProgress();
            }
        });

        openButton = v.findViewById(R.id.btn_open);
        openButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isVigConnected()) {
                    showSafetyAlert(REQUEST_UNLOCK);
                } else {
                    if (isPinCodeVerified(REQUEST_UNLOCK)) {
                        unlockCar();
                    }
                }
            }
        });

        lockButton = v.findViewById(R.id.btn_lock);
        lockButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPinCodeVerified(REQUEST_LOCK)) {
                    lockCar();
                }
            }
        });

        return v;
    }

    private void setupButtonFace(boolean isVigConnected) {
        if (isVigConnected) {
            optimizeButton.setBackgroundResource(R.drawable.btn_optimize_ble);
            engineOffButton.setBackgroundResource(R.drawable.btn_engine_off_ble);
            findCarButton.setBackgroundResource(R.drawable.btn_find_car_ble);
            backDoorButton.setBackgroundResource(R.drawable.btn_back_door_ble);
            openButton.setBackgroundResource(R.drawable.btn_open_ble);
            lockButton.setBackgroundResource(R.drawable.btn_lock_ble);
        } else {
            optimizeButton.setBackgroundResource(R.drawable.btn_optimize);
            engineOffButton.setBackgroundResource(R.drawable.btn_engine_off);
            findCarButton.setBackgroundResource(R.drawable.btn_find_car);
            backDoorButton.setBackgroundResource(R.drawable.btn_back_door);
            openButton.setBackgroundResource(R.drawable.btn_open);
            lockButton.setBackgroundResource(R.drawable.btn_lock);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
        if (carKeyData != null) {
            plateTextView.setText(carKeyData.getCarNo());
            modelTextView.setText(carKeyData.getCarModel());
        }
        String mProgress = Prefs.getRepairProgress(getContext());
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        int function = args.getInt("function");
        LogCat.d("function:"+function);
        if (!isVigConnected() && (function == REQUEST_UNLOCK || function == REQUEST_OPEN)) {
            if(function == REQUEST_OPEN) {
                LogCat.d("functionOPENDoor");
                executeActionFrom = "backDoorButtonClick";
                doProgressAction(mProgress);
            } else {
                showSafetyAlert(function);
            }
        } else {
            executeRemoteControl(function);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.remote_control, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.action_refresh);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_setup);
        if (item != null) {
            item.setVisible(false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode >= REQUEST_OPTIMIZE && requestCode <= REQUEST_LOCK) {
                Prefs.setPinShowTime(getContext(), getCurrentTime());
                executeRemoteControl(requestCode);
            }
        }
    }

    private String getCarId() {
        if (getContext() == null) {
            return null;
        }

        CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
        if (carKeyData == null) {
            return null;
        }

        return carKeyData.getCarId();
    }

    private void startPM25Service() {
        String carId = getCarId();
        if (carId != null) {
            Intent intent = new Intent(getActivity(), PM25Service.class);
            intent.putExtra(PM25Service.PM25_INPUT_CARID, carId);
            getActivity().startService(intent);
        }
    }

    private String getErrorString(String command) {
        if (REMOTE_COMMAND_ONE_CLICK_OPTIMIZE.equals(command)) {
            return getString(R.string.remote_control_op_optimize_failed_message);
        } else if (REMOTE_COMMAND_OPEN_BACKDOOR.equals(command)) {
            return getString(R.string.remote_control_op_open_backdoor_failed_message);
        } else if (REMOTE_COMMAND_DOOR_LOCK.equals(command)) {
            return getString(R.string.remote_control_op_lock_car_failed_message);
        } else if (REMOTE_COMMAND_DOOR_UNLOCK.equals(command)) {
            return getString(R.string.remote_control_op_unlock_car_failed_message);
        } else if (REMOTE_COMMAND_FIND_CAR.equals(command)) {
            return getString(R.string.remote_control_op_find_car_failed_message);
        } else {
            return getString(R.string.remote_control_op_error_failed);
        }
    }

    @Override
    protected void addActionToReceiveBroadcastFromCaService(IntentFilter intentFilter) {
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_REMOTE_CONTROL_DONE);
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_REMOTE_CONTROL_INTERNAL_ERROR);
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_REMOTE_CONTROL_FAILED);
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_REMOTE_CONTROL_BLOCKED);
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_VIG_ONLINE);
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_VIG_OFFLINE);
    }

    @Override
    protected void onReceiveBroadcastFromCaService(Context context, Intent intent) {
        String command = null;
        if (intent.hasExtra(CommunicationAgentService.CA_STATUS_EXTRA_COMMAND)) {
            command = intent.getStringExtra(CommunicationAgentService.CA_STATUS_EXTRA_COMMAND);
        }

        // 清除 result
        if (mCommunicationAgentService != null) {
            mCommunicationAgentService.getLastResult();
        }

        if (CommunicationAgentService.CA_STATUS_REMOTE_CONTROL_FAILED.equals(intent.getAction())) {
            dismissProgressLayout();
            if (intent.hasExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE)) {
                int errorString = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, -1);
                if (errorString != -1) {
                    if (errorString == R.string.remote_control_op_vig_failed_message) {
                        showConfirmDialog(getString(R.string.remote_control_op_vig_failed_title), getString(R.string.remote_control_op_vig_failed_message));
                    } else if (errorString == R.string.remote_control_op_bt_disconnect_during_rc_message) {
                        showConfirmDialog(getString(R.string.remote_control_op_bt_disconnect_during_rc_title), getString(R.string.remote_control_op_bt_disconnect_during_rc_message));
                    } else if (errorString == R.string.remote_control_op_error_offline) {
                        showConfirmDialog(getString(R.string.remote_control_op_error_offline_title), getString(R.string.remote_control_op_error_offline));
                    } else if (errorString == R.string.remote_control_op_error_rejected_optimize) {
                        showConfirmDialog(getString(R.string.remote_control_op_error_optimize_title), getString(R.string.remote_control_op_error_rejected_optimize));
                    } else {
                        showConfirmDialog(getString(R.string.remote_control_op_error_title), getString(errorString));
                    }
                } else {
                    showConfirmDialog(getString(R.string.remote_control_op_error_title), getErrorString(command));
                }
            } else {
                showConfirmDialog(getString(R.string.remote_control_network_error_title), getString(R.string.remote_control_network_error_message));
            }

            mCurrentRemoteControl = null;
            return;
        } else if (CommunicationAgentService.CA_STATUS_REMOTE_CONTROL_INTERNAL_ERROR.equals(intent.getAction())) {
            dismissProgressLayout();
            showConfirmDialog(getString(R.string.remote_control_op_error_title), getString(R.string.remote_control_internal_error));

            mCurrentRemoteControl = null;
            return;
        } else if (CommunicationAgentService.CA_STATUS_REMOTE_CONTROL_DONE.equals(intent.getAction())) {
            dismissProgressLayout();
            String message;
            if (REMOTE_COMMAND_OPEN_BACKDOOR.equals(command)) {
                message = getString(R.string.remote_control_op_open_backdoor_success_message);
            } else if (REMOTE_COMMAND_DOOR_UNLOCK.equals(command)) {
                message = getString(R.string.remote_control_op_unlock_car_success_message);
            } else if (REMOTE_COMMAND_DOOR_LOCK.equals(command)) {
                message = getString(R.string.remote_control_op_lock_car_success_message);
            } else if (REMOTE_COMMAND_FIND_CAR.equals(command)) {
                message = getString(R.string.remote_control_op_find_car_success_message);
            } else if (REMOTE_COMMAND_ONE_CLICK_OPTIMIZE.equals(command)) {
                message = getString(R.string.remote_control_op_optimize_success_message);
            } else {
                LogCat.d("No command value found");
                message = getString(R.string.remote_control_op_success_message);
            }
            showConfirmDialog(getString(R.string.remote_control_op_success_title), message);
            if (REMOTE_COMMAND_ONE_CLICK_OPTIMIZE.equals(command)) {
                long diff = System.currentTimeMillis() - Prefs.getOptimizeTime(getContext());
                if (diff >= RemoteControlAirDataFragment.MAX_TIME_FOR_PM25) {
                    startPM25Service();
                    Prefs.clear(getContext(), Prefs.PREFS_PM25);
                    Prefs.setOptimizeTime(getContext(), System.currentTimeMillis());
                }
            }

            mCurrentRemoteControl = null;
            return;
        } else if (CommunicationAgentService.CA_STATUS_REMOTE_CONTROL_BLOCKED.equals(intent.getAction())) {
            showConfirmDialog(getString(R.string.remote_control_op_error_title), getString(R.string.remote_control_blocked_error));

            mCurrentRemoteControl = null;
            return;
        } else if (CommunicationAgentService.CA_STATUS_VIG_ONLINE.equals(intent.getAction())) {
            if (isAdded() && vigStatusTextView != null) {
                vigStatusTextView.setText(R.string.remote_control_vig_connected);
                vigStatusTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.oval, 0, 0, 0);
                setupButtonFace(true);
            }
        } else if (CommunicationAgentService.CA_STATUS_VIG_OFFLINE.equals(intent.getAction())) {
            if (isAdded() && vigStatusTextView != null) {
                vigStatusTextView.setText(R.string.remote_control_vig_disconnected);
                vigStatusTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.obj_light_off, 0, 0, 0);
                setupButtonFace(false);
            }
        }

        super.onReceiveBroadcastFromCaService(context, intent);
    }

    private void updateDynamicLayout() {
        if (mCommunicationAgentService != null) {
            if (mCurrentRemoteControl == null) {
                mCurrentRemoteControl = mCommunicationAgentService.getCurrentRemoteControl();
            }
            if (!isPinCodeVerified()) {
                mCurrentRemoteControl = null;
            }
            boolean isDoingRemoteCommand = continueProgressLayout(mCurrentRemoteControl);
            if (vigStatusTextView != null) {
                if (mCommunicationAgentService.isVigConnected()) {
                    vigStatusTextView.setText(R.string.remote_control_vig_connected);
                    vigStatusTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.oval, 0, 0, 0);
                    setupButtonFace(true);
                } else {
                    vigStatusTextView.setText(R.string.remote_control_vig_disconnected);
                    vigStatusTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.obj_light_off, 0, 0, 0);
                    setupButtonFace(false);
                    if (!isDoingRemoteCommand && !mSecurityChannelAlertShown) {
                        showSecurityChannelAlert();
                        mSecurityChannelAlertShown = true;
                    }
                }
            }

            Intent intent = mCommunicationAgentService.getLastResult();
            if (intent != null) {
                onReceiveBroadcastFromCaService(getContext(), intent);
            }
        } else {
            continueProgressLayout(mCurrentRemoteControl);
        }
        mCurrentRemoteControl = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        updateDynamicLayout();
    }

    private boolean isPinCodeVerified() {
        return isPinCodeVerified(REQUEST_NONE, false);
    }

    private boolean isPinCodeVerified(int requestCode) {
        return isPinCodeVerified(requestCode, true);
    }

    private boolean isPinCodeVerified(int requestCode, boolean showFragment) {
        long now = getCurrentTime();
        long showTime = Prefs.getPinShowTime(getContext());
        long diff = PIN_TIMEOUT_MS - (now - showTime);
        if (diff > 0) {
            Prefs.setPinShowTime(getContext(), now);
            return true;
        } else {
            if (showFragment) {
                RemoteControlVerifyFragment fragment = RemoteControlVerifyFragment.newInstance();
                fragment.setTargetFragment(this, requestCode);
                ((RemoteControlActivity) getActivity()).navigate(this, fragment,
                        RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_PIN_VERIFY, true);
            }
            return false;
        }
    }

    private void showControlHistory() {
        ((RemoteControlActivity) getActivity()).navigate(this, RemoteControlHistoryFragment.newInstance(),
                RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_HISTORY, true);
    }

    private void showAirData() {
        ((RemoteControlActivity) getActivity()).navigate(this, RemoteControlAirDataFragment.newInstance(false),
                RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_AIR_DATA, true);
    }

    private void oneClickOptimize() {
        LogCat.d("oneClickOptimize");
        if (getCarTemperature() == -1) {
            dismissProgressLayout();
            OptimizeDialogFragment dialog = OptimizeDialogFragment.newInstance(
                    getString(R.string.remote_control_optimize_title),
                    getString(R.string.remote_control_optimize_message));
            dialog.setButtons(getString(R.string.btn_confirm), getString(R.string.btn_cancel), mOptimizeDialogCallback);
            dialog.setTemperature(OptimizeDialogFragment.DEFAULT_TEMPERATURE);
            dialog.show(getFragmentManager(), RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_OPTIMIZE);
        } else {
            setTemperature(String.valueOf(getCarTemperature()));
        }
    }

    private void startNearCommand(String command, String commandValue) {
        Intent intent = new Intent(getActivity(), CommunicationAgentService.class);
        intent.putExtra(CommunicationAgentService.VIG_BLE_COMMAND, command);
        intent.putExtra(CommunicationAgentService.VIG_BLE_COMMAND_VALUE, commandValue);
        getActivity().startService(intent);
    }

    private void sendCommand(String command, String value) {
        mCurrentRemoteControl = command;
        startNearCommand(command, value);
    }

    private void setTemperature(String temperature) {
        showProgressLayout(getString(R.string.remote_control_op_optimize_car_progress_msg));

        LogCat.d("setTemperature");
        sendCommand(REMOTE_COMMAND_ONE_CLICK_OPTIMIZE, temperature);
    }

    private void findCar() {
        showProgressLayout(getString(R.string.remote_control_op_find_car_progress_msg));

        LogCat.d("findCar");
        sendCommand(REMOTE_COMMAND_FIND_CAR, "");
    }

    private void openBackDoor() {
        showProgressLayout(getString(R.string.remote_control_op_open_backdoor_progress_msg));

        LogCat.d("openBackDoor");
        sendCommand(REMOTE_COMMAND_OPEN_BACKDOOR, "");
    }

    private void unlockCar() {
        showProgressLayout(getString(R.string.remote_control_op_unlock_car_progress_msg));

        LogCat.d("unlockCar");
        sendCommand(REMOTE_COMMAND_DOOR_UNLOCK, "");
    }

    private void lockCar() {
        showProgressLayout(getString(R.string.remote_control_op_lock_car_progress_msg));

        LogCat.d("lockCar");
        sendCommand(REMOTE_COMMAND_DOOR_LOCK, "");
    }

    private void engineOff() {
        showProgressLayout(getString(R.string.remote_control_op_engine_off_progress_msg));

        LogCat.d("engineOff");
        sendCommand(REMOTE_COMMAND_ENGINE_OFF, "");
    }

    private float getCarTemperature() {
        return Prefs.getCarSetupTemperature(getContext());
    }

    private void setCarTemperature(float temperature) {
        Prefs.setCarSetupTemperature(getContext(), temperature);
    }

    private void executeRemoteControl(int requestCode) {
        String mProgress = Prefs.getRepairProgress(getContext());
        switch (requestCode) {
            case REQUEST_OPTIMIZE:
                LogCat.d("executeRemoteControl_OPTIMIZE");
                executeActionFrom = "executeRemoteControl_OPTIMIZE";
                doProgressAction(mProgress);
                break;

            case REQUEST_FIND:
                LogCat.d("executeRemoteControl_FIND");
                executeActionFrom = "executeRemoteControl_FIND";
                doProgressAction(mProgress);
                break;

            case REQUEST_OPEN:
                LogCat.d("executeRemoteControl_OPEN");
                executeActionFrom = "executeRemoteControl_OPEN";
                doProgressAction(mProgress);
                break;

            case REQUEST_UNLOCK:
                if (isPinCodeVerified(REQUEST_UNLOCK)) {
                    unlockCar();
                }
                break;

            case REQUEST_LOCK:
                if (isPinCodeVerified(REQUEST_LOCK)) {
                    lockCar();
                }
                break;

            case REQUEST_ENGINE_OFF:
                LogCat.d("executeRemoteControl_ENGINE_OFF");
                executeActionFrom = "executeRemoteControl_ENGINE_OFF";
                doProgressAction(mProgress);
                break;
        }
    }

    private void showProgressLayout(String context) {
        if (mProgressLayout != null) {
            mProgressLayout.setVisibility(View.VISIBLE);
        }
        if (mProgressContentTextView != null) {
            mProgressContentTextView.setText(context);
        }
    }

    private void dismissProgressLayout() {
        if (mProgressLayout != null) {
            mProgressLayout.setVisibility(View.GONE);
        }
    }

    private boolean continueProgressLayout(String command) {
        if (REMOTE_COMMAND_ONE_CLICK_OPTIMIZE.equals(command)) {
            showProgressLayout(getString(R.string.remote_control_op_optimize_car_progress_msg));
        } else if (REMOTE_COMMAND_FIND_CAR.equals(command)) {
            showProgressLayout(getString(R.string.remote_control_op_find_car_progress_msg));
        } else if (REMOTE_COMMAND_OPEN_BACKDOOR.equals(command)) {
            showProgressLayout(getString(R.string.remote_control_op_open_backdoor_progress_msg));
        } else if (REMOTE_COMMAND_DOOR_UNLOCK.equals(command)) {
            showProgressLayout(getString(R.string.remote_control_op_unlock_car_progress_msg));
        } else if (REMOTE_COMMAND_DOOR_LOCK.equals(command)) {
            showProgressLayout(getString(R.string.remote_control_op_lock_car_progress_msg));
        } else if (REMOTE_COMMAND_ENGINE_OFF.equals(command)) {
            showProgressLayout(getString(R.string.remote_control_op_engine_off_progress_msg));
        } else {
            dismissProgressLayout();
            return false;
        }
        return true;
    }

    @Override
    protected void onCaServiceConnected() {
        super.onCaServiceConnected();
        updateDynamicLayout();
    }

    @Override
    protected void showConfirmDialog(String title, String message) {
        if (isAdded()) {
            final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();

            AlertDialogFragment dialog = AlertDialogFragment.newInstance(title, message);
            dialog.setButtons(getString(R.string.btn_confirm), null, null);
            dialog.setOnDismissListener(new AlertDialogFragment.OnDismissListener() {
                @Override
                public void execute() {
                    if (args.containsKey("function") && args.getInt("function") != REQUEST_NONE) {
                        getActivity().finish();
                    }
                }
            });
            dialog.show(getFragmentManager(), FRAGMENT_TAG_GENERAL_ERROR_DIALOG);
        }
    }

    private boolean isVigConnected() {
        if (mCommunicationAgentService != null && mCommunicationAgentService.isVigReady()) {
            return true;
        }

        return false;
    }

    private void showSafetyAlert(int func) {
        if (isAdded()) {
            final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();

            @StringRes int message, positiveButton, negativeButton;
            if (func == REQUEST_OPEN) {
                message = R.string.remote_control_op_remind_backdoor;
                positiveButton = R.string.remote_control_op_remind_backdoor_positive_button;
                negativeButton = R.string.remote_control_op_remind_backdoor_negative_button;
            } else { // REQUEST_UNLOCK
                message = R.string.remote_control_op_remind_unlock_car;
                positiveButton = R.string.remote_control_op_remind_unlock_car_positive_button;
                negativeButton = R.string.remote_control_op_remind_unlock_car_negative_button;
            }

            AlertDialogFragment dialog = AlertDialogFragment.newInstance(getString(R.string.remote_control_op_remind_title),
                    getString(message));
            dialog.setButtons(getString(positiveButton), getString(negativeButton), new AlertDialogFragment.Callbacks() {
                @Override
                public void onPositiveButtonClicked() {
                    executeRemoteControl(func);
                }

                @Override
                public void onNegativeButtonClicked() {
                    if (args.containsKey("function") && args.getInt("function") != REQUEST_NONE) {
                        getActivity().finish();
                    }
                }

                @Override
                public void onBackKeyPressed() {
                    if (args.containsKey("function") && args.getInt("function") != REQUEST_NONE) {
                        getActivity().finish();
                    }
                }
            });
            dialog.show(getFragmentManager(), FRAGMENT_TAG_CONFIRM_DIALOG);
        }
    }

    private void showSecurityChannelAlert() {
        if (isAdded()) {
            AlertDialogFragment dialog = AlertDialogFragment.newInstance(getString(R.string.remote_control_security_check_title),
                    getString(R.string.remote_control_security_check_message));
            dialog.setButtons(getString(R.string.remote_control_security_check_positive_button),
                    getString(R.string.remote_control_security_check_negative_button), new AlertDialogFragment.Callbacks() {
                @Override
                public void onPositiveButtonClicked() {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                }

                @Override
                public void onNegativeButtonClicked() {

                }

                @Override
                public void onBackKeyPressed() {

                }
            });
            dialog.show(getFragmentManager(), FRAGMENT_TAG_CONFIRM_DIALOG);
        }
    }

    private void doProgressAction(String mProgress) {
        if(mProgress.equalsIgnoreCase("0")) {
            switch (executeActionFrom) {
                case "optimizeButtonClick":
                    LogCat.d("Progress optimizeButtonClick");
                    if (!Prefs.getPreferenceKeyValue(getContext(), Prefs.KEY_NO_LONGER_REMIND_AIR_POLLUTION)) {
                        dismissProgressLayout();
                        showNoLongerRemindDialog(getString(R.string.remote_control_dialog_message_air_pollution),
                                Prefs.KEY_NO_LONGER_REMIND_AIR_POLLUTION,
                                getString(R.string.btn_confirm),
                                getString(R.string.btn_cancel),
                                new AlertDialogFragment.Callbacks() {

                                    @Override
                                    public void onPositiveButtonClicked() {
                                        showProgressLayout(getString(R.string.remote_control_op_optimize_car_progress_msg));
                                        if (isPinCodeVerified(REQUEST_OPTIMIZE)) {
                                            oneClickOptimize();
                                        }
                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }

                                    @Override
                                    public void onBackKeyPressed() {

                                    }
                                });
                    } else {
                        if (isPinCodeVerified(REQUEST_OPTIMIZE)) {
                            oneClickOptimize();
                        }
                    }
                    break;
                case "executeRemoteControl_OPTIMIZE":
                    LogCat.d("Progress executeRemoteControl_OPTIMIZE");
                    showProgressLayout(getString(R.string.remote_control_op_optimize_car_progress_msg));
                    if (isPinCodeVerified(REQUEST_OPTIMIZE)) {
                        oneClickOptimize();
                    }
                    break;

                case "findCarButtonClick":
                case "executeRemoteControl_FIND":
                    LogCat.d("Progress findCar");
                    showProgressLayout(getString(R.string.remote_control_op_find_car_progress_msg));
                    if (isPinCodeVerified(REQUEST_FIND)) {
                        findCar();
                    }
                    break;
                case "backDoorButtonClick":
                    LogCat.d("Progress backDoorButtonClick");
                    if (!isVigConnected()) {
                        dismissProgressLayout();
                        showSafetyAlert(REQUEST_OPEN);
                    } else {
                        showProgressLayout(getString(R.string.remote_control_op_open_backdoor_progress_msg));
                        if (isPinCodeVerified(REQUEST_OPEN)) {
                            openBackDoor();
                        }
                    }
                    break;

                case "executeRemoteControl_OPEN":
                    LogCat.d("Progress openBackDoor");
                    if (isPinCodeVerified(REQUEST_OPEN)) {
                        openBackDoor();
                    }
                    break;

                case "engineOffButtonClick":
                case "executeRemoteControl_ENGINE_OFF":
                    LogCat.d("Progress engineOff");
                    showProgressLayout(getString(R.string.remote_control_op_engine_off_progress_msg));
                    if (isPinCodeVerified(REQUEST_ENGINE_OFF)) {
                        engineOff();
                    }
                    break;
            }
        } else if(mProgress.equalsIgnoreCase("1")) {
            dismissProgressLayout();
            showConfirmDialog(getString(R.string.car_progress_in_workshop_error_title), getString(R.string.car_progress_in_workshop_lock_body));
        }
    }

    private OnApiListener<GetCarProgressInWorkshopResponseData> mGetCarProgressInWorkshopApiListener = new OnApiListener<GetCarProgressInWorkshopResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetCarProgressInWorkshopResponseData responseData) {
            ((RemoteControlActivity) getActivity()).setCheckCarProgress(false);
            if (responseData.isSuccessful()) {
                if(responseData.getWsResult() != null) {
                    LogCat.d("Workshop-Progress1:"+responseData.getWsResult().getmProgress());
                    Prefs.setRepairProgress(getContext(), responseData.getWsResult().getmProgress());
                    doProgressAction(responseData.getWsResult().getmProgress());
                }
            } else {
                if(responseData.getErrorCode().equalsIgnoreCase("-9999")) {
                    showConfirmDialog(getString(R.string.car_progress_in_workshop_error_title), getString(R.string.car_progress_in_workshop_error_body));
                } else if(responseData.getErrorCode().equalsIgnoreCase("-1017") || responseData.getErrorCode().equalsIgnoreCase("-889")) {
                    LogCat.d("Workshop-getErrorMessage:"+responseData.getErrorMessage());
                    Prefs.setRepairProgress(getContext(), "0");
                    doProgressAction("0");
                } else {
                    showServerErrorDialog(responseData.getErrorMessage());
                }
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            LogCat.e("Workshop-failMessage:"+failMessage);
            ((RemoteControlActivity) getActivity()).setCheckCarProgress(false);
            String mProgress = Prefs.getRepairProgress(getContext());
            doProgressAction(mProgress);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {

        }
    };

    private void checkCarProgress() {
        ((RemoteControlActivity) getActivity()).callAPIToGetCarProgressInWorkshop(mGetCarProgressInWorkshopApiListener);
        ((RemoteControlActivity) getActivity()).setCheckCarProgress(true);
    }
}

