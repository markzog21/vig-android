package com.luxgen.remote.ui.dialog;


import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.luxgen.remote.R;

public class CustomProgressDialog extends Dialog {

    private TextView mMessageTextView = null;

    public CustomProgressDialog(Context context) {
        super(context);
        init(context);
    }

    public CustomProgressDialog(Context context, int theme) {
        super(context, theme);
        init(context);
    }

    public CustomProgressDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init(context);
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init(Context context) {
        View contentView = getLayoutInflater().inflate(R.layout.dialog_progress, null, false);
        mMessageTextView = contentView.findViewById(R.id.dialog_progress_message_text_view);
        setContentView(contentView);
    }

    public void setMessage(String message) {
        mMessageTextView.setText(message);
    }

}
