package com.luxgen.remote.ui.maintenance;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class MaintenancePagerAdapter extends FragmentStatePagerAdapter {
    private List<String> mPageTitles = new ArrayList<>();
    private List<Fragment> mPages = new ArrayList<>();
    private String mCarNo = null;

    public MaintenancePagerAdapter(FragmentManager fm) {
        super(fm);

        initialPages();
    }

    public MaintenancePagerAdapter(FragmentManager fm, String carNo) {
        super(fm);
        mCarNo = carNo;
        initialPages();
    }

    private void addSchedule() {
        MaintenanceScheduleFragment mMaintenanceScheduleFragment = MaintenanceScheduleFragment.newInstance(mCarNo);
        mPages.add(HostFragment.newInstance(mMaintenanceScheduleFragment));
        mPageTitles.add("保修預約");
    }

    private void addReservations() {
        mPages.add(MaintenanceReservationsFragment.newInstance(mCarNo));
        mPageTitles.add("您的預約");
    }

    private void addProgress() {
        mPages.add(MaintenanceProgressFragment.newInstance(mCarNo));
        mPageTitles.add("保修進度");
    }

    private void addHistory() {
        mPages.add(MaintenanceHistoryFragment.newInstance(mCarNo));
        mPageTitles.add("維修記錄");
    }

    private void initialPages() {
        addSchedule();
        addReservations();
        addProgress();
        addHistory();
    }

    @Override
    public Fragment getItem(int position) {
        return mPages.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mPageTitles.get(position);
    }

    @Override
    public int getCount() {
        return mPages.size();
    }
}

