package com.luxgen.remote.ui.oks;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;

public class CarStolenFragment extends CustomBaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_car_stolen, container, false);

        Button wholeButton = v.findViewById(R.id.btn_whole_car);
        wholeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show(new CarStolenWholeFragment());
            }
        });

        Button partButton = v.findViewById(R.id.btn_part);
        partButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show(new CarStolenPartFragment());
            }
        });

        return v;
    }
}
