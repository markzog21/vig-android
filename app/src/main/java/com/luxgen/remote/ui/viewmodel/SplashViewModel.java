package com.luxgen.remote.ui.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.db.VigDatabase;

public class SplashViewModel extends BasePushViewModel {

    private VigDatabase mDatabase;
    private LiveData<UserInfo> mUser;

    public SplashViewModel(@NonNull Application application) {
        super(application);

        mDatabase = VigDatabase.getInstance(this.getApplication());
        mUser = mDatabase.userModel().get();
    }

    public UserInfo getUserInfo() {
        return mUser.getValue();
    }

    public void update(UserInfo user) {

    }
}
