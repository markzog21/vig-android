package com.luxgen.remote.ui.viewmodel;

import android.app.Application;

import com.luxgen.remote.model.PushData;
import com.luxgen.remote.model.option.NotifactionOptions;
import com.luxgen.remote.network.model.assistant.UseLogData;
import com.luxgen.remote.util.TimeUtils;

import java.text.ParseException;
import java.util.ArrayList;

public class MainViewModel extends BasePushViewModel {

    UseLogRepository mUseLogRepo;
    PushInfoRepository mPushRepo;

    public MainViewModel(Application application, UseLogRepository useLogRepo, PushInfoRepository pushRepo) {
        super(application);

        mUseLogRepo = useLogRepo;
        mPushRepo = pushRepo;
    }

    /**
     * 判斷列表中是否有未讀狀態推播
     */
    public PushData hasUnReadPushMessage(ArrayList<PushData> list) {

        PushData result = null;

        refreshPush(list);
        for (int index = list.size() - 1; index >= 0; index--) {
            final PushData data = list.get(index);
            if (NotifactionOptions.isUnRead(data.getReadStatus())) {
                result = data;
                break;
            }
        }

        return result;
    }

    public void syncUseLog() {
        mUseLogRepo.syncToNetwork();
    }

    /**
     * 只有在首頁被啟動時才需要被觸發
     */
    public void setUseLog() {
        UseLogData data = null;
        try {
            data = new UseLogData(getUserInfo().getPhone(), "首頁"
                    , TimeUtils.parseToServerFormateString(TimeUtils.getCurrentTime()));
            data.setCategory("Homepage");
            data.setAction("PageView");
            mUseLogRepo.setUseLog(getUserInfo(), data);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void logout() {
        if (mPushRepo != null) {
            mPushRepo.clearDb();
        }
    }
}