package com.luxgen.remote.ui.maintenance;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.ui.dialog.AlertDialogFragment;
import com.luxgen.remote.ui.dialog.ProgressDialogFragment;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.maintenance.GetRepairRequestData;
import com.luxgen.remote.network.model.maintenance.GetRepairResponseData;
import com.luxgen.remote.network.model.maintenance.GetWorkShopCapacityRequestData;
import com.luxgen.remote.network.model.maintenance.GetWorkShopCapacityResponseData;
import com.luxgen.remote.network.model.maintenance.GetWorkShopRequestData;
import com.luxgen.remote.network.model.maintenance.GetWorkShopResponseData;
import com.luxgen.remote.network.model.maintenance.WorkShopData;
import com.luxgen.remote.ui.dialog.ItemPickerDialogFragment;
import com.luxgen.remote.util.Prefs;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;

public class MaintenanceScheduleFragment2 extends CustomBaseFragment implements
        DatePickerDialog.OnDateSetListener, DialogInterface.OnCancelListener, TimePickerDialog.OnTimeSetListener {

    private final static int MAX_RESERVATION_PERIOD = 14; // days
    private final static int DAYS_CAN_RESERVER_FROM_TODAY = 3; // days

    private Button mLocationButton;
    private Button mPlantButton;
    private Button mDateButton;

    private int mYear = -1, mMonth = -1, mDay = -1;
    private int mHour = 0, mMinute = 0;

    private String mPlate;

    private int mCitySelection = -1;
    private String[] mCities;
    private String mCity;

    private int mPlantSelection = -1;
    private ArrayList<WorkShopData> mWorkshopArrayList;
    private String mPlant, mDealerCode, mDeptCode;

    private ArrayList<String> mAvailableDaysFromWorkshop;

    java.text.DateFormat mDateFormatter = java.text.DateFormat.getDateInstance(java.text.DateFormat.FULL);
    Calendar mChosenDate = Calendar.getInstance();

    private boolean mFetchData = false;

    private ProgressDialogFragment mProgressDialogFragment;

    private void dismissDialog() {
        if (isAdded() && mProgressDialogFragment != null) {
            mProgressDialogFragment.dismiss();
        }
    }

    private OnApiListener<GetWorkShopResponseData> mGetWorkshopDataApiListener = new OnApiListener<GetWorkShopResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetWorkShopResponseData responseData) {
            dismissDialog();
            if (responseData.isSuccessful()) {
                mWorkshopArrayList = responseData.getList();
                showPlantPicker();
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissDialog();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mProgressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.maintenance_fetching_data));
            mProgressDialogFragment.show(getFragmentManager(), "getRepairDataDialog");
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            dismissDialog();
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetWorkShopResponseData> mGetWorkshopDataApiForInitListener = new OnApiListener<GetWorkShopResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetWorkShopResponseData responseData) {
            dismissDialog();
            if (responseData.isSuccessful()) {
                mWorkshopArrayList = responseData.getList();
                restoreLastSelectedPlant();
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissDialog();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mProgressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.maintenance_fetching_data));
            mProgressDialogFragment.show(getFragmentManager(), "getRepairDataDialog");
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            dismissDialog();
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetWorkShopCapacityResponseData> mOnGetWorkShopCapacityApiListener = new OnApiListener<GetWorkShopCapacityResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetWorkShopCapacityResponseData responseData) {
            dismissDialog();
            if (responseData.isSuccessful()) {
                mAvailableDaysFromWorkshop = responseData.getDay();
                showDatePicker();
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissDialog();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mProgressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.maintenance_fetching_data));
            mProgressDialogFragment.show(getFragmentManager(), "getWorkShopCapacityDialog");
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            dismissDialog();
            showMultiLoginErrorDialog();
        }
    };

    private ItemPickerDialogFragment.OnItemSelectedListener mOnCitySelectedListener = new ItemPickerDialogFragment.OnItemSelectedListener() {
        @Override
        public void onItemSelected(ItemPickerDialogFragment fragment, ItemPickerDialogFragment.Item item, int index) {
            if (mCitySelection != index) {
                mCitySelection = index;
                mCity = item.getTitle();
                Prefs.setMaintenanceCity(getActivity(), mCity);

                mWorkshopArrayList = null;
                mPlantSelection = -1;
                Prefs.clearMaintenancePlant(getActivity());
                mPlantButton.setText(R.string.maintenance_schedule_plant_hint);
                mDealerCode = null;
                mDeptCode = null;
                mYear = -1;
                mMonth = -1;
                mDay = -1;
                mDateButton.setText(R.string.maintenance_schedule_date_hint);

                mLocationButton.setText(mCity);
                mPlantButton.setEnabled(true);
            }
        }
    };

    private ItemPickerDialogFragment.OnItemSelectedListener mOnPlantSelectedListener = new ItemPickerDialogFragment.OnItemSelectedListener() {
        @Override
        public void onItemSelected(ItemPickerDialogFragment fragment, ItemPickerDialogFragment.Item item, int index) {
            if (!item.getStringValue().equals(mDeptCode)) {
                mPlantSelection = index;
                mPlant = item.getTitle();
                Prefs.setMaintenancePlant(getActivity(), mPlant);

                mDeptCode = item.getStringValue();
                mDealerCode = getDealerCode(mWorkshopArrayList, mDeptCode);

                mYear = -1;
                mMonth = -1;
                mDay = -1;
                mDateButton.setText(R.string.maintenance_schedule_date_hint);

                mPlantButton.setText(mPlant);
            }
        }
    };

    public static MaintenanceScheduleFragment2 newInstance(boolean fetch, String plate, String mileage, boolean schedule, boolean repair, String notes, String[] cities) {
        Bundle args = new Bundle();
        args.putBoolean("fetch", fetch);
        args.putString("plate", plate);
        args.putString("mileage", mileage);
        args.putBoolean("schedule", schedule);
        args.putBoolean("repair", repair);
        args.putString("notes", notes);
        args.putStringArray("cities", cities);

        MaintenanceScheduleFragment2 f = new MaintenanceScheduleFragment2();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("mPlate", mPlate);
        outState.putInt("mCitySelection", mCitySelection);
        outState.putString("mCity", mCity);
        outState.putInt("mPlantSelection", mPlantSelection);
        outState.putString("mPlant", mPlant);
        outState.putString("mDealerCode", mDealerCode);
        outState.putString("mDeptCode", mDeptCode);

        outState.putInt("mYear", mYear);
        outState.putInt("mMonth", mMonth);
        outState.putInt("mDay", mDay);
        outState.putInt("mHour", mHour);
        outState.putInt("mMinute", mMinute);
    }

    private void restoreArgsAndSavedInstanceState(Bundle args, Bundle savedInstanceState) {
        if (args != null) {
            mPlate = args.getString("plate");
            mCities = args.getStringArray("cities");
            mFetchData = args.getBoolean("fetch");
        }

        if (savedInstanceState == null) {
            return;
        }

        mPlate = savedInstanceState.getString("mPlate", mPlate);
        mCitySelection = savedInstanceState.getInt("mCitySelection", mCitySelection);
        mCity = savedInstanceState.getString("mCity", mCity);
        mPlantSelection = savedInstanceState.getInt("mPlantSelection", mPlantSelection);
        mPlant = savedInstanceState.getString("mPlant", mPlant);
        mDealerCode = savedInstanceState.getString("mDealerCode", mDealerCode);
        mDeptCode = savedInstanceState.getString("mDeptCode", mDeptCode);

        mYear = savedInstanceState.getInt("mYear", mYear);
        mMonth = savedInstanceState.getInt("mMonth", mMonth);
        mDay = savedInstanceState.getInt("mDay", mDay);
        mHour = savedInstanceState.getInt("mHour", mHour);
        mMinute = savedInstanceState.getInt("mMinute", mMinute);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_maintenance_schedule_2, container, false);

        restoreArgsAndSavedInstanceState(getArguments(), savedInstanceState);

        mLocationButton = v.findViewById(R.id.btn_location);
        mLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCityPicker();
            }
        });

        mPlantButton = v.findViewById(R.id.btn_plant);
        mPlantButton.setEnabled(false);
        mPlantButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchWorkshopList(mCity);
            }
        });

        mDateButton = v.findViewById(R.id.btn_date);
        mDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchSelectableDays(mDealerCode, mDeptCode);
            }
        });

        Button prevButton = v.findViewById(R.id.btn_prev);
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popFragment();
            }
        });

        Button nextButton = v.findViewById(R.id.btn_next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                continueStep3();
            }
        });

        setupTextOnButtons();

        if (mFetchData) {
            getRepairData(mPlate);
        } else {
            restoreLastSelectedCity();
        }

        return v;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        mYear = year;
        mMonth = monthOfYear;
        mDay = dayOfMonth;

        showTimePicker(mHour, mMinute);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        mHour = hourOfDay;
        mMinute = minute;

        setupDateButton();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        mYear = -1;
        mMonth = -1;
        mDay = -1;
        mHour = 0;
        mMinute = 0;
        mDateButton.setText(R.string.maintenance_schedule_date_hint);
    }

    private void setupDateButton() {
        mChosenDate.set(mYear, mMonth, mDay, mHour, mMinute);

        mDateFormatter.setTimeZone(mChosenDate.getTimeZone());
        String formatted = mDateFormatter.format(mChosenDate.getTime());

        mDateButton.setText(String.format(Locale.TAIWAN, "%s %02d:%02d", formatted, mHour, mMinute));
    }

    private void setupTextOnButtons() {
        if (mCity != null) {
            mLocationButton.setText(mCity);
        }

        if (mPlant != null) {
            mPlantButton.setText(mPlant);
        }

        if (mYear != -1 && mMonth != -1 && mDay != -1) {
            setupDateButton();
        }
    }

    private void showCityPicker() {
        ArrayList<ItemPickerDialogFragment.Item> cityArrayList = buildCityListForPicker(mCities);

        // TODO: show after fetch done
        ItemPickerDialogFragment dialog = ItemPickerDialogFragment.newInstance("地區",
                cityArrayList, mCitySelection);

        dialog.setOnItemSelectedListener(mOnCitySelectedListener);
        dialog.show(getChildFragmentManager(), "CityPicker");
    }

    private void showPlantPicker() {
        ArrayList<ItemPickerDialogFragment.Item> plantArrayList = buildPlantListForPicker(mWorkshopArrayList);

        ItemPickerDialogFragment dialog = ItemPickerDialogFragment.newInstance("服務廠",
                plantArrayList, mPlantSelection);

        dialog.setOnItemSelectedListener(mOnPlantSelectedListener);
        dialog.show(getChildFragmentManager(), "PlantPicker");
    }

    private void showDatePicker() {
        initialDates();
        Calendar[] selectableDays = buildSelectableDaysForPicker(mAvailableDaysFromWorkshop);

        DatePickerDialog dpd = DatePickerDialog.newInstance(MaintenanceScheduleFragment2.this,
                mYear, mMonth, mDay);
        dpd.setOnCancelListener(MaintenanceScheduleFragment2.this);

        dpd.setSelectableDays(selectableDays);
        dpd.show(getChildFragmentManager(), "DatePicker");
    }

    private void showTimePicker(int hour, int minute) {
        String date = String.format(Locale.TAIWAN, "%4d/%02d/%02d", mYear, mMonth + 1, mDay);
        Timepoint[] selectableTimes = buildSelectableHoursForPicker(mAvailableDaysFromWorkshop, date);
        TimePickerDialog tpd = TimePickerDialog.newInstance(MaintenanceScheduleFragment2.this,
                hour, minute, true);
        tpd.setOnCancelListener(MaintenanceScheduleFragment2.this);

        tpd.setSelectableTimes(selectableTimes);
        tpd.show(getChildFragmentManager(), "TimePicker");
    }

    private void initialDates() {
        if (mYear == -1) {
            mYear = mChosenDate.get(Calendar.YEAR);
        }
        if (mMonth == -1) {
            mMonth = mChosenDate.get(Calendar.MONTH);
        }
        if (mDay == -1) {
            mDay = mChosenDate.get(Calendar.DAY_OF_MONTH);
        }
    }

    private ArrayList<ItemPickerDialogFragment.Item> buildCityListForPicker(String[] cities) {
        ArrayList<ItemPickerDialogFragment.Item> cityArrayList = new ArrayList<>();
        for (int i = 0; i < cities.length; i++) {
            cityArrayList.add(new ItemPickerDialogFragment.Item(cities[i], i));
        }

        return cityArrayList;
    }

    private ArrayList<ItemPickerDialogFragment.Item> buildPlantListForPicker(ArrayList<WorkShopData> workShops) {
        ArrayList<ItemPickerDialogFragment.Item> plantArrayList = new ArrayList<>();
        for (WorkShopData workShop : workShops) {
            plantArrayList.add(new ItemPickerDialogFragment.Item(workShop.getDeptName(), workShop.getDeptCode()));
        }
        return plantArrayList;
    }

    private class SortByDate implements Comparator<Calendar> {
        @Override
        public int compare(Calendar o1, Calendar o2) {
            return o1.compareTo(o2);
        }
    }

    private Calendar[] buildSelectableDaysForPicker(ArrayList<String> availableDays) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.TAIWAN);
        SortedSet<Calendar> daysSet = new TreeSet<>(new SortByDate());
        for (String input : availableDays) {
            // format: 2018-05-15 08:00-09:00
            String[] splited = input.split("\\s");
            try {
                Calendar calendar = Calendar.getInstance();
                Date date = sdf.parse(splited[0]);
                calendar.setTime(date);
                daysSet.add(calendar);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return daysSet.toArray(new Calendar[daysSet.size()]);
    }

    private class SortByHour implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            int h1 = Integer.parseInt(o1.replace(":", ""));
            int h2 = Integer.parseInt(o2.replace(":", ""));

            return h1 - h2;
        }
    }

    private Timepoint[] buildSelectableHoursForPicker(ArrayList<String> availableDays, String date) {
        ArrayList<Timepoint> hoursArrayList = new ArrayList<>();
        for (String input : availableDays) {
            // format: 2018-05-15 08:00-09:00
            if (input.startsWith(date)) {
                String realHourStr = input.substring(input.indexOf(' ') + 1, input.indexOf(':'));
                String realMinStr = input.substring(input.indexOf(':') + 1, input.length());
                Timepoint timepoint = new Timepoint(Integer.parseInt(realHourStr), Integer.parseInt(realMinStr));
                hoursArrayList.add(timepoint);
            }
        }

        return hoursArrayList.toArray(new Timepoint[hoursArrayList.size()]);
    }

    private void fetchWorkshopList(String city) {
        GetWorkShopRequestData data = new GetWorkShopRequestData(city);

        AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(getContext());
        assistantWebApi.getWorkshopData(data, mGetWorkshopDataApiListener);
    }

    private void fetchWorkshopListForInit(String city) {
        GetWorkShopRequestData data = new GetWorkShopRequestData(city);

        AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(getContext());
        assistantWebApi.getWorkshopData(data, mGetWorkshopDataApiForInitListener);
    }

    private String getDateString(int daysAfter) {
        Calendar calendar = Calendar.getInstance();
        long time = calendar.getTimeInMillis() + 60 * 60 * 24 * 1000 * daysAfter;
        calendar.setTimeInMillis(time);
        return String.format(Locale.TAIWAN, "%4d/%02d/%02d", calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
    }

    private void fetchSelectableDays(String dealerCode, String deptCode) {
        if (!TextUtils.isEmpty(dealerCode) && !TextUtils.isEmpty(deptCode)) {
            String startDate = getDateString(DAYS_CAN_RESERVER_FROM_TODAY);
            String endDate = getDateString(DAYS_CAN_RESERVER_FROM_TODAY + MAX_RESERVATION_PERIOD - 1);
            GetWorkShopCapacityRequestData data = new GetWorkShopCapacityRequestData(dealerCode, deptCode, startDate, endDate);

            AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(getContext());
            assistantWebApi.getWorkShopCapacity(data, mOnGetWorkShopCapacityApiListener);
        }
    }

    private String getDealerCode(ArrayList<WorkShopData> workShops, String depCode) {
        for (WorkShopData workShop : workShops) {
            if (workShop.getDeptCode().equals(depCode)) {
                return workShop.getDealerCode();
            }
        }

        return null;
    }

    private boolean isAllFieldOk() {
        return mCitySelection != -1 && mPlantSelection != -1 && mYear != -1 && mMonth != -1 && mDay != -1;
    }

    private void continueStep3() {
        if (!isAllFieldOk()) {
            return;
        }

        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        args.putString("plate", mPlate);
        startFragment(MaintenanceScheduleFragment3.newInstance(args, mChosenDate.getTimeInMillis(), mPlant, mDealerCode, mDeptCode));
    }

    private void startFragment(Fragment fragment) {
        ((MaintenanceActivity) getActivity()).startFragmentInHost(fragment);
    }

    private OnApiListener<GetRepairResponseData> mGetRepairDataApiListener = new OnApiListener<GetRepairResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetRepairResponseData responseData) {
            dismissDialog();
            if (responseData.isSuccessful()) {
                mCities = responseData.getCountries();
                restoreLastSelectedCity();
            } else {
                showServerErrorDialog(null, responseData.getErrorMessage(), new AlertDialogFragment.Callbacks() {
                    @Override
                    public void onPositiveButtonClicked() {
                        popFragment();
                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }

                    @Override
                    public void onBackKeyPressed() {
                        popFragment();
                    }
                });
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissDialog();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mProgressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.maintenance_fetching_data));
            mProgressDialogFragment.show(getFragmentManager(), "getRepairDataDialog");
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            dismissDialog();
            showMultiLoginErrorDialog();
        }
    };

    private void getRepairData(String carNo) {
        UserInfo userInfo = Prefs.getUserInfo(getContext());
        if (userInfo == null) {
            return;
        }

        String iKeyUid = userInfo.getKeyUid();
        String iKeyToken = userInfo.getToken();

        GetRepairRequestData data = new GetRepairRequestData(iKeyUid, iKeyToken, carNo);

        AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(getContext());
        assistantWebApi.getRepairData(data, mGetRepairDataApiListener);
    }

    private void restoreLastSelectedCity() {
        if (getActivity() == null) {
            return;
        }

        String city = Prefs.getMaintenanceCity(getActivity());
        if (city != null) {
            if (mCities != null && mCities.length > 0) {
                for (int index = 0; index < mCities.length; index++) {
                    if (mCities[index].equals(city)) {
                        mCitySelection = index;
                        mCity = city;
                        mPlantButton.setEnabled(true);
                        mWorkshopArrayList = null;
                        mPlantSelection = -1;
                        mLocationButton.setText(mCity);
                        String plant = Prefs.getMaintenancePlant(getActivity());
                        if (plant != null) {
                            fetchWorkshopListForInit(mCity);
                        }

                        break;
                    }
                }
            }
        }
    }

    private void restoreLastSelectedPlant() {
        if (getActivity() == null) {
            return;
        }

        String plant = Prefs.getMaintenancePlant(getActivity());
        if (plant != null) {
            if (mWorkshopArrayList != null && mWorkshopArrayList.size() > 0) {
                ArrayList<ItemPickerDialogFragment.Item> plantArrayList = buildPlantListForPicker(mWorkshopArrayList);
                if (plantArrayList != null && plantArrayList.size() > 0) {
                    for (int index = 0; index < plantArrayList.size(); index++) {
                        ItemPickerDialogFragment.Item item = plantArrayList.get(index);
                        if (item.getTitle().equals(plant)) {
                            mPlantSelection = index;
                            mPlant = plant;
                            mDeptCode = item.getStringValue();
                            mDealerCode = getDealerCode(mWorkshopArrayList, mDeptCode);
                            mPlantButton.setText(mPlant);

                            break;
                        }
                    }
                }
            }
        }
    }
}
