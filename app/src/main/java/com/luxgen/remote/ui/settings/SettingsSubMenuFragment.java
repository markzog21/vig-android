package com.luxgen.remote.ui.settings;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luxgen.remote.custom.CustomBaseFragment;

public class SettingsSubMenuFragment extends CustomBaseFragment {

    public static SettingsSubMenuFragment newInstance(@StringRes int title, @LayoutRes int layout) {
        SettingsSubMenuFragment f = new SettingsSubMenuFragment();

        Bundle args = new Bundle();
        args.putInt("title", title);
        args.putInt("layout", layout);
        f.setArguments(args);

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(getArguments().getInt("title"));

        return inflater.inflate(getArguments().getInt("layout"), container, false);
    }
}
