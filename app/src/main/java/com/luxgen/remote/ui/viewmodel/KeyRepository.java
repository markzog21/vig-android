package com.luxgen.remote.ui.viewmodel;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.IntDef;
import android.util.Log;

import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.model.KeyInfo;
import com.luxgen.remote.model.db.KeyDao;
import com.luxgen.remote.model.db.VigDatabase;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.car.GetCarKeyRequestData;
import com.luxgen.remote.util.StringUtils;
import com.luxgen.remote.util.TimeUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.inject.Singleton;

/**
 * 為了配合數位鑰匙的呈現順序，這邊做了幾個設計 <br />
 * <ul>
 * <li>針對不同鑰匙種類定義位置，三位數格式：[key_type](1)[position](2)</li>
 * <ul>
 * <li>鑰匙種類(key_type)：</li>
 * <ul>
 * <li>1. Master Key</li>
 * <li>2. Sub Key</li>
 * <li>3. Snap Key</li>
 * </ul>
 * <li>位置(position)，兩位數，不足補 0</li>
 * </ul>
 * </ul>
 */
@Singleton
public class KeyRepository {
    private static final int MASTER_KEY_POSITION1 = 101;
    private static final int SUB_KEY_POSITION1 = 201;
    private static final int SUB_KEY_POSITION2 = 202;
    private static final int SUB_KEY_POSITION3 = 203;
    private static final int SUB_KEY_POSITION4 = 204;
    private static final int SNAP_KEY_POSITION1 = 301;
    private static final int SNAP_KEY_POSITION2 = 302;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({
            MASTER_KEY_POSITION1,
            SUB_KEY_POSITION1, SUB_KEY_POSITION2, SUB_KEY_POSITION3, SUB_KEY_POSITION4,
            SNAP_KEY_POSITION1, SNAP_KEY_POSITION2
    })
    public @interface KeyPosition {
    }

    /**
     * 先以陣列定義鑰匙順序。
     */
    private static final int[] KEY_POSITIONS = {
            MASTER_KEY_POSITION1,
            SUB_KEY_POSITION1, SUB_KEY_POSITION2, SUB_KEY_POSITION3, SUB_KEY_POSITION4,
            SNAP_KEY_POSITION1, SNAP_KEY_POSITION2
    };

    public static int getKeyPosition(int positionInUI) {
        if (positionInUI < 0 || positionInUI >= KEY_POSITIONS.length - 1) {
            return -1;
        }
        return KEY_POSITIONS[positionInUI + 1];
    }

    /**
     * 取得預設定義的鑰匙總數。
     */
    public static int getDefaultSize() {
        return KEY_POSITIONS.length;
    }

    private Context context;
    private VigDatabase database;

    public KeyRepository(Context context, VigDatabase database) {
        this.context = context;
        this.database = database;
    }

    public Boolean updateToDb(KeyInfo keyInfo) {
        if (keyInfo.isNew()) {
            LogCat.e("更新的這個 KeyInfo 不是從 db 裡取出的資料");
            return false;
        }

        try {
            return new UpdateKeyInfoTask(this.database.keyModel(), keyInfo).execute().get();
        } catch (InterruptedException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (ExecutionException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        }
        return false;
    }

    public ArrayList<KeyInfo> fetchListFromDb(String carId) {
        try {
            return new FetchKeyListTask(this.database.keyModel(), carId).execute().get();
        } catch (InterruptedException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (ExecutionException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        }
        return null;
    }

    private class UpdateKeyInfoTask extends AsyncTask<Void, Void, Boolean> {

        private KeyDao dao;
        private KeyInfo keyInfo;

        UpdateKeyInfoTask(KeyDao dao, KeyInfo info) {
            this.dao = dao;
            this.keyInfo = info;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            this.keyInfo.setModifyTime(TimeUtils.getCurrentTime());
            return this.dao.update(this.keyInfo) > 0;
        }
    }

    private class FetchKeyListTask extends AsyncTask<Void, Void, ArrayList<KeyInfo>> {

        private KeyDao dao;
        private String carId;

        FetchKeyListTask(KeyDao dao, String carId) {
            this.dao = dao;
            this.carId = carId;
        }

        @Override
        protected ArrayList<KeyInfo> doInBackground(Void... voids) {
            List<KeyInfo> list = dao.fetchList(this.carId);
            return (ArrayList<KeyInfo>) list;
        }
    }

    /* ----- Start: App 登入或是打開時使用 ----- */

    /**
     * 判斷初始化資料，在 app 剛登入時，或是再次進入 app 時，
     * 在呼叫 {@link com.luxgen.remote.network.CarWebApi#getCarKeyList(GetCarKeyRequestData, OnApiListener)} 後
     * ，確認 local db 中是否已有其中的 master 的對應資料。<br />
     * 若有，則再確認 master 的 key type 是否有變動過 <br />
     * 若無，則增以該 carId 建立將給 {@link com.luxgen.remote.activity.DigitalKeyActivity} 使用的預設資料
     *
     * @return <code>true</code> 順利執行完畢;
     * <code>false</code> 有中斷或是哪裡發生錯誤了
     */
    public Boolean checkInitData(ArrayList<CarKeyData> list) {
        try {
            return new CheckKeyInfoTask(database.keyModel(), list).execute().get();
        } catch (InterruptedException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (ExecutionException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        }
        return false;
    }

    /**
     * 當 DB 裡沒有該 {@link KeyInfo#carId} 對應資料時的初始化資料
     */
    private static ArrayList<KeyInfo> getDefaultList(CarKeyData data) {
        ArrayList<KeyInfo> list = new ArrayList<>();
        for (int position : KEY_POSITIONS) {
            final KeyInfo info = initData(data.getCarId(), position);
            if (position == MASTER_KEY_POSITION1) {
                info.setKeyType(data.getKeyType());
            }
            list.add(info);
        }
        return list;
    }

    private static KeyInfo initData(String carId, int position) {
        KeyInfo data = new KeyInfo();
        data.setCarId(carId);
        data.setPosition(position);
        data.setCreateTime(TimeUtils.getCurrentTime());
        return data;
    }

    /**
     * 根據 carId 取得存在本機端的 Key info list <br />
     * 在取出時，會判斷當資料為空先塞入預設的資料進去
     */
    private class CheckKeyInfoTask extends AsyncTask<Void, Void, Boolean> {

        private KeyDao dao;
        private ArrayList<CarKeyData> remoteList;


        final String LOG_TEMP = "取得 CarKey 共 {keylist_size} 筆，" +
                "有 {{car_ids}} 共 {master_size} 筆 master key，" +
                "新增的 Car Id： {{add_ids}}";
        StringBuilder carIds;
        StringBuilder addIds;
        int master = 0;

        CheckKeyInfoTask(KeyDao dao, ArrayList<CarKeyData> listFromRemote) {
            this.dao = dao;
            this.remoteList = listFromRemote;
            this.carIds = new StringBuilder();
            this.addIds = new StringBuilder();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            boolean result = checkInitData();

            String log = "InitCarKeyInfo Table:\n" +
                    LOG_TEMP.replace("{keylist_size}", String.valueOf(this.remoteList.size()))
                            .replace("{car_ids}", carIds.toString())
                            .replace("{master_size}", String.valueOf(master))
                            .replace("{add_ids}", addIds.toString());

            LogCat.i(log);

            return result;
        }

        private boolean checkInitData() {
            try {

                for (CarKeyData data : this.remoteList) {
                    if (data.isMasterKey()) {
                        this.master++;
                        StringUtils.appendComma(this.carIds, data.getCarId());

                        List<KeyInfo> dbData = this.dao.fetchList(data.getCarId());
                        if (dbData == null || dbData.size() <= 0) {
                            ArrayList<KeyInfo> initList = getDefaultList(data);
                            this.dao.insert(initList);

                            StringUtils.appendComma(this.addIds, data.getCarId());
                        } else {
                            KeyInfo master = checkMasterData(dbData, data);
                            if (master != null) {
                                master.setModifyTime(TimeUtils.getCurrentTime());
                                this.dao.update(master);
                            }
                        }
                    }
                }
                return true;
            } catch (Exception ex) {
                Log.e("checkInitData()", ex.toString());
                return false;
            }
        }

        private KeyInfo checkMasterData(List<KeyInfo> listFromDb, CarKeyData dataFromRemote) {
            for (KeyInfo keyInfo : listFromDb) {
                if (keyInfo.getPosition() == MASTER_KEY_POSITION1) {
                    if (keyInfo.checkKeyTypeAndChange(dataFromRemote.getKeyType())) {
                        return keyInfo;
                    }
                }
            }

            return null;
        }
    }
    /* ----- End: App 登入或是打開時使用 ----- */
}
