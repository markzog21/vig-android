package com.luxgen.remote.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.luxgen.remote.R;

public class ProgressDialogFragment extends DialogFragment {

    public static ProgressDialogFragment newInstance(String message) {
        Bundle args = new Bundle();
        args.putString("message", message);

        ProgressDialogFragment f = new ProgressDialogFragment();
        f.setArguments(args);
        return f;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        String message = args.getString("message");

        CustomProgressDialog progressDialog = new CustomProgressDialog(getContext(), R.style.CustomProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);

        return progressDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
        setCancelable(false);
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }

        super.onDestroyView();
    }
}
