package com.luxgen.remote.ui.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.luxgen.remote.model.PushData;
import com.luxgen.remote.network.AssistantWebApi;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class BasePushViewModel extends BaseKeyViewModel {

    private PushInfoRepository mRepo;

    private LiveData<List<PushData>> mPushListData;

    public LiveData<List<PushData>> getPushList() {
        return mPushListData;
    }

    public BasePushViewModel(@NonNull Application application) {
        super(application);
        mRepo = new PushInfoRepository(getDatabase().pushModel(), AssistantWebApi.getInstance(application.getApplicationContext()));
        mPushListData = mRepo.loadFromDb();
    }

    public void update(PushData data) {
        try {
            new PushInfoRepository.UpdateDbAsyncTask(getDatabase().pushModel()).execute(data).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void update(ArrayList<PushData> list) {
        try {
            new PushInfoRepository.UpdateDbAsyncTask(getDatabase().pushModel(), list).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void syncPushDataToServer() {
        mRepo.syncDbToNetwork(getUserInfo());
    }

    protected void refreshPush(ArrayList<PushData> list) {
        new PushInfoRepository.refreshDbAsyncTask(getDatabase(), list).execute();
    }

    public void insert(ArrayList<PushData> list) {
        new PushInfoRepository.InsertDbAsyncTask(getDatabase().pushModel(), list).execute();
    }

    public void clear() {
        mRepo.clearDb();
    }
}
