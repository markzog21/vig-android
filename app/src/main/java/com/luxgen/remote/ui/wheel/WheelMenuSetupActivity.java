package com.luxgen.remote.ui.wheel;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomFragmentsAppCompatActivity;

public class WheelMenuSetupActivity extends CustomFragmentsAppCompatActivity {

    public final static String FRAGMENT_TAG_WHEEL_MENU_SETUP = "wheelmenusetup";
    public final static String FRAGMENT_TAG_WHEEL_MENU_SETUP_CONTROL = "wheelmenusetupcontrol";
    public final static String FRAGMENT_TAG_WHEEL_MENU_ITEM_CHOOSER = "wheelmenuitemchooser";

    public static Intent newIntent(Context context) {
        return new Intent(context, WheelMenuSetupActivity.class);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_wheel_menu_setup;
    }

    @Override
    protected void setupTitle(String tag) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupTitleChangeListener();

        setTitle(R.string.wheel_menu_setup_title);

        show(WheelMenuSetupFragment.newInstance(), FRAGMENT_TAG_WHEEL_MENU_SETUP);
        bindCaService();
    }

    @Override
    protected void onDestroy() {
        unbindCaService();
        super.onDestroy();
    }

    private void setupTitleChangeListener() {
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                String tag = fragment == null ? "" : fragment.getTag();
                if (FRAGMENT_TAG_WHEEL_MENU_SETUP.equals(tag)) {
                    setTitle(R.string.wheel_menu_setup_title);
                } else if (FRAGMENT_TAG_WHEEL_MENU_SETUP_CONTROL.equals(tag)) {
                    setTitle(R.string.wheel_menu_setup_control_title);
                } else if (FRAGMENT_TAG_WHEEL_MENU_ITEM_CHOOSER.equals(tag)) {
                    setTitle(R.string.wheel_menu_setup_shortcut_title);
                }
            }
        });
    }
}
