package com.luxgen.remote.ui.viewmodel;

import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.model.PushData;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.db.PushDao;
import com.luxgen.remote.model.db.VigDatabase;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.assistant.SetPushRequestData;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.inject.Singleton;

@Singleton
public class PushInfoRepository {
    private static PushInfoRepository mInstance;

    public static PushInfoRepository getInstance(VigDatabase db, AssistantWebApi api) {
        if (mInstance == null) {
            mInstance = new PushInfoRepository(db.pushModel(), api);
        }
        return mInstance;
    }

    private PushDao mDao;
    private AssistantWebApi mApi;

    public PushInfoRepository(PushDao dao, AssistantWebApi api) {
        mDao = dao;
        mApi = api;
    }

    public LiveData<List<PushData>> loadFromDb() {
        return mDao.getPushList();
    }

    public void syncDbToNetwork(UserInfo info) {
        try {
            new SyncDbToNetworkAsyncTask(info, mDao, mApi).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clearDb() {
        new ClearDbAsyncTask(mDao).execute();
    }

    private static OnApiListener getSyncToNetworkListener(final PushDao dao, final ArrayList<PushData> list) {
        return new OnApiListener() {
            @Override
            public void onApiTaskSuccessful(Object responseData) {
                for (PushData data : list) {
                    data.setNeedSyncToServer(false);
                }

                new UpdateDbAsyncTask(dao, list).execute();
            }

            @Override
            public void onApiTaskFailure(String failMessage) {

            }

            @Override
            public void onPreApiTask() {

            }

            @Override
            public void onApiProgress(long value) {

            }

            @Override
            public void onAuthFailure() {

            }
        };
    }

    static class SyncDbToNetworkAsyncTask extends AsyncTask<Void, Void, Void> {

        private PushDao mDao;
        private AssistantWebApi mApi;
        private UserInfo mInfo;

        SyncDbToNetworkAsyncTask(UserInfo info, PushDao dao, AssistantWebApi api) {
            mInfo = info;
            mDao = dao;
            mApi = api;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            List<PushData> list = mDao.getNeedSyncList();

            if (list == null || list.size() <= 0) {
                return null;
            }

            SetPushRequestData request = new SetPushRequestData(mInfo.getKeyUid(), mInfo.getToken());
            for (PushData data : list) {
                request.setData(data);
            }
            OnApiListener listener = getSyncToNetworkListener(mDao, (ArrayList<PushData>) list);
            mApi.setPushStatus(request, listener);

            return null;
        }
    }

    static class ClearDbAsyncTask extends AsyncTask<Void, Void, Void> {

        private PushDao mDao;
        private ArrayList<PushData> mList;

        ClearDbAsyncTask(PushDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(Void... pushData) {
            mDao.clear();
            return null;
        }
    }

    static class InsertDbAsyncTask extends AsyncTask<PushData, Void, Void> {

        private PushDao mDao;
        private ArrayList<PushData> mList;

        InsertDbAsyncTask(PushDao dao) {
            mDao = dao;
        }

        InsertDbAsyncTask(PushDao dao, ArrayList<PushData> list) {
            mDao = dao;
            mList = list;
        }

        @Override
        protected Void doInBackground(PushData... pushData) {
            if (mList != null) {
                mDao.insert(mList);
            } else if (pushData.length > 0) {
                mDao.insert(pushData[0]);
            }
            return null;
        }
    }

    static class UpdateDbAsyncTask extends AsyncTask<PushData, Void, Void> {

        private PushDao mDao;
        private ArrayList<PushData> mList;

        UpdateDbAsyncTask(PushDao dao) {
            mDao = dao;
        }

        UpdateDbAsyncTask(PushDao dao, ArrayList<PushData> list) {
            mDao = dao;
            mList = list;
        }

        @Override
        protected Void doInBackground(PushData... pushData) {
            if (mList != null) {
                mDao.update(mList);
            } else if (pushData.length > 0) {
                mDao.update(pushData[0]);
            }
            return null;
        }
    }

    static class refreshDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private VigDatabase db;
        private ArrayList<PushData> list;

        refreshDbAsyncTask(VigDatabase db, ArrayList<PushData> list) {
            this.db = db;
            this.list = list;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            List<PushData> origin = this.db.pushModel().getList();

            if (origin == null || origin.size() == 0) {
                this.db.pushModel().insert(this.list);
            } else {
                LogCat.i("Push Data Size:" + origin.size());
                this.db.pushModel().update(this.list);
            }

            return null;
        }
    }
}
