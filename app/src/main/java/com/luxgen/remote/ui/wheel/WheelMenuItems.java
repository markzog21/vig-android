package com.luxgen.remote.ui.wheel;

import android.content.Context;
import android.content.Intent;

import com.luxgen.remote.R;
import com.luxgen.remote.ui.maintenance.MaintenanceQueryCarNoActivity;
import com.luxgen.remote.ui.oks.CarAccidentActivity;
import com.luxgen.remote.activity.CarStatusActivity;
import com.luxgen.remote.ui.oks.CarStolenActivity;
import com.luxgen.remote.ui.dashboard.DashboardActivity;
import com.luxgen.remote.activity.DigitalKeyActivity;
import com.luxgen.remote.activity.ETCBalanceActivity;
import com.luxgen.remote.activity.MainActivity;
import com.luxgen.remote.ui.maintenance.MaintenanceActivity;
import com.luxgen.remote.ui.arremote.RemoteControlActivity;
import com.luxgen.remote.ui.settings.SettingsActivity;
import com.luxgen.remote.model.WheelTileItem;
import com.luxgen.remote.util.DialUtils;

import java.util.ArrayList;

import static com.luxgen.remote.activity.MainActivity.INTENT_EXTRA_NAME_SHOW_CAR_KEY_LIST;
import static com.luxgen.remote.ui.arremote.RemoteControlActivity.FRAGMENT_TAG_REMOTE_CONTROL_MENU;
import static com.luxgen.remote.ui.arremote.RemoteControlFragment.REQUEST_ENGINE_OFF;
import static com.luxgen.remote.ui.arremote.RemoteControlFragment.REQUEST_FIND;
import static com.luxgen.remote.ui.arremote.RemoteControlFragment.REQUEST_LOCK;
import static com.luxgen.remote.ui.arremote.RemoteControlFragment.REQUEST_OPEN;
import static com.luxgen.remote.ui.arremote.RemoteControlFragment.REQUEST_OPTIMIZE;
import static com.luxgen.remote.ui.arremote.RemoteControlFragment.REQUEST_UNLOCK;

public class WheelMenuItems {

    public final static String EXTRA_CHECK_KEY = "extra_check_key";

    private static ArrayList<WheelTileItem> mItems = new ArrayList<WheelTileItem>() {{
        add(new WheelTileItem(R.drawable.ic_smart_wheel_ar_remote_white, R.drawable.ic_smart_wheel_set_remote, R.string.wheel_ar_remote_title, R.string.wheel_ar_remote_title_short, R.string.wheel_ar_remote_desc, WheelTileItem.ACTION_AR_REMOTE, WheelTileItem.COLOR_BLUE));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_find_car, R.drawable.ic_smart_wheel_set_find_car, R.string.wheel_find_car_title, R.string.wheel_find_car_title, R.string.wheel_find_car_desc, WheelTileItem.ACTION_FIND_CAR, WheelTileItem.COLOR_BLUE));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_car_lock, R.drawable.ic_smart_wheel_set_car_lock, R.string.wheel_car_lock_title, R.string.wheel_car_lock_title, R.string.wheel_car_lock_desc, WheelTileItem.ACTION_CAR_LOCK, WheelTileItem.COLOR_BLUE));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_lock, R.drawable.ic_smart_wheel_set_car_unlock, R.string.wheel_car_unlock_title, R.string.wheel_car_unlock_title, R.string.wheel_car_unlock_desc, WheelTileItem.ACTION_CAR_UNLOCK, WheelTileItem.COLOR_BLUE));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_backdoor_open, R.drawable.ic_smart_wheel_set_backdoor_open, R.string.wheel_backdoor_open_title, R.string.wheel_backdoor_open_title, R.string.wheel_backdoor_open_desc, WheelTileItem.ACTION_BACKDOOR_OPEN, WheelTileItem.COLOR_BLUE));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_fresh_air, R.drawable.ic_smart_wheel_set_fresh_air, R.string.wheel_fresh_air_title, R.string.wheel_fresh_air_title, R.string.wheel_fresh_air_desc, WheelTileItem.ACTION_FRESH_AIR, WheelTileItem.COLOR_BLUE));
        // add(new WheelTileItem(R.drawable.ic_smart_wheel_fresh_air, R.drawable.ic_smart_wheel_set_fresh_air, R.string.wheel_engine_off_title, R.string.wheel_engine_off_title, R.string.wheel_engine_off_desc, WheelTileItem.ACTION_ENGINE_OFF, WheelTileItem.COLOR_BLUE));

        add(new WheelTileItem(R.drawable.ic_smart_wheel_click_service, R.drawable.ic_smart_wheel_set_click_service, R.string.wheel_click_service_title, R.string.wheel_click_service_title, R.string.wheel_click_service_desc, WheelTileItem.ACTION_CLICK_SERVICE, WheelTileItem.COLOR_YELLOW));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_sales_consultant, R.drawable.ic_smart_wheel_set_sales_consultant, R.string.wheel_sales_consultant_title, R.string.wheel_sales_consultant_title, R.string.wheel_sales_consultant_desc, WheelTileItem.ACTION_SALES_CONSULTANT, WheelTileItem.COLOR_YELLOW));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_110, R.drawable.ic_smart_wheel_set_110, R.string.wheel_110_title, R.string.wheel_110_title, R.string.wheel_110_desc, WheelTileItem.ACTION_110, WheelTileItem.COLOR_YELLOW));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_rescue, R.drawable.ic_smart_wheel_set_rescue, R.string.wheel_rescue_title, R.string.wheel_rescue_title, R.string.wheel_rescue_desc, WheelTileItem.ACTION_RESCUE, WheelTileItem.COLOR_YELLOW));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_secratery, R.drawable.ic_smart_wheel_set_secratery, R.string.wheel_secretary_title, R.string.wheel_secretary_title, R.string.wheel_secretary_desc, WheelTileItem.ACTION_SECRATERY, WheelTileItem.COLOR_YELLOW));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_reservation, R.drawable.ic_smart_wheel_set_reservation, R.string.wheel_reservation_title, R.string.wheel_reservation_title, R.string.wheel_reservation_desc, WheelTileItem.ACTION_RESERVATION, WheelTileItem.COLOR_YELLOW));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_driver_help, R.drawable.ic_smart_wheel_set_driver_help, R.string.wheel_driver_help_title, R.string.wheel_driver_help_title, R.string.wheel_driver_help_desc, WheelTileItem.ACTION_DRIVER_HELP, WheelTileItem.COLOR_YELLOW));

        add(new WheelTileItem(R.drawable.ic_smart_wheel_fix_reservation, R.drawable.ic_smart_wheel_set_make_reservation, R.string.wheel_fix_reservation_title, R.string.wheel_fix_reservation_title, R.string.wheel_fix_reservation_desc, WheelTileItem.ACTION_FIX_RESERVATION, WheelTileItem.COLOR_GREEN));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_your_reservation, R.drawable.ic_smart_wheel_set_your_reservation, R.string.wheel_your_reservation_title, R.string.wheel_your_reservation_title, R.string.wheel_your_reservation_desc, WheelTileItem.ACTION_YOUR_RESERVATION, WheelTileItem.COLOR_GREEN));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_reservation_progress, R.drawable.ic_smart_wheel_set_reservation_progress, R.string.wheel_reservation_progress_title, R.string.wheel_reservation_progress_title, R.string.wheel_reservation_progress_desc, WheelTileItem.ACTION_RESERVATION_PROGRESS, WheelTileItem.COLOR_GREEN));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_reservation_record, R.drawable.ic_smart_wheel_set_reservation_record, R.string.wheel_reservation_record_title, R.string.wheel_reservation_record_title, R.string.wheel_reservation_record_desc, WheelTileItem.ACTION_RESERVATION_RECORD, WheelTileItem.COLOR_GREEN));

        add(new WheelTileItem(R.drawable.ic_smart_wheel_car_list, R.drawable.ic_smart_wheel_set_car_list, R.string.wheel_car_list_title, R.string.wheel_car_list_title, R.string.wheel_car_list_desc, WheelTileItem.ACTION_CAR_LIST, WheelTileItem.COLOR_BLACK));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_digital_key, R.drawable.ic_smart_wheel_set_digital_key, R.string.wheel_digital_key_title, R.string.wheel_digital_key_title, R.string.wheel_digital_key_desc, WheelTileItem.ACTION_DIGITAL_KEY, WheelTileItem.COLOR_BLACK));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_car_status, R.drawable.ic_smart_wheel_set_car_status, R.string.wheel_car_status_title, R.string.wheel_car_status_title, R.string.wheel_car_status_desc, WheelTileItem.ACTION_CAR_STATUS, WheelTileItem.COLOR_BLACK));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_car_accident, R.drawable.ic_smart_wheel_set_car_accident, R.string.wheel_car_accident_title, R.string.wheel_car_accident_title, R.string.wheel_car_accident_desc, WheelTileItem.ACTION_CAR_ACCIDENT, WheelTileItem.COLOR_BLACK));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_car_theft, R.drawable.ic_smart_wheel_set_car_theft, R.string.wheel_car_theft_title, R.string.wheel_car_theft_title, R.string.wheel_car_theft_desc, WheelTileItem.ACTION_CAR_THEFT, WheelTileItem.COLOR_BLACK));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_etc_balance, R.drawable.ic_smart_wheel_set_etc_balance, R.string.wheel_etc_balance_title, R.string.wheel_etc_balance_title, R.string.wheel_etc_balance_desc, WheelTileItem.ACTION_ETC_BALANCE, WheelTileItem.COLOR_BLACK));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_dashboard_signal, R.drawable.ic_smart_wheel_set_dashboard_signal, R.string.wheel_dashboard_signal_title, R.string.wheel_dashboard_signal_title, R.string.wheel_dashboard_signal_desc, WheelTileItem.ACTION_DASHBOARD_SIGNAL, WheelTileItem.COLOR_BLACK));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_setting, R.drawable.ic_smart_wheel_set_setting, R.string.wheel_setting_title, R.string.wheel_setting_title, R.string.wheel_setting_desc, WheelTileItem.ACTION_SETTING, WheelTileItem.COLOR_BLACK));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_account_modify, R.drawable.ic_smart_wheel_set_account_modify, R.string.wheel_account_modify_title, R.string.wheel_account_modify_title, R.string.wheel_account_modify_desc, WheelTileItem.ACTION_ACCOUNT_MODIFY, WheelTileItem.COLOR_BLACK));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_pincode_modify, R.drawable.ic_smart_wheel_set_pincode_modify, R.string.wheel_pincode_modify_title, R.string.wheel_pincode_modify_title, R.string.wheel_pincode_modify_desc, WheelTileItem.ACTION_PINCODE_MODIFY, WheelTileItem.COLOR_BLACK));

        add(new WheelTileItem(R.drawable.ic_smart_wheel_sound_plus, R.drawable.ic_smart_wheel_sound_plus, R.string.wheel_sound_plus, R.string.wheel_sound_plus, R.string.wheel_sound_plus, WheelTileItem.ACTION_SOUND_PLUS, WheelTileItem.COLOR_UNKNOWN));
        add(new WheelTileItem(R.drawable.ic_smart_wheel_smart_eats, R.drawable.ic_smart_wheel_smart_eats, R.string.wheel_smart_eats, R.string.wheel_smart_eats, R.string.wheel_smart_eats, WheelTileItem.ACTION_SMART_EATS, WheelTileItem.COLOR_UNKNOWN));
    }};

    public static ArrayList<WheelTileItem> getAll() {
        return new ArrayList<>(mItems);
    }

    public static WheelTileItem get(int actionId) {
        for (WheelTileItem item : mItems) {
            if (item.getActionId() == actionId) {
                return new WheelTileItem(item.getIcon(), item.getSetupIcon(), item.getTitle(), item.getShortTitle(), item.getDescription(), item.getActionId(), item.getColor());
            }
        }

        return null;
    }

    public static Intent getActionIntent(Context context, int actionId) {
        Intent intent = null;
        switch (actionId) {
            case WheelTileItem.ACTION_AR_REMOTE:
                intent = RemoteControlActivity.newIntent(context, FRAGMENT_TAG_REMOTE_CONTROL_MENU);
                intent.putExtra(EXTRA_CHECK_KEY, true);
                break;

            case WheelTileItem.ACTION_FIND_CAR:
                intent = RemoteControlActivity.newIntent(context, REQUEST_FIND);
                intent.putExtra(EXTRA_CHECK_KEY, true);
                break;

            case WheelTileItem.ACTION_CAR_LOCK:
                intent = RemoteControlActivity.newIntent(context, REQUEST_LOCK);
                intent.putExtra(EXTRA_CHECK_KEY, true);
                break;

            case WheelTileItem.ACTION_CAR_UNLOCK:
                intent = RemoteControlActivity.newIntent(context, REQUEST_UNLOCK);
                intent.putExtra(EXTRA_CHECK_KEY, true);
                break;

            case WheelTileItem.ACTION_BACKDOOR_OPEN:
                intent = RemoteControlActivity.newIntent(context, REQUEST_OPEN);
                intent.putExtra(EXTRA_CHECK_KEY, true);
                break;

            case WheelTileItem.ACTION_FRESH_AIR:
                intent = RemoteControlActivity.newIntent(context, REQUEST_OPTIMIZE);
                intent.putExtra(EXTRA_CHECK_KEY, true);
                break;

            case WheelTileItem.ACTION_ENGINE_OFF:
                intent = RemoteControlActivity.newIntent(context, REQUEST_ENGINE_OFF);
                intent.putExtra(EXTRA_CHECK_KEY, true);
                break;

            case WheelTileItem.ACTION_SERVER:
                intent = DialUtils.callService(context);
                break;

            case WheelTileItem.ACTION_SALES_CONSULTANT:
                intent = DialUtils.callSales(context);
                break;

            case WheelTileItem.ACTION_110:
                intent = DialUtils.videoPolice(context);
                break;

            case WheelTileItem.ACTION_RESCUE:
                intent = DialUtils.callRoadRescue(context);
                break;

            case WheelTileItem.ACTION_SECRATERY:
                intent = DialUtils.callSecretary(context);
                break;

            case WheelTileItem.ACTION_RESERVATION:
                intent = DialUtils.callReservation(context);
                break;

            case WheelTileItem.ACTION_DRIVER_HELP:
                intent = DialUtils.callTaxi(context);
                break;

            case WheelTileItem.ACTION_CAR_FIX:
            case WheelTileItem.ACTION_FIX_RESERVATION:
                intent = MaintenanceQueryCarNoActivity.newIntent(context, 0);
                break;

            case WheelTileItem.ACTION_YOUR_RESERVATION:
                intent = MaintenanceQueryCarNoActivity.newIntent(context, 1);
                break;

            case WheelTileItem.ACTION_RESERVATION_PROGRESS:
                intent = MaintenanceQueryCarNoActivity.newIntent(context, 2);
                break;

            case WheelTileItem.ACTION_RESERVATION_RECORD:
                intent = MaintenanceQueryCarNoActivity.newIntent(context, 3);
                break;

            case WheelTileItem.ACTION_CAR_LIST:
                intent = new Intent(context, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra(INTENT_EXTRA_NAME_SHOW_CAR_KEY_LIST, true);
                break;

            case WheelTileItem.ACTION_DIGITAL_KEY:
                intent = DigitalKeyActivity.newIntent(context);
                break;

            case WheelTileItem.ACTION_CAR_STATUS:
                intent = CarStatusActivity.newIntent(context);
                break;

            case WheelTileItem.ACTION_CAR_ACCIDENT:
                intent = CarAccidentActivity.newIntent(context);
                break;

            case WheelTileItem.ACTION_CAR_THEFT:
                intent = CarStolenActivity.newIntent(context);
                break;

            case WheelTileItem.ACTION_ETC_BALANCE:
                intent = ETCBalanceActivity.newIntent(context);
                break;

            case WheelTileItem.ACTION_DASHBOARD_SIGNAL:
                intent = DashboardActivity.newIntent(context);
                break;

            case WheelTileItem.ACTION_SETTING:
                intent = SettingsActivity.newIntent(context, SettingsActivity.SETTINGS_MENU);
                break;

            case WheelTileItem.ACTION_ACCOUNT_MODIFY:
                intent = SettingsActivity.newIntent(context, SettingsActivity.SETTINGS_INFO);
                break;

            case WheelTileItem.ACTION_PINCODE_MODIFY:
                intent = SettingsActivity.newIntent(context, SettingsActivity.SETTINGS_PIN);
                break;

            case WheelTileItem.ACTION_SMART_EATS:
                intent = DialUtils.smartEats(context);
                break;

            case WheelTileItem.ACTION_SOUND_PLUS:
                intent = DialUtils.soundPlus(context);
                break;
        }

        if (intent != null) {
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        }

        return intent;
    }
}
