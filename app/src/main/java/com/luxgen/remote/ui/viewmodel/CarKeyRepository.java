package com.luxgen.remote.ui.viewmodel;

import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.luxgen.remote.model.db.CarKeyDao;
import com.luxgen.remote.network.CarWebApi;
import com.luxgen.remote.network.model.car.CarKeyData;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class CarKeyRepository {
    private static CarKeyRepository mInstance;
    private CarWebApi mApi;
    private CarKeyDao mDao;

    public CarKeyRepository(CarKeyDao dao, CarWebApi api) {
        mDao = dao;
        mApi = api;
    }

    public LiveData<List<CarKeyData>> loadListLiveDataFromDb() {
        try {
            return new LoadListLiveDataAsyncTask().execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private class LoadListLiveDataAsyncTask extends AsyncTask<Void, Void, LiveData<List<CarKeyData>>> {

        @Override
        protected LiveData<List<CarKeyData>> doInBackground(Void... voids) {
            return mDao.getListLiveData();
        }
    }

    public List<CarKeyData> loadListFromDb() {
        try {
            return new LoadListAsyncTask().execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private class LoadListAsyncTask extends AsyncTask<Void, Void, List<CarKeyData>> {

        @Override
        protected List<CarKeyData> doInBackground(Void... voids) {
            return mDao.getList();
        }
    }

    public void syncListToDb(ArrayList<CarKeyData> list) {
        new SyncListAsyncTask(mDao, list).execute();
    }

    static class SyncListAsyncTask extends AsyncTask<Void, Void, Void> {

        private CarKeyDao mDao;
        private ArrayList<CarKeyData> mList;

        SyncListAsyncTask(CarKeyDao dao, ArrayList<CarKeyData> list) {
            mDao = dao;
            mList = list;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (mList != null) {
                mDao.clear();
                mDao.insert(mList);
            }
            return null;
        }

    }
}
