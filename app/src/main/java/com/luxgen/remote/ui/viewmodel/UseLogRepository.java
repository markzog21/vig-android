package com.luxgen.remote.ui.viewmodel;

import android.os.AsyncTask;

import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.db.UseLogDao;
import com.luxgen.remote.model.db.VigDatabase;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.assistant.SetUseLogRequestData;
import com.luxgen.remote.network.model.assistant.UseLogData;

public class UseLogRepository {
    private static UseLogRepository mRepo;

    private VigDatabase mDb;
    private UseLogData mUseLogData;
    private AssistantWebApi mApi;
    private UserInfo mUserInfo;

    public static UseLogRepository getInstance(UserInfo info, VigDatabase db, AssistantWebApi api) {
        if (mRepo == null) {
            mRepo = new UseLogRepository(info, db, api);
        }
        return mRepo;
    }

    public UseLogRepository(UserInfo info, VigDatabase db, AssistantWebApi api) {
        mDb = db;
        mApi = api;
        mUserInfo = info;
    }

    void syncToNetwork() {
        new GetLogData(mUserInfo, mDb.useLogModel(), mApi).execute();
    }

    private void syncToNetwork(UseLogData data) {
        new SyncToNetwork(mUserInfo, mDb.useLogModel()
                , mApi, getSetUseLogCallback(data)).execute();
    }

    public void setUseLog(UserInfo info, UseLogData data) {
        mUserInfo = info;
        new InsertTask(mDb.useLogModel(), data).execute();
    }

    private void clearUseLog(UseLogData data) {
        new ClearTask(mDb.useLogModel(), data).execute();
    }

    private OnApiListener<ResponseData> getSetUseLogCallback(UseLogData data) {
        return new OnApiListener<ResponseData>() {
            @Override
            public void onApiTaskSuccessful(ResponseData responseData) {
                clearUseLog(data);
            }

            @Override
            public void onApiTaskFailure(String failMessage) {

            }

            @Override
            public void onPreApiTask() {

            }

            @Override
            public void onApiProgress(long value) {

            }

            @Override
            public void onAuthFailure() {

            }
        };
    }

    private static class GetLogData extends AsyncTask<Void, Void, UseLogData> {

        private UserInfo mInfo;
        private UseLogDao mDao;
        private AssistantWebApi mApi;

        GetLogData(UserInfo info, UseLogDao dao, AssistantWebApi api) {
            mInfo = info;
            mDao = dao;
            mApi = api;
        }

        @Override
        protected UseLogData doInBackground(Void... voids) {
            return mDao.get();
        }

        @Override
        protected void onPostExecute(UseLogData data) {
            super.onPostExecute(data);
            if (data != null && mRepo != null) {
                mRepo.syncToNetwork(data);
            }
        }
    }

    private static class SyncToNetwork extends AsyncTask<Void, Void, Void> {
        private UserInfo mInfo;
        private UseLogDao mDao;
        private AssistantWebApi mApi;
        private OnApiListener<ResponseData> mCallback;

        SyncToNetwork(UserInfo info, UseLogDao dao, AssistantWebApi api, OnApiListener<ResponseData> callback) {
            mInfo = info;
            mDao = dao;
            mApi = api;
            mCallback = callback;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            UseLogData data = mDao.get();
            if (data != null) {
                SetUseLogRequestData request = new SetUseLogRequestData(mInfo.getToken(), mInfo.getKeyUid());
                request.setLogData(data);
                mApi.setUseLog(request, mCallback);
            }

            return null;
        }
    }

    private class InsertTask extends AsyncTask<UseLogData, Void, Boolean> {

        private UseLogDao mDao;
        private UseLogData mData;

        InsertTask(UseLogDao dao, UseLogData data) {
            mDao = dao;
            mData = data;
        }

        @Override
        protected Boolean doInBackground(UseLogData... pushData) {
            if (mData != null) {
                mDao.insert(mData);
                return true;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result) {
                syncToNetwork();
            }
        }
    }

    private static class ClearTask extends AsyncTask<Void, Void, Void> {

        private UseLogDao mDao;
        private UseLogData mData;

        ClearTask(UseLogDao dao, UseLogData data) {
            mDao = dao;
            mData = data;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (mData != null) {
                mDao.clear(mData);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mRepo != null) {
                mRepo.syncToNetwork();
            }
        }
    }
}
