package com.luxgen.remote.ui.dashboard;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.luxgen.remote.R;

public class DashboardAdapter extends BaseAdapter {

    int[] symbols = {
            R.drawable.ic_dashboard_turn_left, R.drawable.ic_dashboard_turn_right, R.drawable.ic_dashboard_position_lamp,
            R.drawable.ic_dashboard_low_beam, R.drawable.ic_dashboard_front_fog, R.drawable.ic_dashboard_rear_fog,
            R.drawable.ic_dashboard_high_beam, R.drawable.ic_dashboard_hi_low_beam_auto, R.drawable.ic_dashboard_low_beam_led_error,
            R.drawable.ic_dashboard_over_speed_warning, R.drawable.ic_dashboard_low_fuel_warning, R.drawable.ic_dashboard_engine_check,
            R.drawable.ic_dashboard_abs_fail, R.drawable.ic_dashboard_tpms_warning, R.drawable.ic_dashboard_tcs_off,
            R.drawable.ic_dashboard_tcs_esc_hbb, R.drawable.ic_dashboard_epb_fail, R.drawable.ic_dashboard_coolant_hot,
            R.drawable.ic_dashboard_ebd_fail, R.drawable.ic_dashboard_oil_pressure_warning, R.drawable.ic_dashboard_air_bag_warning,
            R.drawable.ic_dashboard_seat_belt_unfastened, R.drawable.ic_dashboard_battery_charge_warning, R.drawable.ic_dashboard_eps_warning,
            R.drawable.ic_dashboard_accident_warning, R.drawable.ic_dashboard_pedestrian_warning, R.drawable.ic_dashboard_ldws_plus,
            R.drawable.ic_dashboard_cruise, R.drawable.ic_dashboard_cruise_set, R.drawable.ic_dashboard_direction_light,
            R.drawable.ic_dashboard_daylight, R.drawable.ic_dashboard_parking,
    };

    private Context mContext;
    private int mItemSize;
    private int mSelectedItem = -1;

    public DashboardAdapter(Context context) {
        this.mContext = context;
        this.mItemSize = dp2px(context, 100);
    }

    @Override
    public int getCount() {
        return symbols.length;
    }

    @Override
    public Object getItem(int position) {
        return symbols[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AppCompatImageView image;
        if (convertView == null) {
            image = new AppCompatImageView(mContext);
            image.setLayoutParams(new GridView.LayoutParams(mItemSize, mItemSize));
            image.setScaleType(ImageView.ScaleType.CENTER);
        } else {
            image = (AppCompatImageView) convertView;
        }
        if (mSelectedItem == position) {
            image.setBackgroundResource(R.drawable.btn_bg_round_grey);
        } else {
            image.setBackground(null);
        }
        image.setImageResource(symbols[position]);

        return image;
    }

    public void setSelectedItem(int item) {
        mSelectedItem = item;
        notifyDataSetChanged();
    }

    private int dp2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}
