package com.luxgen.remote.ui.wheel;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.util.Prefs;

public class WheelMenuSetupControlFragment extends CustomBaseFragment {

    private SharedPreferences mSharedPreferences;
    private String mKeyUid;

    public static WheelMenuSetupControlFragment newInstance() {
        WheelMenuSetupControlFragment fragment = new WheelMenuSetupControlFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPreferences = getActivity().getSharedPreferences("wheelmenu", Context.MODE_PRIVATE);
        mKeyUid = Prefs.getKeyUid(getActivity());
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_wheel_menu_setup_control, container, false);

        AppCompatRadioButton mRightSideRadioButton = v.findViewById(R.id.right_side);
        mRightSideRadioButton.setChecked(isOnRight());
        mRightSideRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mSharedPreferences.edit().putBoolean(mKeyUid + "_onRight", isChecked).apply();
            }
        });
        AppCompatRadioButton mLeftSideRadioButton = v.findViewById(R.id.left_side);
        mLeftSideRadioButton.setChecked(!isOnRight());

        SwitchCompat mShakeGestureSwitch = v.findViewById(R.id.shake_gesture);
        mShakeGestureSwitch.setChecked(isShakeGestureEnabled());
        mShakeGestureSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mSharedPreferences.edit().putBoolean(mKeyUid + "_shakeGesture", isChecked).apply();
            }
        });

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.clear();
    }

    private boolean isOnRight() {
        return mSharedPreferences.getBoolean(mKeyUid + "_onRight", true);
    }

    private boolean isShakeGestureEnabled() {
        return mSharedPreferences.getBoolean(mKeyUid + "_shakeGesture", false);
    }
}
