package com.luxgen.remote.ui.login;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.ui.dialog.ProgressDialogFragment;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.UserWebApi;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.user.CheckDmsPhoneRequestData;
import com.luxgen.remote.network.model.user.CheckDmsPhoneResponseData;
import com.luxgen.remote.network.model.user.CheckPhoneNoRequestData;

import static com.luxgen.remote.ui.login.LoginActivity.FRAGMENT_TAG_LOGIN_REGISTER_VERIFY_PHONE;

public class LoginRegisterAddPhoneFragment extends CustomBaseFragment {

    private EditText mPhoneEditText;
    private boolean mIsGeneralUser = true;
    private String mEmail;
    private String mAccountId;
    private String mCarNo;
    private ProgressDialogFragment mProgressDialog;

    public static LoginRegisterAddPhoneFragment newInstance(Bundle args) {
        LoginRegisterAddPhoneFragment fragment = new LoginRegisterAddPhoneFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private OnApiListener<ResponseData> mGeneralUserOnApiListener = new OnApiListener<ResponseData>() {
        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            mProgressDialog.dismiss();
            if (responseData.isSuccessful()) {
                showVerifyPhoneScreen();
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            mProgressDialog.dismiss();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mProgressDialog = ProgressDialogFragment.newInstance(getString(R.string.login_register_add_phone_sending_sms));
            mProgressDialog.show(getFragmentManager(), "sendingSMSProgressDialog");
        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            // TODO: check if need this
            mProgressDialog.dismiss();
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<CheckDmsPhoneResponseData> mDMSUserOnApiListener = new OnApiListener<CheckDmsPhoneResponseData>() {
        @Override
        public void onApiTaskSuccessful(CheckDmsPhoneResponseData responseData) {
            mProgressDialog.dismiss();
            if (responseData.isSuccessful()) {
                showVerifyPhoneScreen();
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            mProgressDialog.dismiss();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mProgressDialog = ProgressDialogFragment.newInstance(getString(R.string.login_register_add_phone_sending_sms));
            mProgressDialog.show(getFragmentManager(), "sendingSMSProgressDialog");
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            // TODO: check if need this
            mProgressDialog.dismiss();
            showMultiLoginErrorDialog();
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        mIsGeneralUser = args.getBoolean("isGeneralUser", mIsGeneralUser);
        mEmail = args.getString("email", mEmail);
        mAccountId = args.getString("accountId", mAccountId);
        mCarNo = args.getString("carNo", mCarNo);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login_register_add_phone, container, false);

        TextView mTitleTextView = v.findViewById(R.id.txt_add_phone_title);
        if (!mIsGeneralUser) {
            mTitleTextView.setText(R.string.login_register_add_phone_vig_title);
        }

        mPhoneEditText = v.findViewById(R.id.edit_phone_number);

        Button nextButton = v.findViewById(R.id.btn_next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verify();
            }
        });

        return v;
    }

    private boolean validate() {
        String phone = mPhoneEditText.getText().toString().trim();
        if (TextUtils.isEmpty(phone)) {
            mPhoneEditText.requestFocus();
            mPhoneEditText.setError(getString(R.string.login_register_add_phone_error_empty));
            return false;
        }

        return true;
    }

    private void verify() {
        if (validate()) {
            sendVerificationSMS();
        }
    }

    private CheckPhoneNoRequestData buildCheckPhoneData(String phone, String email) {
        CheckPhoneNoRequestData data = new CheckPhoneNoRequestData(email);
        data.setPhone(phone);
        return data;
    }

    private CheckDmsPhoneRequestData buildCheckDmsPhoneData(String phone, String email, String accountId, String carNo) {
        CheckDmsPhoneRequestData data = new CheckDmsPhoneRequestData(email);
        data.setPhone(phone);
        data.setAccountId(accountId);
        data.setCarNo(carNo);

        return data;
    }

    private void sendVerificationSMS() {
        String phone = mPhoneEditText.getText().toString().trim();

        UserWebApi userWebApi = UserWebApi.getInstance(getActivity());
        if (mIsGeneralUser) {
            CheckPhoneNoRequestData data = buildCheckPhoneData(phone, mEmail);
            userWebApi.checkPhone(data, mGeneralUserOnApiListener);
        } else {
            CheckDmsPhoneRequestData data = buildCheckDmsPhoneData(phone, mEmail, mAccountId, mCarNo);
            userWebApi.checkDmsPhone(data, mDMSUserOnApiListener);
        }
    }

    private void showVerifyPhoneScreen() {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        String phone = mPhoneEditText.getText().toString().trim();
        args.putString("phone", phone);
        ((LoginActivity) getActivity()).navigate(this, LoginRegisterVerifySMSFragment.newInstance(args),
                FRAGMENT_TAG_LOGIN_REGISTER_VERIFY_PHONE, true);
    }
}
