package com.luxgen.remote.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ScrollView;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.util.Prefs;

public class CustomAlertDialog extends Dialog {

    private TextView mTitleTextView = null;
    private ScrollView mMessageScrollView = null;
    private TextView mMessageTextView = null;
    private ScrollView mCustomLayoutScrollView = null;
    private ViewGroup mCustomLayout = null;
    private TextView mPositiveButtonTextView = null;
    private TextView mNegativeButtonTextView = null;
    private CheckBox mCheckBox = null;
    private String mPreferenceKey = null;

    public CustomAlertDialog(Context context) {
        super(context);
        init(context);
    }

    public CustomAlertDialog(Context context, int theme) {
        super(context, theme);
        init(context);
    }

    public CustomAlertDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init(context);
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init(Context context) {
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_alert, null, false);

        mTitleTextView = contentView.findViewById(R.id.dialog_alert_title_text_view);
        mMessageScrollView = contentView.findViewById(R.id.dialog_alert_message_scroll_view);
        mMessageTextView = contentView.findViewById(R.id.dialog_alert_message_text_view);
        mCustomLayoutScrollView = contentView.findViewById(R.id.dialog_alert_custom_layout_scroll_view);
        mCustomLayout = contentView.findViewById(R.id.dialog_alert_custom_layout);
        mCheckBox = contentView.findViewById(R.id.dialog_alert_check_box);
        mPositiveButtonTextView = contentView.findViewById(R.id.dialog_alert_positive_text_view);
        mNegativeButtonTextView = contentView.findViewById(R.id.dialog_alert_negative_text_view);

        setContentView(contentView);
    }

    @Override
    public void setTitle(CharSequence title) {
        if (title != null && title.length() != 0) {
            mTitleTextView.setVisibility(View.VISIBLE);
            mTitleTextView.setText(title);
        }
    }

    public void setMessage(CharSequence message) {
        if (message != null && message.length() != 0) {
            mMessageTextView.setText(message);
            mMessageTextView.setVisibility(View.VISIBLE);
            mMessageScrollView.setVisibility(View.VISIBLE);
        }
    }

    public void setCheckBoxData(CharSequence text, String preferenceKey) {
        if (text != null && text.length() != 0 && preferenceKey != null && preferenceKey.length() != 0) {
            mCheckBox.setVisibility(View.VISIBLE);
            mCheckBox.setText(text);
            mPreferenceKey = preferenceKey;
        }
    }

    public void setCustomContentView(View view) {
        if (view != null) {
            mCustomLayout.addView(view);
            mCustomLayoutScrollView.setVisibility(View.VISIBLE);
        }
    }

    public void restoreDefault() {
        mTitleTextView.setVisibility(View.GONE);
        mMessageTextView.setVisibility(View.GONE);
        mCustomLayout.removeAllViews();
        mCustomLayoutScrollView.setVisibility(View.GONE);
        mCheckBox.setVisibility(View.GONE);
        mNegativeButtonTextView.setVisibility(View.GONE);
        mPositiveButtonTextView.setVisibility(View.GONE);
    }

    public void setButton(int whichButton, CharSequence text, View.OnClickListener listener) {

        switch (whichButton) {
            case DialogInterface.BUTTON_POSITIVE: {
                mPositiveButtonTextView.setText(text);
                mPositiveButtonTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mPreferenceKey != null) {
                            boolean value = mCheckBox.isChecked();
                            Prefs.setPreferenceKeyValue(getContext(), mPreferenceKey, value);
                        }

                        if (listener != null) {
                            listener.onClick(v);
                        }
                    }
                });
                mPositiveButtonTextView.setVisibility(View.VISIBLE);
                break;
            }

            case DialogInterface.BUTTON_NEGATIVE: {
                mNegativeButtonTextView.setText(text);
                mNegativeButtonTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.onClick(v);
                        }
                    }
                });
                mNegativeButtonTextView.setVisibility(View.VISIBLE);
                break;
            }

            default:
                break;

        }
    }
}
