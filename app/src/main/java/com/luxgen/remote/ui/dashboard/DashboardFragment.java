package com.luxgen.remote.ui.dashboard;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;

public class DashboardFragment extends CustomBaseFragment {

    private GridView mGridView;
    private DashboardAdapter mDashboardAdapter;
    private LinearLayout mDescriptionContainer;
    private TextView mTitleTextView;
    private TextView mDescTextView;
    private int mCurrentSymbolId;

    private String[] mTitle = new String[]{
            "方向燈／危險警告燈",
            "方向燈／危險警告燈",
            "小燈指示燈",
            "近光燈指示燈",
            "前霧燈指示燈",
            "後霧燈指示燈",
            "遠光指示燈",
            "自動遠/近光頭燈",
            "LED 頭燈故障警告燈",
            "超速警告燈",

            "燃油不足或油量訊號有誤警告燈",
            "引擎故障警告燈",
            "防鎖定煞車系統( ABS ) 警告燈",
            "TPMS 警告燈",
            "TCS OFF 指示燈",
            "打滑指示燈",
            "電子駐車煞車( EPB ) 故障警告燈",
            "水溫異常警告燈",
            "煞車警告燈/煞車油液面過低警告燈",
            "引擎機油壓力警告燈",

            "輔助氣囊警告燈",
            "安全帶警告燈",
            "充電警告燈",
            "EPS 故障警告燈-紅",
            "前車碰撞警示系統(FCW+ )",
            "行人偵測警示系統( PDS+ )",
            "行車偏移偵測警示系統( LDWS+ )",
            "定速控制主開關指示燈",
            "定速設定指示燈",
            "方向燈／危險警告燈",

            "晝行燈指示燈",
            "自排變速箱檔位指示",
    };

    private String[] mDesc = new String[]{
            "當方向燈開關作動時，與方向燈開關相對應方向的指示燈即會閃爍。\n危險警告燈開關開啟時，兩個指示燈均會閃爍。",
            "當方向燈開關作動時，與方向燈開關相對應方向的指示燈即會閃爍。\n危險警告燈開關開啟時，兩個指示燈均會閃爍。",
            "當開啟小燈時，此指示燈即會亮起。",
            "當開啟近光燈時此指示燈即會亮起。",
            "當開啟前霧燈時，此指示燈即會亮起。",
            "當開啟後霧燈時，此指示燈即會亮起。",
            "當遠光燈開啟時此指示燈即亮起，切換回近光燈時此指示燈即熄滅。當超車燈作動時，遠光指示燈也會亮起。",
            "當開啟自動調整遠/近光燈時此指示燈即會亮起。",
            "當啟動時，此燈號短暫亮起後自動熄滅，則表示系統運作正常。若此燈號於行駛時點亮，請儘速聯絡LUXGEN 服務廠進行檢修。",
            "當導航的警示燈設定為開啟，於導航使用中，若是車速超過設定的條件時，綜合儀表內的超速警告燈會亮起，以提醒駕駛人注意。",

            "當油箱中的燃油液面高度過低或所接收到的油量訊號不正常時，此警告燈會亮起。當您添加燃油之後如果此警告燈持續亮起，請儘速連絡 LUXGEN 服務廠進行檢修。",
            "若車輛發動後持續亮起或閃爍，表示引擎或廢氣排放控制系統有故障，請依下列要點小心駕駛您的車輛並開至最近的 LUXGEN 服務廠進行檢修：\n" +
                    "˙ 請避免以高速駕駛車輛。\n" +
                    "˙ 請避免急加速及急煞車。\n" +
                    "˙ 請避免行駛陡峭的上坡路段。\n" +
                    "˙ 請儘量減少載運的貨物。\n" +
                    "若在引擎或廢氣排放控制系統的故障未修護前繼續行駛車輛，將導致車輛性能降低並增加耗油，甚至使廢氣排放控制系統受到損壞。當故障警告燈閃爍或亮起時，請儘速連絡 LUXGEN服務廠進行檢修。",
            "當啟動時，此燈號短暫亮起後自動熄滅，則表示系統運作正常。若引擎運轉時此警告燈亮起，則表示ABS 作動不正常，請儘速連絡LUXGEN 服務廠進行檢修。\n" +
                    "若系統發生故障，ABS 將不會作動，但是傳統煞車系統仍可繼續正常作動。若行車時此燈亮起， 請儘速連絡LUXGEN 服務廠進行檢修。",
            "將點火開關電源切換至ON，TPMS 警告燈會短暫亮起後熄滅，此時表示警告燈自檢功能正常。\n" +
                    "胎壓監測系統 ( TPMS ) 會監視所有輪胎的胎壓。當偵測到胎壓偵測器失效、胎壓過高或過低時，此警告燈會點亮。\n" +
                    "當胎壓偵測器失效時，TPMS 警告燈會閃爍約 60 ~ 75 秒鐘，然後保持恆亮。若發生上述情形，請先將車輛駛離目前所在地，並稍等數分鐘後檢查警告燈是否熄滅，若警告燈依然保持恆亮，請儘速連絡 LUXGEN 服務廠進行檢修。\n" +
                    "如果車輛在胎壓過高或過低的情況下行駛，TPMS 警告燈會保持恆亮。\n" +
                    "當 TPMS 警告燈點亮時，您應將車輛停下，使用胎壓錶來檢查胎壓並將胎壓調整到胎壓標籤上的建議冷胎壓。調整胎壓後，TPMS 警告燈可能不會馬上自動熄滅。再將輪胎調整到建議的壓力後，請依照下列方式(車輛靜止時，將點火開關電源切換至ON位置，然後等待約15 分鐘，TPMS 警告燈會自動熄滅)來進行系統資料更新，並使TPMS警告燈熄滅。",
            "按下循跡防滑控制系統開關將循跡防滑控制系統( TCS ) 關閉時，此指示燈會亮起( 表示TCS 功能已取消)，再次按下循跡防滑控制系統開關，或再次起動引擎即可恢復正常作動。\n" +
                    "當點火開關切換至ON 時，此指示燈也會亮起。若系統作動正常，則指示燈會亮起約2 秒後自動熄滅。",
            "當動態車輛穩定系統 ( ESC ) 限制車輪的轉動時，此指示燈會閃爍。若此指示燈閃爍時，表示可能行駛於濕滑的路面，若發生此狀況，請依情況調整您的駕駛方式。\n" +
                    "當點火開關切換至ON 時，此指示燈也會亮起。若系統作動正常，則指示燈會亮起約2 秒後自動熄滅。\n" +
                    "若指示燈未點亮或持續亮起無法熄滅時， 請儘速連絡LUXGEN 服務廠進行檢修。",
            "將點火開關切換至ON，若此警告燈短暫亮起後熄滅，表示電子駐車煞車系統( EPB ) 作動正常。\n" +
                    "若此警告燈閃爍，則表示 EPB 系統作用不正常，請儘速連絡LUXGEN服務廠進行檢修。",
            "當點火開關切換至 ON 時，水溫異常警告燈會亮起後熄滅。\n" +
                    "水溫異常警告燈會於引擎冷卻液溫度過高時閃爍紅色水溫異常警告燈。若亮起紅色警告燈，請儘速將車輛移至安全的路旁並進行檢查。車輛在過熱狀況下運轉可能造成引擎嚴重損壞。\n" +
                    "當引擎冷卻液於正常運作溫度時，水溫異常警告燈將會熄滅。\n" +
                    "當引擎冷卻液溫度高於 115°C時，將會閃爍紅色水溫異常警告燈。\n" +
                    "若車輛行駛中閃爍紅色水溫異常警告燈，請將車輛移置安全無虞處，並儘速聯繫 LUXGEN 服務廠。",
            "此警告燈同時適用於駐車煞車系統與腳煞車系統。當車輛啟動且駐車煞車作動時，此警告燈即亮起。\n" +
                    "此警告燈亦有警告煞車油液面過低之功用。若引擎運轉中且未啟動駐車煞車，而燈號卻亮起時，請停下車輛並檢查煞車油液面，若有必要， 請添加LUXGEN 推薦的煞車油至標準範圍內。若煞車油液面高度正確，請儘速連絡LUXGEN服務廠進行檢修。",
            "此燈號為警告引擎機油壓力過低。為了避免引擎嚴重損壞，若此燈號於行車時閃爍或亮起，請將車輛停靠安全處所後將引擎熄火，並儘速連絡 LUXGEN 服務廠進行檢修。此燈號並無指示機油液面過低的功能。機油液面高度請使用機油尺來檢查。",

            "當啟動時，此燈號短暫亮起後自動熄滅，則表示系統運作正常。若下列任一狀況發生，SRS 輔助氣囊系統及智慧預縮式安全帶系統都必須檢修，請儘速連絡LUXGEN 服務廠進行檢修：\n" +
                    "- 輔助氣囊警告燈未先亮起數秒再熄滅。\n" +
                    "- 輔助氣囊警告燈在亮起數秒後，仍持續點亮。\n" +
                    "- 輔助氣囊警告燈間歇閃爍。",
            "點火發車後，此警告燈會閃爍數秒後持續亮著，直到駕駛側安全帶繫上才熄滅，以提醒駕駛人尚未繫上安全帶。",
            "當引擎運轉時此警告燈亮起，則表示充電系統的作動可能不正常，請將引擎熄火並檢查發電機皮帶。若皮帶鬆動、斷裂、脫落，或此警告燈持續亮起，請儘速連絡 LUXGEN 服務廠進行檢修。",
            "當啟動時，此燈號短暫亮起後自動熄滅，則表示系統運作正常。若此燈號於行駛時閃爍或亮起，請儘速聯絡 LUXGEN 服務廠進行檢修。\n" +
                    "若長時間未行駛導致電瓶電壓不足或於更換電瓶後，EPS 及 TCS 警告燈可能會閃爍。此時 EPS 仍可正常提供輔助力，只需要以直線行駛一小段距離讓 EPS 學習，學習完成後警告燈即會熄滅。",
            "若車輛行駛中系統偵測到前方車輛接近時，系統則會響起警示音並同時於儀表板中閃爍DAS+ 警告燈，以提醒駕駛人注意。",
            "若車輛行駛中系統偵測到前方行人接近時，系統則會響起警示音並同時於儀表板中閃爍DAS+ 警告燈，以提醒駕駛人注意。",
            "若車輛行駛中系統偵測到行車偏移時，系統則會響起警示音並同時於儀表板中閃爍DAS+ 警告燈，以提醒駕駛人注意。",
            "當車速由定速控制系統控制時，此指示燈會亮起。",
            "當車速由定速控制系統控制時，此指示燈會亮起。",
            "當方向燈開關作動時，與方向燈開關相對應方向的指示燈即會閃爍。\n" +
                    "危險警告燈開關開啟時，兩個指示燈均會閃爍。",

            "引擎起動後，當駐車煞車釋放或將排檔桿切換至 P 檔以外位置時，此指示燈即會亮起。若燈光開關切換至大燈或同時開啟小燈及霧燈時，此指示燈即會熄滅。",
            "當點火開關轉至 ON 位置時，速率錶下方的檔位指示器會顯示目前自動變速箱的檔位。",
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dashboard, container, false);

        mGridView = v.findViewById(R.id.dashboard_grid);
        mDescriptionContainer = v.findViewById(R.id.dashboard_description);
        mTitleTextView = v.findViewById(R.id.symbol_title);
        mDescTextView = v.findViewById(R.id.symbol_desc);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDashboardAdapter = new DashboardAdapter(getContext());
        mGridView.setAdapter(mDashboardAdapter);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDashboardAdapter.setSelectedItem(position);
                if (mDescriptionContainer.getVisibility() == View.VISIBLE) {
                    if (mCurrentSymbolId == position) {
                        mDescriptionContainer.setVisibility(View.GONE);
                        mDashboardAdapter.setSelectedItem(-1);
                    }
                } else {
                    mDescriptionContainer.setVisibility(View.VISIBLE);
                }
                mCurrentSymbolId = position;
                mTitleTextView.setText(mTitle[position]);
                mDescTextView.setText(mDesc[position]);
            }
        });
    }
}
