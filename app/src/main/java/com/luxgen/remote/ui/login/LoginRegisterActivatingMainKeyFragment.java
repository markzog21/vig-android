package com.luxgen.remote.ui.login;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luxgen.remote.R;
import com.luxgen.remote.activity.MainActivity;
import com.luxgen.remote.ca.CommunicationAgentService;
import com.luxgen.remote.ui.dialog.AlertDialogFragment;
import com.luxgen.remote.custom.CustomBaseFragmentWithCaService;

public class LoginRegisterActivatingMainKeyFragment extends CustomBaseFragmentWithCaService {

    public static LoginRegisterActivatingMainKeyFragment newInstance(Bundle args) {
        LoginRegisterActivatingMainKeyFragment fragment = new LoginRegisterActivatingMainKeyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login_register_activating_main_key, container, false);

        return v;
    }

    @Override
    protected void onCaServiceConnected() {
        mCommunicationAgentService.addMasterKey();
    }

    @Override
    protected void addActionToReceiveBroadcastFromCaService(IntentFilter intentFilter) {
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_ADD_MASTERKEY);
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_VIG_WAKEUP_FAILED);
    }

    @Override
    protected void onReceiveBroadcastFromCaService(Context context, Intent intent) {
        if (CommunicationAgentService.CA_STATUS_ADD_MASTERKEY.equals(intent.getAction())) {
            if (intent.hasExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE)) {
                boolean success = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, CommunicationAgentService.CA_EXTRA_ADD_MASTERKEY_SUCCESS) == CommunicationAgentService.CA_EXTRA_ADD_MASTERKEY_SUCCESS;
                if (success) {
                    showMainScreen();
                } else {
                    showActivationFailedDialog();
                }
            }
            return;
        } else if (CommunicationAgentService.CA_STATUS_VIG_WAKEUP_FAILED.equals(intent.getAction())) {
            showActivationFailedDialog();
            return;
        }

        super.onReceiveBroadcastFromCaService(context, intent);
    }

    private void showMainScreen() {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.putExtras(args);
        startActivity(intent);
        // unregisterCaStatusBroadcastReceiver();
        getActivity().finish();
    }

    private void showActivationFailedDialog() {
        showConfirmDialog(getString(R.string.login_register_activating_key_failed_title), getString(R.string.login_register_activating_key_failed_message),
                new AlertDialogFragment.Callbacks() {
                    @Override
                    public void onPositiveButtonClicked() {
                        if (getFragmentManager() != null) {
                            getFragmentManager().popBackStack();
                        }
                    }

                    @Override
                    public void onNegativeButtonClicked() {
                    }

                    @Override
                    public void onBackKeyPressed() {
                    }
                });
    }
}
