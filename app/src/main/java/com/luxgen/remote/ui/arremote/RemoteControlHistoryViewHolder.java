package com.luxgen.remote.ui.arremote;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.model.option.VigOptions;
import com.luxgen.remote.network.model.car.CarControlLogData;
import com.luxgen.remote.util.TimeUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class RemoteControlHistoryViewHolder extends ViewHolder {

    private TextView mActionTextView;
    private TextView mDateTextView;
    private ImageView mIconImageView;

    private SimpleDateFormat mDateFormatter = new SimpleDateFormat("MM月 dd日 EEE a hh:mm", Locale.TAIWAN);
    private Calendar mChosenDate = Calendar.getInstance();

    public RemoteControlHistoryViewHolder(View itemView) {
        super(itemView);

        mIconImageView = itemView.findViewById(R.id.icon);
        mActionTextView = itemView.findViewById(R.id.action);
        mDateTextView = itemView.findViewById(R.id.date);
    }

    public void setItem(CarControlLogData item) {
        String controlCommand = item.getControlCommand();
        if (VigOptions.REMOTE_CONTROL_COMMAND_CLEAN.equals(controlCommand)) {
            mIconImageView.setImageResource(R.drawable.ic_optimize_dark);
            mActionTextView.setText(R.string.remote_control_optimize_optimize);
        } else if (VigOptions.REMOTE_CONTROL_COMMAND_FIND_CAR.equals(controlCommand)) {
            mIconImageView.setImageResource(R.drawable.ic_find_car_dark);
            mActionTextView.setText(R.string.remote_control_optimize_find_car);
        } else if (VigOptions.REMOTE_CONTROL_COMMAND_DOOR.equals(controlCommand)) {
            mIconImageView.setImageResource(R.drawable.ic_back_door_dark);
            mActionTextView.setText(R.string.remote_control_optimize_back_door);
        } else if (VigOptions.REMOTE_CONTROL_COMMAND_UNLOCK.equals(controlCommand)) {
            mIconImageView.setImageResource(R.drawable.ic_open_dark);
            mActionTextView.setText(R.string.remote_control_optimize_unlock_car);
        } else if (VigOptions.REMOTE_CONTROL_COMMAND_LOCK.equals(controlCommand)) {
            mIconImageView.setImageResource(R.drawable.ic_lock_dark);
            mActionTextView.setText(R.string.remote_control_optimize_lock_car);
        } else if (VigOptions.REMOTE_CONTROL_COMMAND_ENGINE_OFF.equals(controlCommand)) {
            mIconImageView.setImageResource(R.drawable.off_log);
            mActionTextView.setText(R.string.remote_control_optimize_engine_off);
        }
        mDateTextView.setText(getFormattedDate(item.getControlTime()));
    }

    private String getFormattedDate(String dateStr) {
        try {
            long timeStamp = TimeUtils.parseToLong(dateStr);
            mChosenDate.setTimeInMillis(timeStamp);
            mDateFormatter.setTimeZone(mChosenDate.getTimeZone());
            return mDateFormatter.format(mChosenDate.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
}
