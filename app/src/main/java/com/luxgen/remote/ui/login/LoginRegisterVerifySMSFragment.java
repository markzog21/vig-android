package com.luxgen.remote.ui.login;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.activity.MainActivity;
import com.luxgen.remote.ca.CommunicationAgentService;
import com.luxgen.remote.ui.dialog.AlertDialogFragment;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.ui.dialog.ProgressDialogFragment;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.CarWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.UserWebApi;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.car.GetCarKeyRequestData;
import com.luxgen.remote.network.model.car.GetCarKeyResponseData;
import com.luxgen.remote.network.model.user.CheckDmsPhoneRequestData;
import com.luxgen.remote.network.model.user.CheckDmsPhoneResponseData;
import com.luxgen.remote.network.model.user.CheckPhoneNoRequestData;
import com.luxgen.remote.network.model.user.CheckSmsRequestData;
import com.luxgen.remote.network.model.user.CheckSmsResponseData;
import com.luxgen.remote.network.model.user.GetMasterKeyUserRequestData;
import com.luxgen.remote.network.model.user.GetMasterKeyUserResponseData;
import com.luxgen.remote.network.model.user.LoginRequestData;
import com.luxgen.remote.network.model.user.LoginResponseData;
import com.luxgen.remote.util.Prefs;

import java.util.ArrayList;

import static com.luxgen.remote.ui.login.LoginActivity.FRAGMENT_TAG_LOGIN_REGISTER_KEY_ACTIVATION;
import static com.luxgen.remote.ca.CommunicationAgentService.CA_EXTRA_LOGIN_SUCCESS_RETURN_CODE;
import static com.luxgen.remote.ca.CommunicationAgentService.CA_EXTRA_MAX_LOGIN_RETRY_RETURN_CODE;
import static com.luxgen.remote.ui.login.LoginRegisterFragment.LOGIN_TYPE_FACEBOOK;
import static com.luxgen.remote.ui.login.LoginRegisterFragment.LOGIN_TYPE_GOOGLE;

public class LoginRegisterVerifySMSFragment extends CustomBaseFragment {

    private EditText mVerifyCode;
    private ProgressDialogFragment mProgressDialog;

    private boolean mIsGeneralUser = true;
    private String mEmail;
    private String mAccountId;
    private String mCarNo;
    private String mIkeyUID;
    private String mUID;
    private String mPassword;
    private String mDeviceId;
    private String mIkeyToken;
    private String mPhone;
    private String mCarId;
    private String mInputPhone;
    private int mLoginType;
    private long mDeviceCreateTime;
    private ArrayList<CarKeyData> mCars;
    private boolean isIkeyLoggedIn = false;

    public static LoginRegisterVerifySMSFragment newInstance(Bundle args) {
        LoginRegisterVerifySMSFragment fragment = new LoginRegisterVerifySMSFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private OnApiListener<CheckSmsResponseData> mOnApiListener = new OnApiListener<CheckSmsResponseData>() {
        @Override
        public void onApiTaskSuccessful(CheckSmsResponseData responseData) {
            if (responseData.isSuccessful()) {
                // NOTE: do not dismiss progress dialog
                doLogin();
            } else {
                dismissProgressDialog();
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mProgressDialog = ProgressDialogFragment.newInstance(getString(R.string.login_register_waiting));
            mProgressDialog.show(getFragmentManager(), "sendingSMSProgressDialog");
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            // TODO: check if need this
            dismissProgressDialog();
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<ResponseData> mGeneralUserOnApiListener = new OnApiListener<ResponseData>() {
        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            dismissProgressDialog();
            if (responseData.isSuccessful()) {
                showResentCodeDialog();
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mProgressDialog = ProgressDialogFragment.newInstance(getString(R.string.login_register_add_phone_sending_sms));
            mProgressDialog.show(getFragmentManager(), "sendingSMSProgressDialog");
        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            // TODO: check if need this
            dismissProgressDialog();
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<CheckDmsPhoneResponseData> mDMSUserOnApiListener = new OnApiListener<CheckDmsPhoneResponseData>() {
        @Override
        public void onApiTaskSuccessful(CheckDmsPhoneResponseData responseData) {
            dismissProgressDialog();
            if (responseData.isSuccessful()) {
                showResentCodeDialog();
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mProgressDialog = ProgressDialogFragment.newInstance(getString(R.string.login_register_add_phone_sending_sms));
            mProgressDialog.show(getFragmentManager(), "sendingSMSProgressDialog");
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            // TODO: check if need this
            dismissProgressDialog();
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<LoginResponseData> mLoginRequestListener = new OnApiListener<LoginResponseData>() {
        @Override
        public void onApiTaskSuccessful(LoginResponseData responseData) {
            if (responseData.isSuccessful()) {
                mIkeyUID = responseData.getKeyUid();
                mIkeyToken = responseData.getKeyToken();
                mUID = responseData.getUid();
                mPhone = responseData.getPhone();

                if (!isIkeyLoggedIn) {
                    saveUserData(mIkeyUID, mDeviceId);
                    Intent intent = new Intent(getContext(), CommunicationAgentService.class);
                    intent.putExtra(CommunicationAgentService.CA_EXTRA_IKEY_LOGIN, mIkeyUID);
                    getActivity().startService(intent);
                } else {
                    if (TextUtils.isEmpty(mIkeyToken)) {
                        dismissProgressDialog();
                        showServerErrorDialog(getString(R.string.login_error_i3_state));
                    } else {
                        saveUserData(mEmail, mPassword, mUID, mIkeyToken, mIkeyUID, mPhone, mDeviceId);
                        if (mLoginType == LOGIN_TYPE_FACEBOOK) {
                            updateUserFacebookData(mPassword);
                        } else if (mLoginType == LOGIN_TYPE_GOOGLE) {
                            updateUserGoogleData(mPassword);
                        }
                        doGetCarList();
                    }
                }
            } else {
                dismissProgressDialog();
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            // TODO: check if need this
            dismissProgressDialog();
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetCarKeyResponseData> mOnCarKeyListRequestListener = new OnApiListener<GetCarKeyResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetCarKeyResponseData responseData) {
            if (responseData.isSuccessful()) {
                mCars = responseData.getCarKeyDataList();
                if (mCars.size() > 0) {
                    for (CarKeyData carKeyData : mCars) {
                        if (carKeyData.getCarNo().equals(mCarNo)) {
                            Prefs.setCarKeyData(getContext(), carKeyData);
                            mCarId = carKeyData.getCarId();
                            break;
                        }
                    }
                }
                if (TextUtils.isEmpty(mCarId)) {
                    dismissProgressDialog();
                    showMainScreen();
                } else {
                    checkCarKeyStatus();
                }
            } else {
                dismissProgressDialog();
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {

        }
    };

    private OnApiListener<GetMasterKeyUserResponseData> mGetMasterKeyUserDataApiListener = new OnApiListener<GetMasterKeyUserResponseData>() {

        @Override
        public void onApiTaskSuccessful(GetMasterKeyUserResponseData responseData) {
            dismissProgressDialog();
            if (responseData != null && responseData.isSuccessful() && responseData.getStatus() == null) {
                showNextScreen();
            } else {
                showMainScreen();
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissProgressDialog();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {

        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {

        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        mIsGeneralUser = args.getBoolean("isGeneralUser", mIsGeneralUser);
        mEmail = args.getString("email", mEmail);
        mAccountId = args.getString("accountId", mAccountId);
        mCarNo = args.getString("carNo", mCarNo);
        mPassword = args.getString("password", mPassword);
        mDeviceId = args.getString("deviceId", mDeviceId);
        mLoginType = args.getInt("loginType", mLoginType);
        mInputPhone = args.getString("phone", mInputPhone);
        mDeviceCreateTime = Prefs.getDeviceIdCreateTime(getContext());
    }

    @Override
    protected void addActionToReceiveBroadcastFromCaService(IntentFilter intentFilter) {
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_IKEY_LOGIN);
    }

    @Override
    protected void onReceiveBroadcastFromCaService(Context context, Intent intent) {
        if (CommunicationAgentService.CA_STATUS_IKEY_LOGIN.equals(intent.getAction())) {
            if (intent.hasExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE)) {
                int returnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
                if (returnCode == CA_EXTRA_LOGIN_SUCCESS_RETURN_CODE) {
                    isIkeyLoggedIn = true;
                    doLogin();
                } else {
                    dismissProgressDialog();
                    if (returnCode == CA_EXTRA_MAX_LOGIN_RETRY_RETURN_CODE) {
                        showServerErrorDialog("ikey login error: 失敗重試已達上限");
                    } else {
                        showServerErrorDialog("ikey login error: " + returnCode);
                    }
                }
            } else if (intent.hasExtra(CommunicationAgentService.CA_STATUS_EXTRA_COMMAND)) {
                dismissProgressDialog();
                showServerErrorDialog("ikey: " + intent.getStringExtra(CommunicationAgentService.CA_STATUS_EXTRA_COMMAND));
            }

            return;
        }

        super.onReceiveBroadcastFromCaService(context, intent);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login_register_verify_phone, container, false);

        mVerifyCode = v.findViewById(R.id.edit_verification_code);
        mVerifyCode.requestFocus();
        mVerifyCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    verifySMS();
                    return true;
                }
                return false;
            }
        });

        Button nextButton = v.findViewById(R.id.btn_next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifySMS();
            }
        });

        TextView resentTextView = v.findViewById(R.id.txt_resent);
        resentTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doResentCode();
            }
        });

        return v;
    }

    private boolean isVigUser(ArrayList<CarKeyData> cars) {
        if (cars == null) {
            return false;
        }

        for (CarKeyData data : cars) {
            if (data.isMasterKeyAndReadyForActivation()) {
                return true;
            }
        }

        return false;
    }

    private String getFirstVigCarId(ArrayList<CarKeyData> cars) {
        if (cars == null) {
            return null;
        }

        for (CarKeyData data : cars) {
            if (data.isVig() && (data.isDriver() || data.isOwner())) {
                return data.getVigCarId();
            }
        }

        return null;
    }

    private boolean validate() {
        String code = mVerifyCode.getText().toString().trim();
        if (TextUtils.isEmpty(code)) {
            mVerifyCode.requestFocus();
            mVerifyCode.setError(getString(R.string.login_register_verify_phone_error_empty));
            return false;
        }

        return true;
    }

    private void verifySMS() {
        if (validate()) {
            doVerification();
        }
    }

    private CheckSmsRequestData buildCheckSMSData(String email, String accountId, String code) {
        CheckSmsRequestData data = new CheckSmsRequestData(email);
        data.setId(accountId);
        data.setSms(code);

        return data;
    }

    private CheckPhoneNoRequestData buildCheckPhoneData(String phone, String email) {
        CheckPhoneNoRequestData data = new CheckPhoneNoRequestData(email);
        data.setPhone(phone);
        return data;
    }

    private CheckDmsPhoneRequestData buildCheckDmsPhoneData(String phone, String email, String accountId, String carNo) {
        CheckDmsPhoneRequestData data = new CheckDmsPhoneRequestData(email);
        data.setPhone(phone);
        data.setAccountId(accountId);
        data.setCarNo(carNo);

        return data;
    }

    private LoginRequestData buildLoginRequest(String email, String password, String deviceId) {
        LoginRequestData data = new LoginRequestData(email);
        data.setPassword(password);
        data.setPhoneId(deviceId);
        data.setOsVersion(Build.VERSION.RELEASE);
        data.setPhoneBrand(Build.BRAND);

        return data;
    }

    private GetCarKeyRequestData buildGetCarKeyListData(String uid, String token) {
        GetCarKeyRequestData data = new GetCarKeyRequestData(uid, token);
        return data;
    }

    private void doVerification() {
        String code = mVerifyCode.getText().toString().trim();
        CheckSmsRequestData data = buildCheckSMSData(mEmail, mIsGeneralUser ? null : mAccountId, code);

        UserWebApi userWebApi = UserWebApi.getInstance(getActivity());
        userWebApi.checkSms(data, mOnApiListener);
    }

    private void doResentCode() {
        UserWebApi userWebApi = UserWebApi.getInstance(getActivity());
        if (mIsGeneralUser) {
            CheckPhoneNoRequestData data = buildCheckPhoneData(mInputPhone, mEmail);
            userWebApi.checkPhone(data, mGeneralUserOnApiListener);
        } else {
            CheckDmsPhoneRequestData data = buildCheckDmsPhoneData(mInputPhone, mEmail, mAccountId, mCarNo);
            userWebApi.checkDmsPhone(data, mDMSUserOnApiListener);
        }
    }

    private void doLogin() {
        LoginRequestData data = buildLoginRequest(mEmail, mPassword, mDeviceId);
        data.setDeviceCreateTime(mDeviceCreateTime);

        UserWebApi userWebApi = UserWebApi.getInstance(getActivity());
        userWebApi.login(data, mLoginRequestListener);
    }

    private void doGetCarList() {
        GetCarKeyRequestData data = buildGetCarKeyListData(mIkeyUID, mIkeyToken);

        CarWebApi carWebApi = CarWebApi.getInstance(getActivity());
        carWebApi.getCarKeyList(data, mOnCarKeyListRequestListener);
    }

    private Bundle packNewData() {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        args.putString("ikeyUID", mIkeyUID);
        args.putString("ikeyToken", mIkeyToken);
        args.putString("UID", mUID);
        args.putString("VigCarId", getFirstVigCarId(mCars));
        // ArrayList<CarKeyData> mCars;

        return args;
    }

    private void saveUserData(String iKeyUID, String deviceId) {
        UserInfo mUserInfo = new UserInfo(null, null);
        mUserInfo.setKeyUid(iKeyUID);
        mUserInfo.setDeviceId(deviceId);

        if (getContext() != null) {
            Prefs.setUserInfo(getContext(), mUserInfo);
        }
    }

    private void saveUserData(String email, String password, String UID, String iKeyToken, String iKeyUID, String phone, String deviceId) {
        UserInfo mUserInfo = new UserInfo(email, password);
        mUserInfo.setUid(UID);
        mUserInfo.setToken(iKeyToken);
        mUserInfo.setKeyUid(iKeyUID);
        mUserInfo.setPhone(phone);
        mUserInfo.setDeviceId(deviceId);

        if (getContext() != null) {
            Prefs.setUserInfo(getContext(), mUserInfo);
        }
    }

    private void updateUserFacebookData(String password) {
        if (getContext() == null) {
            return;
        }

        UserInfo mUserInfo = Prefs.getUserInfo(getContext());
        if (mUserInfo != null) {
            mUserInfo.setFacebookPassword(password);
        }

        Prefs.setUserInfo(getContext(), mUserInfo);
    }

    private void updateUserGoogleData(String password) {
        if (getContext() == null) {
            return;
        }

        UserInfo mUserInfo = Prefs.getUserInfo(getContext());
        if (mUserInfo != null) {
            mUserInfo.setGooglePassword(password);
        }

        Prefs.setUserInfo(getContext(), mUserInfo);
    }

    private void showMainScreen() {
        if (getActivity() == null) {
            return;
        }

        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.putExtras(packNewData());
        startActivity(intent);
        getActivity().finish();
    }

    private void showKeyActivationScreen() {
        Bundle args = packNewData();
        ((LoginActivity) getActivity()).navigate(this, LoginRegisterKeyActivationFragment.newInstance(args),
                FRAGMENT_TAG_LOGIN_REGISTER_KEY_ACTIVATION, false);
    }

    private void checkCarKeyStatus() {
        GetMasterKeyUserRequestData data = new GetMasterKeyUserRequestData(mIkeyUID, mIkeyToken, mCarId);

        UserWebApi userWebApi = UserWebApi.getInstance(getContext());
        userWebApi.getMasterKeyUserData(data, mGetMasterKeyUserDataApiListener);
    }

    private void showNextScreen() {
        boolean mIsVigUser = isVigUser(mCars);

        if (mIsGeneralUser) {
            showSuccessVerifyDialog();
        } else if (mIsVigUser) {
            showKeyActivationScreen();
        } else {
            showMainScreen();
        }
    }

    private void showSuccessVerifyDialog() {
        AlertDialogFragment dialog = AlertDialogFragment.newInstance(
                getString(R.string.login_register_verify_phone_success_title), getString(R.string.login_register_verify_phone_success_desc));
        dialog.setButtons(getString(R.string.btn_finish), null, new AlertDialogFragment.Callbacks() {
            @Override
            public void onPositiveButtonClicked() {
                showMainScreen();
            }

            @Override
            public void onNegativeButtonClicked() {
            }

            @Override
            public void onBackKeyPressed() {
                showMainScreen();
            }
        });
        dialog.show(getFragmentManager(), "showSuccessVerifyDialog");
    }

    private void showResentCodeDialog() {
        showConfirmDialog(getString(R.string.login_register_verify_phone_resent_title), getString(R.string.login_register_verify_phone_resent_desc));
    }

    private void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }
}
