package com.luxgen.remote.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.luxgen.remote.R;

public class EULADialogFragment extends DialogFragment {

    public interface Callbacks {
        void onPositiveButtonClicked();

        void onNegativeButtonClicked();
    }

    private Callbacks callbacks;
    private String positiveMsg, negativeMsg;
    private CheckBox mAgreeCheckBox;

    public static EULADialogFragment newInstance(String title, String message) {
        EULADialogFragment f = new EULADialogFragment();

        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);

        f.setArguments(args);
        return f;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();

        String title = args.getString("title");
        String message = args.getString("message");

        if (savedInstanceState != null) {
            positiveMsg = savedInstanceState.getString("positiveMsg", null);
            negativeMsg = savedInstanceState.getString("negativeMsg", null);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false);

        if (positiveMsg != null) {
            builder.setPositiveButton(positiveMsg, null);
        }

        if (negativeMsg != null) {
            builder.setNegativeButton(negativeMsg, null);
        }

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_eula, null);
        mAgreeCheckBox = view.findViewById(R.id.agree_checkbox);
        builder.setView(view);

        AlertDialog mAlertDialog = builder.create();
        mAlertDialog.setCanceledOnTouchOutside(false);
        mAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button confirm = mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                confirm.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        if (mAgreeCheckBox.isChecked()) {
                            if (callbacks != null) {
                                callbacks.onPositiveButtonClicked();
                            }
                            mAlertDialog.dismiss();
                        }
                    }
                });

                Button cancel = mAlertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (callbacks != null) {
                            callbacks.onNegativeButtonClicked();
                        }
                        mAlertDialog.dismiss();
                    }
                });
            }
        });


        return mAlertDialog;
    }

    public void setButtons(String positiveMsg, String negativeMsg, Callbacks callbacks) {
        this.positiveMsg = positiveMsg;
        this.negativeMsg = negativeMsg;
        this.callbacks = callbacks;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("positiveMsg", positiveMsg);
        outState.putString("negativeMsg", negativeMsg);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }
}
