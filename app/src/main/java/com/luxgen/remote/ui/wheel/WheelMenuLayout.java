package com.luxgen.remote.ui.wheel;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.model.WheelPage;
import com.luxgen.remote.model.WheelTileItem;

import java.util.ArrayList;
import java.util.Arrays;

public class WheelMenuLayout extends ConstraintLayout {

    private final static int SCROLL_DURATION_MS = 300;
    private final static int ON_BOARD_FADEIN_MS = 350;

    private final static int WHEEL_TILE_NONE = -1;
    private final static int WHEEL_TILE_1 = 0;
    private final static int WHEEL_TILE_2 = 1;
    private final static int WHEEL_TILE_3 = 2;
    private final static int WHEEL_TILE_4 = 3;
    private final static int WHEEL_TILE_5 = 4;
    public final static int WHEEL_TILE_MAX = 5;

    public final static int WHEEL_PAGES_MAX = 5;

    private TextView mTitleTextView;
    private TextView mDescTextView;
    private ImageView mBackImageView;
    private ImageView mWheelFirstHalfImageView;
    private ImageView mWheelSecondHalfImageView;

    private LinearLayout mDotsContainer;
    private ImageView[] mDotsImageView;

    private int mCurrentTile = -1;
    private int mCurrentPage = 0;
    private int mCurrentLevel = 0;
    private int mSavedCurrentPage = 0;
    private boolean mIsLongPressed = false;
    private ImageView[] mWheelTiles = new ImageView[WHEEL_TILE_MAX];
    private ImageView[] mPressedWheelTiles = new ImageView[WHEEL_TILE_MAX];

    private boolean mIsFirstHalfAtFront = false;

    private float mOriginX, mOriginY;

    private WheelPage[] mWheelPages = new WheelPage[WHEEL_PAGES_MAX];
    private WheelPage[] mWheelPagesOneClick = new WheelPage[2];

    private boolean mOnRight = true;
    private boolean mStarted = false;
    private boolean mIsOneClickInitialized = false;
    private boolean mIsDms = false;
    private boolean mIsVig = false;

    public interface WheelMenuEventListener {
        void onLevelChanged(int level);
    }

    private WheelMenuEventListener mWheelMenuEventListener;

    private Animation.AnimationListener mFrontToBackAnimationListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            mIsFirstHalfAtFront = false;
            mStarted = false;
            showTileIcons(true);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };

    private Animation.AnimationListener mBackToFrontAnimationListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            mIsFirstHalfAtFront = false;
            mStarted = false;
            showTileIcons(true);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };

    public WheelMenuLayout(Context context) {
        super(context);
        init(context);
    }

    public WheelMenuLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public WheelMenuLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context) {
        init(context, null, 0);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.WheelMenuLayout, defStyleAttr, 0);

        mOnRight = arr.getBoolean(R.styleable.WheelMenuLayout_onRight, true);

        arr.recycle();

        View v = LayoutInflater.from(context).inflate(mOnRight ? R.layout.wheel_menu_right : R.layout.wheel_menu_left, this);

        mWheelTiles[0] = v.findViewById(R.id.wheel_tile_1);
        mWheelTiles[1] = v.findViewById(R.id.wheel_tile_2);
        mWheelTiles[2] = v.findViewById(R.id.wheel_tile_3);
        mWheelTiles[3] = v.findViewById(R.id.wheel_tile_4);
        mWheelTiles[4] = v.findViewById(R.id.wheel_tile_5);

        mPressedWheelTiles[0] = v.findViewById(R.id.wheel_tile_1_pressed);
        mPressedWheelTiles[1] = v.findViewById(R.id.wheel_tile_2_pressed);
        mPressedWheelTiles[2] = v.findViewById(R.id.wheel_tile_3_pressed);
        mPressedWheelTiles[3] = v.findViewById(R.id.wheel_tile_4_pressed);
        mPressedWheelTiles[4] = v.findViewById(R.id.wheel_tile_5_pressed);

        mTitleTextView = v.findViewById(R.id.title);
        mDescTextView = v.findViewById(R.id.desc);
        mBackImageView = v.findViewById(R.id.back);
        mBackImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                closeOneClickItems(true);
            }
        });

        mDotsContainer = v.findViewById(R.id.dots);

        mWheelSecondHalfImageView = v.findViewById(R.id.wheel_second_half);
        mWheelFirstHalfImageView = v.findViewById(R.id.wheel_first_half);

        mWheelPages[0] = new WheelPage(null);

        start();
    }

    private void showOneClickItems() {
        mCurrentLevel = 1;
        mSavedCurrentPage = mCurrentPage;
        mCurrentPage = 0;

        hideTileIcons(true);
        showTileIcons(false);
        initialDotsIndicator(0);
        mBackImageView.setVisibility(View.VISIBLE);
        if (mWheelMenuEventListener != null) {
            mWheelMenuEventListener.onLevelChanged(1);
        }
    }

    public void closeOneClickItems(boolean animation) {
        mCurrentLevel = 0;
        mCurrentPage = mSavedCurrentPage;

        if (animation) {
            hideTileIcons(true);
            showTileIcons(false);
            initialDotsIndicator(mCurrentPage);
        }
        mBackImageView.setVisibility(View.INVISIBLE);
        if (mWheelMenuEventListener != null) {
            mWheelMenuEventListener.onLevelChanged(0);
        }
    }

    private void hideTileIcons(boolean doScaleAnimation) {
        for (ImageView mWheelTile : mWheelTiles) {
            if (doScaleAnimation) {
                Animation animation = getScaleDownAnimation(100);
                mWheelTile.startAnimation(animation);
            } else {
                mWheelTile.setVisibility(View.INVISIBLE);
            }
        }
    }

    private WheelTileItem[] getWheelTileItemsInCurrentPage() {
        WheelPage page = mCurrentLevel == 0 ? mWheelPages[mCurrentPage] : mWheelPagesOneClick[mCurrentPage];
        WheelTileItem[] items = null;
        if (page != null) {
            items = page.getItems();
        }
        return items;
    }

    private WheelTileItem getCurrentWheelTileItem(int which) {
        if (which == -1) {
            return null;
        }

        WheelTileItem[] items = getWheelTileItemsInCurrentPage();
        WheelTileItem item = null;
        if (items != null) {
            item = items[which];
        }

        return item;
    }

    public void showTileIcons(boolean doFadeInAnimation) {
        WheelTileItem[] items = getWheelTileItemsInCurrentPage();

        for (int i = 0; i < mWheelTiles.length; i++) {
            Animation animationEffect;
            if (doFadeInAnimation) {
                animationEffect = getFadeInAnimation(100);
                animationEffect.setStartOffset(20 * i);
            } else {
                animationEffect = getScaleUpAnimation(100);
            }

            WheelTileItem item = null;
            if (items != null) {
                item = items[i];
            }

            if (item != null && item.getActionId() != -1) {
                if (WheelTileItem.shouldHaveThisItem(mIsDms, mIsVig, item.getActionId())) {
                    mWheelTiles[i].setImageResource(item.getIcon());
                } else {
                    mWheelTiles[i].setImageResource(R.drawable.ic_smart_wheel_add_new);
                }
            } else {
                if (mCurrentLevel == 0) {
                    mWheelTiles[i].setImageResource(R.drawable.ic_smart_wheel_add_new);
                } else {
                    mWheelTiles[i].setImageDrawable(null);
                }
            }
            mWheelTiles[i].setVisibility(View.VISIBLE);
            mWheelTiles[i].startAnimation(animationEffect);
        }
    }

    public void toggleWheelTile(int wheelTile) {
        if (mCurrentTile != -1) {
            mPressedWheelTiles[mCurrentTile].setVisibility(View.INVISIBLE);
            mTitleTextView.setText(null);
            mDescTextView.setText(null);
        }

        if (wheelTile != -1) {
            mCurrentTile = wheelTile;

            WheelPage page = (mCurrentLevel == 0) ? mWheelPages[mCurrentPage] : mWheelPagesOneClick[mCurrentPage];
            WheelTileItem[] items = null;
            if (page != null) {
                items = page.getItems();
            }

            WheelTileItem item = null;
            if (items != null) {
                item = items[mCurrentTile];
            }

            if (item != null && item.getActionId() != -1) {
                if (WheelTileItem.shouldHaveThisItem(mIsDms, mIsVig, item.getActionId())) {
                    mPressedWheelTiles[mCurrentTile].setVisibility(View.VISIBLE);
                    mTitleTextView.setText(item.getTitle());
                    mDescTextView.setText(item.getDescription());
                } else {
                    mTitleTextView.setText(null);
                    mDescTextView.setText(null);
                }
            } else {
                mTitleTextView.setText(null);
                mDescTextView.setText(null);
            }
        }
    }

    public int whichTile(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        float slope = (mOriginY - y) / (mOriginX - x);
        double distance = Math.sqrt(Math.pow(x - mOriginX, 2) + Math.pow(y - mOriginY, 2));
        int radius = dp2px(getContext(), 180);
        int radiusInner = dp2px(getContext(), 70);

        if (distance < radius && distance > radiusInner) {
            if (slope > 1.37585) {
                return mOnRight ? WHEEL_TILE_1 : WHEEL_TILE_5;
            } else if (0.338 < slope && slope <= 1.37585) {
                return mOnRight ? WHEEL_TILE_2 : WHEEL_TILE_4;
            } else if (-0.338 <= slope && slope <= 0.338) {
                return WHEEL_TILE_3;
            } else if (-1.37585 <= slope && slope < -0.338) {
                return mOnRight ? WHEEL_TILE_4 : WHEEL_TILE_2;
            } else if (slope < -1.37585) {
                return mOnRight ? WHEEL_TILE_5 : WHEEL_TILE_1;
            }
        }

        return WHEEL_TILE_NONE;
    }

    private int dp2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    private Animation getScaleUpAnimation(int duration) {
        Animation scaleUp = new ScaleAnimation(
                0f, 1f, 0f, 1f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        scaleUp.setInterpolator(new AccelerateInterpolator());
        scaleUp.setDuration(duration);

        return scaleUp;
    }

    private Animation getScaleDownAnimation(int duration) {
        Animation scaleDown = new ScaleAnimation(
                1f, 0f, 1f, 0f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        scaleDown.setFillEnabled(true);
        scaleDown.setFillAfter(true);
        scaleDown.setInterpolator(new AccelerateInterpolator());
        scaleDown.setDuration(duration);

        return scaleDown;
    }

    private Animation getFadeInAnimation(int duration) {
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new AccelerateInterpolator());
        fadeIn.setDuration(duration);

        return fadeIn;
    }

    private Animation getBackScrollUpAnimation() {
        RotateAnimation rotate = new RotateAnimation(180, mOnRight ? 0 : 360,
                Animation.RELATIVE_TO_SELF, mOnRight ? 1f : 0f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(SCROLL_DURATION_MS);
        rotate.setInterpolator(new AccelerateDecelerateInterpolator());

        return rotate;
    }

    private Animation getBackScrollDownAnimation() {
        RotateAnimation rotate = new RotateAnimation(180, mOnRight ? 360 : 0,
                Animation.RELATIVE_TO_SELF, mOnRight ? 1f : 0f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(SCROLL_DURATION_MS);
        rotate.setInterpolator(new AccelerateDecelerateInterpolator());

        return rotate;
    }

    private Animation getFrontScrollUpAnimation() {
        RotateAnimation rotate = new RotateAnimation(0, mOnRight ? 180 : -180,
                Animation.RELATIVE_TO_SELF, mOnRight ? 1f : 0f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(SCROLL_DURATION_MS);
        rotate.setInterpolator(new AccelerateDecelerateInterpolator());

        return rotate;
    }

    private Animation getFrontScrollDownAnimation() {
        RotateAnimation rotate = new RotateAnimation(0, mOnRight ? -180 : 180,
                Animation.RELATIVE_TO_SELF, mOnRight ? 1f : 0f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(SCROLL_DURATION_MS);
        rotate.setInterpolator(new AccelerateDecelerateInterpolator());

        return rotate;
    }

    private Animation getOnBoardAnimation() {
        AnimationSet animation = new AnimationSet(false);
        animation.addAnimation(getFadeInAnimation(ON_BOARD_FADEIN_MS));
        animation.addAnimation(getBackScrollUpAnimation());
        animation.setFillEnabled(true);
        animation.setFillAfter(true);
        animation.setAnimationListener(mBackToFrontAnimationListener);

        return animation;
    }

    public void swipeUp() {
        int pageCount = getWheelPageCount();
        if (pageCount <= 1) {
            return;
        }

        mDotsImageView[mCurrentPage].setImageResource(R.drawable.page_indicator_unselected);

        if (mCurrentPage == pageCount - 1) {
            mCurrentPage = 0;
        } else {
            mCurrentPage++;
        }

        mDotsImageView[mCurrentPage].setImageResource(R.drawable.page_indicator_selected);

        hideTileIcons(false);
        if (mIsFirstHalfAtFront) {
            Animation firstHalfAnimation = getFrontScrollUpAnimation();
            firstHalfAnimation.setFillEnabled(true);
            firstHalfAnimation.setFillAfter(true);
            firstHalfAnimation.setAnimationListener(mFrontToBackAnimationListener);
            mWheelFirstHalfImageView.startAnimation(firstHalfAnimation);

            Animation secondHalfAnimation = getBackScrollDownAnimation();
            secondHalfAnimation.setFillEnabled(true);
            secondHalfAnimation.setFillAfter(true);
            mWheelSecondHalfImageView.startAnimation(secondHalfAnimation);
            mWheelSecondHalfImageView.setVisibility(View.VISIBLE);
        } else {
            Animation firstHalfAnimation = getBackScrollDownAnimation();
            firstHalfAnimation.setFillEnabled(true);
            firstHalfAnimation.setFillAfter(true);
            firstHalfAnimation.setAnimationListener(mBackToFrontAnimationListener);
            mWheelFirstHalfImageView.startAnimation(firstHalfAnimation);

            Animation secondHalfAnimation = getFrontScrollUpAnimation();
            secondHalfAnimation.setFillEnabled(true);
            secondHalfAnimation.setFillAfter(true);
            mWheelSecondHalfImageView.startAnimation(secondHalfAnimation);
            mWheelSecondHalfImageView.setVisibility(View.VISIBLE);
        }
    }

    public void swipeDown() {
        int pageCount = getWheelPageCount();
        if (pageCount <= 1) {
            return;
        }

        mDotsImageView[mCurrentPage].setImageResource(R.drawable.page_indicator_unselected);

        if (mCurrentPage == 0) {
            mCurrentPage = pageCount - 1;
        } else {
            mCurrentPage--;
        }

        mDotsImageView[mCurrentPage].setImageResource(R.drawable.page_indicator_selected);

        hideTileIcons(false);
        if (mIsFirstHalfAtFront) {
            Animation firstHalfAnimation = getFrontScrollDownAnimation();
            firstHalfAnimation.setFillEnabled(true);
            firstHalfAnimation.setFillAfter(true);
            firstHalfAnimation.setAnimationListener(mFrontToBackAnimationListener);
            mWheelFirstHalfImageView.startAnimation(firstHalfAnimation);

            Animation secondHalfAnimation = getBackScrollUpAnimation();
            secondHalfAnimation.setFillEnabled(true);
            secondHalfAnimation.setFillAfter(true);
            mWheelSecondHalfImageView.startAnimation(secondHalfAnimation);
            mWheelSecondHalfImageView.setVisibility(View.VISIBLE);
        } else {
            Animation firstHalfAnimation = getBackScrollUpAnimation();
            firstHalfAnimation.setFillEnabled(true);
            firstHalfAnimation.setFillAfter(true);
            firstHalfAnimation.setAnimationListener(mBackToFrontAnimationListener);
            mWheelFirstHalfImageView.startAnimation(firstHalfAnimation);

            Animation secondHalfAnimation = getFrontScrollDownAnimation();
            secondHalfAnimation.setFillEnabled(true);
            secondHalfAnimation.setFillAfter(true);
            mWheelSecondHalfImageView.startAnimation(secondHalfAnimation);
            mWheelSecondHalfImageView.setVisibility(View.VISIBLE);
        }
    }

    private void initialDotsIndicator(int selected) {
        int marginTopBottomPx = dp2px(getContext(), 4);
        mDotsContainer.removeAllViews();
        mDotsImageView = new ImageView[getWheelPageCount()];
        for (int i = 0; i < mDotsImageView.length; i++) {
            mDotsImageView[i] = new ImageView(getContext());

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, marginTopBottomPx, 0, marginTopBottomPx);
            mDotsImageView[i].setLayoutParams(params);
            mDotsImageView[i].setImageResource(R.drawable.page_indicator_unselected);

            mDotsContainer.addView(mDotsImageView[i], mDotsContainer.getChildCount());
            mDotsContainer.bringToFront();
        }
        mDotsImageView[selected].setImageResource(R.drawable.page_indicator_selected);
    }

    public void setWheelPages(WheelPage[] pages) {
        if (pages != null && pages.length > 0) {
            mWheelPages = Arrays.copyOf(pages, pages.length);
        }

        mCurrentPage = 0;
        mCurrentLevel = 0;
        initialDotsIndicator(0);
    }

    public int getWheelPageCount() {
        if (mCurrentLevel == 1) {
            return 2;
        }

        int i = 0;
        for (; i < WHEEL_PAGES_MAX; i++) {
            WheelPage page = mWheelPages[i];
            if (page == null) {
                break;
            }
        }

        return i;
    }

    public void onTouchEvent(View v, MotionEvent event) {
        mOriginX = mOnRight ? v.getWidth() : 0;
        mOriginY = v.getHeight() / 2;

        if (event.getAction() == MotionEvent.ACTION_UP) {
            mIsLongPressed = false;
            toggleWheelTile(-1);
        }

        if (mIsLongPressed) {
            toggleWheelTile(whichTile(event));
        }
    }

    private void initialOneClickItems() {
        if (mIsOneClickInitialized) {
            return;
        }
        mIsOneClickInitialized = true;
        mWheelPagesOneClick[0] = new WheelPage(new WheelTileItem[]{
                new WheelTileItem(R.drawable.ic_smart_wheel_sales_consultant, R.drawable.ic_smart_wheel_set_sales_consultant, R.string.wheel_sales_consultant_title, R.string.wheel_sales_consultant_title, R.string.wheel_sales_consultant_desc, WheelTileItem.ACTION_SALES_CONSULTANT, WheelTileItem.COLOR_YELLOW),
                new WheelTileItem(R.drawable.ic_smart_wheel_110, R.drawable.ic_smart_wheel_set_110, R.string.wheel_110_title, R.string.wheel_110_title, R.string.wheel_110_desc, WheelTileItem.ACTION_110, WheelTileItem.COLOR_YELLOW),
                new WheelTileItem(R.drawable.ic_smart_wheel_rescue, R.drawable.ic_smart_wheel_set_rescue, R.string.wheel_rescue_title, R.string.wheel_rescue_title, R.string.wheel_rescue_desc, WheelTileItem.ACTION_RESCUE, WheelTileItem.COLOR_YELLOW),
                new WheelTileItem(R.drawable.ic_smart_wheel_secratery, R.drawable.ic_smart_wheel_set_secratery, R.string.wheel_secretary_title, R.string.wheel_secretary_title, R.string.wheel_secretary_desc, WheelTileItem.ACTION_SECRATERY, WheelTileItem.COLOR_YELLOW),
                new WheelTileItem(R.drawable.ic_smart_wheel_reservation, R.drawable.ic_smart_wheel_set_reservation, R.string.wheel_reservation_title, R.string.wheel_reservation_title, R.string.wheel_reservation_desc, WheelTileItem.ACTION_RESERVATION, WheelTileItem.COLOR_YELLOW),
        });
        mWheelPagesOneClick[1] = new WheelPage(new WheelTileItem[]{
                new WheelTileItem(R.drawable.ic_smart_wheel_driver_help, R.drawable.ic_smart_wheel_set_driver_help, R.string.wheel_driver_help_title, R.string.wheel_driver_help_title, R.string.wheel_driver_help_desc, WheelTileItem.ACTION_DRIVER_HELP, WheelTileItem.COLOR_YELLOW),
        });
    }

    public int onSingleTapConfirmed(MotionEvent event) {
        int actionId = -1;
        int wheelTile = whichTile(event);
        WheelTileItem item = getCurrentWheelTileItem(wheelTile);
        if (item != null) {
            actionId = item.getActionId();
            if (!WheelTileItem.shouldHaveThisItem(mIsDms, mIsVig, actionId)) {
                actionId = WheelTileItem.ACTION_EMPTY;
            } else {
                if (actionId == WheelTileItem.ACTION_CLICK_SERVICE) {
                    initialOneClickItems();
                    showOneClickItems();
                    return -1;
                }
            }
        } else {
            if (wheelTile != WHEEL_TILE_NONE && mCurrentLevel == 0) {
                actionId = WheelTileItem.ACTION_EMPTY;
            }
        }
        return actionId;
    }

    public void onLongPress(MotionEvent event) {
        mIsLongPressed = true;
        toggleWheelTile(whichTile(event));
    }

    public boolean isInOneClickMenu() {
        return mCurrentLevel == 1;
    }

    public ArrayList<WheelPage> getWheelPages() {
        ArrayList<WheelPage> mWheelPageArrayList = new ArrayList<>();
        for (WheelPage page : mWheelPages) {
            if (page != null) {
                mWheelPageArrayList.add(page);
            }
        }
        return mWheelPageArrayList;
    }

    public void setWheelTile(int which, WheelTileItem item) {
        WheelPage page = mCurrentLevel == 0 ? mWheelPages[mCurrentPage] : mWheelPagesOneClick[mCurrentPage];
        page.getItems()[which] = item;
        start();
    }

    public void start() {
        if (mStarted) {
            return;
        }
        mStarted = true;
        mWheelFirstHalfImageView.startAnimation(getOnBoardAnimation());
    }

    public void stop() {
        hideTileIcons(true);
    }

    public void setDms(boolean val) {
        mIsDms = val;
    }

    public void setVig(boolean val) {
        mIsVig = val;
    }

    public void setWheelMenuEventListener(WheelMenuEventListener listener) {
        mWheelMenuEventListener = listener;
    }
}
