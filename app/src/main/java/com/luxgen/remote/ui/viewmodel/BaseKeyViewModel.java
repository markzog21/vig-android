package com.luxgen.remote.ui.viewmodel;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.luxgen.remote.model.KeyInfo;
import com.luxgen.remote.network.CarWebApi;
import com.luxgen.remote.network.model.car.CarKeyData;

import java.util.ArrayList;

public class BaseKeyViewModel extends BaseViewModel {

    private CarKeyRepository mCarKeyRepo;
    private KeyRepository mKeyRepo;

    protected CarKeyRepository getCarKeyRepo() {
        return mCarKeyRepo;
    }

    protected KeyRepository getKeyRepo() {
        return mKeyRepo;
    }

    private MutableLiveData<ArrayList<CarKeyData>> mCarKeyDataListLiveData;
    private MutableLiveData<ArrayList<KeyInfo>> mKeyInfoListLiveData;
    private ArrayList<CarKeyData> mCarKeyDataList;
    private ArrayList<KeyInfo> mKeyInfoList;

    public BaseKeyViewModel(@NonNull Application application) {
        super(application);

        mCarKeyRepo = new CarKeyRepository(getDatabase().carKeyModel(), CarWebApi.getInstance(application.getApplicationContext()));
        mKeyRepo = new KeyRepository(application.getApplicationContext(), getDatabase());
    }

    public void checkKeyInfoListFromDb(ArrayList<CarKeyData> carKeyArrayList) {
        mKeyRepo.checkInitData(carKeyArrayList);
    }

    public ArrayList<CarKeyData> getCarKeyDataListFromDb() {
        return (ArrayList<CarKeyData>) mCarKeyRepo.loadListFromDb();
    }

    public void syncCarKeyDataListToDb(ArrayList<CarKeyData> list) {
        mCarKeyRepo.syncListToDb(list);
    }

    public ArrayList<KeyInfo> getKeyInfoListFromDb(String carId) {
        mKeyInfoList = mKeyRepo.fetchListFromDb(carId);
        return mKeyInfoList;
    }

    /**
     * 取得 KeyInfo 的 List，若還未初始化，則會先呼叫 {@link #getKeyInfoList(String)}，
     * 若仍是 null，則表示 DB 裡仍沒能找到。<br />
     * 或是因為資料有更新，想要更新的話，請直接呼叫 {@link #getKeyInfoListFromDb(String)}
     */
    public ArrayList<KeyInfo> getKeyInfoList(String carId) {
        if (mKeyInfoList == null) {
            return getKeyInfoListFromDb(carId);
        }
        return mKeyInfoList;
    }

    /**
     * 更新既有的 KeyInfo 到資料庫中，若不是即有的 KeyInfo 則會更新失敗
     */
    public boolean updateKeyInfoToDb(KeyInfo keyInfo) {
        return mKeyRepo.updateToDb(keyInfo);
    }

    /**
     * 從已取出成的資料陣列中取得 {@link KeyRepository.KeyPosition} 位置資訊
     */
    public KeyInfo getKeyInfoWithPosition(@KeyRepository.KeyPosition int position) {
        KeyInfo matchedKeyInfo = null;
        if (mKeyInfoList != null) {
            for (KeyInfo keyInfo : mKeyInfoList) {
                if (keyInfo.getPosition() == position) {
                    matchedKeyInfo = keyInfo;
                    break;
                }
            }
        }
        return matchedKeyInfo;
    }

}
