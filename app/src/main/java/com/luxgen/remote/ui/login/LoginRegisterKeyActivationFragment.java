package com.luxgen.remote.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.activity.MainActivity;
import com.luxgen.remote.custom.CustomBaseFragment;

import static com.luxgen.remote.ui.login.LoginActivity.FRAGMENT_TAG_LOGIN_REGISTER_ACTIVATING_KEY;

public class LoginRegisterKeyActivationFragment extends CustomBaseFragment {

    public static LoginRegisterKeyActivationFragment newInstance(Bundle args) {
        LoginRegisterKeyActivationFragment fragment = new LoginRegisterKeyActivationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login_register_key_activation, container, false);

        Button activateButton = v.findViewById(R.id.btn_activate_main_key);
        activateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activateMainKey();
            }
        });

        TextView skipTextView = v.findViewById(R.id.txt_skip);
        skipTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMainActivity();
            }
        });

        return v;
    }

    private void activateMainKey() {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        ((LoginActivity) getActivity()).navigate(this, LoginRegisterActivatingMainKeyFragment.newInstance(args),
                FRAGMENT_TAG_LOGIN_REGISTER_ACTIVATING_KEY, true);
    }

    private void startMainActivity() {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.putExtras(args);
        startActivity(intent);
        // unregisterCaStatusBroadcastReceiver();
        getActivity().finish();
    }
}
