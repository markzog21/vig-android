package com.luxgen.remote.ui.arremote;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomFragmentsAppCompatActivity;
import com.luxgen.remote.util.Prefs;

import static com.luxgen.remote.ui.arremote.RemoteControlFragment.REQUEST_NONE;
import static com.luxgen.remote.ui.arremote.RemoteControlFragment.REQUEST_OPTIMIZE;

public class RemoteControlActivity extends CustomFragmentsAppCompatActivity {

    public final static String FRAGMENT_TAG_REMOTE_CONTROL_MENU = "remotecontrolmenu";
    public final static String FRAGMENT_TAG_REMOTE_CONTROL_PIN_SETUP = "remotecontrolpinsetup";
    public final static String FRAGMENT_TAG_REMOTE_CONTROL_PIN_VERIFY = "remotecontrolpinverify";
    public final static String FRAGMENT_TAG_REMOTE_CONTROL_HISTORY = "remotecontrolhistory";
    public final static String FRAGMENT_TAG_REMOTE_CONTROL_AIR_DATA = "remotecontrolairdata";
    public final static String FRAGMENT_TAG_REMOTE_CONTROL_OPTIMIZE = "remotecontroloptimize";

    private boolean mIsCheckCarProgressOnGoing = false;

    public static Intent newIntent(Context context, String startUpFragment) {
        Intent intent = new Intent(context, RemoteControlActivity.class);
        intent.putExtra("startUp", startUpFragment);
        return intent;
    }

    public static Intent newIntent(Context context, int function) {
        Intent intent = new Intent(context, RemoteControlActivity.class);
        if (function == REQUEST_OPTIMIZE) {
            intent.putExtra("startUp", FRAGMENT_TAG_REMOTE_CONTROL_AIR_DATA);
        } else {
            intent.putExtra("startUp", FRAGMENT_TAG_REMOTE_CONTROL_MENU);
        }
        intent.putExtra("function", function);
        return intent;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_remote_control;
    }

    @Override
    protected void setupTitle(String tag) {
        if (FRAGMENT_TAG_REMOTE_CONTROL_PIN_SETUP.equals(tag)) {
            getSupportActionBar().setTitle(R.string.remote_control_pincode_title);
        } else if (FRAGMENT_TAG_REMOTE_CONTROL_MENU.equals(tag) || FRAGMENT_TAG_REMOTE_CONTROL_PIN_VERIFY.equals(tag)) {
            getSupportActionBar().setTitle(R.string.remote_control_title);
        } else if (FRAGMENT_TAG_REMOTE_CONTROL_HISTORY.equals(tag)) {
            getSupportActionBar().setTitle(R.string.remote_control_history_title);
        } else if (FRAGMENT_TAG_REMOTE_CONTROL_AIR_DATA.equals(tag)) {
            getSupportActionBar().setTitle(R.string.remote_control_air_data_title);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.remote_control_title);
        } else {
            setTitle(R.string.remote_control_title);
        }

        Intent intent = getIntent();
        String startUpFragment = FRAGMENT_TAG_REMOTE_CONTROL_MENU;
        int function = REQUEST_NONE;
        if (intent != null) {
            startUpFragment = intent.getStringExtra("startUp");
            function = intent.getIntExtra("function", function);
        }

        if (FRAGMENT_TAG_REMOTE_CONTROL_AIR_DATA.equals(startUpFragment)) {
            show(RemoteControlAirDataFragment.newInstance(false), FRAGMENT_TAG_REMOTE_CONTROL_AIR_DATA);
        } else {
            String mPinCode = Prefs.getPinCode(this);
            if (mPinCode == null || mPinCode.isEmpty()) {
                show(RemoteControlSetupPinFragment.newInstance(startUpFragment, function), FRAGMENT_TAG_REMOTE_CONTROL_PIN_SETUP);
            } else {
                show(RemoteControlFragment.newInstance(function), FRAGMENT_TAG_REMOTE_CONTROL_MENU);
            }
        }
        bindCaService();
    }

    @Override
    protected void onDestroy() {
        unbindCaService();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (!mIsCheckCarProgressOnGoing) {
            super.onBackPressed();
        }
    }

    public void setCheckCarProgress(boolean check) {
        mIsCheckCarProgressOnGoing = check;
    }
}
