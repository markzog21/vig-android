package com.luxgen.remote.ui.oks;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.util.DialUtils;

public class CarAccidentInsuranceFragment extends CustomBaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_car_accident_insurance, container, false);

        Button buttonCallService = v.findViewById(R.id.btn_call_service);
        buttonCallService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialSales();
            }
        });

        Button buttonFinish = v.findViewById(R.id.btn_finish);
        buttonFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        return v;
    }

    private void dialSales() {
        startActivity(DialUtils.callSales(getContext()));
    }
}
