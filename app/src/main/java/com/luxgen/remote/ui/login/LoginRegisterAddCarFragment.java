package com.luxgen.remote.ui.login;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.ui.dialog.AlertDialogFragment;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.ui.dialog.ProgressDialogFragment;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.UserWebApi;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.user.UserRegisterRequestData;

import static com.luxgen.remote.ui.login.LoginActivity.FRAGMENT_TAG_LOGIN_REGISTER_ADD_PHONE;

public class LoginRegisterAddCarFragment extends CustomBaseFragment {

    private EditText mIdNumberEditText;
    private EditText mPlateNumberEditText;
    private ProgressDialogFragment mProgressDialog;
    private String mEmail;

    public static LoginRegisterAddCarFragment newInstance(Bundle args) {
        LoginRegisterAddCarFragment fragment = new LoginRegisterAddCarFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private OnApiListener<ResponseData> mOnApiListener = new OnApiListener<ResponseData>() {
        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            mProgressDialog.dismiss();
            if (responseData.isSuccessful()) {
                showAddPhoneScreen(false);
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            mProgressDialog.dismiss();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mProgressDialog = ProgressDialogFragment.newInstance(getString(R.string.login_register_add_new_car_checking));
            mProgressDialog.show(getFragmentManager(), "registerProgressDialog");
        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            // TODO: check if need this
            mProgressDialog.dismiss();
            showMultiLoginErrorDialog();
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        mEmail = args.getString("email");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login_register_add_car, container, false);

        mIdNumberEditText = v.findViewById(R.id.edit_id_number);
        mPlateNumberEditText = v.findViewById(R.id.edit_plate_number);

        Button nextButton = v.findViewById(R.id.btn_next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkDMS();
            }
        });

        TextView skipTextView = v.findViewById(R.id.txt_skip);
        skipTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmSkipDialog();
            }
        });

        return v;
    }

    private boolean validate() {
        String idNumber = mIdNumberEditText.getText().toString().trim();
        if (TextUtils.isEmpty(idNumber)) {
            mIdNumberEditText.requestFocus();
            mIdNumberEditText.setError(getString(R.string.login_register_add_new_car_id_number_hint));
            return false;
        }

        String plateNumber = mPlateNumberEditText.getText().toString().trim();
        if (TextUtils.isEmpty(plateNumber)) {
            mPlateNumberEditText.requestFocus();
            mPlateNumberEditText.setError(getString(R.string.login_register_add_new_car_plate_number_hint));
            return false;
        }

        return true;
    }

    private void checkDMS() {
        if (validate()) {
            registerDMS();
        }
    }

    private UserRegisterRequestData buildRegisterData(String ikeyAccount, String accountId, String carNo) {
        UserRegisterRequestData data = new UserRegisterRequestData(ikeyAccount);
        data.setAccountId(accountId);
        data.setCarNo(carNo);

        return data;
    }

    private void registerDMS() {
        String accountId = mIdNumberEditText.getText().toString().trim().toUpperCase();
        String carNo = mPlateNumberEditText.getText().toString().trim().toUpperCase();

        UserRegisterRequestData data = buildRegisterData(mEmail, accountId, carNo);

        UserWebApi userWebApi = UserWebApi.getInstance(getActivity());
        userWebApi.register(data, mOnApiListener);
    }

    private Bundle buildArguments(boolean generalUser) {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        args.putBoolean("isGeneralUser", generalUser);
        if (!generalUser) {
            String accountId = mIdNumberEditText.getText().toString().trim().toUpperCase();
            String carNo = mPlateNumberEditText.getText().toString().trim().toUpperCase();

            args.putString("accountId", accountId);
            args.putString("carNo", carNo);
        }

        return args;
    }

    private void showAddPhoneScreen(boolean generalUser) {
        ((LoginActivity) getActivity()).navigate(this, LoginRegisterAddPhoneFragment.newInstance(buildArguments(generalUser)),
                FRAGMENT_TAG_LOGIN_REGISTER_ADD_PHONE, true);
    }

    private void showConfirmSkipDialog() {
        showConfirmDialog(getString(R.string.login_register_add_new_car_skip_title), getString(R.string.login_register_add_new_car_skip_message), new AlertDialogFragment.Callbacks() {
            @Override
            public void onPositiveButtonClicked() {
                showAddPhoneScreen(true);
            }

            @Override
            public void onNegativeButtonClicked() {

            }

            @Override
            public void onBackKeyPressed() {

            }
        });
    }
}
