package com.luxgen.remote.ui.oks;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.luxgen.remote.R;
import com.luxgen.remote.ca.CommunicationAgentService;
import com.luxgen.remote.ui.dialog.AlertDialogFragment;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.ui.dialog.EULADialogFragment;
import com.luxgen.remote.ui.dialog.ProgressDialogFragment;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.CarWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.VigRequestData;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.car.CarTrackData;
import com.luxgen.remote.network.model.car.GetCarTrackResponseData;
import com.luxgen.remote.util.DialUtils;
import com.luxgen.remote.util.Prefs;

import java.util.ArrayList;

import static com.luxgen.remote.ui.oks.CarStolenTrackingActivity.INTENT_EXTRA_NAME_LATITUDE;
import static com.luxgen.remote.ui.oks.CarStolenTrackingActivity.INTENT_EXTRA_NAME_LONGITUDE;

public class CarStolenTrackingFragment extends CustomBaseFragment implements OnMapReadyCallback {

    private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";

    private double mLat, mLng;

    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private ArrayList<Polyline> mPolylines = new ArrayList<>();
    private boolean mShowTracking = false;
    private boolean mIsGettingTracks = false;
    private ImageView mShowTrackingImageView;
    private TextView mShowTrackingTextView;
    private boolean mShowEULA = true;
    private boolean mSetSpeedLimit = false;
    private ImageView mSpeedLimitImageView;
    private TextView mSpeedLimitTextView;
    private View mSpeedLimitContainer;
    private ProgressDialogFragment mProgressDialog;

    private UserInfo mUserInfo;
    private CarKeyData mSelectedCarKeyData;

    private void getLocation(Bundle args) {
        if (args != null) {
            mLat = args.getDouble(INTENT_EXTRA_NAME_LATITUDE);
            mLng = args.getDouble(INTENT_EXTRA_NAME_LONGITUDE);
        }
    }

    private View.OnClickListener mOnSpeedLimitClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mShowEULA) {
                showEULADialog();
            }
        }
    };

    private View.OnClickListener mOnTrackingClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mIsGettingTracks) {
                return;
            }
            mIsGettingTracks = true;

            mShowTracking = !mShowTracking;
            updateShowTrackingView();
            if (mShowTracking) {
                if (mSelectedCarKeyData != null) {
                    getCarTrackingData();
                } else {
                    mIsGettingTracks = false;
                }
            } else {
                for (Polyline polyline : mPolylines) {
                    polyline.remove();
                }
                mPolylines.clear();
                mIsGettingTracks = false;
            }
        }
    };

    private OnApiListener<GetCarTrackResponseData> mGetCarTrackListener = new OnApiListener<GetCarTrackResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetCarTrackResponseData responseData) {
            mProgressDialog.dismiss();
            if (responseData.isSuccessful()) {
                showCarTracks(responseData.getList());
            } else {
                showServerErrorDialog(responseData.getErrorMessage());
            }
            mIsGettingTracks = false;
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            mProgressDialog.dismiss();
            showServerErrorDialog(failMessage);
            mIsGettingTracks = false;
        }

        @Override
        public void onPreApiTask() {
            mProgressDialog = ProgressDialogFragment.newInstance(getString(R.string.car_stolen_tracking_getting));
            mProgressDialog.show(getFragmentManager(), "getCarTracksDialog");
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            mProgressDialog.dismiss();
            showMultiLoginErrorDialog();
        }
    };

    private ArrayList<CarTrackData> generateFakeTrackingData() {
        Gson gson = new Gson();
        ArrayList<CarTrackData> mCarTrack = new ArrayList<>();
        mCarTrack.add(gson.fromJson("" +
                " { \"lat\":\"25.080587\", \"lon\":\"121.564073\", " +
                "    \"speed\":\"0\", \"timeStamp\":\"0\" }", CarTrackData.class));
        mCarTrack.add(gson.fromJson("" +
                " { \"lat\":\"25.080838\", \"lon\":\"121.565059\", " +
                "    \"speed\":\"0\", \"timeStamp\":\"0\" }", CarTrackData.class));
        mCarTrack.add(gson.fromJson("" +
                " { \"lat\":\"25.082301\", \"lon\":\"121.565469\", " +
                "    \"speed\":\"0\", \"timeStamp\":\"0\" }", CarTrackData.class));
        mCarTrack.add(gson.fromJson("" +
                " { \"lat\":\"25.084540\", \"lon\":\"121.561172\", " +
                "    \"speed\":\"0\", \"timeStamp\":\"0\" }", CarTrackData.class));
        mCarTrack.add(gson.fromJson("" +
                " { \"lat\":\"25.080579\", \"lon\":\"121.562541\", " +
                "    \"speed\":\"0\", \"timeStamp\":\"0\" }", CarTrackData.class));
        mCarTrack.add(gson.fromJson("" +
                " { \"lat\":\"25.079024\", \"lon\":\"121.564365\", " +
                "    \"speed\":\"0\", \"timeStamp\":\"0\" }", CarTrackData.class));
        return mCarTrack;
    }

    private void showCarTracks(ArrayList<CarTrackData> tracks) {
        if (tracks == null) {
            tracks = generateFakeTrackingData();
        }

        for (int i = 0; i < tracks.size() - 1; i++) {
            LatLng start = new LatLng(tracks.get(i).getLat(), tracks.get(i).getLon());
            LatLng end = new LatLng(tracks.get(i + 1).getLat(), tracks.get(i + 1).getLon());

            Polyline polyline = mGoogleMap.addPolyline(new PolylineOptions()
                    .add(start, end)
                    .width(16)
                    .color(ContextCompat.getColor(getContext(), R.color.colorAccent)));
            mPolylines.add(polyline);
        }
    }

    private VigRequestData buildVigRequestData() {
        VigRequestData data = new VigRequestData(mUserInfo.getKeyUid(), mUserInfo.getToken(), mSelectedCarKeyData.getCarId());

        return data;
    }

    private void getCarTrackingData() {
        VigRequestData vigRequestData = buildVigRequestData();

        CarWebApi carWebApi = CarWebApi.getInstance(getContext());
        carWebApi.getCarTrack(vigRequestData, mGetCarTrackListener);
    }

    private void updateShowTrackingView() {
        if (mShowTracking) {
            mShowTrackingImageView.setImageResource(R.drawable.ic_track_pressed);
            mShowTrackingTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.bgColorDefault));
        } else {
            mShowTrackingImageView.setImageResource(R.drawable.ic_track_normal);
            mShowTrackingTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.textColorDark));
        }
    }

    private void updateSpeedLimitViews() {
        if (mSetSpeedLimit) {
            mSpeedLimitImageView.setImageResource(R.drawable.ic_speed_limit_pressed);
            mSpeedLimitTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.bgColorDefault));
        } else {
            mSpeedLimitImageView.setImageResource(R.drawable.ic_speed_limit_normal);
            mSpeedLimitTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.textColorDark));
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        getLocation(args);

        View v = inflater.inflate(R.layout.fragment_car_stolen_tracking, container, false);

        mMapView = v.findViewById(R.id.map);

        mSpeedLimitContainer = v.findViewById(R.id.speed_limit_container);
        mSpeedLimitContainer.setOnClickListener(mOnSpeedLimitClickListener);
        mSpeedLimitImageView = v.findViewById(R.id.speed_limit_icon);
        mSpeedLimitTextView = v.findViewById(R.id.speed_limit_text);

        View showTrackingContainer = v.findViewById(R.id.show_tracking_container);
        showTrackingContainer.setOnClickListener(mOnTrackingClickListener);
        mShowTrackingImageView = v.findViewById(R.id.show_tracking_icon);
        mShowTrackingTextView = v.findViewById(R.id.show_tracking_text);

        View dial110View = v.findViewById(R.id.dial_110);
        dial110View.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dialIntent = DialUtils.callPolice();
                startActivity(dialIntent);
            }
        });

        Button button = v.findViewById(R.id.btn_finish_tracking);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFinishDialog();
            }
        });

        return v;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }

        if (mMapView != null) {
            mMapView.onSaveInstanceState(mapViewBundle);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }

        if (mMapView != null) {
            mMapView.onCreate(mapViewBundle);
            mMapView.getMapAsync(this);
        }

        mUserInfo = Prefs.getUserInfo(getContext());
        mSelectedCarKeyData = Prefs.getCarKeyData(getContext());
        if (mSelectedCarKeyData == null) {
            Toast.makeText(getContext(), "尚未有車輛", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        LatLng carLocation = new LatLng(mLat, mLng);
        mGoogleMap.addMarker(new MarkerOptions()
                .position(carLocation)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ctn_car_location)));
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(carLocation, 15), 1500, null);
    }

    @Override
    public void onStart() {
        if (mMapView != null) {
            mMapView.onStart();
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (mMapView != null) {
            mMapView.onStop();
        }
        super.onStop();
    }

    @Override
    public void onLowMemory() {
        if (mMapView != null) {
            mMapView.onLowMemory();
        }
        super.onLowMemory();
    }

    private void showEULADialog() {
        EULADialogFragment dialog = EULADialogFragment.newInstance(getString(R.string.car_stolen_tracking_EULA_title), null);
        dialog.setButtons(getString(R.string.btn_confirm), getString(R.string.btn_cancel), new EULADialogFragment.Callbacks() {
            @Override
            public void onPositiveButtonClicked() {
                startRemoteCommand("LimitedVehSpeedON", "");
                mSetSpeedLimit = true;
                mShowEULA = false;
                updateSpeedLimitViews();
                mSpeedLimitContainer.setClickable(false);
            }

            @Override
            public void onNegativeButtonClicked() {
            }
        });
        dialog.show(getFragmentManager(), "EULADialog");
    }

    private void showFinishDialog() {
        showConfirmDialog(getString(R.string.car_stolen_tracking_finish_title), getString(R.string.car_stolen_tracking_finish_message), new AlertDialogFragment.Callbacks() {
            @Override
            public void onPositiveButtonClicked() {
                startRemoteCommand("LimitedVehSpeedOFF", "");
                getActivity().finish();
            }

            @Override
            public void onNegativeButtonClicked() {
            }

            @Override
            public void onBackKeyPressed() {
            }
        });
    }

    private void startRemoteCommand(String command, String commandValue) {
        Intent intent = new Intent(getActivity(), CommunicationAgentService.class);
        intent.putExtra(CommunicationAgentService.CA_EXTRA_REMOTE_CONTROL, command);
        intent.putExtra(CommunicationAgentService.CA_EXTRA_COMMAND_VALUE, commandValue);
        getActivity().startService(intent);
    }
}
