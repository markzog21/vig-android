package com.luxgen.remote.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;

import com.luxgen.remote.R;

public class AlertDialogFragment extends DialogFragment {

    public interface Callbacks {
        void onPositiveButtonClicked();

        void onNegativeButtonClicked();

        void onBackKeyPressed();
    }

    public interface OnDismissListener {
        void execute();
    }

    private Activity mActivity;
    private Callbacks callbacks;
    private OnDismissListener mOnDismissListener;
    private View mCustomContentView;
    private String positiveMsg, negativeMsg;
    private boolean finishOnClose = false;

    public static AlertDialogFragment newInstance(String title, String message) {
        return newInstance(title, message, null, null, true);
    }

    public static AlertDialogFragment newInstance(String title, String message, String checkBoxText, String prefKey) {
        return newInstance(title, message, checkBoxText, prefKey, true);
    }

    private static AlertDialogFragment newInstance(String title, String message, String checkBoxText, String prefKey, boolean canceledOnTouchOutside) {
        AlertDialogFragment f = new AlertDialogFragment();

        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        if (!TextUtils.isEmpty(checkBoxText)) {
            args.putString("checkBoxText", checkBoxText);
        }
        if (!TextUtils.isEmpty(prefKey)) {
            args.putString("prefKey", prefKey);
        }
        args.putBoolean("canceledOnTouchOutside", canceledOnTouchOutside);

        f.setArguments(args);
        return f;
    }

    private final DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                if (callbacks != null) {
                    callbacks.onBackKeyPressed();
                    return true;
                }
            }
            return false;
        }
    };

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();

        String title = args.getString("title");
        String message = args.getString("message");
        String checkBoxText = args.getString("checkBoxText");
        String prefKey = args.getString("prefKey");
        boolean canceledOnTouchOutside = args.getBoolean("canceledOnTouchOutside");

        if (savedInstanceState != null) {
            positiveMsg = savedInstanceState.getString("positiveMsg", null);
            negativeMsg = savedInstanceState.getString("negativeMsg", null);
        }

        CustomAlertDialog alertDialog = new CustomAlertDialog(getContext(), R.style.CustomDialog);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        if(mCustomContentView != null) {
            alertDialog.setCustomContentView(mCustomContentView);
        }
        alertDialog.setCheckBoxData(checkBoxText, prefKey);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(canceledOnTouchOutside);

        if (positiveMsg != null) {
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
                    positiveMsg,
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (callbacks != null) {
                                callbacks.onPositiveButtonClicked();
                            }

                            dismiss();

                            if (finishOnClose) {
                                mActivity.finish();
                            }
                        }
                    });
        }

        if (negativeMsg != null) {
            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                    negativeMsg,
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (callbacks != null) {
                                callbacks.onNegativeButtonClicked();
                            }

                            dismiss();

                            if (finishOnClose && mOnDismissListener == null) {
                                mActivity.finish();
                            }
                        }
                    });
        }

        alertDialog.setOnKeyListener(keyListener);

        return alertDialog;
    }

    public void setButtons(String positiveMsg, String negativeMsg, Callbacks callbacks) {
        this.positiveMsg = positiveMsg;
        this.negativeMsg = negativeMsg;
        this.callbacks = callbacks;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("positiveMsg", positiveMsg);
        outState.putString("negativeMsg", negativeMsg);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        if (mOnDismissListener != null) {
            mOnDismissListener.execute();
        }
        super.onDestroyView();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    public void setFinishOnClose(boolean enable) {
        finishOnClose = enable;
    }

    public void setOnDismissListener(OnDismissListener listener) {
        mOnDismissListener = listener;
    }

    public void setCustomContentView(View view) {
        mCustomContentView = view;
    }
}
