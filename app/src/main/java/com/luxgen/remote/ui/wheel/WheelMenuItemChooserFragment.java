package com.luxgen.remote.ui.wheel;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.utils.Utils;
import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.WheelPage;
import com.luxgen.remote.model.WheelTileItem;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.util.Prefs;

import java.util.ArrayList;

public class WheelMenuItemChooserFragment extends CustomBaseFragment {

    private ArrayList<WheelTileItem> mWheelTileItems;

    public static WheelMenuItemChooserFragment newInstance(ArrayList<WheelPage> pages, boolean fullScreen) {
        Bundle args = new Bundle();
        args.putParcelableArrayList("pages", pages);
        args.putBoolean("fullScreen", fullScreen);

        WheelMenuItemChooserFragment fragment = new WheelMenuItemChooserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        Utils.init(getContext());
    }

    private ArrayList<WheelTileItem> getAddedWheelTileItems(boolean isDMS, boolean isVig, ArrayList<WheelPage> pages) {
        ArrayList<WheelTileItem> addedWheelTileItems = new ArrayList<>();

        if (pages == null) {
            return addedWheelTileItems;
        }

        for (WheelPage page : pages) {
            if (page == null) {
                continue;
            }

            WheelTileItem[] addedItems = page.getItems();
            for (WheelTileItem addedItem : addedItems) {
                if (addedItem == null) {
                    continue;
                }

                if (WheelTileItem.shouldHaveThisItem(isDMS, isVig, addedItem.getActionId())) {
                    addedWheelTileItems.add(addedItem);
                }
            }
        }

        return addedWheelTileItems;
    }

    private ArrayList<WheelTileItem> getFilteredWheelTileItems(boolean isDms, boolean isVig, ArrayList<WheelTileItem> inputItems, ArrayList<WheelTileItem> filterItems) {
        ArrayList<WheelTileItem> filteredWheelTileItems = new ArrayList<>();

        if (inputItems == null) {
            return filteredWheelTileItems;
        }

        if (filterItems == null) {
            return inputItems;
        }

        for (WheelTileItem input : inputItems) {
            boolean shouldAdd = true;
            for (WheelTileItem filter : filterItems) {
                if (filter.getActionId() == input.getActionId()) {
                    shouldAdd = false;
                    break;
                }
            }

            if (shouldAdd && WheelTileItem.shouldHaveThisItem(isDms, isVig, input.getActionId())) {
                filteredWheelTileItems.add(input);
            }
        }
        return filteredWheelTileItems;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        View v = inflater.inflate(R.layout.fragment_wheel_menu_item_chooser, container, false);

        boolean fullScreen = args.getBoolean("fullScreen", false);
        if (fullScreen) {
            Toolbar toolbar = v.findViewById(R.id.toolbar);
            toolbar.setVisibility(View.VISIBLE);
            toolbar.setTitle(R.string.wheel_menu_setup_shortcut_title);
            if (((AppCompatActivity) getActivity()).getSupportActionBar() == null) {
                ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
            }
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popFragment();
                }
            });
        }

        TextView mAddedItemTitle = v.findViewById(R.id.added_title);
        LinearLayout mAddedItemContainer = v.findViewById(R.id.added_item_container);
        LinearLayout mBlueItemContainer = v.findViewById(R.id.blue_item_container);
        LinearLayout mYellowItemContainer = v.findViewById(R.id.yellow_item_container);
        LinearLayout mGreenItemContainer = v.findViewById(R.id.green_item_container);
        LinearLayout mBlackItemContainer = v.findViewById(R.id.black_item_container);
        LinearLayout mUnknownItemContainer = v.findViewById(R.id.unknown_item_container);

        mAddedItemTitle.setVisibility(View.GONE);
        mAddedItemContainer.setVisibility(View.GONE);

        ArrayList<WheelPage> wheelPages = args.getParcelableArrayList("pages");

        UserInfo userInfo = Prefs.getUserInfo(getContext());
        CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
        boolean isVig = (carKeyData != null) && carKeyData.isVig();
        boolean isDMS = (userInfo != null) && userInfo.isDMS();

        ArrayList<WheelTileItem> mAddedWheelTileItems = getAddedWheelTileItems(isDMS, isVig, wheelPages);
        mWheelTileItems = getFilteredWheelTileItems(isDMS, isVig, WheelMenuItems.getAll(), mAddedWheelTileItems);

        if (mAddedWheelTileItems.size() > 0) {
            mAddedItemContainer.setVisibility(View.VISIBLE);
            mAddedItemTitle.setVisibility(View.VISIBLE);
        }

        setupContainer(mAddedItemContainer, mAddedWheelTileItems, -1);
        setupContainer(mBlueItemContainer, mWheelTileItems, WheelTileItem.COLOR_BLUE);
        setupContainer(mYellowItemContainer, mWheelTileItems, WheelTileItem.COLOR_YELLOW);
        setupContainer(mGreenItemContainer, mWheelTileItems, WheelTileItem.COLOR_GREEN);
        setupContainer(mBlackItemContainer, mWheelTileItems, WheelTileItem.COLOR_BLACK);
        setupContainer(mUnknownItemContainer, mWheelTileItems, WheelTileItem.COLOR_UNKNOWN);

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.clear();
    }

    private void setupContainer(LinearLayout container, ArrayList<WheelTileItem> items, int color) {
        for (WheelTileItem item : items) {
            if (color == -1 || item.getColor() == color) {
                LinearLayout itemView = setupWheelTileItemView(item, color != -1);
                container.addView(itemView, container.getChildCount());

                ImageView dividerImageView = getDividerView();
                container.addView(dividerImageView, container.getChildCount());
            }
        }

        container.setVisibility((container.getChildCount() == 0) ? View.GONE : View.VISIBLE);
    }

    private ImageView getDividerView() {
        LinearLayout.LayoutParams dividerImageViewLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ImageView dividerImageView = new ImageView(getContext());
        dividerImageView.setImageResource(R.drawable.wheel_setup_horizontal_divider);
        dividerImageView.setLayoutParams(dividerImageViewLayoutParams);

        return dividerImageView;
    }

    private LinearLayout setupWheelTileItemView(WheelTileItem item, boolean clickable) {
        LinearLayout itemContainer = new LinearLayout(getContext());
        LinearLayout.LayoutParams itemContainerLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        itemContainer.setLayoutParams(itemContainerLayoutParams);
        itemContainer.setOrientation(LinearLayout.HORIZONTAL);

        LinearLayout.LayoutParams iconImageViewLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        iconImageViewLayoutParams.setMargins(0, (int) Utils.convertDpToPixel(16), 0, (int) Utils.convertDpToPixel(16));
        iconImageViewLayoutParams.setMarginStart((int) Utils.convertDpToPixel(24));
        iconImageViewLayoutParams.setMarginEnd((int) Utils.convertDpToPixel(16));

        ImageView iconImageView = new ImageView(getContext());
        iconImageView.setImageResource(item.getSetupIcon());
        iconImageView.setLayoutParams(iconImageViewLayoutParams);
        itemContainer.addView(iconImageView);

        LinearLayout.LayoutParams titleTextViewLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        titleTextViewLayoutParams.gravity = Gravity.CENTER_VERTICAL;

        TextView titleTextView = new TextView(getContext());
        titleTextView.setText(item.getTitle());
        titleTextView.setTextSize(14);
        titleTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.textColorDark));
        titleTextView.setLayoutParams(titleTextViewLayoutParams);
        itemContainer.addView(titleTextView);

        if (clickable) {
            itemContainer.setTag(item.getActionId());
            itemContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    returnWheelMenuItem((int) v.getTag());
                }
            });
        }

        return itemContainer;
    }

    private WheelTileItem getWheelTileItem(int actionId) {
        for (WheelTileItem item : mWheelTileItems) {
            if (item.getActionId() == actionId) {
                return item;
            }
        }

        return null;
    }

    private void returnWheelMenuItem(int action) {
        WheelTileItem item = getWheelTileItem(action);
        if (item == null) {
            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, null);
        } else {
            Intent result = new Intent();
            result.putExtra("item", item);
            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, result);
        }
        popFragment();
    }
}
