package com.luxgen.remote.ui.oks;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.luxgen.remote.R;
import com.luxgen.remote.custom.LocationRequestFragment;
import com.luxgen.remote.util.DialUtils;
import com.luxgen.remote.util.GeocodeAddressIntentService;

public class CarStolenWholeFragment extends LocationRequestFragment implements OnMapReadyCallback {

    private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";
    private AddressResultReceiver mResultReceiver;
    private TextView mCurrentAddressTextView;
    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private Location mCurrentLocation = null;

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (resultData == null) {
                return;
            }

            mCurrentAddressTextView.setText(resultData.getString(GeocodeAddressIntentService.RESULT_DATA_KEY));
        }
    }

    @Override
    protected void onLocationUpdated(Location location) {
        mCurrentLocation = location;
        markAndMoveToLocation(location);
        translateAddress(location);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mResultReceiver = new AddressResultReceiver(new Handler());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_car_stolen_whole, container, false);

        mMapView = v.findViewById(R.id.img_current_map);
        mCurrentAddressTextView = v.findViewById(R.id.txt_current_address);

        Button policeButton = v.findViewById(R.id.btn_police);
        policeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialPolice();
            }
        });

        Button policeStationButton = v.findViewById(R.id.btn_police_station);
        policeStationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show(new CarStolenNearbyPoliceFragment());
            }
        });

        Button prevButton = v.findViewById(R.id.btn_prev);
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popFragment();
            }
        });

        Button buttonFinish = v.findViewById(R.id.btn_finish);
        buttonFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        return v;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }

        if (mMapView != null) {
            mMapView.onSaveInstanceState(mapViewBundle);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }

        if (mMapView != null) {
            mMapView.onCreate(mapViewBundle);
            mMapView.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        if (mCurrentLocation != null) {
            markAndMoveToLocation(mCurrentLocation);
        }
    }

    @Override
    public void onStart() {
        if (mMapView != null) {
            mMapView.onStart();
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (mMapView != null) {
            mMapView.onStop();
        }
        super.onStop();
    }

    @Override
    public void onLowMemory() {
        if (mMapView != null) {
            mMapView.onLowMemory();
        }
        super.onLowMemory();
    }

    private void markAndMoveToLocation(Location location) {
        if (location != null && mGoogleMap != null) {
            LatLng carLocation = new LatLng(location.getLatitude(), location.getLongitude());
            mGoogleMap.addMarker(new MarkerOptions().position(carLocation));
            mGoogleMap.getUiSettings().setAllGesturesEnabled(false);
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(carLocation, 16));
        }
    }

    private void dialPolice() {
        startActivity(DialUtils.callPolice());
    }

    private void translateAddress(Location loc) {
        Intent intent = new Intent(getActivity(), GeocodeAddressIntentService.class);
        intent.putExtra(GeocodeAddressIntentService.RECEIVER, mResultReceiver);
        intent.putExtra(GeocodeAddressIntentService.LOCATION_DATA_EXTRA, loc);
        getActivity().startService(intent);
    }
}
