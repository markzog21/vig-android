package com.luxgen.remote.ui.maintenance;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.maintenance.RepairHistory;
import com.luxgen.remote.network.model.maintenance.RepairItem;
import com.luxgen.remote.util.Prefs;

import java.util.ArrayList;

public class MaintenanceHistoryDetailFragment extends CustomBaseFragment {

    private RepairHistory mItem;

    public static MaintenanceHistoryDetailFragment newInstance(RepairHistory item) {
        MaintenanceHistoryDetailFragment f = new MaintenanceHistoryDetailFragment();

        Bundle args = new Bundle();
        args.putParcelable("item", item);
        f.setArguments(args);

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_maintenance_history_detail, container, false);

        Bundle args = getArguments();
        if (args != null) {
            mItem = args.getParcelable("item");
        }

        TextView dateTextView = v.findViewById(R.id.date);
        dateTextView.setText(mItem.getDate());

        TextView plantTextView = v.findViewById(R.id.plant);
        plantTextView.setText(mItem.getDept());

        CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
        TextView plateTextView = v.findViewById(R.id.plate);
        plateTextView.setText(carKeyData.getCarNo());

        TextView mileageTextView = v.findViewById(R.id.mileage);
        mileageTextView.setText(getString(R.string.maintenance_reservation_mileage_unit, mItem.getOdo()));

        TextView amountTextView = v.findViewById(R.id.amount);
        amountTextView.setText(getString(R.string.maintenance_reservation_amount_unit, mItem.getAmount()));

//        TextView notesTextView = v.findViewById(R.id.notes);
//        if (mItem.getNotes() == null || mItem.getNotes().isEmpty()) {
//            notesTextView.setText(R.string.maintenance_reservation_notes_none);
//        } else {
//            notesTextView.setText(mItem.getNotes());
//        }

        TextView detailTextView = v.findViewById(R.id.detail);
        if (mItem.getItems() != null) {
            detailTextView.setText(getRepairItems(mItem.getItems()));
        } else {
            detailTextView.setText(getString(R.string.maintenance_history_item_detail_none));
        }

        return v;
    }

    private String getRepairItems(ArrayList<RepairItem> repairItems) {
        StringBuilder sb = new StringBuilder();
        for (RepairItem repairItem : repairItems) {
            sb.append(repairItem.getType());
            sb.append(" - ");
            sb.append(repairItem.getItem());
            sb.append("\n");
        }
        return sb.toString();
    }
}
