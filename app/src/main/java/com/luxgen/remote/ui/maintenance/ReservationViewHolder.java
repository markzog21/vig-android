package com.luxgen.remote.ui.maintenance;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.network.model.maintenance.CarRepairPlanData;

public class ReservationViewHolder extends ViewHolder {

    private TextView mDateTextView;
    private TextView mPlantTextView;
    private TextView mPlateTextView;
    private TextView mMileageTextView;
    private TextView mMaintenanceTextView;
    private TextView mNotesTextView;

    private ReservationListener mListener;

    public interface ReservationListener {
        void onMapClicked(int position);

        void onPhoneClicked(int position);

        void onCancelClicked(int position);
    }

    public ReservationViewHolder(View itemView, ReservationListener listener) {
        super(itemView);
        this.mListener = listener;

        mDateTextView = itemView.findViewById(R.id.date);
        mPlantTextView = itemView.findViewById(R.id.plant);

        View v = itemView.findViewById(R.id.extra);
        v.setVisibility(View.VISIBLE);
        ImageButton mMapImageButton = itemView.findViewById(R.id.map);
        ImageButton mPhoneImageButton = itemView.findViewById(R.id.phone);
        mPlateTextView = itemView.findViewById(R.id.plate);
        mMileageTextView = itemView.findViewById(R.id.mileage);
        mMaintenanceTextView = itemView.findViewById(R.id.amount);
        mNotesTextView = itemView.findViewById(R.id.notes);
        View maintenanceItemContainer = itemView.findViewById(R.id.maintenance_item_container);
        maintenanceItemContainer.setVisibility(View.GONE);
        View notesContainer = itemView.findViewById(R.id.notes_container);
        notesContainer.setVisibility(View.GONE);

        Button mCancelButton = itemView.findViewById(R.id.btn_cancel);

        mMapImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onMapClicked(getLayoutPosition());
            }
        });

        mPhoneImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onPhoneClicked(getLayoutPosition());
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onCancelClicked(getLayoutPosition());
            }
        });
    }

    public void setItem(CarRepairPlanData item) {
        mDateTextView.setText(item.getBookingDate());
        mPlantTextView.setText(item.getDept());
        mPlateTextView.setText(item.getCarNo());
        if (TextUtils.isEmpty(item.getOdo())) {
            mMileageTextView.setText(itemView.getContext().getString(R.string.maintenance_reservation_mileage_unit, "0"));
        } else {
            mMileageTextView.setText(itemView.getContext().getString(R.string.maintenance_reservation_mileage_unit, item.getOdo()));
        }
        if (item.isNormalCheck() && item.isRepairCheck()) {
            mMaintenanceTextView.setText(R.string.maintenance_reservation_maintenance_n_repair);
        } else if (item.isNormalCheck()) {
            mMaintenanceTextView.setText(R.string.maintenance_reservation_maintenance);
        } else if (item.isRepairCheck()) {
            mMaintenanceTextView.setText(R.string.maintenance_reservation_repair);
        }
        if (item.getNote() == null || item.getNote().isEmpty()) {
            mNotesTextView.setText(R.string.maintenance_reservation_notes_none);
        } else {
            mNotesTextView.setText(item.getNote());
        }
    }
}
