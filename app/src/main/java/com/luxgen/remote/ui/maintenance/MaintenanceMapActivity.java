package com.luxgen.remote.ui.maintenance;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomFragmentsAppCompatActivity;

public class MaintenanceMapActivity extends CustomFragmentsAppCompatActivity {

    public static Intent newIntent(Context context, String address, String name) {
        Intent intent = new Intent(context, MaintenanceMapActivity.class);
        intent.putExtra("address", address);
        intent.putExtra("name", name);
        return intent;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_maintenance_map;
    }

    @Override
    protected void setupTitle(String tag) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String address = null;
        String name = null;
        Intent intent = getIntent();
        if (intent != null) {
            address = intent.getStringExtra("address");
            name = intent.getStringExtra("name");
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.maintenance_map_title, name));
        } else {
            setTitle(getString(R.string.maintenance_map_title, name));
        }

        show(MaintenanceMapFragment.newInstance(address, name));
        bindCaService();
    }

    @Override
    protected void onDestroy() {
        unbindCaService();
        super.onDestroy();
    }
}
