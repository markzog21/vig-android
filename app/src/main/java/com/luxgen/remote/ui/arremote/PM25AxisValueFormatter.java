package com.luxgen.remote.ui.arremote;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.Locale;

public class PM25AxisValueFormatter implements IAxisValueFormatter {
    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        int val = 3 - (int) (Math.ceil(value / 2));
        if (val == 0) {
            return "分鐘前";
        }
        return String.format(Locale.TAIWAN, "%d", val);
    }
}
