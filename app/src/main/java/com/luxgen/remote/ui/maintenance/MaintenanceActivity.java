package com.luxgen.remote.ui.maintenance;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.MenuItem;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomFragmentsAppCompatActivity;

public class MaintenanceActivity extends CustomFragmentsAppCompatActivity {

    private ViewPager mPager;
    private MaintenancePagerAdapter mPagerAdapter;
    private int mStartPage = 0;
    private String mCurrentCarNo = null;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_maintenance;
    }

    @Override
    protected void setupTitle(String tag) {

    }

    public static Intent newIntent(Context context, int start) {
        Intent intent = new Intent(context, MaintenanceActivity.class);
        intent.putExtra("start", start);
        return intent;
    }

    public static Intent newIntent(Context context, int start, String currentCarNo) {
        Intent intent = new Intent(context, MaintenanceActivity.class);
        intent.putExtra("start", start);
        intent.putExtra("currentCarNo", currentCarNo);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (intent != null) {
            mStartPage = intent.getIntExtra("start", mStartPage);
            mCurrentCarNo = intent.getStringExtra("currentCarNo");
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.activity_maintenance_title);
        } else {
            setTitle(R.string.activity_maintenance_title);
        }

        setupTabs();
        bindCaService();
    }

    @Override
    protected void onDestroy() {
        unbindCaService();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (!BackStackFragment.handleBackPressed(getSupportFragmentManager())) {
            if (mPager.getCurrentItem() != 0) {
                mPager.setCurrentItem(0);
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void startFragmentInHost(Fragment fragment) {
        HostFragment hostFragment = (HostFragment) mPagerAdapter.getItem(mPager.getCurrentItem());
        hostFragment.replaceFragment(fragment, true);
    }

    public void finishAndGoReservations() {
        HostFragment hostFragment = (HostFragment) mPagerAdapter.getItem(mPager.getCurrentItem());
        hostFragment.popFragment();
        hostFragment.popFragment();
        mPager.setCurrentItem(1);
    }

    private void setupTabs() {
        if (TextUtils.isEmpty(mCurrentCarNo)) {
            mPagerAdapter = new MaintenancePagerAdapter(getSupportFragmentManager());
        } else {
            mPagerAdapter = new MaintenancePagerAdapter(getSupportFragmentManager(), mCurrentCarNo);
        }

        mPager = findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(mStartPage);

        TabLayout mTabLayout = findViewById(R.id.tab);
        mTabLayout.setupWithViewPager(mPager);
    }
}
