package com.luxgen.remote.ui.oks;

import android.Manifest;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.luxgen.remote.BuildConfig;
import com.luxgen.remote.R;
import com.luxgen.remote.custom.LocationRequestFragment;
import com.luxgen.remote.util.DialUtils;
import com.luxgen.remote.util.GeocodeAddressIntentService;
import com.luxgen.remote.util.TimeUtils;

import java.io.File;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import static android.app.Activity.RESULT_OK;

public class CarStolenPartFragment extends LocationRequestFragment implements OnMapReadyCallback {

    private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";
    private static final int REQUEST_TAKE_PHOTO = 9527;
    private static final int REQUEST_CAMERA_PERMISSION = 9528;
    private File mPhotoFile;

    private AddressResultReceiver mResultReceiver;
    private TextView mCurrentAddressTextView;
    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private Location mCurrentLocation = null;

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (resultData == null) {
                return;
            }

            mCurrentAddressTextView.setText(resultData.getString(GeocodeAddressIntentService.RESULT_DATA_KEY));
        }
    }

    @Override
    protected void onLocationUpdated(Location location) {
        mCurrentLocation = location;
        markAndMoveToLocation(location);
        translateAddress(location);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mResultReceiver = new AddressResultReceiver(new Handler());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_car_stolen_part, container, false);

        mMapView = v.findViewById(R.id.img_current_map);
        mCurrentAddressTextView = v.findViewById(R.id.txt_current_address);

        Button policeButton = v.findViewById(R.id.btn_police);
        policeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialPolice();
            }
        });

        Button policeStationButton = v.findViewById(R.id.btn_police_station);
        policeStationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show(new CarStolenNearbyPoliceFragment());
            }
        });

        Button mCameraButton = v.findViewById(R.id.btn_camera);
        mCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCameraAndStoragePermission();
            }
        });

        Button prevButton = v.findViewById(R.id.btn_prev);
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popFragment();
            }
        });

        Button buttonFinish = v.findViewById(R.id.btn_finish);
        buttonFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        return v;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }

        if (mMapView != null) {
            mMapView.onSaveInstanceState(mapViewBundle);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }

        if (mMapView != null) {
            mMapView.onCreate(mapViewBundle);
            mMapView.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        if (mCurrentLocation != null) {
            markAndMoveToLocation(mCurrentLocation);
        }
    }

    @Override
    public void onStart() {
        if (mMapView != null) {
            mMapView.onStart();
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (mMapView != null) {
            mMapView.onStop();
        }
        super.onStop();
    }

    @Override
    public void onLowMemory() {
        if (mMapView != null) {
            mMapView.onLowMemory();
        }
        super.onLowMemory();
    }

    private void markAndMoveToLocation(Location location) {
        if (location != null && mGoogleMap != null) {
            LatLng carLocation = new LatLng(location.getLatitude(), location.getLongitude());
            mGoogleMap.addMarker(new MarkerOptions().position(carLocation));
            mGoogleMap.getUiSettings().setAllGesturesEnabled(false);
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(carLocation, 16));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            galleryAddPic();
        }
    }

    private void checkCameraAndStoragePermission() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(getContext(), perms)) {
            takePicture();
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.car_stolen_camera_rationale), REQUEST_CAMERA_PERMISSION, perms);
        }
    }

    private void dialPolice() {
        startActivity(DialUtils.callPolice());
    }

    private File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Luxgen");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = TimeUtils.getCurrentTimeString();
        return new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
    }

    @AfterPermissionGranted(REQUEST_CAMERA_PERMISSION)
    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            mPhotoFile = getOutputMediaFile();
            Uri photoUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", mPhotoFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

            startActivityForResult(intent, REQUEST_TAKE_PHOTO);
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri photoUri = Uri.fromFile(mPhotoFile);
        mediaScanIntent.setData(photoUri);

        getActivity().sendBroadcast(mediaScanIntent);
    }

    private void translateAddress(Location loc) {
        Intent intent = new Intent(getActivity(), GeocodeAddressIntentService.class);
        intent.putExtra(GeocodeAddressIntentService.RECEIVER, mResultReceiver);
        intent.putExtra(GeocodeAddressIntentService.LOCATION_DATA_EXTRA, loc);
        getActivity().startService(intent);
    }
}
