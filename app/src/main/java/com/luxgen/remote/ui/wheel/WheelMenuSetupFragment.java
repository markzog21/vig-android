package com.luxgen.remote.ui.wheel;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.WheelPage;
import com.luxgen.remote.model.WheelTileItem;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.util.Prefs;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;
import static com.luxgen.remote.ui.wheel.WheelMenuSetupActivity.FRAGMENT_TAG_WHEEL_MENU_ITEM_CHOOSER;
import static com.luxgen.remote.ui.wheel.WheelMenuSetupActivity.FRAGMENT_TAG_WHEEL_MENU_SETUP_CONTROL;

public class WheelMenuSetupFragment extends CustomBaseFragment {

    private final static int REQUEST_ADD_WHEEL_MENU_ITEM = 1;

    private LinearLayout mEditorsContainer;
    private int mPages = 0;
    private MenuItem mAddMenu, mEditMenu, mFinishMenu;

    private View mSetupControlView;
    private int mCurrentItemPosition;
    private int mCurrentItemPage;
    private ArrayList<WheelPage> mWheelPages = new ArrayList<>();

    private SharedPreferences mSharedPreferences;
    private boolean mIsDMS = false;
    private boolean mIsVig = false;
    private String mKeyUid;

    private WheelMenuPageEditorLayout.OnItemClickListener mOnItemClickListener = new WheelMenuPageEditorLayout.OnItemClickListener() {
        @Override
        public void onAddItemClick(ImageView v, int page, int position) {
            mCurrentItemPage = page;
            mCurrentItemPosition = position;

            startWheelMenuItemChooser();
        }

        @Override
        public void onDelItemClick(ImageView v, int page, int position) {
            mCurrentItemPage = page;
            mCurrentItemPosition = position;

            delItem();
        }

        @Override
        public void onDelPageClick(int pageNumber) {
            delPage(pageNumber);
        }
    };

    public static WheelMenuSetupFragment newInstance() {
        WheelMenuSetupFragment fragment = new WheelMenuSetupFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPreferences = getActivity().getSharedPreferences("wheelmenu", Context.MODE_PRIVATE);
        mKeyUid = Prefs.getKeyUid(getActivity());

        UserInfo userInfo = Prefs.getUserInfo(getContext());
        CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
        mIsVig = (carKeyData != null) && carKeyData.isVig();
        mIsDMS = (userInfo != null) && userInfo.isDMS();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_wheel_menu_setup, container, false);

        mEditorsContainer = v.findViewById(R.id.page_editors_container);
        mSetupControlView = v.findViewById(R.id.setup_control);
        mSetupControlView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startWheelMenuSetupControl();
            }
        });

        mWheelPages = loadWheelMenu();
        mPages = 0;
        for (WheelPage page : mWheelPages) {
            addPage(page);
        }
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.wheel_menu_setup, menu);
        mAddMenu = menu.findItem(R.id.action_add);
        mEditMenu = menu.findItem(R.id.action_edit);
        mFinishMenu = menu.findItem(R.id.action_finish);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                if (addPage(null)) {
                    saveWheelMenu(mWheelPages);
                    return true;
                }
                return false;

            case R.id.action_edit:
                goEditMode(true);
                return true;

            case R.id.action_finish:
                goEditMode(false);
                saveWheelMenu(mWheelPages);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void goEditMode(boolean enable) {
        for (int i = 0; i < mEditorsContainer.getChildCount(); i++) {
            WheelMenuPageEditorLayout editor = (WheelMenuPageEditorLayout) mEditorsContainer.getChildAt(i);
            editor.setInEditMode(enable);
        }
        mAddMenu.setVisible(!enable);
        mEditMenu.setVisible(!enable);
        mFinishMenu.setVisible(enable);
        mSetupControlView.setVisibility(enable ? View.GONE : View.VISIBLE);
    }

    private void delPageEditorUI(int page) {
        mEditorsContainer.removeViewAt(page);
        for (int i = 0; i < mEditorsContainer.getChildCount(); i++) {
            WheelMenuPageEditorLayout editor = (WheelMenuPageEditorLayout) mEditorsContainer.getChildAt(i);
            editor.setPageNumber(i);
        }

        if (mEditorsContainer.getChildCount() == 1) {
            WheelMenuPageEditorLayout editor = (WheelMenuPageEditorLayout) mEditorsContainer.getChildAt(0);
            editor.hidePageRemovalImageView();
        }
    }

    private void delPageData(int page) {
        mWheelPages.remove(page);
    }

    private boolean delPage(int page) {
        if (mPages == 1) {
            return false;
        }
        mPages--;

        delPageEditorUI(page);
        delPageData(page);

        return true;
    }

    private void addPageEditorUI(WheelPage page) {
        WheelMenuPageEditorLayout editor = new WheelMenuPageEditorLayout(getContext());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        editor.setLayoutParams(params);
        editor.setPageNumber(mPages - 1);
        editor.setOnItemClickListener(mOnItemClickListener);

        if (page != null) {
            WheelTileItem[] items = page.getItems();
            for (int i = 0; i < WheelMenuLayout.WHEEL_TILE_MAX; i++) {
                WheelTileItem item = items[i];
                if (item != null) {
                    if (WheelTileItem.shouldHaveThisItem(mIsDMS, mIsVig, item.getActionId())) {
                        editor.setItemImageResource(i, item.getSetupIcon());
                    }
                }
            }
        }
        mEditorsContainer.addView(editor, mEditorsContainer.getChildCount());
    }

    private void addPageData() {
        WheelPage wheelPage = new WheelPage(null);
        mWheelPages.add(wheelPage);
    }

    private boolean addPage(WheelPage page) {
        if (mPages == WheelMenuLayout.WHEEL_PAGES_MAX) {
            return false;
        }
        mPages++;

        addPageEditorUI(page);
        if (page == null) {
            addPageData();
        }

        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_ADD_WHEEL_MENU_ITEM) {
            WheelTileItem item = data.getParcelableExtra("item");
            addItem(item);
        }
    }

    private void addItem(WheelTileItem item) {
        if (item == null) {
            return;
        }

        WheelMenuPageEditorLayout editor = (WheelMenuPageEditorLayout) mEditorsContainer.getChildAt(mCurrentItemPage);
        editor.setItemImageResource(mCurrentItemPosition, item.getSetupIcon());

        WheelPage page = mWheelPages.get(mCurrentItemPage);
        if (page != null) {
            page.setItem(item, mCurrentItemPosition);
        }
        saveWheelMenu(mWheelPages);
    }

    private void delItem() {
        WheelMenuPageEditorLayout editor = (WheelMenuPageEditorLayout) mEditorsContainer.getChildAt(mCurrentItemPage);
        editor.setItemImageResource(mCurrentItemPosition, R.drawable.ic_smart_wheel_set_add_new_item);

        WheelPage page = mWheelPages.get(mCurrentItemPage);
        if (page != null) {
            page.setItem(null, mCurrentItemPosition);
        }
    }

    private void logWheelPage() {
        for (int i = 0; i < mWheelPages.size(); i++) {
            LogCat.w("page: " + i);
            WheelPage page = mWheelPages.get(i);
            if (page != null) {
                for (WheelTileItem item : page.getItems()) {
                    if (item == null) {
                        LogCat.w("item: null");
                    } else {
                        LogCat.w("item: " + item.toString());
                    }
                }
            }
        }
    }

    private void startWheelMenuSetupControl() {
        WheelMenuSetupControlFragment fragment = WheelMenuSetupControlFragment.newInstance();
        show(fragment, FRAGMENT_TAG_WHEEL_MENU_SETUP_CONTROL, true);
    }

    private void startWheelMenuItemChooser() {
        WheelMenuItemChooserFragment fragment = WheelMenuItemChooserFragment.newInstance(mWheelPages, false);
        fragment.setTargetFragment(WheelMenuSetupFragment.this, REQUEST_ADD_WHEEL_MENU_ITEM);
        show(fragment, FRAGMENT_TAG_WHEEL_MENU_ITEM_CHOOSER, true);
    }

    private ArrayList<WheelPage> loadWheelMenu() {
        int numPages = mSharedPreferences.getInt(mKeyUid + "_num_pages", 1);

        ArrayList<WheelPage> pages = new ArrayList<>();
        for (int i = 0; i < numPages; i++) {
            WheelPage page = new WheelPage(null);
            for (int j = 0; j < WheelMenuLayout.WHEEL_TILE_MAX; j++) {
                int actionId = mSharedPreferences.getInt(mKeyUid + "_page_" + i + "_pos_" + j, -1);
                if (actionId != -1) {
                    WheelTileItem item = WheelMenuItems.get(actionId);
                    page.setItem(item, j);
                }
            }
            pages.add(page);
        }

        return pages;
    }

    private void saveWheelMenu(ArrayList<WheelPage> pages) {
        SharedPreferences.Editor mEdit = mSharedPreferences.edit();

        int numPages = pages.size();
        mEdit.putInt(mKeyUid + "_num_pages", numPages);

        for (int i = 0; i < numPages; i++) {
            WheelPage page = pages.get(i);
            if (page == null) {
                continue;
            }

            WheelTileItem[] items = page.getItems();
            for (int j = 0; j < WheelMenuLayout.WHEEL_TILE_MAX; j++) {
                WheelTileItem item = items[j];
                mEdit.putInt(mKeyUid + "_page_" + i + "_pos_" + j, item == null ? -1 : item.getActionId());
            }
        }

        mEdit.apply();
    }
}
