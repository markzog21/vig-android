package com.luxgen.remote.ui.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.util.Prefs;

public class SettingsPinCodeFragment extends CustomBaseFragment {

    private String mPinCode;
    private EditText mCurrentPinCodeEditText, mNewPinCodeEditText, mConfirmPinCodeEditText;

    public static SettingsPinCodeFragment newInstance() {
        SettingsPinCodeFragment f = new SettingsPinCodeFragment();

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(R.string.settings_pincode_title);

        View v = inflater.inflate(R.layout.fragment_settings_pincode, container, false);

        if (!hasPinCode()) {
            v.findViewById(R.id.pincode_current_container).setVisibility(View.GONE);
        }

        mCurrentPinCodeEditText = v.findViewById(R.id.pincode_current);
        mCurrentPinCodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mCurrentPinCodeEditText.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mNewPinCodeEditText = v.findViewById(R.id.pincode_new);
        mConfirmPinCodeEditText = v.findViewById(R.id.pincode_confirm);
        mConfirmPinCodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                if (!str.equals(mNewPinCodeEditText.getText().toString().trim())) {
                    mConfirmPinCodeEditText.setError(getString(R.string.settings_pincode_error_not_match));
                } else {
                    mConfirmPinCodeEditText.setError(null);
                }
            }
        });

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.settings_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                String oldPinCode = mCurrentPinCodeEditText.getText().toString().trim();
                String newPinCode = mNewPinCodeEditText.getText().toString().trim();
                String confirmPinCode = mConfirmPinCodeEditText.getText().toString().trim();

                if (mPinCode != null && !oldPinCode.equals(mPinCode)) {
                    mCurrentPinCodeEditText.requestFocus();
                    mCurrentPinCodeEditText.selectAll();
                    mCurrentPinCodeEditText.setError(getString(R.string.settings_pincode_error_not_match));
                    break;
                }

                if (newPinCode.isEmpty()) {
                    mNewPinCodeEditText.requestFocus();
                    mNewPinCodeEditText.setError(getString(R.string.settings_pincode_error_empty));
                    break;
                }

                if (newPinCode.length() < 4) {
                    mNewPinCodeEditText.requestFocus();
                    mNewPinCodeEditText.setError(getString(R.string.settings_pincode_error_short));
                    break;
                }

                if (confirmPinCode.isEmpty()) {
                    mConfirmPinCodeEditText.requestFocus();
                    mConfirmPinCodeEditText.setError(getString(R.string.settings_pincode_error_empty));
                    break;
                }

                if (!newPinCode.equals(confirmPinCode)) {
                    mConfirmPinCodeEditText.requestFocus();
                    mConfirmPinCodeEditText.selectAll();
                    mConfirmPinCodeEditText.setError(getString(R.string.settings_pincode_error_not_match));
                    break;
                }

                setPinCode(confirmPinCode);
                Toast.makeText(getContext(), R.string.settings_userinfo_modification_success, Toast.LENGTH_SHORT).show();
                popFragment();
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean hasPinCode() {
        mPinCode = getPinCode();
        return mPinCode != null && !mPinCode.isEmpty();
    }

    private String getPinCode() {
        return Prefs.getPinCode(getContext());
    }

    private void setPinCode(String pinCode) {
        Prefs.setPinCode(getContext(), pinCode);
    }
}
