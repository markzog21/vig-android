package com.luxgen.remote.ui.maintenance;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.ui.dialog.ProgressDialogFragment;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.model.option.CarOptions;
import com.luxgen.remote.model.option.RepairOptions;
import com.luxgen.remote.network.AssistantWebApi;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.model.car.CarKeyData;
import com.luxgen.remote.network.model.maintenance.GetCarRepairProgressRequestData;
import com.luxgen.remote.network.model.maintenance.GetCarRepairProgressResponseData;
import com.luxgen.remote.util.Prefs;

public class MaintenanceProgressFragment extends CustomBaseFragment {

    private View mNoDataContainer;
    private View mBaseContainer;
    private View mSubProgress4Container;
    private ImageView mProgress1ImageView;
    private ImageView mProgress2ImageView;
    private ImageView mProgress3ImageView;
    private ImageView mProgress4ImageView;
    private Button mQuestionnaireButton;
    private ProgressDialogFragment mProgressDialogFragment;
    private boolean mIsVisibleToUser = false;
    private String mRoNo;

    public static MaintenanceProgressFragment newInstance(String carNo) {
        MaintenanceProgressFragment f = new MaintenanceProgressFragment();

        if (carNo != null) {
            Bundle args = new Bundle();
            args.putString("carNo", carNo);
            f.setArguments(args);
        }

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maintenance_progress, container, false);

        mNoDataContainer = view.findViewById(R.id.no_data_container);
        mBaseContainer = view.findViewById(R.id.base_container);
        mProgress1ImageView = view.findViewById(R.id.maintenance_progress_1);
        mProgress2ImageView = view.findViewById(R.id.maintenance_progress_2);
        mProgress3ImageView = view.findViewById(R.id.maintenance_progress_3);
        mProgress4ImageView = view.findViewById(R.id.maintenance_progress_4);
        mSubProgress4Container = view.findViewById(R.id.maintenance_progress_4_sub_container);
        mQuestionnaireButton = view.findViewById(R.id.btn_questionnaire);
        mQuestionnaireButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = Prefs.getRepairCarQuestionnaireURL(getContext(), mRoNo);
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url)));
            }
        });

        TextView mNoDataTitleTextView = view.findViewById(R.id.no_data_title);
        if (isAuthorized() && isCurrentCar()) {
            mNoDataTitleTextView.setText(R.string.maintenance_progress_no_data);

            if (mIsVisibleToUser) {
                fetchRepairProgress();
            }
        } else {
            mNoDataTitleTextView.setText(R.string.maintenance_progress_no_authority);
        }


        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        mIsVisibleToUser = isVisibleToUser;
        if (isVisibleToUser && isAdded() && isAuthorized() && isCurrentCar()) {
            fetchRepairProgress();
        }
    }

    private void dismissDialog() {
        if (isAdded() && mProgressDialogFragment != null) {
            mProgressDialogFragment.dismiss();
        }
    }

    private OnApiListener<GetCarRepairProgressResponseData> mOnRepairProgressApiListener = new OnApiListener<GetCarRepairProgressResponseData>() {
        @Override
        public void onApiTaskSuccessful(GetCarRepairProgressResponseData responseData) {
            dismissDialog();
            if (responseData.isSuccessful()) {
                if (responseData.hasProgress()) {
                    mNoDataContainer.setVisibility(View.GONE);
                    mBaseContainer.setVisibility(View.VISIBLE);
                    updateProgress(responseData);
                } else {
                    mNoDataContainer.setVisibility(View.VISIBLE);
                    mBaseContainer.setVisibility(View.GONE);
                }
            } else {
                mNoDataContainer.setVisibility(View.VISIBLE);
                mBaseContainer.setVisibility(View.GONE);
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            dismissDialog();
            mNoDataContainer.setVisibility(View.VISIBLE);
            mBaseContainer.setVisibility(View.GONE);
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mProgressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.maintenance_fetching_data));
            mProgressDialogFragment.show(getFragmentManager(), "getRepairProgressDialog");
        }

        @Override
        public void onApiProgress(long value) {

        }

        @Override
        public void onAuthFailure() {
            dismissDialog();
            showMultiLoginErrorDialog();
        }
    };

    private void fetchRepairProgress() {
        if (getContext() == null) {
            return;
        }

        UserInfo userInfo = Prefs.getUserInfo(getContext());
        if (userInfo == null) {
            return;
        }

        CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
        if (carKeyData == null) {
            return;
        }

        String carNo = getUserInputCarNo();
        if (TextUtils.isEmpty(carNo)) {
            carNo = carKeyData.getCarNo();
        }

        GetCarRepairProgressRequestData data = new GetCarRepairProgressRequestData(
                userInfo.getKeyUid(), userInfo.getToken(), carNo);

        AssistantWebApi assistantWebApi = AssistantWebApi.getInstance(getContext());
        assistantWebApi.getCarRepairProgress(data, mOnRepairProgressApiListener);
    }

    private void updateProgress(GetCarRepairProgressResponseData responseData) {
        final String status = responseData.getStatus();
        if (status.equals(RepairOptions.PREPARING)) {
            mProgress1ImageView.setImageResource(getOngoingResource());
        } else if (status.equals(RepairOptions.MAINTENANCE)) {
            mProgress1ImageView.setImageResource(getDoneResource());
            mProgress2ImageView.setImageResource(getOngoingResource());
        } else if (status.equals(RepairOptions.CLEANING)) {
            mProgress1ImageView.setImageResource(getDoneResource());
            mProgress2ImageView.setImageResource(getDoneResource());
            mProgress3ImageView.setImageResource(getOngoingResource());
        } else if (status.equals(RepairOptions.CHECKOUT)) {
            mProgress1ImageView.setImageResource(getDoneResource());
            mProgress2ImageView.setImageResource(getDoneResource());
            mProgress3ImageView.setImageResource(getDoneResource());
            mProgress4ImageView.setImageResource(getOngoingResource());
            mSubProgress4Container.setVisibility(View.VISIBLE);
        }
        mRoNo = responseData.getRoNo();
    }

    private int getDoneResource() {
        return R.drawable.ic_fix_progress_finished;
    }

    private int getOngoingResource() {
        return R.drawable.ic_fix_progress_ing;
    }

    private boolean isAuthorized() {
        CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
        if (carKeyData != null) {
            if (carKeyData.isOwner() || carKeyData.isDriver()) {
                return true;
            }

            return carKeyData.getKeyType().equals(CarOptions.KEY_M) ||
                    carKeyData.getKeyType().equals(CarOptions.KEY_M2) ||
                    carKeyData.getKeyType().equals(CarOptions.KEY_S1) ||
                    carKeyData.getKeyType().equals(CarOptions.KEY_S2);
        }

        return false;
    }

    private String getUserInputCarNo() {
        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        return args.getString("carNo");
    }

    private boolean isCurrentCar() {
        String carNo = getUserInputCarNo();
        if (TextUtils.isEmpty(carNo)) {
            return true;
        }
        carNo = carNo.replace("-", "");

        CarKeyData carKeyData = Prefs.getCarKeyData(getContext());
        if (carKeyData != null) {
            String carNoInDB = carKeyData.getCarNo();
            if (TextUtils.isEmpty(carNoInDB)) {
                return false;
            }

            return carNoInDB.toUpperCase().replace("-", "").equals(carNo);
        }

        return false;
    }
}
