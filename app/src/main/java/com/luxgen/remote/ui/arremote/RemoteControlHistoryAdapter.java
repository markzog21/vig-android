package com.luxgen.remote.ui.arremote;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luxgen.remote.R;
import com.luxgen.remote.network.model.car.CarControlLogData;

import java.util.ArrayList;
import java.util.List;

public class RemoteControlHistoryAdapter extends RecyclerView.Adapter {
    private List<CarControlLogData> items;
    private LayoutInflater inflater;

    public RemoteControlHistoryAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
        items = new ArrayList<>();
    }

    public void updateItems(final List<CarControlLogData> newItems) {
        final List<CarControlLogData> oldItems = new ArrayList<>(this.items);
        this.items.clear();
        if (newItems != null) {
            this.items.addAll(newItems);
        }

        DiffUtil.calculateDiff(new DiffUtil.Callback() {
            @Override
            public int getOldListSize() {
                return oldItems.size();
            }

            @Override
            public int getNewListSize() {
                return items.size();
            }

            @Override
            public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                return oldItems.get(oldItemPosition).equals(newItems.get(newItemPosition));
            }

            @Override
            public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                return oldItems.get(oldItemPosition).equals(newItems.get(newItemPosition));
            }
        }).dispatchUpdatesTo(this);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_remote_control_history, parent, false);
        return new RemoteControlHistoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RemoteControlHistoryViewHolder viewHolder = (RemoteControlHistoryViewHolder) holder;
        viewHolder.setItem(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
