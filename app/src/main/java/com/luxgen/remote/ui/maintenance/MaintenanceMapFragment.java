package com.luxgen.remote.ui.maintenance;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomSupportMapFragment;
import com.luxgen.remote.util.GeocodeLocationIntentService;

public class MaintenanceMapFragment extends CustomSupportMapFragment implements OnMapReadyCallback {

    private static final String TAG = "LUXGEN";

    private GoogleMap mMap;
    private String mAddress;
    private String mDeptName;
    private LocationResultReceiver mLocationResultReceiver;

    class LocationResultReceiver extends ResultReceiver {
        LocationResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (resultData == null || !resultData.containsKey(GeocodeLocationIntentService.RESULT_DATA_KEY)) {
                return;
            }

            LatLng latLng = resultData.getParcelable(GeocodeLocationIntentService.RESULT_DATA_KEY);
            if (latLng != null) {
                markStation(latLng.latitude, latLng.longitude, mDeptName);
            }
        }
    }

    public static MaintenanceMapFragment newInstance(String address, String name) {
        MaintenanceMapFragment fragment = new MaintenanceMapFragment();

        Bundle args = new Bundle();
        args.putString("address", address);
        args.putString("name", name);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        final Bundle args = getArguments() == null ? Bundle.EMPTY : getArguments();
        mAddress = args.getString("address");
        mDeptName = args.getString("name");

        mLocationResultReceiver = new LocationResultReceiver(new Handler());
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mMap == null) {
            getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        translateLocation(mAddress);
    }

    private void zoomToLocation(double lat, double lng, boolean animation) {
        LatLng userLocation = new LatLng(lat, lng);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(userLocation, 16);
        if (mMap != null) {
            if (animation) {
                mMap.animateCamera(cameraUpdate, 1500, null);
            } else {
                mMap.moveCamera(cameraUpdate);
            }
        }
    }

    private void markStation(double lat, double lng, String name) {
        LatLng loc = new LatLng(lat, lng);
        if (mMap != null) {
            mMap.addMarker(new MarkerOptions()
                    .position(loc)
                    .title(name)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ctn_police_station)));
        }
        zoomToLocation(lat, lng, true);
    }

    private void translateLocation(String address) {
        Intent intent = new Intent(getActivity(), GeocodeLocationIntentService.class);
        intent.putExtra(GeocodeLocationIntentService.RECEIVER, mLocationResultReceiver);
        intent.putExtra(GeocodeLocationIntentService.LOCATION_DATA_EXTRA, address);
        getActivity().startService(intent);
    }
}
