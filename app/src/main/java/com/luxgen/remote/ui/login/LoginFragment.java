package com.luxgen.remote.ui.login;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.luxgen.remote.BuildConfig;
import com.luxgen.remote.R;
import com.luxgen.remote.activity.MainActivity;
import com.luxgen.remote.ui.settings.SettingsActivity;
import com.luxgen.remote.ca.CommunicationAgentService;
import com.luxgen.remote.ui.dialog.AlertDialogFragment;
import com.luxgen.remote.custom.CustomBaseFragment;
import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.ui.dialog.ProgressDialogFragment;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.OnApiListener;
import com.luxgen.remote.network.UserWebApi;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.user.GetIkeyEmployeeDataRequestData;
import com.luxgen.remote.network.model.user.GetIkeyUidRequestData;
import com.luxgen.remote.network.model.user.GetIkeyUidResponseData;
import com.luxgen.remote.network.model.user.LoginRequestData;
import com.luxgen.remote.network.model.user.LoginResponseData;
import com.luxgen.remote.network.model.user.SetAccountRequestData;
import com.luxgen.remote.util.PlayStoreChecker;
import com.luxgen.remote.util.Prefs;

import org.json.JSONObject;

import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static android.app.Activity.RESULT_OK;
import static com.luxgen.remote.ui.login.LoginActivity.FRAGMENT_TAG_LOGIN_REGISTER_MAIN;
import static com.luxgen.remote.ui.login.LoginActivity.FRAGMENT_TAG_LOGIN_REGISTER_PASSWORD;
import static com.luxgen.remote.ca.CommunicationAgentService.CA_EXTRA_LOGIN_SUCCESS_RETURN_CODE;
import static com.luxgen.remote.ca.CommunicationAgentService.CA_EXTRA_MAX_LOGIN_RETRY_RETURN_CODE;

public class LoginFragment extends CustomBaseFragment implements EasyPermissions.PermissionCallbacks {

    private final static String TAG = LoginFragment.class.getSimpleName();

    public final static int PERMISSIONS_REQUEST_READ_PHONE_STATE = 9527;
    private final static int REQUEST_CHANGE_PASSWORD = 9529;
    private final static int REQUEST_GOOGLE_SIGN_IN = 9528;
    private final static int LOGIN_TYPE_EMAIL = 0;
    private final static int LOGIN_TYPE_FACEBOOK = 1;
    private final static int LOGIN_TYPE_GOOGLE = 2;

    private OnLoginListener mLoginListener;

    private EditText mEmailEditText;
    private EditText mPasswordEditText;
    private LoginButton mFacebookSignInButton;
    private View mFacebookSingInButtonMaskView;
    private AppCompatCheckBox mPrivacyPolicyCheckBox;
    private ProgressDialogFragment mLoginProgressFragment;

    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager mFacebookCallbackManager;

    private int mLoginType = LOGIN_TYPE_EMAIL;
    private String mEmail;
    private String mIkeyUid;
    private String mIkeyToken;
    private String mUID;
    private String mSocialId;
    private String mSocialAlias;

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    private OnApiListener<LoginResponseData> mLoginRequestListener = new OnApiListener<LoginResponseData>() {
        @Override
        public void onApiTaskSuccessful(LoginResponseData responseData) {
            if (responseData.isSuccessful()) {
                if (TextUtils.isEmpty(responseData.getKeyToken())) {
                    mLoginProgressFragment.dismiss();
                    showServerErrorDialog(getString(R.string.login_error_i3_state));
                } else {
                    saveUserData(responseData);
                    mIkeyUid = responseData.getKeyUid();
                    mIkeyToken = responseData.getKeyToken();
                    mUID = responseData.getUid();
                    if (responseData.isRedirect()) {
                        mLoginProgressFragment.dismiss();
                        showChangePasswordScreen();
                    } else {
                        doIkeyLogin(mIkeyUid);
                    }
                }
            } else {
                mLoginProgressFragment.dismiss();
                showServerErrorDialog(responseData.getErrorMessage());
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            mLoginProgressFragment.dismiss();
            showServerErrorDialog(failMessage);
        }

        @Override
        public void onPreApiTask() {
            mLoginProgressFragment = ProgressDialogFragment.newInstance(getString(R.string.login_authenticating));
            mLoginProgressFragment.show(getFragmentManager(), "loginProgressDialog");
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            // TODO: check if need this
            mLoginProgressFragment.dismiss();
            showMultiLoginErrorDialog();
        }
    };

    private OnApiListener<GetIkeyUidResponseData> mGetIkeyDataRequestListener = new OnApiListener<GetIkeyUidResponseData>() {

        private void doNext() {
            if (isAdded()) {
                mLoginProgressFragment.dismiss();
            }
            showMainScreen();
        }

        @Override
        public void onApiTaskSuccessful(GetIkeyUidResponseData responseData) {
            if (responseData.isSuccessful()) {
                updateUserData(responseData.getEmail(), responseData.getName());
            }
            doNext();
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            doNext();
        }

        @Override
        public void onPreApiTask() {
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            // TODO: check if need this
            showMultiLoginErrorDialog();
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mLoginListener = (OnLoginListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initialGoogleSignInClient();
        initialFacebookCallbackManager();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        mLoginType = LOGIN_TYPE_EMAIL;
        mEmailEditText = v.findViewById(R.id.edit_email);
        mPasswordEditText = v.findViewById(R.id.edit_password);
        mPrivacyPolicyCheckBox = v.findViewById(R.id.privacy_policy_check_box);
        mPrivacyPolicyCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mFacebookSingInButtonMaskView.setVisibility(View.GONE);
                } else {
                    mFacebookSingInButtonMaskView.setVisibility(View.VISIBLE);
                }
            }
        });

        Button loginButton = v.findViewById(R.id.btn_login);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPrivacyPolicyCheckBox.isChecked()) {
                    mLoginType = LOGIN_TYPE_EMAIL;
                    login();
                } else {
                    showConfirmDialog(getString(R.string.dialog_title_remind), getString(R.string.dialog_message_privacy_policy));
                }
            }
        });
        loginButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (!BuildConfig.IS_OFFICIAL) {
                    mEmailEditText.setText("lingling120@hotmail.com");
                    mPasswordEditText.setText("QWER1234");
                }
                return true;
            }
        });

        Button mGoogleSignInButton = v.findViewById(R.id.google_sign_in);
        mGoogleSignInButton.setAllCaps(false);
        mGoogleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPrivacyPolicyCheckBox.isChecked()) {
                    startActivityForResult(mGoogleSignInClient.getSignInIntent(), REQUEST_GOOGLE_SIGN_IN);
                } else {
                    showConfirmDialog(getString(R.string.dialog_title_remind), getString(R.string.dialog_message_privacy_policy));
                }
            }
        });

        mFacebookSignInButton = v.findViewById(R.id.fb_sign_in);
        mFacebookSignInButton.setReadPermissions("email");
        mFacebookSignInButton.setFragment(this);
        mFacebookSignInButton.registerCallback(mFacebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookSignInResult(loginResult);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        mFacebookSingInButtonMaskView = v.findViewById(R.id.fb_sign_in_mask);
        mFacebookSingInButtonMaskView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showConfirmDialog(getString(R.string.dialog_title_remind), getString(R.string.dialog_message_privacy_policy));
            }
        });

        AppCompatTextView privacyPolicyTextView = v.findViewById(R.id.privacy_policy_text_view);
        privacyPolicyTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmDialog(getString(R.string.dialog_title_privacy_polocy), getString(R.string.login_register_privacy_policy_content));
            }
        });

        TextView forgetPasswordTextView = v.findViewById(R.id.txt_forget_password);
        forgetPasswordTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgetPassword();
            }
        });

        TextView registerTextView = v.findViewById(R.id.txt_register);
        registerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

        mPrivacyPolicyCheckBox.setChecked(true);

//        mEmailEditText.setText("w189666@gmail.com");
//        mPasswordEditText.setText("QWER1234");
//
//        mEmailEditText.setText("lxmgpm001@gmail.com");
//        mPasswordEditText.setText("Gpmgpm0001");

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLoginProgressFragment = ProgressDialogFragment.newInstance(null);
        mLoginProgressFragment.show(getFragmentManager(), "checkUpdateDialog");
        PlayStoreChecker playStoreChecker = new PlayStoreChecker(getActivity(), new PlayStoreChecker.CheckResultListener() {
            @Override
            public void noNeedToUpdate() {
                mLoginProgressFragment.dismiss();
            }
        });
        playStoreChecker.start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder builder = new AppSettingsDialog.Builder(this);
            builder.setTitle(R.string.login_permission_rationale_title)
                    .setRationale(R.string.login_permission_rationale_message)
                    .build().show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_GOOGLE_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleGoogleSignInResult(task);
        } else if (requestCode == REQUEST_CHANGE_PASSWORD && resultCode == RESULT_OK) {
            continueLogin();
        }
    }

    private void handleGoogleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            mEmail = account.getEmail();
            mSocialId = account.getId();
            mSocialAlias = account.getDisplayName();
            mLoginType = LOGIN_TYPE_GOOGLE;
            login();
        } catch (ApiException e) {
            LogCat.w("signInResult:failed code=" + e.getStatusCode());
        }
    }

    private void initialGoogleSignInClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getContext(), gso);
    }

    private void handleFacebookSignInResult(LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        mEmail = object.optString("email", "");
                        mSocialId = object.optString("id", "");
                        mSocialAlias = object.optString("name", "");
                        mLoginType = LOGIN_TYPE_FACEBOOK;

                        if (TextUtils.isEmpty(mEmail)) {
                            LoginManager.getInstance().logOut();
                            showConfirmDialog(getString(R.string.login_error_fb_no_email_title),
                                    getString(R.string.login_error_fb_no_email_message));
                        } else {
                            login();
                        }
                    }
                });
//        mSocialId = loginResult.getAccessToken().getUserId();
        Bundle parameters = new Bundle();
        parameters.putString("fields", "email,id,name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void initialFacebookCallbackManager() {
        mFacebookCallbackManager = CallbackManager.Factory.create();
    }

    private boolean validate() {
        if (mLoginType == LOGIN_TYPE_EMAIL) {
            String email = mEmailEditText.getText().toString().trim();
            String password = mPasswordEditText.getText().toString().trim();

            if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                mEmailEditText.setError(getString(R.string.login_error_email));
                return false;
            }

            if (TextUtils.isEmpty(password)) {
                mPasswordEditText.setError(getString(R.string.login_error_password));
                return false;
            }
        }

        return true;
    }

    private LoginRequestData buildLoginRequest(String email, String password, String deviceId, long deviceCreateTime) {
        LoginRequestData data = new LoginRequestData(email);
        data.setPassword(password);
        data.setPhoneId(deviceId);
        data.setOsVersion(Build.VERSION.RELEASE);
        data.setPhoneBrand(Build.BRAND);
        data.setDeviceCreateTime(deviceCreateTime);

        return data;
    }

    @AfterPermissionGranted(PERMISSIONS_REQUEST_READ_PHONE_STATE)
    private void doLogin() {
        String email, password = null;
        if (mLoginType == LOGIN_TYPE_FACEBOOK) {
            email = mEmail;
            if (getContext() != null) {
                password = Prefs.getFacebookPassword(getContext());
            }
            mEmailEditText.setText(email);
            mPasswordEditText.setText(password);
            if (TextUtils.isEmpty(password)) {
                LoginManager.getInstance().logOut();
                mLoginListener.onSocialRegisterInLoginFragment();
                checkRepeat(email);
                return;
            }
        } else if (mLoginType == LOGIN_TYPE_GOOGLE) {
            email = mEmail;
            if (getContext() != null) {
                password = Prefs.getGooglePassword(getContext());
            }
            mEmailEditText.setText(email);
            mPasswordEditText.setText(password);
            if (TextUtils.isEmpty(password)) {
                mGoogleSignInClient.revokeAccess();
                mLoginListener.onSocialRegisterInLoginFragment();
                checkRepeat(email);
                return;
            }
        } else { // LOGIN_TYPE_EMAIL
            email = mEmailEditText.getText().toString().trim();
            password = mPasswordEditText.getText().toString().trim();
        }
        String deviceId = Prefs.getDeviceId(getContext());
        long deviceCreateTime = Prefs.getDeviceIdCreateTime(getContext());

        LoginRequestData data = buildLoginRequest(email, password, deviceId, deviceCreateTime);

        UserWebApi userWebApi = UserWebApi.getInstance(getActivity());
        userWebApi.login(data, mLoginRequestListener);
    }

    private GetIkeyEmployeeDataRequestData buildGetIkeyEmployeeData(String iKeyUid, String iKeyToken) {
        GetIkeyEmployeeDataRequestData data = new GetIkeyEmployeeDataRequestData(iKeyUid, iKeyToken);

        return data;
    }

    private GetIkeyUidRequestData buildGetIkeyData(String iKeyUid, String iKeyToken) {
        GetIkeyUidRequestData data = new GetIkeyUidRequestData(iKeyUid, iKeyToken);

        return data;
    }

    private void doGetIkeyData(String iKeyUid, String iKeyToken) {
        GetIkeyUidRequestData data = buildGetIkeyData(iKeyUid, iKeyToken);

        UserWebApi userWebApi = UserWebApi.getInstance(getActivity());
        userWebApi.getIkeyUid(data, mGetIkeyDataRequestListener);
    }

    private void login() {
        if (validate()) {
            String[] perms = {Manifest.permission.WRITE_CONTACTS, Manifest.permission.READ_CONTACTS};
            if (EasyPermissions.hasPermissions(getContext(), perms)) {
                doLogin();
            } else {
                EasyPermissions.requestPermissions(this,
                        getString(R.string.login_permission_rationale),
                        PERMISSIONS_REQUEST_READ_PHONE_STATE, perms);
            }
        }
    }

    private void forgetPassword() {
        startActivity(LoginForgetPasswordActivity.newIntent(getActivity()));
    }

    private void register() {
        ((LoginActivity) getActivity()).navigate(this,
                LoginRegisterFragment.newInstance(), FRAGMENT_TAG_LOGIN_REGISTER_MAIN, true);
    }

    private void showRegisterPasswordScreen() {
        String email = mEmailEditText.getText().toString().trim();
        String deviceId = Prefs.getDeviceId(getContext());

        ((LoginActivity) getActivity()).navigate(this,
                LoginRegisterPasswordFragment.newInstance(email, deviceId, mLoginType, mSocialId, mSocialAlias),
                FRAGMENT_TAG_LOGIN_REGISTER_PASSWORD, true);
    }

    private ProgressDialogFragment mCheckRepeatProgressDialogFragment;

    private OnApiListener<ResponseData> mCheckRepeatListener = new OnApiListener<ResponseData>() {
        @Override
        public void onApiTaskSuccessful(ResponseData responseData) {
            mCheckRepeatProgressDialogFragment.dismiss();
            if (responseData.isSuccessful()) {
                showRegisterPasswordScreen();
            }
        }

        @Override
        public void onApiTaskFailure(String failMessage) {
            mCheckRepeatProgressDialogFragment.dismiss();
            mPasswordEditText.requestFocus();
        }

        @Override
        public void onPreApiTask() {
            mCheckRepeatProgressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.login_register_checking_repeat));
            mCheckRepeatProgressDialogFragment.show(getFragmentManager(), "checkRepeatProgressDialog");
        }

        @Override
        public void onApiProgress(long value) {
        }

        @Override
        public void onAuthFailure() {
            // TODO: check if need this
            mCheckRepeatProgressDialogFragment.dismiss();
            showMultiLoginErrorDialog();
        }
    };

    private SetAccountRequestData buildCheckRepeatData(String email) {
        SetAccountRequestData data = new SetAccountRequestData();
        data.setAccount(email);

        return data;
    }

    private void checkRepeat(String email) {
        SetAccountRequestData data = buildCheckRepeatData(email);

        UserWebApi userWebApi = UserWebApi.getInstance(getActivity());
        userWebApi.checkRepeat(data, mCheckRepeatListener);
    }

    private void saveUserData(LoginResponseData userData) {
        String UID = userData.getUid();
        String iKeyToken = userData.getKeyToken();
        String iKeyUID = userData.getKeyUid();
        String phone = userData.getPhone();
        String email = mEmail;
        if (mLoginType == LOGIN_TYPE_EMAIL) {
            email = mEmailEditText.getText().toString().trim();
        }
        String password = mPasswordEditText.getText().toString().trim();

        UserInfo userInfo = new UserInfo(email, password);
        userInfo.setPhone(phone);
        userInfo.setUid(UID);
        userInfo.setToken(iKeyToken);
        userInfo.setKeyUid(iKeyUID);

        if (getContext() != null) {
            Prefs.setUserInfo(getContext(), userInfo);
        }
    }

    private void updateUserData(String email, String name) {
        if (getContext() == null) {
            return;
        }

        UserInfo userInfo = Prefs.getUserInfo(getContext());
        if (userInfo != null) {
            userInfo.setEmail(email);
            Prefs.setUserInfo(getContext(), userInfo);
        }
    }

    private void doIkeyLogin(String ikeyUid) {
        Intent ikeyLoginIntent = new Intent(getContext(), CommunicationAgentService.class);
        ikeyLoginIntent.putExtra(CommunicationAgentService.CA_EXTRA_IKEY_LOGIN, ikeyUid);
        getActivity().startService(ikeyLoginIntent);
    }

    private void continueLogin() {
        if (getContext() == null) {
            return;
        }
        UserInfo userInfo = Prefs.getUserInfo(getContext());
        if (userInfo != null) {
            mIkeyUid = userInfo.getKeyUid();
            mIkeyToken = userInfo.getToken();
        }
        mLoginProgressFragment = ProgressDialogFragment.newInstance(getString(R.string.login_authenticating));
        mLoginProgressFragment.show(getFragmentManager(), "loginProgressDialog");
        doIkeyLogin(mIkeyUid);
    }

    private void showChangePasswordScreen() {
        Intent intent = SettingsActivity.newIntent(getContext(), SettingsActivity.SETTINGS_CHANGE_PASSWORD);
        startActivityForResult(intent, REQUEST_CHANGE_PASSWORD);
    }

    private void showMainScreen() {
        startActivity(new Intent(getActivity(), MainActivity.class));
        // unregisterCaStatusBroadcastReceiver();
        getActivity().finish();
    }

    public interface OnLoginListener {
        void onSocialRegisterInLoginFragment();
    }

    private final AlertDialogFragment.Callbacks mFinishActivityCallbacks = new AlertDialogFragment.Callbacks() {
        @Override
        public void onPositiveButtonClicked() {
            if (getActivity() != null) {
                getActivity().finish();
            }
        }

        @Override
        public void onNegativeButtonClicked() {

        }

        @Override
        public void onBackKeyPressed() {

        }
    };

    @Override
    protected void addActionToReceiveBroadcastFromCaService(IntentFilter intentFilter) {
        intentFilter.addAction(CommunicationAgentService.CA_STATUS_IKEY_LOGIN);
    }

    @Override
    protected void onReceiveBroadcastFromCaService(Context context, Intent intent) {
        if (CommunicationAgentService.CA_STATUS_IKEY_LOGIN.equals(intent.getAction())) {
            if (intent.hasExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE)) {
                int returnCode = intent.getIntExtra(CommunicationAgentService.CA_STATUS_EXTRA_RETURN_CODE, 0);
                LogCat.d("ikey login ack:" + returnCode);
                if (returnCode == CA_EXTRA_LOGIN_SUCCESS_RETURN_CODE) {
                    doGetIkeyData(mIkeyUid, mIkeyToken);
                } else {
                    if (returnCode == CA_EXTRA_MAX_LOGIN_RETRY_RETURN_CODE) {
                        LogCat.d("ikey login error: 失敗重試已達上限");
                        showMainScreen();
                    } else {
                        LogCat.d("ikey login error: " + returnCode);
                        showServerErrorDialog(getString(R.string.fake_999), mFinishActivityCallbacks);
                    }
                }
            } else if (intent.hasExtra(CommunicationAgentService.CA_STATUS_EXTRA_COMMAND)) {
                String message = intent.getStringExtra(CommunicationAgentService.CA_STATUS_EXTRA_COMMAND);
                LogCat.d("ikey login failed:" + message);
                showServerErrorDialog(getString(R.string.fake_999), mFinishActivityCallbacks);
            }
            return;
        }

        super.onReceiveBroadcastFromCaService(context, intent);
    }

}
