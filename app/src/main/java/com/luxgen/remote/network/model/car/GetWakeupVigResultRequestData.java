package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.VigRequestData;

import java.util.UUID;

public class GetWakeupVigResultRequestData extends VigRequestData {

    @SerializedName("wakeUpID")
    private String mWakeUpId;

    /**
     * 參數皆必填 <br />
     * Weak Up ID 在 {@link WakeupVigRequestData} 初始化時建立
     *
     * @param iKeyUid
     * @param iKeyToken
     * @param carId
     */
    public GetWakeupVigResultRequestData(String iKeyUid, String iKeyToken, String carId) {
        super(iKeyUid, iKeyToken, carId);
        mWakeUpId = UUID.randomUUID().toString();
    }

    /**
     * 在初始化時就會建立當次的 UUID，若有需要請記得取出並暫存
     */
    public String getWakeUpId() {
        return mWakeUpId;
    }
}
