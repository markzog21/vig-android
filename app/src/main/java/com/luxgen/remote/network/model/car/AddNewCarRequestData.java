package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class AddNewCarRequestData extends RequestData {
    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName(KEY_CAR_NO)
    private String mCarNo;
    @SerializedName(KEY_ACCOUNT_ID)
    private String mAcctId;

    public AddNewCarRequestData(String keyUid, String keyToken,
                                String carNo, String acctId) {
        mKeyUid = keyUid;
        mKeyToken = keyToken;
        mCarNo = carNo;
        mAcctId = encrypt(acctId);
    }
}
