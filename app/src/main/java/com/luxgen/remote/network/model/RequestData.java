package com.luxgen.remote.network.model;

import com.luxgen.remote.util.AESCipher;

public class RequestData {
    protected static final String KEY_ACCOUNT = "ikeyAccount";
    protected static final String KEY_PASSWORD = "password";
    protected static final String KEY_ACCOUNT_ID = "accountID";
    protected static final String KEY_REGO_NO = "regoNO";
    protected static final String KEY_CAR_NO = "carNO";
    protected static final String KEY_CAR_ID = "carID";
    protected static final String KEY_VIGCAR_ID = "VIGCarID";
    protected static final String KEY_IKEY_UID = "ikeyUID";
    protected static final String KEY_IKEY_TOKEN = "ikeyToken";
    protected static final String KEY_DEALER_CODE = "dealerCode";
    protected static final String KEY_DEPT_CODE = "deptCode";
    protected static final String KEY_WS_CAR_NO = "carNo";

    protected String encrypt(String value) {
        return AESCipher.safeEncryptAES(value);
    }

    protected String decrypt(String value) {
        return AESCipher.safeDecryptAES(value);
    }
}
