package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class CarRepairPlanData extends ResponseData {
    @SerializedName("bookingNO")
    private String mBookingNo;
    @SerializedName("bookingDate")
    private String mBookingDate;
    @SerializedName("regoNO")
    private String mCarNo;
    @SerializedName("dealerCode")
    private String mDealerCode;
    @SerializedName("deptCode")
    private String mDeptCode;
    @SerializedName("dept")
    private String mDept;
    @SerializedName("deptContactaddress")
    private String mDeptAddress;
    @SerializedName("deptPhoneNO1")
    private String mDeptPhoneNo1;
    @SerializedName("deptPhoneNO2")
    private String mDeptPhoneNo2;
    @SerializedName("odo")
    private String mOdo;
    @SerializedName("normalCheck")
    private String mNormalCheck;
    @SerializedName("repairCheck")
    private String mRepairCheck;
    @SerializedName("notes")
    private String mNote;
    @SerializedName("address")
    private String mAddress;

    /**
     * 預約號碼
     * */
    public String getBookingNo() {
        return mBookingNo;
    }

    /**
     * 預約日期與時間
     * */
    public String getBookingDate() {
        return mBookingDate;
    }

    /**
     * 車牌號碼
     * */
    public String getCarNo() {
        return mCarNo;
    }

    /**
     * 經銷商代碼
     * */
    public String getDealerCode() {
        return mDealerCode;
    }

    /**
     * 保養廠代碼
     */
    public String getDeptCode() {
        return mDeptCode;
    }

    /**
     * 保養廠名稱
     */
    public String getDept() {
        return mDept;
    }

    /**
     * 保養廠完整地址
     */
    public String getDeptAddress() {
        return mDeptAddress;
    }

    /**
     * 保養廠Phone1
     */
    public String getDeptPhone1() {
        return mDeptPhoneNo1;
    }

    /**
     * 保養廠Phone2
     */
    public String getDeptPhone2() {
        return mDeptPhoneNo2;
    }

    /**
     * 里程
     */
    public String getOdo() {
        return mOdo;
    }

    /**
     * 是否有定保
     *
     * @return boolean 當回傳值不為空，且為 "Y" 才為真
     */
    public boolean isNormalCheck() {
        return mNormalCheck != null && mNormalCheck.toUpperCase().equals("Y");
    }

    /**
     * 是否有維修
     *
     * @return boolean 當回傳值不為空，且為 "Y" 才為真
     */
    public boolean isRepairCheck() {
        return mRepairCheck != null && mRepairCheck.toUpperCase().equals("Y");
    }

    /**
     * 備註
     */
    public String getNote() {
        return mNote;
    }

    /**
     * 完整地址
     */
    public String getAddress() {
        return mAddress;
    }
}
