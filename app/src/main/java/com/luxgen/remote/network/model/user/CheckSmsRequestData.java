package com.luxgen.remote.network.model.user;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class CheckSmsRequestData extends RequestData {
    @SerializedName(KEY_ACCOUNT)
    private String mAccount;
    @SerializedName(KEY_ACCOUNT_ID)
    private String mAccountId;
    @SerializedName("SMSMessage")
    private String mSms;

    public CheckSmsRequestData(String account) {
        mAccount = account;
        mAccountId = "";
        mSms = "";
    }

    public void setSms(String sms) {
        mSms = sms;
    }

    public void setId(String accountId) {
        if (!TextUtils.isEmpty(accountId)) {
            mAccountId = encrypt(accountId);
        }
    }
}
