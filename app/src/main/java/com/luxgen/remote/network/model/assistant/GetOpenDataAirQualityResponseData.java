package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class GetOpenDataAirQualityResponseData extends ResponseData {
    @SerializedName("PM25")
    private int mPM25;

    public int getPM25() {
        return mPM25;
    }
}
