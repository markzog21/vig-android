package com.luxgen.remote.network.model.ikey;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class IkeyFuncToServerResponseData extends ResponseData {

    @SerializedName("appSecret")
    private String mAppSecret;
    @SerializedName("loginSetting")
    private IkeyLoginSettingData mLoginSetting;
    @SerializedName("keyOperationProcedure")
    private KeyOperationProcedure mKeyOperationProcedure;
    @SerializedName("action")
    private ActionData mAction;
    @SerializedName("errorMessage")
    private String mErrorMessage;

    public boolean isTimeout() {
        return "Read timed out".equals(mErrorMessage);
    }

    public ActionData getAction() {
        return mAction;
    }

    public IkeyLoginSettingData getLoginSetting() {
        return mLoginSetting;
    }

    public KeyOperationProcedure getKeyOperationProcedure() {
        return mKeyOperationProcedure;
    }

    public String getActionId() {
        if (mAction == null) {
            return "";
        }
        return mAction.getId();
    }

    public String getAppSecret() {
        return mAppSecret;
    }
}
