package com.luxgen.remote.network.ApiInterface;

import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.maintenance.DelCarRepairPlanRequestData;
import com.luxgen.remote.network.model.maintenance.DelCarRepairPlanResponseData;
import com.luxgen.remote.network.model.maintenance.GetCarProgressInWorkshopRequestData;
import com.luxgen.remote.network.model.maintenance.GetCarProgressInWorkshopResponseData;
import com.luxgen.remote.network.model.maintenance.GetCarRepairPlanRequestData;
import com.luxgen.remote.network.model.maintenance.GetCarRepairPlanResponseData;
import com.luxgen.remote.network.model.maintenance.GetCarRepairProgressRequestData;
import com.luxgen.remote.network.model.maintenance.GetCarRepairProgressResponseData;
import com.luxgen.remote.network.model.maintenance.GetRepairHistoryRequestData;
import com.luxgen.remote.network.model.maintenance.GetRepairHistoryResponseData;
import com.luxgen.remote.network.model.maintenance.GetRepairRequestData;
import com.luxgen.remote.network.model.maintenance.GetRepairResponseData;
import com.luxgen.remote.network.model.maintenance.GetWorkShopCapacityRequestData;
import com.luxgen.remote.network.model.maintenance.GetWorkShopCapacityResponseData;
import com.luxgen.remote.network.model.maintenance.GetWorkShopRequestData;
import com.luxgen.remote.network.model.maintenance.GetWorkShopResponseData;
import com.luxgen.remote.network.model.maintenance.SetCarRepairPeriodRequestData;
import com.luxgen.remote.network.model.maintenance.SetCarRepairPeriodResponseData;
import com.luxgen.remote.network.model.maintenance.ValidateVehicleRequestData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface MaintenanceInterface {
    // API No.1	取得車輛保修基本里程資料 (不用 -)
    @POST("/LuxgenVIG/VIGP1/repairManagement/getRepairData")
    Call<GetRepairResponseData> getRepairData(@Body GetRepairRequestData data);

    // API No.2 取得保養廠資料
    @POST("/LuxgenVIG/VIGP1/repairManagement/getWorkShopData")
    Call<GetWorkShopResponseData> getWorkShopList(@Body GetWorkShopRequestData data);

    // API No.3 取得保養廠保修能量
    @POST("/LuxgenVIG/VIGP1/repairManagement/getWorkShopCapacity")
    Call<GetWorkShopCapacityResponseData> getWorkShopCapacity(@Body GetWorkShopCapacityRequestData data);

    // API No.4 預約保養廠
    @POST("/LuxgenVIG/VIGP1/repairManagement/setCarRepairPeriod")
    Call<SetCarRepairPeriodResponseData> setCarRepairPeriod(@Body SetCarRepairPeriodRequestData data);

    // API No.5 查詢您已預約保養 (要 - )
    @POST("/LuxgenVIG/VIGP1/repairManagement/getCarRepairPlan")
    Call<GetCarRepairPlanResponseData> getCarRepairPlanList(@Body GetCarRepairPlanRequestData data);

    // API No.6 刪除預約保養 (要 -)
    @POST("/LuxgenVIG/VIGP1/repairManagement/deleteCarRepairPlan")
    Call<DelCarRepairPlanResponseData> delCarRepairPlan(@Body DelCarRepairPlanRequestData data);

    // API No.7 查詢保修紀錄 (要 -)
    @POST("/LuxgenVIG/VIGP1/repairManagement/getRepairHistory")
    Call<GetRepairHistoryResponseData> getRepairHistory(@Body GetRepairHistoryRequestData data);

    // API No.54 查詢保修進度 (要 -)
    @POST("/LuxgenVIG/VIGP1/repairManagement/getCarRepairProgress")
    Call<GetCarRepairProgressResponseData> getRepairProgress(@Body GetCarRepairProgressRequestData data);

    @POST("/LuxgenVIG/VIGP1/repairManagement/validateVehicle")
    Call<ResponseData> validateVehicle(@Body ValidateVehicleRequestData data);

    @POST("/VIGP1/repairManagement/getCarProgressInWorkshop")
    Call<GetCarProgressInWorkshopResponseData> getProgressInWorkshop(@Body GetCarProgressInWorkshopRequestData data);
}
