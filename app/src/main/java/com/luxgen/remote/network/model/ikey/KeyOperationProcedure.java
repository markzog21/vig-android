package com.luxgen.remote.network.model.ikey;

import com.google.gson.annotations.SerializedName;

public class KeyOperationProcedure {

    @SerializedName("procedureId")
    String mProcedureId;
    @SerializedName("operationType")
    String mOperationType;
    @SerializedName("operationStatus")
    String mOperationStatus;
    @SerializedName("from")
    KeyData mFrom;
    @SerializedName("to")
    KeyData mTo;
    @SerializedName("createTimestamp")
    String mCreateTimestamp;

    public boolean isCompleted() {
        return "complete".equalsIgnoreCase(mOperationStatus);
    }

    public boolean isAddSubKey() {
        return "addSubKey".equalsIgnoreCase(mOperationType);
    }

    public boolean isDelSubKey() {
        return "deleteSubKey".equalsIgnoreCase(mOperationType);
    }

    public boolean isAddSnapKey() {
        return "allocateSnapKey".equalsIgnoreCase(mOperationType);
    }

    public boolean isTerminateSnapKey() {
        return "terminateSnapKey".equalsIgnoreCase(mOperationType);
    }

    public boolean isDeleteMasterKey() {
        return "deleteMasterKey".equalsIgnoreCase(mOperationType);
    }

    public String getToUserId() {
        if (mTo != null && mTo.mUserProfile != null) {
            return mTo.mUserProfile.getUserId();
        }

        return null;
    }

    public String getFromUserId() {
        if (mFrom != null && mFrom.mUserProfile != null) {
            return mFrom.mUserProfile.getUserId();
        }

        return null;
    }

    public String getProcedureId() {
        return mProcedureId;
    }

    private class KeyData {
        @SerializedName("userProfile")
        UserProfileData mUserProfile;
        @SerializedName("deviceProfile")
        DeviceProfileData mDeviceProfile;
        @SerializedName("carProfile")
        CarProfileData mCarProfile;
    }
}
