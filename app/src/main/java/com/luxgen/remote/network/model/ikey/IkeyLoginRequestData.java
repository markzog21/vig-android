package com.luxgen.remote.network.model.ikey;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class IkeyLoginRequestData extends RequestData {

    @SerializedName("appSecret")
    private String mAppSecret;
    @SerializedName("loginSetting")
    private Setting mLoginSetting;
    @SerializedName("action")
    private ActionData mAction;

    public IkeyLoginRequestData(
            UserProfileData userProfileData,
            DeviceProfileData deviceProfileData,
            ActionData actionData,
            String appSecret,
            String timeStamp
    ) {
        mLoginSetting = new Setting(userProfileData, deviceProfileData);
        mAction = actionData;
        //TODO: 不確定 create time stamp 是否一樣
        mLoginSetting.setTimeStamp(timeStamp);
        mAction.setTimeStamp(timeStamp);
        mAppSecret = appSecret;
    }

    private class Setting {
        @SerializedName("timestamp")
        private String timeStamp;
        @SerializedName("userProfile")
        private UserProfileData userProfile;
        @SerializedName("deviceProfile")
        private DeviceProfileData deviceProfile;

        public Setting(UserProfileData userProfileData, DeviceProfileData deviceProfileData) {
            this.userProfile = userProfileData;
            this.deviceProfile = deviceProfileData;
        }

        public void setTimeStamp(String timeStamp) {
            this.timeStamp = timeStamp;
            this.userProfile.setCreateTimeStamp(timeStamp);
        }
    }
}
