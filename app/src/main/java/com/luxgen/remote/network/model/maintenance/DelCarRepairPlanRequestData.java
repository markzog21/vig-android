package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;

public class DelCarRepairPlanRequestData extends CarRepairRequestData {

    @SerializedName("bookingNO")
    private String mBookingNo;
    @SerializedName("dealerCode")
    private String mDealerCode;
    @SerializedName("deptCode")
    private String mDeptCode;

    /**
     * @param iKeyUid
     * @param iKeyToken
     * @param carNo
     * @param bookingNo     預約單號
     * @param dealerCode    經銷商代碼
     * @param deptCode      保養廠代碼
     */
    public DelCarRepairPlanRequestData(String iKeyUid, String iKeyToken, String carNo,
                                       String bookingNo, String dealerCode, String deptCode) {
        super(iKeyUid, iKeyToken, carNo);
        mBookingNo = bookingNo;
        mDealerCode = dealerCode;
        mDeptCode = deptCode;
    }
}
