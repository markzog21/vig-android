package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

import java.util.ArrayList;

public class GetKeyChainListResponseData extends ResponseData {
    @SerializedName("masterKeyUID")
    private String mMasterKeyUid = "";
    @SerializedName("maxSubKeyNO")
    private int mMaxSubKeyNo = 0;
    @SerializedName("subKeyList")
    private ArrayList<SubKey> mSubKeyList;
    @SerializedName("snapKeyList")
    private ArrayList<SnapKey> mSnapKeyList;

    public String getMasterKeyUid() {
        return mMasterKeyUid;
    }

    public int getMaxSubKeyNo() {
        return mMaxSubKeyNo;
    }

    public ArrayList<SubKey> getSubKeyList() {
        if (mSubKeyList == null) {
            mSubKeyList = new ArrayList<>();
        }
        return mSubKeyList;
    }

    public ArrayList<SnapKey> getSnapKeyList() {
        if (mSnapKeyList == null) {
            mSnapKeyList = new ArrayList<>();
        }
        return mSnapKeyList;
    }
}
