package com.luxgen.remote.network.model.ikey;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class IkeyLoginResponseData extends ResponseData {
    @SerializedName("loginSetting")
    private IkeyLoginSettingData mLoginSetting;
    @SerializedName("appSecret")
    private String mAppSecret;
    @SerializedName("action")
    private ActionData mAction;

    /**
     * 登入用 token，若登入失敗時回傳空字串。
     */
    public String getToken() {
        if (mLoginSetting == null) {
            return "";
        }

        return mLoginSetting.getToken();
    }

    public ActionData getAction() {
        return mAction;
    }

    /**
     * 給 APP 自行辨識用的任意值，會在 Response 中帶回。
     */
    public String getActionId() {
        if (mAction == null) {
            return "";
        }
        return mAction.getId();
    }

    /**
     * 由 iKeyServer 生成要給 App 裡的 CommunicationAgent 的溝通資料，若為空字串則代表沒有資料需要傳送。
     */
    public String getAppSecret() {
        return mAppSecret;
    }

    public IkeyLoginSettingData getLoginSetting() {
        return mLoginSetting;
    }

    public boolean isLoginSuccess() {
        if (mAction == null || mLoginSetting == null || TextUtils.isEmpty(mAppSecret)) {
            return false;
        }

        if (TextUtils.isEmpty(mLoginSetting.getToken())) {
            return false;
        }

        return (mAction.getReturnCode() == 200);
    }
}
