package com.luxgen.remote.network.model.maintenance;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RepairHistory implements Parcelable {

    @SerializedName("receptionDate")
    private String mDate;
    @SerializedName("odo")
    private String mOdo;
    @SerializedName("amount")
    private int mAmount;
    @SerializedName("dept")
    private String mDept;
    @SerializedName("items")
    private ArrayList<RepairItem> mItems;

    private RepairHistory(Parcel in) {
        mDate = in.readString();
        mOdo = in.readString();
        mAmount = in.readInt();
        mDept = in.readString();
        mItems = in.createTypedArrayList(RepairItem.CREATOR);
    }

    public static final Creator<RepairHistory> CREATOR = new Creator<RepairHistory>() {
        @Override
        public RepairHistory createFromParcel(Parcel in) {
            return new RepairHistory(in);
        }

        @Override
        public RepairHistory[] newArray(int size) {
            return new RepairHistory[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mDate);
        dest.writeString(mOdo);
        dest.writeInt(mAmount);
        dest.writeString(mDept);
        dest.writeTypedList(mItems);
    }

    /**
     * 進廠日期
     */
    public String getDate() {
        return mDate;
    }

    /**
     * 里程
     */
    public String getOdo() {
        return mOdo;
    }

    /**
     * 金額
     */
    public int getAmount() {
        return mAmount;
    }

    /**
     * 保修廠名稱
     */
    public String getDept() {
        return mDept;
    }

    /**
     * 保養項目
     */
    public ArrayList<RepairItem> getItems() {
        return mItems;
    }
}
