package com.luxgen.remote.network.model.assistant;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.util.AESCipher;

@Entity
public class UseLogData {
    @Ignore
    private final static String TAG = UseLogData.class.getSimpleName();

    @Ignore
    @SerializedName("mphone")
    private String mPhoneOri;
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;
    @ColumnInfo(name = "action")
    @SerializedName("action")
    private String mAction;
    @ColumnInfo(name = "label")
    @SerializedName("label")
    private String mLabel;
    @ColumnInfo(name = "category")
    @SerializedName("category")
    private String mCategory;
    @ColumnInfo(name = "time")
    @SerializedName("eventTime")
    private String mTime;
    @ColumnInfo(name = "phone")
    private String mPhone;

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public String getPhone() {
        return mPhoneOri;
    }

    public void setPhone(String phone) {
        mPhoneOri = phone;
    }

    public String getPhoneData() {
        return mPhone;
    }

    public void setPhoneData(String phone) {
        setPhone(phone);
        try {
            mPhone = AESCipher.encryptAES(phone);
        } catch (Exception e) {
            Log.e(TAG, "Phone decrypt error");
        }
    }

    public String getAction() {
        return mAction;
    }

    public void setAction(String action) {
        mAction = action;
    }

    public void setLabel(String label) {
        mLabel = label;
    }

    public String getLabel() {
        return mLabel;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String cateogry) {
        mCategory = cateogry;
    }

    public void setTime(String time) {
        mTime = time;
    }

    public String getTime() {
        return mTime;
    }

    public UseLogData(String phone, String label, String time) {
        mPhoneOri = phone;
        try {
            mPhone = AESCipher.encryptAES(phone);
        } catch (Exception e) {
            Log.e(TAG, "Phone encrypt error");
        }

        mAction = "DialPhone";
        mLabel = label;
        mCategory = "DialphonePage";
        mTime = time;
    }
}
