package com.luxgen.remote.network.model;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class ResponseData {

    @SerializedName("errCde")
    private String mErrCode = "";
    @SerializedName("errMsg")
    private String mErrMessage = "";

    /**
     * 是否執行成功，從文件來看當回傳的 code 為 0 時
     * 即辨定為執行成功
     */
    public boolean isSuccessful() {
        if (mErrCode == null || mErrCode.isEmpty()) {
            return false;
        } else {
            // 因為看文件似乎有些是 "0", 有些是 "00"
            int code = Integer.valueOf(mErrCode);
            return code == 0;
        }
    }

    public boolean isAuthFail() {
        if (TextUtils.isEmpty(mErrCode)) {
            return false;
        } else {
            int code = Math.abs(Integer.valueOf(mErrCode));
            return code == 856;
        }
    }

    /**
     * 伺服器回傳的錯誤編碼
     */
    public String getErrorCode() {
        return mErrCode;
    }

    /**
     * 伺服器回傳的錯誤訊息
     */
    public String getErrorMessage() {
        return mErrMessage;
    }

    private int mCode;

    /**
     * 取得執行結果的 HTTP Code
     */
    public int getStatusCode() {
        return mCode;
    }

    public void setStatusCode(int code) {
        mCode = code;
    }

    public static ResponseData parseResponseBy(String resString) {
        if (TextUtils.isEmpty(resString)) {
            return new ResponseData();
        }

        Gson gson = new Gson();
        return gson.fromJson(resString, ResponseData.class);
    }

}
