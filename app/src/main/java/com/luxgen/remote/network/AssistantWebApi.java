package com.luxgen.remote.network;

import android.content.Context;
import android.support.annotation.NonNull;

import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.network.ApiInterface.AssistantInterface;
import com.luxgen.remote.network.ApiInterface.MaintenanceInterface;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.VigRequestData;
import com.luxgen.remote.network.model.assistant.CheckRemoteControlStartResponseData;
import com.luxgen.remote.network.model.assistant.GetCampaignResponseData;
import com.luxgen.remote.network.model.assistant.GetEtcRequestData;
import com.luxgen.remote.network.model.assistant.GetEtcResponseData;
import com.luxgen.remote.network.model.assistant.GetInitialDataResponseData;
import com.luxgen.remote.network.model.assistant.GetOpenDataAirQualityRequestData;
import com.luxgen.remote.network.model.assistant.GetOpenDataAirQualityResponseData;
import com.luxgen.remote.network.model.assistant.GetPm25ResponseData;
import com.luxgen.remote.network.model.assistant.GetPushRequestData;
import com.luxgen.remote.network.model.assistant.GetPushResponseData;
import com.luxgen.remote.network.model.assistant.GetWeatherRequestData;
import com.luxgen.remote.network.model.assistant.GetWeatherResponseData;
import com.luxgen.remote.network.model.assistant.SetIkeyFCMTokenRequestData;
import com.luxgen.remote.network.model.assistant.SetPushRequestData;
import com.luxgen.remote.network.model.assistant.SetUseLogRequestData;
import com.luxgen.remote.network.model.maintenance.DelCarRepairPlanRequestData;
import com.luxgen.remote.network.model.maintenance.DelCarRepairPlanResponseData;
import com.luxgen.remote.network.model.maintenance.GetCarProgressInWorkshopRequestData;
import com.luxgen.remote.network.model.maintenance.GetCarProgressInWorkshopResponseData;
import com.luxgen.remote.network.model.maintenance.GetCarRepairPlanRequestData;
import com.luxgen.remote.network.model.maintenance.GetCarRepairPlanResponseData;
import com.luxgen.remote.network.model.maintenance.GetCarRepairProgressRequestData;
import com.luxgen.remote.network.model.maintenance.GetCarRepairProgressResponseData;
import com.luxgen.remote.network.model.maintenance.GetRepairHistoryRequestData;
import com.luxgen.remote.network.model.maintenance.GetRepairHistoryResponseData;
import com.luxgen.remote.network.model.maintenance.GetRepairRequestData;
import com.luxgen.remote.network.model.maintenance.GetRepairResponseData;
import com.luxgen.remote.network.model.maintenance.GetWorkShopCapacityRequestData;
import com.luxgen.remote.network.model.maintenance.GetWorkShopCapacityResponseData;
import com.luxgen.remote.network.model.maintenance.GetWorkShopRequestData;
import com.luxgen.remote.network.model.maintenance.GetWorkShopResponseData;
import com.luxgen.remote.network.model.maintenance.SetCarRepairPeriodRequestData;
import com.luxgen.remote.network.model.maintenance.SetCarRepairPeriodResponseData;
import com.luxgen.remote.network.model.maintenance.ValidateVehicleRequestData;

import java.io.IOException;
import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AssistantWebApi extends WebApi {

    private final String LOG_TAG = AssistantWebApi.class.getSimpleName();

    private static WeakReference<AssistantWebApi> mInstance;

    public static AssistantWebApi getInstance(Context context) {
        if (mInstance == null || mInstance.get() == null) {
            mInstance = new WeakReference<>(new AssistantWebApi(context));
        }

        return mInstance.get();
    }

    private AssistantWebApi(Context context) {
        super(context);
    }

    /**
     * API No.1	取得車輛保修基本里程資料(保修)
     */
    public void getRepairData(GetRepairRequestData data,
                              final OnApiListener<GetRepairResponseData> listener) {
        listener.onPreApiTask();

        getVigRetrofit().create(MaintenanceInterface.class)
                .getRepairData(data)
                .enqueue(new Callback<GetRepairResponseData>() {
                    @Override
                    public void onResponse(Call<GetRepairResponseData> call, Response<GetRepairResponseData> response) {
                        GetRepairResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GetRepairResponseData> call, Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getRepairData", t));
                    }
                });
    }

    /**
     * API No.2 取得保養廠資料
     */
    public void getWorkshopData(GetWorkShopRequestData data,
                                final OnApiListener<GetWorkShopResponseData> listener) {
        listener.onPreApiTask();
        getVigRetrofit().create(MaintenanceInterface.class)
                .getWorkShopList(data)
                .enqueue(new Callback<GetWorkShopResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetWorkShopResponseData> call,
                                           @NonNull Response<GetWorkShopResponseData> response) {
                        GetWorkShopResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetWorkShopResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getWorkshopData", t));
                    }
                });
    }


    // API No.3 取得保養廠保修能量
    public void getWorkShopCapacity(GetWorkShopCapacityRequestData data,
                                    final OnApiListener<GetWorkShopCapacityResponseData> listener) {
        listener.onPreApiTask();
        getVigRetrofit().create(MaintenanceInterface.class)
                .getWorkShopCapacity(data)
                .enqueue(new Callback<GetWorkShopCapacityResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetWorkShopCapacityResponseData> call,
                                           @NonNull Response<GetWorkShopCapacityResponseData> response) {
                        GetWorkShopCapacityResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetWorkShopCapacityResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getWorkShopCapacity", t));
                    }
                });
    }

    // API No.4 預約保養廠
    public void setCarRepairPeriod(SetCarRepairPeriodRequestData data,
                                   final OnApiListener<SetCarRepairPeriodResponseData> listener) {
        listener.onPreApiTask();
        getVigRetrofit().create(MaintenanceInterface.class)
                .setCarRepairPeriod(data)
                .enqueue(new Callback<SetCarRepairPeriodResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<SetCarRepairPeriodResponseData> call,
                                           @NonNull Response<SetCarRepairPeriodResponseData> response) {
                        SetCarRepairPeriodResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<SetCarRepairPeriodResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("setCarRepairPeriod", t));
                    }
                });
    }

    /**
     * API No.5 查詢您已預約保養
     */
    public void getCarRepairPlan(GetCarRepairPlanRequestData data,
                                 final OnApiListener<GetCarRepairPlanResponseData> listener) {
        listener.onPreApiTask();

        getVigRetrofit().create(MaintenanceInterface.class)
                .getCarRepairPlanList(data)
                .enqueue(new Callback<GetCarRepairPlanResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetCarRepairPlanResponseData> call,
                                           @NonNull Response<GetCarRepairPlanResponseData> response) {
                        GetCarRepairPlanResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetCarRepairPlanResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getCarRepairPlanList", t));
                    }
                });
    }

    /**
     * API No.6 刪除預約保養
     */
    public void delCarRepairPlan(DelCarRepairPlanRequestData data,
                                 final OnApiListener<DelCarRepairPlanResponseData> listener) {
        listener.onPreApiTask();

        getVigRetrofit().create(MaintenanceInterface.class)
                .delCarRepairPlan(data)
                .enqueue(new Callback<DelCarRepairPlanResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<DelCarRepairPlanResponseData> call,
                                           @NonNull Response<DelCarRepairPlanResponseData> response) {
                        DelCarRepairPlanResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<DelCarRepairPlanResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getCarRepairPlanList", t));
                    }
                });
    }

    /**
     * API No.7 查詢保修紀錄
     */
    public void getRepairHistory(GetRepairHistoryRequestData data,
                                 final OnApiListener<GetRepairHistoryResponseData> listener) {
        listener.onPreApiTask();

        getVigRetrofit().create(MaintenanceInterface.class)
                .getRepairHistory(data)
                .enqueue(new Callback<GetRepairHistoryResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetRepairHistoryResponseData> call,
                                           @NonNull Response<GetRepairHistoryResponseData> response) {
                        GetRepairHistoryResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetRepairHistoryResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getRepairHistory", t));
                    }
                });
    }

    /**
     * API No.10 取得行銷活動URL
     */
    public void getCampaign(final OnApiListener<GetCampaignResponseData> listener) {
        listener.onPreApiTask();

        getVigRetrofit()
                .create(AssistantInterface.class)
                .getCampaignList()
                .enqueue(new Callback<GetCampaignResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetCampaignResponseData> call,
                                           @NonNull Response<GetCampaignResponseData> response) {
                        GetCampaignResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetCampaignResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getCampaign", t));
                    }
                });
    }

    /**
     * API No.21 ikeyFCM Token註冊
     */
    public void setFcmToken(SetIkeyFCMTokenRequestData data, final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(AssistantInterface.class)
                .setFcmToken(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                        onSuccessful(response, listener);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("setPushStatus", t));
                    }
                });
    }


    /**
     * API No.23 取得車主推播通知內容
     */
    public void getPushList(GetPushRequestData data, final OnApiListener<GetPushResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(AssistantInterface.class)
                .getPushList(data)
                .enqueue(new Callback<GetPushResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetPushResponseData> call,
                                           @NonNull Response<GetPushResponseData> response) {
                        GetPushResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetPushResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getPushList", t));
                    }
                });
    }

    /**
     * 開機參數，罐頭訊息
     * API No.26 開機參數取得
     */
    public void getInitialData(final OnApiListener<GetInitialDataResponseData> listener) {
        listener.onPreApiTask();

        getVigRetrofit()
                .create(AssistantInterface.class)
                .getInitialData()
                .enqueue(new Callback<GetInitialDataResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetInitialDataResponseData> call,
                                           @NonNull Response<GetInitialDataResponseData> response) {
                        GetInitialDataResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetInitialDataResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getInitialData", t));
                    }
                });
    }

    /**
     * API No.43 取得PM2.5值，prefix: api_vig_prefix
     */
    public void getPm25(VigRequestData data, final OnApiListener<GetPm25ResponseData> listener) {
        listener.onPreApiTask();

        getVigRetrofit()
                .create(AssistantInterface.class)
                .getPm25(data)
                .enqueue(new Callback<GetPm25ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetPm25ResponseData> call,
                                           @NonNull Response<GetPm25ResponseData> response) {
                        GetPm25ResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetPm25ResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getPm25", t));
                    }
                });
    }

    /**
     * API No.45 ETC API
     */
    public void getEtc(GetEtcRequestData data,
                       final OnApiListener<GetEtcResponseData> listener) {
        listener.onPreApiTask();
        getVigRetrofit().create(AssistantInterface.class)
                .getEtc(data)
                .enqueue(new Callback<GetEtcResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetEtcResponseData> call,
                                           @NonNull Response<GetEtcResponseData> response) {
                        GetEtcResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetEtcResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getEtc", t));
                    }
                });
    }

    /**
     * API No.48 取得天氣資訊
     *
     * @param data     參數為縣市名
     * @param listener
     */
    public void getWeather(GetWeatherRequestData data, final OnApiListener<GetWeatherResponseData> listener) {
        listener.onPreApiTask();
        getIotRetrofit().create(AssistantInterface.class)
                .getWeather(data)
                .enqueue(new Callback<GetWeatherResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetWeatherResponseData> call,
                                           @NonNull Response<GetWeatherResponseData> response) {
                        GetWeatherResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetWeatherResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getWeather", t));
                    }
                });
    }

    /**
     * API No.52 取得外部空氣品質
     */
    public void getOpenDataAirQuality(GetOpenDataAirQualityRequestData data, final OnApiListener<GetOpenDataAirQualityResponseData> listener) {
        listener.onPreApiTask();
        getIotRetrofit().create(AssistantInterface.class)
                .getOpenDataAirQuality(data)
                .enqueue(new Callback<GetOpenDataAirQualityResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetOpenDataAirQualityResponseData> call,
                                           @NonNull Response<GetOpenDataAirQualityResponseData> response) {
                        GetOpenDataAirQualityResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetOpenDataAirQualityResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getOpenDataAirQuality", t));
                    }
                });
    }


    /**
     * API No.54 查詢保修進度
     */
    public void getCarRepairProgress(GetCarRepairProgressRequestData data,
                                     final OnApiListener<GetCarRepairProgressResponseData> listener) {
        listener.onPreApiTask();

        getVigRetrofit().create(MaintenanceInterface.class)
                .getRepairProgress(data)
                .enqueue(new Callback<GetCarRepairProgressResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetCarRepairProgressResponseData> call,
                                           @NonNull Response<GetCarRepairProgressResponseData> response) {
                        GetCarRepairProgressResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetCarRepairProgressResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getCarRepairProgress", t));
                    }
                });
    }

    /**
     * API No.64 檢核是否可以進行遠端發動
     */
    public void checkRemoteControlStart(VigRequestData data, final OnApiListener<CheckRemoteControlStartResponseData> listener) {
        listener.onPreApiTask();

        getVigRetrofit()
                .create(AssistantInterface.class)
                .checkRemoteControlStart(data)
                .enqueue(new Callback<CheckRemoteControlStartResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<CheckRemoteControlStartResponseData> call,
                                           @NonNull Response<CheckRemoteControlStartResponseData> response) {
                        CheckRemoteControlStartResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<CheckRemoteControlStartResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("checkRemoteControlStart", t));
                    }
                });
    }

    /**
     * API No.72 設定推播通知內容狀態
     */
    public void setPushStatus(SetPushRequestData data, final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(AssistantInterface.class)
                .setPushStatus(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseData> call,
                                           @NonNull Response<ResponseData> response) {
                        onSuccessful(response, listener);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("setPushStatus", t));
                    }
                });
    }

    /**
     * API No.73 UseLog(撥打電話Log)
     */
    public void setUseLog(SetUseLogRequestData data, final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(AssistantInterface.class)
                .setUseLog(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseData> call,
                                           @NonNull Response<ResponseData> response) {
                        onSuccessful(response, listener);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("setPushStatus", t));
                    }
                });
    }

    public void validateVehicle(ValidateVehicleRequestData data,
                                final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();

        getVigRetrofit().create(MaintenanceInterface.class)
                .validateVehicle(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseData> call,
                                           @NonNull Response<ResponseData> response) {
                        ResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("validateVehicle", t));
                    }
                });
    }

    public void getCarProgressInWorkshop(GetCarProgressInWorkshopRequestData data,
                                     final OnApiListener<GetCarProgressInWorkshopResponseData> listener) {
        listener.onPreApiTask();

        getVigRetrofit().create(MaintenanceInterface.class)
                .getProgressInWorkshop(data)
                .enqueue(new Callback<GetCarProgressInWorkshopResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetCarProgressInWorkshopResponseData> call,
                                           @NonNull Response<GetCarProgressInWorkshopResponseData> response) {
                        GetCarProgressInWorkshopResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetCarProgressInWorkshopResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getCarProgressInWorkshopResponse", t));
                    }
                });
    }
}
