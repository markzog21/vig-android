package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.model.PushData;
import com.luxgen.remote.model.option.NotifactionOptions;
import com.luxgen.remote.network.model.RequestData;

import java.util.ArrayList;

public class SetPushRequestData extends RequestData {
    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName("dataList")
    private ArrayList<SetPushData> mList;

    public SetPushRequestData(String iKeyUid, String iKeyToken) {
        mKeyUid = iKeyUid;
        mKeyToken = iKeyToken;
        mList = new ArrayList<>();
    }

    public void setDeleteList(ArrayList<PushData> list) {
        for (PushData data : list) {
            SetPushData spd = contain(data);
            spd.setStatus(NotifactionOptions.changeDeleteStatus(data.getReadStatus()));
            mList.add(spd);
        }
    }

    public void setReadList(ArrayList<PushData> list) {
        for (PushData data : list) {
            SetPushData spd = contain(data);
            spd.setStatus(NotifactionOptions.READ);
            mList.add(spd);
        }
    }

    public void setData(PushData data) {
        final SetPushData spd = new SetPushData();
        spd.setUuid(data.getUuid());
        spd.setStatus(data.getReadStatus());
        mList.add(spd);
    }

    private SetPushData contain(PushData item) {
        for (SetPushData data : mList) {
            if (data.getUuid().equals(item.getUuid())) {
                return data;
            }
        }
        SetPushData data = new SetPushData();
        data.setUuid(item.getUuid());
        return data;
    }
}
