package com.luxgen.remote.network;

import android.content.Context;
import android.support.annotation.NonNull;

import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.network.ApiInterface.CarInterface;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.VigRequestData;
import com.luxgen.remote.network.model.car.AddNewCarRequestData;
import com.luxgen.remote.network.model.car.CheckPasswordRequestData;
import com.luxgen.remote.network.model.car.DeleteMasterKeyByAPPResponseData;
import com.luxgen.remote.network.model.car.DmsUpgradeRequestData;
import com.luxgen.remote.network.model.car.DmsUpgradeResponseData;
import com.luxgen.remote.network.model.car.GetCarKeyRequestData;
import com.luxgen.remote.network.model.car.GetCarKeyResponseData;
import com.luxgen.remote.network.model.car.GetCarStatusResponseData;
import com.luxgen.remote.network.model.car.GetCarTrackResponseData;
import com.luxgen.remote.network.model.car.GetKeyChainListRequestData;
import com.luxgen.remote.network.model.car.GetKeyChainListResponseData;
import com.luxgen.remote.network.model.car.GetKeyListResponseData;
import com.luxgen.remote.network.model.car.GetProcedureRecordbyAPPRequestData;
import com.luxgen.remote.network.model.car.GetProcedureRecordbyAPPResponseData;
import com.luxgen.remote.network.model.car.GetRemoteControlLogResponseData;
import com.luxgen.remote.network.model.car.GetSpeedLimitStatusRequestData;
import com.luxgen.remote.network.model.car.GetSpeedLimitStatusResponseData;
import com.luxgen.remote.network.model.car.GetWakeupVigResultRequestData;
import com.luxgen.remote.network.model.car.GetWakeupVigResultResponseData;
import com.luxgen.remote.network.model.car.SetBeaconLearingStatusRequestData;
import com.luxgen.remote.network.model.car.SetBeaconRequestData;
import com.luxgen.remote.network.model.car.SetCarControlLogRequestData;
import com.luxgen.remote.network.model.car.UpgradeCheckMessageRequestData;
import com.luxgen.remote.network.model.car.UpgradeCheckMessageResponseData;
import com.luxgen.remote.network.model.car.WakeupVigRequestData;

import java.io.IOException;
import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarWebApi extends WebApi {

    private final String LOG_TAG = CarWebApi.class.getSimpleName();

    private static WeakReference<CarWebApi> mInstance;

    public static CarWebApi getInstance(Context context) {
        if (mInstance == null || mInstance.get() == null) {
            mInstance = new WeakReference<>(new CarWebApi(context));
        }

        return mInstance.get();
    }

    private CarWebApi(Context context) {
        super(context);
    }

    /**
     * 取得完整車輛列表內容 <br />
     * API No.24 取得完整車輛列表內容
     *
     * @param data     皆必填
     * @param listener 裡面的物件請參見 {@link com.luxgen.remote.network.model.car.CarKeyData} <br />
     *                 而物件中有兩個 StringDef Type: <br />
     *                 1. {@link com.luxgen.remote.model.option.CarOptions.CarMaster} 此車資料的所屬
     *                 2. {@link com.luxgen.remote.model.option.CarOptions.KeyType}   此鑰匙的分類
     */
    public void getCarKeyList(GetCarKeyRequestData data,
                              final OnApiListener<GetCarKeyResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(CarInterface.class)
                .getCarKeyList(data)
                .enqueue(new Callback<GetCarKeyResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetCarKeyResponseData> call,
                                           @NonNull Response<GetCarKeyResponseData> response) {
                        GetCarKeyResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetCarKeyResponseData> call,
                                          @NonNull Throwable t) {
                        String log = LOG_TAG + "getCarKeyList: " + t.toString();
                        LogCat.d(log);
                        listener.onApiTaskFailure(getFailMessage("getCarKeyList", t));
                    }
                });
    }

    /**
     * API No.28 新增車輛
     */
    public void addNewCar(AddNewCarRequestData data,
                          final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(CarInterface.class)
                .addNewCar(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseData> call,
                                           @NonNull Response<ResponseData> response) {
                        onSuccessful(response, listener);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        onFail("addNewCar", t, listener);
                    }
                });
    }

    /**
     * API No.30 密碼驗證
     */
    public void checkPassword(CheckPasswordRequestData data, final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(CarInterface.class)
                .checkPassword(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseData> call,
                                           @NonNull Response<ResponseData> response) {
                        onSuccessful(response, listener);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        onFail(LOG_TAG + "checkPassword: ", t, listener);
                    }
                });
    }

    /**
     * API No.31 喚醒VIG
     *
     * @param listener 是否成功，請用 {@link ResponseData#isSuccessful()} 判斷
     */
    public void wakeUpVig(WakeupVigRequestData data,
                          final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofitLongTimeout().create(CarInterface.class)
                .wakeUpVig(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseData> call,
                                           @NonNull Response<ResponseData> response) {
                        onSuccessful(response, listener);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        onFail(LOG_TAG + "wakeUpVig: ", t, listener);
                    }
                });
    }

    /**
     * API No.32 查詢VIG是否已喚醒
     */
    public void getWakeUpVigResult(GetWakeupVigResultRequestData data,
                                   final OnApiListener<GetWakeupVigResultResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(CarInterface.class)
                .getWakeUpVigResult(data)
                .enqueue(new Callback<GetWakeupVigResultResponseData>() {
                    @Override
                    public void onResponse(Call<GetWakeupVigResultResponseData> call,
                                           Response<GetWakeupVigResultResponseData> response) {
                        GetWakeupVigResultResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GetWakeupVigResultResponseData> call, Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getWakeUpVigResult", t));
                    }
                });
    }

    /**
     * API No.35 設定Beacon
     */
    public void setBeacons(SetBeaconRequestData data,
                           final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(CarInterface.class)
                .setBeacons(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseData> call,
                                           @NonNull Response<ResponseData> response) {
                        onSuccessful(response, listener);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        onFail(LOG_TAG + "setBeacons: ", t, listener);
                    }
                });
    }

    /**
     * API No.37 查詢是否限速
     */
    public void getSpeedLimitStatus(GetSpeedLimitStatusRequestData data,
                                    final OnApiListener<GetSpeedLimitStatusResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(CarInterface.class)
                .getSpeedLimitStatus(data)
                .enqueue(new Callback<GetSpeedLimitStatusResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetSpeedLimitStatusResponseData> call,
                                           @NonNull Response<GetSpeedLimitStatusResponseData> response) {
                        GetSpeedLimitStatusResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetSpeedLimitStatusResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getSpeedLimitStatus", t));
                    }
                });
    }

    /**
     * API No.38 查詢Key列表
     */
    public void getKeyList(VigRequestData data,
                           final OnApiListener<GetKeyListResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(CarInterface.class)
                .getKeyList(data)
                .enqueue(new Callback<GetKeyListResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetKeyListResponseData> call,
                                           @NonNull Response<GetKeyListResponseData> response) {
                        GetKeyListResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetKeyListResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getKeyList", t));
                    }
                });
    }

    /**
     * API No.39 設定車輛控制Log
     */
    public void setCarControlLog(SetCarControlLogRequestData data,
                                 final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(CarInterface.class)
                .setCarControlLog(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseData> call,
                                           @NonNull Response<ResponseData> response) {
                        onSuccessful(response, listener);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        onFail(LOG_TAG + "setCarControlLog: ", t, listener);
                    }
                });
    }

    /**
     * API No.42 取得車輛軌跡
     */
    public void getCarTrack(VigRequestData data,
                            final OnApiListener<GetCarTrackResponseData> listener) {
        listener.onPreApiTask();

        getVigRetrofit().create(CarInterface.class)
                .getCarTrack(data)
                .enqueue(new Callback<GetCarTrackResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetCarTrackResponseData> call,
                                           @NonNull Response<GetCarTrackResponseData> response) {
                        GetCarTrackResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetCarTrackResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getCarTrack", t));
                    }
                });
    }

    /**
     * API No.46 取得車輛狀態
     *
     * @param listener 裡面的各種狀態，
     *                 請直接使用 {@link com.luxgen.remote.model.option.CarOptions.CarStatus}
     *                 所有的狀態皆已在物件裡對應了
     */
    public void getCarStatus(VigRequestData data,
                             final OnApiListener<GetCarStatusResponseData> listener) {
        listener.onPreApiTask();

        getVigRetrofit().create(CarInterface.class)
                .getCarStatus(data)
                .enqueue(new Callback<GetCarStatusResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetCarStatusResponseData> call,
                                           @NonNull Response<GetCarStatusResponseData> response) {
                        GetCarStatusResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetCarStatusResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getCarStatus", t));
                    }
                });
    }

    /**
     * API No.47 取得遠端控制Log
     */
    public void getRemoteControlLog(VigRequestData data,
                                    final OnApiListener<GetRemoteControlLogResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(CarInterface.class)
                .getRemoteControlLog(data)
                .enqueue(new Callback<GetRemoteControlLogResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetRemoteControlLogResponseData> call,
                                           @NonNull Response<GetRemoteControlLogResponseData> response) {
                        GetRemoteControlLogResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetRemoteControlLogResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getRemoteControlLog", t));
                    }
                });
    }

    /**
     * API No.49 "新增車輛程序" 電話號碼驗證程序
     */
    public void upgradePhoneNo(DmsUpgradeRequestData data,
                               final OnApiListener<DmsUpgradeResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(CarInterface.class)
                .upgradePhoneNo(data)
                .enqueue(new Callback<DmsUpgradeResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<DmsUpgradeResponseData> call,
                                           @NonNull Response<DmsUpgradeResponseData> response) {
                        DmsUpgradeResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<DmsUpgradeResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("upgradePhoneNo", t));
                    }
                });
    }

    /**
     * API No.50 "新增車輛程序" 驗證簡訊資料
     */
    public void upgradeCheckPhone(UpgradeCheckMessageRequestData data,
                                  final OnApiListener<UpgradeCheckMessageResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(CarInterface.class)
                .upgradeCheckMessage(data)
                .enqueue(new Callback<UpgradeCheckMessageResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<UpgradeCheckMessageResponseData> call,
                                           @NonNull Response<UpgradeCheckMessageResponseData> response) {
                        UpgradeCheckMessageResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<UpgradeCheckMessageResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("upgradeCheckPhone", t));
                    }
                });
    }

    /**
     * API No.70 詢問 keyChainList
     */
    public void getKeyChainList(GetKeyChainListRequestData data,
                                final OnApiListener<GetKeyChainListResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(CarInterface.class)
                .getKeyChainList(data)
                .enqueue(new Callback<GetKeyChainListResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetKeyChainListResponseData> call,
                                           @NonNull Response<GetKeyChainListResponseData> response) {
                        GetKeyChainListResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetKeyChainListResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getKeyChainList", t));
                    }
                });
    }

    /**
     * API No.78 設定用戶 beacon 學習狀態
     */
    public void setBeaconLearingStatus(SetBeaconLearingStatusRequestData data,
                                       final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(CarInterface.class)
                .setBeaconLearingStatus(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseData> call,
                                           @NonNull Response<ResponseData> response) {
                        onSuccessful(response, listener);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        onFail(LOG_TAG + "setBeaconLearingStatusData: ", t, listener);
                    }
                });
    }

    /**
     * API No.79 刪除所有Key
     */
    public void deleteMasterKeyByAPP(VigRequestData data,
                                     final OnApiListener<DeleteMasterKeyByAPPResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(CarInterface.class)
                .deleteMasterKeyByAPP(data)
                .enqueue(new Callback<DeleteMasterKeyByAPPResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<DeleteMasterKeyByAPPResponseData> call,
                                           @NonNull Response<DeleteMasterKeyByAPPResponseData> response) {
                        DeleteMasterKeyByAPPResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<DeleteMasterKeyByAPPResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("deleteMasterKeyByAPP", t));
                    }
                });
    }

    /**
     * API No.80 查詢 ProcedureID 執行結果
     */
    public void getProcedureRecordbyAPP(GetProcedureRecordbyAPPRequestData data,
                                        final OnApiListener<GetProcedureRecordbyAPPResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(CarInterface.class)
                .getProcedureRecordbyAPP(data)
                .enqueue(new Callback<GetProcedureRecordbyAPPResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetProcedureRecordbyAPPResponseData> call,
                                           @NonNull Response<GetProcedureRecordbyAPPResponseData> response) {
                        GetProcedureRecordbyAPPResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetProcedureRecordbyAPPResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getProcedureRecordbyAPP", t));
                    }
                });
    }
}
