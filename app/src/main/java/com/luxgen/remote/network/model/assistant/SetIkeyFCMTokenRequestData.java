package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class SetIkeyFCMTokenRequestData extends RequestData {
    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName("FCMToken")
    private String mFcmToken;
    @SerializedName("phoneType")
    private String mPhoneType;

    public SetIkeyFCMTokenRequestData(String iKeyUid, String iKeyToken, String fcmToken) {
        mKeyUid = iKeyUid;
        mKeyToken = iKeyToken;
        mFcmToken = fcmToken;
        mPhoneType = "A";
    }
}
