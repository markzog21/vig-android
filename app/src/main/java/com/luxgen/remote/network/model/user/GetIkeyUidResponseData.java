package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class GetIkeyUidResponseData extends ResponseData {

    @SerializedName("ikeyUID")
    private String mKeyUid;
    @SerializedName("ikeyEmail")
    private String mKeyMail;
    @SerializedName("ikeyName")
    private String mKeyName;
    @SerializedName("userName")
    private String mUserName;

    public String getUid() {
        return mKeyUid;
    }

    public String getEmail() {
        return mKeyMail;
    }

    public String getName() {
        return mKeyName;
    }

    public String getUserName() {
        return mUserName;
    }
}
