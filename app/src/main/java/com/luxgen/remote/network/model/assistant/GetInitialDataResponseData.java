package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class GetInitialDataResponseData extends ResponseData {

    @SerializedName("beaconIdentify")
    private String mId;
    @SerializedName("wakeUpTimeLimit")
    private int mTime = 60;
    @SerializedName("repairCarQuestionnaireURL")
    private String mUrl;
    @SerializedName("canConversation")
    private String mConversation;
    @SerializedName("PinCodeMaxTime")
    private int mPinCodeMaxTime;

    private String[] mConversations;

    /**
     * 辨識藍芽用
     */
    public String getBeaconId() {
        return mId;
    }

    /**
     * 喚醒最長等待時間，單位秒，暫定60秒
     */
    public int getWakeUpTime() {
        return mTime;
    }

    /**
     * 保修問卷 URL
     */
    public String getUrl() {
        return mUrl;
    }

    /**
     * 銷顧罐頭問候
     */
    public String[] getConversations() {
        init();
        return mConversations;
    }

    public int getConversationSize() {
        init();
        return mConversations.length;
    }

    public int getPinCodeMaxTime() {
        return mPinCodeMaxTime;
    }

    private void init() {
        if (mConversations == null) {
            mConversations = (mConversation == null || mConversation.isEmpty())
                    ? new String[]{}
                    : mConversation.split(";");
        }
    }
}
