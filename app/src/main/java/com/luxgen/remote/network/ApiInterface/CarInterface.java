package com.luxgen.remote.network.ApiInterface;

import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.VigRequestData;
import com.luxgen.remote.network.model.car.AddNewCarRequestData;
import com.luxgen.remote.network.model.car.CheckPasswordRequestData;
import com.luxgen.remote.network.model.car.DeleteMasterKeyByAPPResponseData;
import com.luxgen.remote.network.model.car.DmsUpgradeRequestData;
import com.luxgen.remote.network.model.car.DmsUpgradeResponseData;
import com.luxgen.remote.network.model.car.GetCarKeyRequestData;
import com.luxgen.remote.network.model.car.GetCarKeyResponseData;
import com.luxgen.remote.network.model.car.GetCarStatusResponseData;
import com.luxgen.remote.network.model.car.GetCarTrackResponseData;
import com.luxgen.remote.network.model.car.GetKeyChainListRequestData;
import com.luxgen.remote.network.model.car.GetKeyChainListResponseData;
import com.luxgen.remote.network.model.car.GetKeyListResponseData;
import com.luxgen.remote.network.model.car.GetProcedureRecordbyAPPRequestData;
import com.luxgen.remote.network.model.car.GetProcedureRecordbyAPPResponseData;
import com.luxgen.remote.network.model.car.GetRemoteControlLogResponseData;
import com.luxgen.remote.network.model.car.GetSpeedLimitStatusRequestData;
import com.luxgen.remote.network.model.car.GetSpeedLimitStatusResponseData;
import com.luxgen.remote.network.model.car.GetWakeupVigResultRequestData;
import com.luxgen.remote.network.model.car.GetWakeupVigResultResponseData;
import com.luxgen.remote.network.model.car.SetBeaconLearingStatusRequestData;
import com.luxgen.remote.network.model.car.SetBeaconRequestData;
import com.luxgen.remote.network.model.car.SetCarControlLogRequestData;
import com.luxgen.remote.network.model.car.UpgradeCheckMessageRequestData;
import com.luxgen.remote.network.model.car.UpgradeCheckMessageResponseData;
import com.luxgen.remote.network.model.car.WakeupVigRequestData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface CarInterface {
    // API No.24 取得完整車輛列表內容
    @POST("/VIGP1/accountManager/getCarKeyList")
    Call<GetCarKeyResponseData> getCarKeyList(@Body GetCarKeyRequestData data);

    // API No.28 新增車輛
    @POST("/VIGP1/accountManager/addNewCar")
    Call<ResponseData> addNewCar(@Body AddNewCarRequestData data);

    // API No.30 密碼驗證
    @POST("/VIGP1/accountManager/checkPassword")
    Call<ResponseData> checkPassword(@Body CheckPasswordRequestData data);

    // API No.31 喚醒VIG
    @POST("/VIGP1/accountManager/wakeUpVIG")
    Call<ResponseData> wakeUpVig(@Body WakeupVigRequestData data);

    // API No.32 查詢VIG是否已喚醒
    @POST("/VIGP1/accountManager/getVIGWakeUpResult")
    Call<GetWakeupVigResultResponseData> getWakeUpVigResult(@Body GetWakeupVigResultRequestData data);

    // API No.35 設定Beacon
    @POST("/VIGP1/accountManager/setBeacons")
    Call<ResponseData> setBeacons(@Body SetBeaconRequestData data);

    // API No.37 查詢是否限速
    @POST("/VIGP1/accountManager/getSpeedLimitStatus")
    Call<GetSpeedLimitStatusResponseData> getSpeedLimitStatus(@Body GetSpeedLimitStatusRequestData data);

    // API No.38 查詢Key列表
    @POST("/VIGP1/accountManager/getKeyList")
    Call<GetKeyListResponseData> getKeyList(@Body VigRequestData data);

    // API No.39 設定車輛控制Log
    @POST("/VIGP1/accountManager/setCarControlLog")
    Call<ResponseData> setCarControlLog(@Body SetCarControlLogRequestData data);

    // API No.42 取得車輛軌跡
    @POST("/VIGP1/accountManager/getCarTrack")
    Call<GetCarTrackResponseData> getCarTrack(@Body VigRequestData data);

    // API No.46 取得車輛狀態
    @POST("/VIGP1/accountManager/getCarStatus")
    Call<GetCarStatusResponseData> getCarStatus(@Body VigRequestData data);

    // API No.47 取得遠端控制Log
    @POST("/VIGP1/accountManager/getRemoteContolLog")
    Call<GetRemoteControlLogResponseData> getRemoteControlLog(@Body VigRequestData data);

    // API No.49 "新增車輛程序" 電話號碼驗證程序
    @POST("/VIGP1/memberManager/DMSUpgradeCheckPhoneNO")
    Call<DmsUpgradeResponseData> upgradePhoneNo(@Body DmsUpgradeRequestData data);

    // API No.50 "新增車輛程序" 驗證簡訊資料
    @POST("/VIGP1/accountManager/upgradeCheckMessage")
    Call<UpgradeCheckMessageResponseData> upgradeCheckMessage(@Body UpgradeCheckMessageRequestData data);

    // API No.70 詢問keyChainList
    @POST("/VIGP1/accountManager/getKeyChainList")
    Call<GetKeyChainListResponseData> getKeyChainList(@Body GetKeyChainListRequestData data);

    // API No.78 設定用戶beacon學習狀態
    @POST("/VIGP1/accountManager/setBeaconLearingStatus")
    Call<ResponseData> setBeaconLearingStatus(@Body SetBeaconLearingStatusRequestData data);

    // API No.79 刪除所有Key
    @POST("/VIGP1/accountManager/DeleteMasterKeyByAPP")
    Call<DeleteMasterKeyByAPPResponseData> deleteMasterKeyByAPP(@Body VigRequestData data);

    // API No.80 查詢ProcedureID執行結果
    @POST("/VIGP1/accountManager/getProcedureRecordbyAPP")
    Call<GetProcedureRecordbyAPPResponseData> getProcedureRecordbyAPP(@Body GetProcedureRecordbyAPPRequestData data);
}
