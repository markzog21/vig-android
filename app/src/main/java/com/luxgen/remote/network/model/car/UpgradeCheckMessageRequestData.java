package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class UpgradeCheckMessageRequestData extends RequestData {
    @SerializedName(KEY_ACCOUNT)
    private String mKeyAccount;
    @SerializedName(KEY_ACCOUNT_ID)
    private String mAccountId;
    @SerializedName("SMSMessage")
    private String mMessage;

    public UpgradeCheckMessageRequestData(String iKeyAccount, String accountId, String sms) {
        mKeyAccount = iKeyAccount;
        mAccountId = encrypt(accountId);
        mMessage = sms;
    }
}
