package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class UserRegisterRequestData extends RequestData {
    @SerializedName(KEY_ACCOUNT)
    private String mAccount;
    @SerializedName(KEY_ACCOUNT_ID)
    private String mAccountId;
    @SerializedName(KEY_CAR_NO)
    private String mCarNo;

    public UserRegisterRequestData(String account) {
        mAccount = account;
        mAccountId = "";
        mCarNo = "";
    }

    public void setCarNo(String no) {
        mCarNo = encrypt(no);
    }

    public void setAccountId(String id) {
        mAccountId = encrypt(id);
    }
}
