package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class DmsUpgradeRequestData extends RequestData {
    @SerializedName(KEY_ACCOUNT)
    private String mKeyAccount;
    @SerializedName(KEY_ACCOUNT_ID)
    private String mAccountId;
    @SerializedName(KEY_CAR_NO)
    private String mCarNo;
    @SerializedName("mPhone")
    private String mPhone;

    public DmsUpgradeRequestData(String iKeyAccount, String accountId, String carNo, String phone) {
        mKeyAccount = iKeyAccount;
        mAccountId = encrypt(accountId);
        mCarNo = encrypt(carNo);
        mPhone = encrypt(phone);
    }
}
