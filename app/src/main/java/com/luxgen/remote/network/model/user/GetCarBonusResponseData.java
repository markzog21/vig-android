package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class GetCarBonusResponseData extends ResponseData {
    @SerializedName("result")
    private Result mResult;

    public Result getResult() {
        return mResult;
    }

    public class Result {
        @SerializedName("points")
        private int mPoints;

        public int getPoints() {
            return mPoints;
        }
    }
}
