package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class GetProcedureRecordbyAPPRequestData extends RequestData {

    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName("prcedureID")
    private String mPrcedureID;

    public GetProcedureRecordbyAPPRequestData(String iKeyUid, String iKeyToken, String procedureId) {
        mKeyUid = iKeyUid;
        mKeyToken = iKeyToken;
        mPrcedureID = procedureId;
    }
}
