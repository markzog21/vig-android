package com.luxgen.remote.network.model.maintenance;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class GetRepairRequestData extends RequestData {

    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName(KEY_CAR_NO)
    private String mCarNo;

    /**
     * 參數皆必填 <br />
     */
    public GetRepairRequestData(String iKeyUid, String iKeyToken, String carNo) {
        mKeyUid = iKeyUid;
        mKeyToken = iKeyToken;
        if (!TextUtils.isEmpty(carNo)) {
            mCarNo = carNo.replace("-", "");
        }
    }
}
