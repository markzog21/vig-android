package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;

public class GetRepairHistoryRequestData extends CarRepairRequestData {

    @SerializedName("qBeginDate")
    private String mBegin;
    @SerializedName("qEndDate")
    private String mEnd;

    /**
     * @param iKeyUid
     * @param iKeyToken
     * @param carNo
     * @param beginDate  查詢開始日期 YYYY/01/01
     * @param endDate    查詢結束日期 YYYY/12/31 ，一次查詢一年
     */
    public GetRepairHistoryRequestData(String iKeyUid, String iKeyToken, String carNo,
                                       String beginDate, String endDate) {
        super(iKeyUid, iKeyToken, carNo);
        mBegin = beginDate;
        mEnd = endDate;
    }
}
