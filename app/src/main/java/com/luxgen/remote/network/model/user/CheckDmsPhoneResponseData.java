package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class CheckDmsPhoneResponseData extends ResponseData {
    //服務專員ID,若驗證成功此欄位請移除
    @SerializedName("contactUserID")
    //服務專員名稱,若驗證成功此欄位請移除
    private String mContactId;
    @SerializedName("contactUserName")
    private String mContactName;
    //服務專員電話,若驗證成功此欄位請移除
    @SerializedName("contactUserPhoneNo")
    private String mContactPhone;

    public String getName() {
        return mContactName;
    }

    public String getPhone() {
        return mContactPhone;
    }
}
