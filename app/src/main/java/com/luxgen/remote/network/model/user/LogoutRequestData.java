package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class LogoutRequestData extends RequestData {
    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;

    public LogoutRequestData(String keyUid) {
        mKeyUid = keyUid;
    }
}
