package com.luxgen.remote.network.model.soap;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;

@Root(name = "soap:Envelope", strict = false)
@NamespaceList({
        @Namespace(prefix = "xsi", reference = "http://www.w3.org/2001/XMLSchema-instance"),
        @Namespace(prefix = "xsd", reference = "http://www.w3.org/2001/XMLSchema"),
        @Namespace(prefix = "soap", reference = "http://schemas.xmlsoap.org/soap/envelope/")
})
public class ResponseEnvelope {

    @Element(name = "Body", required = false)
    private RescueResponseBody body;

    public RescueResponseBody getBody() {
        return body;
    }

    public void setBody(RescueResponseBody body) {
        this.body = body;
    }

    public boolean isSuccess() {
        if (getBody() != null && getBody().getRescueResponseData() != null) {
            return getBody().getRescueResponseData().isSuccess();
        }
        return false;
    }
}
