package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;

public class SetCarRepairPeriodRequestData extends CarRepairRequestData {
    @SerializedName("bookingDate")
    private String mBookingDate;
    @SerializedName(KEY_DEALER_CODE)
    private String mDealerCode;
    @SerializedName(KEY_DEPT_CODE)
    private String mDeptCode;
    @SerializedName("odo")
    private String mOdo;
    @SerializedName("maintainMile")
    private String mMaintainMile;
    @SerializedName("maintainItem")
    private String mMaintainItem;
    @SerializedName("others")
    private String mOther;
    @SerializedName("notes")
    private String mNotes;

    /**
     * @param iKeyUid
     * @param iKeyToken
     * @param carNo       ˊ車牌號碼
     * @param bookingDate 預約日期與時段，YYYY/MM/DD HH:MM
     * @param dealerCode  經銷商代碼
     * @param deptCode    保養廠代碼
     */
    public SetCarRepairPeriodRequestData(
            String iKeyUid, String iKeyToken,
            String carNo, String bookingDate,
            String dealerCode, String deptCode
    ) {
        super(iKeyUid, iKeyToken, carNo);
        mBookingDate = bookingDate;
        mDealerCode = dealerCode;
        mDeptCode = deptCode;
        mOdo = "";
        mMaintainMile = "";
        mMaintainItem = "";
        mOther = "";
        mNotes = "";
    }

    /**
     * @param odo 里程，非必填
     */
    public void setOdo(String odo) {
        mOdo = odo;
    }

    /**
     * @param mile 定保，非必填
     */
    public void setMile(String mile) {
        mMaintainMile = mile;
    }

    /**
     * @param item 維修，非必填
     */
    public void setItem(String item) {
        mMaintainItem = item;
    }

    /**
     * @param other 其他，非必填
     */
    public void setOther(String other) {
        mOther = other;
    }

    /**
     * @param notes 備註，非必填
     */
    public void setNotes(String notes) {
        mNotes = notes;
    }
}
