package com.luxgen.remote.network.model.ikey;

import com.google.gson.annotations.SerializedName;

public class ActionData {
    @SerializedName("actionId")
    private String mId;
    @SerializedName("command")
    private String mCommand;
    @SerializedName("commandValue")
    private String mCommandValue;
    @SerializedName("timestamp")
    private String mTimeStamp;
    @SerializedName("returnCode")
    private int mReturnCode;
    @SerializedName("returnValue")
    private String mReturnValue;
    @SerializedName("carProfile")
    private CarProfileData mCarProfile;

    public void setTimeStamp(String timeStamp) {
        mTimeStamp = timeStamp;
    }

    public String getId() {
        return mId;
    }

    public int getReturnCode() {
        return mReturnCode;
    }

    public String getReturnValue() {
        return mReturnValue;
    }

    public void setCarProfile(CarProfileData carProfileData) {
        mCarProfile = carProfileData;
    }

    public void setCommandValue(String commandValue) {
        mCommandValue = commandValue;
    }

    /**
     * response 獲得
     * */
    public void setId(String id) {
        mId = id;
    }

    /**
     * action id 會在呼叫 iKey Server 的 login 後的 response 獲得
     */
    public ActionData(String command) {
        mId = "";
        mCommand = command;
        mCommandValue = "";
        mTimeStamp = "";
    }
}
