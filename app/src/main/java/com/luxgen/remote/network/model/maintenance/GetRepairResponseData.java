package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class GetRepairResponseData extends ResponseData {
    @SerializedName("mileData")
    private String mMileData;
    @SerializedName("county")
    private String mCounty;

    private String[] mCounties;

    /**
     * 限VIG才有此資料，若非VIG則保留空白(非0)
     */
    public String getMile() {
        if (mMileData != null && mMileData.endsWith(".0")) {
            mMileData = mMileData.replace(".0", "");
        }
        return mMileData;
    }

    /**
     * 將取得的城市資料依「、」轉成 String[]
     */
    public String[] getCountries() {
        if (mCounties == null) {
            if (mCounty == null) {
                mCounties = new String[]{};
            } else {
                mCounties = mCounty.split(",");
            }
        }
        return mCounties;
    }

    /**
     * 將收至的 county 直接給出
     */
    public String getCountry() {
        return mCounty;
    }
}
