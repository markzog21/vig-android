package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

import java.util.ArrayList;

public class GetSpeedLimitStatusRequestData extends RequestData {
    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName(KEY_VIGCAR_ID)
    private String mVigCars;

    /**
     * 查詢是否限速 <br />
     * 因為 API 可以查詢多筆，所以這邊請擇一使用 <br />
     * 多筆用 {@link #setCarIds(ArrayList)} <br />
     * 單筆請用 {@link #setCarId(String)}
     */
    public GetSpeedLimitStatusRequestData(String uid, String token) {
        mKeyUid = uid;
        mKeyToken = token;
    }

    public void setCarIds(ArrayList<String> ids) {
        StringBuilder temp = new StringBuilder();
        for (int i = 0; i < ids.size(); i++) {
            if (i > 0) {
                temp.append(",");
            }
            temp.append(ids.get(i));
        }
        mVigCars = temp.toString();
    }

    public void setCarId(String id) {
        mVigCars = id;
    }
}
