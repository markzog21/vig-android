package com.luxgen.remote.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.network.ApiInterface.UserInterface;
import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.user.CheckDmsPhoneRequestData;
import com.luxgen.remote.network.model.user.CheckDmsPhoneResponseData;
import com.luxgen.remote.network.model.user.CheckPhoneNoRequestData;
import com.luxgen.remote.network.model.user.CheckSmsRequestData;
import com.luxgen.remote.network.model.user.CheckSmsResponseData;
import com.luxgen.remote.network.model.user.GetCarBonusRequestData;
import com.luxgen.remote.network.model.user.GetCarBonusResponseData;
import com.luxgen.remote.network.model.user.GetIkeyEmployeeDataRequestData;
import com.luxgen.remote.network.model.user.GetIkeyEmployeeDataResponseData;
import com.luxgen.remote.network.model.user.GetIkeyUidRequestData;
import com.luxgen.remote.network.model.user.GetIkeyUidResponseData;
import com.luxgen.remote.network.model.user.GetMasterKeyUserRequestData;
import com.luxgen.remote.network.model.user.GetMasterKeyUserResponseData;
import com.luxgen.remote.network.model.user.GetRegisterTimeRequestData;
import com.luxgen.remote.network.model.user.GetRegisterTimeResponseData;
import com.luxgen.remote.network.model.user.LoginRequestData;
import com.luxgen.remote.network.model.user.LoginResponseData;
import com.luxgen.remote.network.model.user.LogoutRequestData;
import com.luxgen.remote.network.model.user.QueryUserRequestData;
import com.luxgen.remote.network.model.user.QueryUserResponseData;
import com.luxgen.remote.network.model.user.ReSendPasswordRequestData;
import com.luxgen.remote.network.model.user.SetAccountRequestData;
import com.luxgen.remote.network.model.user.SetIkeyUidRequestData;
import com.luxgen.remote.network.model.user.SetPasswordRequestData;
import com.luxgen.remote.network.model.user.UserRegisterRequestData;

import java.io.IOException;
import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserWebApi extends WebApi {
    private final String LOG_TAG = UserWebApi.class.getSimpleName();

    private static WeakReference<UserWebApi> mInstance;

    public static UserWebApi getInstance(Context context) {
        if (mInstance == null || mInstance.get() == null) {
            mInstance = new WeakReference<>(new UserWebApi(context));
        }
        return mInstance.get();
    }

    private UserWebApi(Context context) {
        super(context);
    }

    /**
     * 註冊用 <br />
     * <p>
     * API No.12 註冊帳號與密碼
     *
     * @param data     傳入的物件 <br />
     *                 如果要社群登入記得設定社群參數 <br />
     *                 Google Account{@link SetAccountRequestData#setGoogleAccount(String, String)} <br />
     *                 Facebook Account {@link SetAccountRequestData#setFacebookAccount(String, String)}
     * @param listener 如成功註冊會直接進 {@link OnApiListener#onApiTaskSuccessful(Object)}
     */
    public void setAccount(SetAccountRequestData data
            , final OnApiListener<ResponseData> listener) {

        listener.onPreApiTask();
        getIotRetrofit().create(UserInterface.class)
                .setAccount(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseData> call,
                                           @NonNull Response<ResponseData> response) {
                        onSuccessful(response, listener);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        onFail(LOG_TAG + "setAccount: ", t, listener);
                    }
                });
    }

    /**
     * 確認電話號碼是否存在 <br />
     * API No.13 一般會員電話號碼驗證
     *
     * @param data
     * @param listener
     */
    public void checkPhone(CheckPhoneNoRequestData data, final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();
        getIotRetrofit().create(UserInterface.class)
                .checkPhoneNo(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseData> call,
                                           @NonNull Response<ResponseData> response) {
                        onSuccessful(response, listener);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        onFail(LOG_TAG + "checkPhone: ", t, listener);
                    }
                });
    }

    /**
     * 確認電話號碼是否存在 <br />
     * API No.14 DMS會員註冊
     *
     * @param data     參數皆必埴
     * @param listener
     */
    public void register(UserRegisterRequestData data, final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();
        getIotRetrofit().create(UserInterface.class)
                .register(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseData> call,
                                           @NonNull Response<ResponseData> response) {
                        onSuccessful(response, listener);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        onFail(LOG_TAG + "checkPhone: ", t, listener);
                    }
                });
    }

    /**
     * DMS會員電話號碼驗證 <br />
     * API No.15 DMS會員電話號碼驗證
     *
     * @param data     參數皆必填
     * @param listener 記得確認一下 {@link CheckDmsPhoneResponseData#isSuccessful()} <br />
     *                 若是成功 {@link CheckDmsPhoneResponseData} 中跟服務專員有關的資料皆會有值
     */
    public void checkDmsPhone(CheckDmsPhoneRequestData data, final OnApiListener<CheckDmsPhoneResponseData> listener) {
        listener.onPreApiTask();
        getIotRetrofit().create(UserInterface.class)
                .checkDmsPhone(data)
                .enqueue(new Callback<CheckDmsPhoneResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<CheckDmsPhoneResponseData> call,
                                           @NonNull Response<CheckDmsPhoneResponseData> response) {
                        CheckDmsPhoneResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<CheckDmsPhoneResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("checkDmsPhone", t));
                    }
                });
    }

    /**
     * 驗證簡訊資料 <br />
     * API No.16 驗證簡訊資料(首次註冊驗證電話號碼)
     *
     * @param data     參數皆必填
     * @param listener 記得確認一下 {@link CheckSmsResponseData#isSuccessful()} <br />
     *                 若是成功 {@link CheckSmsResponseData#getUid()} 會有值
     */
    public void checkSms(CheckSmsRequestData data,
                         final OnApiListener<CheckSmsResponseData> listener) {
        listener.onPreApiTask();
        getIotRetrofit().create(UserInterface.class)
                .checkSms(data)
                .enqueue(new Callback<CheckSmsResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<CheckSmsResponseData> call,
                                           @NonNull Response<CheckSmsResponseData> response) {

                        CheckSmsResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<CheckSmsResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("checkSms", t));
                    }
                });
    }

    /**
     * API No.17 取得一般會員資料
     */
    public void getIkeyUid(GetIkeyUidRequestData data,
                           final OnApiListener<GetIkeyUidResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(UserInterface.class)
                .getIkeyUid(data)
                .enqueue(new Callback<GetIkeyUidResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetIkeyUidResponseData> call,
                                           @NonNull Response<GetIkeyUidResponseData> response) {
                        GetIkeyUidResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GetIkeyUidResponseData> call, Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getIkeyUid", t));
                    }
                });
    }

    /**
     * API No.18 修改一般會員資料
     */
    public void setIkeyUid(SetIkeyUidRequestData data,
                           final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(UserInterface.class)
                .setIkeyUid(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                        ResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseData> call, Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("setIkeyUid", t));
                    }
                });
    }

    /**
     * 密碼修改 <br />
     * API No.19 一般會員密碼修改
     */
    public void setPassword(SetPasswordRequestData data,
                            final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(UserInterface.class)
                .setPassword(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseData> call,
                                           @NonNull Response<ResponseData> response) {
                        onSuccessful(response, listener);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        onFail(LOG_TAG + "setPassword: ", t, listener);
                    }
                });
    }

    /**
     * 登入 <br />
     * API No.20 會員登入
     *
     * @param data     有些是系統值，要留意一下看是否能被拿到
     * @param listener 記得確認一下 {@link CheckSmsResponseData#isSuccessful()} <br />
     *                 若是成功，裡面的參數應該都要不為空才是
     */
    public void login(LoginRequestData data, final OnApiListener<LoginResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(UserInterface.class)
                .login(data)
                .enqueue(new Callback<LoginResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<LoginResponseData> call,
                                           @NonNull Response<LoginResponseData> response) {
                        LoginResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<LoginResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("login", t));
                    }
                });
    }

    /**
     * API No.22 取回服專/銷顧通訊錄
     */
    public void getIkeyEmployeeData(GetIkeyEmployeeDataRequestData data,
                                    final OnApiListener<GetIkeyEmployeeDataResponseData> listener) {
        listener.onPreApiTask();

        getIotRetrofit().create(UserInterface.class)
                .getIkeyEmployeeData(data)
                .enqueue(new Callback<GetIkeyEmployeeDataResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetIkeyEmployeeDataResponseData> call,
                                           @NonNull Response<GetIkeyEmployeeDataResponseData> response) {
                        GetIkeyEmployeeDataResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GetIkeyEmployeeDataResponseData> call, Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getIkeyEmployeeData", t));
                    }
                });
    }

    /**
     * 重送密碼 <br />
     * API No.27 重送密碼
     *
     * @param data     皆必填
     * @param listener 如成功會直接進 {@link OnApiListener#onApiTaskSuccessful(Object)}
     */
    public void reSendPassword(ReSendPasswordRequestData data,
                               final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();
        getIotRetrofit().create(UserInterface.class)
                .reSendPassword(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseData> call,
                                           @NonNull Response<ResponseData> response) {
                        onSuccessful(response, listener);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        onFail(LOG_TAG + "reSendPassword: ", t, listener);
                    }
                });
    }

    /**
     * API No.29 取得用戶註冊時間
     */
    public void getRegisterTime(GetRegisterTimeRequestData data,
                                final OnApiListener<GetRegisterTimeResponseData> listener) {
        listener.onPreApiTask();
        getIotRetrofit().create(UserInterface.class)
                .getRegisterTime(data)
                .enqueue(new Callback<GetRegisterTimeResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetRegisterTimeResponseData> call,
                                           @NonNull Response<GetRegisterTimeResponseData> response) {
                        GetRegisterTimeResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetRegisterTimeResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getRegisterTime", t));
                    }
                });
    }

    /**
     * API No.36 查詢用戶身分
     */
    public void getUserData(QueryUserRequestData data,
                            final OnApiListener<QueryUserResponseData> listener) {
        listener.onPreApiTask();
        getIotRetrofit().create(UserInterface.class)
                .getUserData(data)
                .enqueue(new Callback<QueryUserResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<QueryUserResponseData> call,
                                           @NonNull Response<QueryUserResponseData> response) {
                        QueryUserResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<QueryUserResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getUserData", t));
                    }
                });
    }

    /**
     * API No.41 會員登出
     */
    public void logout(LogoutRequestData data,
                       final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();
        getIotRetrofit().create(UserInterface.class)
                .logout(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseData> call,
                                           @NonNull Response<ResponseData> response) {
                        String error = getDefaultErrorMessage();
                        if (response.errorBody() != null) {
                            try {
                                final String err = response.errorBody().string();
                                error = ResponseData.parseResponseBy(err).getErrorMessage();
                            } catch (IOException e) {
                                error = e.getMessage();
                            }
                        } else if (response.body() != null) {
                            ResponseData data = response.body();
                            if (data.isSuccessful()) {
                                listener.onApiTaskSuccessful(data);
                                return;
                            } else if (!TextUtils.isEmpty(data.getErrorMessage())) {
                                error = data.getErrorMessage();
                            }
                        }

                        if (TextUtils.isEmpty(error)) {
                            error = getDefaultErrorMessage();
                        }
                        listener.onApiTaskFailure(error);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        onFail(LOG_TAG + "logout: ", t, listener);
                    }
                });
    }

    /**
     * 確認帳號是否存在 <br />
     * API No.57 檢核是否有重複註冊的帳號
     *
     * @param data     物件只要記得有放入帳號即可 {@link SetAccountRequestData#setAccount(String)}
     * @param listener 如成功註冊會直接進 {@link OnApiListener#onApiTaskSuccessful(Object)}
     */
    public void checkRepeat(SetAccountRequestData data
            , final OnApiListener<ResponseData> listener) {
        listener.onPreApiTask();
        getIotRetrofit().create(UserInterface.class)
                .checkRepeat(data)
                .enqueue(new Callback<ResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseData> call,
                                           @NonNull Response<ResponseData> response) {
                        onSuccessful(response, listener);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseData> call,
                                          @NonNull Throwable t) {
                        onFail(LOG_TAG + "checkRepeat: ", t, listener);
                    }
                });
    }

    /**
     * API No.76 查詢車主身分
     */
    public void getMasterKeyUserData(GetMasterKeyUserRequestData data,
                                     final OnApiListener<GetMasterKeyUserResponseData> listener) {
        listener.onPreApiTask();
        getIotRetrofit().create(UserInterface.class)
                .getMasterKeyUserData(data)
                .enqueue(new Callback<GetMasterKeyUserResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetMasterKeyUserResponseData> call,
                                           @NonNull Response<GetMasterKeyUserResponseData> response) {
                        GetMasterKeyUserResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetMasterKeyUserResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getMasterKeyUserData", t));
                    }
                });
    }

    /**
     * #A01082 查詢車輛紅利點數
     */
    public void getCarBonus(GetCarBonusRequestData data,
                            final OnApiListener<GetCarBonusResponseData> listener) {
        listener.onPreApiTask();
        getIotRetrofit().create(UserInterface.class)
                .getCarBonus(data)
                .enqueue(new Callback<GetCarBonusResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetCarBonusResponseData> call,
                                           @NonNull Response<GetCarBonusResponseData> response) {
                        GetCarBonusResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetCarBonusResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("getCarBonus", t));
                    }
                });
    }
}
