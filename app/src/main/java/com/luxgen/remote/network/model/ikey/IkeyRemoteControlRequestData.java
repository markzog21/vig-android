package com.luxgen.remote.network.model.ikey;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

import org.json.JSONException;
import org.json.JSONObject;

import tw.com.haitec.ae.project.smartphoneikey.util.Base64;

public class IkeyRemoteControlRequestData extends RequestData {

    @SerializedName("appSecret")
    private String mAppSecret;
    @SerializedName("loginSetting")
    private IkeyLoginSettingData mLoginSetting;
    @SerializedName("action")
    private ActionData mAction;

    public IkeyRemoteControlRequestData(
            int actionId,
            String command,
            String value,
            String timeStamp,
            String appSecret,
            CarProfileData carProfileData,
            IkeyLoginSettingData loginSetting
    ) {
        mAction = new ActionData("remoteControl");
        mAction.setCommandValue(getBase64EncodedRemoteControlCommand(command, value));
        mAction.setId(String.valueOf(actionId));
        mAction.setTimeStamp(timeStamp);
        mAction.setCarProfile(carProfileData);

        mAppSecret = appSecret;
        mLoginSetting = loginSetting;
    }

    private String getBase64EncodedRemoteControlCommand(String command, String value) {
        JSONObject jsonObject = new JSONObject();
        String encodedString = "";
        try {
            jsonObject.put(command, value);
            encodedString = Base64.encodeBytes(jsonObject.toString().getBytes());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return encodedString;
    }
}
