package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class UpgradeCheckMessageResponseData extends ResponseData {
    @SerializedName("UID")
    private String mUid;

    public String getUid() {
        return mUid;
    }
}
