package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class GetWorkShopCapacityRequestData extends RequestData {
    @SerializedName(KEY_DEALER_CODE)
    private String mDealerCode;
    @SerializedName(KEY_DEPT_CODE)
    private String mDeptCode;
    @SerializedName("dateFrom")
    private String mDateFrom;
    @SerializedName("dateTo")
    private String mDateTo;

    /**
     * @param dealerCode    經銷商代碼
     * @param deptCode      保養廠代碼
     * @param from          查詢日 (起)
     * @param to            查詢日 (迄)
     * */
    public GetWorkShopCapacityRequestData(String dealerCode, String deptCode,
                                          String from, String to) {
        mDealerCode = dealerCode;
        mDeptCode = deptCode;
        mDateFrom = from;
        mDateTo = to;
    }
}
