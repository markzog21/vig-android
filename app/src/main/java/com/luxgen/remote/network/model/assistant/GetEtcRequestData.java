package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.VigRequestData;

public class GetEtcRequestData extends VigRequestData {
    @SerializedName(KEY_CAR_NO)
    private String mCarNo;
    @SerializedName("UserID")
    private String mUserId;

    /**
     * 要留一點，如果目前使用者，非該車的擁有者，
     * 請記得再使用 {@link #setUserId(String)} 傳入身分證字號
     */
    public GetEtcRequestData(String iKeyUid, String iKeyToken, String carId) {
        super(iKeyUid, iKeyToken, carId);
    }

    /**
     * 身分證，非該車擁有者為必填
     */
    public void setUserId(String id) {
        mUserId = id;
    }

    public void setCarNo(String carNo) {
        mCarNo = carNo;
    }
}
