package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class GetWeatherRequestData extends RequestData {
    @SerializedName("county")
    private String mCounty;

    public GetWeatherRequestData(String county) {
        mCounty = county;
    }
}
