package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class CheckPasswordRequestData extends RequestData {
    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_PASSWORD)
    private String mPassword;

    public CheckPasswordRequestData(String keyUid, String password) {
        mKeyUid = keyUid;
        mPassword = encrypt(password);
    }
}
