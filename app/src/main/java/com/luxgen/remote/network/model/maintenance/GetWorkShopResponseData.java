package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

import java.util.ArrayList;

public class GetWorkShopResponseData extends ResponseData {
    @SerializedName("workShopList")
    private ArrayList<WorkShopData> mList;

    public ArrayList<WorkShopData> getList() {
        if (mList == null) {
            mList = new ArrayList<>();
        }
        return mList;
    }

    public int getSize() {
        if (mList == null) {
            return 0;
        }

        return mList.size();
    }
}
