package com.luxgen.remote.network.model.maintenance;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class RepairItem implements Parcelable {
    @SerializedName("articleType")
    private String mType;
    @SerializedName("item")
    private String mItem;

    private RepairItem(Parcel in) {
        mType = in.readString();
        mItem = in.readString();
    }

    public static final Creator<RepairItem> CREATOR = new Creator<RepairItem>() {
        @Override
        public RepairItem createFromParcel(Parcel in) {
            return new RepairItem(in);
        }

        @Override
        public RepairItem[] newArray(int size) {
            return new RepairItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mType);
        dest.writeString(mItem);
    }

    /**
     * 保養類別
     * */
    public String getType() {
        return mType;
    }

    /**
     * 保養項目
     * */
    public String getItem() {
        return mItem;
    }
}
