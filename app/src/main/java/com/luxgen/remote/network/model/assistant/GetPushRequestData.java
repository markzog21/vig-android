package com.luxgen.remote.network.model.assistant;

import android.annotation.SuppressLint;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class GetPushRequestData extends RequestData {
    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName("qStartDate")
    private String mStartData;
    @SerializedName("qEndDate")
    private String mEndData;

    /**
     * 取得推播列表資料，
     * 取得的時間區於於初始化時，即設定以當下後的一天之間
     * */
    public GetPushRequestData(String iKeyUid, String iKeyToken) {
        mKeyUid = iKeyUid;
        mKeyToken = iKeyToken;
        setTime();
    }

    private void setTime() {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        final Date current = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(current);
        calendar.add(Calendar.DATE, 1);
        mEndData = sdf.format(calendar.getTime());
        mStartData = "2017/01/01 00:00:00";
    }
}
