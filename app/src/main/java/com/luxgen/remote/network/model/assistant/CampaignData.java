package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;

public class CampaignData {
    @SerializedName("campaignUUID")
    private String mUuid;
    @SerializedName("URL")
    private String mUrl;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("bannerURL")
    private String mBannerUrl;
    @SerializedName("beginDate")
    private String mBeginDate;
    @SerializedName("endDate")
    private String mEndDate;

    public String getUuid() {
        return mUuid;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getBannerUrl() {
        return mBannerUrl;
    }

    public String getBeginDate() {
        return mBeginDate;
    }

    public String getEndDate() {
        return mEndDate;
    }

}
