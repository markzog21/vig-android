package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.model.option.CarOptions;

public class CarWarnLevelA {
    @SerializedName("errLowFuelLED")
    private String mLowFuel = "-1";
    @SerializedName("errBatLowWarn")
    private String mBatLow = "-1";
    @SerializedName("errCharge")
    private String mBatCharge = "-1";
    @SerializedName("errOilPressureLED")
    private String mOilPressureLed = "-1";
    @SerializedName("errLowBrakeFluidSta")
    private String mLowBrakeFluidSta = "-1";
    @SerializedName("errLVCANNERR")
    private String mLvCann = "-1";
    @SerializedName("errCANETCUSVL")
    private String mCanecuSvl = "-1";
    @SerializedName("errCANETCUSTATE")
    private String mCanecuState = "-1";
    @SerializedName("errABSFailedABS")
    private String mAbs = "-1";
    @SerializedName("errSRSLED")
    private String mSrsLed = "-1";
    @SerializedName("errEPBMalDisp")
    private String mEpb = "-1";
    @SerializedName("errEXwheelPressureStatusRF")
    private String mExWheelPressureRf = "-1";
    @SerializedName("errEXwheelPressureStatusLF")
    private String mExWheelPressureLf = "-1";
    @SerializedName("errEXwheelPressureStatusRR")
    private String mExWheelPressureRr = "-1";
    @SerializedName("errEXwheelPressureStatusLR")
    private String mExWheelPressureLR = "-1";
    @SerializedName("errALRTTEMPEAU")
    private String mAlrtTempEau = "-1";
    @SerializedName("errLowSOCWarn")
    private String mLowSoc = "-1";
    @SerializedName("errEpasFailed")
    private String mEpas = "-1";
    @SerializedName("errESCLCRITICALFAIL")
    private String mEsclCritical = "-1";
    @SerializedName("errESCLFUNCTIONFAIL")
    private String mEsclFunction = "-1";
    @SerializedName("errROTATESTEERWHEEL")
    private String mRota = "-1";
    @SerializedName("errABMAirBagFailCmd")
    private String mAbmAirBag = "-1";
    @SerializedName("errairBagFailCmd")
    private String mAirBagFailCmd = "-1";
    @SerializedName("errCANTCOSTA")
    private String mCantcosta = "-1";

    /**
     * 油量過低
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} ON
     * {@link CarOptions.CarStatus#ABNORMAL_1} OFF
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getLowFuelStatus() {
        return CarOptions.getErrStatus(mLowFuel);
    }

    /**
     * 電瓶異常低電量
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} ON
     * {@link CarOptions.CarStatus#ABNORMAL_1} OFF
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getBatLowStatus() {
        return CarOptions.getErrStatus(mBatLow);
    }

    /**
     * 電瓶充電異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} ON
     * {@link CarOptions.CarStatus#ABNORMAL_1} OFF
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getBatChargeStatus() {
        return CarOptions.getErrStatus(mBatCharge);
    }

    /**
     * 機油壓力異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getOilPressureLedStatus() {
        return CarOptions.getErrStatus(mOilPressureLed);
    }

    /**
     * 剎車油異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getLowBrakeFluidStaStatus() {
        return CarOptions.getErrStatus(mLowBrakeFluidSta);
    }

    /**
     * 引擎發動機異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getLvCannStatus() {
        return CarOptions.getErrStatus(mLvCann);
    }

    /**
     * 變速箱異常1
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getCanecuSvlStatus() {
        return CarOptions.getErrStatus(mCanecuSvl);
    }

    /**
     * 變速箱異常2
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getCanecuStateStatus() {
        return CarOptions.getErrStatus(mCanecuState);
    }

    /**
     * ABS異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getAbsStatus() {
        return CarOptions.getErrStatus(mAbs);
    }

    /**
     * 安全氣囊異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getSrsLedStatus() {
        return CarOptions.getErrStatus(mSrsLed);
    }

    /**
     * EPB異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getEpbStatus() {
        return CarOptions.getErrStatus(mEpb);
    }

    /**
     * 引擎水溫異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getAlrtTempEauStatus() {
        return CarOptions.getErrStatus(mAlrtTempEau);
    }

    /**
     * 右前胎壓過高/低異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getExWheelPressureRfStatus() {
        return CarOptions.getErrStatus(mExWheelPressureRf);
    }

    /**
     * 左前胎壓過高/低異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getExWheelPressureLfStatus() {
        return CarOptions.getErrStatus(mExWheelPressureLf);
    }

    /**
     * 右後胎壓過高/低異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getExWheelPressureRrStatus() {
        return CarOptions.getErrStatus(mExWheelPressureRr);
    }

    /**
     * 左後胎壓過高/低異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getExWheelPressureLrStatus() {
        return CarOptions.getErrStatus(mExWheelPressureLR);
    }


    /**
     * 電動車低電量/及低電量提示
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 低電量
     * {@link CarOptions.CarStatus#ABNORMAL_2} 極低電量
     */
    public @CarOptions.CarStatus
    int getLowSocStatus() {
        return CarOptions.getLowSocStatus(mLowSoc);
    }

    /**
     * 動力方向盤異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getEpasStatus() {
        return CarOptions.getErrStatus(mEpas);
    }

    /**
     * ESCL錯誤:嚴重
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getEsclCriticalStatus() {
        return CarOptions.getErrStatus(mEsclCritical);
    }

    /**
     * ESCL錯誤:中等
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getEsclFunctionStatus() {
        return CarOptions.getErrStatus(mEsclFunction);
    }

    /**
     * ESCL錯誤:異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getRotateSteerWheelStatus() {
        return CarOptions.getErrStatus(mRota);
    }

    /**
     * 安全氣囊異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getAbmAirBagStatus() {
        return CarOptions.getErrStatus(mAbmAirBag);
    }

    /**
     * 安全氣囊異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getAirBagFailCmd() {
        return CarOptions.getErrStatus(mAirBagFailCmd);
    }

    /**
     * 引擎冷卻過熱
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     * {@link CarOptions.CarStatus#ABNORMAL_2} 沒有狀態
     */
    public @CarOptions.CarStatus
    int getCantcosta() {
        return CarOptions.getErrStatus(mCantcosta);
    }
}
