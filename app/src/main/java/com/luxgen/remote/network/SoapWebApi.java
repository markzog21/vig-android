package com.luxgen.remote.network;

import android.content.Context;

import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.network.ApiInterface.SoapInterface;
import com.luxgen.remote.network.model.soap.RequestEnvelope;
import com.luxgen.remote.network.model.soap.ResponseEnvelope;

import java.io.IOException;
import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SoapWebApi extends WebApi {

    private final String LOG_TAG = SoapWebApi.class.getSimpleName();

    private static WeakReference<SoapWebApi> mInstance;

    public static SoapWebApi getInstance(Context context) {
        if (mInstance == null || mInstance.get() == null) {
            mInstance = new WeakReference<>(new SoapWebApi(context));
        }

        return mInstance.get();
    }

    private SoapWebApi(Context context) {
        super(context);
    }

    public void uploadRescueData(RequestEnvelope data,
                                 final OnApiListener<ResponseEnvelope> listener) {
        listener.onPreApiTask();

        getSoapRetrofit().create(SoapInterface.class)
                .uploadRescueData(data)
                .enqueue(new Callback<ResponseEnvelope>() {
                    @Override
                    public void onResponse(Call<ResponseEnvelope> call, Response<ResponseEnvelope> response) {
                        ResponseEnvelope data = response.body();
                        if (data != null) {
                            listener.onApiTaskSuccessful(data);
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseEnvelope> call, Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("uploadRescueData", t));
                    }
                });
    }
}
