package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.model.option.VigOptions;

public class CarControlLogData {

    @SerializedName("controlCommand")
    private String mControlCommand;
    @SerializedName("controlDateTime")
    private String mControlDateTime;
    @SerializedName("controlResult")
    private String mControlResult;
    @SerializedName("controlType")
    private String mControlType;

    public String getControlCommand() {
        return mControlCommand;
    }

    public String getControlTime() {
        return mControlDateTime;
    }

    public @VigOptions.RemoteControlStatus
    String getControlResult() {
        return mControlResult;
    }

    public @VigOptions.RemoteControlType
    String getControlType() {
        return mControlType;
    }
}
