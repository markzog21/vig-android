package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;

public class UserDeviceData {
    @SerializedName("ikeyUID")
    private String mKeyUid;
    @SerializedName("ikeyAccount")
    private String mKeyAccount;
    @SerializedName("phoneNO")
    private String mPhone;
    @SerializedName("deviceID")
    private String mDeviceID;
    @SerializedName("name")
    private String mName;
    @SerializedName("deviceCreateTime")
    private long mDeviceCreateTime;

    public String getName() {
        return mName;
    }

    public String getKeyUid() {
        return mKeyUid;
    }

    public String getKeyAccount() {
        return mKeyAccount;
    }

    public String getDeviceId() {
        return mDeviceID;
    }

    public String getPhone() {
        return mPhone;
    }

    public long getDeviceCreateTime() {
        return mDeviceCreateTime;
    }
}
