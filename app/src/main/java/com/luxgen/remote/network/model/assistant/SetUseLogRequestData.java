package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

import java.util.ArrayList;

public class SetUseLogRequestData extends RequestData {
    @SerializedName("sysID")
    private String mId;
    @SerializedName("deviceType")
    private String mType;
    @SerializedName("token")
    private String mToken;
    @SerializedName(KEY_IKEY_UID)
    private String mUid;
    @SerializedName("dataList")
    private ArrayList<UseLogData> mList;

    public SetUseLogRequestData(String iKeyToken, String iKeyUid) {
        mId = "Luxgen";
        mType = "android";
        mToken = iKeyToken;
        mUid = iKeyUid;
        mList = new ArrayList<>();
    }

    public void setLogData(UseLogData data) {
        mList.add(data);
    }
}
