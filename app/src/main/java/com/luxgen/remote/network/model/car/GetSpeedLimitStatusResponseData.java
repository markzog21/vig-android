package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

import java.util.ArrayList;

public class GetSpeedLimitStatusResponseData extends ResponseData {
    @SerializedName("speedLimit")
    private ArrayList<SpeedLimitStatusData> mList;

    public ArrayList<SpeedLimitStatusData> getList() {
        if (mList == null) {
            mList = new ArrayList<>();
        }
        return mList;
    }

    public int getSize() {
        return mList == null ? 0 : mList.size();
    }
}
