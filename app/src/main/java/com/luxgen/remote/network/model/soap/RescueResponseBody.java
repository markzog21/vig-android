package com.luxgen.remote.network.model.soap;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "Body", strict = false)
public class RescueResponseBody {

    @Element(name = "ws_RSS_AppInsCaseResponse", required = false)
    private RescueResponseData mRescueResponseData;

    public RescueResponseData getRescueResponseData() {
        return mRescueResponseData;
    }

    public void setRescueResponseData(RescueResponseData rescueResponseData) {
        this.mRescueResponseData = rescueResponseData;
    }
}
