package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class CheckPhoneNoRequestData extends RequestData {
    @SerializedName(KEY_ACCOUNT)
    private String mAccount;
    @SerializedName("ikeyPhone")
    private String mPhone;

    public CheckPhoneNoRequestData(String account) {
        mAccount = account;
        mPhone = "";
    }

    public void setPhone(String phone) {
        mPhone = encrypt(phone);
    }

    public String getPhone() {
        return decrypt(mPhone);
    }
}
