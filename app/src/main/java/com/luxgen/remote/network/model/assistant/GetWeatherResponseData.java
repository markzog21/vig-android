package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class GetWeatherResponseData extends ResponseData {
    @SerializedName("weatherFactor")
    private String mWeather;

    @SerializedName("temperature")
    private double mTemperature = -273.15;

    public String getWeatherFactor() {
        return mWeather;
    }

    public String getTemperature() {
        return mTemperature == -273.15 ? "" : String.valueOf(mTemperature);
    }
}
