package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class GetEtcResponseData extends ResponseData {
    @SerializedName("avaBalance")
    private int mValue = 0;

    /**
     * 餘額
     */
    public int getAvaBalance() {
        return mValue;
    }
}
