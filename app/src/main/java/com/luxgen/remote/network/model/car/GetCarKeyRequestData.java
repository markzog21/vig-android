package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class GetCarKeyRequestData extends RequestData {
    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;

    public GetCarKeyRequestData(String keyUid, String keyToken) {
        mKeyUid = keyUid;
        mKeyToken = keyToken;
    }
}
