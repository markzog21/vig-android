package com.luxgen.remote.network;

import android.content.Context;
import android.text.TextUtils;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.network.model.ResponseData;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

import static com.luxgen.remote.BuildConfig.SOAP_URL;

abstract class WebApi {

    private final static int READ_TIMEOUT = 30; // seconds
    private final static int READ_TIMEOUT_LONG = 70; // seconds
    private final static int READ_TIMEOUT_VERY_LONG = 90; // seconds
    private WeakReference<Retrofit> mIotRetrofit;
    private WeakReference<Retrofit> mIotRetrofitLongTimeout;
    private WeakReference<Retrofit> mVigRetrofit;
    private WeakReference<Retrofit> mIkeyRetrofit;
    private WeakReference<Retrofit> mSoapRetrofit;

    private String mIotPrefix;
    private String mVigPrefix;
    private String mKeyPrefix;
    private String mServerErrorMessage;
    private String mTimeoutMessage;

    Retrofit getIotRetrofit() {
        if (mIotRetrofit == null || mIotRetrofit.get() == null) {
            mIotRetrofit = new WeakReference<>(initRetrofit(mIotPrefix));
        }
        return mIotRetrofit.get();
    }

    Retrofit getIotRetrofitLongTimeout() {
        if (mIotRetrofitLongTimeout == null || mIotRetrofitLongTimeout.get() == null) {
            mIotRetrofitLongTimeout = new WeakReference<>(initRetrofit(mIotPrefix, READ_TIMEOUT_VERY_LONG));
        }
        return mIotRetrofitLongTimeout.get();
    }

    Retrofit getVigRetrofit() {
        if (mVigRetrofit == null || mVigRetrofit.get() == null) {
            mVigRetrofit = new WeakReference<>(initRetrofit(mVigPrefix));
        }
        return mVigRetrofit.get();
    }

    Retrofit getIkeyRetrofit() {
        if (mIkeyRetrofit == null || mIkeyRetrofit.get() == null) {
            mIkeyRetrofit = new WeakReference<>(initRetrofit(mKeyPrefix, READ_TIMEOUT_LONG));
        }

        return mIkeyRetrofit.get();
    }

    Retrofit getIkeyRetrofit(int readTimeout) {
        if (mIkeyRetrofit == null || mIkeyRetrofit.get() == null) {
            mIkeyRetrofit = new WeakReference<>(initRetrofit(mKeyPrefix, readTimeout));
        }

        return mIkeyRetrofit.get();
    }

    Retrofit getSoapRetrofit() {
        if (mSoapRetrofit == null || mSoapRetrofit.get() == null) {
            mSoapRetrofit = new WeakReference<>(initSoapRetrofit(SOAP_URL));
        }

        return mSoapRetrofit.get();
    }

    WebApi(Context context) {
        mIotPrefix = context.getString(R.string.api_iot_prefix);
        mVigPrefix = context.getString(R.string.api_vig_prefix);
        mKeyPrefix = context.getString(R.string.ikey_prefix);

        mServerErrorMessage = context.getString(R.string.api_server_error);
        mTimeoutMessage = context.getString(R.string.dialog_message_timeout);
    }

    private Retrofit initRetrofit(String baseUrl) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient().newBuilder()
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(logging).build();

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private Retrofit initSoapRetrofit(String baseUrl) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient().newBuilder()
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(logging).build();

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
    }

    private Retrofit initRetrofit(String baseUrl, int timeout) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient().newBuilder()
                .readTimeout(timeout, TimeUnit.SECONDS)
                .addInterceptor(logging).build();

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    void onSuccessful(Response<ResponseData> response, OnApiListener<ResponseData> listener) {
        String error = getDefaultErrorMessage();

        if (response.body() != null) {
            ResponseData data = response.body();
            if (data.isAuthFail()) {
                listener.onAuthFailure();
            } else {
                listener.onApiTaskSuccessful(data);
            }
        } else if (response.errorBody() != null) {
            try {
                final String err = response.errorBody().string();
                error = ResponseData.parseResponseBy(err).getErrorMessage();
            } catch (IOException e) {
                error = e.getMessage();
            }
            if (TextUtils.isEmpty(error)) {
                error = getDefaultErrorMessage();
            }
            listener.onApiTaskFailure(error);
        } else {
            listener.onApiTaskFailure(error);
        }
    }

    void onFail(String tag, Throwable t, OnApiListener<ResponseData> listener) {
        listener.onApiTaskFailure(getFailMessage(tag, t));
    }

    String getFailMessage(String tag, Throwable t) {
        String log = "[" + tag + "]" + t.toString();
        LogCat.d(log);
        return t.toString().contains("timeout") ?
                mTimeoutMessage : log;
    }

    String getDefaultErrorMessage() {
        return mServerErrorMessage;
    }
}
