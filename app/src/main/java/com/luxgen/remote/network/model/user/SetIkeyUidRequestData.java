package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class SetIkeyUidRequestData extends RequestData {
    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName("ikeyEmail")
    private String mEmail;
    @SerializedName("ikeyPhoneNO")
    private String mPhone;

    /**
     * 除了 phone 以外，其他都為必填
     */
    public SetIkeyUidRequestData(String iKeyUid, String iKeyToken
            , String email) {
        mKeyUid = iKeyUid;
        mKeyToken = iKeyToken;
        mEmail = email;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }
}
