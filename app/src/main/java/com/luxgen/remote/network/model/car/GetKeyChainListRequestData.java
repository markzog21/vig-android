package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class GetKeyChainListRequestData extends RequestData {

    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName(KEY_VIGCAR_ID)
    private String mVigCarId;

    public GetKeyChainListRequestData(String iKeyUid, String iKeyToken, String vigCarId) {
        mKeyUid = iKeyUid;
        mKeyToken = iKeyToken;
        mVigCarId = vigCarId;
    }
}
