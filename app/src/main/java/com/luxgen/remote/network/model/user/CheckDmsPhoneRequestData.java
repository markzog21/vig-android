package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class CheckDmsPhoneRequestData extends RequestData {
    @SerializedName(KEY_ACCOUNT)
    private String mAccount;
    @SerializedName(KEY_ACCOUNT_ID)
    private String mAccountId;
    @SerializedName(KEY_CAR_NO)
    private String mCarNo;
    @SerializedName("mPhone")
    private String mPhone;

    public CheckDmsPhoneRequestData(String account) {
        mAccount = account;
        mAccountId = "";
        mCarNo = "";
        mPhone = "";
    }

    public void setCarNo(String no) {
        mCarNo = encrypt(no);
    }

    public void setAccountId(String id) {
        mAccountId = encrypt(id);
    }

    public void setPhone(String phone) {
        mPhone = encrypt(phone);
    }
}
