package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class GetMasterKeyUserResponseData extends ResponseData {
    @SerializedName("name")
    private String mName;
    @SerializedName("status")
    private String mStatus;

    public String getName() {
        return mName;
    }

    public String getStatus() {
        return mStatus;
    }
}
