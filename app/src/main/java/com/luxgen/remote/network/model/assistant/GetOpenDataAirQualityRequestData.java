package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class GetOpenDataAirQualityRequestData extends RequestData {
    @SerializedName("lat")
    private double mLat;
    @SerializedName("lon")
    private double mLon;

    public GetOpenDataAirQualityRequestData(double lat, double lng) {
        mLat = lat;
        mLon = lng;
    }
}
