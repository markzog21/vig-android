package com.luxgen.remote.network.model.soap;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "soap:Body", strict = false)
public class RescueRequestBody {

    @Element(name = "ws_RSS_AppInsCase")
    private RescueRequestData mRescueRequestData;

    public RescueRequestData getRescueRequestData() {
        return mRescueRequestData;
    }

    public void setRescueRequestData(RescueRequestData rescueRequestData) {
        this.mRescueRequestData = rescueRequestData;
    }
}
