package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;

public class CarTrackData {
    @SerializedName("lat")
    private double mLat;
    @SerializedName("lon")
    private double mLon;
    @SerializedName("speed")
    private int mSpeed;
    @SerializedName("timeStamp")
    private String mTime;

    public double getLat() {
        return mLat;
    }

    public double getLon() {
        return mLon;
    }
}
