package com.luxgen.remote.network.model.user;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class EmployeeData extends ResponseData implements Parcelable {

    @SerializedName("preName")
    private String mPreName;
    @SerializedName("name")
    private String mName;
    @SerializedName("employeeType")
    private String mEmployeeType;
    @SerializedName("dept")
    private String mDept;
    @SerializedName("deptCode")
    private String mDeptCode;
    @SerializedName("phoneNo")
    private String mPhone;
    @SerializedName("carNO")
    private String mCarNo;

    /*
     * 被修改前姓名,若非更名或換人員可空白
     */
    public String getPreName() {
        return mPreName;
    }

    /*
     * 修改後姓名
     */
    public String getName() {
        return mName;
    }

    /*
     * S1=銷售顧問, S2=服務專員
     */
    public String getEmployeeType() {
        return mEmployeeType;
    }

    public String getPhone() {
        return mPhone;
    }

    public String getCarNo() {
        return mCarNo;
    }

    public boolean isService() {
        return mEmployeeType.startsWith("S");
    }

    public boolean isAdvisor() {
        return mEmployeeType.startsWith("V");
    }

    private EmployeeData(Parcel in) {
        mPreName = in.readString();
        mName = in.readString();
        mEmployeeType = in.readString();
        mDept = in.readString();
        mDeptCode = in.readString();
        mPhone = in.readString();
        mCarNo = in.readString();
    }

    public static final Creator<EmployeeData> CREATOR = new Creator<EmployeeData>() {
        @Override
        public EmployeeData createFromParcel(Parcel in) {
            return new EmployeeData(in);
        }

        @Override
        public EmployeeData[] newArray(int size) {
            return new EmployeeData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mPreName);
        dest.writeString(mName);
        dest.writeString(mEmployeeType);
        dest.writeString(mDept);
        dest.writeString(mDeptCode);
        dest.writeString(mPhone);
        dest.writeString(mCarNo);
    }
}
