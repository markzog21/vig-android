package com.luxgen.remote.network.model.ikey;

import com.google.gson.annotations.SerializedName;

public class IkeyLoginSettingData {
    @SerializedName("timestamp")
    private String mTimeStamp;
    @SerializedName("token")
    private String mToken;

    public String getTimeStamp() {
        return mTimeStamp;
    }

    public String getToken() {
        return mToken;
    }

    public void setTimeStamp(String timeStamp) {
        mTimeStamp = timeStamp;
    }

    public void setToken(String token) {
        mToken = token;
    }
}
