package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.model.option.NotifactionOptions;

public class SetPushData {
    @SerializedName("pushUUID")
    private String mUuid = "";

    @SerializedName("readStatus")
    @NotifactionOptions.ReadStatus
    private String mReadStatus;

    public void setUuid(String uuid) {
        mUuid = uuid;
    }

    public void setStatus(@NotifactionOptions.ReadStatus String status) {
        mReadStatus = status;
    }

    public String getUuid() {
        return mUuid;
    }
}
