package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class GetWorkShopRequestData extends RequestData {
    @SerializedName("county")
    private String mCountry;

    public GetWorkShopRequestData(String country) {
        mCountry = country;
    }
}
