package com.luxgen.remote.network.ApiInterface;

import com.luxgen.remote.network.model.ikey.IkeyFuncToServerRequestData;
import com.luxgen.remote.network.model.ikey.IkeyFuncToServerResponseData;
import com.luxgen.remote.network.model.ikey.IkeyLoginRequestData;
import com.luxgen.remote.network.model.ikey.IkeyLoginResponseData;
import com.luxgen.remote.network.model.ikey.IkeyRemoteControlRequestData;
import com.luxgen.remote.network.model.ikey.IkeyRemoteControlResponseData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface IkeyInterface {

    // API No.55 ikey gateway login
    @POST("/VIGP1/accountManagement/ikeyLogin")
    Call<IkeyLoginResponseData> login(@Body IkeyLoginRequestData data);

    // API No.56 ikey gateway function to ikey server
    @POST("/VIGP1/accountManagement/ikeyFuncToServer")
    Call<IkeyFuncToServerResponseData> funcToServer(@Body IkeyFuncToServerRequestData data);

    // API No.67 ikey gateway remotecontrol
    @POST("/VIGP1/accountManagement/ikeyRemoteControl")
    Call<IkeyRemoteControlResponseData> remoteControl(@Body IkeyRemoteControlRequestData data);
}
