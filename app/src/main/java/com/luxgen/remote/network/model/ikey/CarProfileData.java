package com.luxgen.remote.network.model.ikey;

import com.google.gson.annotations.SerializedName;

public class CarProfileData {
    @SerializedName("carId")
    private String mCarId;
    @SerializedName("carBrand")
    private String mCarBrand;
    @SerializedName("carName")
    private String mCarName;
    @SerializedName("createTimestamp")
    private String mCreateTimeStamp;

    public void setCreateTimeStamp(String timeStamp) {
        mCreateTimeStamp = timeStamp;
    }

    public void setCarName(String carName) {
        mCarName = carName;
    }

    public void setCarBrand(String carBrand) {
        mCarBrand = carBrand;
    }

    public CarProfileData(String carId) {
        mCarId = carId;
        mCarBrand = "";
        mCarName = "";
        mCreateTimeStamp = "";
    }
}
