package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.model.option.RepairOptions;
import com.luxgen.remote.network.model.ResponseData;

public class GetCarRepairProgressResponseData extends ResponseData {
    @SerializedName("roNO")
    private String mRoNo;
    @SerializedName("bookingNO")
    private String mBookingNo;
    @SerializedName("regoNO")
    private String mCarNo;
    @SerializedName("repairOrderType")
    private String mOrderType;
    @SerializedName("repairOrderTypeID")
    private String mOrderTypeId;
    @SerializedName("dealerCode")
    private String mDealerCode;
    @SerializedName("deptCode")
    private String mDeptCode;
    @SerializedName("dept")
    private String mDept;
    @SerializedName("receptionDate")
    private String mReceptionDate;
    @SerializedName("statusRo")
    private String mStatusRo;
    @SerializedName("accountDate")
    private String mAccountDate;

    /**
     * 工單號碼
     */
    public String getRoNo() {
        return mRoNo;
    }

    /**
     * 工單類別
     */
    public String getOrderType() {
        return mOrderType;
    }

    /**
     * 工單類別號碼
     */
    public String getOrderTypeId() {
        return mOrderTypeId;
    }

    /**
     * 預約號碼
     */
    public String getBookingNo() {
        return mBookingNo;
    }

    /**
     * 車牌號碼
     */
    public String getCarNo() {
        return mCarNo;
    }

    /**
     * 經銷商代碼
     */
    public String getDealerCode() {
        return mDealerCode;
    }

    /**
     * 保養廠代碼
     */
    public String getDeptCode() {
        return mDeptCode;
    }

    /**
     * 保養廠名稱
     */
    public String getDept() {
        return mDept;
    }

    /**
     * 進場日期
     */
    public String getReceptionDate() {
        return mReceptionDate;
    }

    /**
     * 工單狀態
     */
    public @RepairOptions.RepairStatus
    String getStatus() {
        return mStatusRo;
    }

    /**
     * 結帳日期
     */
    public String getAccountDate() {
        return mAccountDate;
    }

    public boolean hasProgress() {
        return mRoNo != null;
    }
}
