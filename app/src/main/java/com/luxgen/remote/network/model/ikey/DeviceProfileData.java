package com.luxgen.remote.network.model.ikey;

import android.os.Build;

import com.google.gson.annotations.SerializedName;

public class DeviceProfileData {
    @SerializedName("deviceId")
    private String mId;
    @SerializedName("deviceType")
    private String mType;
    @SerializedName("osType")
    private String mOsType;
    @SerializedName("osVersion")
    private String mOsVersion;
    @SerializedName("createTimestamp")
    private String mCreateTimeStamp;

    public void setCreateTimeStamp(String timeStamp) {
        mCreateTimeStamp = timeStamp;
    }

    public DeviceProfileData(String deviceId) {
        mId = deviceId;
        mType = "PHONE";
        mOsType = "ANDROID";
        mOsVersion = Build.VERSION.RELEASE;
        mCreateTimeStamp = "";
    }
}
