package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.model.option.CarOptions;
import com.luxgen.remote.network.model.ResponseData;

public class GetProcedureRecordbyAPPResponseData extends ResponseData {

    @SerializedName("deleteMasterResult")
    private String mDeleteMasterResult;

    public @CarOptions.DeleteStatus
    String getDeleteMasterResult() {
        return mDeleteMasterResult;
    }
}
