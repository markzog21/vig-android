package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.model.option.VigOptions;
import com.luxgen.remote.network.model.ResponseData;

public class CheckRemoteControlStartResponseData extends ResponseData {

    @SerializedName("result")
    private String mResult;

    public @VigOptions.RemoteControlStatus
    String getResult() {
        return mResult;
    }
}
