package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class GetCarBonusRequestData extends RequestData {
    @SerializedName("ikeyUid")
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName("carNo")
    private String mCarNo;

    /**
     * @param uId   iKeyUid
     * @param token iKeyToken
     * @param carNo 要查詢車子ID
     */
    public GetCarBonusRequestData(String uId, String token, String carNo) {
        mKeyUid = uId;
        mKeyToken = token;
        mCarNo = carNo;
    }
}
