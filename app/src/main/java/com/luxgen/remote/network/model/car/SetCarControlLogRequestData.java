package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class SetCarControlLogRequestData extends RequestData {
    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName(KEY_CAR_ID)
    private String mCarId;
    @SerializedName(KEY_CAR_NO)
    private String mCarNo;
    @SerializedName(KEY_VIGCAR_ID)
    private String mVigCarId;
    @SerializedName("controlCommand")
    private String mControlCommand;
    @SerializedName("controlDateTime")
    private String mControlDateTime;
    @SerializedName("commandResult")
    private String mCommandResult;
    @SerializedName("controlType")
    private String mControlType;

    public SetCarControlLogRequestData(String keyUid, String keyToken, String carId, String carNo, String vigCarId,
                                       String command, String dateTime) {
        mKeyUid = keyUid;
        mKeyToken = keyToken;
        mCarId = carId;
        mCarNo = encrypt(carNo);
        mVigCarId = vigCarId;
        mControlCommand = command;
        mControlDateTime = dateTime;
    }

    public void setResult(String result) {
        mCommandResult = result;
    }

    public void setType(String type) {
        mControlType = type;
    }
}

