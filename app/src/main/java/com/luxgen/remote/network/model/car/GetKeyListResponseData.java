package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

import java.util.ArrayList;

public class GetKeyListResponseData extends ResponseData {
    @SerializedName("carList")
    private ArrayList<KeyData> mCarList;

    public int size() {
        if (mCarList == null) {
            return 0;
        }
        return mCarList.size();
    }

    public ArrayList<KeyData> getCarList() {
        if (mCarList == null) {
            mCarList = new ArrayList<>();
        }

        return mCarList;
    }
}
