package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

import java.util.ArrayList;

public class GetRepairHistoryResponseData extends ResponseData {
    @SerializedName("repairHistory")
    private ArrayList<RepairHistory> mList;

    public int size() {
        if (mList == null) {
            return 0;
        }
        return mList.size();
    }

    public ArrayList<RepairHistory> getList() {
        if (mList == null) {
            mList = new ArrayList<>();
        }
        return mList;
    }
}
