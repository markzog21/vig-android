package com.luxgen.remote.network.ApiInterface;

import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.VigRequestData;
import com.luxgen.remote.network.model.assistant.CheckRemoteControlStartResponseData;
import com.luxgen.remote.network.model.assistant.GetCampaignResponseData;
import com.luxgen.remote.network.model.assistant.GetEtcRequestData;
import com.luxgen.remote.network.model.assistant.GetEtcResponseData;
import com.luxgen.remote.network.model.assistant.GetInitialDataResponseData;
import com.luxgen.remote.network.model.assistant.GetOpenDataAirQualityRequestData;
import com.luxgen.remote.network.model.assistant.GetOpenDataAirQualityResponseData;
import com.luxgen.remote.network.model.assistant.GetPm25ResponseData;
import com.luxgen.remote.network.model.assistant.GetPushRequestData;
import com.luxgen.remote.network.model.assistant.GetPushResponseData;
import com.luxgen.remote.network.model.assistant.GetWeatherRequestData;
import com.luxgen.remote.network.model.assistant.GetWeatherResponseData;
import com.luxgen.remote.network.model.assistant.SetIkeyFCMTokenRequestData;
import com.luxgen.remote.network.model.assistant.SetPushRequestData;
import com.luxgen.remote.network.model.assistant.SetUseLogRequestData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AssistantInterface {

    // API No.10 取得行銷活動URL，prefix: api_vig_prefix
    @POST("/LuxgenVIG/VIGP1/campaignManagement/getCampaignList")
    Call<GetCampaignResponseData> getCampaignList();

    // API No.21 ikeyFCM Token註冊
    @POST("/VIGP1/accountManager/setIkeyFCMToken")
    Call<ResponseData> setFcmToken(@Body SetIkeyFCMTokenRequestData data);

    // API No.23 取得車主推播通知內容
    @POST("/VIGP1/accountManager/getAllPushList")
    Call<GetPushResponseData> getPushList(@Body GetPushRequestData data);

    // API No.26 開機參數取得
    @POST("/LuxgenVIG/VIGP1/accountManager/getInitialData")
    Call<GetInitialDataResponseData> getInitialData();

    // API No.43 取得PM2.5值，prefix: api_vig_prefix
    @POST("/VIGP1/accountManager/getPM25")
    Call<GetPm25ResponseData> getPm25(@Body VigRequestData data);

    // API No.45 ETC API
    @POST("/VIGP1/accountManager/getETC")
    Call<GetEtcResponseData> getEtc(@Body GetEtcRequestData data);

    // API No.48 取得天氣資訊
    @POST("/VIGP1/accountManager/getWeather")
    Call<GetWeatherResponseData> getWeather(@Body GetWeatherRequestData data);

    // API No.52 取得外部空氣品質
    @POST("/VIGP1/accountManager/getOpenDataAirQuality")
    Call<GetOpenDataAirQualityResponseData> getOpenDataAirQuality(@Body GetOpenDataAirQualityRequestData data);

    // API No.64 檢核是否可以進行遠端發動
    @POST("/VIGP1/accountManager/checkRemoteContolStart")
    Call<CheckRemoteControlStartResponseData> checkRemoteControlStart(@Body VigRequestData data);

    // API No.72 設定推播通知內容狀態
    @POST("/VIGP1/accountManager/setPushStatus")
    Call<ResponseData> setPushStatus(@Body SetPushRequestData data);

    // API No.73 UseLog(撥打電話Log)
    @POST("/VIGP1/accountManager/setUseLog")
    Call<ResponseData> setUseLog(@Body SetUseLogRequestData data);
}
