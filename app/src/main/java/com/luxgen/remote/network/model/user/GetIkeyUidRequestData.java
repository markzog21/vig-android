package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class GetIkeyUidRequestData extends RequestData {
    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;

    /**
     * 參數皆必填
     */
    public GetIkeyUidRequestData(String iKeyUid, String iKeyToken) {
        mKeyUid = iKeyUid;
        mKeyToken = iKeyToken;
    }
}
