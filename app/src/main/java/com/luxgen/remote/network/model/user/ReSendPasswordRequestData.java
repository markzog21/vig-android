package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class ReSendPasswordRequestData extends RequestData {
    @SerializedName(KEY_ACCOUNT)
    private String mAccount;
    @SerializedName("phoneNo")
    private String mPhone;

    public ReSendPasswordRequestData(String account) {
        mAccount = account;
        mPhone = "";
    }

    /**
     * 已驗證過的手機，必填
     */
    public void setPhone(String phone) {
        mPhone = phone;
    }
}
