package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

import java.util.ArrayList;

public class SetBeaconRequestData extends RequestData {
    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName(KEY_VIGCAR_ID)
    private String mVigCarId;
    @SerializedName("beacons")
    private ArrayList<BeaconData> mBeacons;

    public SetBeaconRequestData(String keyUid, String keyToken, String vigCarId, ArrayList<BeaconData> beacons) {
        mKeyUid = keyUid;
        mKeyToken = keyToken;
        mVigCarId = vigCarId;
        mBeacons = beacons;
    }
}

