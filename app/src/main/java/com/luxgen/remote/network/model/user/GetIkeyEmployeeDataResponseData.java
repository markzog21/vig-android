package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

import java.util.ArrayList;

public class GetIkeyEmployeeDataResponseData extends ResponseData {

    @SerializedName("Employees")
    private ArrayList<EmployeeData> mList;

    public int getSize() {
        return mList == null ? 0 : mList.size();
    }

    public ArrayList<EmployeeData> getEmployeeDataList() {
        if (mList == null) {
            mList = new ArrayList<>();
        }
        return mList;
    }
}
