package com.luxgen.remote.network.model.ikey;

import android.util.Base64;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class IkeyRemoteControlResponseData extends ResponseData {
    @SerializedName("loginSetting")
    private IkeyLoginSettingData mLoginSetting;
    @SerializedName("appSecret")
    private String mAppSecret;
    @SerializedName("action")
    private ActionData mAction;

    /**
     * 登入用 token，若登入失敗時回傳空字串。
     */
    public String getToken() {
        if (mLoginSetting == null) {
            return "";
        }

        return mLoginSetting.getToken();
    }

    /**
     * 給 APP 自行辨識用的任意值，會在 Response 中帶回。
     */
    public String getActionId() {
        if (mAction == null) {
            return "";
        }
        return mAction.getId();
    }

    /**
     * 由 iKeyServer 生成要給 App 裡的 CommunicationAgent 的溝通資料，若為空字串則代表沒有資料需要傳送。
     */
    public String getAppSecret() {
        return mAppSecret;
    }

    public String getReturnValue() {
        if (mAction == null) {
            return "";
        }

        String retVal = mAction.getReturnValue();
        if (retVal == null || retVal.length() == 0) {
            return "";
        }

        return new String(Base64.decode(retVal, Base64.NO_WRAP));
    }

    public int getReturnCode() {
        if (mAction == null) {
            return 0;
        }

        return mAction.getReturnCode();
    }

    public boolean isSuccess() {
        if (mAction == null) {
            return false;
        }

        return (mAction.getReturnCode() == 200);
    }
}
