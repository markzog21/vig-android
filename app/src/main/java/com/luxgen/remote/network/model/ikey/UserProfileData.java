package com.luxgen.remote.network.model.ikey;

import com.google.gson.annotations.SerializedName;

public class UserProfileData {
    @SerializedName("userId")
    private String mUserId;
    @SerializedName("alias")
    private String mAlias;
    @SerializedName("createTimestamp")
    private String mCreateTimeStamp;

    public String getUserId() {
        return mUserId;
    }

    public void setCreateTimeStamp(String timeStamp) {
        mCreateTimeStamp = timeStamp;
    }

    public UserProfileData(String userId, String alias) {
        mUserId = userId;
        mAlias = alias;
        mCreateTimeStamp = "";
    }
}
