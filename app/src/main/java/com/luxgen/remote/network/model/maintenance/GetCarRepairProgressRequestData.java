package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class GetCarRepairProgressRequestData extends RequestData {

    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName(KEY_REGO_NO)
    private String mCarNo;

    /**
     * 參數皆必填
     */
    public GetCarRepairProgressRequestData(String iKeyUid, String iKeyToken, String carNo) {
        mKeyUid = iKeyUid;
        mKeyToken = iKeyToken;
        mCarNo = carNo;
    }
}
