package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.VigRequestData;

public class SetBeaconLearingStatusRequestData extends VigRequestData {

    @SerializedName("learingStatus")
    private String mLearingStatus;

    public SetBeaconLearingStatusRequestData(String keyUid, String keyToken, String carId) {
        super(keyUid, keyToken, carId);
        // 固定 ”R”，代表已通知但沒有學習，未來不需要重複提示用戶了。
        mLearingStatus = "R";
    }

    public void setLearningStatus(String status) {
        mLearingStatus = status;
    }
}

