package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

import java.util.ArrayList;

public class GetRemoteControlLogResponseData extends ResponseData {
    @SerializedName("carID")
    private String mCarId;
    @SerializedName("carNO")
    private String mCarNo;
    @SerializedName("carControlLog")
    private ArrayList<CarControlLogData> mList;

    public int size() {
        if (mList == null) {
            return 0;
        }
        return mList.size();
    }

    public ArrayList<CarControlLogData> getList() {
        if (mList == null) {
            mList = new ArrayList<>();
        }
        return mList;
    }
}
