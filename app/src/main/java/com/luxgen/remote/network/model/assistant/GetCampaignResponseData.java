package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

import java.util.ArrayList;

public class GetCampaignResponseData extends ResponseData {
    @SerializedName("campaignList")
    private ArrayList<CampaignData> mList;

    public int getSize() {
        return mList == null ? 0 : mList.size();
    }

    public ArrayList<CampaignData> getCampaignList() {
        if (mList == null) {
            mList = new ArrayList<>();
        }
        return mList;
    }
}
