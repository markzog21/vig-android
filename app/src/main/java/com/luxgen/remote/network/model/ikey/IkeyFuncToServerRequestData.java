package com.luxgen.remote.network.model.ikey;

import com.google.gson.annotations.SerializedName;

public class IkeyFuncToServerRequestData {

    @SerializedName("appSecret")
    private String mAppSecret;
    @SerializedName("loginSetting")
    private IkeyLoginSettingData mLoginSetting;
    @SerializedName("action")
    private ActionData mAction;

    public IkeyFuncToServerRequestData(
            int actionId,
            String appSecret,
            IkeyLoginSettingData loginSetting
    ) {
        mAction = new ActionData("bypass");
        mAction.setId(String.valueOf(actionId));
        mAction.setTimeStamp(String.valueOf(System.currentTimeMillis()));

        mAppSecret = appSecret;
        mLoginSetting = loginSetting;
    }
}
