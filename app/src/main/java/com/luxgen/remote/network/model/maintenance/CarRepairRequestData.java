package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class CarRepairRequestData extends RequestData {

    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName(KEY_REGO_NO)
    private String mCarNo;

    /**
     * 參數皆必填 <br />
     * 用到此物件有: <br />
     * <ul>
     * <li>API No.4 預約保養廠</li>
     * <li>API No.6 刪除預約保養</li>
     * <li>API No.7 查詢保修紀錄</li>
     * </ul>
     */
    public CarRepairRequestData(String iKeyUid, String iKeyToken, String carNo) {
        mKeyUid = iKeyUid;
        mKeyToken = iKeyToken;
        mCarNo = carNo;
    }
}
