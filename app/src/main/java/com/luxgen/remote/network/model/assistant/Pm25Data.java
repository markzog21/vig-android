package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.util.TimeUtils;

import java.text.ParseException;

public class Pm25Data {
    @SerializedName("pm25Range")
    private double mRange;
    @SerializedName("pm25Index")
    private String mIndex;
    // @SerializedName("pm25Time")
    @SerializedName("timeStamp")
    private String mTime;

    public Pm25Data(double range) {
        mRange = range;
        mIndex = "0";
        mTime = "";
    }

    public double getRange() {
        return mRange;
    }

    /**
     * PM 2.5 數值
     */
    public String getIndex() {
        return mIndex;
    }

    /**
     * PM的事件時間點
     */
    public String getTime() {
        return mTime;
    }

    public long getTimeStamp() {
        try {
            return TimeUtils.parseToLong(mTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
