package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class GetRegisterTimeResponseData extends ResponseData {
    @SerializedName("registerTime")
    private String mRegisterTime;

    public String getRegisterTime() {
        return mRegisterTime;
    }
}
