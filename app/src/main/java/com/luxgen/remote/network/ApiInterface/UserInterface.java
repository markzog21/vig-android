package com.luxgen.remote.network.ApiInterface;

import com.luxgen.remote.network.model.ResponseData;
import com.luxgen.remote.network.model.user.CheckDmsPhoneRequestData;
import com.luxgen.remote.network.model.user.CheckDmsPhoneResponseData;
import com.luxgen.remote.network.model.user.CheckPhoneNoRequestData;
import com.luxgen.remote.network.model.user.CheckSmsRequestData;
import com.luxgen.remote.network.model.user.CheckSmsResponseData;
import com.luxgen.remote.network.model.user.GetCarBonusRequestData;
import com.luxgen.remote.network.model.user.GetCarBonusResponseData;
import com.luxgen.remote.network.model.user.GetIkeyEmployeeDataRequestData;
import com.luxgen.remote.network.model.user.GetIkeyEmployeeDataResponseData;
import com.luxgen.remote.network.model.user.GetIkeyUidRequestData;
import com.luxgen.remote.network.model.user.GetIkeyUidResponseData;
import com.luxgen.remote.network.model.user.GetMasterKeyUserRequestData;
import com.luxgen.remote.network.model.user.GetMasterKeyUserResponseData;
import com.luxgen.remote.network.model.user.GetRegisterTimeRequestData;
import com.luxgen.remote.network.model.user.GetRegisterTimeResponseData;
import com.luxgen.remote.network.model.user.LoginRequestData;
import com.luxgen.remote.network.model.user.LoginResponseData;
import com.luxgen.remote.network.model.user.LogoutRequestData;
import com.luxgen.remote.network.model.user.QueryUserRequestData;
import com.luxgen.remote.network.model.user.QueryUserResponseData;
import com.luxgen.remote.network.model.user.ReSendPasswordRequestData;
import com.luxgen.remote.network.model.user.SetAccountRequestData;
import com.luxgen.remote.network.model.user.SetIkeyUidRequestData;
import com.luxgen.remote.network.model.user.SetPasswordRequestData;
import com.luxgen.remote.network.model.user.UserRegisterRequestData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserInterface {

    // API No.12 註冊帳號與密碼
    @POST("/VIGP1/accountManager/setAccount")
    Call<ResponseData> setAccount(@Body SetAccountRequestData data);

    // API No.13 一般會員電話號碼驗證
    @POST("/VIGP1/accountManager/checkPhoneNo")
    Call<ResponseData> checkPhoneNo(@Body CheckPhoneNoRequestData data);

    // API No.14 DMS會員註冊
    @POST("/VIGP1/memberManager/DMSUserRegister")
    Call<ResponseData> register(@Body UserRegisterRequestData data);

    // API No.15 DMS會員電話號碼驗證
    @POST("/VIGP1/memberManager/DMSCheckPhoneNO")
    Call<CheckDmsPhoneResponseData> checkDmsPhone(@Body CheckDmsPhoneRequestData data);

    // API No.16 驗證簡訊資料(首次註冊驗證電話號碼)
    @POST("/VIGP1/accountManager/checkMessage")
    Call<CheckSmsResponseData> checkSms(@Body CheckSmsRequestData data);

    // API No.17 取得一般會員資料
    @POST("/VIGP1/accountManager/getIkeyUID")
    Call<GetIkeyUidResponseData> getIkeyUid(@Body GetIkeyUidRequestData data);

    // API No.18 修改一般會員資料
    @POST("/VIGP1/accountManager/setIkeyUID")
    Call<ResponseData> setIkeyUid(@Body SetIkeyUidRequestData data);

    // API No.19 一般會員密碼修改
    @POST("/VIGP1/accountManager/setIkeyPassword")
    Call<ResponseData> setPassword(@Body SetPasswordRequestData data);

    // API No.20 會員登入
    @POST("/VIGP1/accountManager/allLogIn")
    Call<LoginResponseData> login(@Body LoginRequestData data);

    // API No.22 取回服專/銷顧通訊錄
    @POST("/VIGP1/accountManager/getIkeyEmployeeData")
    Call<GetIkeyEmployeeDataResponseData> getIkeyEmployeeData(@Body GetIkeyEmployeeDataRequestData data);

    // API No.27 重送密碼
    @POST("/VIGP1/accountManager/reSendPassword")
    Call<ResponseData> reSendPassword(@Body ReSendPasswordRequestData data);

    // API No.29 取得用戶註冊時間
    @POST("/VIGP1/accountManager/getRegisterTime")
    Call<GetRegisterTimeResponseData> getRegisterTime(@Body GetRegisterTimeRequestData data);

    // API No.36 查詢用戶身分
    @POST("/VIGP1/accountManager/getUserData")
    Call<QueryUserResponseData> getUserData(@Body QueryUserRequestData data);

    // API No.41 會員登出
    @POST("/VIGP1/accountManager/ikeyLogOut")
    Call<ResponseData> logout(@Body LogoutRequestData data);

    // API No.57 檢核是否有重複註冊的帳號
    @POST("/VIGP1/accountManager/checkRepeatAccount")
    Call<ResponseData> checkRepeat(@Body SetAccountRequestData data);

    // API 76. 查詢車主身分
    @POST("/VIGP1/accountManager/getMasterKeyUserData")
    Call<GetMasterKeyUserResponseData> getMasterKeyUserData(@Body GetMasterKeyUserRequestData data);

    // #A01082 查詢車輛紅利點數
    @POST("/VIGP1/accountManager/getCarBonus")
    Call<GetCarBonusResponseData> getCarBonus(@Body GetCarBonusRequestData data);
}
