package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class LoginResponseData extends ResponseData {
    @SerializedName("UID")
    private String mUid;
    @SerializedName("ikeyUID")
    private String mKeyUid;
    @SerializedName("mPhone")
    private String mPhone;
    @SerializedName("ikeyToken")
    private String mToken;
    @SerializedName("isRedirect")
    private String mIsRedirect;

    /**
     * 如果該帳號可以關連到有效的User Account，請一併取回其UID
     */
    public String getUid() {
        return mUid;
    }

    /**
     * ikey UID
     */
    public String getKeyUid() {
        return mKeyUid;
    }

    /**
     * ikeymember 中驗證的電話號碼，可用為UseLog之用
     */
    public String getPhone() {
        return mPhone;
    }

    /**
     * ikey Token
     */
    public String getKeyToken() {
        return mToken;
    }

    /**
     * Y: 要轉頁至修改密碼頁
     */
    public boolean isRedirect() {
        return mIsRedirect.toUpperCase().equals("Y");
    }
}
