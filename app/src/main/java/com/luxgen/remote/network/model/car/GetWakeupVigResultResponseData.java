package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.model.option.VigOptions;
import com.luxgen.remote.network.model.ResponseData;

public class GetWakeupVigResultResponseData extends ResponseData {
    @SerializedName("result")
    private String mResult;

    public @VigOptions.VigWakeUpStatus
    String getResult() {
        return mResult;
    }
}
