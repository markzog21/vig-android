package com.luxgen.remote.network.model;

import com.google.gson.annotations.SerializedName;

public class VigRequestData extends RequestData {

    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName(KEY_CAR_ID)
    private String mCarId;

    /**
     * 參數皆必填 <br />
     * 用到此物件有: <br />
     * <ul>
     * <li>API No.43 取得PM2.5值</li>
     * <li>API No.46 取得車輛狀態</li>
     * </ul>
     */
    public VigRequestData(String iKeyUid, String iKeyToken, String carId) {
        mKeyUid = iKeyUid;
        mKeyToken = iKeyToken;
        mCarId = carId;
    }
}
