package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class GetMasterKeyUserRequestData extends RequestData {
    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName(KEY_CAR_ID)
    private String mCarId;

    /**
     * @param uid   iKeyUid
     * @param token iKeyToken
     * @param carid 要查詢車子ID
     */
    public GetMasterKeyUserRequestData(String uid, String token, String carid) {
        mKeyUid = uid;
        mKeyToken = token;
        mCarId = carid;
    }
}
