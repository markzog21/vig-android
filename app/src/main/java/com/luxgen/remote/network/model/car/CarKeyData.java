package com.luxgen.remote.network.model.car;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.model.Beacon;
import com.luxgen.remote.model.option.BeaconOptions;
import com.luxgen.remote.model.option.CarOptions;

import java.io.Serializable;

@Entity
public class CarKeyData implements Serializable {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "car_id")
    @SerializedName("carID")
    private String mCarId;
    @ColumnInfo(name = "vig_car_id")
    @SerializedName("VIGCarID")
    private String mVigCarId;
    @ColumnInfo(name = "vig_mac")
    @SerializedName("VIGMac")
    private String mVigMac;
    @ColumnInfo(name = "vig_imei")
    @SerializedName("VIGIMEI")
    private String mVigImei;
    @ColumnInfo(name = "vehicle_no")
    @SerializedName("vehicleNumber")
    private String mVehicleNumber;
    @ColumnInfo(name = "vin")
    @SerializedName("VIN")
    private String mVin;
    @ColumnInfo(name = "car_no")
    @SerializedName("carNO")
    private String mCarNo;
    @ColumnInfo(name = "car_master")
    @SerializedName("carMaster")
    private String mMaster;
    @ColumnInfo(name = "car_model")
    @SerializedName("carModel")
    private String mModel;
    @ColumnInfo(name = "key_type")
    @SerializedName("keyType")
    private String mKeyType;
    @ColumnInfo(name = "phone_type")
    @SerializedName("phoneType")
    private String mPhoneType;
    @ColumnInfo(name = "phone_id")
    @SerializedName("phoneID")
    private String mPhoneId;
    @ColumnInfo(name = "beacon_1_no")
    @SerializedName("beacon1NO")
    private String mBeacon1No;
    @ColumnInfo(name = "beacon_2_no")
    @SerializedName("beacon2NO")
    private String mBeacon2No;
    @ColumnInfo(name = "beacon_3_no3")
    @SerializedName("beacon3NO")
    private String mBeacon3No;
    @ColumnInfo(name = "beacon_learning_status")
    @SerializedName("beaconLearningStatus")
    private String mBeaconLearningStatus;
    @ColumnInfo(name = "begin_date")
    @SerializedName("beginDate")
    private long mBeginDate;
    @ColumnInfo(name = "end_date")
    @SerializedName("endDate")
    private long mEndDate;
    @ColumnInfo(name = "force_to_be_masterkey")
    @SerializedName("getForceToBeMasterKey")
    private boolean mForceToBeMasterKey = false;

    public String getCarModel() {
        return getModel();
    }

    public String getModel() {
        return mModel;
    }

    public void setModel(String model) {
        mModel = model;
    }

    public String getCarId() {
        return mCarId;
    }

    public void setCarId(String carId) {
        mCarId = carId;
    }

    public String getVigCarId() {
        if (mVigCarId == null || mVigCarId.equals("")) {
            return "";
        }

        if (mVigCarId.length() >= 16) {
            return mVigCarId;
        }

        return "0000000000000000".substring(0, 16 - mVigCarId.length()).concat(mVigCarId);
    }

    public void setVigCarId(String id) {
        mVigCarId = id;
    }

    public String getVigMac() {
        return mVigMac;
    }

    public void setVigMac(String mac) {
        mVigMac = mac;
    }

    public String getVigImei() {
        return mVigImei;
    }

    public void setVigImei(String imei) {
        mVigImei = imei;
    }

    public String getVehicleNumber() {
        return mVehicleNumber;
    }

    public void setVehicleNumber(String no) {
        mVehicleNumber = no;
    }

    public String getVin() {
        return mVin;
    }

    public void setVin(String vin) {
        mVin = vin;
    }

    public String getCarNo() {
        return mCarNo;
    }

    public void setCarNo(String no) {
        mCarNo = no;
    }

    public @CarOptions.CarMaster
    String getMaster() {
        return mMaster;
    }

    public void setMaster(@CarOptions.CarMaster String type) {
        mMaster = type;
    }

    public @CarOptions.KeyType
    String getKeyType() {
        return mKeyType;
    }

    public void setKeyType(@CarOptions.KeyType String type) {
        mKeyType = type;
    }

    /**
     * 配Key時的裝置類型 A:Android、I:Iphone
     */
    public String getPhoneType() {
        return mPhoneType;
    }

    public void setPhoneType(String type) {
        mPhoneType = type;
    }

    /**
     * 手機配Key時的唯一識別碼
     */
    public String getPhoneId() {
        return (TextUtils.isEmpty(mPhoneId)) ? "" : mPhoneId;
    }

    public void setPhoneId(String id) {
        mPhoneId = id;
    }

    private Beacon getBeaconFromStr(String uuid, String input) {
        if (TextUtils.isEmpty(input) || TextUtils.isEmpty(uuid)) {
            return null;
        }

        String[] token = input.split(",");
        if (token.length == 2) {
            int major = Integer.parseInt(token[0]);
            int minor = Integer.parseInt(token[1]);
            return new Beacon(uuid, major, minor);
        }
        return null;
    }

    /**
     * <<Beacon Major + minor NO>>”, //左側藍芽資料
     */
    public String getBeacon1No() {
        return mBeacon1No;
    }

    public void setBeacon1No(String no) {
        mBeacon1No = no;
    }

    public Beacon getLeftBeacon(String uuid) {
        return getBeaconFromStr(uuid, mBeacon1No);
    }

    /**
     * <<Beacon Major + minor NO>>”, //後側藍芽資料
     */
    public String getBeacon2No() {
        return mBeacon2No;
    }

    public void setBeacon2No(String no) {
        mBeacon2No = no;
    }

    public Beacon getBackBeacon(String uuid) {
        return getBeaconFromStr(uuid, mBeacon2No);
    }

    /**
     * <<Beacon Major + minor NO>>”, //右側藍芽資料
     */
    public String getBeacon3No() {
        return mBeacon3No;
    }

    public void setBeacon3No(String no) {
        mBeacon3No = no;
    }

    public Beacon getRightBeacon(String uuid) {
        return getBeaconFromStr(uuid, mBeacon3No);
    }

    public @BeaconOptions.BeaconLearningStatus
    String getBeaconLearningStatus() {
        return mBeaconLearningStatus;
    }

    public void setBeaconLearningStatus(@BeaconOptions.BeaconLearningStatus String status) {
        mBeaconLearningStatus = status;
    }

    public long getBeginDate() {
        return mBeginDate;
    }

    public void setBeginDate(long date) {
        mBeginDate = date;
    }

    public long getEndDate() {
        return mEndDate;
    }

    public void setEndDate(long date) {
        mEndDate = date;
    }

    public void setForceToBeMasterKey(boolean forceToBeMasterKey) {
        mForceToBeMasterKey = forceToBeMasterKey;
    }

    /**
     * 是否強制轉為Master Key
     */
    public boolean getForceToBeMasterKey() {
        return mForceToBeMasterKey;
    }

    public boolean isVig() {
        return !getVigCarId().equals("");
    }

    public boolean isMasterKeyM1() {
        return CarOptions.KEY_M1.equals(getKeyType());
    }

    public boolean isMasterKeyM2() {
        return CarOptions.KEY_M2.equals(getKeyType());
    }

    public boolean isMasterKeyAndReadyForActivation() {
        return isVig() && ((isDriver() || isOwner()) && getKeyType().equals(CarOptions.KEY_NO) || mForceToBeMasterKey);
    }

    public boolean isMasterKey() {
        return isVig() && ((isDriver() || isOwner()) &&
                (getKeyType().endsWith(CarOptions.KEY_M) || getKeyType().equals(CarOptions.KEY_M1) ||
                        getKeyType().equals(CarOptions.KEY_M2) || getKeyType().equals(CarOptions.KEY_NO)) ||
                mForceToBeMasterKey);
    }

    public boolean isSubKey() {
        return isVig() && ((getKeyType().equals(CarOptions.KEY_S1) || getKeyType().equals(CarOptions.KEY_S2)) &&
                !mForceToBeMasterKey);
    }

    public boolean isSnapKey() {
        return isVig() && ((getKeyType().equals(CarOptions.KEY_T1) || getKeyType().equals(CarOptions.KEY_T2)) &&
                !mForceToBeMasterKey);
    }

    public boolean isKeyReady() {
        return isVig() && (getKeyType().endsWith(CarOptions.KEY_M) || getKeyType().endsWith(CarOptions.KEY_M2) || getKeyType().equals(CarOptions.KEY_S2) || getKeyType().equals(CarOptions.KEY_T2));
    }

    public boolean isDriver() {
        return CarOptions.CAR_DRIVER.equals(getMaster());
    }

    public boolean isOwner() {
        return CarOptions.CAR_OWNER.equals(getMaster());
    }

    public String getVigName() {
        String mac = getVigMac();
        if (mac == null || mac.length() == 0) {
            return "";
        }

        return "VIG2_BLE_" + mac.toUpperCase();
    }
}
