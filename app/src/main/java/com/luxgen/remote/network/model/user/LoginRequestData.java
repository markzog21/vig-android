package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class LoginRequestData extends RequestData {
    @SerializedName(KEY_ACCOUNT)
    private String mAccount;
    @SerializedName(KEY_PASSWORD)
    private String mPassword;
    @SerializedName("phoneType")
    private String mType;
    @SerializedName("phoneBrand")
    private String mBrand;
    @SerializedName("phoneID")
    private String mPhoneId;
    @SerializedName("phoneOSVersion")
    private String mOsVersion;
    @SerializedName("deviceCreateTime")
    private long mDeviceCreateTime;

    public LoginRequestData(String account) {
        mAccount = account;
        mPassword = "";
        mType = "A";
        mBrand = "";
        mPhoneId = "";
        mOsVersion = "";
    }

    /**
     * 密碼，必填
     */
    public void setPassword(String password) {
        mPassword = encrypt(password);
    }

    /**
     * 手機廠牌
     * Ex: HTC U11, 若沒有則填空白
     */
    public void setPhoneBrand(String brand) {
        mBrand = brand;
    }

    /**
     * IMEI, IDFV，必填
     */
    public void setPhoneId(String id) {
        mPhoneId = id;
    }

    /**
     * 作業系統版本，必填
     */
    public void setOsVersion(String version) {
        mOsVersion = version;
    }

    public void setDeviceCreateTime(long time) {
        mDeviceCreateTime = time;
    }
}
