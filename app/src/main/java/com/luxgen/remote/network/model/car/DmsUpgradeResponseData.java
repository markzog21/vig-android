package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class DmsUpgradeResponseData extends ResponseData {

    @SerializedName("contactUserID")
    private String mId;
    @SerializedName("contactUserName")
    private String mName;
    @SerializedName("contactUserPhoneNO")
    private String mPhoneNo;
}
