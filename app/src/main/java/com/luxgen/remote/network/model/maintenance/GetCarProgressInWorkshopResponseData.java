package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class GetCarProgressInWorkshopResponseData extends ResponseData {
    @SerializedName("result")
    final WorkShopProgressResult wsResult = null;

    public WorkShopProgressResult getWsResult() {
        return wsResult;
    }
}
