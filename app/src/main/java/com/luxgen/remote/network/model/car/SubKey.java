package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;

public class SubKey {
    @SerializedName("subkeyUID")
    private String mUid = "";

    public String getUid() {
        return mUid;
    }
}
