package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class QueryUserRequestData extends RequestData {
    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mKeyToken;
    @SerializedName("qphoneNO")
    private String mPhone;

    /**
     * @param uid   iKeyUid
     * @param token iKeyToken
     * @param phone 被查詢者的電話
     */
    public QueryUserRequestData(String uid, String token, String phone) {
        mKeyUid = uid;
        mKeyToken = token;
        mPhone = phone;
    }
}
