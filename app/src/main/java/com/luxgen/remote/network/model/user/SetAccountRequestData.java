package com.luxgen.remote.network.model.user;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class SetAccountRequestData extends RequestData implements Parcelable {
    @SerializedName(KEY_ACCOUNT)
    private String mAccount;
    @SerializedName(KEY_PASSWORD)
    private String mPassword;
    @SerializedName("name")
    private String mName;
    @SerializedName("socialType")
    private String mSocialType;
    @SerializedName("socialID")
    private String mSocialId;
    @SerializedName("socialAlias")
    private String mSocialAlias;

    public SetAccountRequestData() {
        mAccount = "";
        mPassword = "";
        mName = "";
        //FB:Facebook、G:Google、N:沒有
        mSocialType = "N";
        mSocialId = "";
        mSocialAlias = "";
    }

    public void setAccount(String account) {
        mAccount = account;
    }

    public void setPassword(String password) {
        mPassword = encrypt(password);
    }

    public String getPassword() {
        return decrypt(mPassword);
    }

    /**
     * 社群帳號的姓名
     */
    public void setName(String name) {
        mName = name;
    }

    /**
     * 以 Google Account 進行帳號設定
     *
     * @param id    社群帳號的唯一識別碼
     * @param alias 社群帳號中的暱稱
     */
    public void setGoogleAccount(String id, String alias) {
        mSocialType = "G";
        setSocialData(id, alias);
    }

    /**
     * 以 Facebook 進行帳號設定
     *
     * @param id    社群帳號的唯一識別碼
     * @param alias 社群帳號中的暱稱
     */
    public void setFacebookAccount(String id, String alias) {
        mSocialType = "FB";
        setSocialData(id, alias);
    }

    /**
     * @param id    社群帳號的唯一識別碼
     * @param alias 社群帳號中的暱稱
     */
    private void setSocialData(String id, String alias) {
        mSocialId = id;
        mSocialAlias = alias;
    }

    private SetAccountRequestData(Parcel source) {
        mAccount = source.readString();
        mPassword = source.readString();
        mName = source.readString();
        mSocialType = source.readString();
        mSocialId = source.readString();
        mSocialAlias = source.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mAccount);
        dest.writeString(mPassword);
        dest.writeString(mName);
        dest.writeString(mSocialType);
        dest.writeString(mSocialId);
        dest.writeString(mSocialAlias);
    }

    public static final Parcelable.Creator<SetAccountRequestData> CREATOR = new Parcelable.Creator<SetAccountRequestData>() {

        @Override
        public SetAccountRequestData createFromParcel(Parcel source) {
            return new SetAccountRequestData(source);
        }

        @Override
        public SetAccountRequestData[] newArray(int size) {
            return new SetAccountRequestData[size];
        }
    };
}
