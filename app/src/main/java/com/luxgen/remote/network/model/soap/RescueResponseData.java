package com.luxgen.remote.network.model.soap;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

import static com.luxgen.remote.BuildConfig.SOAP_URL;

@Root(name = "ws_RSS_AppInsCaseResponse", strict = false)
@Namespace(reference = SOAP_URL)
public class RescueResponseData {

    @Element(name = "ws_RSS_AppInsCaseResult", required = false)
    private int ws_RSS_AppInsCaseResult;

    @Element(name = "iCaseID", required = false)
    private String iCaseID;

    @Element(name = "iResult", required = false)
    private int iResult;

    @Element(name = "iErrMsg", required = false)
    private String iErrMsg;

    public boolean isSuccess() {
        return iResult == 0;
    }
}
