package com.luxgen.remote.network;

import android.content.Context;
import android.support.annotation.NonNull;

import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.network.ApiInterface.IkeyInterface;
import com.luxgen.remote.network.model.ikey.IkeyFuncToServerRequestData;
import com.luxgen.remote.network.model.ikey.IkeyFuncToServerResponseData;
import com.luxgen.remote.network.model.ikey.IkeyLoginRequestData;
import com.luxgen.remote.network.model.ikey.IkeyLoginResponseData;
import com.luxgen.remote.network.model.ikey.IkeyRemoteControlRequestData;
import com.luxgen.remote.network.model.ikey.IkeyRemoteControlResponseData;

import java.io.IOException;
import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IkeyWebApi extends WebApi {

    private final String LOG_TAG = IkeyWebApi.class.getSimpleName();

    private static WeakReference<IkeyWebApi> mInstance;

    public static IkeyWebApi getInstance(Context context) {
        if (mInstance == null || mInstance.get() == null) {
            mInstance = new WeakReference<>(new IkeyWebApi(context));
        }

        return mInstance.get();
    }

    IkeyWebApi(Context context) {
        super(context);
    }

    // API No.55 ikey gateway login
    public void login(IkeyLoginRequestData data,
                      final OnApiListener<IkeyLoginResponseData> listener) {
        listener.onPreApiTask();

        getIkeyRetrofit().create(IkeyInterface.class)
                .login(data)
                .enqueue(new Callback<IkeyLoginResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<IkeyLoginResponseData> call,
                                           @NonNull Response<IkeyLoginResponseData> response) {
                        IkeyLoginResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure(getDefaultErrorMessage());
                            try {
                                LogCat.e(response.errorBody().string());
                            } catch (IOException e) {
                                LogCat.e("Server error: " + response.errorBody().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<IkeyLoginResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("iKey Interface Login", t));
                    }
                });
    }

    // API No.56 ikey gateway functino to ikey server
    public void funcToServer(IkeyFuncToServerRequestData data,
                             final OnApiListener<IkeyFuncToServerResponseData> listener) {
        listener.onPreApiTask();

        getIkeyRetrofit().create(IkeyInterface.class)
                .funcToServer(data)
                .enqueue(new Callback<IkeyFuncToServerResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<IkeyFuncToServerResponseData> call,
                                           @NonNull Response<IkeyFuncToServerResponseData> response) {
                        IkeyFuncToServerResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure("");
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<IkeyFuncToServerResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("iKey Interface function to server", t));
                    }
                });
    }

    // API No.67 ikey gateway remotecontrol
    public void remoteControl(IkeyRemoteControlRequestData data,
                              final OnApiListener<IkeyRemoteControlResponseData> listener) {
        listener.onPreApiTask();

        getIkeyRetrofit().create(IkeyInterface.class)
                .remoteControl(data)
                .enqueue(new Callback<IkeyRemoteControlResponseData>() {
                    @Override
                    public void onResponse(@NonNull Call<IkeyRemoteControlResponseData> call,
                                           @NonNull Response<IkeyRemoteControlResponseData> response) {
                        IkeyRemoteControlResponseData data = response.body();
                        if (data != null) {
                            if (data.isAuthFail()) {
                                listener.onAuthFailure();
                            } else {
                                listener.onApiTaskSuccessful(data);
                            }
                        } else {
                            listener.onApiTaskFailure("");
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<IkeyRemoteControlResponseData> call,
                                          @NonNull Throwable t) {
                        listener.onApiTaskFailure(getFailMessage("iKey Interface remote control", t));
                    }
                });
    }
}
