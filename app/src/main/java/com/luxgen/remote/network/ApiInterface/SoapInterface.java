package com.luxgen.remote.network.ApiInterface;

import com.luxgen.remote.network.model.soap.RequestEnvelope;
import com.luxgen.remote.network.model.soap.ResponseEnvelope;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface SoapInterface {

    @Headers({"Content-Type: text/xml; charset=utf-8"})

    @POST("/WS_RSS_O/MultiClientAPP.asmx")
    Call<ResponseEnvelope> uploadRescueData(@Body RequestEnvelope body);
}
