package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;

public class SpeedLimitStatusData {
    @SerializedName("carID")
    private String mCarId;
    @SerializedName("VIGCarID")
    private String mVigCarId;
    @SerializedName("startDateTime")
    private String mStartDateTime;

    public String getCarId() {
        return mCarId;
    }

    public String getVigCarId() {
        if (mVigCarId == null || mVigCarId.equals("")) {
            return "";
        }

        if (mVigCarId.length() >= 16) {
            return mVigCarId;
        }

        return "0000000000000000".substring(0, 16 - mVigCarId.length()).concat(mVigCarId);
    }

    public String getStartDateTime() {
        return mStartDateTime;
    }
}
