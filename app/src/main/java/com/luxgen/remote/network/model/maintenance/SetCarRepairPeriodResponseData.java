package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class SetCarRepairPeriodResponseData extends ResponseData {
    @SerializedName("bookingNo")
    private String mBookingNo;

    public String getBookingNo() {
        return mBookingNo;
    }
}
