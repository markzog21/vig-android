package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

import java.util.ArrayList;

public class QueryUserResponseData extends ResponseData {
    @SerializedName("UserList")
    private ArrayList<UserDeviceData> mList;

    public int getSize() {
        return mList == null ? 0 : mList.size();
    }

    public ArrayList<UserDeviceData> getList() {
        if (mList == null) {
            mList = new ArrayList<>();
        }

        return mList;
    }
}
