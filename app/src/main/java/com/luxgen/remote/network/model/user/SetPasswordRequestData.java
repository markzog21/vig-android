package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.RequestData;

public class SetPasswordRequestData extends RequestData {
    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName(KEY_IKEY_TOKEN)
    private String mToken;
    @SerializedName("oldPassword")
    private String mOldPassword;
    @SerializedName("newPassword")
    private String mNewPassword;

    public SetPasswordRequestData() {
        mKeyUid = "";
        mToken = "";
        mOldPassword = "";
        mNewPassword = "";
    }

    /**
     * iKeyUID 必填
     * */
    public void setUid(String uid) {
        mKeyUid = uid;
    }

    /**
     * ikeyToken 必填
     */
    public void setKeyToken(String token) {
        mToken = token;
    }

    /**
     * 必埴
     *
     * @param old      舊密碼
     * @param password 新密碼
     */
    public void setPassword(String old, String password) {
        mOldPassword = encrypt(old);
        mNewPassword = encrypt(password);
    }

}
