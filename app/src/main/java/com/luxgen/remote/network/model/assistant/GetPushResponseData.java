package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.custom.PushComparator;
import com.luxgen.remote.model.PushData;
import com.luxgen.remote.model.option.NotifactionOptions;
import com.luxgen.remote.network.model.ResponseData;

import java.util.ArrayList;
import java.util.Collections;

public class GetPushResponseData extends ResponseData {
    @SerializedName("TotalNO")
    private int mCount = 0;
    @SerializedName("PushList")
    private ArrayList<PushData> mList;

    public int getSize() {
        return mCount;
    }

    public ArrayList<PushData> getPushList() {
        if (mList == null) {
            mList = new ArrayList<>();
        }

        if (mList.size() > 1) {
            Collections.sort(mList, new PushComparator());
        }

        return mList;
    }

    public boolean hasUnRead() {
        final ArrayList<PushData> list = getPushList();

        for (PushData data : list) {
            if (NotifactionOptions.isUnRead(data.getReadStatus())) {
                return true;
            }
        }
        return false;
    }
}
