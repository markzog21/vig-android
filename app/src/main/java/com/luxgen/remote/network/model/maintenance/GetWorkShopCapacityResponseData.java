package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

import java.util.ArrayList;

public class GetWorkShopCapacityResponseData extends ResponseData {
    @SerializedName("availableDay")
    private ArrayList<String> mDay;

    /**
     * 可預約時段，格式: YYYY/MM/DD 18:00
     */
    public ArrayList<String> getDay() {
        if (mDay == null) {
            mDay = new ArrayList<>();
        }
        return mDay;
    }
}
