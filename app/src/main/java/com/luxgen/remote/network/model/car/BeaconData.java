package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;

public class BeaconData {
    @SerializedName("place")
    private String mPlace;
    @SerializedName("major")
    private int mMajor;
    @SerializedName("minor")
    private int mMinor;

    public BeaconData(int major, int minor, String place) {
        mPlace = place;
        mMajor = major;
        mMinor = minor;
    }
}
