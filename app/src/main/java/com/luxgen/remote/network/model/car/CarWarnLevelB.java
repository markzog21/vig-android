package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.model.option.CarOptions;

public class CarWarnLevelB {
    @SerializedName("errLDWSERROR")
    private String mLdws = "-1";
    @SerializedName("mWheelPressureSensorRF")
    private String mWheelPressureSensorRF = "-1";
    @SerializedName("errWheelPressureBatRF")
    private String mWheelPressureBatRf = "-1";
    @SerializedName("errWheelPressureSensorLF")
    private String mWheelPressureSensorLf = "-1";
    @SerializedName("errWheelPressureBatLF")
    private String mWheelPressureBatLf = "-1";
    @SerializedName("errWheelPressureSensorRR")
    private String mWheelPressureSensorRr = "-1";
    @SerializedName("errWheelPressureBatRR")
    private String mWheelPressureBatRr = "-1";
    @SerializedName("errWheelPressureSensorLR")
    private String mWheelPressureSensorLr = "-1";
    @SerializedName("errWheelPressureBatLR")
    private String mWheelPressureBatLr = "-1";
    @SerializedName("errEXwheelPressureStatusRF")
    private String mExWheelPressureStatusRf = "-1";
    @SerializedName("errEXwheelPressureStatusLF")
    private String mExWheelPressureStatusLf = "-1";
    @SerializedName("errEXwheelPressureStatusRR")
    private String mExWheelPressureStatusRr = "-1";
    @SerializedName("errEXwheelPressureStatusLR")
    private String mExWheelPressureStatusLr = "-1";
    @SerializedName("errESCFailed")
    private String mEsc = "-1";
    @SerializedName("errTCSFailed")
    private String mTcs = "-1";
    @SerializedName("errAPAStatusSystem")
    private String mApa = "-1";
    @SerializedName("errSNRStatusLF")
    private String mSnrLf = "-1";
    @SerializedName("errSNRStatusRF")
    private String mSnrRf = "-1";
    @SerializedName("errSNRStatusLS")
    private String mSnrLs = "-1";
    @SerializedName("errSNRStatusRS")
    private String mSnrRs = "-1";
    @SerializedName("errSNRStatusLR")
    private String mSnrLr = "-1";
    @SerializedName("errSNRStatusRR")
    private String mSnrRr = "-1";
    @SerializedName("errEVSErrSTA")
    private String mEvs = "-1";
    @SerializedName("errWifiStaOBUVIG")
    private String mWifi = "-1";
    @SerializedName("errKeyCardWrnReq")
    private String mBle = "-1";
    @SerializedName("errTPMSExist")
    private String mTPMSExist = "-1";
    @SerializedName("errTPMSWarnInd")
    private String mTPMSWarnInd = "-1";
    @SerializedName("errWheelPressureStatusLF")
    private String mWheelPressureStatusLf = "-1";
    @SerializedName("errWheelPressureStatusLR")
    private String mWheelPressureStatusLr = "-1";
    @SerializedName("errWheelPressureStatusRF")
    private String mWheelPressureStatusRf = "-1";
    @SerializedName("errWheelPressureStatusRR")
    private String mWheelPressureStatusRr = "-1";

    /**
     * 智慧型偵測警示系統發生錯誤
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getLdwsStatus() {
        return CarOptions.getErrStatus(mLdws);
    }

    /**
     * 右前胎壓計異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getWhellPressureSensorRfStatus() {
        return CarOptions.getErrStatus(mWheelPressureSensorRF);
    }

    /**
     * 右前胎壓計電力不足
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getWhellPressureBatRfStatus() {
        return CarOptions.getErrStatus(mWheelPressureBatRf);
    }

    /**
     * 左前胎壓計異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getWhellPressureSensorLfStatus() {
        return CarOptions.getErrStatus(mWheelPressureSensorLf);
    }

    /**
     * 左前胎壓計電力不足
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getWhellPressureBatLfStatus() {
        return CarOptions.getErrStatus(mWheelPressureBatLf);
    }

    /**
     * 右後胎壓計異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getWhellPressureSensorRrStatus() {
        return CarOptions.getErrStatus(mWheelPressureSensorRr);
    }

    /**
     * 右後胎壓計電力不足
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getWhellPressureBatRrStatus() {
        return CarOptions.getErrStatus(mWheelPressureBatRr);
    }

    /**
     * 左後胎壓計異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getWhellPressureSensorLrStatus() {
        return CarOptions.getErrStatus(mWheelPressureSensorLr);
    }

    /**
     * 左後胎壓計電力不足
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getWhellPressureBatLrStatus() {
        return CarOptions.getErrStatus(mWheelPressureBatLr);
    }

    /**
     * 右前胎壓偏高/低異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getExWheelPressureRfStatus() {
        return CarOptions.getErrStatus(mExWheelPressureStatusRf);
    }

    /**
     * 左前胎壓偏高/低異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getExWheelPressureLfStatus() {
        return CarOptions.getErrStatus(mExWheelPressureStatusLf);
    }

    /**
     * 右後胎壓偏高/低異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getExWheelPressureRrStatus() {
        return CarOptions.getErrStatus(mExWheelPressureStatusRr);
    }

    /**
     * 左後胎壓偏高/低異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getExWheelPressureLrStatus() {
        return CarOptions.getErrStatus(mExWheelPressureStatusLr);
    }

    /**
     * ESC異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getEscStatus() {
        return CarOptions.getErrStatus(mEsc);
    }

    /**
     * TCS異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getTcsStatus() {
        return CarOptions.getErrStatus(mTcs);
    }

    /**
     * APA系統異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getApaStatus() {
        return CarOptions.getErrStatus(mApa);
    }

    /**
     * 左前雷達系統異常(任一有誤就算)
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getSnrLfStatus() {
        return CarOptions.getErrStatus(mSnrLf);
    }

    /**
     * 右前雷達系統異常(任一有誤就算)
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getSnrRfStatus() {
        return CarOptions.getErrStatus(mSnrRf);
    }

    /**
     * 左側雷達系統異常(任一有誤就算)
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getSnrLsStatus() {
        return CarOptions.getErrStatus(mSnrLs);
    }

    /**
     * 右側雷達系統異常(任一有誤就算)
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getSnrRsStatus() {
        return CarOptions.getErrStatus(mSnrRs);
    }

    /**
     * 左後雷達系統異常(任一有誤就算)
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getSnrLrStatus() {
        return CarOptions.getErrStatus(mSnrLr);
    }

    /**
     * 右後雷達系統異常(任一有誤就算)
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getSnrRrStatus() {
        return CarOptions.getErrStatus(mSnrRr);
    }

    /**
     * 行車AR影像系統檢測
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getEvsStatus() {
        return CarOptions.getErrStatus(mEvs);
    }

    /**
     * VIG-OBU Wi-Fi熱點連接
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getObuWifiStatus() {
        return CarOptions.getErrStatus(mWifi);
    }

    /**
     *VIG-APP BLE連接異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getAppBleStatus() {
        return CarOptions.getErrStatus(mBle);
    }

    /**
     *胎壓計電力不足
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getTpmsExist() {
        return CarOptions.getErrStatus(mTPMSExist);
    }

    /**
     *胎壓計電力不足
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getTpmsWarnInd() {
        return CarOptions.getErrStatus(mTPMSWarnInd);
    }

    /**
     *左前胎壓偏高/低異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getWheelPressureLfStatus() {
        return CarOptions.getErrStatus(mWheelPressureStatusLf);
    }

    /**
     *左後胎壓偏高/低異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getWheelPressureLrStatus() {
        return CarOptions.getErrStatus(mWheelPressureStatusLr);
    }

    /**
     *右前胎壓偏高/低異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getWheelPressureRfStatus() {
        return CarOptions.getErrStatus(mWheelPressureStatusRf);
    }

    /**
     *右後胎壓偏高/低異常
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 正常
     * {@link CarOptions.CarStatus#ABNORMAL_1} 異常
     */
    public @CarOptions.CarStatus
    int getWheelPressureRrStatus() {
        return CarOptions.getErrStatus(mWheelPressureStatusRr);
    }
}
