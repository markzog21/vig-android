package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;

public class WorkShopProgressResult {
    @SerializedName("progress")
    private String mProgress;

    public String getmProgress() {
        return mProgress;
    }
}
