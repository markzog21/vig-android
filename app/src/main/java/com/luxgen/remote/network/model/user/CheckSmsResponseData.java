package com.luxgen.remote.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

public class CheckSmsResponseData extends ResponseData {
    //
    @SerializedName("UID")
    private String mUid;

    @SerializedName("ikeyUID")
    private String mKeyUid;

    /**
     * 若驗證成功則回傳UID，DMS會員才回傳
     * */
    public String getUid() {
        return mUid;
    }

    /**
     * ikey_UID
     * */
    public String getKeyUid() {
        return mKeyUid;
    }

}
