package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

import java.util.ArrayList;

public class GetCarTrackResponseData extends ResponseData {
    @SerializedName("carTracks")
    private ArrayList<CarTrackData> mList;

    public ArrayList<CarTrackData> getList() {
        if (mList == null) {
            mList = new ArrayList<>();
        }
        return mList;
    }

    public int getSize() {
        return mList == null ? 0 : mList.size();
    }
}
