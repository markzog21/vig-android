package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.model.option.CarOptions;
import com.luxgen.remote.network.model.RequestData;

public class KeyData extends RequestData {

    @SerializedName(KEY_IKEY_UID)
    private String mKeyUid;
    @SerializedName("phoneNO")
    private String mPhoneNo;
    @SerializedName("name")
    private String mName;
    @SerializedName("eMail")
    private String mEmail;
    @SerializedName("keyType")
    private String mKeyType;
    @SerializedName("phoneType")
    private String mPhoneType;
    @SerializedName("phoneID")
    private String mPhoneId;
    @SerializedName("deviceCreateTime")
    private String mDeviceCreateTime;
    @SerializedName("keyStartDate")
    private String mStartDate;
    @SerializedName("keyEndDate")
    private String mEndDate;
    @SerializedName("deleteKeyStatus")
    private String mDeleteKeyStatus;

    public String getKeyUid() {
        return mKeyUid;
    }

    public String getPhoneNo() {
        return mPhoneNo;
    }

    public String getName() {
        return mName;
    }

    public String getEmail() {
        return mEmail;
    }

    public @CarOptions.KeyType
    String getKeyType() {
        return mKeyType;
    }

    public String getPhoneType() {
        return mPhoneType;
    }

    public String getPhoneId() {
        return mPhoneId;
    }

    public String getDeviceCreateTime() {
        return mDeviceCreateTime;
    }

    public String getKeyStartDate() {
        return mStartDate;
    }

    public String getKeyEndDate() {
        return mEndDate;
    }

    public String getDeleteKeyStatus() {
        return mDeleteKeyStatus;
    }
}
