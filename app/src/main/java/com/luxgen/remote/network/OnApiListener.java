package com.luxgen.remote.network;

public interface OnApiListener<T> {
    void onApiTaskSuccessful(T responseData);

    void onApiTaskFailure(String failMessage);

    /**
     * 請記得這邊可能需要用 runOnUiThread(() -> {}
     * */
    void onPreApiTask();

    void onApiProgress(long value);

    void onAuthFailure();
}
