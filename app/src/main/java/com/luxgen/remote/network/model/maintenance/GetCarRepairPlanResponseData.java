package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

import java.util.ArrayList;

public class GetCarRepairPlanResponseData extends ResponseData {
    @SerializedName("carList")
    private ArrayList<CarRepairPlanData> mList;

    public int getSize() {
        return mList == null ? 0 : mList.size();
    }

    public ArrayList<CarRepairPlanData> getRepairPlayList() {
        if (mList == null) {
            mList = new ArrayList<>();
        }
        return mList;
    }
}
