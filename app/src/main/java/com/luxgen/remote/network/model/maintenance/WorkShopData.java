package com.luxgen.remote.network.model.maintenance;

import com.google.gson.annotations.SerializedName;

public class WorkShopData {
    @SerializedName("dealerCode")
    private String mDealerCode;
    @SerializedName("deptCode")
    private String mDeptCode;
    @SerializedName("dept")
    private String mDeptName;
    @SerializedName("phoneNO1")
    private String mPhone1;
    @SerializedName("phoneNO2")
    private String mPhone2;
    @SerializedName("fax")
    private String mFax;
    @SerializedName("zipCode")
    private String mZipCode;
    @SerializedName("county")
    private String mCountry;
    @SerializedName("address")
    private String mAddress;

    /**
     * 經銷商代碼
     */
    public String getDealerCode() {
        return mDealerCode;
    }

    /**
     * 部門代碼
     */
    public String getDeptCode() {
        return mDeptCode;
    }

    /**
     * 經銷商名稱
     */
    public String getDeptName() {
        return mDeptName;
    }

    /**
     * 保養廠Phone1
     */
    public String getDeptPhone1() {
        return mPhone1;
    }

    /**
     * 保養廠Phone2
     */
    public String getDeptPhone2() {
        return mPhone2;
    }

    /**
     * 傳真
     */
    public String getFax() {
        return mFax;
    }

    /**
     * 郵遞區號
     */
    public String getZipCode() {
        return mZipCode;
    }

    /**
     * 城市
     */
    public String getCounty() {
        return mCountry;
    }

    /**
     * 完整地址
     */
    public String getAddress() {
        return mAddress;
    }
}
