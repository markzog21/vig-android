package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.VigRequestData;

import java.util.UUID;

public class WakeupVigRequestData extends VigRequestData {

    @SerializedName("phoneType")
    private String mType;
    @SerializedName("phoneID")
    private String mPhoneId;
    @SerializedName("wakeUpID")
    private String mWakeUpId;

    /**
     * 參數皆必填 <br />
     * Weak Up ID 在初始化會自動建立，但要記得設定 phone ID
     *
     * @param iKeyUid
     * @param iKeyToken
     * @param carId
     */
    public WakeupVigRequestData(String iKeyUid, String iKeyToken, String carId) {
        super(iKeyUid, iKeyToken, carId);
        mType = "A";
        mWakeUpId = UUID.randomUUID().toString();
    }

    /**
     * 手機的唯一識別碼 e.g. IMEI
     */
    public void setPhoneId(String id) {
        mPhoneId = id;
    }

    /**
     * 在初始化時就會建立當次的 UUID，若有需要請記得取出並暫存
     */
    public String getWakeUpId() {
        return mWakeUpId;
    }
}
