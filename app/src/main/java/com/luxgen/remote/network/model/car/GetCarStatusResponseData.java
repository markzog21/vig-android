package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.model.option.CarOptions;
import com.luxgen.remote.network.model.ResponseData;

public class GetCarStatusResponseData extends ResponseData {
    @SerializedName("carModel")
    private String mCarModel;
    @SerializedName("vehicleNumber")
    private String mVehicleNo;
    @SerializedName("carNO")
    private String mCarNo;
    @SerializedName("eventDateTime")
    private String mEventDate;
    @SerializedName("availableTrip")
    private double mAvailableTrip = -1;
    @SerializedName("fuelGauge")
    private double mFuelGauge = -1.0;
    @SerializedName("StaDrvTempSet")
    private int mTempSet = -1;
    @SerializedName("wheelPressureRF")
    private int mWheelPressureRf = -1;
    @SerializedName("wheelPressureLF")
    private int mWheelPressureLf = -1;
    @SerializedName("wheelPressureRR")
    private int mWheelPressureRr = -1;
    @SerializedName("wheelPressureLR")
    private int mWheelPressureLr = -1;
    @SerializedName("BCMDoorLockSta")
    private int mDoorUnLock = -1;
    @SerializedName("BCMAllDrSwSta")
    private int mAllDoor = -1;
    @SerializedName("BCMLRDrSwSta")
    private int mLrDoor = -1;
    @SerializedName("BCMRRDrSwSta")
    private int mRrDoor = -1;
    @SerializedName("BCMTGateSwSta")
    private int mGate = -1;
    @SerializedName("warnLevelA")
    private CarWarnLevelA mWarnA;
    @SerializedName("warnLevelB")
    private CarWarnLevelB mWarnB;

    /**
     * 車型
     */
    public String getModel() {
        return mCarModel;
    }

    /**
     * 車身碼
     */
    public String getVehicleNo() {
        return mVehicleNo;
    }

    /**
     * 車牌
     */
    public String getCarNo() {
        return mCarNo;
    }

    /**
     * 事件發生時間
     */
    public String getEventDate() {
        return mEventDate;
    }

    /**
     * 可行駛剩餘里程數
     *
     * @return double 預設值為 -1，即沒有狀態
     */
    public double getAvailableTrip() {
        return mAvailableTrip;
    }

    /**
     * 剩餘油量
     *
     * @return double 預設值為 -1.0，即沒有狀態
     */
    public double getFuelGauge() {
        return mFuelGauge;
    }

    /**
     * 右前輪胎壓
     *
     * @return int 預設值為 -1，即沒有狀態
     */
    public int getWheelPressureRf() {
        return mWheelPressureRf;
    }

    /**
     * 左前輪胎壓
     *
     * @return int 預設值為 -1，即沒有狀態
     */
    public int getWheelPressureLf() {
        return mWheelPressureLf;
    }

    /**
     * 右後輪胎壓
     *
     * @return int 預設值為 -1，即沒有狀態
     */
    public int getWheelPressureRr() {
        return mWheelPressureRr;
    }

    /**
     * 左後輪胎壓
     *
     * @return int 預設值為 -1，即沒有狀態
     */
    public int getWheelPressureLr() {
        return mWheelPressureLr;
    }

    /**
     * 門的上鎖狀態
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 有鎖
     * {@link CarOptions.CarStatus#ABNORMAL_1} 沒鎖
     */
    public @CarOptions.CarStatus
    int isDoorLockStatus() {
        return CarOptions.getLockStatus(mDoorUnLock);
    }

    /**
     * 所有門都關
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 有關
     * {@link CarOptions.CarStatus#ABNORMAL_1} 沒關
     */
    public @CarOptions.CarStatus
    int isAllDoorCloseStatus() {
        return CarOptions.getCloseStatus(mAllDoor);
    }

    /**
     * 右後滑門關閉
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 有關
     * {@link CarOptions.CarStatus#ABNORMAL_1} 沒關
     */
    public @CarOptions.CarStatus
    int getRrDoorCloseStatus() {
        return CarOptions.getCloseStatus(mRrDoor);
    }

    /**
     * 左後滑門關閉
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 有關
     * {@link CarOptions.CarStatus#ABNORMAL_1} 沒關
     */
    public @CarOptions.CarStatus
    int getLrDoorCloseStatus() {
        return CarOptions.getCloseStatus(mLrDoor);
    }

    /**
     * 尾門關閉
     *
     * @return {@link CarOptions.CarStatus}
     * {@link CarOptions.CarStatus#NON} 沒有狀態
     * {@link CarOptions.CarStatus#NORMAL} 有關
     * {@link CarOptions.CarStatus#ABNORMAL_1} 沒關
     */
    public @CarOptions.CarStatus
    int getGateCloseStatus() {
        return CarOptions.getCloseStatus(mGate);
    }

    /**
     * Level A error
     */
    public CarWarnLevelA getWarnLevelA() {
        return mWarnA;
    }

    /**
     * Level B error
     */
    public CarWarnLevelB getWarnLevelB() {
        return mWarnB;
    }
}
