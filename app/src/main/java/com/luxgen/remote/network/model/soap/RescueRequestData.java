package com.luxgen.remote.network.model.soap;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

import static com.luxgen.remote.BuildConfig.SOAP_URL;

@Root(name = "ws_RSS_AppInsCase", strict = false)
@Namespace(reference = SOAP_URL)
public class RescueRequestData {
    @Element(name = "ApplyDate")
    private String mApplyDate;

    @Element(name = "MolifeMemberNo")
    private String mMolifeMemberNo;

    @Element(name = "RoadMemberID")
    private int mRoadMemberID;

    @Element(name = "ContractID")
    private int mContractID;

    @Element(name = "ContractName")
    private String mContractName;

    @Element(name = "CardType")
    private int mCardType;

    @Element(name = "CardTypeName")
    private String mCardTypeName;

    @Element(name = "MemberName")
    private String mMemberName;

    @Element(name = "PlateNo")
    private String mPlateNo;

    @Element(name = "MemberMobile")
    private String mMemberMobile;

    @Element(name = "BreakLongitude")
    private double mBreakLongitude;

    @Element(name = "BreakLatitude")
    private double mBreakLatitude;

    @Element(name = "ini_BreakType")
    private int mIni_BreakType;

    @Element(name = "ini_BreakArea")
    private String mIni_BreakArea;

    @Element(name = "ini_BreakRegion")
    private String mIni_BreakRegion;

    @Element(name = "ini_BreakCode")
    private String mIni_BreakCode;

    @Element(name = "ini_BreakAddr")
    private String mIni_BreakAddr;

    @Element(name = "upd_BreakType")
    private String mUpd_BreakType;

    @Element(name = "upd_BreakArea")
    private String mUpd_BreakArea;

    @Element(name = "upd_BreakRegion")
    private String mUpd_BreakRegion;

    @Element(name = "upd_BreakCode")
    private String mUpd_BreakCode;

    @Element(name = "upd_BreakAddr")
    private String mUpd_BreakAddr;

    @Element(name = "BreakDescNo")
    private String mBreakDescNo;

    @Element(name = "BreakDesc")
    private String mBreakDesc;

    @Element(name = "PreHandleWayCd")
    private String mPreHandleWayCd;

    @Element(name = "RelativeKeyNo")
    private String mRelativeKeyNo;

    @Element(name = "OtherKey")
    private String mOtherKey;

    @Element(name = "Remark")
    private String mRemark;

    @Element(name = "Service")
    private String mService;

    @Element(name = "InsertSourceType")
    private String mInsertSourceType;

    @Element(name = "iCaseID")
    private String mICaseID;

    @Element(name = "iResult")
    private int mIResult;

    @Element(name = "iErrMsg")
    private String mIErrMsg;

    public RescueRequestData() {
        mMolifeMemberNo = "";
        mRoadMemberID = 0;
        mContractID = 66;
        mContractName = "LUXGEN納智捷";
        mCardType = 6601;
        mCardTypeName = "納智捷保固會員";
        mUpd_BreakType = "";
        mUpd_BreakArea = "";
        mUpd_BreakRegion = "";
        mUpd_BreakCode = "";
        mUpd_BreakAddr = "";
        mBreakDescNo = "0";
        mBreakDesc = "";
        mPreHandleWayCd = "T";
        mRelativeKeyNo = "";
        mOtherKey = "";
        mRemark = "";
        mService = "LUX_APP_C";
        mInsertSourceType = "5";
        mICaseID = "";
        mIResult = 0;
        mIErrMsg = "";
    }

    public void setDate(String date) {
        mApplyDate = date;
    }

    public void setName(String name) {
        mMemberName = name;
    }

    public void setPlateNo(String plateNo) {
        mPlateNo = plateNo;
    }

    public void setMobile(String mobile) {
        mMemberMobile = mobile;
    }

    public void setType(int type) {
        mIni_BreakType = type;
    }

    public void setArea(String area) {
        mIni_BreakArea = area;
    }

    public void setRegion(String region) {
        mIni_BreakRegion = region;
    }

    public void setCode(String code) {
        mIni_BreakCode = code;
    }

    public void setAddress(String address) {
        mIni_BreakAddr = address;
    }

    public void setLongitude(double longitude) {
        mBreakLongitude = longitude;
    }

    public void setLatitude(double latitude) {
        mBreakLatitude = latitude;
    }
}
