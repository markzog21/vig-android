package com.luxgen.remote.network.model.car;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.model.option.CarOptions;
import com.luxgen.remote.network.model.ResponseData;

public class DeleteMasterKeyByAPPResponseData extends ResponseData {

    @SerializedName("procedureID")
    private String mProcedureID;
    @SerializedName("deleteStatus")
    private String mDeleteStatus;

    public String getProcedureID() {
        return mProcedureID;
    }

    public @CarOptions.DeleteStatus
    String getDeleteStatus() {
        return mDeleteStatus;
    }
}
