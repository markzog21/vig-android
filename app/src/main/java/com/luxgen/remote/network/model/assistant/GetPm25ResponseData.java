package com.luxgen.remote.network.model.assistant;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.network.model.ResponseData;

import java.util.ArrayList;

public class GetPm25ResponseData extends ResponseData {
    @SerializedName("pm25Tracks")
    private ArrayList<Pm25Data> mList;

    public int getSize() {
        return mList == null ? 0 : mList.size();
    }

    public ArrayList<Pm25Data> getList() {
        if (mList == null) {
            mList = new ArrayList<>();
        }

        return mList;
    }
}
