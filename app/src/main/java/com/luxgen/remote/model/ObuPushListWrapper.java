package com.luxgen.remote.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ObuPushListWrapper {

    @SerializedName("Action")
    private String mAction;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("StatusMsg")
    private String mStatusMsg;
    @SerializedName("TotalNo")
    private int mTotalNo;
    @SerializedName("PushList")
    private List<ObuPushData> mPushList;

    public ObuPushListWrapper() {
        mAction = "";
        mStatus = "";
        mStatusMsg = "";
        mTotalNo = 0;
        mPushList = new ArrayList<>();
    }

    public void setAction(String action) {
        mAction = action;
    }

    public String getAction() {
        return mAction;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public void setStatusMsg(String msg) {
        mStatusMsg = msg;
    }

    public void setSize(int no) {
        mTotalNo = no;
    }

    public void setPushList(List<ObuPushData> list) {
        mPushList = list;
    }
}
