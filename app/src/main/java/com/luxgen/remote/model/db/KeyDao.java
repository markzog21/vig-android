package com.luxgen.remote.model.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.luxgen.remote.model.KeyInfo;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface KeyDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(KeyInfo data);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> insert(ArrayList<KeyInfo> list);

    @Query("SELECT * FROM KeyInfo WHERE car_id = :carId ORDER BY position ASC")
    List<KeyInfo> fetchList(String carId);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(ArrayList<KeyInfo> list);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(KeyInfo data);
}
