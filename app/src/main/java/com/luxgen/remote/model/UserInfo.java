package com.luxgen.remote.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.luxgen.remote.util.Prefs;

@Entity
public class UserInfo {

    // email
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "account")
    private String mAccount;

    // the same with mAccount when initialized, can be changed to 'contact' mail address
    @ColumnInfo(name = "email")
    private String mEmail;

    @ColumnInfo(name = "password")
    private String mPassword;

    @ColumnInfo(name = "facebook_password")
    private String mFacebookPassword;

    @ColumnInfo(name = "google_password")
    private String mGooglePassword;

    @ColumnInfo(name = "uid")
    // only DMS user got this
    private String mUid;

    // fixed
    @ColumnInfo(name = "ikey_uid")
    private String mKeyUid;

    @ColumnInfo(name = "ikey_token")
    // changed every login
    private String mToken;

    @ColumnInfo(name = "phone")
    private String mPhone;

    @ColumnInfo(name = "device_id")
    private String mDeviceId;

    public UserInfo(String account, String password) {
        this.mAccount = account;
        this.mEmail = account;
        this.mPassword = password;
        this.mFacebookPassword = "";
        this.mGooglePassword = "";
        this.mPhone = "";
        this.mUid = "";
        this.mToken = "";
        this.mKeyUid = "";
        this.mDeviceId = "";
    }

    public void setAccount(String account) {
        this.mAccount = account;
    }

    public String getAccount() {
        return mAccount;
    }

    public void setEmail(String mail) {
        this.mEmail = mail;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setPassword(String password) {
        this.mPassword = password;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setFacebookPassword(String password) {
        this.mFacebookPassword = password;
    }

    public String getFacebookPassword() {
        return mFacebookPassword;
    }

    public void setGooglePassword(String password) {
        this.mGooglePassword = password;
    }

    public String getGooglePassword() {
        return mGooglePassword;
    }

    public void setUid(String uid) {
        this.mUid = uid;
    }

    public String getUid() {
        return mUid;
    }

    public void setKeyUid(String uid) {
        this.mKeyUid = uid;
    }

    public String getKeyUid() {
        return mKeyUid;
    }

    public void setToken(String token) {
        this.mToken = token;
    }

    public String getToken() {
        return mToken;
    }

    public void setPhone(String phone) {
        this.mPhone = phone;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setDeviceId(String deviceId) {
        this.mDeviceId = deviceId;
    }

    public String getDeviceId() {
        return mDeviceId;
    }

    public String getDeviceId(Context context) {
        if (TextUtils.isEmpty(mDeviceId)) {
            setDeviceId(Prefs.getDeviceId(context));
        }
        return getDeviceId();
    }

    public long getDeviceCreateTime(Context context) {
        return Prefs.getDeviceIdCreateTime(context);
    }

    public boolean isDMS() {
        return !TextUtils.isEmpty(mUid);
    }

    public boolean hasLoginData() {
        return !TextUtils.isEmpty(mEmail) && !TextUtils.isEmpty(mPassword);
    }
}
