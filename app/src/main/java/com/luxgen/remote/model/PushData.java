package com.luxgen.remote.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.luxgen.remote.model.option.NotifactionOptions;

@Entity
public class PushData {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "uuid")
    @SerializedName("Push_UUID")
    private String mUuid = "";

    @ColumnInfo(name = "provider")
    @SerializedName("Provider")
    private String mProvider;

    @ColumnInfo(name = "title")
    @SerializedName("Title")
    private String mTitle;

    @ColumnInfo(name = "contents")
    @SerializedName("Contents")
    private String mContents;

    @ColumnInfo(name = "date")
    @SerializedName("PushDate")
    private String mDate;

    @ColumnInfo(name = "status")
    @SerializedName("ReadStatus")
    @NotifactionOptions.ReadStatus
    private String mReadStatus;

    @ColumnInfo(name = "sync")
    private boolean mNeedSyncToServer = false;

    @Ignore
    private boolean isSelected = false;
    @Ignore
    private boolean isStatusChanged = false;

    /**
     * Push UUID
     */
    public String getUuid() {
        return mUuid;
    }

    public void setUuid(String uuid) {
        mUuid = uuid;
    }

    /**
     * 推播者提供者訊息
     */
    public String getProvider() {
        return mProvider;
    }

    public void setProvider(String provider) {
        mProvider = provider;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setContents(String content) {
        mContents = content;
    }

    public String getContents() {
        return mContents;
    }

    /**
     * 推播時間
     */
    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public void setReadStatus(@NotifactionOptions.ReadStatus String status) {
        mReadStatus = status;
    }

    public boolean isStatusChanged() {
        return this.isStatusChanged;
    }

    public String getReadStatus() {
        return mReadStatus;
    }

    public void changeToDeleteStatus() {
        mReadStatus = NotifactionOptions.changeDeleteStatus(mReadStatus);
    }

    public void changeReadStatus() {
        if (NotifactionOptions.isUnRead(mReadStatus)) {
            mReadStatus = NotifactionOptions.READ;
            this.isStatusChanged = true;
        }
    }

    /**
     * 是否已讀
     */
    public boolean isRead() {
        return mReadStatus.equals(NotifactionOptions.READ);
    }

    public void setSelect(boolean isSelect) {
        isSelected = isSelect;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setNeedSyncToServer(boolean isNeed) {
        mNeedSyncToServer = isNeed;
    }

    public boolean getNeedSyncToServer() {
        return mNeedSyncToServer;
    }
}
