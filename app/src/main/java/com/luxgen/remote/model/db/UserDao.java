package com.luxgen.remote.model.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.luxgen.remote.model.UserInfo;

@Dao
public interface UserDao {

    /**
     * 登入時才使用，其餘時的資料更新請用 {@link #update(UserInfo)}
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void login(UserInfo data);

    @Update
    void update(UserInfo data);

    @Query("SELECT * FROM UserInfo LIMIT 1")
    LiveData<UserInfo> get();

    @Query("DELETE FROM UserInfo")
    void logout();
}
