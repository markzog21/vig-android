package com.luxgen.remote.model.option;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class BeaconOptions {
    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            LEFT_FRONT,
            RIGHT_FRONT,
            TAILGATE
    })
    public @interface BeaconStatus {
    }

    /**
     * 左前
     */
    public static final String LEFT_FRONT = "L";
    /**
     * 右前
     */
    public static final String RIGHT_FRONT = "R";
    /**
     * 尾門
     */
    public static final String TAILGATE = "B";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            LEARNT,
            READ
    })
    public @interface BeaconLearningStatus {
    }

    /**
     * 已學習
     */
    public static final String LEARNT = "L";
    /**
     * 已通知但用戶無啟動學習
     */
    public static final String READ = "R";
}

