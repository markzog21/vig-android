package com.luxgen.remote.model.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.luxgen.remote.network.model.car.CarKeyData;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface CarKeyDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> insert(ArrayList<CarKeyData> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(CarKeyData data);

    @Query("SELECT * FROM CarKeyData LIMIT 1")
    CarKeyData get();

    @Query("SELECT * FROM CarKeyData")
    LiveData<List<CarKeyData>> getListLiveData();

    @Query("SELECT * FROM CarKeyData")
    List<CarKeyData> getList();

    @Delete
    void clear(CarKeyData data);

    @Query("DELETE FROM CarKeyData")
    void clear();
}
