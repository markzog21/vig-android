package com.luxgen.remote.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

public class WheelTileItem implements Parcelable {

    public final static int COLOR_BLUE = 1;
    public final static int COLOR_YELLOW = 2;
    public final static int COLOR_GREEN = 3;
    public final static int COLOR_BLACK = 4;
    public final static int COLOR_UNKNOWN = 5;

    public final static int ACTION_EMPTY = 0;
    public final static int ACTION_AR_REMOTE = 1;
    public final static int ACTION_FIND_CAR = 2;
    public final static int ACTION_CAR_LOCK = 3;
    public final static int ACTION_CAR_UNLOCK = 4;
    public final static int ACTION_BACKDOOR_OPEN = 5;
    public final static int ACTION_FRESH_AIR = 6;
    public final static int ACTION_ENGINE_OFF = 32;

    public final static int ACTION_CLICK_SERVICE = 7;
    public final static int ACTION_SERVER = 8;
    public final static int ACTION_SALES_CONSULTANT = 9;
    public final static int ACTION_110 = 10;
    public final static int ACTION_RESCUE = 11;
    public final static int ACTION_SECRATERY = 12;
    public final static int ACTION_RESERVATION = 13;
    public final static int ACTION_DRIVER_HELP = 14;

    public final static int ACTION_CAR_FIX = 15;
    public final static int ACTION_FIX_RESERVATION = 16;
    public final static int ACTION_YOUR_RESERVATION = 17;
    public final static int ACTION_RESERVATION_PROGRESS = 18;
    public final static int ACTION_RESERVATION_RECORD = 19;

    public final static int ACTION_CAR_LIST = 20;
    public final static int ACTION_DIGITAL_KEY = 21;
    public final static int ACTION_CAR_STATUS = 22;
    public final static int ACTION_CAR_ACCIDENT = 23;
    public final static int ACTION_CAR_THEFT = 24;
    public final static int ACTION_ETC_BALANCE = 25;
    public final static int ACTION_DASHBOARD_SIGNAL = 26;
    public final static int ACTION_SETTING = 27;
    public final static int ACTION_ACCOUNT_MODIFY = 28;
    public final static int ACTION_PINCODE_MODIFY = 29;
    public final static int ACTION_SOUND_PLUS = 30;
    public final static int ACTION_SMART_EATS = 31;

    @DrawableRes
    private int mIcon;

    @DrawableRes
    private int mSetupIcon;

    @StringRes
    private int mTitle;

    @StringRes
    private int mShortTitle;

    @StringRes
    private int mDescription;

    private int mActionId = -1;
    private int mColor;

    private WheelTileItem(Parcel in) {
        mIcon = in.readInt();
        mSetupIcon = in.readInt();
        mTitle = in.readInt();
        mShortTitle = in.readInt();
        mDescription = in.readInt();
        mActionId = in.readInt();
        mColor = in.readInt();
    }

    public static final Creator<WheelTileItem> CREATOR = new Creator<WheelTileItem>() {
        @Override
        public WheelTileItem createFromParcel(Parcel in) {
            return new WheelTileItem(in);
        }

        @Override
        public WheelTileItem[] newArray(int size) {
            return new WheelTileItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mIcon);
        dest.writeInt(mSetupIcon);
        dest.writeInt(mTitle);
        dest.writeInt(mShortTitle);
        dest.writeInt(mDescription);
        dest.writeInt(mActionId);
        dest.writeInt(mColor);
    }

    public WheelTileItem(int icon, int setupIcon, int title, int shortTitle, int description, int action, int color) {
        mIcon = icon;
        mSetupIcon = setupIcon;
        mTitle = title;
        mShortTitle = shortTitle;
        mDescription = description;
        mActionId = action;
        mColor = color;
    }

    public @DrawableRes
    int getIcon() {
        return mIcon;
    }

    public @DrawableRes
    int getSetupIcon() {
        return mSetupIcon;
    }

    public @StringRes
    int getTitle() {
        return mTitle;
    }

    public @StringRes
    int getShortTitle() {
        return mShortTitle;
    }

    public @StringRes
    int getDescription() {
        return mDescription;
    }

    public int getActionId() {
        return mActionId;
    }

    public int getColor() {
        return mColor;
    }

    public String toString() {
        return "[mIcon=" + mIcon + ", mSetupIcon=" + mSetupIcon + ", mTitle=" + mTitle +
                ", mShortTitle=" + mShortTitle + ", mDescription=" + mDescription +
                ", mActionId=" + mActionId + ", mColor=" + mColor + "]";
    }

    private static boolean isDmsRelatedFunction(int actionId) {
        return isVigRelatedFunction(actionId) ||
                // 預約保修
                actionId == WheelTileItem.ACTION_RESERVATION_PROGRESS ||
                actionId == WheelTileItem.ACTION_RESERVATION_RECORD || actionId == WheelTileItem.ACTION_YOUR_RESERVATION ||

                // 一鍵服務
                actionId == WheelTileItem.ACTION_CAR_ACCIDENT || actionId == WheelTileItem.ACTION_CAR_THEFT ||
                actionId == WheelTileItem.ACTION_CLICK_SERVICE ||
                actionId == WheelTileItem.ACTION_SERVER || actionId == WheelTileItem.ACTION_SALES_CONSULTANT ||
                actionId == WheelTileItem.ACTION_110 || actionId == WheelTileItem.ACTION_RESCUE ||
                actionId == WheelTileItem.ACTION_SECRATERY || actionId == WheelTileItem.ACTION_RESERVATION ||
                actionId == WheelTileItem.ACTION_DRIVER_HELP;
    }

    private static boolean isVigRelatedFunction(int actionId) {
        return actionId == WheelTileItem.ACTION_AR_REMOTE || actionId == WheelTileItem.ACTION_FIND_CAR ||
                actionId == WheelTileItem.ACTION_CAR_LOCK || actionId == WheelTileItem.ACTION_CAR_UNLOCK ||
                actionId == WheelTileItem.ACTION_BACKDOOR_OPEN || actionId == WheelTileItem.ACTION_FRESH_AIR ||
                actionId == WheelTileItem.ACTION_ENGINE_OFF ||

                // 車輛狀態
                actionId == WheelTileItem.ACTION_CAR_STATUS ||

                // 數位鑰匙
                actionId == WheelTileItem.ACTION_DIGITAL_KEY;
    }

    public static boolean shouldHaveThisItem(boolean isDMS, boolean isVig, int actionId) {
        if (isDMS) {
            if (isVig) {
                return true;
            } else {
                // only add non-vig related function to non-vig car
                return !isVigRelatedFunction(actionId);
            }
        } else {
            // only add non-dms related function to non-dms user
            return !isDmsRelatedFunction(actionId);
        }
    }
}
