package com.luxgen.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.luxgen.remote.util.Hex;

public class Beacon implements Parcelable {

    private final static int DETECTABLE_RSSI_THRESHOLD = -120;
    private final static int EXPIRED_THRESHOLD = 3 * 1000; // 3 seconds

    private String mUUID;
    private int mMajor;
    private int mMinor;
    private int mTxPower;

    private int mRssi;
    private String mMac;
    private long mCreateTimestamp;

    private Beacon(Parcel in) {
        mUUID = in.readString();
        mMajor = in.readInt();
        mMinor = in.readInt();
        mTxPower = in.readInt();
        mRssi = in.readInt();
        mMac = in.readString();
        mCreateTimestamp = in.readLong();
    }

    public static final Creator<Beacon> CREATOR = new Creator<Beacon>() {
        @Override
        public Beacon createFromParcel(Parcel in) {
            return new Beacon(in);
        }

        @Override
        public Beacon[] newArray(int size) {
            return new Beacon[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mUUID);
        dest.writeInt(mMajor);
        dest.writeInt(mMinor);
        dest.writeInt(mTxPower);
        dest.writeInt(mRssi);
        dest.writeString(mMac);
        dest.writeLong(mCreateTimestamp);
    }

    public static final class Builder {
        private final static int UUID_LENGTH = 16;
        private int startByte = 5;
        private byte[] mData;
        private int mRSSI = 0;
        private String mAddress = null;

        private boolean isBeacon(byte[] data) {
            return ((int) data[startByte + 2] & 0xff) == 0x02 && ((int) data[startByte + 3] & 0xff) == 0x15;
        }

        private String extractUUID(byte[] data) {
            byte[] uuidBytes = new byte[UUID_LENGTH];
            System.arraycopy(data, startByte + 4, uuidBytes, 0, UUID_LENGTH);
            String hexString = Hex.fromByteArray(uuidBytes);

            return hexString.substring(0, 8) + "-" + hexString.substring(8, 12) + "-"
                    + hexString.substring(12, 16) + "-" + hexString.substring(16, 20) + "-"
                    + hexString.substring(20, 32);
        }

        private int extractMajorNumber(byte[] data) {
            return (data[startByte + 20] & 0xff) * 0x100 + (data[startByte + 21] & 0xff);
        }

        private int extractMinorNumber(byte[] data) {
            return (data[startByte + 22] & 0xff) * 0x100 + (data[startByte + 23] & 0xff);
        }

        private int extractTxPower(byte[] data) {
            return (data[startByte + 24]);
        }

        public Builder setData(byte[] data) {
            mData = data;
            return this;
        }

        public Builder setRssi(int rssi) {
            mRSSI = rssi;
            return this;
        }

        public Builder setAddress(String address) {
            mAddress = address;
            return this;
        }

        public Beacon build() {
            String uuid = null;
            int major = -1, minor = -1, txPower = -1;
            if (mData != null && isBeacon(mData)) {
                uuid = extractUUID(mData);
                major = extractMajorNumber(mData);
                minor = extractMinorNumber(mData);
                txPower = extractTxPower(mData);
            }

            Beacon beacon = new Beacon(uuid, major, minor);
            beacon.setTxPower(txPower);
            if (mRSSI != 0) {
                beacon.setRssi(mRSSI);
            }
            if (mAddress != null) {
                beacon.setMac(mAddress);
            }
            return beacon;
        }
    }

    public Beacon(String uuid, int major, int minor) {
        mUUID = uuid;
        mMajor = major;
        mMinor = minor;
        mCreateTimestamp = System.currentTimeMillis();
    }

    public void setTxPower(int power) {
        mTxPower = power;
    }

    public void setRssi(int rssi) {
        mRssi = rssi;
    }

    public void setMac(String mac) {
        mMac = mac;
    }

    public String getUUID() {
        return mUUID;
    }

    public int getMajor() {
        return mMajor;
    }

    public int getMinor() {
        return mMinor;
    }

    public int getRssi() {
        return mRssi;
    }

    public String getMac() {
        return mMac;
    }

    public boolean isValid() {
        return mMajor > 0 && mMinor > 0 && mUUID != null;
    }

    public boolean isNearby() {
        return mRssi > DETECTABLE_RSSI_THRESHOLD;
    }

    public boolean equals(Beacon beacon) {
        return beacon != null && beacon.getMajor() == mMajor && beacon.getMinor() == mMinor && beacon.getUUID().equals(mUUID);
    }

    public boolean isOld() {
        return (System.currentTimeMillis() - mCreateTimestamp > EXPIRED_THRESHOLD);
    }

    public int getEffectiveRssi() {
        if (isOld()) {
            return 0;
        }

        return mRssi;
    }
}
