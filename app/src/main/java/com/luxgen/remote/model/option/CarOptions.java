package com.luxgen.remote.model.option;

import android.support.annotation.IntDef;
import android.support.annotation.StringDef;
import android.text.TextUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class CarOptions {
    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            CAR_OWNER, CAR_DRIVER, CAR_NO
    })
    public @interface CarMaster {
    }

    /**
     * Owner
     */
    public static final String CAR_OWNER = "O";
    /**
     * Driver
     */
    public static final String CAR_DRIVER = "D";
    /**
     * 未配發
     */
    public static final String CAR_NO = "N";

    /**
     * 鑰匙的配置分類
     */
    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            KEY_M, KEY_M1, KEY_M2, KEY_S1, KEY_S2, KEY_T1, KEY_T2, KEY_NO, KEY_DELETING
    })
    public @interface KeyType {
    }

    /**
     * 主 Key
     */
    public static final String KEY_M = "M"; // No longer use
    /**
     * 主 Key
     */
    public static final String KEY_M1 = "M1";
    /**
     * 主 Key
     */
    public static final String KEY_M2 = "M2";
    /**
     * 副Key已配，未領
     */
    public static final String KEY_S1 = "S1";
    /**
     * 副Key已領
     */
    public static final String KEY_S2 = "S2";
    /**
     * 臨時Key，未領
     */
    public static final String KEY_T1 = "T1";
    /**
     * 臨時Key，已領
     */
    public static final String KEY_T2 = "T2";
    /**
     * 未配副
     */
    public static final String KEY_NO = "N";

    public static final String KEY_DELETING = "D1";

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({
            NORMAL, ABNORMAL_1, ABNORMAL_2, NON
    })
    public @interface CarStatus {

    }

    /**
     * 正常
     */
    public static final int NORMAL = 0;
    /**
     * 異常
     */
    public static final int ABNORMAL_1 = 1;
    /**
     * 異常
     */
    public static final int ABNORMAL_2 = 2;
    /**
     * 沒有狀態
     */
    public static final int NON = -1;

    public static @CarStatus
    int getCloseStatus(Integer value) {
        if (value == null || value == NON) {
            return NON;
        } else if (value == 0) {
            return NORMAL;
        } else {
            return ABNORMAL_1;
        }
    }

    public static @CarStatus
    int getLockStatus(Integer value) {
        if (value == null || value == NON) {
            return NON;
        } else if (value == 1) {
            return NORMAL;
        } else {
            return ABNORMAL_1;
        }
    }

    public static @CarStatus
    int getSwitchStatus(Integer value) {
        if (value == null || value == NON) {
            return NON;
        } else if (value == 1) {
            return NORMAL;
        } else {
            return ABNORMAL_1;
        }
    }

    public static @CarStatus
    int getErrStatus(String value) {
        if (TextUtils.isEmpty(value) || value.equals(String.valueOf(NON))) {
            return NON;
        } else if (value.equals(String.valueOf(0))) {
            return NORMAL;
        } else {
            return ABNORMAL_1;
        }
    }

    public static @CarStatus
    int getLowSocStatus(String value) {
        if (TextUtils.isEmpty(value) || value.equals(String.valueOf(NON))) {
            return NON;
        } else if (value.equals(String.valueOf(0))) {
            return NORMAL;
        } else if (value.equals(String.valueOf(1))) {
            return ABNORMAL_1;
        } else {
            return ABNORMAL_2;
        }
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            DELETE_START, DELETE_PROCESSING, DELETE_COMPLETE, DELETE_NO_DATA
    })
    public @interface DeleteStatus {
    }

    /**
     * Start
     */
    public static final String DELETE_START = "S";
    /**
     * Complete
     */
    public static final String DELETE_COMPLETE = "C";
    /**
     * Processing
     */
    public static final String DELETE_PROCESSING = "P";
    /**
     * No data
     */
    public static final String DELETE_NO_DATA = "N";

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({
            KEY_STATUS_DEF,
            KEY_STATUS_AP1, KEY_STATUS_AP2, KEY_STATUS_AP2S1, KEY_STATUS_S1, KEY_STATUS_S2,
            KEY_STATUS_DP1, KEY_STATUS_DP2, KEY_STATUS_DP2D1, KEY_STATUS_D1,
            KEY_STATUS_SNAP_INIT, KEY_STATUS_SNAP_NORMAL, KEY_STATUS_T1, KEY_STATUS_T2, KEY_STATUS_SNAP_TERMINATE
    })
    public @interface KeyStatus {
    }

    /** Initial value */
    public static final int KEY_STATUS_DEF = -1;
    /** 新增鑰匙 Prepare 成功前*/
    public static final int KEY_STATUS_AP1 = 1;
    /** 新增鑰匙 Prepare 成功後*/
    public static final int KEY_STATUS_AP2 = 2;
    /** 新增鑰匙 近端成功*/
    public static final int KEY_STATUS_AP2S1 = 3;
    /** 副鑰匙 未領取*/
    public static final int KEY_STATUS_S1 = 4;
    /** 副鑰匙 已領取*/
    public static final int KEY_STATUS_S2 = 5;
    /** 刪除鑰匙 prepare 成功前 */
    public static final int KEY_STATUS_DP1 = 6;
    /** 刪除鑰匙 prepare 成功後 */
    public static final int KEY_STATUS_DP2 = 7;
    /** 刪除鑰匙 近端成功 */
    public static final int KEY_STATUS_DP2D1 = 8;
    /** 刪除鑰匙 遠端成功，刪除中 */
    public static final int KEY_STATUS_D1 = 9;
    /** 刪除鑰匙 遠端成功，已刪除 */
    // 已刪除沒有這把鑰匙，不用記狀態

    /** 臨時鑰匙沒有SnapTickets的狀態 */
    public static final int KEY_STATUS_SNAP_INIT = 10;
    /** 臨時鑰匙有SnapTickets的狀態 */
    public static final int KEY_STATUS_SNAP_NORMAL = 11;
    /** 副鑰匙 未領取*/
    public static final int KEY_STATUS_T1 = 12;
    /** 副鑰匙 已領取*/
    public static final int KEY_STATUS_T2 = 13;
    /** 臨時鑰匙過期的狀態 */
    public static final int KEY_STATUS_SNAP_TERMINATE = 14;


    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            MASTER, SUB, SNAP
    })
    public @interface KeyCategory {

    }

    public static final String MASTER = "master_key";
    public static final String SUB = "sub_key";
    public static final String SNAP = "snap_key";
}
