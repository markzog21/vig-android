package com.luxgen.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

public class WheelPage implements Parcelable {

    private final static int MAX_ITEMS = 5;
    private WheelTileItem[] mItems = new WheelTileItem[MAX_ITEMS];

    private WheelPage(Parcel in) {
        mItems = in.createTypedArray(WheelTileItem.CREATOR);
    }

    public static final Creator<WheelPage> CREATOR = new Creator<WheelPage>() {
        @Override
        public WheelPage createFromParcel(Parcel in) {
            return new WheelPage(in);
        }

        @Override
        public WheelPage[] newArray(int size) {
            return new WheelPage[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedArray(mItems, 0);
    }

    public WheelPage(WheelTileItem[] items) {
        if (items == null) {
            return;
        }

        int length = items.length > MAX_ITEMS ? MAX_ITEMS : items.length;
        System.arraycopy(items, 0, mItems, 0, length);
    }

    public WheelTileItem[] getItems() {
        return mItems;
    }

    public boolean setItem(WheelTileItem item, int pos) {
        if (pos >= 0 && pos < MAX_ITEMS) {
            mItems[pos] = item;
            return true;
        }
        return false;
    }
}
