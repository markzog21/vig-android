package com.luxgen.remote.model;

public class VigDevice {

    private String mAddress;
    private int mRssi;

    public VigDevice(String address) {
        mAddress = address;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setRssi(int rssi) {
        mRssi = rssi;
    }

    public int getRssi() {
        return mRssi;
    }
}
