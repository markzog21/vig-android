package com.luxgen.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MaintenanceData implements Parcelable {

    private long mDate;
    private String mPlant;
    private String mAddress;
    private String mPhone;

    private String mPlate;
    private String mMileage;
    private boolean mMaintenance;
    private boolean mRepair;
    private String mNotes;
    private String mDetail;

    private MaintenanceData(Parcel in) {
        mDate = in.readLong();
        mPlant = in.readString();
        mAddress = in.readString();
        mPhone = in.readString();
        mPlate = in.readString();
        mMileage = in.readString();
        mMaintenance = (in.readInt() == 1);
        mRepair = (in.readInt() == 1);
        mNotes = in.readString();
        mDetail = in.readString();
    }

    public static final Creator<MaintenanceData> CREATOR = new Creator<MaintenanceData>() {
        @Override
        public MaintenanceData createFromParcel(Parcel in) {
            return new MaintenanceData(in);
        }

        @Override
        public MaintenanceData[] newArray(int size) {
            return new MaintenanceData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mDate);
        dest.writeString(mPlant);
        dest.writeString(mAddress);
        dest.writeString(mPhone);
        dest.writeString(mPlate);
        dest.writeString(mMileage);
        dest.writeInt(mMaintenance ? 1 : 0);
        dest.writeInt(mRepair ? 1 : 0);
        dest.writeString(mNotes);
        dest.writeString(mDetail);
    }

    public MaintenanceData(long date, String plant, String address, String phone, String plate, String mileage, boolean maintenance, boolean repair, String notes, String detail) {
        this.mDate = date;
        this.mPlant = plant;
        this.mAddress = address;
        this.mPhone = phone;
        this.mPlate = plate;
        this.mMileage = mileage;
        this.mMaintenance = maintenance;
        this.mRepair = repair;
        this.mNotes = notes;
        this.mDetail = detail;
    }

    public long getDate() {
        return mDate;
    }

    public String getPlant() {
        return mPlant;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getPhone() {
        return mPhone;
    }

    public String getPlate() {
        return mPlate;
    }

    public String getMileage() {
        return mMileage;
    }

    public boolean isMaintenance() {
        return mMaintenance;
    }

    public boolean isRepair() {
        return mRepair;
    }

    public String getNotes() {
        return mNotes;
    }

    public String getDetail() {
        return mDetail;
    }
}
