package com.luxgen.remote.model.option;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class VigOptions {
    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            REMOTE_CONTROL_OK,
            REMOTE_CONTROL_FAIL
    })
    public @interface RemoteControlStatus {
    }

    public static final String REMOTE_CONTROL_OK = "S";

    public static final String REMOTE_CONTROL_FAIL = "F";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            REMOTE_CONTROL_NEAR,
            REMOTE_CONTROL_FAR
    })
    public @interface RemoteControlType {
    }

    public static final String REMOTE_CONTROL_NEAR = "N";

    public static final String REMOTE_CONTROL_FAR = "F";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            REMOTE_CONTROL_COMMAND_CLEAN,
            REMOTE_CONTROL_COMMAND_FIND_CAR,
            REMOTE_CONTROL_COMMAND_DOOR,
            REMOTE_CONTROL_COMMAND_LOCK,
            REMOTE_CONTROL_COMMAND_UNLOCK,
            REMOTE_CONTROL_COMMAND_ENGINE_OFF
    })
    public @interface RemoteControlCommand {
    }

    public static final String REMOTE_CONTROL_COMMAND_CLEAN = "AcTemp";

    public static final String REMOTE_CONTROL_COMMAND_FIND_CAR = "honk";

    public static final String REMOTE_CONTROL_COMMAND_DOOR = "bdooron";

    public static final String REMOTE_CONTROL_COMMAND_LOCK = "doorlock";

    public static final String REMOTE_CONTROL_COMMAND_UNLOCK = "doorunlock";

    public static final String REMOTE_CONTROL_COMMAND_ENGINE_OFF = "engineoff";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            WAKE_UP_CONNECTING,
            WAKE_UP_LINKING,
            WAKE_UP_SUCCESS,
            WAKE_UP_TIME_OUT,
            WAKE_UP_FAIL
    })
    public @interface VigWakeUpStatus {
    }

    /**
     * 連接 VIG-Jasper 中
     */
    public static final String WAKE_UP_CONNECTING = "N";
    /**
     * 已連接 VIG-Jasper
     */
    public static final String WAKE_UP_LINKING = "C";
    /**
     * 成功喚醒 VIG-Jasper
     */
    public static final String WAKE_UP_SUCCESS = "Y";
    /**
     * 失敗，喚醒 VIG-Jasper Time out
     */
    public static final String WAKE_UP_TIME_OUT = "E1";
    /**
     * 失敗，嘗試喚醒 VIG-Jasper，但失敗
     */
    public static final String WAKE_UP_FAIL = "E2";
}
