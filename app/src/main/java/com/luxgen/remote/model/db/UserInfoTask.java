package com.luxgen.remote.model.db;

import android.os.AsyncTask;

import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.model.UserInfo;

public class UserInfoTask extends AsyncTask<UserInfo, Void, Void> {

    private static final String LOG_TAG = UserInfoTask.class.getSimpleName();

    private static final int LOGIN = 1;
    private static final int LOGOUT = 2;
    private static final int UPDATE = 3;

    private VigDatabase mDatabase;
    private int mExecType = 0;

    public UserInfoTask(VigDatabase db) {
        mDatabase = db;
    }

    public void login(UserInfo user) {
        mExecType = LOGIN;
        execute(user);
    }

    public void update(UserInfo user) {
        mExecType = UPDATE;
        execute(user);
    }

    @Override
    protected Void doInBackground(UserInfo... userInfos) {
        if (mExecType == LOGIN) {
            LogCat.i(LOG_TAG + ": LOGIN in room db");
        } else if (mExecType == UPDATE) {
            LogCat.i(LOG_TAG + ": LOGIN in room db");
        } else if (mExecType == LOGOUT) {
            LogCat.i(LOG_TAG + ": room db ");
        }
        return null;
    }
}
