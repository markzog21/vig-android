package com.luxgen.remote.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.luxgen.remote.custom.LogCat;
import com.luxgen.remote.model.option.CarOptions;
import com.luxgen.remote.util.AESCipher;
import com.luxgen.remote.util.TimeUtils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

@Entity
public class KeyInfo {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;
    @ColumnInfo(name = "car_id")
    @NonNull
    private String carId = "";
    @ColumnInfo(name = "user_id")
    private String userId;
    @ColumnInfo(name = "device_id")
    private String deviceId;
    @ColumnInfo(name = "phone")
    private String phone;
    @ColumnInfo(name = "email")
    private String email;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "display_name")
    private String displayName;
    @ColumnInfo(name = "position")
    @NonNull
    private int position = 0;
    @ColumnInfo(name = "key_type")
    private String keyType;
    @ColumnInfo(name = "key_status")
    private int keyStatus;
    @ColumnInfo(name = "create_time")
    private long createTime;
    @ColumnInfo(name = "modify_time")
    private long modifyTime;
    @ColumnInfo(name = "device_create_time")
    private long deviceCreateTime;
    @ColumnInfo(name = "snap_key_start_time")
    private long snapKeyStartTime;
    @ColumnInfo(name = "snap_key_end_time")
    private long snapKeyEndTime;

    public KeyInfo() {
        init();
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getCarId() {
        return this.carId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceId() {
        return this.deviceId;
    }

    /**
     * 因應個資法，加密處理
     * */
    public void setPhone(String phone) {
        try {
            this.phone = TextUtils.isEmpty(phone) ? "" : AESCipher.encryptAES(phone);
        } catch (InvalidKeyException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (NoSuchAlgorithmException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (NoSuchPaddingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (UnsupportedEncodingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (InvalidAlgorithmParameterException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (IllegalBlockSizeException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (BadPaddingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        }
    }

    public String getPhone() {
        String phone = "";
        try {
            if (!TextUtils.isEmpty(this.phone)) {
                phone = AESCipher.decryptAES(this.phone);
            }
        } catch (InvalidKeyException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (NoSuchAlgorithmException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (NoSuchPaddingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (UnsupportedEncodingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (InvalidAlgorithmParameterException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (IllegalBlockSizeException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (BadPaddingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        }
        return phone;
    }

    /**
     * 因應個資法，加密處理
     * */
    public void setEmail(String email) {
        this.email = "";
        try {
            if (!TextUtils.isEmpty(email)) {
                AESCipher.encryptAES(email);
            }
        } catch (InvalidKeyException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (NoSuchAlgorithmException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (NoSuchPaddingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (UnsupportedEncodingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (InvalidAlgorithmParameterException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (IllegalBlockSizeException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (BadPaddingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        }
    }

    public String getEmail() {
        String email = "";
        try {
            if (!TextUtils.isEmpty(this.email)) {
                email = AESCipher.decryptAES(this.email);
            }
        } catch (InvalidKeyException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (NoSuchAlgorithmException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (NoSuchPaddingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (UnsupportedEncodingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (InvalidAlgorithmParameterException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (IllegalBlockSizeException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (BadPaddingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        }
        return email;
    }

    /**
     * 因應個資法，加密處理
     * */
    public void setName(String name) {
        try {
            this.name = TextUtils.isEmpty(name) ? "" : AESCipher.encryptAES(name);
        } catch (InvalidKeyException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (NoSuchAlgorithmException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (NoSuchPaddingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (UnsupportedEncodingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (InvalidAlgorithmParameterException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (IllegalBlockSizeException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (BadPaddingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        }
    }

    public String getName() {
        String name = "";
        try {
            if (!TextUtils.isEmpty(this.name)) {
                name = AESCipher.decryptAES(this.name);
            }
        } catch (InvalidKeyException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (NoSuchAlgorithmException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (NoSuchPaddingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (UnsupportedEncodingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (InvalidAlgorithmParameterException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (IllegalBlockSizeException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        } catch (BadPaddingException e) {
            LogCat.e(Arrays.toString(e.getStackTrace()));
        }
        return name;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return this.position;
    }

    public void setKeyType(@CarOptions.KeyType String type) {
        this.keyType = type;
    }

    public String getKeyType() {
        return this.keyType;
    }

    public void setKeyStatus(@CarOptions.KeyStatus int status) {
        this.keyStatus = status;
    }

    public int getKeyStatus() {
        return this.keyStatus;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * 有被設定就給 display name，否則就看 name 是否有資料，
     * 再無，就是給 phone。
     * */
    public String getDisplayName() {
        if (!TextUtils.isEmpty(this.displayName)) {
            return this.displayName;
        } else if (!TextUtils.isEmpty(this.name)) {
            return getName();
        }
        return getPhone();
    }

    public void setModifyTime(String time) throws ParseException {
        setModifyTime(TimeUtils.parseToLong(time));
    }

    public void setModifyTime(long time) {
        this.modifyTime = time;
    }

    public long getModifyTime() {
        return this.modifyTime;
    }

    public void setCreateTime(String time) throws ParseException {
        setCreateTime(TimeUtils.parseToLong(time));
    }

    public void setCreateTime(long time) {
        this.createTime = time;
    }

    public long getCreateTime() {
        return this.createTime;
    }

    public long getDeviceCreateTime() {
        return this.deviceCreateTime;
    }

    public void setDeviceCreateTime(long time) {
        this.deviceCreateTime = time;
    }

    public void setDeviceCreateTime(String time) throws ParseException {
        setDeviceCreateTime(TimeUtils.parseToLong(time));
    }

    public long getSnapKeyStartTime() {
        return this.snapKeyStartTime;
    }

    public void setSnapKeyStartTime(long time) {
        this.snapKeyStartTime = time;
    }

    public void setSnapKeyStartTime(String time) throws ParseException {
        setSnapKeyStartTime(TimeUtils.parseToLong(time));
    }

    public long getSnapKeyEndTime() {
        return this.snapKeyEndTime;
    }

    public void setSnapKeyEndTime(long time) {
        this.snapKeyEndTime = time;
    }

    public void setSnapKeyEndTime(String time) throws ParseException {
        setSnapKeyEndTime(TimeUtils.parseToLong(time));
    }

    public boolean isNew() {
        return this.id == null || this.id < 0;
    }

    /**
     * 初始化，除了 carId、position
     */
    private void init() {
        this.userId = "";
        this.deviceId = "";
        this.phone = "";
        this.email = "";
        this.name = "";
        this.displayName = "";
        this.keyType = "";
        this.keyStatus = CarOptions.KEY_STATUS_DEF;
        this.deviceCreateTime = 0;
        this.snapKeyStartTime = 0;
        this.snapKeyEndTime = 0;
    }

    /**
     * 判斷 key type 是否跟原有不同，並改變原物件的 {@link #keyType}
     *
     * @return <code>true</code> 有變化;
     * <code>false</code> 沒變化
     */
    public boolean checkKeyTypeAndChange(String checkType) {
        if (isKeyTypeChanged(checkType)) {
            setKeyType(checkType);
            return true;
        }
        return false;
    }


    public void clear() {
        init();
    }

    private boolean isKeyTypeChanged(String type) {
        return !TextUtils.isEmpty(type) &&        // 不為空值
                !type.equals(getKeyType()); // 不等同於原本的 keyType)
    }

}
