package com.luxgen.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

public class WorkShop implements Parcelable {
    private String dealerCode;
    private String deptCode;
    private String dept;
    private String phone;
    private String address;

    private WorkShop(Parcel in) {
        dealerCode = in.readString();
        deptCode = in.readString();
        dept = in.readString();
        phone = in.readString();
        address = in.readString();
    }

    public static final Creator<WorkShop> CREATOR = new Creator<WorkShop>() {
        @Override
        public WorkShop createFromParcel(Parcel in) {
            return new WorkShop(in);
        }

        @Override
        public WorkShop[] newArray(int size) {
            return new WorkShop[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dealerCode);
        dest.writeString(deptCode);
        dest.writeString(dept);
        dest.writeString(phone);
        dest.writeString(address);
    }

    public WorkShop(String dealerCode, String deptCode, String dept, String phone, String address) {
        this.dealerCode = dealerCode;
        this.deptCode = deptCode;
        this.dept = dept;
        this.phone = phone;
        this.address = address;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public String getDept() {
        return dept;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }
}
