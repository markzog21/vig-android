package com.luxgen.remote.model.option;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class NotifactionOptions {
    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            READ, UNREAD
    })
    public @interface ReadStatus {
    }

    /**
     * 已讀
     */
    public static final String READ = "R";
    /**
     * 未譯
     */
    public static final String UNREAD = "U";

    /**
     * 已讀刪除
     */
    public static final String DELETE_READ = "DR";

    /**
     * 未讀刪除
     */
    public static final String DELETE_UNREAD = "DU";

    public static boolean isUnRead(String state) {
        return state.equals(UNREAD);
    }

    public static boolean isDelete(String state) {
        return state.equalsIgnoreCase(DELETE_READ) || state.equalsIgnoreCase(DELETE_UNREAD);
    }

    public static String changeDeleteStatus(@ReadStatus String status) {
        if (status.equals(READ)) {
            return DELETE_READ;
        } else if (status.equals(UNREAD)) {
            return DELETE_UNREAD;
        }
        return status;
    }
}
