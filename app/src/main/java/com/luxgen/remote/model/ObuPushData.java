package com.luxgen.remote.model;

import com.google.gson.annotations.SerializedName;

public class ObuPushData {

    @SerializedName("Push_UUID")
    private String mUuid = "";

    @SerializedName("Provider")
    private String mProvider;

    @SerializedName("Title")
    private String mTitle;

    @SerializedName("Contents")
    private String mContents;

    @SerializedName("PushDate")
    private String mDate;

    @SerializedName("ReadStatus")
    private String mReadStatus;

    public ObuPushData(String uuid, String provider, String title, String contents, String data, String readStatus) {
        mUuid = uuid;
        mProvider = provider;
        mTitle = title;
        mContents = contents;
        mDate = data;
        mReadStatus = readStatus;
    }

    public static ObuPushData convert(PushData data) {
        return new ObuPushData(
                data.getUuid(),
                data.getProvider(),
                data.getTitle(),
                data.getContents(),
                data.getDate(),
                data.getReadStatus()
        );
    }
}
