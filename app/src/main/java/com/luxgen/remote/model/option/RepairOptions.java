package com.luxgen.remote.model.option;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class RepairOptions {
    @Retention(RetentionPolicy.SOURCE)
//    @IntDef({
//            PREPARING, MAINTENANCE, CLEANING, CHECKOUT
//    })
    @StringDef({
            PREPARING, MAINTENANCE, CLEANING, CHECKOUT
    })
    public @interface RepairStatus {
    }

    /**
     * 準備中
     */
    public static final String PREPARING = "準備中";
//    public static final int PREPARING = 1;
    /**
     * 維修中
     */
    public static final String MAINTENANCE = "維修中";
//    public static final int MAINTENANCE = 2;
    /**
     * 清潔整備中
     */
    public static final String CLEANING = "清潔整備中";
//    public static final int CLEANING = 3;
    /**
     * 結帳
     */
    public static final String CHECKOUT = "結帳";
//    public static final int CHECKOUT = 4;
}
