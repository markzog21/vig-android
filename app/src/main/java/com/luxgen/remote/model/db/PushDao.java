package com.luxgen.remote.model.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.luxgen.remote.model.PushData;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface PushDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(PushData data);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ArrayList<PushData> list);

    @Query("SELECT * FROM PushData WHERE uuid = :uuid")
    PushData getData(String uuid);

    @Query("SELECT * FROM PushData WHERE status NOT IN ('DR', 'DU')")
    LiveData<List<PushData>> getPushList();

    @Query("SELECT * FROM PushData WHERE status NOT IN ('DR', 'DU')")
    List<PushData> getList();

    @Query("SELECT * FROM PushData WHERE sync = 1")
    List<PushData> getNeedSyncList();

    @Update
    void update(PushData data);

    @Update
    void update(ArrayList<PushData> list);

    @Query("DELETE FROM PushData")
    void clear();
}
