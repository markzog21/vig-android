package com.luxgen.remote.model.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.luxgen.remote.network.model.assistant.UseLogData;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface UseLogDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> insert(ArrayList<UseLogData> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(UseLogData data);

    @Query("SELECT * FROM UseLogData LIMIT 1")
    UseLogData get();

    @Delete
    void clear(UseLogData data);
}
