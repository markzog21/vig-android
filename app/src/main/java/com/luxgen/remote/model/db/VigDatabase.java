package com.luxgen.remote.model.db;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.luxgen.remote.model.KeyInfo;
import com.luxgen.remote.model.PushData;
import com.luxgen.remote.model.UserInfo;
import com.luxgen.remote.network.model.assistant.UseLogData;
import com.luxgen.remote.network.model.car.CarKeyData;

@Database(entities = {PushData.class, UserInfo.class, CarKeyData.class, KeyInfo.class, UseLogData.class}
        , version = 1
        , exportSchema = false)
public abstract class VigDatabase extends RoomDatabase {

    private final static String DB_NAME = "vig_db";

    private static VigDatabase mInstance;

    public static VigDatabase getInstance(Context context) {
        if (mInstance == null) {
            mInstance = Room.databaseBuilder(context.getApplicationContext(),
                    VigDatabase.class, DB_NAME).build();
        }
        return mInstance;
    }

    public abstract PushDao pushModel();

    public abstract UserDao userModel();

    public abstract CarKeyDao carKeyModel();

    public abstract KeyDao keyModel();

    public abstract UseLogDao useLogModel();

    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }
}
