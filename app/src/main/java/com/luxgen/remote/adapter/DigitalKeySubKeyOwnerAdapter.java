package com.luxgen.remote.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomRecyclerViewBaseAdapter;
import com.luxgen.remote.model.KeyInfo;
import com.luxgen.remote.model.option.CarOptions;


public class DigitalKeySubKeyOwnerAdapter extends CustomRecyclerViewBaseAdapter<ViewHolder> {

    public interface OnInnerViewsClickListener {
        void onSubKeyOwnerImageViewClick(DigitalKeySubKeyOwnerAdapter adapter, int position);

        void onSubKeyOwnerPrepareAddingImageViewClick(DigitalKeySubKeyOwnerAdapter adapter, int position);

        void onSubKeyOwnerPrepareDeletingImageViewClick(DigitalKeySubKeyOwnerAdapter adapter, int position);

        void onAddNewImageViewClick(DigitalKeySubKeyOwnerAdapter adapter, int position);

    }

    public static final int DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_NONE = -1;
    public static final int DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_OWNER = 0;
    public static final int DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_OWNER_PREPARE_ADD = 1;
    public static final int DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_OWNER_PREPARE_DELETE = 2;
    public static final int DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_ADDNEW = 3;

    private Drawable mSubKeyS1Drawable = null;
    private Drawable mSubKeyPrepareAddingDrawable = null;
    private Drawable mSubKeyPrepareDeletingDrawable = null;
    private Drawable mSubKeyS2Drawable = null;

    private boolean mIsKeyAddingEnabled = true;

    public static class SubKeyOwnerViewHolder extends ViewHolder {
        ImageView mSubKeyOwnerImageView;
        TextView mSubKeyOwnerNameTextView;

        SubKeyOwnerViewHolder(View view) {
            super(view);

            mSubKeyOwnerImageView = view.findViewById(R.id.recyclerview_item_digital_key_sub_key_owner_image_view);
            mSubKeyOwnerNameTextView = view.findViewById(R.id.recyclerview_item_digital_key_sub_key_owner_name_text_view);
        }
    }

    public static class SubKeyOwnerPrepareAddingViewHolder extends ViewHolder {
        ImageView mSubKeyOwnerPrepareAddingImageView;
        TextView mSubKeyOwnerPrepareAddingNameTextView;

        SubKeyOwnerPrepareAddingViewHolder(View view) {
            super(view);

            mSubKeyOwnerPrepareAddingImageView = view.findViewById(R.id.recyclerview_item_digital_key_sub_key_owner_image_view);
            mSubKeyOwnerPrepareAddingNameTextView = view.findViewById(R.id.recyclerview_item_digital_key_sub_key_owner_name_text_view);
        }
    }

    public static class SubKeyOwnerPrepareDeletingViewHolder extends ViewHolder {
        ImageView mSubKeyOwnerPrepareDeletingImageView;
        TextView mSubKeyOwnerPrepareDeletingNameTextView;

        SubKeyOwnerPrepareDeletingViewHolder(View view) {
            super(view);

            mSubKeyOwnerPrepareDeletingImageView = view.findViewById(R.id.recyclerview_item_digital_key_sub_key_owner_image_view);
            mSubKeyOwnerPrepareDeletingNameTextView = view.findViewById(R.id.recyclerview_item_digital_key_sub_key_owner_name_text_view);
        }
    }

    public static class AddNewViewHolder extends ViewHolder {
        ImageView mAddNewImageView;
        TextView mAddNewTextView;

        AddNewViewHolder(View view) {
            super(view);

            mAddNewImageView = view.findViewById(R.id.recyclerview_item_digital_key_sub_key_add_new_image_view);
            mAddNewTextView = view.findViewById(R.id.recyclerview_item_digital_key_sub_key_add_new_text_view);
        }
    }

    private OnInnerViewsClickListener mOnInnerViewsClickListener = null;

    private OnClickListener mOnSubKeyOwnerImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mOnInnerViewsClickListener != null) {
                mOnInnerViewsClickListener.onSubKeyOwnerImageViewClick(DigitalKeySubKeyOwnerAdapter.this, (Integer) v.getTag());
            }
        }

    };

    private OnClickListener mOnSubKeyOwnerPrepareAddingImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mOnInnerViewsClickListener != null) {
                mOnInnerViewsClickListener.onSubKeyOwnerPrepareAddingImageViewClick(DigitalKeySubKeyOwnerAdapter.this, (Integer) v.getTag());
            }
        }

    };

    private OnClickListener mOnSubKeyOwnerPrepareDeletingImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mOnInnerViewsClickListener != null) {
                mOnInnerViewsClickListener.onSubKeyOwnerPrepareDeletingImageViewClick(DigitalKeySubKeyOwnerAdapter.this, (Integer) v.getTag());
            }
        }

    };

    private OnClickListener mOnAddNewImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mOnInnerViewsClickListener != null) {
                mOnInnerViewsClickListener.onAddNewImageViewClick(DigitalKeySubKeyOwnerAdapter.this, (Integer) v.getTag());
            }
        }

    };

    public DigitalKeySubKeyOwnerAdapter(Context context) {
        super(context);

        mSubKeyS1Drawable = context.getDrawable(R.drawable.btn_key_disable);
        mSubKeyPrepareAddingDrawable = context.getDrawable(R.drawable.btn_key_prepare_adding);
        mSubKeyPrepareDeletingDrawable = context.getDrawable(R.drawable.btn_key_prepare_deleting);
        mSubKeyS2Drawable = context.getDrawable(R.drawable.btn_key_enable);
    }

    public void setOnInnerViewsClickListener(OnInnerViewsClickListener l) {
        mOnInnerViewsClickListener = l;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewHolder viewHolder;

        switch (viewType) {
            case DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_OWNER: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_digital_key_sub_key_owner, parent,
                        false);
                viewHolder = new SubKeyOwnerViewHolder(view);
                break;
            }

            case DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_OWNER_PREPARE_ADD: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_digital_key_sub_key_owner, parent,
                        false);
                viewHolder = new SubKeyOwnerPrepareAddingViewHolder(view);
                break;
            }

            case DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_OWNER_PREPARE_DELETE: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_digital_key_sub_key_owner, parent,
                        false);
                viewHolder = new SubKeyOwnerPrepareDeletingViewHolder(view);
                break;
            }

            case DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_ADDNEW: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_digital_key_sub_key_add_new, parent,
                        false);
                viewHolder = new AddNewViewHolder(view);
                break;
            }

            default: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_digital_key_sub_key_add_new, parent,
                        false);
                viewHolder = new AddNewViewHolder(view);
            }
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        RecyclerViewItemData itemData = mDisplayArrayList.get(position);
        int itemType = itemData.getItemType();
        Object dataObject = itemData.getDataObject();

        switch (itemType) {
            case DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_OWNER: {
                SubKeyOwnerViewHolder subKeyOwnerViewHolder = (SubKeyOwnerViewHolder) viewHolder;
                KeyInfo keyInfo = (KeyInfo) dataObject;

                switch (keyInfo.getKeyStatus()) {
                    case CarOptions.KEY_STATUS_S2: {
                        subKeyOwnerViewHolder.mSubKeyOwnerImageView.setImageDrawable(mSubKeyS2Drawable);

                        String displayName = keyInfo.getDisplayName();
                        if (!TextUtils.isEmpty(displayName)) {
                            subKeyOwnerViewHolder.mSubKeyOwnerNameTextView.setText(displayName);
                        } else {
                            String name = keyInfo.getName();
                            if (!TextUtils.isEmpty(name)) {
                                subKeyOwnerViewHolder.mSubKeyOwnerNameTextView.setText(name);
                            } else {
                                subKeyOwnerViewHolder.mSubKeyOwnerNameTextView.setText(keyInfo.getPhone());
                            }
                        }
                        break;
                    }

                    case CarOptions.KEY_STATUS_S1: {
                        subKeyOwnerViewHolder.mSubKeyOwnerImageView.setImageDrawable(mSubKeyS1Drawable);

                        String nameFormat = mContext.getString(R.string.digital_key_master_key_sub_key_s1_label);
                        String displayName = keyInfo.getDisplayName();
                        if (!TextUtils.isEmpty(displayName)) {
                            nameFormat = String.format(nameFormat, displayName);
                            subKeyOwnerViewHolder.mSubKeyOwnerNameTextView.setText(nameFormat);
                        } else {
                            String name = keyInfo.getName();
                            if (!TextUtils.isEmpty(name)) {
                                nameFormat = String.format(nameFormat, name);
                                subKeyOwnerViewHolder.mSubKeyOwnerNameTextView.setText(nameFormat);
                            } else {
                                nameFormat = String.format(nameFormat, keyInfo.getPhone());
                                subKeyOwnerViewHolder.mSubKeyOwnerNameTextView.setText(nameFormat);
                            }
                        }
                        break;
                    }

                    case CarOptions.KEY_STATUS_AP1: {
                        subKeyOwnerViewHolder.mSubKeyOwnerImageView.setImageDrawable(mSubKeyPrepareAddingDrawable);
                        subKeyOwnerViewHolder.mSubKeyOwnerNameTextView.setText(mContext.getString(R.string.digital_key_master_key_sub_key_ap1_label));
                        break;
                    }

                    case CarOptions.KEY_STATUS_AP2S1: {
                        subKeyOwnerViewHolder.mSubKeyOwnerImageView.setImageDrawable(mSubKeyPrepareAddingDrawable);
                        subKeyOwnerViewHolder.mSubKeyOwnerNameTextView.setText(mContext.getString(R.string.digital_key_master_key_sub_key_ap2s1_label));
                        break;
                    }

                    case CarOptions.KEY_STATUS_D1: {
                        subKeyOwnerViewHolder.mSubKeyOwnerImageView.setImageDrawable(mSubKeyPrepareDeletingDrawable);

                        String nameFormat = mContext.getString(R.string.digital_key_master_key_sub_key_d1_label);
                        String displayName = keyInfo.getDisplayName();
                        if (!TextUtils.isEmpty(displayName)) {
                            nameFormat = String.format(nameFormat, displayName);
                            subKeyOwnerViewHolder.mSubKeyOwnerNameTextView.setText(nameFormat);
                        } else {
                            String name = keyInfo.getName();
                            if (!TextUtils.isEmpty(name)) {
                                nameFormat = String.format(nameFormat, name);
                                subKeyOwnerViewHolder.mSubKeyOwnerNameTextView.setText(nameFormat);
                            } else {
                                nameFormat = String.format(nameFormat, keyInfo.getPhone());
                                subKeyOwnerViewHolder.mSubKeyOwnerNameTextView.setText(nameFormat);
                            }
                        }
                        break;
                    }

                    case CarOptions.KEY_STATUS_DP1: {
                        subKeyOwnerViewHolder.mSubKeyOwnerImageView.setImageDrawable(mSubKeyPrepareDeletingDrawable);
                        subKeyOwnerViewHolder.mSubKeyOwnerNameTextView.setText(mContext.getString(R.string.digital_key_master_key_sub_key_dp1_label));
                        break;
                    }

                    case CarOptions.KEY_STATUS_DP2D1: {
                        subKeyOwnerViewHolder.mSubKeyOwnerImageView.setImageDrawable(mSubKeyPrepareDeletingDrawable);
                        subKeyOwnerViewHolder.mSubKeyOwnerNameTextView.setText(mContext.getString(R.string.digital_key_master_key_sub_key_dp2d1_label));
                        break;
                    }
                }

                subKeyOwnerViewHolder.mSubKeyOwnerImageView.setOnClickListener(mOnSubKeyOwnerImageViewClickListener);
                subKeyOwnerViewHolder.mSubKeyOwnerImageView.setTag(position);

                break;
            }

            case DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_OWNER_PREPARE_ADD: {
                SubKeyOwnerPrepareAddingViewHolder subKeyOwnerPrepareAddingViewHolder = (SubKeyOwnerPrepareAddingViewHolder) viewHolder;
                KeyInfo keyInfo = (KeyInfo) dataObject;

                subKeyOwnerPrepareAddingViewHolder.mSubKeyOwnerPrepareAddingImageView.setImageDrawable(mSubKeyPrepareAddingDrawable);
                subKeyOwnerPrepareAddingViewHolder.mSubKeyOwnerPrepareAddingNameTextView.setText(mContext.getString(R.string.digital_key_master_key_sub_key_ap2_label));

                subKeyOwnerPrepareAddingViewHolder.mSubKeyOwnerPrepareAddingImageView.setOnClickListener(mOnSubKeyOwnerPrepareAddingImageViewClickListener);
                subKeyOwnerPrepareAddingViewHolder.mSubKeyOwnerPrepareAddingImageView.setTag(position);

                break;
            }

            case DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_OWNER_PREPARE_DELETE: {
                SubKeyOwnerPrepareDeletingViewHolder subKeyOwnerPrepareDeletingViewHolder = (SubKeyOwnerPrepareDeletingViewHolder) viewHolder;
                KeyInfo keyInfo = (KeyInfo) dataObject;

                subKeyOwnerPrepareDeletingViewHolder.mSubKeyOwnerPrepareDeletingImageView.setImageDrawable(mSubKeyPrepareDeletingDrawable);
                subKeyOwnerPrepareDeletingViewHolder.mSubKeyOwnerPrepareDeletingNameTextView.setText(mContext.getString(R.string.digital_key_master_key_sub_key_dp2_label));

                subKeyOwnerPrepareDeletingViewHolder.mSubKeyOwnerPrepareDeletingImageView.setOnClickListener(mOnSubKeyOwnerPrepareDeletingImageViewClickListener);
                subKeyOwnerPrepareDeletingViewHolder.mSubKeyOwnerPrepareDeletingImageView.setTag(position);

                break;
            }

            case DIGITALKEY_SUBKEY_OWNER_ITEM_TYPE_ADDNEW: {
                AddNewViewHolder addNewViewHolder = (AddNewViewHolder) viewHolder;

                addNewViewHolder.mAddNewImageView.setTag(position);
                addNewViewHolder.mAddNewTextView.setText(String.format(mContext.getString(R.string.digital_key_master_key_add_new_sub_key_label), position + 1));

                if (mIsKeyAddingEnabled) {
                    addNewViewHolder.mAddNewImageView.setImageResource(R.drawable.btn_add_new_key);
                    addNewViewHolder.mAddNewImageView.setOnClickListener(mOnAddNewImageViewClickListener);
                    addNewViewHolder.mAddNewTextView.setTextColor(Color.WHITE);
                } else {
                    addNewViewHolder.mAddNewImageView.setImageResource(R.drawable.btn_add_new_key_disable);
                    addNewViewHolder.mAddNewImageView.setOnClickListener(null);
                    addNewViewHolder.mAddNewTextView.setTextColor(Color.GRAY);
                }

                break;
            }

            default: {

            }
        }

    }

    public void enableKeyAdding(boolean enable) {
        mIsKeyAddingEnabled = enable;
    }

}
