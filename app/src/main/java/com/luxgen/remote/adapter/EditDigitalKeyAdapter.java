package com.luxgen.remote.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CommonDefs;
import com.luxgen.remote.custom.CustomRecyclerViewBaseAdapter;
import com.luxgen.remote.custom.DataKeys;
import com.luxgen.remote.model.KeyInfo;
import com.luxgen.remote.util.LanguageUtils;
import com.luxgen.remote.util.Prefs;

import org.json.JSONException;
import org.json.JSONObject;


public class EditDigitalKeyAdapter extends CustomRecyclerViewBaseAdapter<ViewHolder> {

    public interface OnInnerViewsClickListener {
        void onSubKeyPrepareDeleteImageViewClick(EditDigitalKeyAdapter adapter, int position);

        void onSubKeyDeleteImageViewClick(EditDigitalKeyAdapter adapter, int position);

        void onTempKeyDeleteImageViewClick(EditDigitalKeyAdapter adapter, int position);

        void onCheckImageViewClick(KeyInfo changedKeyInfo);

    }

    public static final int EDITDIGITALKEY_ITEM_TYPE_NONE = -1;
    public static final int EDITDIGITALKEY_ITEM_TYPE_LABEL = 0;
    public static final int EDITDIGITALKEY_ITEM_TYPE_SUBKEY_PREPARE = 1;
    public static final int EDITDIGITALKEY_ITEM_TYPE_SUBKEY = 2;
    public static final int EDITDIGITALKEY_ITEM_TYPE_TEMPKEY = 3;
    public static final int EDITDIGITALKEY_ITEM_TYPE_DIVIDER = 4;

    private int mInEditingPosition = -1;

    public static class LabelViewHolder extends ViewHolder {
        TextView mLabelTextView;

        LabelViewHolder(View view) {
            super(view);

            mLabelTextView = view.findViewById(R.id.recyclerview_item_edit_digital_key_label_text_view);
        }
    }

    public static class SubKeyPrepareViewHolder extends ViewHolder {
        ViewGroup mDisplayLayout;
        TextView mNameTextView;
        ImageView mDeleteImageView;
        ViewGroup mEditLayout;
        EditText mNameEditText;
        ImageView mCheckImageView;
        ImageView mCancelImageView;

        SubKeyPrepareViewHolder(View view) {
            super(view);

            mDisplayLayout = view.findViewById(R.id.recyclerview_item_edit_digital_key_display_layout);
            mNameTextView = view.findViewById(R.id.recyclerview_item_edit_digital_key_key_name_text_view);
            mDeleteImageView = view.findViewById(R.id.recyclerview_item_edit_digital_key_key_delete_image_view);
            mEditLayout = view.findViewById(R.id.recyclerview_item_edit_digital_key_edit_layout);
            mNameEditText = view.findViewById(R.id.recyclerview_item_edit_digital_key_key_name_edit_text);
            mNameEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (!hasFocus) {
                        ((EditText) v).setError(null);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    } else {
                        imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
                    }
                }
            });
            mCheckImageView = view.findViewById(R.id.recyclerview_item_edit_digital_key_key_check_image_view);
            mCancelImageView = view.findViewById(R.id.recyclerview_item_edit_digital_key_key_cancel_image_view);
        }
    }

    public static class SubKeyViewHolder extends ViewHolder {
        ViewGroup mDisplayLayout;
        TextView mNameTextView;
        ImageView mDeleteImageView;
        ViewGroup mEditLayout;
        EditText mNameEditText;
        ImageView mCheckImageView;
        ImageView mCancelImageView;

        SubKeyViewHolder(View view) {
            super(view);

            mDisplayLayout = view.findViewById(R.id.recyclerview_item_edit_digital_key_display_layout);
            mNameTextView = view.findViewById(R.id.recyclerview_item_edit_digital_key_key_name_text_view);
            mDeleteImageView = view.findViewById(R.id.recyclerview_item_edit_digital_key_key_delete_image_view);
            mEditLayout = view.findViewById(R.id.recyclerview_item_edit_digital_key_edit_layout);
            mNameEditText = view.findViewById(R.id.recyclerview_item_edit_digital_key_key_name_edit_text);
            mNameEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (!hasFocus) {
                        ((EditText) v).setError(null);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    } else {
                        imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
                    }
                }
            });
            mCheckImageView = view.findViewById(R.id.recyclerview_item_edit_digital_key_key_check_image_view);
            mCancelImageView = view.findViewById(R.id.recyclerview_item_edit_digital_key_key_cancel_image_view);
        }
    }

    public static class TempKeyViewHolder extends ViewHolder {
        ViewGroup mDisplayLayout;
        TextView mNameTextView;
        ImageView mDeleteImageView;
        ViewGroup mEditLayout;
        EditText mNameEditText;
        ImageView mCheckImageView;
        ImageView mCancelImageView;

        TempKeyViewHolder(View view) {
            super(view);

            mDisplayLayout = view.findViewById(R.id.recyclerview_item_edit_digital_key_display_layout);
            mNameTextView = view.findViewById(R.id.recyclerview_item_edit_digital_key_key_name_text_view);
            mDeleteImageView = view.findViewById(R.id.recyclerview_item_edit_digital_key_key_delete_image_view);
            mEditLayout = view.findViewById(R.id.recyclerview_item_edit_digital_key_edit_layout);
            mNameEditText = view.findViewById(R.id.recyclerview_item_edit_digital_key_key_name_edit_text);
            mNameEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (!hasFocus) {
                        ((EditText) v).setError(null);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    } else {
                        imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
                    }
                }
            });
            mCheckImageView = view.findViewById(R.id.recyclerview_item_edit_digital_key_key_check_image_view);
            mCancelImageView = view.findViewById(R.id.recyclerview_item_edit_digital_key_key_cancel_image_view);
        }
    }

    public static class DividerViewHolder extends ViewHolder {

        DividerViewHolder(View view) {
            super(view);
        }
    }

    private OnInnerViewsClickListener mOnInnerViewsClickListener = null;

    private OnClickListener mOnSubKeyPrepareDeleteImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mOnInnerViewsClickListener != null) {
                mOnInnerViewsClickListener.onSubKeyPrepareDeleteImageViewClick(EditDigitalKeyAdapter.this, (Integer) v.getTag());
            }
        }

    };

    private OnClickListener mOnSubKeyDeleteImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mOnInnerViewsClickListener != null) {
                mOnInnerViewsClickListener.onSubKeyDeleteImageViewClick(EditDigitalKeyAdapter.this, (Integer) v.getTag());
            }
        }

    };

    private OnClickListener mOnTempKeyDeleteImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mOnInnerViewsClickListener != null) {
                mOnInnerViewsClickListener.onTempKeyDeleteImageViewClick(EditDigitalKeyAdapter.this, (Integer) v.getTag());
            }
        }

    };

    private OnClickListener mOnSubKeyPrepareCheckImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mOnInnerViewsClickListener != null) {
                KeyInfo keyInfo = (KeyInfo) getItem(mInEditingPosition).getDataObject();
                SubKeyPrepareViewHolder viewHolder = (SubKeyPrepareViewHolder) v.getTag();
                String displayName = viewHolder.mNameEditText.getText().toString().trim();
                int cnCount = LanguageUtils.getChineseCharacterCount(displayName);
                int enCount = displayName.length() - cnCount;
                if (TextUtils.isEmpty(displayName)) {
                    keyInfo.setDisplayName(displayName);
                    mOnInnerViewsClickListener.onCheckImageViewClick(keyInfo);
                    mInEditingPosition = -1;
                    EditDigitalKeyAdapter.this.notifyDataSetChanged();
                    Prefs.setRefreshDigitalKeyListFlag(mContext, true);
                } else if ((cnCount * 2 + enCount) > CommonDefs.DIGITAL_KEY_DISPLAYNAME_MAX_LENGTH) {
                    viewHolder.mNameEditText.requestFocus();
                    viewHolder.mNameEditText.setError(mContext.getString(R.string.edit_digital_key_edit_owner_name_hint));
                } else {
                    keyInfo.setDisplayName(displayName);
                    mOnInnerViewsClickListener.onCheckImageViewClick(keyInfo);
                    mInEditingPosition = -1;
                    EditDigitalKeyAdapter.this.notifyDataSetChanged();
                    Prefs.setRefreshDigitalKeyListFlag(mContext, true);
                }
            }
        }

    };

    private OnClickListener mOnSubKeyCheckImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mOnInnerViewsClickListener != null) {
                KeyInfo keyInfo = (KeyInfo) getItem(mInEditingPosition).getDataObject();
                SubKeyViewHolder viewHolder = (SubKeyViewHolder) v.getTag();
                String displayName = viewHolder.mNameEditText.getText().toString().trim();
                int cnCount = LanguageUtils.getChineseCharacterCount(displayName);
                int enCount = displayName.length() - cnCount;
                if (TextUtils.isEmpty(displayName)) {
                    keyInfo.setDisplayName(displayName);
                    mOnInnerViewsClickListener.onCheckImageViewClick(keyInfo);
                    mInEditingPosition = -1;
                    EditDigitalKeyAdapter.this.notifyDataSetChanged();
                    Prefs.setRefreshDigitalKeyListFlag(mContext, true);
                } else if ((cnCount * 2 + enCount) > CommonDefs.DIGITAL_KEY_DISPLAYNAME_MAX_LENGTH) {
                    viewHolder.mNameEditText.requestFocus();
                    viewHolder.mNameEditText.setError(mContext.getString(R.string.edit_digital_key_edit_owner_name_hint));
                } else {
                    keyInfo.setDisplayName(displayName);
                    mOnInnerViewsClickListener.onCheckImageViewClick(keyInfo);
                    mInEditingPosition = -1;
                    EditDigitalKeyAdapter.this.notifyDataSetChanged();
                    Prefs.setRefreshDigitalKeyListFlag(mContext, true);
                }
            }
        }

    };

    private OnClickListener mOnTempKeyCheckImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mOnInnerViewsClickListener != null) {
                KeyInfo keyInfo = (KeyInfo) getItem(mInEditingPosition).getDataObject();
                TempKeyViewHolder viewHolder = (TempKeyViewHolder) v.getTag();
                String displayName = viewHolder.mNameEditText.getText().toString().trim();
                int cnCount = LanguageUtils.getChineseCharacterCount(displayName);
                int enCount = displayName.length() - cnCount;
                if (TextUtils.isEmpty(displayName)) {
                    keyInfo.setDisplayName(displayName);
                    mOnInnerViewsClickListener.onCheckImageViewClick(keyInfo);
                    mInEditingPosition = -1;
                    EditDigitalKeyAdapter.this.notifyDataSetChanged();
                    Prefs.setRefreshDigitalKeyListFlag(mContext, true);
                } else if ((cnCount * 2 + enCount) > CommonDefs.DIGITAL_KEY_DISPLAYNAME_MAX_LENGTH) {
                    viewHolder.mNameEditText.requestFocus();
                    viewHolder.mNameEditText.setError(mContext.getString(R.string.edit_digital_key_edit_owner_name_hint));
                } else {
                    keyInfo.setDisplayName(displayName);
                    mOnInnerViewsClickListener.onCheckImageViewClick(keyInfo);
                    mInEditingPosition = -1;
                    EditDigitalKeyAdapter.this.notifyDataSetChanged();
                    Prefs.setRefreshDigitalKeyListFlag(mContext, true);
                }
            }
        }

    };

    private OnClickListener mOnNameTextViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mInEditingPosition = (int) v.getTag();
            EditDigitalKeyAdapter.this.notifyDataSetChanged();
        }

    };

    private OnClickListener mOnCancelImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mInEditingPosition = -1;
            EditDigitalKeyAdapter.this.notifyDataSetChanged();
        }

    };

    public EditDigitalKeyAdapter(Context context) {
        super(context);
    }

    public void setOnInnerViewsClickListener(OnInnerViewsClickListener l) {
        mOnInnerViewsClickListener = l;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewHolder viewHolder;

        switch (viewType) {
            case EDITDIGITALKEY_ITEM_TYPE_LABEL: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_edit_digital_key_label, parent,
                        false);
                viewHolder = new LabelViewHolder(view);
                break;
            }

            case EDITDIGITALKEY_ITEM_TYPE_SUBKEY_PREPARE: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_edit_digital_key_key, parent,
                        false);
                viewHolder = new SubKeyPrepareViewHolder(view);
                break;
            }

            case EDITDIGITALKEY_ITEM_TYPE_SUBKEY: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_edit_digital_key_key, parent,
                        false);
                viewHolder = new SubKeyViewHolder(view);
                break;
            }

            case EDITDIGITALKEY_ITEM_TYPE_TEMPKEY: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_edit_digital_key_key, parent,
                        false);
                viewHolder = new TempKeyViewHolder(view);
                break;
            }

            case EDITDIGITALKEY_ITEM_TYPE_DIVIDER: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_edit_digital_key_divider, parent,
                        false);
                viewHolder = new DividerViewHolder(view);
                break;
            }

            default: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_edit_digital_key_divider, parent,
                        false);
                viewHolder = new DividerViewHolder(view);
            }
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        RecyclerViewItemData itemData = mDisplayArrayList.get(position);
        int itemType = itemData.getItemType();
        Object dataObject = itemData.getDataObject();

        switch (itemType) {
            case EDITDIGITALKEY_ITEM_TYPE_LABEL: {
                try {
                    LabelViewHolder labelViewHolder = (LabelViewHolder) viewHolder;
                    labelViewHolder.mLabelTextView.setText(((JSONObject) dataObject).getString(DataKeys.KEY_LABEL));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
            }

            case EDITDIGITALKEY_ITEM_TYPE_SUBKEY_PREPARE: {
                SubKeyPrepareViewHolder subKeyPrepareViewHolder = (SubKeyPrepareViewHolder) viewHolder;
                KeyInfo keyInfo = (KeyInfo) dataObject;

                String phone = keyInfo.getPhone();
                String name = keyInfo.getName();
                String displayName = keyInfo.getDisplayName();
                if (!TextUtils.isEmpty(displayName)) {
                    subKeyPrepareViewHolder.mNameTextView.setText(displayName);
                } else if (!TextUtils.isEmpty(name)) {
                    subKeyPrepareViewHolder.mNameTextView.setText(name);
                } else {
                    subKeyPrepareViewHolder.mNameTextView.setText(phone);
                }

                subKeyPrepareViewHolder.mDeleteImageView.setOnClickListener(mOnSubKeyPrepareDeleteImageViewClickListener);
                subKeyPrepareViewHolder.mDeleteImageView.setTag(position);
                subKeyPrepareViewHolder.mNameTextView.setOnClickListener(mOnNameTextViewClickListener);
                subKeyPrepareViewHolder.mNameTextView.setTag(position);
                subKeyPrepareViewHolder.mCheckImageView.setOnClickListener(mOnSubKeyPrepareCheckImageViewClickListener);
                subKeyPrepareViewHolder.mCheckImageView.setTag(subKeyPrepareViewHolder);
                subKeyPrepareViewHolder.mCancelImageView.setOnClickListener(mOnCancelImageViewClickListener);
                subKeyPrepareViewHolder.mCancelImageView.setTag(position);

                if (position == mInEditingPosition) {
                    subKeyPrepareViewHolder.mDisplayLayout.setVisibility(View.GONE);
                    subKeyPrepareViewHolder.mEditLayout.setVisibility(View.VISIBLE);
                    CharSequence text = subKeyPrepareViewHolder.mNameTextView.getText();
                    subKeyPrepareViewHolder.mNameEditText.setText(text);
                    subKeyPrepareViewHolder.mNameEditText.setSelection(text.length());
                } else {
                    subKeyPrepareViewHolder.mDisplayLayout.setVisibility(View.VISIBLE);
                    subKeyPrepareViewHolder.mEditLayout.setVisibility(View.GONE);
                }

                break;
            }

            case EDITDIGITALKEY_ITEM_TYPE_SUBKEY: {
                SubKeyViewHolder subKeyViewHolder = (SubKeyViewHolder) viewHolder;
                KeyInfo keyInfo = (KeyInfo) dataObject;

                String phone = keyInfo.getPhone();
                String name = keyInfo.getName();
                String displayName = keyInfo.getDisplayName();
                if (!TextUtils.isEmpty(displayName)) {
                    subKeyViewHolder.mNameTextView.setText(displayName);
                } else if (!TextUtils.isEmpty(name)) {
                    subKeyViewHolder.mNameTextView.setText(name);
                } else {
                    subKeyViewHolder.mNameTextView.setText(phone);
                }

                subKeyViewHolder.mDeleteImageView.setOnClickListener(mOnSubKeyDeleteImageViewClickListener);
                subKeyViewHolder.mDeleteImageView.setTag(position);
                subKeyViewHolder.mNameTextView.setOnClickListener(mOnNameTextViewClickListener);
                subKeyViewHolder.mNameTextView.setTag(position);
                subKeyViewHolder.mCheckImageView.setOnClickListener(mOnSubKeyCheckImageViewClickListener);
                subKeyViewHolder.mCheckImageView.setTag(subKeyViewHolder);
                subKeyViewHolder.mCancelImageView.setOnClickListener(mOnCancelImageViewClickListener);
                subKeyViewHolder.mCancelImageView.setTag(position);

                if (position == mInEditingPosition) {
                    subKeyViewHolder.mDisplayLayout.setVisibility(View.GONE);
                    subKeyViewHolder.mEditLayout.setVisibility(View.VISIBLE);
                    CharSequence text = subKeyViewHolder.mNameTextView.getText();
                    subKeyViewHolder.mNameEditText.setText(text);
                    subKeyViewHolder.mNameEditText.setSelection(text.length());
                } else {
                    subKeyViewHolder.mDisplayLayout.setVisibility(View.VISIBLE);
                    subKeyViewHolder.mEditLayout.setVisibility(View.GONE);
                }

                break;
            }

            case EDITDIGITALKEY_ITEM_TYPE_TEMPKEY: {
                TempKeyViewHolder tempKeyViewHolder = (TempKeyViewHolder) viewHolder;
                KeyInfo keyInfo = (KeyInfo) dataObject;

                String phone = keyInfo.getPhone();
                String name = keyInfo.getName();
                String displayName = keyInfo.getDisplayName();
                if (!TextUtils.isEmpty(displayName)) {
                    tempKeyViewHolder.mNameTextView.setText(displayName);
                } else if (!TextUtils.isEmpty(name)) {
                    tempKeyViewHolder.mNameTextView.setText(name);
                } else {
                    tempKeyViewHolder.mNameTextView.setText(phone);
                }

                tempKeyViewHolder.mDeleteImageView.setOnClickListener(mOnTempKeyDeleteImageViewClickListener);
                tempKeyViewHolder.mDeleteImageView.setTag(position);
                tempKeyViewHolder.mNameTextView.setOnClickListener(mOnNameTextViewClickListener);
                tempKeyViewHolder.mNameTextView.setTag(position);
                tempKeyViewHolder.mCheckImageView.setOnClickListener(mOnTempKeyCheckImageViewClickListener);
                tempKeyViewHolder.mCheckImageView.setTag(tempKeyViewHolder);
                tempKeyViewHolder.mCancelImageView.setOnClickListener(mOnCancelImageViewClickListener);
                tempKeyViewHolder.mCancelImageView.setTag(position);

                if (position == mInEditingPosition) {
                    tempKeyViewHolder.mDisplayLayout.setVisibility(View.GONE);
                    tempKeyViewHolder.mEditLayout.setVisibility(View.VISIBLE);
                    CharSequence text = tempKeyViewHolder.mNameTextView.getText();
                    tempKeyViewHolder.mNameEditText.setText(text);
                    tempKeyViewHolder.mNameEditText.setSelection(text.length());
                } else {
                    tempKeyViewHolder.mDisplayLayout.setVisibility(View.VISIBLE);
                    tempKeyViewHolder.mEditLayout.setVisibility(View.GONE);
                }

                break;
            }

            case EDITDIGITALKEY_ITEM_TYPE_DIVIDER: {

                break;
            }

            default: {

            }
        }

    }

}
