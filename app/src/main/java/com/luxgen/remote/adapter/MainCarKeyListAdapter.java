package com.luxgen.remote.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomRecyclerViewBaseAdapter;
import com.luxgen.remote.network.model.car.CarKeyData;


public class MainCarKeyListAdapter extends CustomRecyclerViewBaseAdapter<ViewHolder> {

    public interface OnInnerViewsClickListener {
        void onCarKeyListItemViewClick(MainCarKeyListAdapter adapter, int position);

        void onCarKeyListAddNewCarTextViewClick();
    }

    public static final int MAIN_CAR_KEY_LIST_ITEM_TYPE_NONE = -1;
    public static final int MAIN_CAR_KEY_LIST_ITEM_TYPE_LABEL = 0;
    public static final int MAIN_CAR_KEY_LIST_ITEM_TYPE_CAR_KEY = 1;
    public static final int MAIN_CAR_KEY_LIST_ITEM_TYPE_ADD_NEW_CAR = 2;

    private int mSelectedItemPosition = -1;
    private boolean mEnableAddNewCarFunction = false;

    public static class LabelData {
        private String mLabelString = null;
        private int mTheLastChildItemPosition = -1;

        public void setLabel(String labelString) {
            mLabelString = labelString;
        }

        String getLabel() {
            return mLabelString;
        }

        public void setTheLastChildItemPosition(int position) {
            mTheLastChildItemPosition = position;
        }

        int getTheLastChildItemPosition() {
            return mTheLastChildItemPosition;
        }
    }

    public static class ExtraData {
        private boolean mIsCarKeyActive = false;

        public void setCarKeyActivationStatus(boolean isCarKeyActive) {
            mIsCarKeyActive = isCarKeyActive;
        }

        public boolean isCarKeyActive() {
            return mIsCarKeyActive;
        }
    }

    public static class LabelViewHolder extends ViewHolder {

        LabelViewHolder(View view) {
            super(view);
        }

        public void setVisibility(int visible) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) itemView.getLayoutParams();
            switch (visible) {
                case View.VISIBLE: {
                    layoutParams.height = RecyclerView.LayoutParams.WRAP_CONTENT;
                    layoutParams.width = RecyclerView.LayoutParams.MATCH_PARENT;
                    itemView.setVisibility(View.VISIBLE);
                    break;
                }

                case View.INVISIBLE: {
                    layoutParams.height = RecyclerView.LayoutParams.WRAP_CONTENT;
                    layoutParams.width = RecyclerView.LayoutParams.MATCH_PARENT;
                    itemView.setVisibility(View.GONE);
                    break;
                }

                case View.GONE: {
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    itemView.setVisibility(View.GONE);
                    break;
                }

                default: {
                    layoutParams.height = RecyclerView.LayoutParams.WRAP_CONTENT;
                    layoutParams.width = RecyclerView.LayoutParams.MATCH_PARENT;
                    itemView.setVisibility(View.VISIBLE);
                }
            }

            itemView.setLayoutParams(layoutParams);
        }
    }

    public static class CarKeyViewHolder extends ViewHolder {

        CarKeyViewHolder(View view) {
            super(view);
        }

        public void setVisibility(int visible) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) itemView.getLayoutParams();
            switch (visible) {
                case View.VISIBLE: {
                    layoutParams.height = RecyclerView.LayoutParams.WRAP_CONTENT;
                    layoutParams.width = RecyclerView.LayoutParams.MATCH_PARENT;
                    itemView.setVisibility(View.VISIBLE);
                    break;
                }

                case View.INVISIBLE: {
                    layoutParams.height = RecyclerView.LayoutParams.WRAP_CONTENT;
                    layoutParams.width = RecyclerView.LayoutParams.MATCH_PARENT;
                    itemView.setVisibility(View.GONE);
                    break;
                }

                case View.GONE: {
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    itemView.setVisibility(View.GONE);
                    break;
                }

                default: {
                    layoutParams.height = RecyclerView.LayoutParams.WRAP_CONTENT;
                    layoutParams.width = RecyclerView.LayoutParams.MATCH_PARENT;
                    itemView.setVisibility(View.VISIBLE);
                }
            }

            itemView.setLayoutParams(layoutParams);
        }

    }

    public static class AddNewCarViewHolder extends ViewHolder {

        AddNewCarViewHolder(View view) {
            super(view);
        }

        public void setVisibility(int visible) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) itemView.getLayoutParams();
            switch (visible) {
                case View.VISIBLE: {
                    layoutParams.height = RecyclerView.LayoutParams.WRAP_CONTENT;
                    layoutParams.width = RecyclerView.LayoutParams.MATCH_PARENT;
                    itemView.setVisibility(View.VISIBLE);
                    break;
                }

                case View.INVISIBLE: {
                    layoutParams.height = RecyclerView.LayoutParams.WRAP_CONTENT;
                    layoutParams.width = RecyclerView.LayoutParams.MATCH_PARENT;
                    itemView.setVisibility(View.GONE);
                    break;
                }

                case View.GONE: {
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    itemView.setVisibility(View.GONE);
                    break;
                }

                default: {
                    layoutParams.height = RecyclerView.LayoutParams.WRAP_CONTENT;
                    layoutParams.width = RecyclerView.LayoutParams.MATCH_PARENT;
                    itemView.setVisibility(View.VISIBLE);
                }
            }

            itemView.setLayoutParams(layoutParams);
        }
    }

    private OnInnerViewsClickListener mOnInnerViewsClickListener = null;

    private OnClickListener mOnCarKeyTextViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mOnInnerViewsClickListener != null) {
                mOnInnerViewsClickListener.onCarKeyListItemViewClick(MainCarKeyListAdapter.this, (Integer) v.getTag());
            }
        }

    };

    private OnClickListener mOnAddNewCarTextViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mOnInnerViewsClickListener != null) {
                mOnInnerViewsClickListener.onCarKeyListAddNewCarTextViewClick();
            }
        }

    };

    public MainCarKeyListAdapter(Context context) {
        super(context);
    }

    public void setOnInnerViewsClickListener(OnInnerViewsClickListener l) {
        mOnInnerViewsClickListener = l;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewHolder viewHolder;

        switch (viewType) {
            case MAIN_CAR_KEY_LIST_ITEM_TYPE_LABEL: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_main_car_key_list_label, parent,
                        false);
                viewHolder = new LabelViewHolder(view);
                break;
            }

            case MAIN_CAR_KEY_LIST_ITEM_TYPE_CAR_KEY: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_main_car_key_list_car_key, parent,
                        false);
                viewHolder = new CarKeyViewHolder(view);
                break;
            }

            case MAIN_CAR_KEY_LIST_ITEM_TYPE_ADD_NEW_CAR: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_main_car_key_list_add_new_car, parent,
                        false);
                viewHolder = new AddNewCarViewHolder(view);
                break;
            }

            default: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_main_car_key_list_label, parent,
                        false);
                viewHolder = new LabelViewHolder(view);
            }
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        RecyclerViewItemData itemData = mDisplayArrayList.get(position);
        int itemType = itemData.getItemType();
        Object dataObject = itemData.getDataObject();
        Object extraDataObject = itemData.getExtraDataObject();

        switch (itemType) {
            case MAIN_CAR_KEY_LIST_ITEM_TYPE_LABEL: {
                LabelViewHolder labelViewHolder = (LabelViewHolder) viewHolder;
                TextView labelTextView = (TextView) labelViewHolder.itemView;
                LabelData labelData = (LabelData) dataObject;
                int theLastChildItemPosition = labelData.getTheLastChildItemPosition();

                if (position == theLastChildItemPosition) { // No child item
                    labelViewHolder.setVisibility(View.GONE);
                } else if (((theLastChildItemPosition - position) == 1) &&
                        (theLastChildItemPosition == mSelectedItemPosition)) { // Just has one child and is selected
                    labelViewHolder.setVisibility(View.GONE);
                } else {
                    labelViewHolder.setVisibility(View.VISIBLE);
                }

                labelTextView.setText(labelData.getLabel());

                break;
            }


            case MAIN_CAR_KEY_LIST_ITEM_TYPE_CAR_KEY: {
                CarKeyViewHolder carKeyViewHolder = (CarKeyViewHolder) viewHolder;
                TextView carKeyTextView = (TextView) viewHolder.itemView;
                CarKeyData data = (CarKeyData) dataObject;
                ExtraData extraData = (ExtraData) extraDataObject;

                carKeyTextView.setText(data.getCarNo());

                if (position == mSelectedItemPosition) {
                    carKeyViewHolder.setVisibility(View.GONE);
                } else {
                    carKeyViewHolder.setVisibility(View.VISIBLE);
                }

                if (data.isMasterKey() || extraData.isCarKeyActive() || !data.isVig()) {
                    carKeyTextView.setTextColor(Color.WHITE);
                    carKeyTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_digital_key_white, 0, 0, 0);

                    carKeyTextView.setOnClickListener(mOnCarKeyTextViewClickListener);
                } else {
                    carKeyTextView.setTextColor(Color.GRAY);
                    carKeyTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_digital_key_white, 0, 0, 0);

                    carKeyTextView.setOnClickListener(null);
                }

                carKeyTextView.setTag(position);

                break;
            }

            case MAIN_CAR_KEY_LIST_ITEM_TYPE_ADD_NEW_CAR: {
                AddNewCarViewHolder addNewCarViewHolder = (AddNewCarViewHolder) viewHolder;
                viewHolder.itemView.setOnClickListener(mOnAddNewCarTextViewClickListener);
                if (mEnableAddNewCarFunction) {
                    addNewCarViewHolder.setVisibility(View.VISIBLE);
                } else {
                    addNewCarViewHolder.setVisibility(View.GONE);
                }
                break;
            }

            default: {

            }
        }

    }

    public void setSelectedItemPosition(int selectedItemPosition) {
        mSelectedItemPosition = selectedItemPosition;
    }

    public int getSelectedItemPosition() {
        return mSelectedItemPosition;
    }

    public void enableAddNewCarFunction(boolean enable) {
        mEnableAddNewCarFunction = enable;
    }

}
