package com.luxgen.remote.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.luxgen.remote.R;
import com.luxgen.remote.custom.CustomRecyclerViewBaseAdapter;
import com.luxgen.remote.model.KeyInfo;
import com.luxgen.remote.model.option.CarOptions;

import java.text.SimpleDateFormat;
import java.util.Locale;


public class DigitalKeyTempKeyOwnerAdapter extends CustomRecyclerViewBaseAdapter<ViewHolder> {

    public interface OnInnerViewsClickListener {
        void onTempKeyOwnerImageViewClick(DigitalKeyTempKeyOwnerAdapter adapter, int position);

        void onSetImageViewClick(DigitalKeyTempKeyOwnerAdapter adapter, int position);

        void onRenewImageViewClick(DigitalKeyTempKeyOwnerAdapter adapter, int position);

    }

    public static final int DIGITALKEY_TEMPKEY_OWNER_ITEM_TYPE_NONE = -1;
    public static final int DIGITALKEY_TEMPKEY_OWNER_ITEM_TYPE_OWNER = 0;
    public static final int DIGITALKEY_TEMPKEY_OWNER_ITEM_TYPE_SET = 1;
    public static final int DIGITALKEY_TEMPKEY_OWNER_ITEM_TYPE_RENEW = 2;

    private SimpleDateFormat mSimpleDateFormatForServer = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.TAIWAN);

    private Drawable mTempKeyDisableDrawable = null;
    private Drawable mTempKeyEnableDrawable = null;

    private boolean mIsKeySettingEnabled = true;

    public static class TempKeyOwnerViewHolder extends ViewHolder {
        ImageView mTempKeyOwnerImageView;
        TextView mTempKeyOwnerNameTextView;

        TempKeyOwnerViewHolder(View view) {
            super(view);

            mTempKeyOwnerImageView = view.findViewById(R.id.recyclerview_item_digital_key_temp_key_owner_image_view);
            mTempKeyOwnerNameTextView = view.findViewById(R.id.recyclerview_item_digital_key_temp_key_owner_name_text_view);
        }
    }

    public static class SetViewHolder extends ViewHolder {
        ImageView mSetImageView;
        TextView mSetTextView;

        SetViewHolder(View view) {
            super(view);

            mSetImageView = view.findViewById(R.id.recyclerview_item_digital_key_temp_key_set_image_view);
            mSetTextView = view.findViewById(R.id.recyclerview_item_digital_key_temp_key_set_text_view);
        }
    }

    public static class RenewViewHolder extends ViewHolder {
        ImageView mRenewImageView;

        RenewViewHolder(View view) {
            super(view);

            mRenewImageView = view.findViewById(R.id.recyclerview_item_digital_key_temp_key_renew_image_view);
        }
    }

    private OnInnerViewsClickListener mOnInnerViewsClickListener = null;

    private OnClickListener mOnTempKeyOwnerImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mOnInnerViewsClickListener != null) {
                mOnInnerViewsClickListener.onTempKeyOwnerImageViewClick(DigitalKeyTempKeyOwnerAdapter.this, (Integer) v.getTag());
            }
        }

    };

    private OnClickListener mOnSetImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mOnInnerViewsClickListener != null) {
                mOnInnerViewsClickListener.onSetImageViewClick(DigitalKeyTempKeyOwnerAdapter.this, (Integer) v.getTag());
            }
        }

    };

    private OnClickListener mOnRenewImageViewClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mOnInnerViewsClickListener != null) {
                mOnInnerViewsClickListener.onRenewImageViewClick(DigitalKeyTempKeyOwnerAdapter.this, (Integer) v.getTag());
            }
        }

    };

    public DigitalKeyTempKeyOwnerAdapter(Context context) {
        super(context);

        mTempKeyDisableDrawable = context.getDrawable(R.drawable.btn_key_disable);
        mTempKeyEnableDrawable = context.getDrawable(R.drawable.btn_key_enable);
    }

    public void setOnInnerViewsClickListener(OnInnerViewsClickListener l) {
        mOnInnerViewsClickListener = l;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewHolder viewHolder;

        switch (viewType) {
            case DIGITALKEY_TEMPKEY_OWNER_ITEM_TYPE_OWNER: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_digital_key_temp_key_owner, parent,
                        false);
                viewHolder = new TempKeyOwnerViewHolder(view);
                break;
            }

            case DIGITALKEY_TEMPKEY_OWNER_ITEM_TYPE_SET: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_digital_key_temp_key_set, parent,
                        false);
                viewHolder = new SetViewHolder(view);
                break;
            }

            case DIGITALKEY_TEMPKEY_OWNER_ITEM_TYPE_RENEW: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_digital_key_temp_key_renew, parent,
                        false);
                viewHolder = new RenewViewHolder(view);
                break;
            }

            default: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_digital_key_temp_key_renew, parent,
                        false);
                viewHolder = new RenewViewHolder(view);
            }
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        RecyclerViewItemData itemData = mDisplayArrayList.get(position);
        int itemType = itemData.getItemType();
        Object dataObject = itemData.getDataObject();

        switch (itemType) {
            case DIGITALKEY_TEMPKEY_OWNER_ITEM_TYPE_OWNER: {
                TempKeyOwnerViewHolder tempKeyOwnerViewHolder = (TempKeyOwnerViewHolder) viewHolder;
                KeyInfo keyInfo = (KeyInfo) dataObject;

                switch (keyInfo.getKeyStatus()) {
                    case CarOptions.KEY_STATUS_T2: {
                        tempKeyOwnerViewHolder.mTempKeyOwnerImageView.setImageDrawable(mTempKeyEnableDrawable);

                        String displayName = keyInfo.getDisplayName();
                        if (!TextUtils.isEmpty(displayName)) {
                            tempKeyOwnerViewHolder.mTempKeyOwnerNameTextView.setText(displayName);
                        } else {
                            String name = keyInfo.getName();
                            if (!TextUtils.isEmpty(name)) {
                                tempKeyOwnerViewHolder.mTempKeyOwnerNameTextView.setText(name);
                            } else {
                                tempKeyOwnerViewHolder.mTempKeyOwnerNameTextView.setText(keyInfo.getPhone());
                            }
                        }
                        break;
                    }

                    case CarOptions.KEY_STATUS_T1: {
                        tempKeyOwnerViewHolder.mTempKeyOwnerImageView.setImageDrawable(mTempKeyDisableDrawable);

                        String nameFormat = mContext.getString(R.string.digital_key_master_key_temp_key_t1_label);
                        String displayName = keyInfo.getDisplayName();
                        if (!TextUtils.isEmpty(displayName)) {
                            nameFormat = String.format(nameFormat, displayName);
                            tempKeyOwnerViewHolder.mTempKeyOwnerNameTextView.setText(nameFormat);
                        } else {
                            String name = keyInfo.getName();
                            if (name != null && !TextUtils.isEmpty(name)) {
                                nameFormat = String.format(nameFormat, name);
                                tempKeyOwnerViewHolder.mTempKeyOwnerNameTextView.setText(nameFormat);
                            } else {
                                nameFormat = String.format(nameFormat, keyInfo.getPhone());
                                tempKeyOwnerViewHolder.mTempKeyOwnerNameTextView.setText(nameFormat);
                            }
                        }
                        break;
                    }

                    case CarOptions.KEY_STATUS_D1: {
                        tempKeyOwnerViewHolder.mTempKeyOwnerImageView.setImageDrawable(mTempKeyDisableDrawable);

                        String nameFormat = mContext.getString(R.string.digital_key_master_key_temp_key_d1_label);
                        String displayName = keyInfo.getDisplayName();
                        if (!TextUtils.isEmpty(displayName)) {
                            nameFormat = String.format(nameFormat, displayName);
                            tempKeyOwnerViewHolder.mTempKeyOwnerNameTextView.setText(nameFormat);
                        } else {
                            String name = keyInfo.getName();
                            if (!TextUtils.isEmpty(name)) {
                                nameFormat = String.format(nameFormat, name);
                                tempKeyOwnerViewHolder.mTempKeyOwnerNameTextView.setText(nameFormat);
                            } else {
                                nameFormat = String.format(nameFormat, keyInfo.getPhone());
                                tempKeyOwnerViewHolder.mTempKeyOwnerNameTextView.setText(nameFormat);
                            }
                        }
                        break;
                    }
                }

                tempKeyOwnerViewHolder.mTempKeyOwnerImageView.setOnClickListener(mOnTempKeyOwnerImageViewClickListener);
                tempKeyOwnerViewHolder.mTempKeyOwnerImageView.setTag(position);

                break;
            }

            case DIGITALKEY_TEMPKEY_OWNER_ITEM_TYPE_SET: {
                SetViewHolder setViewHolder = (SetViewHolder) viewHolder;

                setViewHolder.mSetImageView.setTag(position);

                if (mIsKeySettingEnabled) {
                    setViewHolder.mSetImageView.setImageResource(R.drawable.btn_add_new_key);
                    setViewHolder.mSetImageView.setOnClickListener(mOnSetImageViewClickListener);
                    setViewHolder.mSetTextView.setTextColor(Color.WHITE);
                } else {
                    setViewHolder.mSetImageView.setImageResource(R.drawable.btn_add_new_key_disable);
                    setViewHolder.mSetImageView.setOnClickListener(null);
                    setViewHolder.mSetTextView.setTextColor(Color.GRAY);
                }

                break;
            }

            case DIGITALKEY_TEMPKEY_OWNER_ITEM_TYPE_RENEW: {
                RenewViewHolder renewViewHolder = (RenewViewHolder) viewHolder;

                renewViewHolder.mRenewImageView.setOnClickListener(mOnRenewImageViewClickListener);
                renewViewHolder.mRenewImageView.setTag(position);

                break;
            }

            default: {

            }
        }

    }

    public void enableKeySetting(boolean enable) {
        mIsKeySettingEnabled = enable;
    }

}
