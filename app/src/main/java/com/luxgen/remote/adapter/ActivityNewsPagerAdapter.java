package com.luxgen.remote.adapter;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ActivityNewsPagerAdapter extends PagerAdapter {
    private ArrayList<View> mViewArrayList = null;

    public ActivityNewsPagerAdapter(ArrayList<View> viewArrayList) {
        mViewArrayList = viewArrayList;
    }

    @Override
    public int getCount() {
        return mViewArrayList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = mViewArrayList.get(position);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView(mViewArrayList.get(position));
    }
}
